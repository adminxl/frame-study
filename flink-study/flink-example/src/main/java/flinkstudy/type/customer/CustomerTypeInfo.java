package flinkstudy.type.customer;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;

/**
 * @author daocr
 * @date 2019/12/17
 */
public class CustomerTypeInfo<T0, T1> extends TypeInformation<CustomerTuple<T0, T1>> {

    private TypeInformation f0;
    private TypeInformation f1;

    public CustomerTypeInfo(TypeInformation f0, TypeInformation f1) {
        this.f0 = f0;
        this.f1 = f1;
    }

    @Override
    public boolean isBasicType() {
        return false;
    }

    @Override
    public boolean isTupleType() {
        return false;
    }

    @Override
    public int getArity() {
        return 0;
    }

    @Override
    public int getTotalFields() {
        return 0;
    }

    @Override
    public Class<CustomerTuple<T0, T1>> getTypeClass() {
        return null;
    }

    @Override
    public boolean isKeyType() {
        return false;
    }

    @Override
    public TypeSerializer<CustomerTuple<T0, T1>> createSerializer(ExecutionConfig config) {
        return null;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean canEqual(Object obj) {
        return false;
    }
}
