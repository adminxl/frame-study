package flinkstudy.type.customer;

import org.apache.flink.api.common.typeinfo.TypeInfoFactory;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * 自定义  type info
 *
 * @author daocr
 * @date 2019/12/17
 */
public class CustomerTypeInfoFactory extends TypeInfoFactory<CustomerTuple> {
    @Override
    public TypeInformation<CustomerTuple> createTypeInfo(Type t, Map<String, TypeInformation<?>> genericParameters) {
        return new CustomerTypeInfo(genericParameters.get("T0"), genericParameters.get("T1"));
    }
}
