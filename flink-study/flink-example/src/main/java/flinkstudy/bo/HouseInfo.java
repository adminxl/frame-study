package flinkstudy.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author daocr
 * @date 2019/12/18
 */
@Data
@NoArgsConstructor
public class HouseInfo {

    public HouseInfo(Integer houseId) {
        this.houseId = houseId;
    }

    /**
     * 小区id
     */
    private Integer houseId;

    /**
     * 城市id
     */
    private Integer cityId;
    /**
     * 小区名称
     */
    private String houseName;
    /**
     * 区域
     */
    private Integer districtId;

    /**
     * 板块
     */
    private Integer plateId;
}
