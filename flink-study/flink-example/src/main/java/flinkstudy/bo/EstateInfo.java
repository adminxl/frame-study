package flinkstudy.bo;

import lombok.Data;

import java.util.Date;

/**
 * @author daocr
 * @date 2019/12/14
 */

@Data
public class EstateInfo {

    /**
     * 价格
     */
    private Long price;
    /**
     * 城市
     */
    private Integer cityId;
    /**
     * 区域
     */
    private Integer districtId;

    /**
     * 板块
     */
    private Integer plateId;
    /**
     * 小区
     */
    private Integer houseId;
    /**
     * 发布时间
     */
    private String createTimeFormat;

    /**
     * 创建时间
     */
    private Date createTime;

}
