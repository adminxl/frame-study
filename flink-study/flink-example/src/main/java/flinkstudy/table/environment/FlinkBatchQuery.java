package flinkstudy.table.environment;

import lombok.Data;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.table.api.java.BatchTableEnvironment;

/**
 * 基于 flink  批处理 查询
 */
@Data
public class FlinkBatchQuery {

    ExecutionEnvironment flinkBatchEnv = null;
    BatchTableEnvironment flinkBatchTableEnv = null;

    public FlinkBatchQuery() {
        flinkBatchEnv = ExecutionEnvironment.getExecutionEnvironment();
        flinkBatchTableEnv = BatchTableEnvironment.create(flinkBatchEnv);
    }
}
