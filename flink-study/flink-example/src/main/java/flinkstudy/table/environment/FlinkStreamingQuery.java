package flinkstudy.table.environment;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.java.StreamTableEnvironment;

/**
 * 基于 flink Stream 查询
 */
public class FlinkStreamingQuery {

    EnvironmentSettings bsSettings = null;
    StreamExecutionEnvironment flinkStreamEnv = null;
    StreamTableEnvironment flinkStreamTableEnv = null;

    public FlinkStreamingQuery() {
        bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
        flinkStreamEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        flinkStreamTableEnv = StreamTableEnvironment.create(flinkStreamEnv, bsSettings);
    }
}
