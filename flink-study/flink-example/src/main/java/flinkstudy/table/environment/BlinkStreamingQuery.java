package flinkstudy.table.environment;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.java.StreamTableEnvironment;

/**
 * 基于 blink Stream 查询
 */
public class BlinkStreamingQuery {

    EnvironmentSettings bsSettings = null;
    StreamExecutionEnvironment blinkStreamEnv = null;
    StreamTableEnvironment blinkStreamTableEnv = null;

    public BlinkStreamingQuery() {
        blinkStreamEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        blinkStreamTableEnv = StreamTableEnvironment.create(blinkStreamEnv, bsSettings);
        bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
    }
}
