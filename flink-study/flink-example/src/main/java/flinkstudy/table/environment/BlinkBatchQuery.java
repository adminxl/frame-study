package flinkstudy.table.environment;

import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

/**
 * 基于 blink  批处理 查询
 */
public class BlinkBatchQuery {

    EnvironmentSettings bbSettings = null;
    TableEnvironment BlinkBatchTableEnv = null;

    public BlinkBatchQuery() {
        bbSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inBatchMode().build();
        BlinkBatchTableEnv = TableEnvironment.create(bbSettings);
    }
}
