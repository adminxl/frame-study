package flinkstudy.table.mock;

import flinkstudy.Constant;
import flinkstudy.bo.EstateInfo;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.io.PojoCsvInputFormat;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.flink.core.fs.Path;

import java.io.File;

/**
 * 房源  csv 数据
 *
 * @author daocr
 * @date 2019/12/14
 */
public class EstateInfoCsvDataSource {


    /**
     * 房源表列
     */
    public static final String ESTATE_INFO_COLUMN = "price,cityId,districtId,plateId,houseId,dateTime";

    /**
     * 房源表
     */
    public static final String T_ESTATE_INFO = "t_estate_info";

    public static DataSource<EstateInfo> of(ExecutionEnvironment env) {

        // 抽取 UserBehavior 的 TypeInformation，是一个 PojoTypeInfo

        PojoTypeInfo<EstateInfo> pojoType = (PojoTypeInfo<EstateInfo>) TypeExtractor.createTypeInfo(EstateInfo.class);
        // 由于 Java 反射抽取出的字段顺序是不确定的，需要显式指定下文件中字段的顺序
        String[] fieldOrder = new String[]{"price", "cityId", "districtId", "plateId", "houseId", "dateTime"};

        // 创建 PojoCsvInputFormat
        PojoCsvInputFormat<EstateInfo> csvInput = new PojoCsvInputFormat<>(Path.fromLocalFile(new File(Constant.FilePath.ESTATE_CSV_PATH)), pojoType, fieldOrder);

        DataSource<EstateInfo> input = env.createInput(csvInput, pojoType);

        return input;
    }

}
