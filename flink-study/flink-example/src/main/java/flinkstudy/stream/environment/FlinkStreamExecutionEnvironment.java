package flinkstudy.stream.environment;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 流处理环境
 *
 * @author daocr
 * @date 2019/12/19
 */
public class FlinkStreamExecutionEnvironment {

    public StreamExecutionEnvironment streamExecutionEnvironment = null;

    public FlinkStreamExecutionEnvironment() {
        streamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment();
    }

}
