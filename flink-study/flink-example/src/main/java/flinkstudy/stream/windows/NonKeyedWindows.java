package flinkstudy.stream.windows;

import flinkstudy.bo.EstateInfo;
import flinkstudy.stream.Windows;
import flinkstudy.stream.environment.FlinkStreamExecutionEnvironment;
import flinkstudy.stream.source.mock.DynamicUnlimitedData;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ContinuousProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.junit.Before;
import org.junit.Test;

/**
 * 没有key的窗口
 *
 * <pre>
 *
 * Non-Keyed Windows
 *
 * stream
 *        .windowAll(...)           <-  required: "assigner"
 *       [.trigger(...)]            <-  optional: "trigger" (else default trigger)
 *       [.evictor(...)]            <-  optional: "evictor" (else no evictor)
 *       [.allowedLateness(...)]    <-  optional: "lateness" (else zero)
 *       [.sideOutputLateData(...)] <-  optional: "output tag" (else no side output for late data)
 *        .reduce/aggregate/fold/apply()      <-  required: "function"
 *       [.getSideOutput(...)]      <-  optional: "output tag"
 * </pre>
 *
 * @author daocr
 * @date 2019/12/19
 */
public class NonKeyedWindows {
    private FlinkStreamExecutionEnvironment flinkStreamExecutionEnvironment = null;


    @Before
    public void init() {
        flinkStreamExecutionEnvironment = new FlinkStreamExecutionEnvironment();
    }


    /**
     * 翻滚窗口：将数据根据固定窗口长度对数据进行切片。
     * 特点是：时间对齐，窗口长度固定，没有重叠。
     * 使用场景：适合做BI统计（做每个时间段的聚合统计）
     */
    @Test
    public void tumblingWindows() throws Exception {
        StreamExecutionEnvironment environment = flinkStreamExecutionEnvironment.streamExecutionEnvironment;
        environment.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);


    }


}
