package flinkstudy;

import dnl.utils.text.table.TextTable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daocr
 * @date 2019/12/18
 */
public class PrintUtils {

    /**
     * 打印日志
     *
     * @param title
     * @param rows
     */
    public static void print(List<String> title, List<List<Object>> rows) {


        List<Object[]> rowArray = new ArrayList<>();

        for (List<Object> row : rows) {
            rowArray.add(row.toArray());
        }

        TextTable tt = new TextTable(title.toArray(new String[1]), rowArray.toArray(new Object[1][]));

        tt.printTable();
    }
}
