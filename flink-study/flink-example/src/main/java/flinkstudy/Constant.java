package flinkstudy;

/**
 * @author daocr
 * @date 2019/12/14
 */
public class Constant {

    public static class FilePath {

        /**
         * 房源 csv 文件
         */
        public static String ESTATE_CSV_PATH = Constant.class.getResource("/csv/estate_info.csv").getPath();

        /**
         * 小区 csv 文件
         */
        public static String HOUSE_CSV_PATH = Constant.class.getResource("/csv/house_info.csv").getPath();


    }
}
