package org.totoro.plugin;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.totoro.config.User;

import java.util.ArrayList;
import java.util.Properties;


@Intercepts(
        {
                @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
                @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class})
        }
)
public class MybatisCache implements Interceptor {


    @Override
    public Object intercept(Invocation invocation) throws Throwable {


        ArrayList<User> list = new ArrayList<>();

        list.add(new User());

        return list;

    }

    @Override
    public Object plugin(Object target) {

        Object wrap = Plugin.wrap(target, this);
        return wrap;

    }

    @Override
    public void setProperties(Properties properties) {

        String prop1 = properties.getProperty("prop1");
        String prop2 = properties.getProperty("prop2");
        System.out.println(prop1 + "------" + prop2);

    }
}