package org.totoro.filter;


import com.alibaba.druid.support.http.WebStatFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * <pre>
 *
 *
 *
 * 传统 web.xml 配置方法
 *
 * 参考：https://app.yinxiang.com/shard/s45/nl/10505682/9db3a141-a8e3-46ec-b94a-d209b1895bfd/
 *
 *  <filter>
 *      <filter-name>TestFilter</filter-name>
 *      <filter-class>com.cppba.filter.TestFilter</filter-class>
 * </filter>
 * <filter-mapping>
*       <filter-name>TestFilter</filter-name>
 *      <url-pattern>/*</url-pattern>
 *      <init-param>
 *              <param-name>paramName</param-name>
 *              <param-value>paramValue</param-value>
 *      </init-param>
 * </filter-mapping>
 *
 *
 *
 * </pre>
 */
@WebFilter(filterName = "druidWebStatFilter", urlPatterns = "/*",
        initParams = {
                @WebInitParam(name = "exclusions", value = "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")//忽略资源
        })
public class DruidStatFilter extends WebStatFilter {
}
