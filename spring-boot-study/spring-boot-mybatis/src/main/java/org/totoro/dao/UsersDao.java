package org.totoro.dao;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.totoro.config.User;

@Mapper
public interface UsersDao {


    public User byId(@Param("id") Integer id);

}
