package org.totoro;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.totoro.config.User;
import org.totoro.dao.UsersDao;

@SpringBootApplication
// 启用事物管理器
@EnableTransactionManagement
/*
    如果不指定位置就扫描 启动类，下面同级包和同级子包下面所有的  spring 注解
 */
@ComponentScan(basePackages = {"org.totoro"})

/*
 * 1、制定扫描mybati  @Mapper 注解的接口
 *
 * 2、下面配置可达到同样效果
 * mybatis:
    typeHandlersPackage: org.totoro.dao

 *
 */
@MapperScan(basePackages = {"org.totoro.dao"})
//启用  @WebFilter @WebServlet  等注解识别
@ServletComponentScan
//@EnableWebMvc
@Controller
public class App {

    @Autowired
    private UsersDao usersDao;

    private final static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);

    }

    @RequestMapping("/userSelect")
    @ResponseBody
    public User userSelect() {

        User user = usersDao.byId(1);

        return user;
    }
}
