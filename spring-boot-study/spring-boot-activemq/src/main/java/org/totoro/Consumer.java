package org.totoro;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @author yhl
 * @Description: 消费者
 * @date：2017/9/30 0030
 * @tiem：15:10
 */
@Component
public class Consumer {

    /**
     * 订阅 mytest.queue 的消息！
     *
     * @param text
     */
    @JmsListener(destination = "mytest.queue")
    public void receiveQueue(String text) throws InterruptedException {
        Thread thread = Thread.currentThread();
        System.out.println("Consumer收到的报文为:" + text + thread.getName() +"  method :" + "receiveQueue1" );
        Thread.sleep(5000);
    }

    /**
     * 订阅 mytest.queue 的消息！
     *
     * @param text
     */
    @JmsListener(destination = "mytest.queue")
    public void receiveQueue2(String text) throws InterruptedException {
        Thread thread = Thread.currentThread();
        System.out.println("Consumer收到的报文为:" + text + thread.getName() +"  method :" + "receiveQueue2" );
        Thread.sleep(5000);

    }
}
