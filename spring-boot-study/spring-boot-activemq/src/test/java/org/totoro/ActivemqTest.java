package org.totoro;

import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.Destination;

/**
 * @author yhl
 * @Description:
 * @date：2017/9/30 0030
 * @tiem：15:13
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivemqTest {

    @Autowired
    private Producer producer;

    @Test
    public void contextLoads() throws InterruptedException {
        /**
         * 创建消息队列
         */
        Destination destination = new ActiveMQQueue("mytest.queue");

        for (int i = 0; i < 100; i++) {
            producer.sendMessage(destination, "我是测试消息！" + i);
            Thread.sleep(1000);
        }
    }

}
