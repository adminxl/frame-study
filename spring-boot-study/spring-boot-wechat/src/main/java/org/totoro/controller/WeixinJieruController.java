//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.totoro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.weixin4j.WeixinException;
import org.weixin4j.spi.IMessageHandler;
import org.weixin4j.util.TokenUtil;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/certification")
public class WeixinJieruController {
    @Autowired
    private IMessageHandler messageHandler;

    public WeixinJieruController() {
    }

    @RequestMapping(
            method = {RequestMethod.GET}
    )
    public void get(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String token = TokenUtil.get();
        String echostr = request.getParameter("echostr");
        if (TokenUtil.checkSignature(token, signature, timestamp, nonce)) {
            response.getWriter().write(echostr);
        }

    }

    @RequestMapping(
            method = {RequestMethod.POST}
    )
    public void post(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String token = TokenUtil.get();
        if (!TokenUtil.checkSignature(token, signature, timestamp, nonce)) {
            response.getWriter().write("");
        } else {
            try {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/xml");
                ServletInputStream in = request.getInputStream();
                String xml = this.messageHandler.invoke(in);
                response.getWriter().write(xml);
            } catch (IOException var9) {
                response.getWriter().write("");
            } catch (WeixinException var10) {
                response.getWriter().write("");
            }

        }
    }
}
