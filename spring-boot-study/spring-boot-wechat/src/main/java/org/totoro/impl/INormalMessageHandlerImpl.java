package org.totoro.impl;

import org.springframework.stereotype.Component;
import org.weixin4j.model.message.Articles;
import org.weixin4j.model.message.OutputMessage;
import org.weixin4j.model.message.normal.*;
import org.weixin4j.model.message.output.NewsOutputMessage;
import org.weixin4j.spi.INormalMessageHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-09-18
 */
@Component
public class INormalMessageHandlerImpl implements INormalMessageHandler {

    @Override
    public OutputMessage textTypeMsg(TextInputMessage msg) {

        NewsOutputMessage newsOutputMessage = new NewsOutputMessage();

        Articles articles = new Articles();
        articles.setTitle("测试");
        articles.setPicUrl("https://avatar.csdn.net/4/C/6/3_yakson.jpg");
        articles.setUrl("https://www.baidu.com");

        List<Articles> list = new ArrayList<>();
        list.add(articles);

        newsOutputMessage.setArticles(list);

        return newsOutputMessage;
    }

    @Override
    public OutputMessage imageTypeMsg(ImageInputMessage msg) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public OutputMessage voiceTypeMsg(VoiceInputMessage msg) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public OutputMessage videoTypeMsg(VideoInputMessage msg) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public OutputMessage shortvideoTypeMsg(ShortVideoInputMessage msg) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public OutputMessage locationTypeMsg(LocationInputMessage msg) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public OutputMessage linkTypeMsg(LinkInputMessage msg) {
        //To change body of generated methods, choose Tools | Templates.
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
