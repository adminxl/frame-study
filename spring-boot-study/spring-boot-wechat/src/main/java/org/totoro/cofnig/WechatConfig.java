package org.totoro.cofnig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.weixin4j.spi.DefaultMessageHandler;
import org.weixin4j.spi.IMessageHandler;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-09-18
 */
@Configuration
public class WechatConfig {

    @Bean
    public IMessageHandler getDefaultMessageHandler() {
        return new DefaultMessageHandler();
    }
}
