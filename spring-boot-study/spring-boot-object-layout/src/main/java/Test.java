import com.sun.tools.attach.AttachNotSupportedException;
import com.vip.vjtools.vjmap.oops.HeapUtils;
import sun.jvm.hotspot.HotSpotAgent;
import sun.jvm.hotspot.gc_implementation.parallelScavenge.PSOldGen;
import sun.jvm.hotspot.gc_implementation.parallelScavenge.PSYoungGen;
import sun.jvm.hotspot.gc_implementation.shared.MutableSpace;
import sun.jvm.hotspot.gc_interface.CollectedHeap;
import sun.jvm.hotspot.memory.ConcurrentMarkSweepGeneration;
import sun.jvm.hotspot.memory.ContiguousSpace;
import sun.jvm.hotspot.memory.DefNewGeneration;
import sun.jvm.hotspot.memory.EdenSpace;
import sun.jvm.hotspot.runtime.VM;

import java.io.IOException;

/**
 * @author daocr
 * @date 2019-05-16
 */
public class Test {

    public static void main(String[] args) throws IOException, AttachNotSupportedException {


        HotSpotAgent agent = new HotSpotAgent();

        agent.attach(1290);

        CollectedHeap heap;

        EdenSpace cmsEden = null;
        ContiguousSpace cmsSur = null;
        ConcurrentMarkSweepGeneration cmsOld = null;

        MutableSpace parEden = null;
        MutableSpace parSur = null;
        PSOldGen parOld = null;
        boolean isCms = false;


        heap = HeapUtils.getHeap();
        if (HeapUtils.isCMSGC(heap)) {
            DefNewGeneration youngGen = HeapUtils.getYoungGenForCMS(heap);
            cmsEden = youngGen.eden();
            cmsSur = youngGen.from();
            cmsOld = HeapUtils.getOldGenForCMS(heap);
            isCms = true;
        } else if (HeapUtils.isParallelGC(heap)) {
            PSYoungGen youngGen = HeapUtils.getYongGenForPar(heap);
            parEden = youngGen.edenSpace();
            parSur = youngGen.fromSpace();
            parOld = HeapUtils.getOldGenForPar(heap);
            isCms = false;
        } else {
            throw new RuntimeException("Only support CMS and Parallel GC. Unsupport Heap:" + heap.getClass().getName());
        }

        System.out.println(cmsEden);


    }
}
