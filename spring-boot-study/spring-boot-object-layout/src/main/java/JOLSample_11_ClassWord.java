import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

import static java.lang.System.out;

// JVM Args : -Djol.tryWithSudo=true
public class JOLSample_11_ClassWord {

    public static void main(String[] args) throws Exception {

        out.println(VM.current().details());

        A a = new A();


        synchronized (a){
            System.out.println( a.hashCode());

            String s = ClassLayout.parseInstance(a).toPrintable();
            System.out.println(s);
        }



    //    Thread.sleep(Integer.MAX_VALUE);

    }

    public static class A {
        // no fields
    }

    public static class B {
        // no fields
    }


//    2054881392
//    JOLSample_11_ClassWord$A object internals:
//    OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
//            0     4        (object header)                           01 70 00 7b (00000001 01110000 00000000 01111011) (2063626241)
//            4     4        (object header)                           7a 00 00 00 (01111010 00000000 00000000 00000000) (122)
//            8     4        (object header)                           16 00 01 f8 (00010110 00000000 00000001 11111000) (-134152170)
//            12     4        (loss due to the next object alignment)
//    Instance size: 16 bytes
//    Space losses: 0 bytes internal + 4 bytes external = 4 bytes total


//    2054881392
//    JOLSample_11_ClassWord$A object internals:
//    OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
//            0     4        (object header)                           3a 5b 00 4e (00111010 01011011 00000000 01001110) (1308646202)
//            4     4        (object header)                           80 7f 00 00 (10000000 01111111 00000000 00000000) (32640)
//            8     4        (object header)                           16 00 01 f8 (00010110 00000000 00000001 11111000) (-134152170)
//            12     4        (loss due to the next object alignment)
//    Instance size: 16 bytes
//    Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
}