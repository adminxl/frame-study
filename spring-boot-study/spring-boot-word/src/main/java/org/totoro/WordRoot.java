package org.totoro;

import lombok.Data;

import java.util.List;

/**
 * @author daocr
 * @date 2019-09-02
 */
@Data
public class WordRoot {

    private String content;

    /**
     * 词根
     */
    private List<String> roots;

    /**
     * 词根解释
     */
    private String rootExplanation;

    // 词源 正则表达式
    //【词源】(.|\n)*?【引申】


    // [a-z]{1,}\s*\[
}
