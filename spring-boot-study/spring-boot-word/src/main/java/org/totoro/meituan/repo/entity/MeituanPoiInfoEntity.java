package org.totoro.meituan.repo.entity ;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * poi 信息(MeituanPoiInfo)实体类
 *
 * @author makejava
 * @since 2019-12-27 23:46:18
 */
@Entity
@Table(name = "meituan_poi_info")
public class MeituanPoiInfoEntity implements Serializable {

    private static final long serialVersionUID = 799392796874084525L;
    
    @Column(name = "id")
    private Long id;
    
    /**
    *poi id
    */
    @Column(name = "poi_id")
    private String poiId;
    
    /**
    *平均价
    */
    @Column(name = "avg_price")
    private Integer avgPrice;
    
    /**
    *评价评价
    */
    @Column(name = "avg_score")
    private Double avgScore;
    
    /**
    *poi 名称
    */
    @Column(name = "poi_name")
    private String poiName;
    
    /**
    *区域名称
    */
    @Column(name = "area_name")
    private String areaName;
    
    /**
    *城市
    */
    @Column(name = "city_code")
    private String cityCode;
    
    /**
    *区域
    */
    @Column(name = "district_code")
    private String districtCode;
    
    /**
    *板块 （商圈）
    */
    @Column(name = "plate_code")
    private String plateCode;
    
    /**
    *地址
    */
    @Column(name = "address")
    private String address;
    
    /**
    *联系电话
    */
    @Column(name = "phone")
    private String phone;
    
    /**
    *开店时间
    */
    @Column(name = "begin_time")
    private Date beginTime;
    
    /**
    *营业时间
    */
    @Column(name = "open_time")
    private String openTime;
    
    @Column(name = "lng")
    private Double lng;
    
    @Column(name = "lat")
    private Double lat;
    
    /**
    *品牌id
    */
    @Column(name = "brand_id")
    private Object brandId;
    
    /**
    *品牌名称
    */
    @Column(name = "brand_name")
    private String brandName;
    
    /**
    *第一分类
    */
    @Column(name = "firstCate")
    private String firstcate;
    
    /**
    *第二分类
    */
    @Column(name = "sub_cate")
    private String subCate;
    
    /**
    *第三分类
    */
    @Column(name = "thrid_cate")
    private String thridCate;
    
    /**
    *评论数量
    */
    @Column(name = "comment_cnt")
    private Integer commentCnt;
    
    /**
    *创建时间
    */
    @Column(name = "create_time")
    private Date createTime;
    
    /**
    *更新时间
    */
    @Column(name = "update_time")
    private Date updateTime;
    


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPoiId() {
        return poiId;
    }

    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    public Integer getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(Integer avgPrice) {
        this.avgPrice = avgPrice;
    }

    public Double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(Double avgScore) {
        this.avgScore = avgScore;
    }

    public String getPoiName() {
        return poiName;
    }

    public void setPoiName(String poiName) {
        this.poiName = poiName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getPlateCode() {
        return plateCode;
    }

    public void setPlateCode(String plateCode) {
        this.plateCode = plateCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Object getBrandId() {
        return brandId;
    }

    public void setBrandId(Object brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getFirstcate() {
        return firstcate;
    }

    public void setFirstcate(String firstcate) {
        this.firstcate = firstcate;
    }

    public String getSubCate() {
        return subCate;
    }

    public void setSubCate(String subCate) {
        this.subCate = subCate;
    }

    public String getThridCate() {
        return thridCate;
    }

    public void setThridCate(String thridCate) {
        this.thridCate = thridCate;
    }

    public Integer getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(Integer commentCnt) {
        this.commentCnt = commentCnt;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}