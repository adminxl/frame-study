package org.totoro;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.totoro.dao.ItemRepository;
import org.totoro.entity.Item;

/**
 * @author daocr
 * @date 2020/5/12
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TemplateTest {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 排序
     * <pre>
     *     select * from Item order by price asc
     * </pre>
     */
    @Test
    public void searchAndSort() {
        // 构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 添加基本分词查询
        queryBuilder.withQuery(QueryBuilders.termQuery("category", "手机"));

        // 排序
        queryBuilder.withSort(SortBuilders.fieldSort("price").order(SortOrder.ASC));

        AggregatedPage<Item> items = elasticsearchTemplate.queryForPage(queryBuilder.build(), Item.class);

//        // 总条数
        long total = items.getTotalElements();
        System.out.println("总条数 = " + total);

        for (Item item : items) {
            System.out.println(item);
        }
 
    }


}
