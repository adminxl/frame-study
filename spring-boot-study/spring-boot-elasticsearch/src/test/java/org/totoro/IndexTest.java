package org.totoro;

import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.admin.indices.template.put.PutIndexTemplateRequest;
import org.elasticsearch.action.admin.indices.template.put.PutIndexTemplateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.AliasBuilder;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.test.context.junit4.SpringRunner;
import org.totoro.entity.HouseInfo;
import org.totoro.entity.Item;
import org.totoro.entity.StockRefEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author daocr
 * @date 2019-08-11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class IndexTest {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    /**
     * 创建索引
     */
    @Test
    public void testCreateIndex() {
        elasticsearchTemplate.createIndex(HouseInfo.class);
        elasticsearchTemplate.putMapping(HouseInfo.class);
    }

    /**
     * 删除索引
     */
    @Test
    public void testDelIndex() {
        elasticsearchTemplate.deleteIndex(Item.class);

        AliasQuery aliasQuery = new AliasQuery();
    }

    /**
     * 创建映射
     */
    @Test
    public void testCreateMapping() {
        elasticsearchTemplate.putMapping(Item.class);
    }


    /**
     * 创建 别名
     */
    @Test
    public void createAliasName() {

        String aliasName = "tbs-alias";
        AliasQuery aliasQuery = new AliasBuilder()
                .withIndexName("tbs-2019")
                .withAliasName(aliasName).build();

        elasticsearchTemplate.addAlias(aliasQuery);


    }

    /**
     * 查询索引 关联的别名
     */

    @Test
    public void queryIndexLink2Alias() {
        /**
         *
         */
        List<AliasMetaData> aliasMetaData = elasticsearchTemplate.queryForAlias("tbs-2019");

        for (AliasMetaData aliasMetaDatum : aliasMetaData) {
            System.out.println(aliasMetaDatum);
        }
    }

    /**
     * 创建索引 模板
     */
    @Test
    public void createIndexTemplate() throws IOException {


        PutIndexTemplateRequest request = new PutIndexTemplateRequest("ubi_index_template");

        List<String> indexPatterns = new ArrayList<String>();
        indexPatterns.add("ubi*");
        request.patterns(indexPatterns);

        /** 创建映射 */


        XContentBuilder jsonBuilder = XContentFactory.jsonBuilder()
                .startObject()
                .startObject("_source")
                .field("enabled", false)
                .endObject()
                    .startObject("properties")

                        .startObject("host_name")
                        .field("type", "keyword")
                        .endObject()

                        .startObject("created_at")
                        .field("type", "date")
                        .field("format", "yyyy-MM-dd HH:mm:ss")
                        .endObject()

                    .endObject()
                .endObject();

        request.mapping("_doc", jsonBuilder);


        Map<String, Object> settings = new HashMap<>();

        settings.put("number_of_shards", 1);
        request.settings(settings);

        ActionFuture<PutIndexTemplateResponse> putIndexTemplateResponseActionFuture = elasticsearchTemplate.getClient().admin().indices().putTemplate(request);

        System.out.println(putIndexTemplateResponseActionFuture.actionGet());

    }

}
