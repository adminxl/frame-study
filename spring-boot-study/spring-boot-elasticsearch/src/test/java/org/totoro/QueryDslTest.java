package org.totoro;

import com.google.gson.Gson;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.*;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.index.reindex.UpdateByQueryAction;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.search.sort.GeoDistanceSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.test.context.junit4.SpringRunner;
import org.totoro.dao.HouseRepository;
import org.totoro.dao.ItemRepository;
import org.totoro.entity.HouseInfo;
import org.totoro.entity.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * 关键类：
 *
 * @author daocr
 * @date 2019-08-11
 * @see NativeSearchQueryBuilder
 * @see QueryBuilders
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class QueryDslTest {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private HouseRepository houseRepository;

    /**
     * query dsl match query
     *
     * <pre>
     *
     * {
     *   "from": 0,
     *   "query": {
     *     "match": {
     *       "title": {
     *         "query": "坚果"
     *       }
     *     }
     *   }
     * }
     * </pre>
     */
    @Test
    public void testMathQuery() {
        // 创建对象
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 在queryBuilder对象中自定义查询
        //matchQuery:底层就是使用的termQuery
        queryBuilder.withQuery(QueryBuilders.matchQuery("title", "坚果"));
        //查询，search 默认就是分页查找
        Page<Item> page = this.itemRepository.search(queryBuilder.build());
        //获取数据
        long totalElements = page.getTotalElements();
        System.out.println("获取的总条数:" + totalElements);

        for (Item item : page) {
            System.out.println(item);
        }


    }


    /**
     * query dsl term Query
     *
     *
     * <pre>
     *
     * {
     *   "from": 0,
     *   "query": {
     *     "term": {
     *       "price": {
     *         "value": 3699
     *       }
     *     }
     *   }
     * }
     * </pre>
     */
    @Test
    public void testTermQuery() {
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        builder.withQuery(QueryBuilders.termQuery("price", 3699));
        // 查找
        Page<Item> page = this.itemRepository.search(builder.build());

        for (Item item : page) {
            System.out.println(item);
        }
    }

    /**
     * query dsl bool query
     *
     * <pre>
     *
     *
     * {
     *   "from": 0,
     *   "query": {
     *     "bool": {
     *       "must": [
     *         {
     *           "match": {
     *             "title": {
     *               "query": "华为"
     *             }
     *           }
     *         },
     *         {
     *           "match": {
     *             "brand": {
     *               "query": "华为"
     *             }
     *           }
     *         }
     *       ]
     *     }
     *   },
     *   "version": true
     * }
     *
     * </pre>
     */
    @Test
    public void testBooleanQuery() {
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();

        builder.withQuery(
                QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("title", "华为"))
                        .must(QueryBuilders.matchQuery("brand", "华为"))
        );

        // 查找
        Page<Item> page = this.itemRepository.search(builder.build());
        for (Item item : page) {
            System.out.println(item);
        }
    }

    /**
     * 模糊查询
     */
    @Test
    public void testFuzzyQuery() {
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        builder.withQuery(QueryBuilders.fuzzyQuery("title", "faceoooo"));
        Page<Item> page = this.itemRepository.search(builder.build());
        for (Item item : page) {
            System.out.println(item);
        }

    }


    /**
     * 分页查询
     */

    @Test
    public void searchByPage() {
        // 构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 添加基本分词查询
        queryBuilder.withQuery(QueryBuilders.termQuery("category", ""));
        // 分页：
        int page = 0;
        int size = 2;
        queryBuilder.withPageable(PageRequest.of(page, size));

        // 搜索，获取结果
        Page<Item> items = this.itemRepository.search(queryBuilder.build());
        // 总条数
        long total = items.getTotalElements();
        System.out.println("总条数 = " + total);
        // 总页数
        System.out.println("总页数 = " + items.getTotalPages());
        // 当前页
        System.out.println("当前页：" + items.getNumber());
        // 每页大小
        System.out.println("每页大小：" + items.getSize());

        for (Item item : items) {
            System.out.println(item);
        }

        BoolQueryBuilder qb = QueryBuilders.boolQuery();

        GeoDistanceQueryBuilder location = QueryBuilders.geoDistanceQuery("location");
        location.distance("1km");


        itemRepository.search(qb);
    }

    public void searchNearWithOrder(double lon, double lat, String distance, @PageableDefault Pageable pageable) {

        //搜索字段为 location
        GeoDistanceQueryBuilder geoBuilder = new GeoDistanceQueryBuilder("location");
        geoBuilder.point(lat, lon);//指定从哪个位置搜索
        geoBuilder.distance(distance, DistanceUnit.KILOMETERS);//指定搜索多少km

        //距离排序
        GeoDistanceSortBuilder sortBuilder = new GeoDistanceSortBuilder("location", lat, lon);
        sortBuilder.order(SortOrder.ASC);//升序
        sortBuilder.unit(DistanceUnit.METERS);

        //构造查询器
        NativeSearchQueryBuilder qb = new NativeSearchQueryBuilder()
                .withPageable(pageable)
                .withFilter(geoBuilder)
                .withSort(sortBuilder);

        //可添加其他查询条件
        //qb.must(QueryBuilders.matchQuery("address", address));
        Page<Item> search = itemRepository.search(qb.build());
//        List<Location> list = page.getContent();
//        list.forEach(l -> {
//            double calculate = GeoDistance.PLANE.calculate(l.getLocation().getLat(), l.getLocation().getLon(), lat, lon, DistanceUnit.METERS);
//            l.setDistanceMeters("距离" + (int)calculate + "m");
//        });
//        return list;
    }

    /**
     * 别名查询
     */

    @Test
    public void aliasQuery() {

        BoolQueryBuilder boolQuery = new BoolQueryBuilder();


        // 构建查询条件
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder().withQuery(boolQuery);

        // 指定别名
        builder.withIndices("tbs-alias");
        // 指定类
        builder.withTypes("pay");

        //Execute the query
        SearchQuery searchQuery = builder.build();


        AggregatedPage<Item> items = elasticsearchTemplate.queryForPage(searchQuery, Item.class);

        items.get().forEach(System.out::println);

    }


    /**
     * 批量插入
     */
    @Test
    public void bulkIndex() {

        Gson gson = new Gson();
        List<IndexQuery> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Item item = new Item();
            item.setBrand("OPPO");
            item.setCategory("手机");
            item.setPrice(1000D);
            IndexQuery pay = new IndexQueryBuilder().withIndexName("tbs-2019")
                    .withType("pay")
                    .withSource(gson.toJson(item)).build();
            list.add(pay);

        }

        elasticsearchTemplate.bulkIndex(list);
    }

    /**
     * <pre>
     *
     *     GET second_hand_house_deal_2020-08-19_1/_search
     *     {
     *     "query":{
     *         "bool":{
     *             "must":[
     *                 {
     *                     "match_all":{
     *
     *                     }
     *                 },
     *                 {
     *                     "match_phrase":{
     *                         "cityId":{
     *                             "query":605
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "match_phrase":{
     *                         "firstHand":{
     *                             "query":2
     *                         }
     *                     }
     *                 },
     *                 {
     *                     "range":{
     *                         "bargaindate":{
     *                             "gte":"2020-07-01",
     *                             "lt":"2020-08-01"
     *                         }
     *                     }
     *                 }
     *             ],
     *             "filter":[
     *
     *             ],
     *             "should":[
     *
     *             ],
     *             "must_not":[
     *
     *             ]
     *         }
     *     }
     * }
     *
     *
     * </pre>
     */

    @Test
    public void del() {

        String indexName = "house_info_2020.06.02";
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.termQuery("plate_id", "5219"));

        BulkByScrollResponse bulkByScrollResponse = DeleteByQueryAction.INSTANCE.
                newRequestBuilder(elasticsearchTemplate.getClient()).
                filter(boolQueryBuilder).source(indexName).get();

        long deleted = bulkByScrollResponse.getDeleted();

        System.out.println(deleted);

    }

    /**
     * <pre>
     *
     *      query: 文档必须匹配才能更新的查询。
     *      script: inline 脚本源。
     *      script_file: 文件脚本名称。
     *      script_id: 索引脚本 id。
     *      lang: 脚本语言。
     *           @see ScriptType
     *      params: 脚本参数。
     *
     *
     *
     *   上下文支持字段：
     *     ctx
     *       _source
     *       _index
     *       _type
     *       _id
     *       _version
     *       _routing
     *       _parent
     *       _timestamp
     *       _ttl
     *
     *
     * </pre>
     */

    public void update() {


        String indexName = "house_info_2020.06.02";

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.termQuery("plate_id", "5219"));


        Script painless = new Script("painless");


        BulkByScrollResponse bulkByScrollResponse = UpdateByQueryAction.INSTANCE.
                newRequestBuilder(elasticsearchTemplate.getClient()).
                filter(boolQueryBuilder)
                .script(painless).source(indexName).get();

        long deleted = bulkByScrollResponse.getDeleted();

        System.out.println(deleted);


    }


    @Test
    public void test1() {
        HouseInfo houseInfo = new HouseInfo();
        houseInfo.setCity_id(10L);
        houseInfo.setHouse_id(14L);
        houseInfo.setPlate_id(10L);
        houseInfo.setDistrict_id(10L);
        GeoPoint geoPoint = new GeoPoint(10D, 20D);
        houseInfo.setLocation(geoPoint);

        houseRepository.save(houseInfo);
    }


}
