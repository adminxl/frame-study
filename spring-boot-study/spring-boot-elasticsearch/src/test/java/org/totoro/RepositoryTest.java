package org.totoro;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.totoro.dao.ItemRepository;
import org.totoro.entity.Item;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTest {

    @Autowired
    private ItemRepository tbsDao;



    /**
     * 添加
     */
    @Test
    public void insert() {
        Item item = new Item(1L, "小米手机7", "手机", "小米", 3499.00, "123");
        tbsDao.save(item);
    }

    /**
     * 批量添加
     */
    @Test
    public void insertList() {
        List<Item> list = new ArrayList<>();
        list.add(new Item(2L, "坚果手机R1", "手机", "锤子", 3699.00, "http://image.baidu.com/13123.jpg"));
        list.add(new Item(3L, "华为META10", "手机", "华为", 4499.00, "http://image.baidu.com/13123.jpg"));
        list.add(new Item(4L, "小米Mix2S", "手机", "小米", 4299.00, "http://image.baidu.com/13123.jpg"));
        list.add(new Item(5L, "荣耀V10", "手机", "华为", 2799.00, "http://image.baidu.com/13123.jpg"));
        // 接收对象集合，实现批量新增
        tbsDao.saveAll(list);

    }


    @Test
    public void update() {
        Item item = new Item(1L, "小米手机7", " 手机", "小米", 3499.00, "123");
        item.setTitle("华为 p30");
        tbsDao.save(item);
    }


    @Test
    public void del() {
        Item item = new Item(1L, "小米手机7", " 手机", "小米", 3499.00, "123");
        item.setTitle("华为 p30");
        tbsDao.delete(item);
    }


    @Test
    public void testQueryAll() {
        // 查找所有
        //Iterable<Item> list = this.itemRepository.findAll();
        // 对某字段排序查找所有 Sort.by("price").descending() 降序
        // Sort.by("price").ascending():升序
        Iterable<Item> list = this.tbsDao.findAll(Sort.by("price").ascending());

        for (Item item : list) {
            System.out.println(item.getBrand());
        }
    }


}