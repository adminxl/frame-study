package org.totoro.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.io.Serializable;
import java.util.Date;

/**
 * 楼盘详情页交易明细数据表(HouseDetailDealData)实体类
 *
 * @author daocr
 * @since 2020-07-21 13:56:34
 */
@Data
public class HouseDetailDealDataBase implements Serializable {

    private static final long serialVersionUID = 581971417969137117L;

    @Id
    private String id;

    public HouseDetailDealDataBase() {
    }

    /**
     * 小区名称
     */
    @Field(type = FieldType.Keyword)
    private String house_name;
    /**
     * 区域id
     */
    @Field(type = FieldType.Integer)
    private int district_id;
    /**
     * 板块id
     */
    @Field(type = FieldType.Integer)
    private int plate_id;

    /**
     * 经纬度信息
     */
    @GeoPointField
    private GeoPoint location;

    /**
     * 排名
     */
    @Field(type = FieldType.Double)
    private Double rank;

    @Field(type = FieldType.Long)
    private Long bargainid;

    /**
     * 来源,99:交易数据,非99是抓取数据
     */
    @Field(type = FieldType.Integer)
    private Integer sourceId;

    /**
     * 小区ID
     */
    @Field(type = FieldType.Integer)
    private Integer houseId;

    /**
     * 交易日期
     */
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd")
    private Date bargaindate;

    /**
     * 合同单价
     */

    @Field(type = FieldType.Long)
    private Long sinprice;

    /**
     * 合同总价
     */
    @Field(type = FieldType.Long)
    private Long totalprice;

    /**
     * 面积
     */
    @Field(type = FieldType.Long)
    private Long bargainArea;

    /**
     * 室
     */
    @Field(type = FieldType.Keyword)
    private String roominfo;

    /**
     * 楼栋号
     */
    @Field(type = FieldType.Keyword)
    private String building;

    /**
     * 户型
     */
    @Field(type = FieldType.Keyword)
    private String roomtype;

    /**
     * 户型编码
     */
    @Field(type = FieldType.Long)
    private Long roomtypeId;

    /**
     * 楼层
     */
    @Field(type = FieldType.Integer)
    private Integer floor;

    /**
     * 新房/二手房
     */
    @Field(type = FieldType.Integer)
    private Long firstHand;

    /**
     * 中介简称
     */
    @Field(type = FieldType.Keyword)
    private String agentShortname;

    /**
     * 做低标识
     */
    @Field(type = FieldType.Integer)
    private Integer priceTag;

    /**
     * 中介门店名称
     */
    @Field(type = FieldType.Keyword)
    private String fullStoreName;

    /**
     * 经纪人名称
     */
    @Field(type = FieldType.Keyword)
    private String superiorName;

    /**
     * 手机联系方式
     */
    @Field(type = FieldType.Keyword)
    private String contactinfo;

    /**
     * 源数据是否有电话,Y:有
     */
    @Field(type = FieldType.Keyword)
    private String isBroker;

    /**
     * 复原单价（做低时为估价单价，否则为成交实价单价/合同单价）
     */
    @Field(type = FieldType.Long)
    private Long sinpriceAvg;

    /**
     * 评估总价
     */
    @Field(type = FieldType.Long)
    private Long evalTotalprice;

    @Field(type = FieldType.Integer)
    private Integer cityId;

    /**
     * 价格标签, 1:签约实价（成交实价）; 2:行情参考（兔估价）
     */
    @Field(type = FieldType.Keyword)
    private String priceLabel;

    /**
     * 经纪人是匹配的:1 其他:0
     */
    @Field(type = FieldType.Integer)
    private Integer superiorFlag;

    /**
     * 楼层描述：中高低
     */
    @Field(type = FieldType.Keyword)
    private String floorTag;

    /**
     * 估价的单价
     */
    @Field(type = FieldType.Long)
    private Long evalPrice;

    /**
     * 成交实价单价
     */

    @Field(type = FieldType.Long)
    private Long actlPrice;

    /**
     * 成交实价总价
     */

    @Field(type = FieldType.Long)
    private Long actlTotalprice;

    /**
     * 估价生成时间
     */
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date evalTime;


}