package org.totoro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.GeoPointField;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

/**
 * @author daocr
 * @date 2020/6/2
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "tbs-house-info", type = "house-001")
public class HouseInfo {

    @Field(type = FieldType.Long)
    @Id
    private Long house_id;

    @Field(type = FieldType.Long)
    private Long city_id;


    @Field(type = FieldType.Long)
    private Long plate_id;

    @Field(type = FieldType.Long)
    private Long district_id;
    /**
     * geo 数据类型
     */
    @GeoPointField
    private GeoPoint location;


}
