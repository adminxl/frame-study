package org.totoro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * 股票 被基金引用信息
 *
 * @author daocr
 * @date 2020/5/23
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "stock_ref", type = "stock_ref")
public class StockRefEntity {

    public static String indexName = "stock_ref";

    public static String indexType = "stock_ref";

    @Id
    @Field(type = FieldType.Keyword)
    private String id;


    /**
     * SECNAME,股票名称
     */
    @Field(type = FieldType.Keyword)
    private String secName;

    /**
     * F003N,持股总市值
     */
    @Field(type = FieldType.Double)
    private double totalAmount;
    /**
     * F001N,基金引用次数
     */
    @Field(type = FieldType.Integer)
    private int refNum;

    /**
     * F002N,持股总数
     */
    @Field(type = FieldType.Long)
    private Long totalNum;
    /**
     * ID,排名
     */

    @Field(type = FieldType.Integer)
    private int sort;
    /**
     * SECCODE,股票代码
     */
    @Field(type = FieldType.Keyword)
    private String secCode;
    /**
     * ENDDATE,发布时间
     */
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;

    /**
     * 创建时间
     */
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    /**
     * 更新时间
     */
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateDate;


}
