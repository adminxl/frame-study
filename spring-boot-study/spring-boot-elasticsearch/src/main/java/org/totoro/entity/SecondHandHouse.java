package org.totoro.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @author daocr
 * @date 2020/7/21
 */
@Data
@Document(indexName = "second_hand_house", type = "second_hand_house")
public class SecondHandHouse extends HouseDetailDealDataBase {


    /**
     * 成交数据导入日期
     */
    @Field(type = FieldType.Date, format = DateFormat.custom, pattern = "yyyy-MM-dd")
    private Date addDate;

    /**
     * 1：与抓取成交匹配  0：与抓取成交不匹配
     */
    @Field(type = FieldType.Integer)
    private Integer dealMatchFlag;

    /**
     * 室号
     */
    @Field(type = FieldType.Keyword)
    private String shiNum;

    /**
     * 单元号
     */
    @Field(type = FieldType.Keyword)
    private String unitNum;

}
