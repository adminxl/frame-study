package org.totoro.dao;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.totoro.entity.HouseInfo;
import org.totoro.entity.Item;

/**
 * @author daocr
 * @date 2019-08-11
 */
public interface HouseRepository extends ElasticsearchRepository<HouseInfo, Long> {
}
