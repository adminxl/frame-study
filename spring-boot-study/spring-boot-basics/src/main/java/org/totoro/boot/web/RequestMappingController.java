package org.totoro.boot.web;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.totoro.boot.bean.UserBean;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/7/21.
 */

@RestController
@RequestMapping("/mediaType")
public class RequestMappingController {


    /**
     * 根据headers 头信息，过滤请求
     * <p>
     * 在headers 中必须包含  zdykey=123
     * <p>
     * <p>
     * <p>
     * 如果没有包含则返回404
     * <p>
     * {
     * "timestamp": 1500622826498,
     * "status": 404,
     * "error": "Not Found",
     * "message": "No message available",
     * "path": "/mediaType/accept"
     * }
     * <p>
     * <p>
     * 请求数据：
     * <p>
     * GET /mediaType/headers HTTP/1.1
     * Host: 127.0.0.1:8080
     * zdykey: 123
     * Cache-Control: no-cache
     * Postman-Token: 8b247fda-08a5-946c-34db-6e06c16439f0
     *
     * @return
     */
    @RequestMapping(value = "/headers", headers = {"zdykey=123"})
    public UserBean headers(HttpServletRequest request) {

        String zdykey = request.getHeader("zdykey");

        System.out.println(zdykey);

        UserBean userBean = new UserBean(01, "headers");


        return userBean;
    }

    /**
     * 根据Content-Type 过滤请求，实际处理可以通过请求不同 Content-Type，去调用不一样的方法
     * <p>
     * 仅处理request Content-Type 为“application/json”类型的请求。
     * <p>
     * <p>
     * <p>
     * GET /mediaType/consumes HTTP/1.1
     * Host: 127.0.0.1:8080
     * Content-Type: application/json
     * Cache-Control: no-cache
     * Postman-Token: 1350806f-853b-2213-b6e8-501ee7e93d3c
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/consumes", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public UserBean consumes(HttpServletRequest request) {

        String name = "consumes";

        UserBean userBean = new UserBean(01, name);
        return userBean;
    }


    /**
     * 方法仅处理request请求中Accept头中包含了"application/json"的请求，同时暗示了返回的内容类型为application/json;
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/produces", produces = {MediaType.APPLICATION_XML_VALUE})
    public UserBean produces(HttpServletRequest request) {

        UserBean userBean = new UserBean(01, "consumes");


        return userBean;
    }


}
