package org.totoro.boot.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;

/**
 * Created by Administrator on 2017/6/8.
 *
 *
 *
 *
 */
@Component
public class InstantiationAwareBeanPostProcessorTest implements InstantiationAwareBeanPostProcessor {


    /**
     * config 实例化前调用
     * @param aClass
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInstantiation(Class<?> aClass, String s) throws BeansException {

        return null;
    }


    /**
     * config 实例化后调用
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public boolean postProcessAfterInstantiation(Object o, String s) throws BeansException {
        return true;
    }

    /**
     * 可以对象成员变量的值
     * @param propertyValues
     * @param propertyDescriptors
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public PropertyValues postProcessPropertyValues(PropertyValues propertyValues, PropertyDescriptor[] propertyDescriptors, Object o, String s) throws BeansException {


        /***
         *  通过 propertyDescriptors[] 去操作成员变量值
         */

        if(s.indexOf("userBean") !=-1){
            System.out.println("");
        }


        System.out.println("postProcessPropertyValues " +s);
        return propertyValues;
    }

    /**
     * 实例化中
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {

        System.out.println("postProcessBeforeInitialization " +s);

        return o;
    }

    /**
     * 实例化后
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {

        System.out.println("postProcessAfterInitialization " +s);

        return o;
    }
}
