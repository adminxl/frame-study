package org.totoro.boot.aop;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.stereotype.Component;

/**
 * @author daocr
 * @date 2019-08-15
 * 前置通知
 * @see org.springframework.aop.MethodBeforeAdvice
 * 后置通知
 * @see org.springframework.aop.AfterReturningAdvice
 * 环绕通知
 * @see MethodInterceptor
 * 异常通知
 * @see org.springframework.aop.ThrowsAdvice
 */
@Component
@Slf4j
public class TestMethodInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        log.info("执行 代理 方法 ,{  }", invocation.getMethod().getName());

        return invocation.proceed();
    }
}
