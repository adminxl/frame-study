

##### @ConditionalOnBean当容器里有指定的Bean的条件下 
#####  @ConditionalOnClass　　当类路径下有指定的class的条件下
##### 
#####  @ConditionalOnExpression基于SpEL表达式作为判断条件
##### 
#####  @ConditionalOnJava基于jvm版本作为判断条件
##### 
#####  @ConditionalOnJndi在JNDI存在的条件下查找指定的位置
##### 
#####  @ConditionalOnMissingBean当容器里没有指定Bean的情况下
##### 
#####  @ConditionalOnMissingClass当类路径下没有指定的类的情况下
##### 
#####  @ConditionalOnNotWebApplication当前项目不是web项目的情况下　
##### 
#####  @ConditionalOnProperty指定的属性是否有指定的值
##### 
#####  @ConditionalOnResource类路径下是否有指定的资源
##### 
#####  @ConditionalOnSingleCandidate当指定的Bean在容器中只有一个，或者虽然有多个但是指定首选的Bean
##### 
#####  @ConditionalOnWebApplication当前项目是web项目的条件下