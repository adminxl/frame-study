package org.totoro.boot.bean;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by Administrator on 2017/6/7.
 */
@Component("userBean")
public class UserBean {

    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserBean() {

        System.out.println("初始化 UserBean");

    }

    public UserBean(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @PostConstruct
    public void init(){

        System.out.println("初始化   .....");

    }

    @PreDestroy
    public void destory(){

        System.out.println("销毁方法   .....");


    }



}
