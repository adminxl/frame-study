package org.totoro.boot.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/6/8.
 */
@Component
public class BeanPostProcessorTest implements BeanPostProcessor {


    /**
     * 对象实例化中
     *
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {


//        try {
//            使用工具类设置 属性
//            Class cls = Class.forName("net.sg.spring.service.UpperAction");
//            Object obj = cls.newInstance();
//            BeanWrapper beanWrapper = new BeanWrapperImpl(obj);
//            beanWrapper.setPropertyValue("msg", "hello");
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }


        if (s.indexOf("Processor") != -1) {
            System.out.println("bean名称：" + s);
        }


        return o;
    }

    /**
     * 对象实例化完成
     *
     * @param o
     * @param s
     * @return
     * @throws BeansException
     */
    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        return o;
    }
}
