package org.totoro.boot.annotation.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Created by Administrator on 2017/6/7.
 *
 *启用某一个 属性文件
 *
 */
@EnableConfigurationProperties(ConfigurationPropertiesAnnotation.class)
public class EnableConfigurationPropertiesAnnotation {


    @Autowired
    private ConfigurationPropertiesAnnotation configurationPropertiesAnnotation;


}
