package org.totoro.boot.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/6/8.
 *
 *
 * 修改配置bean的元数据。
 *
 *
 */
@Component
public class BeanFactoryPostProcessorTest implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {


        /**
         * 如果找不到bean 会抛异常
         *
         * A component required a config named 'exampleBean' that could not be found.
         *
         */
        BeanDefinition bd = beanFactory.getBeanDefinition("requestMappingHandlerAdapter");
        MutablePropertyValues pv = bd.getPropertyValues();
        if(pv.contains("year"))
        {
            pv.addPropertyValue("ultimeateAnswer", "nothing ");
        }

    }
}
