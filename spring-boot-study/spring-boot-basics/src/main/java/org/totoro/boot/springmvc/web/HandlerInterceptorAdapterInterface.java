package org.totoro.boot.springmvc.web;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器
 * <p>
 * Created by Administrator on 2017/7/20.
 */
@Component
public class HandlerInterceptorAdapterInterface implements HandlerInterceptor {
    /**
     * true 表示执行  postHandle、afterCompletion
     * false 表示不执行 postHandle、afterCompletion
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    /**
     * 在调用 control方法前执行
     * <p>
     * 扩展点：
     * 1、可以在方法上扩展自定义注解
     * 2、
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * 在调用 control方法后执行
     * <p>
     * 扩展点：
     * 可以修改modeandview 对象中的数据
     *
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
