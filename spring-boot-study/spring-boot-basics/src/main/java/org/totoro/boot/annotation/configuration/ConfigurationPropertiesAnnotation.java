package org.totoro.boot.annotation.configuration;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/6/7.
 * 用于读取 properties 配置文件
 * <p>
 * <p>
 * prefix:string
 * 用于匹配Properties 文件内容的前缀
 * <p>
 * 例如：前缀为：article.redis
 * <p>
 * <p>
 * <p>
 *
 *
 * 必须要直接或者间接的有 @Component
 *
 * <p>
 * 如果使用PropertySource注解的话，默认是从 application.properties 或者application.yml文件中读取数据
 *
 *
 */
@Component
@ConfigurationProperties(prefix = "article.redis")
@PropertySource("classpath:org/totoro/boot/annotation/configuration/conf/config.properties")
public class ConfigurationPropertiesAnnotation {


    public ConfigurationPropertiesAnnotation() {
        System.out.println("初始化了....");
    }

    private String ip;

    private Integer port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        System.out.println("ip:" + ip);
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "ConfigurationPropertiesAnnotation{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
