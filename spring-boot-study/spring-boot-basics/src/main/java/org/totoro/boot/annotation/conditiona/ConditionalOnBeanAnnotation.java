package org.totoro.boot.annotation.conditiona;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2017/6/7.
 *
 *
 *
 *
 */
@Configuration
@ConditionalOnBean(name = "userBean")
public class ConditionalOnBeanAnnotation {

    public ConditionalOnBeanAnnotation() {
        System.out.println("依赖的bean 存在，执行初始化...");
    }
}
