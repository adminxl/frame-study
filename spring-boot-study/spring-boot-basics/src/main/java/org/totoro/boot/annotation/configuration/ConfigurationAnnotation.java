package org.totoro.boot.annotation.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * 类似于我们在xml中的beans 标签
 * <p>
 * 然后可以先定义很多bean
 * <p>
 * <p>
 * BeanAnnotation
 * name：配置bean的名字
 * initMethod：初始化方法
 * destroyMethod：销毁方法
 * <p>
 * <p>
 * ScopeAnnotation
 * 配置bean的生命周期（单例、或者多例）
 */
@Configuration
public class ConfigurationAnnotation {


    public ConfigurationAnnotation() {

        System.out.println("ConfigurationAnnotation 初始化成功！");

    }


    @Bean()
    @Scope()
    public ConfigurationAnnotation getConfigurationAnnotation() {

        System.out.println("构建了 ConfigurationAnnotation config");
        return new ConfigurationAnnotation();

    }


}
