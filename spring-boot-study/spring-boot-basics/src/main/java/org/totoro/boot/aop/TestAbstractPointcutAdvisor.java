package org.totoro.boot.aop;

import lombok.extern.slf4j.Slf4j;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;

/**
 * @author daocr
 * @date 2019-08-15
 */
@Component
@Slf4j
public class TestAbstractPointcutAdvisor extends AbstractPointcutAdvisor {


    @Autowired
    private TestMethodInterceptor testMethodInterceptor;

    public TestAbstractPointcutAdvisor() {
        log.info("初始化:TestAbstractPointcutAdvisor");
    }

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE;
    }

    private final StaticMethodMatcherPointcut pointcut = new StaticMethodMatcherPointcut() {

        @Override
        public boolean matches(Method method, Class<?> aClass) {
            return method.isAnnotationPresent(RequestMapping.class);
        }
    };

    @Override
    public Pointcut getPointcut() {

        log.info("获取切点");
        return pointcut;
    }

    @Override
    public Advice getAdvice() {
        log.info("获取 获取通知实现类");
        return testMethodInterceptor;
    }
}
