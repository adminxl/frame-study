package org.totoro.boot.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.totoro.boot.annotation.configuration.ConfigurationPropertiesAnnotation;

/**
 * @author yhl
 */

@RestController
@RequestMapping("/resource")
public class AnnotationController {

    @Autowired
    private ConfigurationPropertiesAnnotation configurationPropertiesAnnotation;

    @RequestMapping(value = "/config")
    public Object configurationPropertiesAnnotation() {
        System.out.println(" 调用/user/save成功！" + configurationPropertiesAnnotation);
        return "调用/test/configurationPropertiesAnnotation！";
    }


}
