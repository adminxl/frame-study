package org.totoro.config;

import org.springframework.context.annotation.Bean;
import org.totoro.amap.service.UserService;

/**
 * @author YHL
 * @version V1.0
 * @Description: 服务初始化
 * @date 2018-01-20
 */


public class ServiceConfig {

    public ServiceConfig() {
        System.out.println("        ServiceConfig init ");
    }

    @Bean
    public UserService getUserService() {
        System.out.println("        create UserService   ");
        return new UserService();
    }


}
