package org.totoro.annotation;

import org.springframework.context.annotation.Import;
import org.totoro.config.ServiceConfig;

import java.lang.annotation.*;

/**
 * @author YHL
 * @version V1.0
 * @Description: 扫描 服务
 * @date 2018-01-20
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Import(ServiceConfig.class)
public @interface ScanService {
}
