package org.totoro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.totoro.annotation.EnableTestStarter;

/**
 * 自定义  spring - boot - starter
 * <p>
 * 主要配合了 @Import 注解
 *
 */
@SpringBootApplication
@EnableTestStarter
public class App {
    public static void main(String[] args) throws InterruptedException {

        ConfigurableApplicationContext run = SpringApplication.run(App.class, args);

        Thread.sleep(Integer.MAX_VALUE);
    }
}
