package org.totoro;

import com.dangdang.ddframe.job.lite.lifecycle.api.JobSettingsAPI;
import com.dangdang.ddframe.job.lite.lifecycle.domain.JobBriefInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.totoro.context.ElasticJobConfig;
import org.totoro.context.console.service.JobAPIService;
import org.totoro.context.console.service.impl.JobAPIServiceImpl;
import org.totoro.context.task.CustomSimpleTask;

import java.util.Collection;

/**
 * 调度平台
 */
@SpringBootApplication
public class App {
    public static void main(String[] args) throws InterruptedException {
        ConfigurableApplicationContext run = SpringApplication.run(App.class, args);


        Thread.sleep(1000 * 10);

        System.out.println("开始添加任务");

        ElasticJobConfig bean = run
                .getBean(ElasticJobConfig.class);

        bean.addTask(new CustomSimpleTask(), "* 0/2 * * * ?");
        System.out.println("添加任务完成");


        JobAPIService jobAPIService = new JobAPIServiceImpl();

    }
}
