package org.totoro.context.task;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author YHL
 * @version V1.0
 * @Description: 数据流
 * @date 2018-04-04
 */
@Component
public class DataflowTask implements DataflowJob<String> {
    /**
     * 获取待处理数据.
     * <p>
     * fetchData 获取 数据为null 时，则不执行 processData
     *
     * @param shardingContext 分片上下文
     * @return 待处理的数据集合
     */
    @Override
    public List<String> fetchData(ShardingContext shardingContext) {

        int shardingTotalCount = shardingContext.getShardingTotalCount();
        
        System.out.println("并发总分片：" + shardingTotalCount);

        int shardingItem = shardingContext.getShardingItem();

        if (shardingItem == 4) {
            return null;
        }

        return Arrays.asList(shardingItem + "");


    }

    /**
     * 处理数据.
     *
     * @param shardingContext 分片上下文
     * @param data            待处理数据集合
     */
    @Override
    public void processData(ShardingContext shardingContext, List<String> data) {

        System.out.println("processData" + data);
    }
}
