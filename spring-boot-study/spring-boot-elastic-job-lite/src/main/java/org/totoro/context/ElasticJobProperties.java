package org.totoro.context;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author YHL
 * @version V1.0
 * @Description: elastic-job 配置类
 * @date 2018-03-28
 */
@ConfigurationProperties(prefix = "elastic-job")
@Component
public class ElasticJobProperties {
    /**
     * 平台名称
     */
    private String name;
    /**
     * zk 地址
     */
    private String zkAddr;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZkAddr() {
        return zkAddr;
    }

    public void setZkAddr(String zkAddr) {
        this.zkAddr = zkAddr;
    }
}
