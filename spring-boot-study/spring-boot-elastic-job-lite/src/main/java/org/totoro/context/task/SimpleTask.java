package org.totoro.context.task;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author YHL
 * @version V1.0
 * @Description: 简单定时任务
 * @date 2018-02-08
 */
@Component
public class SimpleTask implements SimpleJob {


    public Logger logger = LoggerFactory.getLogger(SimpleTask.class);


    /**
     * 执行作业.
     *
     * @param shardingContext 分片上下文
     */
    @Override
    public void execute(ShardingContext shardingContext) {

        System.out.println("getJobParameter :" + shardingContext.getJobParameter());

        System.out.println("getShardingParameter :" + shardingContext.getShardingParameter());

        System.out.println("execute  +" + shardingContext);
    }


}
