package org.totoro;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.locationtech.jts.io.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.totoro.amap.service.PoiCityInfoPolylineService;
import org.totoro.amap.service.PoiCityInfoService;
import org.totoro.amap.service.PoiService;
import org.totoro.meituan.service.MeiTuanService;

import java.io.IOException;

/**
 * 参考：https://my.oschina.net/ordiychen/blog/858715
 * https://halfrost.com/go_spatial_search/
 * https://www.jianshu.com/p/3dbaf73a09af
 *
 *
 *
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-08-07
 */
@SpringBootApplication
public class StartUpApplication {

    public static void main(String[] args) throws IOException, InterruptedException, ParseException {
        ConfigurableApplicationContext run = SpringApplication.run(StartUpApplication.class, args);

//        PoiCityInfoService countryService = run.getBean(PoiCityInfoService.class);
//        countryService.loadStreet();
//        countryService.loadPlate();
//
//        PoiCityInfoPolylineService countryPolylineService = run.getBean(PoiCityInfoPolylineService.class);
//        countryPolylineService.loadProvincePolyline();
//        countryPolylineService.loadCityPolyline();
//        countryPolylineService.loadDistrictPolyline();

//        PoiService poiService = run.getBean(PoiService.class);
//        poiService.searchAndSave("310115", "120302");
        MeiTuanService meiTuanService = run.getBean(MeiTuanService.class);


//        meiTuanService.saveCity();


    }
//
}
