package org.totoro.amap.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author yhl
 */
@NoArgsConstructor
@Data
public class DistrictsResponse {

    private String status;
    private String info;
    private String infocode;
    private String count;

    private SuggestionVO suggestion;
    private List<DistrictsVO> districts;

    @NoArgsConstructor
    @Data
    public static class SuggestionVO {
        private List<?> keywords;
        private List<?> cities;
    }

    @NoArgsConstructor
    @Data
    public static class DistrictsVO {

        private String adcode;
        private String polyline;
        private String name;
        private String center;
        private String level;
        private String citycode;
        private List<DistrictsVO> districts;
    }
}