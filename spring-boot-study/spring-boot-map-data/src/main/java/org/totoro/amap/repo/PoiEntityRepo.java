package org.totoro.amap.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.totoro.amap.repo.entity.PoiCityInfoEntity;
import org.totoro.amap.repo.entity.PoiEntityEntity;

/**
 * poi 数据(PoiEntity)实体类
 *
 * @author yhl
 */
public interface PoiEntityRepo extends JpaRepository<PoiEntityEntity, Long> {
}
