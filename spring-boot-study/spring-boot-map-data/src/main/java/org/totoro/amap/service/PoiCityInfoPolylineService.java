package org.totoro.amap.service;

import org.totoro.amap.service.bo.PoiCityInfoPolylineBo;

/**
 * 城市栅栏(CountryPolyline)实体类
 *
 * @author makejava
 * @since 2019-09-30 09:23:45
 */
public interface PoiCityInfoPolylineService {


    /**
     * 保存
     *
     * @param countryPolylineBo
     * @return
     */
    PoiCityInfoPolylineBo save(PoiCityInfoPolylineBo countryPolylineBo);


    /**
     * code 查询
     *
     * @param adcCode
     * @return
     */
    PoiCityInfoPolylineBo queryByAdcCode(String adcCode);

    /**
     * 加载省份经纬度
     */
    void loadProvincePolyline();


    /**
     * 加载城市数据
     */
    void loadCityPolyline();

    /**
     * 加载城市数据
     */
    void loadDistrictPolyline();

}