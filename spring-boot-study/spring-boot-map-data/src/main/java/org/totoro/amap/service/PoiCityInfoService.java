package org.totoro.amap.service;

import org.totoro.amap.enums.Level;
import org.totoro.amap.service.bo.PoiCityInfoBo;

import java.io.IOException;
import java.util.List;

/**
 * 国家城市服务
 *
 * @author yhl
 */
public interface PoiCityInfoService {

    /**
     * 保存信息
     *
     * @param countryBo
     * @return
     */
    PoiCityInfoBo save(PoiCityInfoBo countryBo);

    /**
     * 根据 类型 查询
     *
     * @param level
     * @return
     */
    List<PoiCityInfoBo> findCountryBoByLevel(Level level);

    /**
     * 查询不存在 围栏的城市
     *
     * @param level
     * @return
     */
    List<PoiCityInfoBo> findNotFindPolylineCountryBo(Level level);

    /**
     * 加载城市
     */
    void loadCity();

    /**
     * 加载镇
     */
    void loadStreet();

    /**
     * 加载商圈
     */
    void loadPlate() throws IOException, InterruptedException;

}
