package org.totoro.amap.repo.entity;

import javax.persistence.*;
import java.util.Date;
import java.io.Serializable;


/**
 * poi 数据(PoiEntity)实体类
 *
 * @author makejava
 * @since 2019-11-17 22:58:32
 */
@Entity
@Table(name = "poi_entity")
public class PoiEntityEntity implements Serializable {

    private static final long serialVersionUID = 393525456385470163L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 高德地图 实体id
     */
    @Column(name = "gaode_id")
    private String gaodeId;

    /**
     * 高德地图 父类id
     */
    @Column(name = "gaode_parent")
    private String gaodeParent;

    /**
     * 省份code
     */
    @Column(name = "province_code")
    private String provinceCode;

    /**
     * 城市code
     */
    @Column(name = "city_code")
    private String cityCode;

    /**
     * 区域code
     */
    @Column(name = "district_code")
    private String districtCode;

    /**
     * 省份名称
     */
    @Column(name = "province_name")
    private String provinceName;

    /**
     * 城市名称
     */
    @Column(name = "city_name")
    private String cityName;

    /**
     * 区域id
     */
    @Column(name = "district_name")
    private String districtName;

    /**
     * 商圈
     */
    @Column(name = "business_area")
    private String businessArea;

    /**
     * poi 名称
     */
    @Column(name = "poi_name")
    private String poiName;

    /**
     * 兴趣点类型
     * 顺序为大类、中类、小类
     * 例如：餐饮服务;中餐厅;特色/地方风味餐厅
     */
    @Column(name = "type_name")
    private String typeName;

    /**
     * 兴趣点类型编码
     */
    @Column(name = "type_code")
    private String typeCode;

    /**
     * 行业类型
     */
    @Column(name = "biz_type")
    private String bizType;

    /**
     * 地址
     */
    @Column(name = "address")
    private String address;

    /**
     * 经纬度
     */
    @Column(name = "location")
    private String location;

    /**
     * 联系电话
     */
    @Column(name = "tel")
    private String tel;

    /**
     * POI的入口经纬度
     */
    @Column(name = "entr_location")
    private String entrLocation;

    /**
     * 别名
     */
    @Column(name = "alias")
    private String alias;

    /**
     * 停车场类型
     * 仅在停车场类型POI的时候显示该字段
     * 展示停车场类型，包括：地下、地面、路边
     * extensions=all的时候显示
     */
    @Column(name = "parking_type")
    private String parkingType;

    /**
     * 该POI的特色内容
     * 主要出现在美食类POI中，代表特色菜
     * 例如“烤鱼,麻辣香锅,老干妈回锅肉”
     */
    @Column(name = "tag")
    private String tag;

    /**
     * 评分
     * 仅存在于餐饮、酒店、景点、影院类POI之下
     */
    @Column(name = "biz_rating")
    private String bizRating;

    /**
     * 消费
     */
    @Column(name = "biz_cost")
    private String bizCost;

    /**
     * 照片地址
     */
    @Column(name = "photos")
    private String photos;

    @Column(name = "update_timestamp")
    private Date updateTimestamp;

    @Column(name = "create_time")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGaodeId() {
        return gaodeId;
    }

    public void setGaodeId(String gaodeId) {
        this.gaodeId = gaodeId;
    }

    public String getGaodeParent() {
        return gaodeParent;
    }

    public void setGaodeParent(String gaodeParent) {
        this.gaodeParent = gaodeParent;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public String getPoiName() {
        return poiName;
    }

    public void setPoiName(String poiName) {
        this.poiName = poiName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEntrLocation() {
        return entrLocation;
    }

    public void setEntrLocation(String entrLocation) {
        this.entrLocation = entrLocation;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getParkingType() {
        return parkingType;
    }

    public void setParkingType(String parkingType) {
        this.parkingType = parkingType;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getBizRating() {
        return bizRating;
    }

    public void setBizRating(String bizRating) {
        this.bizRating = bizRating;
    }

    public String getBizCost() {
        return bizCost;
    }

    public void setBizCost(String bizCost) {
        this.bizCost = bizCost;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}