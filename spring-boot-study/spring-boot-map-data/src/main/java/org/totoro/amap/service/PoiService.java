package org.totoro.amap.service;

import org.locationtech.jts.io.ParseException;
import org.totoro.amap.response.PoiSearchResponse;
import org.totoro.amap.service.bo.PoiInfoBo;

/**
 * @author daocr
 * @date 2019/11/17
 */
public interface PoiService {


    /**
     * 多边形搜索
     *
     * @param adcCode
     * @param poiType
     * @throws ParseException
     */
    void searchAndSave(String adcCode, String poiType) throws ParseException;

    /**
     * 保存 poi 信息
     *
     * @param poiInfoBo
     * @return
     */
    PoiInfoBo save(PoiSearchResponse poiInfoBo);

}
