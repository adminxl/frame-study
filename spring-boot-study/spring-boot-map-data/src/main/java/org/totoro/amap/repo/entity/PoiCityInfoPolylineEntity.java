package org.totoro.amap.repo.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * 城市栅栏(CountryPolyline)实体类
 *
 * @author makejava
 * @since 2019-09-30 09:23:45
 */
@Entity
@Table(name = "poi_city_info_polyline")
@Data
public class PoiCityInfoPolylineEntity implements Serializable {

    private static final long serialVersionUID = -21487566770539668L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "adc_code")
    private String adcCode;

    /**
     * 多边形 围栏
     */
    @Column(name = "geo")
    private String geo;

    /**
     * 中点点经度
     */
    @Column(name = "center_longitude")
    private String centerLongitude;

    /**
     * 中心点纬度
     */
    @Column(name = "center_latitude")
    private String centerLatitude;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * @see org.totoro.amap.enums.Level
     */
    @Column(name = "level")
    private String level;


}