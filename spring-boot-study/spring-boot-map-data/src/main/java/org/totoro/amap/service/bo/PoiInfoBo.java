package org.totoro.amap.service.bo;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * poi 数据(PoiEntity)实体类
 *
 * @author makejava
 * @since 2019-11-17 10:26:07
 */
@Data
public class PoiInfoBo implements Serializable {

    private static final long serialVersionUID = 153220436801952545L;

    private Long id;

    /**
     * 高德地图 实体id
     */

    private String gaodeId;

    /**
     * 高德地图 父类id
     */

    private String gaodeParent;

    /**
     * 省份code
     */

    private String provinceCode;

    /**
     * 城市code
     */

    private String cityCode;

    /**
     * 区域code
     */

    private String districtCode;

    /**
     * 商圈
     */

    private String businessArea;


    private String poiName;

    /**
     * 兴趣点类型
     * 顺序为大类、中类、小类
     * 例如：餐饮服务;中餐厅;特色/地方风味餐厅
     */

    private String typeName;

    /**
     * 兴趣点类型编码
     */

    private Integer typeCode;

    /**
     * 行业类型
     */

    private String bizType;

    /**
     * 地址
     */

    private String address;

    /**
     * 经纬度
     */

    private String location;

    /**
     * 联系电话
     */

    private String tel;

    /**
     * POI的入口经纬度
     */

    private String entrLocation;

    /**
     * 别名
     */

    private String alias;

    /**
     * 停车场类型
     * 仅在停车场类型POI的时候显示该字段
     * 展示停车场类型，包括：地下、地面、路边
     * extensions=all的时候显示
     */

    private String parkingType;

    /**
     * 该POI的特色内容
     * 主要出现在美食类POI中，代表特色菜
     * 例如“烤鱼,麻辣香锅,老干妈回锅肉”
     */
    @Column(name = "tag")
    private String tag;

    /**
     * 评分
     * 仅存在于餐饮、酒店、景点、影院类POI之下
     */

    private String bizRating;

    /**
     * 消费
     */

    private String bizCost;

    /**
     * 照片地址
     */

    private String photos;


    private Date updateTimestamp;


    private Date createTime;


}