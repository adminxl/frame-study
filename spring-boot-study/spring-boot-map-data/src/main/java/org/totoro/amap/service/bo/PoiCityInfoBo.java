package org.totoro.amap.service.bo;

import lombok.Data;

import java.util.Date;

/**
 * 国家城市信息
 *
 * @author yhl
 */
@Data
public class PoiCityInfoBo {

    private Long id;

    private String adcCode;

    private String parentAdcCode;

    private String cityCode;

    /**
     * 名称
     */
    private String adcName;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 级别
     * country：国家
     * province：省
     * city：城市
     * district：区
     */
    private String level;

    private Date createTime;

    private Date updateTime;
}
