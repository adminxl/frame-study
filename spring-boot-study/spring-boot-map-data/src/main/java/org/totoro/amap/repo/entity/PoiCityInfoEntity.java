package org.totoro.amap.repo.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * (CountryTable)实体类
 *
 * @author makejava
 * @since 2019-09-29 13:24:47
 */
@Entity
@Table(name = "poi_city_info")
@Data
public class PoiCityInfoEntity implements Serializable {

    private static final long serialVersionUID = 963966811033846164L;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "adc_code")
    private String adcCode;

    @Column(name = "city_code")
    private String cityCode;


    @Column(name = "parent_adc_code")
    private String parentAdcCode;

    /**
     * 名称
     */
    @Column(name = "adc_name")
    private String adcName;

    /**
     * 经度
     */
    @Column(name = "longitude")
    private String longitude;

    /**
     * 纬度
     */
    @Column(name = "latitude")
    private String latitude;

    /**
     * 级别
     * country：国家
     * province：省
     * city：城市
     * district：区
     * street:街道
     *
     * @see org.totoro.amap.enums.Level
     */
    @Column(name = "level")
    private String level;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;
}