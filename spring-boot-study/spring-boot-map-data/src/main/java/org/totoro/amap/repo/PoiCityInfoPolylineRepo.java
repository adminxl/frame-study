package org.totoro.amap.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.totoro.amap.repo.entity.PoiCityInfoPolylineEntity;

/**
 * 城市栅栏(CountryPolyline)实体类
 *
 * @author makejava
 * @since 2019-09-30 09:23:45
 */
public interface PoiCityInfoPolylineRepo extends JpaRepository<PoiCityInfoPolylineEntity, Long> {


}