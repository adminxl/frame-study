package org.totoro.amap.config;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 高德 key
 *
 * @author yhl
 */
public abstract class Key {

    private static AtomicLong loadbalancing = new AtomicLong(1);


    /**
     * 高德 key
     */

    private static List<String> keys = Arrays.asList(
            "7fe3f170aa6463157dd635cc2dc69bc5",
            "5b185b74c7a8a9ad91f0bb2dea364bfd",
            "d3f5f65691eeae4e5f1ce3ae7e95ff24",
            "481cc8edf9545114d86197832564149a",
            "066757b95f9c16ee33a638a85bc9875a",
            "01beb0845960baadc3671230159878cd",
            "89388a8de1c11aff4179f07a8b4f9ab1",
            "cf47e152f6802c2f4f234a6dee429866",
            "7875d651bb937ff43e117255a0f81071",
            "dbaed71e7b588836aea02a355a01dbec");

    public static String loadbalancing() {

        Long index = loadbalancing.get() % keys.size();

        loadbalancing.getAndAdd(1);

        return keys.get(index.intValue());
    }

}
