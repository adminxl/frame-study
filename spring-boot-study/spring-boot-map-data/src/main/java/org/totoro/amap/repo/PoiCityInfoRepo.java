package org.totoro.amap.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.totoro.amap.repo.entity.PoiCityInfoEntity;

/**
 * 国家城市信息
 *
 * @author yhl
 */
public interface PoiCityInfoRepo extends JpaRepository<PoiCityInfoEntity, Long> {
}
