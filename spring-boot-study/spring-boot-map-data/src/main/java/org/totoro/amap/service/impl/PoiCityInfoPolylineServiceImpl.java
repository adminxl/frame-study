package org.totoro.amap.service.impl;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.totoro.amap.config.Key;
import org.totoro.amap.enums.Level;
import org.totoro.amap.repo.PoiCityInfoPolylineRepo;
import org.totoro.amap.repo.entity.PoiCityInfoPolylineEntity;
import org.totoro.amap.repo.entity.QPoiCityInfoPolylineEntity;
import org.totoro.amap.response.DistrictsResponse;
import org.totoro.amap.service.PoiCityInfoPolylineService;
import org.totoro.amap.service.PoiCityInfoService;
import org.totoro.amap.service.bo.PoiCityInfoBo;
import org.totoro.amap.service.bo.PoiCityInfoPolylineBo;

import java.util.Date;
import java.util.List;

/**
 * 城市栅栏服务
 *
 * @author yhl
 */
@Service
public class PoiCityInfoPolylineServiceImpl implements PoiCityInfoPolylineService {


    @Autowired
    private PoiCityInfoPolylineRepo countryPolylineRepo;

    @Autowired
    private PoiCityInfoService countryService;

    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Override
    public PoiCityInfoPolylineBo save(PoiCityInfoPolylineBo countryPolylineBo) {

        PoiCityInfoPolylineEntity countryPolylineEntity = getCountryPolylineEntity(countryPolylineBo);

        PoiCityInfoPolylineEntity save = countryPolylineRepo.save(countryPolylineEntity);

        PoiCityInfoPolylineBo result = getCountryPolylineBo(save);

        return result;
    }

    @Override
    public PoiCityInfoPolylineBo queryByAdcCode(String adcCode) {

        QPoiCityInfoPolylineEntity poiCityInfoPolylineEntity = QPoiCityInfoPolylineEntity.poiCityInfoPolylineEntity;

        PoiCityInfoPolylineEntity polylineEntity = jpaQueryFactory.selectFrom(poiCityInfoPolylineEntity)
                .where(poiCityInfoPolylineEntity.adcCode.eq(adcCode)).fetchOne();

         return getCountryPolylineBo(polylineEntity);
    }

    @NotNull
    private PoiCityInfoPolylineEntity getCountryPolylineEntity(PoiCityInfoPolylineBo countryPolylineBo) {
        PoiCityInfoPolylineEntity countryPolylineEntity = new PoiCityInfoPolylineEntity();

        BeanUtils.copyProperties(countryPolylineBo, countryPolylineEntity);
        return countryPolylineEntity;
    }

    @NotNull
    private PoiCityInfoPolylineBo getCountryPolylineBo(PoiCityInfoPolylineEntity save) {
        PoiCityInfoPolylineBo countryPolylineBo1 = new PoiCityInfoPolylineBo();
        BeanUtils.copyProperties(save, countryPolylineBo1);
        return countryPolylineBo1;
    }

    @Override
    public void loadProvincePolyline() {

        List<PoiCityInfoBo> countryBoByLevel = countryService.findCountryBoByLevel(Level.province);


        getGeo(countryBoByLevel);
    }


    @Override
    public void loadCityPolyline() {
        List<PoiCityInfoBo> countryBoByLevel = countryService.findCountryBoByLevel(Level.city);

        getGeo(countryBoByLevel);
    }

    @Override
    public void loadDistrictPolyline() {
        List<PoiCityInfoBo> countryBoByLevel = countryService.findNotFindPolylineCountryBo(Level.district);

        getGeo(countryBoByLevel);
    }


    private void getGeo(List<PoiCityInfoBo> countryBoByLevel) {
        for (PoiCityInfoBo countryBo : countryBoByLevel) {

            String url = "restapi.amap.com/v3/config/district?key=" + Key.loadbalancing() + "&keywords=" + countryBo.getAdcCode() + "&subdistrict=0&extensions=all";

            HttpResponse execute = HttpUtil.createGet(url).execute();

            DistrictsResponse districtsResponse = JSONUtil.toBean(execute.body(), DistrictsResponse.class);

            if (!ObjectUtils.isEmpty(districtsResponse.getDistricts())) {


                for (DistrictsResponse.DistrictsVO district : districtsResponse.getDistricts()) {

                    if (district.getAdcode().equals(countryBo.getAdcCode())) {
                        System.out.println(countryBo.getAdcName());
                        PoiCityInfoPolylineBo countryPolylineBo = new PoiCityInfoPolylineBo();
                        countryPolylineBo.setAdcCode(countryBo.getAdcCode());
                        countryPolylineBo.setGeo(district.getPolyline());
                        countryPolylineBo.setLevel(countryBo.getLevel());
                        String[] split = district.getCenter().split(",");
                        countryPolylineBo.setCenterLongitude(split[0]);
                        countryPolylineBo.setCenterLatitude(split[1]);
                        countryPolylineBo.setCreateTime(new Date());
                        countryPolylineBo.setUpdateTime(new Date());

                        try {
                            this.save(countryPolylineBo);
                        } catch (Exception e) {

                        }
                    }

                }
            }
        }
    }
}
