package org.totoro.amap.service.bo;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * 城市栅栏(CountryPolyline)实体类
 *
 * @author makejava
 * @since 2019-09-30 09:23:45
 */
@Data
public class PoiCityInfoPolylineBo implements Serializable {

    private static final long serialVersionUID = -21487566770539668L;

    private Integer id;

    private String adcCode;

    /**
     * 多边形 围栏
     */
    private String geo;

    /**
     * 中点点经度
     */
    private String centerLongitude;

    /**
     * 中心点纬度
     */
    private String centerLatitude;

    private Date createTime;

    private Date updateTime;

    private String level;


}