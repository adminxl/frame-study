package org.totoro.amap.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author daocr
 * @date 2019/11/17
 */
@NoArgsConstructor
@Data
public class PoiSearchResponse {

    private Integer status;
    private Integer count;
    private String infocode;
    private List<PoisVO> pois;

    @NoArgsConstructor
    @Data
    public static class PoisVO {
        private String id;
        private String name;
        private String type;
        private String typecode;
        private String address;
        private String location;
        private String pcode;
        private String pname;
        private String citycode;
        private String cityname;
        private String adcode;
        private String adname;
        private String shopinfo;
        private String gridcode;
        private String entr_location;
        private String business_area;
        private String match;
        private String recommend;
        private String timestamp;
        private String indoor_map;
        private BizExtVO biz_ext;
        private List<?> parent;
        private List<?> childtype;
        private List<?> tag;
        private List<?> biz_type;
        private List<?> tel;
        private List<?> postcode;
        private List<?> website;
        private List<?> email;
        private List<?> importance;
        private List<?> shopid;
        private List<?> poiweight;
        private List<?> distance;
        private List<?> navi_poiid;
        private List<?> exit_location;
        private List<?> alias;
        private List<?> event;
        private List<?> children;
        private List<?> photos;
        private String parking_type;

        @NoArgsConstructor
        @Data
        public static class BizExtVO {
            private String rating;
            private String cost;
        }
    }
}
