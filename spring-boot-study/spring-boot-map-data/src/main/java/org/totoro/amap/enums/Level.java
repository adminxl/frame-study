package org.totoro.amap.enums;

/**
 * 级别
 *
 * @author yhl
 */
public enum Level {

    /**
     * 国家
     */
    country,
    /**
     * 省份
     */
    province,
    /**
     * 城市
     */
    city,
    /**
     * 区域
     */
    district,
    /**
     * 街道 镇
     */
    street,
    /**
     * 商圈
     */
    plate

}
