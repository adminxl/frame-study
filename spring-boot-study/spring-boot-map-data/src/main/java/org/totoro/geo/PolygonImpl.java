package org.totoro.geo;

import com.github.davidmoten.rtree.geometry.Geometry;
import com.github.davidmoten.rtree.geometry.Rectangle;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import java.util.Arrays;
import java.util.List;

/**
 * 多边形
 *
 * @author yhl
 */
public class PolygonImpl implements Rectangle {

    private GeometryFactory geometryFactory = new GeometryFactory();

    private Polygon polygon = null;

    public PolygonImpl(List<Coordinate> point) {
        polygon = geometryFactory.createPolygon(point.toArray(new Coordinate[1]));
    }

    @Override
    public float x1() {
        return 0;
    }

    @Override
    public float y1() {
        return 0;
    }

    @Override
    public float x2() {
        return 0;
    }

    @Override
    public float y2() {
        return 0;
    }

    @Override
    public float area() {
        return new Double(polygon.getArea()).floatValue();
    }

    @Override
    public Rectangle add(Rectangle r) {
        throw new RuntimeException("不支持添加");

    }

    @Override
    public boolean contains(double x, double y) {
        Point point = geometryFactory.createPoint(new Coordinate(x, y));
        return polygon.contains(point);
    }

    /**
     * 获取 相交面积
     *
     * @param r
     * @return
     */
    @Override
    public float intersectionArea(Rectangle r) {

        // 多边形
        if (r instanceof PolygonImpl) {
            PolygonImpl polygon = (PolygonImpl) r;
            double area = this.polygon.intersection(polygon.polygon).getArea();
            return new Double(area).floatValue();
        }

        // 点
        if (r instanceof Point) {
            Point point = (Point) r;
            Point point1 = geometryFactory.createPoint(new Coordinate(point.getX(), point.getY()));
            double area = this.polygon.intersection(point1).getArea();
            return new Double(area).floatValue();
        }

        // 矩形
        List<Coordinate> point = Arrays.asList(new Coordinate(r.x1(), y1()), new Coordinate(y1(), x2()), new Coordinate(y2(), x2()), new Coordinate(y2(), x1()));
        Polygon polygon = geometryFactory.createPolygon(point.toArray(new Coordinate[1]));
        return new Double(this.polygon.intersection(polygon).getArea()).floatValue();
    }

    /**
     * 周长
     *
     * @return
     */
    @Override
    public float perimeter() {
        return new Double(polygon.getLength()).floatValue();
    }

    @Override
    public double distance(Rectangle r) {

        // 多边形
        if (r instanceof PolygonImpl) {
            PolygonImpl polygon = (PolygonImpl) r;
            double distance = this.polygon.distance(polygon.polygon);
            return new Double(distance).floatValue();
        }

        // 点
        if (r instanceof Point) {
            Point point = (Point) r;
            Point point1 = geometryFactory.createPoint(new Coordinate(point.getX(), point.getY()));
            double distance = this.polygon.distance(point1);
            return new Double(distance).floatValue();
        }

        // 矩形
        List<Coordinate> point = Arrays.asList(new Coordinate(r.x1(), y1()), new Coordinate(y1(), x2()), new Coordinate(y2(), x2()), new Coordinate(y2(), x1()));
        Polygon polygon = geometryFactory.createPolygon(point.toArray(new Coordinate[1]));
        return new Double(this.polygon.distance(polygon)).floatValue();
    }

    @Override
    public Rectangle mbr() {
        return this;
    }

    @Override
    public boolean intersects(Rectangle r) {

        // 多边形
        if (r instanceof PolygonImpl) {
            PolygonImpl polygon = (PolygonImpl) r;

            return this.polygon.intersects(polygon.polygon);
        }

        // 点
        if (r instanceof Point) {
            Point point = (Point) r;
            Point point1 = geometryFactory.createPoint(new Coordinate(point.getX(), point.getY()));
            return this.polygon.intersects(point1);

        }

        // 矩形
        List<Coordinate> point = Arrays.asList(new Coordinate(r.x1(), y1()), new Coordinate(y1(), x2()), new Coordinate(y2(), x2()), new Coordinate(y2(), x1()));
        Polygon polygon = geometryFactory.createPolygon(point.toArray(new Coordinate[1]));
        return this.polygon.intersects(polygon);
    }

    @Override
    public Geometry geometry() {
        return this;
    }
}
