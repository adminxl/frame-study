package org.totoro.geo;


import com.github.davidmoten.rtree.geometry.Circle;
import com.github.davidmoten.rtree.geometry.Line;
import com.github.davidmoten.rtree.geometry.Point;
import com.github.davidmoten.rtree.geometry.Rectangle;
import com.vividsolutions.jts.geom.Coordinate;

import java.util.List;

/**
 * @author yhl
 */
public final class Geometries {


    public static Point point(double x, double y) {
        return com.github.davidmoten.rtree.geometry.Geometries.point(x, y);
    }

    public static Point point(float x, float y) {
        return com.github.davidmoten.rtree.geometry.Geometries.point(x, y);
    }

    public static Rectangle rectangle(double x1, double y1, double x2, double y2) {
        return com.github.davidmoten.rtree.geometry.Geometries.rectangle(x1, y1, x2, y2);
    }

    public static Rectangle rectangle(float x1, float y1, float x2, float y2) {
        return com.github.davidmoten.rtree.geometry.Geometries.rectangle(x1, y1, x2, y2);
    }

    public static Circle circle(double x, double y, double radius) {
        return com.github.davidmoten.rtree.geometry.Geometries.circle(x, y, radius);
    }

    public static Circle circle(float x, float y, float radius) {
        return com.github.davidmoten.rtree.geometry.Geometries.circle(x, y, radius);
    }

    public static Line line(double x1, double y1, double x2, double y2) {
        return com.github.davidmoten.rtree.geometry.Geometries.line(x1, y1, x2, y2);
    }

    public static Line line(float x1, float y1, float x2, float y2) {
        return com.github.davidmoten.rtree.geometry.Geometries.line(x1, y1, x2, y2);
    }

    /**
     * 创建 地理多边形
     *
     * @param lon1
     * @param lat1
     * @param lon2
     * @param lat2
     * @return
     */
    public static Rectangle rectangleGeographic(double lon1, double lat1, double lon2,
                                                double lat2) {
        return com.github.davidmoten.rtree.geometry.Geometries.rectangleGeographic(lon1, lat1, lon2, lat2);
    }

    /**
     * 创建点
     *
     * @param lon
     * @param lat
     * @return
     */
    public static Point pointGeographic(double lon, double lat) {
        return com.github.davidmoten.rtree.geometry.Geometries.point(lon, lat);
    }

    /**
     * 创建多边形
     *
     * @param point
     * @return
     */
    public static Rectangle Polygon(List<Coordinate> point) {
        return new PolygonImpl(point);
    }


}
