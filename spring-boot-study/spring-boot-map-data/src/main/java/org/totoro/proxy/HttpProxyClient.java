package org.totoro.proxy;

import okhttp3.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author daocr
 * @date 2019/12/27
 */
public class HttpProxyClient {

    // 代理服务器
    final static String proxyHost = "http-dyn.abuyun.com";
    final static Integer proxyPort = 9020;

    // 代理隧道验证信息
    final static String proxyUser = "H17850SH09GH2G9D";
    final static String proxyPass = "74E579E17BEF8177";

    public static OkHttpClient createProxyOkHttp() {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));

        Authenticator proxyAuthenticator = new Authenticator() {
            @Override
            public Request authenticate(Route route, Response response) {
                String credential = Credentials.basic(proxyUser, proxyPass);
                return response.request().newBuilder()
                        .header("Proxy-Authorization", credential)
                        .build();
            }
        };

        OkHttpClient build = new OkHttpClient().newBuilder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .proxy(proxy)
                .proxyAuthenticator(proxyAuthenticator)
                .connectionPool(new ConnectionPool(5, 1, TimeUnit.SECONDS))
                .build();

        return build;
    }

    /**
     * 获取 无代理  OkHttpClient
     *
     * @return
     */
    public static OkHttpClient createOkHttpClient() {
        Map<String, String> map = new HashMap<>();
        //设置对所有https证书信任
        TrustAllManager trustAllManager = new TrustAllManager();
        OkHttpClient.Builder okHttpClientaBuilder = new OkHttpClient.Builder().sslSocketFactory(createTrustAllSSLFactory(trustAllManager), trustAllManager);

        //获得client
        OkHttpClient okHttpClient = okHttpClientaBuilder.build();

        return okHttpClient;
    }


    protected static SSLSocketFactory createTrustAllSSLFactory(TrustAllManager trustAllManager) {
        SSLSocketFactory ssfFactory = null;
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{trustAllManager}, new SecureRandom());
            ssfFactory = sc.getSocketFactory();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return ssfFactory;
    }


    public static class TrustAllManager implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

}
