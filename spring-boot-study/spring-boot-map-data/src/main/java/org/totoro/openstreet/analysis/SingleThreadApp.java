package org.totoro.openstreet.analysis;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.totoro.openstreet.analysis.AnalysisNode;
import org.totoro.openstreet.bo.SubArea;

import java.io.IOException;

/**
 *
 *  文件下载地址：http://download.geofabrik.de/
 */
public class SingleThreadApp {


    public static void main(String[] args) throws IOException {

        Document document = Jsoup.connect("https://www.openstreetmap.org/api/0.6/relation/371650").get();

        AnalysisNode app = new AnalysisNode();

        SubArea way = app.getWay(document);

    }


}
