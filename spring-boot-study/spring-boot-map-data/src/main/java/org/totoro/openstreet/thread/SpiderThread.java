package org.totoro.openstreet.thread;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * @author daocr
 * @date 2019/9/27
 */
public class SpiderThread implements Runnable {

    private Element parentElement;

    private Element currentElement;


    private ExecutorService subAreaPool;


    public SpiderThread(Element parentElement, Element currentElement, ExecutorService subAreaPool) {
        this.parentElement = parentElement;
        this.currentElement = currentElement;
        this.subAreaPool = subAreaPool;
    }

    @Override
    public void run() {

        ConcurrentAnalysisNode concurrentAnalysisNode = new ConcurrentAnalysisNode(subAreaPool);

        try {
            String ref = currentElement.attr("ref");
            Document document = Jsoup.connect("https://www.openstreetmap.org/api/0.6/relation/" + ref).get();
            concurrentAnalysisNode.getWay(document);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
