package org.totoro.openstreet.bo;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * 级别
 *
 * @author daocr
 */
public enum Level {


    COUNTRY(1, "国家"), COUNTRY_2(2, "国家"), PROVINCE(4, "省份"), CITY(5, "市"), COUNTY(6, "县"), AREA(7, "区");

    Level(Integer code, String name) {
        this.code = code;
        this.name = name;
    }


    private Integer code;

    private String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static Level of(Integer code) {

        Optional<Level> first = Stream.of(values()).filter(e -> e.code.equals(code)).findFirst();

        return first.get();
    }


}
