package org.totoro.openstreet.thread;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.totoro.openstreet.analysis.AnalysisNode;
import org.totoro.openstreet.bo.SubArea;
import org.totoro.openstreet.thread.ConcurrentAnalysisNode;

import java.io.IOException;
import java.util.concurrent.ExecutorService;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * Hello world!
 */
public class ConcurrentThreadApp {

    public static void main(String[] args) throws IOException {

        ExecutorService subAreaPool = newFixedThreadPool(5);

        Document document = Jsoup.connect("https://www.openstreetmap.org/api/0.6/relation/3149752").get();

        ConcurrentAnalysisNode app = new ConcurrentAnalysisNode(subAreaPool);

        app.getWay(document);

    }


}
