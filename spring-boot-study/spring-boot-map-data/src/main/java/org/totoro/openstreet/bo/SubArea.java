package org.totoro.openstreet.bo;

import org.totoro.openstreet.bo.*;

import cn.hutool.json.JSONUtil;
import lombok.Data;

import java.util.List;

/**
 * 内部区域
 *
 * @author daocr
 */
@Data
public class SubArea {


    /**
     * 上级区域
     */
    private SubArea parentArea;

    /**
     * 级别
     */
    private Level level;

    /**
     * 名称
     */
    private String name;

    /**
     * 中心区域
     */
    private AdminCentreNode adminCentre;

    /**
     * 轮廓线
     */
    private List<OuterLine> outerLines;

    /**
     * 外部区域，区域争议地带
     */
    private List<OuterLine> excludeOutlines;

    /**
     * 下级 区域
     */
    private List<SubArea> subAreas;


    @Override
    public String toString() {
        return JSONUtil.toJsonStr(this);
    }
}
