package org.totoro.openstreet.bo;

import cn.hutool.json.JSONUtil;
import lombok.Data;

/**
 * 节点信息
 *
 * @author daocr
 */
@Data
public class PointNode {

    /**
     * node id
     */
    protected String id;
    /**
     * 最后更新时间
     */
    protected String timestamp;
    /**
     * 纬度
     */
    protected String lat;
    /**
     * 经度
     */
    protected String lng;

    protected Integer sort;

    public PointNode(String id, String timestamp, String lat, String lng) {
        this.id = id;
        this.timestamp = timestamp;
        this.lat = lat;
        this.lng = lng;
    }

    public PointNode(String id, String timestamp, String lat, String lng, Integer sort) {
        this.id = id;
        this.timestamp = timestamp;
        this.lat = lat;
        this.lng = lng;
        this.sort = sort;
    }

    public PointNode() {

    }

    @Override
    public String toString() {
        return JSONUtil.toJsonStr(this);
    }
}
