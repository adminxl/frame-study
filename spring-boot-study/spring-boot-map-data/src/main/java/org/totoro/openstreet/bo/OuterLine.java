package org.totoro.openstreet.bo;

import cn.hutool.json.JSONUtil;
import lombok.Data;

import java.util.List;

/**
 * @author daocr
 */
@Data
public class OuterLine {


    /**
     * 轮廓线名称
     */
    private String name;
    /**
     * 轮廓线 id
     */
    private String id;

    /**
     * 最后更新时间
     */
    private String timestamp;

    /**
     * 轮廓线 构成 点
     */
    private List<PointNode> pointNodes;


    @Override
    public String toString() {
        return JSONUtil.toJsonStr(this);
    }
}
