package org.totoro.openstreet.bo;

import cn.hutool.json.JSONUtil;
import lombok.Data;

import java.util.List;

/**
 * 行政中心节点
 *
 * @author daocr
 */
@Data
public class AdminCentreNode extends PointNode {


    /**
     * 邮政编码
     */
    private String postalCode;


    /**
     * 名称
     */
    private String name;
    /**
     * 老名称
     */
    private List<String> oldName;


    @Override
    public String toString() {

        return JSONUtil.toJsonStr(this);
    }
}
