package org.totoro.meituan.repo.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * poi 信息(MeituanPoiInfo)实体类
 *
 * @author makejava
 * @since 2019-12-27 23:47:50
 */
@Data
@Entity
@Table(name = "meituan_poi_info")
public class MeituanPoiInfoEntity implements Serializable {

    private static final long serialVersionUID = 104536571745902542L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * poi id
     */
    @Column(name = "poi_id")
    private String poiId;

    /**
     * 平均价
     */
    @Column(name = "avg_price")
    private Integer avgPrice;

    /**
     * 评价评价
     */
    @Column(name = "avg_score")
    private Double avgScore;

    /**
     * poi 名称
     */
    @Column(name = "poi_name")
    private String poiName;

    /**
     * 区域名称
     */
    @Column(name = "area_name")
    private String areaName;

    /**
     * 城市
     */
    @Column(name = "city_code")
    private String cityCode;

    /**
     * 区域
     */
    @Column(name = "district_code")
    private String districtCode;

    /**
     * 板块 （商圈）
     */
    @Column(name = "plate_code")
    private String plateCode;

    /**
     * 地址
     */
    @Column(name = "address")
    private String address;

    /**
     * 联系电话
     */
    @Column(name = "phone")
    private String phone;

    /**
     * 开店时间
     */
    @Column(name = "begin_time")
    private Date beginTime;

    /**
     * 营业时间
     */
    @Column(name = "open_time")
    private String openTime;

    @Column(name = "lng")
    private Double lng;

    @Column(name = "lat")
    private Double lat;

    /**
     * 品牌id
     */
    @Column(name = "brand_id")
    private Long brandId;

    /**
     * 品牌名称
     */
    @Column(name = "brand_name")
    private String brandName;

    /**
     * 第一分类
     */
    @Column(name = "firstCate")
    private String firstcate;

    /**
     * 第二分类
     */
    @Column(name = "sub_cate")
    private String subCate;

    /**
     * 第三分类
     */
    @Column(name = "thrid_cate")
    private String thridCate;

    /**
     * 评论数量
     */
    @Column(name = "comment_cnt")
    private Integer commentCnt;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;


}