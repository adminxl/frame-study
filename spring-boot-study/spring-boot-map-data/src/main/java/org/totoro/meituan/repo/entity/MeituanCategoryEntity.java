package org.totoro.meituan.repo.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * 美团 分类(MeituanCategory)实体类
 *
 * @author makejava
 * @since 2019-12-27 23:46:18
 */
@Data
@Entity
@Table(name = "meituan_category")
public class MeituanCategoryEntity implements Serializable {

    private static final long serialVersionUID = -77803318659313217L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city_code")
    private String cityCode;

    /**
     * 区域 code
     */
    @Column(name = "district_code")
    private String districtCode;

    /**
     * 板块（商圈） code
     */
    @Column(name = "plate_code")
    private String plateCode;

    /**
     * 分类 id
     */
    @Column(name = "category_id")
    private Long categoryId;

    /**
     * 分类名称
     */
    @Column(name = "category_name")
    private Integer categoryName;

    /**
     * 数量
     */
    @Column(name = "cut")
    private Integer cut;

    /**
     * 分析时间
     */
    @Column(name = "analysis_time")
    private Date analysisTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;


}