package org.totoro.meituan;

import cn.hutool.json.JSONUtil;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;
import org.totoro.meituan.response.AreaInfo;
import org.totoro.meituan.response.CityInfo;
import org.totoro.meituan.response.PoiInfoList;
import org.totoro.meituan.response.ShopInfo;
import org.totoro.proxy.HttpProxyClient;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author daocr
 * @date 2019/12/29
 */
@Service
public class SpiderMeituan {


    /**
     * 获取城市信息
     *
     * @return
     * @throws IOException
     */
    public CityInfo getCityInfo() throws IOException {

        OkHttpClient client = HttpProxyClient.createOkHttpClient();

        Request request = new Request.Builder()
                .url("https://apimobile.meituan.com/group/v1/city/mergeCityList?prod=true&ci=1&uuid=ffff&historyCityIds=")
                .get()
                .addHeader("host", "apimobile.meituan.com")
                .addHeader("content-type", "application/json")
                .addHeader("accept", "*/*")
                //    .addHeader("user-agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 MicroMessenger/7.0.8(0x17000820) NetType/WIFI Language/zh_CN")
                .addHeader("referer", "https://servicewechat.com/wxde8ac0a21135c07d/312/page-frame.html")
                .addHeader("accept-language", "zh-cn")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();

        return JSONUtil.toBean(response.body().string(), CityInfo.class);

    }

    /**
     * 获取板块和商圈信息
     *
     * @param cityId
     * @return
     * @throws IOException
     */

    public AreaInfo getAreaInfo(Integer cityId) throws IOException {

        OkHttpClient client = HttpProxyClient.createOkHttpClient();

        Request request = new Request.Builder()
                .url("https://apimeishi.meituan.com/meishi/filter/groupapi/mt/geolist?cityId=" + cityId)
                .get()
                .addHeader("host", "apimeishi.meituan.com")
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("accept", "*/*")
                //   .addHeader("user-agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 MicroMessenger/7.0.8(0x17000820) NetType/WIFI Language/zh_CN")
                .addHeader("referer", "https://servicewechat.com/wxde8ac0a21135c07d/312/page-frame.html")
                .addHeader("accept-language", "zh-cn")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();

        return JSONUtil.toBean(response.body().string(), AreaInfo.class);

    }


    /**
     * 获取 poi 信息
     *
     * @param cityId
     * @param areaId
     * @param categoryId
     * @param offset
     * @param limit
     * @return
     * @throws IOException
     */
    public PoiInfoList getPois(Integer cityId, Integer areaId, Integer categoryId, Integer offset, Integer limit) throws IOException {

        OkHttpClient client = HttpProxyClient.createOkHttpClient();

        String url = "https://apimeishi.meituan.com/meishi/filter/v6/deal/select?cateId={cateId}&areaId={areaId}&sort=defaults&limit={limit}&offset={offset}&lineId=&stationId=&distance=&uuid={uuid}&cityId={cityId}";

        ThreadLocalRandom current = ThreadLocalRandom.current();
        String replace = url.replace("{cateId}", categoryId.toString())
                .replace("{areaId}", areaId.toString())
                .replace("{limit}", limit + "")
                .replace("{offset}", offset + "")
                .replace("{uuid}", current.nextLong(1000000, 1000000000) + "")
                .replace("{cityId}", cityId.toString());


        Request request = new Request.Builder()
                .url(replace)
                .get()
                .addHeader("host", "apimeishi.meituan.com")
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .addHeader("accept", "*/*")
                .addHeader("user-agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 MicroMessenger/7.0.8(0x17000820) NetType/WIFI Language/zh_CN")
                .addHeader("referer", "https://servicewechat.com/wx13k1j31j3/312/page-frame.html")
                .addHeader("accept-language", "zh-cn")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();

        PoiInfoList shopInfo = JSONUtil.toBean(response.body().string(), PoiInfoList.class);

        return shopInfo;

    }


    /**
     * 获取店铺基础信息
     *
     * @param poiId
     * @param cityId
     * @return
     * @throws IOException
     */
    public ShopInfo getShopInfo(Long poiId, Integer cityId) throws IOException {

        OkHttpClient client = HttpProxyClient.createOkHttpClient();

        String url = "https://i.meituan.com/wrapapi/allpoiinfo?riskLevel=71&optimusCode={cityId}&poiId={poiId}&isDaoZong=false";

        String replace = url.replace("{poiId}", poiId + "")
                .replace("{cityId}", cityId + "");

        Request request = new Request.Builder()
                .url(replace)
                .method("GET", null)
                .addHeader("accept", "*/*")
                .addHeader("accept-language", "zh-cn")
                .addHeader("cache-control", "no-cache")
                .addHeader("clientversion", "2.9.4")
                .addHeader("content-type", "application/json")
                .addHeader("host", "i.meituan.com")
                .addHeader("referer", "https://servicewechat.com/wxde8ac0a21135c07d/316/page-frame.html")
                .addHeader("user-agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 MicroMessenger/7.0.8(0x17000820) NetType/WIFI Language/zh_CN")
                .addHeader("utm_medium", "iphone")
                .build();

        Response response = client.newCall(request).execute();

        ShopInfo shopInfo = JSONUtil.toBean(response.body().string(), ShopInfo.class);

        return shopInfo;
    }
}
