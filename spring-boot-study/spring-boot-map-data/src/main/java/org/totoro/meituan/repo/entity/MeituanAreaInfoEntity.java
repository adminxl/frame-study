package org.totoro.meituan.repo.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.*;

/**
 * 美团区域信息(MeituanAreaInfo)实体类
 *
 * @author makejava
 * @since 2019-12-27 23:05:05
 */
@Data
@Entity
@Table(name = "meituan_area_info")
public class MeituanAreaInfoEntity implements Serializable {

    private static final long serialVersionUID = 582580554601257203L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 城市/区域/板块 code
     */
    @Column(name = "adc_code")
    private String adcCode;

    /**
     * 城市/区域/板块 中文名称
     */
    @Column(name = "adc_name")
    private String adcName;

    /**
     * province：省
     * city：城市
     * district：区
     */
    @Column(name = "level")
    private String level;

    /**
     * 父类id
     */
    @Column(name = "parent_code")
    private String parentCode;

    /**
     * 排名权限
     */
    @Column(name = "ranking")
    private String ranking;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;


}