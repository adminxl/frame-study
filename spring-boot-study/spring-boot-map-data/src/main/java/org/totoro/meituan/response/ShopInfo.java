package org.totoro.meituan.response;

import lombok.Data;

import java.util.List;

/**
 * 店铺信息
 *
 * @author daocr
 * @date 2019/12/27
 */
@Data
public class ShopInfo {

    public ShopInfoData data;

    @Data
    public static class ShopInfoData {
        private ShopBaseInfo baseInfo;
    }

    /**
     * 店铺基础信息
     */
    @Data
    public static class ShopBaseInfo {

        /**
         * poi id
         */
        private Long id;
        /**
         * 店铺名称
         */
        private String name;

        /**
         * 评分
         */
        private Double score;
        /**
         * 均价
         */
        private Integer avgPrice;
        /**
         * 地址
         */
        private String address;
        /**
         * 联系方式
         */
        private String phone;
        /**
         * 营业时间
         */
        private String openTime;

        private Double lng;
        private Double lat;
        /**
         * 品牌id
         */
        private Integer brandId;
        /**
         * 城市id
         */
        private Integer cityId;
        /**
         * 最低消费
         */
        private Integer lowestPrice;
        /**
         * 第一分类
         */
        private List<Integer> firstCate;
        /**
         * 第二分类
         */
        private List<Integer> subCate;
        /**
         * 第三分类
         */
        private List<Integer> thridCate;
        /**
         * 品牌名称
         */
        private String brandName;

        /**
         * 评论数量
         */
        private String commentCnt;
    }
}
