package org.totoro.meituan.response;

import lombok.Data;

import java.util.List;

/**
 * @author daocr
 * @date 2019/12/26
 */
@Data
public class CityInfo {

    public ListData data;

    @Data
    public static class ListData {
        private List<City> cityList;
    }

    @Data
    public static class City {
        private Integer id;
        private String rank;
        private String pinyin;
        private Boolean isOpen;
        private Double lng;
        private Double lat;
        private String divisionStr;
        private Boolean weather;

        private String name;
        private String label;
    }


}
