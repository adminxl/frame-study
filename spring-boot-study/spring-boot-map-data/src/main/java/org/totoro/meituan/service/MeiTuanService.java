package org.totoro.meituan.service;

import org.totoro.meituan.response.AreaInfo;
import org.totoro.meituan.response.CityInfo;
import org.totoro.meituan.response.PoiInfoList;
import org.totoro.meituan.response.ShopInfo;

import java.io.IOException;

/**
 * @author daocr
 * @date 2019/12/26
 */

public interface MeiTuanService {


    /**
     * 保存城市信息
     */
    void saveCity(CityInfo.City city) throws IOException;


    /**
     * 保存区域/板块信息
     *
     * @param areaInfo
     */
    void saveAreaInfo(AreaInfo areaInfo);


}

