package org.totoro.meituan.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.totoro.meituan.repo.entity.MeituanCategoryEntity;
import org.totoro.meituan.repo.entity.MeituanPoiInfoEntity;

/**
 * 美团内别店铺统计信息
 *
 * @author yhl
 */
public interface MeituanPoiInfoRepo extends JpaRepository<MeituanPoiInfoEntity, Long> {

}
