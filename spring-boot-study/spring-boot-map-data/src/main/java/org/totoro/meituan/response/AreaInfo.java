package org.totoro.meituan.response;

import lombok.Data;

import java.util.List;

/**
 * 区域信息
 *
 * @author daocr
 * @date 2019/12/26
 */
@Data
public class AreaInfo {

    private AreaInfoData data;

    @Data
    public static class AreaInfoData {
        private List<AreaInfoItem> areasinfo;
        private List<SubAreaInfo> subareasinfo;
    }

    @Data
    public static class AreaInfoItem {

        /**
         * 区域id  例如浦东新区
         */
        private Integer id;
        /**
         * 区域名称
         */
        private String name;
        /**
         * 城市id
         */
        private Integer city;
        /**
         * 区域id
         */
        private Integer district;
        /**
         * 0：区域
         * 1：商圈
         */
        private Integer type;

        private String slug;
        /**
         * 子区域 ids
         */
        private List<Integer> subareas;
    }

    @Data
    public static class SubAreaInfo {
        /**
         * 商圈id
         */
        private Integer id;
        /**
         * 区域名称
         */
        private String name;
        /**
         * 城市id
         */
        private Integer city;
        /**
         * 区域id
         */
        private Integer district;

    }


}
