package org.totoro.meituan.service.impl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.totoro.amap.enums.Level;
import org.totoro.meituan.SpiderMeituan;
import org.totoro.meituan.repo.MeituanAreaInfoRepo;
import org.totoro.meituan.repo.entity.MeituanAreaInfoEntity;
import org.totoro.meituan.response.AreaInfo;
import org.totoro.meituan.response.CityInfo;
import org.totoro.meituan.service.MeiTuanService;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author daocr
 * @date 2019/12/26
 */
@Service
@Slf4j
public class MeiTuanServiceImpl implements MeiTuanService {


    @Autowired
    private MeituanAreaInfoRepo meituanAreaInfoRepo;


    @Autowired
    private JPAQueryFactory jpaQueryFactory;


    @Override
    public void saveCity(CityInfo.City city) throws IOException {

        // 保存城市信息
        MeituanAreaInfoEntity meituanAreaInfoEntity = new MeituanAreaInfoEntity();
        meituanAreaInfoEntity.setAdcCode(city.getId().toString());
        meituanAreaInfoEntity.setAdcName(city.getName());
        meituanAreaInfoEntity.setLevel(Level.city.name());
        meituanAreaInfoEntity.setCreateTime(new Date());
        meituanAreaInfoEntity.setUpdateTime(new Date());
        meituanAreaInfoEntity.setRanking(city.getRank());

        meituanAreaInfoRepo.save(meituanAreaInfoEntity);

    }


    @Override
    public void saveAreaInfo(AreaInfo areaInfo) {

        AreaInfo.AreaInfoData areaInfoData = areaInfo.getData();

        // 区域信息
        List<AreaInfo.AreaInfoItem> areasinfo = areaInfoData.getAreasinfo();

        List<MeituanAreaInfoEntity> areaList = areasinfo.stream().map(area -> {

            MeituanAreaInfoEntity entity = new MeituanAreaInfoEntity();

            entity.setAdcCode(area.getId().toString());
            entity.setLevel(Level.district.name());
            entity.setAdcName(area.getName());
            entity.setParentCode(area.getCity().toString());
            entity.setCreateTime(new Date());
            entity.setUpdateTime(new Date());

            return entity;
        }).collect(Collectors.toList());

        meituanAreaInfoRepo.saveAll(areaList);

        // 商圈信息
        List<AreaInfo.SubAreaInfo> subareasinfo = areaInfoData.getSubareasinfo();


        List<MeituanAreaInfoEntity> plateList = subareasinfo.stream().map(palte -> {

            Integer district = palte.getDistrict();

            if (district == 0) {
                log.info("错误数据：{}", palte);
                return null;
            }

            MeituanAreaInfoEntity entity = new MeituanAreaInfoEntity();

            entity.setAdcCode(palte.getId().toString());
            entity.setLevel(Level.plate.name());
            entity.setAdcName(palte.getName());
            entity.setParentCode(palte.getDistrict().toString());
            entity.setCreateTime(new Date());
            entity.setUpdateTime(new Date());

            return entity;
        }).filter(Objects::nonNull).collect(Collectors.toList());

        meituanAreaInfoRepo.saveAll(plateList);
    }

//    @Override
//    public void saveCategory() {
//
//
//    }


    //    @Test
//    public void test() throws IOException {
//
////        CityInfo cityInfo = new MeiTuanServiceImpl().getCityInfo();
//
//        //    AreaInfo areaInfo = new MeiTuanServiceImpl().getAreaInfo(10);
//
//        //   new MeiTuanServiceImpl().getPois(10, 275, 36, 0, 20);
//
//        ShopInfo shopInfo = new MeiTuanServiceImpl().getShopInfo(40685654L, 10);
//
//        System.out.println(shopInfo);
//    }
}
