package org.totoro.meituan.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.totoro.meituan.repo.entity.MeituanAreaInfoEntity;

/**
 * 美团区域数据
 *
 * @author yhl
 */
public interface MeituanAreaInfoRepo extends JpaRepository<MeituanAreaInfoEntity, Long> {

}
