package org.totoro.meituan.response;

import lombok.Data;

import java.util.List;

/**
 * 店铺名称
 *
 * @author daocr
 * @date 2019/12/27
 */
@Data
public class PoiInfoList {

    private ShopInfoData data;

    @Data
    public static class ShopInfoData {
        private PoiList poiList;
    }

    @Data
    public static class PoiList {

        private Long totalCount;

        private List<PoiInfo> poiInfos;
    }

    @Data
    public static class PoiInfo {

        /**
         * poi id
         */
        private String poiid;

        /**
         * 平均价格
         */
        private int avgPrice;

        /**
         * 评分
         */
        private double avgScore;

        /**
         * 类别
         */
        private String cateName;

        private String channel;

        private String showType;

        /**
         * 首页图
         */
        private String frontImg;

        /**
         * 经纬度
         */
        private double lat;

        private double lng;

        /**
         * 店铺名称
         */
        private String name;

        /**
         * 区域名称
         */
        private String areaName;

    }
}
