package org.totoro;

import com.github.davidmoten.rtree.Entry;
import com.github.davidmoten.rtree.RTree;
import com.github.davidmoten.rtree.geometry.Geometries;
import com.github.davidmoten.rtree.geometry.Point;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import org.junit.Test;
import org.locationtech.jts.index.strtree.STRtree;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;


/**
 * Unit test for simple App.
 */
public class RtreeTest {

    /**
     * 矩形 框选 查询
     */
    @Test
    public void pointleSearch() {

        RTree<String, Point> tree = RTree.maxChildren(5).create();
        tree = tree.add("DAVE", Geometries.point(10, 20))
                .add("FRED", Geometries.point(12, 25))
                .add("MARY", Geometries.point(97, 125))
                .add("red", Geometries.point(10, 20))
                .add("green", Geometries.point(12, 25))
                .add("blue", Geometries.point(97, 125));

        Observable<Entry<String, Point>> entries =
                tree.search(Geometries.rectangle(8, 15, 30, 35));
        entries.subscribe(System.out::println);

    }


    @Test
    public void rectangleSearch() {

        RTree<String, Point> tree = RTree.create();


    }


    /**
     * 最近的点选择 选择
     */
    @Test
    public void nearestSearch() {


        RTree<String, Point> mRtree = RTree.maxChildren(5).create();
        mRtree = mRtree.add("DAVE", Geometries.point(10, 20))
                .add("FRED", Geometries.point(12, 25))
                .add("MARY", Geometries.point(97, 125))
                .add("red", Geometries.point(10, 20))
                .add("green", Geometries.point(12, 25))
                .add("blue", Geometries.point(97, 125));

        Point currentLocation = Geometries.point(12, 26);

        /**
         * 1 当前位置
         * 2 最大距离
         * 3 最大数量
         */
        List<Entry<String, Point>> list = mRtree.nearest(currentLocation,
                Integer.MAX_VALUE, 1).toList()
                .toBlocking().single();

        // 获取 id
        System.out.println(list.get(0).value());
    }


    /**
     * 矩形 包含 查询
     */
    @Test
    public void rectangLeontainsSearch() {


    }


    /**
     * 建立R树索引
     *
     * @return
     */
    public com.vividsolutions.jts.index.strtree.STRtree createRtree(List<Geometry> geometryList) {

//        四叉树Quadtree类:org.vividsolutions.jts.index.quadtree;
//        Kdtree类:com.vividsolutions.jts.index.kdtree.KdTree;
//        Rtree类:com.vividsolutions.jts.index.strtree.STRtree;

        com.vividsolutions.jts.index.strtree.STRtree stRtree = new com.vividsolutions.jts.index.strtree.STRtree();

        for (Geometry geometry : geometryList) {
            Envelope envelope = geometry.getEnvelopeInternal();
            stRtree.insert(envelope, geometry);
        }
        stRtree.build();

        return stRtree;
    }

    /**
     * R树查询
     *
     * @param stRtree
     * @param searchGeo
     * @return
     */
    public List<Geometry> query(com.vividsolutions.jts.index.strtree.STRtree stRtree, Geometry searchGeo) {
        List<Geometry> result = new ArrayList<>();
        @SuppressWarnings("rawtypes")
        List list = stRtree.query(searchGeo.getEnvelopeInternal());
        for (int i = 0; i < list.size(); i++) {
            Geometry lineStr = (Geometry) list.get(i);
            if (lineStr.intersects(searchGeo)) {
                result.add(lineStr);
            }
        }
        return result;
    }

    //根据两点生成矩形搜索框
    public static Geometry generateSearchGeo(double left_top_x, double left_top_y, double right_bottom_x, double right_bottom_y) {
        Coordinate[] coors = new Coordinate[5];
        coors[0] = new Coordinate(left_top_x, left_top_y);
        coors[1] = new Coordinate(right_bottom_x, left_top_y);
        coors[2] = new Coordinate(left_top_x, right_bottom_y);
        coors[3] = new Coordinate(right_bottom_x, right_bottom_y);
        coors[4] = new Coordinate(left_top_x, left_top_y);
        LinearRing ring = new LinearRing(new CoordinateArraySequence(coors), new GeometryFactory());
        return ring;
    }


}
