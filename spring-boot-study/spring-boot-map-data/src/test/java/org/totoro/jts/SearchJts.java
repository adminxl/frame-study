package org.totoro.jts;

import org.junit.Test;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

/**
 * @author daocr
 * @date 2019/9/25
 */
public class SearchJts {

    private GeometryFactory geometryFactory = new GeometryFactory();

    //Google-S2 空间索引框架简介及部分API使用示例
    // https://blog.csdn.net/weixin_34273479/article/details/92065476

    @Test
    public void test1() throws ParseException {

        WKTReader reader = new WKTReader(geometryFactory);

        Polygon polygon = (Polygon) reader.read("POLYGON((20 10, 30 0, 40 10, 30 20, 20 10))");

        Polygon polygon2 = (Polygon) reader.read("POLYGON((30 15, 40 30, 30 35, 20 30, 30 15))");

        GeometryCollection geometryCollection = new GeometryCollection(new Geometry[]{polygon, polygon2}, geometryFactory);
    }
}
