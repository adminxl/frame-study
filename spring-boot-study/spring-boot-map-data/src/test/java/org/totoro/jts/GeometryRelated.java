package org.totoro.jts;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

/**
 * gemotry之间的关系
 *
 * @author xingxing.dxx
 */
public class GeometryRelated {

    private GeometryFactory geometryFactory = new GeometryFactory();


    /**
     * 两个几何对象是否是重叠的
     *
     * @return
     * @throws ParseException
     */
    public boolean equalsGeo() throws ParseException {
        WKTReader reader = new WKTReader(geometryFactory);
        LineString geometry1 = (LineString) reader.read("LINESTRING(0 0, 2 0, 5 0)");
        LineString geometry2 = (LineString) reader.read("LINESTRING(5 0, 0 0)");
        return geometry1.equals(geometry2);//true
    }

    /**
     * 几何对象没有交点(相邻)
     *
     * @return
     * @throws ParseException
     */
    public boolean disjointGeo() throws ParseException {
        WKTReader reader = new WKTReader(geometryFactory);
        LineString geometry1 = (LineString) reader.read("LINESTRING(0 0, 2 0, 5 0)");
        LineString geometry2 = (LineString) reader.read("LINESTRING(0 1, 0 2)");
        return geometry1.disjoint(geometry2);
    }


    /**
     * 几何形状有至少一个公共的边界点，但是没有内部点(接触)
     *
     * @return
     * @throws ParseException
     */
    public boolean touchesGeo() throws ParseException {
        WKTReader reader = new WKTReader(geometryFactory);
        LineString geometry1 = (LineString) reader.read("LINESTRING(0 0, 2 0, 5 0)");
        LineString geometry2 = (LineString) reader.read("LINESTRING(0 1, 0 2)");
        return geometry1.touches(geometry2);
    }


    /**
     * 几何形状共享一些但不是所有的内部点(交叉 )
     *
     * @return
     * @throws ParseException
     */
    public boolean crossesGeo() throws ParseException {
        WKTReader reader = new WKTReader(geometryFactory);
        LineString geometry1 = (LineString) reader.read("LINESTRING(0 0, 2 0, 5 0)");
        LineString geometry2 = (LineString) reader.read("LINESTRING(0 1, 0 2)");
        return geometry1.crosses(geometry2);
    }


    /**
     * 至少一个公共点(相交)
     *
     * @return
     * @throws ParseException
     */
    public boolean intersectsGeo() throws ParseException {
        WKTReader reader = new WKTReader(geometryFactory);
        LineString geometry1 = (LineString) reader.read("LINESTRING(0 0, 2 0, 5 0)");
        LineString geometry2 = (LineString) reader.read("LINESTRING(0 0, 0 2)");
        Geometry interPoint = geometry1.intersection(geometry2);//相交点
        System.out.println(interPoint.toText());//输出 POINT (0 0)
        return geometry1.intersects(geometry2);
    }

    /**
     * 几何形状共享一部分但不是所有的公共点，而且相交处有他们自己相同的区域 (重叠)
     *
     * @return
     * @throws ParseException
     */
    public boolean overlapsGeo() throws ParseException {
        WKTReader reader = new WKTReader(geometryFactory);
        LineString geometry1 = (LineString) reader.read("LINESTRING(0 0, 2 0, 5 0)");
        LineString geometry2 = (LineString) reader.read("LINESTRING(0 0, 0 2)");
        return geometry1.overlaps(geometry2);
    }

    /**
     * 判断以x,y为坐标的点point(x,y)是否在geometry表示的Polygon中
     *
     * @param x
     * @param y
     * @param geometry wkt格式
     * @return
     */
    public boolean withinGeo(double x, double y, String geometry) throws ParseException {

        Coordinate coord = new Coordinate(x, y);
        Point point = geometryFactory.createPoint(coord);

        WKTReader reader = new WKTReader(geometryFactory);
        Polygon polygon = (Polygon) reader.read(geometry);
        return point.within(polygon);
    }

    /**
     * 获取 最小矩形
     *
     * @throws ParseException
     */
    public void envelopeInternal() throws ParseException {

        WKTReader reader = new WKTReader(geometryFactory);

        Polygon polygon = (Polygon) reader.read("POLYGON((121.576483 31.207759,121.578072 31.209429,121.579364 31.210156,121.586177 31.211877,121.587336 31.212307,121.588925 31.213439,121.590999 31.214841,121.591969 31.215407,121.598652 31.217482,121.608249 31.219317,121.611188 31.219992,121.614074 31.220614,121.616825 31.220858,121.623678 31.221079,121.625729 31.221378,121.627159 31.222161,121.630128 31.223298,121.632854 31.223732,121.638496 31.224629,121.641439 31.225227,121.645489 31.225934,121.645948 31.225989,121.646380 31.226124,121.647190 31.224912,121.650076 31.220227,121.650866 31.218817,121.652400 31.215648,121.654086 31.212207,121.653934 31.211994,121.653327 31.211705,121.650167 31.209848,121.646825 31.207992,121.643863 31.206819,121.643650 31.206652,121.643681 31.206349,121.644880 31.198279,121.644986 31.193924,121.645229 31.191633,121.646310 31.182888,121.646174 31.182700,121.645806 31.182443,121.645080 31.182101,121.644285 31.181784,121.643679 31.181313,121.642910 31.181150,121.641782 31.180671,121.639467 31.179686,121.638288 31.179274,121.636938 31.178786,121.634487 31.177853,121.634017 31.177665,121.633411 31.177570,121.632899 31.177646,121.632019 31.177594,121.631506 31.177559,121.630644 31.177251,121.630106 31.176951,121.629602 31.176848,121.628270 31.176659,121.627817 31.176488,121.627587 31.176232,121.627348 31.175839,121.626021 31.175108,121.624009 31.174372,121.622540 31.174029,121.621624 31.173877,121.621430 31.173910,121.621341 31.174086,121.621922 31.177774,121.622357 31.179652,121.623331 31.181723,121.623325 31.181883,121.623201 31.182080,121.622063 31.182676,121.620854 31.182303,121.619773 31.181915,121.618058 31.180644,121.614700 31.178882,121.611524 31.177565,121.608672 31.176573,121.603839 31.174690,121.601777 31.173942,121.597007 31.172870,121.595187 31.172405,121.590610 31.171537,121.588672 31.170945,121.587487 31.170973,121.585655 31.171891,121.584901 31.172836,121.582963 31.177720,121.581563 31.182873,121.578892 31.192622,121.578413 31.194300,121.578004 31.195581,121.577961 31.196118,121.576989 31.200750,121.576554 31.202688,121.576086 31.204438,121.575967 31.205266,121.575890 31.205906,121.575975 31.206452,121.576112 31.206990,121.576483 31.207759))");

        Envelope envelopeInternal = polygon.getEnvelopeInternal();


    }

    /**
     * @param args
     * @throws ParseException
     */
    public static void main(String[] args) throws ParseException {
        GeometryRelated gr = new GeometryRelated();
        System.out.println(gr.equalsGeo());
        System.out.println(gr.disjointGeo());
        System.out.println(gr.intersectsGeo());
        System.out.println(gr.touchesGeo());
        System.out.println(gr.overlapsGeo());
        System.out.println(gr.crossesGeo());

        System.out.println(gr.withinGeo(5, 5, "POLYGON((0 0, 10 0, 10 10, 0 10,0 0))"));
    }

}