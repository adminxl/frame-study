package org.totoro.jts;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.*;

/**
 * gemotry之间的关系分析
 *
 * @author xingxing.dxx
 */
public class Operation {

    private GeometryFactory geometryFactory = new GeometryFactory();

    /**
     * create a Point
     *
     * @param x
     * @param y
     * @return
     */
    public Coordinate point(double x, double y) {
        return new Coordinate(x, y);
    }


    /**
     * create a line
     *
     * @return
     */
    public LineString createLine(List<Coordinate> points) {
        Coordinate[] coords = (Coordinate[]) points.toArray(new Coordinate[points.size()]);
        LineString line = geometryFactory.createLineString(coords);
        return line;
    }

    /**
     * 缓冲区分析（Buffer）L
     * 返回a指定距离内的多边形和多多边形
     *
     * @param a
     * @param distance
     * @return
     */
    public Geometry bufferGeo(Geometry a, double distance) {
        return a.buffer(distance);
    }


    /**
     * 凸壳分析（ConvexHull）
     * 包含几何形体的所有点的最小凸壳多边形,外包多边形
     *
     * @param a
     * @return
     */
    public Geometry ConvexHull(Geometry a) {
        return a.convexHull();
    }

    /**
     * 返回(A)与(B)中距离最近的两个点的距离
     *
     * @param a
     * @param b
     * @return
     */
    public double distanceGeo(Geometry a, Geometry b) {
        return a.distance(b);
    }

    /**
     * 交叉分析（Intersection）
     * 两个几何对象的交集
     *
     * @param a
     * @param b
     * @return
     */
    public Geometry intersectionGeo(Geometry a, Geometry b) {
        return a.intersection(b);
    }

    /**
     * 联合分析（Union）
     * 几何对象合并
     *
     * @param a
     * @param b
     * @return
     */
    public Geometry unionGeo(Geometry a, Geometry b) {
        return a.union(b);
    }

    /**
     * 差异分析（Difference）
     * 在A几何对象中有的，但是B几何对象中没有
     *
     * @param a
     * @param b
     * @return
     */
    public Geometry differenceGeo(Geometry a, Geometry b) {
        return a.difference(b);
    }

    /**
     * 对称差异分析（SymDifference）
     * <p>
     * (AUB-A∩B) AB形状的对称差异分析就是位于A中或者B中但不同时在AB中的所有点的集合
     *
     * @param a
     * @param b
     * @return
     */
    public Geometry SymDifference(Geometry a, Geometry b) {
        return a.symDifference(b);
    }


    public static void main(String[] args) {
        Operation op = new Operation();


        //创建一条线
        List<Coordinate> points1 = new ArrayList<Coordinate>();
        points1.add(op.point(0, 0));
        points1.add(op.point(1, 3));
        points1.add(op.point(2, 3));
        LineString line1 = op.createLine(points1);
        //创建第二条线
        List<Coordinate> points2 = new ArrayList<Coordinate>();
        points2.add(op.point(3, 0));
        points2.add(op.point(3, 3));
        points2.add(op.point(5, 6));
        LineString line2 = op.createLine(points2);

        System.out.println(op.distanceGeo(line1, line2));//out 1.0
        System.out.println(op.intersectionGeo(line1, line2));//out GEOMETRYCOLLECTION EMPTY
        System.out.println(op.unionGeo(line1, line2)); //out MULTILINESTRING ((0 0, 1 3, 2 3), (3 0, 3 3, 5 6))
        System.out.println(op.differenceGeo(line1, line2));//out LINESTRING (0 0, 1 3, 2 3)

        GeometryCollection geometryCollection = new GeometryCollection(null, null);
    }
}