package org.totoro;

import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author daocr
 * @date 2020/1/28
 */
public class JsoupTest {


    @Test
    public void test() throws IOException {

        Document document = Jsoup.connect("https://www.cnblogs.com/rickiyang/").get();
//        Document document = Jsoup.connect("https://juejin.im/user/5d5ea68e6fb9a06afa328f56").get();
//        Document document = Jsoup.connect("https://www.jianshu.com/u/51b4ef597b53?utm_source=desktop&utm_medium=index-users").get();

        List<ElementObj> parsing = parsing(document.body(), new ArrayList<>());

        List<ElementObj> collect = getSort2(parsing);

        for (ElementObj elementObj : collect) {
            System.out.println(elementObj.getMaxClass());
        }

        System.out.println("");
    }

    @NotNull
    private List<ElementObj> getSortV1(List<ElementObj> parsing) {
        parsing.stream().forEach(e -> {
            Elements children = e.getChildren();
            Optional<Map.Entry<String, Long>> mapEntity = children.stream().collect(Collectors.groupingBy(g -> g.attr("class"), Collectors.counting())).entrySet()
                    .stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).filter(Objects::nonNull).findFirst();
            mapEntity.ifPresent(p -> {
                e.setMaxClass(p);
            });
        });

        return parsing.stream().sorted(Comparator.comparing(e -> {
            if (!ObjectUtils.isEmpty(e.getMaxClass())) {
                return e.getMaxClass().getValue();
            }
            return 0L;
        }, Comparator.reverseOrder())).collect(Collectors.toList());
    }

    @NotNull
    private List<ElementObj> getSort2(List<ElementObj> parsing) {

        parsing.forEach(e -> {

            Elements children = e.getChildren();

            List<Map.Entry<String, Long>> collect = children.stream().collect(Collectors.groupingBy(g -> g.tagName(), Collectors.counting())).entrySet()
                    .stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).filter(Objects::nonNull).collect(Collectors.toList());

            Optional<Map.Entry<String, Long>> mapEntity = collect.stream().findFirst();

            mapEntity.ifPresent(p -> {
                e.setMaxClass(p);
            });
        });

        return parsing.stream().sorted(Comparator.comparing(e -> {
            if (!ObjectUtils.isEmpty(e.getMaxClass())) {
                return e.getMaxClass().getValue();
            }
            return 0L;
        }, Comparator.reverseOrder())).collect(Collectors.toList());
    }


    /**
     * 解析节点
     *
     * @param
     * @return
     */
    public List<ElementObj> parsing(Element element, List<ElementObj> result) {

        ElementObj elementObj = new ElementObj();
        Elements children = element.children();

        if (!ObjectUtils.isEmpty(children)) {
            for (Element child : children) {
                parsing(child, result);
            }
        }

        elementObj.setElement(element);
        elementObj.setChildren(children);
        result.add(elementObj);

        return result;

    }


    @Data
    public static class ElementObj {
        private Map.Entry<String, Long> maxClass;
        private Element element;
        private Elements children;

    }
}
