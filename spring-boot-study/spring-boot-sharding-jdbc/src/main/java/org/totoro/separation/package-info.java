/**
 * @author YHL
 * @version V1.0
 * @Description: 读写分离
 * @date 2018-08-03
 * @see http://shardingsphere.io/document/current/cn/manual/sharding-jdbc/usage/read-write-splitting/
 */
package org.totoro.separation;

