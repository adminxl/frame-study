package org.totoro.ddl;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/DDLStatement.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link DDLStatementParser}.
 */
public interface DDLStatementListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createTable}.
	 * @param ctx the parse tree
	 */
	void enterCreateTable(DDLStatementParser.CreateTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createTable}.
	 * @param ctx the parse tree
	 */
	void exitCreateTable(DDLStatementParser.CreateTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterTable}.
	 * @param ctx the parse tree
	 */
	void enterAlterTable(DDLStatementParser.AlterTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterTable}.
	 * @param ctx the parse tree
	 */
	void exitAlterTable(DDLStatementParser.AlterTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropTable}.
	 * @param ctx the parse tree
	 */
	void enterDropTable(DDLStatementParser.DropTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropTable}.
	 * @param ctx the parse tree
	 */
	void exitDropTable(DDLStatementParser.DropTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropIndex}.
	 * @param ctx the parse tree
	 */
	void enterDropIndex(DDLStatementParser.DropIndexContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropIndex}.
	 * @param ctx the parse tree
	 */
	void exitDropIndex(DDLStatementParser.DropIndexContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#truncateTable}.
	 * @param ctx the parse tree
	 */
	void enterTruncateTable(DDLStatementParser.TruncateTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#truncateTable}.
	 * @param ctx the parse tree
	 */
	void exitTruncateTable(DDLStatementParser.TruncateTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createIndex}.
	 * @param ctx the parse tree
	 */
	void enterCreateIndex(DDLStatementParser.CreateIndexContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createIndex}.
	 * @param ctx the parse tree
	 */
	void exitCreateIndex(DDLStatementParser.CreateIndexContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createDatabase}.
	 * @param ctx the parse tree
	 */
	void enterCreateDatabase(DDLStatementParser.CreateDatabaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createDatabase}.
	 * @param ctx the parse tree
	 */
	void exitCreateDatabase(DDLStatementParser.CreateDatabaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterDatabase}.
	 * @param ctx the parse tree
	 */
	void enterAlterDatabase(DDLStatementParser.AlterDatabaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterDatabase}.
	 * @param ctx the parse tree
	 */
	void exitAlterDatabase(DDLStatementParser.AlterDatabaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createDatabaseSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterCreateDatabaseSpecification_(DDLStatementParser.CreateDatabaseSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createDatabaseSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitCreateDatabaseSpecification_(DDLStatementParser.CreateDatabaseSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropDatabase}.
	 * @param ctx the parse tree
	 */
	void enterDropDatabase(DDLStatementParser.DropDatabaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropDatabase}.
	 * @param ctx the parse tree
	 */
	void exitDropDatabase(DDLStatementParser.DropDatabaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterInstance}.
	 * @param ctx the parse tree
	 */
	void enterAlterInstance(DDLStatementParser.AlterInstanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterInstance}.
	 * @param ctx the parse tree
	 */
	void exitAlterInstance(DDLStatementParser.AlterInstanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#instanceAction}.
	 * @param ctx the parse tree
	 */
	void enterInstanceAction(DDLStatementParser.InstanceActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#instanceAction}.
	 * @param ctx the parse tree
	 */
	void exitInstanceAction(DDLStatementParser.InstanceActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createEvent}.
	 * @param ctx the parse tree
	 */
	void enterCreateEvent(DDLStatementParser.CreateEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createEvent}.
	 * @param ctx the parse tree
	 */
	void exitCreateEvent(DDLStatementParser.CreateEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterEvent}.
	 * @param ctx the parse tree
	 */
	void enterAlterEvent(DDLStatementParser.AlterEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterEvent}.
	 * @param ctx the parse tree
	 */
	void exitAlterEvent(DDLStatementParser.AlterEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropEvent}.
	 * @param ctx the parse tree
	 */
	void enterDropEvent(DDLStatementParser.DropEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropEvent}.
	 * @param ctx the parse tree
	 */
	void exitDropEvent(DDLStatementParser.DropEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createFunction}.
	 * @param ctx the parse tree
	 */
	void enterCreateFunction(DDLStatementParser.CreateFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createFunction}.
	 * @param ctx the parse tree
	 */
	void exitCreateFunction(DDLStatementParser.CreateFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterFunction}.
	 * @param ctx the parse tree
	 */
	void enterAlterFunction(DDLStatementParser.AlterFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterFunction}.
	 * @param ctx the parse tree
	 */
	void exitAlterFunction(DDLStatementParser.AlterFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropFunction}.
	 * @param ctx the parse tree
	 */
	void enterDropFunction(DDLStatementParser.DropFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropFunction}.
	 * @param ctx the parse tree
	 */
	void exitDropFunction(DDLStatementParser.DropFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createProcedure}.
	 * @param ctx the parse tree
	 */
	void enterCreateProcedure(DDLStatementParser.CreateProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createProcedure}.
	 * @param ctx the parse tree
	 */
	void exitCreateProcedure(DDLStatementParser.CreateProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterProcedure}.
	 * @param ctx the parse tree
	 */
	void enterAlterProcedure(DDLStatementParser.AlterProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterProcedure}.
	 * @param ctx the parse tree
	 */
	void exitAlterProcedure(DDLStatementParser.AlterProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropProcedure}.
	 * @param ctx the parse tree
	 */
	void enterDropProcedure(DDLStatementParser.DropProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropProcedure}.
	 * @param ctx the parse tree
	 */
	void exitDropProcedure(DDLStatementParser.DropProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createServer}.
	 * @param ctx the parse tree
	 */
	void enterCreateServer(DDLStatementParser.CreateServerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createServer}.
	 * @param ctx the parse tree
	 */
	void exitCreateServer(DDLStatementParser.CreateServerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterServer}.
	 * @param ctx the parse tree
	 */
	void enterAlterServer(DDLStatementParser.AlterServerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterServer}.
	 * @param ctx the parse tree
	 */
	void exitAlterServer(DDLStatementParser.AlterServerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropServer}.
	 * @param ctx the parse tree
	 */
	void enterDropServer(DDLStatementParser.DropServerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropServer}.
	 * @param ctx the parse tree
	 */
	void exitDropServer(DDLStatementParser.DropServerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createView}.
	 * @param ctx the parse tree
	 */
	void enterCreateView(DDLStatementParser.CreateViewContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createView}.
	 * @param ctx the parse tree
	 */
	void exitCreateView(DDLStatementParser.CreateViewContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterView}.
	 * @param ctx the parse tree
	 */
	void enterAlterView(DDLStatementParser.AlterViewContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterView}.
	 * @param ctx the parse tree
	 */
	void exitAlterView(DDLStatementParser.AlterViewContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropView}.
	 * @param ctx the parse tree
	 */
	void enterDropView(DDLStatementParser.DropViewContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropView}.
	 * @param ctx the parse tree
	 */
	void exitDropView(DDLStatementParser.DropViewContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createTablespaceInnodb}.
	 * @param ctx the parse tree
	 */
	void enterCreateTablespaceInnodb(DDLStatementParser.CreateTablespaceInnodbContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createTablespaceInnodb}.
	 * @param ctx the parse tree
	 */
	void exitCreateTablespaceInnodb(DDLStatementParser.CreateTablespaceInnodbContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createTablespaceNdb}.
	 * @param ctx the parse tree
	 */
	void enterCreateTablespaceNdb(DDLStatementParser.CreateTablespaceNdbContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createTablespaceNdb}.
	 * @param ctx the parse tree
	 */
	void exitCreateTablespaceNdb(DDLStatementParser.CreateTablespaceNdbContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterTablespace}.
	 * @param ctx the parse tree
	 */
	void enterAlterTablespace(DDLStatementParser.AlterTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterTablespace}.
	 * @param ctx the parse tree
	 */
	void exitAlterTablespace(DDLStatementParser.AlterTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropTablespace}.
	 * @param ctx the parse tree
	 */
	void enterDropTablespace(DDLStatementParser.DropTablespaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropTablespace}.
	 * @param ctx the parse tree
	 */
	void exitDropTablespace(DDLStatementParser.DropTablespaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void enterCreateLogfileGroup(DDLStatementParser.CreateLogfileGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void exitCreateLogfileGroup(DDLStatementParser.CreateLogfileGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void enterAlterLogfileGroup(DDLStatementParser.AlterLogfileGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void exitAlterLogfileGroup(DDLStatementParser.AlterLogfileGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void enterDropLogfileGroup(DDLStatementParser.DropLogfileGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropLogfileGroup}.
	 * @param ctx the parse tree
	 */
	void exitDropLogfileGroup(DDLStatementParser.DropLogfileGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createTrigger}.
	 * @param ctx the parse tree
	 */
	void enterCreateTrigger(DDLStatementParser.CreateTriggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createTrigger}.
	 * @param ctx the parse tree
	 */
	void exitCreateTrigger(DDLStatementParser.CreateTriggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createTableSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterCreateTableSpecification_(DDLStatementParser.CreateTableSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createTableSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitCreateTableSpecification_(DDLStatementParser.CreateTableSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableNotExistClause_}.
	 * @param ctx the parse tree
	 */
	void enterTableNotExistClause_(DDLStatementParser.TableNotExistClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableNotExistClause_}.
	 * @param ctx the parse tree
	 */
	void exitTableNotExistClause_(DDLStatementParser.TableNotExistClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createDefinitionClause}.
	 * @param ctx the parse tree
	 */
	void enterCreateDefinitionClause(DDLStatementParser.CreateDefinitionClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createDefinitionClause}.
	 * @param ctx the parse tree
	 */
	void exitCreateDefinitionClause(DDLStatementParser.CreateDefinitionClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void enterCreateDefinition(DDLStatementParser.CreateDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createDefinition}.
	 * @param ctx the parse tree
	 */
	void exitCreateDefinition(DDLStatementParser.CreateDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#columnDefinition}.
	 * @param ctx the parse tree
	 */
	void enterColumnDefinition(DDLStatementParser.ColumnDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#columnDefinition}.
	 * @param ctx the parse tree
	 */
	void exitColumnDefinition(DDLStatementParser.ColumnDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#storageOption}.
	 * @param ctx the parse tree
	 */
	void enterStorageOption(DDLStatementParser.StorageOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#storageOption}.
	 * @param ctx the parse tree
	 */
	void exitStorageOption(DDLStatementParser.StorageOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#generatedOption}.
	 * @param ctx the parse tree
	 */
	void enterGeneratedOption(DDLStatementParser.GeneratedOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#generatedOption}.
	 * @param ctx the parse tree
	 */
	void exitGeneratedOption(DDLStatementParser.GeneratedOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dataTypeGenericOption}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeGenericOption(DDLStatementParser.DataTypeGenericOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dataTypeGenericOption}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeGenericOption(DDLStatementParser.DataTypeGenericOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#checkConstraintDefinition}.
	 * @param ctx the parse tree
	 */
	void enterCheckConstraintDefinition(DDLStatementParser.CheckConstraintDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#checkConstraintDefinition}.
	 * @param ctx the parse tree
	 */
	void exitCheckConstraintDefinition(DDLStatementParser.CheckConstraintDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#referenceDefinition}.
	 * @param ctx the parse tree
	 */
	void enterReferenceDefinition(DDLStatementParser.ReferenceDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#referenceDefinition}.
	 * @param ctx the parse tree
	 */
	void exitReferenceDefinition(DDLStatementParser.ReferenceDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#referenceOption_}.
	 * @param ctx the parse tree
	 */
	void enterReferenceOption_(DDLStatementParser.ReferenceOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#referenceOption_}.
	 * @param ctx the parse tree
	 */
	void exitReferenceOption_(DDLStatementParser.ReferenceOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#indexDefinition_}.
	 * @param ctx the parse tree
	 */
	void enterIndexDefinition_(DDLStatementParser.IndexDefinition_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#indexDefinition_}.
	 * @param ctx the parse tree
	 */
	void exitIndexDefinition_(DDLStatementParser.IndexDefinition_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#indexType_}.
	 * @param ctx the parse tree
	 */
	void enterIndexType_(DDLStatementParser.IndexType_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#indexType_}.
	 * @param ctx the parse tree
	 */
	void exitIndexType_(DDLStatementParser.IndexType_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#keyParts_}.
	 * @param ctx the parse tree
	 */
	void enterKeyParts_(DDLStatementParser.KeyParts_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#keyParts_}.
	 * @param ctx the parse tree
	 */
	void exitKeyParts_(DDLStatementParser.KeyParts_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#keyPart_}.
	 * @param ctx the parse tree
	 */
	void enterKeyPart_(DDLStatementParser.KeyPart_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#keyPart_}.
	 * @param ctx the parse tree
	 */
	void exitKeyPart_(DDLStatementParser.KeyPart_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#indexOption_}.
	 * @param ctx the parse tree
	 */
	void enterIndexOption_(DDLStatementParser.IndexOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#indexOption_}.
	 * @param ctx the parse tree
	 */
	void exitIndexOption_(DDLStatementParser.IndexOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#constraintDefinition}.
	 * @param ctx the parse tree
	 */
	void enterConstraintDefinition(DDLStatementParser.ConstraintDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#constraintDefinition}.
	 * @param ctx the parse tree
	 */
	void exitConstraintDefinition(DDLStatementParser.ConstraintDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#primaryKeyOption}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryKeyOption(DDLStatementParser.PrimaryKeyOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#primaryKeyOption}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryKeyOption(DDLStatementParser.PrimaryKeyOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#primaryKey}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryKey(DDLStatementParser.PrimaryKeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#primaryKey}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryKey(DDLStatementParser.PrimaryKeyContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#uniqueOption_}.
	 * @param ctx the parse tree
	 */
	void enterUniqueOption_(DDLStatementParser.UniqueOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#uniqueOption_}.
	 * @param ctx the parse tree
	 */
	void exitUniqueOption_(DDLStatementParser.UniqueOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#foreignKeyOption}.
	 * @param ctx the parse tree
	 */
	void enterForeignKeyOption(DDLStatementParser.ForeignKeyOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#foreignKeyOption}.
	 * @param ctx the parse tree
	 */
	void exitForeignKeyOption(DDLStatementParser.ForeignKeyOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createLikeClause}.
	 * @param ctx the parse tree
	 */
	void enterCreateLikeClause(DDLStatementParser.CreateLikeClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createLikeClause}.
	 * @param ctx the parse tree
	 */
	void exitCreateLikeClause(DDLStatementParser.CreateLikeClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#createIndexSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterCreateIndexSpecification_(DDLStatementParser.CreateIndexSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#createIndexSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitCreateIndexSpecification_(DDLStatementParser.CreateIndexSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterDefinitionClause}.
	 * @param ctx the parse tree
	 */
	void enterAlterDefinitionClause(DDLStatementParser.AlterDefinitionClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterDefinitionClause}.
	 * @param ctx the parse tree
	 */
	void exitAlterDefinitionClause(DDLStatementParser.AlterDefinitionClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAlterSpecification(DDLStatementParser.AlterSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alterSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAlterSpecification(DDLStatementParser.AlterSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableOptions_}.
	 * @param ctx the parse tree
	 */
	void enterTableOptions_(DDLStatementParser.TableOptions_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableOptions_}.
	 * @param ctx the parse tree
	 */
	void exitTableOptions_(DDLStatementParser.TableOptions_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableOption_}.
	 * @param ctx the parse tree
	 */
	void enterTableOption_(DDLStatementParser.TableOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableOption_}.
	 * @param ctx the parse tree
	 */
	void exitTableOption_(DDLStatementParser.TableOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#addColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAddColumnSpecification(DDLStatementParser.AddColumnSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#addColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAddColumnSpecification(DDLStatementParser.AddColumnSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#firstOrAfterColumn}.
	 * @param ctx the parse tree
	 */
	void enterFirstOrAfterColumn(DDLStatementParser.FirstOrAfterColumnContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#firstOrAfterColumn}.
	 * @param ctx the parse tree
	 */
	void exitFirstOrAfterColumn(DDLStatementParser.FirstOrAfterColumnContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#addIndexSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAddIndexSpecification(DDLStatementParser.AddIndexSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#addIndexSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAddIndexSpecification(DDLStatementParser.AddIndexSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#addConstraintSpecification}.
	 * @param ctx the parse tree
	 */
	void enterAddConstraintSpecification(DDLStatementParser.AddConstraintSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#addConstraintSpecification}.
	 * @param ctx the parse tree
	 */
	void exitAddConstraintSpecification(DDLStatementParser.AddConstraintSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#changeColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void enterChangeColumnSpecification(DDLStatementParser.ChangeColumnSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#changeColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void exitChangeColumnSpecification(DDLStatementParser.ChangeColumnSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#modifyColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void enterModifyColumnSpecification(DDLStatementParser.ModifyColumnSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#modifyColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void exitModifyColumnSpecification(DDLStatementParser.ModifyColumnSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void enterDropColumnSpecification(DDLStatementParser.DropColumnSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void exitDropColumnSpecification(DDLStatementParser.DropColumnSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropIndexSpecification}.
	 * @param ctx the parse tree
	 */
	void enterDropIndexSpecification(DDLStatementParser.DropIndexSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropIndexSpecification}.
	 * @param ctx the parse tree
	 */
	void exitDropIndexSpecification(DDLStatementParser.DropIndexSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropPrimaryKeySpecification}.
	 * @param ctx the parse tree
	 */
	void enterDropPrimaryKeySpecification(DDLStatementParser.DropPrimaryKeySpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropPrimaryKeySpecification}.
	 * @param ctx the parse tree
	 */
	void exitDropPrimaryKeySpecification(DDLStatementParser.DropPrimaryKeySpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#renameColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void enterRenameColumnSpecification(DDLStatementParser.RenameColumnSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#renameColumnSpecification}.
	 * @param ctx the parse tree
	 */
	void exitRenameColumnSpecification(DDLStatementParser.RenameColumnSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#renameIndexSpecification}.
	 * @param ctx the parse tree
	 */
	void enterRenameIndexSpecification(DDLStatementParser.RenameIndexSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#renameIndexSpecification}.
	 * @param ctx the parse tree
	 */
	void exitRenameIndexSpecification(DDLStatementParser.RenameIndexSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#renameTableSpecification}.
	 * @param ctx the parse tree
	 */
	void enterRenameTableSpecification(DDLStatementParser.RenameTableSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#renameTableSpecification}.
	 * @param ctx the parse tree
	 */
	void exitRenameTableSpecification(DDLStatementParser.RenameTableSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionDefinitions_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionDefinitions_(DDLStatementParser.PartitionDefinitions_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionDefinitions_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionDefinitions_(DDLStatementParser.PartitionDefinitions_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionDefinition_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionDefinition_(DDLStatementParser.PartitionDefinition_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionDefinition_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionDefinition_(DDLStatementParser.PartitionDefinition_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionLessThanValue_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionLessThanValue_(DDLStatementParser.PartitionLessThanValue_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionLessThanValue_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionLessThanValue_(DDLStatementParser.PartitionLessThanValue_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionValueList_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionValueList_(DDLStatementParser.PartitionValueList_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionValueList_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionValueList_(DDLStatementParser.PartitionValueList_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionDefinitionOption_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionDefinitionOption_(DDLStatementParser.PartitionDefinitionOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionDefinitionOption_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionDefinitionOption_(DDLStatementParser.PartitionDefinitionOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#subpartitionDefinition_}.
	 * @param ctx the parse tree
	 */
	void enterSubpartitionDefinition_(DDLStatementParser.SubpartitionDefinition_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#subpartitionDefinition_}.
	 * @param ctx the parse tree
	 */
	void exitSubpartitionDefinition_(DDLStatementParser.SubpartitionDefinition_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropTableSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterDropTableSpecification_(DDLStatementParser.DropTableSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropTableSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitDropTableSpecification_(DDLStatementParser.DropTableSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableExistClause_}.
	 * @param ctx the parse tree
	 */
	void enterTableExistClause_(DDLStatementParser.TableExistClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableExistClause_}.
	 * @param ctx the parse tree
	 */
	void exitTableExistClause_(DDLStatementParser.TableExistClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dropIndexSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterDropIndexSpecification_(DDLStatementParser.DropIndexSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dropIndexSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitDropIndexSpecification_(DDLStatementParser.DropIndexSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#ownerStatement}.
	 * @param ctx the parse tree
	 */
	void enterOwnerStatement(DDLStatementParser.OwnerStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#ownerStatement}.
	 * @param ctx the parse tree
	 */
	void exitOwnerStatement(DDLStatementParser.OwnerStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#scheduleExpression_}.
	 * @param ctx the parse tree
	 */
	void enterScheduleExpression_(DDLStatementParser.ScheduleExpression_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#scheduleExpression_}.
	 * @param ctx the parse tree
	 */
	void exitScheduleExpression_(DDLStatementParser.ScheduleExpression_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#timestampValue}.
	 * @param ctx the parse tree
	 */
	void enterTimestampValue(DDLStatementParser.TimestampValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#timestampValue}.
	 * @param ctx the parse tree
	 */
	void exitTimestampValue(DDLStatementParser.TimestampValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#routineBody}.
	 * @param ctx the parse tree
	 */
	void enterRoutineBody(DDLStatementParser.RoutineBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#routineBody}.
	 * @param ctx the parse tree
	 */
	void exitRoutineBody(DDLStatementParser.RoutineBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#serverOption_}.
	 * @param ctx the parse tree
	 */
	void enterServerOption_(DDLStatementParser.ServerOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#serverOption_}.
	 * @param ctx the parse tree
	 */
	void exitServerOption_(DDLStatementParser.ServerOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#routineOption_}.
	 * @param ctx the parse tree
	 */
	void enterRoutineOption_(DDLStatementParser.RoutineOption_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#routineOption_}.
	 * @param ctx the parse tree
	 */
	void exitRoutineOption_(DDLStatementParser.RoutineOption_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#procedureParameter_}.
	 * @param ctx the parse tree
	 */
	void enterProcedureParameter_(DDLStatementParser.ProcedureParameter_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#procedureParameter_}.
	 * @param ctx the parse tree
	 */
	void exitProcedureParameter_(DDLStatementParser.ProcedureParameter_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#fileSizeLiteral_}.
	 * @param ctx the parse tree
	 */
	void enterFileSizeLiteral_(DDLStatementParser.FileSizeLiteral_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#fileSizeLiteral_}.
	 * @param ctx the parse tree
	 */
	void exitFileSizeLiteral_(DDLStatementParser.FileSizeLiteral_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#parameterMarker}.
	 * @param ctx the parse tree
	 */
	void enterParameterMarker(DDLStatementParser.ParameterMarkerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#parameterMarker}.
	 * @param ctx the parse tree
	 */
	void exitParameterMarker(DDLStatementParser.ParameterMarkerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#literals}.
	 * @param ctx the parse tree
	 */
	void enterLiterals(DDLStatementParser.LiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#literals}.
	 * @param ctx the parse tree
	 */
	void exitLiterals(DDLStatementParser.LiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#stringLiterals}.
	 * @param ctx the parse tree
	 */
	void enterStringLiterals(DDLStatementParser.StringLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#stringLiterals}.
	 * @param ctx the parse tree
	 */
	void exitStringLiterals(DDLStatementParser.StringLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#numberLiterals}.
	 * @param ctx the parse tree
	 */
	void enterNumberLiterals(DDLStatementParser.NumberLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#numberLiterals}.
	 * @param ctx the parse tree
	 */
	void exitNumberLiterals(DDLStatementParser.NumberLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 */
	void enterDateTimeLiterals(DDLStatementParser.DateTimeLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 */
	void exitDateTimeLiterals(DDLStatementParser.DateTimeLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 */
	void enterHexadecimalLiterals(DDLStatementParser.HexadecimalLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 */
	void exitHexadecimalLiterals(DDLStatementParser.HexadecimalLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 */
	void enterBitValueLiterals(DDLStatementParser.BitValueLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 */
	void exitBitValueLiterals(DDLStatementParser.BitValueLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#booleanLiterals}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiterals(DDLStatementParser.BooleanLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#booleanLiterals}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiterals(DDLStatementParser.BooleanLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 */
	void enterNullValueLiterals(DDLStatementParser.NullValueLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 */
	void exitNullValueLiterals(DDLStatementParser.NullValueLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#characterSetName_}.
	 * @param ctx the parse tree
	 */
	void enterCharacterSetName_(DDLStatementParser.CharacterSetName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#characterSetName_}.
	 * @param ctx the parse tree
	 */
	void exitCharacterSetName_(DDLStatementParser.CharacterSetName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#collationName_}.
	 * @param ctx the parse tree
	 */
	void enterCollationName_(DDLStatementParser.CollationName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#collationName_}.
	 * @param ctx the parse tree
	 */
	void exitCollationName_(DDLStatementParser.CollationName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(DDLStatementParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(DDLStatementParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#unreservedWord}.
	 * @param ctx the parse tree
	 */
	void enterUnreservedWord(DDLStatementParser.UnreservedWordContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#unreservedWord}.
	 * @param ctx the parse tree
	 */
	void exitUnreservedWord(DDLStatementParser.UnreservedWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(DDLStatementParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(DDLStatementParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#schemaName}.
	 * @param ctx the parse tree
	 */
	void enterSchemaName(DDLStatementParser.SchemaNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#schemaName}.
	 * @param ctx the parse tree
	 */
	void exitSchemaName(DDLStatementParser.SchemaNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableName}.
	 * @param ctx the parse tree
	 */
	void enterTableName(DDLStatementParser.TableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableName}.
	 * @param ctx the parse tree
	 */
	void exitTableName(DDLStatementParser.TableNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#columnName}.
	 * @param ctx the parse tree
	 */
	void enterColumnName(DDLStatementParser.ColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#columnName}.
	 * @param ctx the parse tree
	 */
	void exitColumnName(DDLStatementParser.ColumnNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#indexName}.
	 * @param ctx the parse tree
	 */
	void enterIndexName(DDLStatementParser.IndexNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#indexName}.
	 * @param ctx the parse tree
	 */
	void exitIndexName(DDLStatementParser.IndexNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#userName}.
	 * @param ctx the parse tree
	 */
	void enterUserName(DDLStatementParser.UserNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#userName}.
	 * @param ctx the parse tree
	 */
	void exitUserName(DDLStatementParser.UserNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#eventName}.
	 * @param ctx the parse tree
	 */
	void enterEventName(DDLStatementParser.EventNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#eventName}.
	 * @param ctx the parse tree
	 */
	void exitEventName(DDLStatementParser.EventNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#serverName}.
	 * @param ctx the parse tree
	 */
	void enterServerName(DDLStatementParser.ServerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#serverName}.
	 * @param ctx the parse tree
	 */
	void exitServerName(DDLStatementParser.ServerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#wrapperName}.
	 * @param ctx the parse tree
	 */
	void enterWrapperName(DDLStatementParser.WrapperNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#wrapperName}.
	 * @param ctx the parse tree
	 */
	void exitWrapperName(DDLStatementParser.WrapperNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(DDLStatementParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(DDLStatementParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#viewName}.
	 * @param ctx the parse tree
	 */
	void enterViewName(DDLStatementParser.ViewNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#viewName}.
	 * @param ctx the parse tree
	 */
	void exitViewName(DDLStatementParser.ViewNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#owner}.
	 * @param ctx the parse tree
	 */
	void enterOwner(DDLStatementParser.OwnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#owner}.
	 * @param ctx the parse tree
	 */
	void exitOwner(DDLStatementParser.OwnerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(DDLStatementParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(DDLStatementParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableNames}.
	 * @param ctx the parse tree
	 */
	void enterTableNames(DDLStatementParser.TableNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableNames}.
	 * @param ctx the parse tree
	 */
	void exitTableNames(DDLStatementParser.TableNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#columnNames}.
	 * @param ctx the parse tree
	 */
	void enterColumnNames(DDLStatementParser.ColumnNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#columnNames}.
	 * @param ctx the parse tree
	 */
	void exitColumnNames(DDLStatementParser.ColumnNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#groupName}.
	 * @param ctx the parse tree
	 */
	void enterGroupName(DDLStatementParser.GroupNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#groupName}.
	 * @param ctx the parse tree
	 */
	void exitGroupName(DDLStatementParser.GroupNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#shardLibraryName}.
	 * @param ctx the parse tree
	 */
	void enterShardLibraryName(DDLStatementParser.ShardLibraryNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#shardLibraryName}.
	 * @param ctx the parse tree
	 */
	void exitShardLibraryName(DDLStatementParser.ShardLibraryNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#componentName}.
	 * @param ctx the parse tree
	 */
	void enterComponentName(DDLStatementParser.ComponentNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#componentName}.
	 * @param ctx the parse tree
	 */
	void exitComponentName(DDLStatementParser.ComponentNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#pluginName}.
	 * @param ctx the parse tree
	 */
	void enterPluginName(DDLStatementParser.PluginNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#pluginName}.
	 * @param ctx the parse tree
	 */
	void exitPluginName(DDLStatementParser.PluginNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#hostName}.
	 * @param ctx the parse tree
	 */
	void enterHostName(DDLStatementParser.HostNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#hostName}.
	 * @param ctx the parse tree
	 */
	void exitHostName(DDLStatementParser.HostNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#port}.
	 * @param ctx the parse tree
	 */
	void enterPort(DDLStatementParser.PortContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#port}.
	 * @param ctx the parse tree
	 */
	void exitPort(DDLStatementParser.PortContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#cloneInstance}.
	 * @param ctx the parse tree
	 */
	void enterCloneInstance(DDLStatementParser.CloneInstanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#cloneInstance}.
	 * @param ctx the parse tree
	 */
	void exitCloneInstance(DDLStatementParser.CloneInstanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#cloneDir}.
	 * @param ctx the parse tree
	 */
	void enterCloneDir(DDLStatementParser.CloneDirContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#cloneDir}.
	 * @param ctx the parse tree
	 */
	void exitCloneDir(DDLStatementParser.CloneDirContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#channelName}.
	 * @param ctx the parse tree
	 */
	void enterChannelName(DDLStatementParser.ChannelNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#channelName}.
	 * @param ctx the parse tree
	 */
	void exitChannelName(DDLStatementParser.ChannelNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#logName}.
	 * @param ctx the parse tree
	 */
	void enterLogName(DDLStatementParser.LogNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#logName}.
	 * @param ctx the parse tree
	 */
	void exitLogName(DDLStatementParser.LogNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#roleName}.
	 * @param ctx the parse tree
	 */
	void enterRoleName(DDLStatementParser.RoleNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#roleName}.
	 * @param ctx the parse tree
	 */
	void exitRoleName(DDLStatementParser.RoleNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#engineName}.
	 * @param ctx the parse tree
	 */
	void enterEngineName(DDLStatementParser.EngineNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#engineName}.
	 * @param ctx the parse tree
	 */
	void exitEngineName(DDLStatementParser.EngineNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#triggerName}.
	 * @param ctx the parse tree
	 */
	void enterTriggerName(DDLStatementParser.TriggerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#triggerName}.
	 * @param ctx the parse tree
	 */
	void exitTriggerName(DDLStatementParser.TriggerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#triggerTime}.
	 * @param ctx the parse tree
	 */
	void enterTriggerTime(DDLStatementParser.TriggerTimeContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#triggerTime}.
	 * @param ctx the parse tree
	 */
	void exitTriggerTime(DDLStatementParser.TriggerTimeContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#userOrRole}.
	 * @param ctx the parse tree
	 */
	void enterUserOrRole(DDLStatementParser.UserOrRoleContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#userOrRole}.
	 * @param ctx the parse tree
	 */
	void exitUserOrRole(DDLStatementParser.UserOrRoleContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionName}.
	 * @param ctx the parse tree
	 */
	void enterPartitionName(DDLStatementParser.PartitionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionName}.
	 * @param ctx the parse tree
	 */
	void exitPartitionName(DDLStatementParser.PartitionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#triggerEvent}.
	 * @param ctx the parse tree
	 */
	void enterTriggerEvent(DDLStatementParser.TriggerEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#triggerEvent}.
	 * @param ctx the parse tree
	 */
	void exitTriggerEvent(DDLStatementParser.TriggerEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#triggerOrder}.
	 * @param ctx the parse tree
	 */
	void enterTriggerOrder(DDLStatementParser.TriggerOrderContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#triggerOrder}.
	 * @param ctx the parse tree
	 */
	void exitTriggerOrder(DDLStatementParser.TriggerOrderContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(DDLStatementParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(DDLStatementParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperator(DDLStatementParser.LogicalOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperator(DDLStatementParser.LogicalOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#notOperator_}.
	 * @param ctx the parse tree
	 */
	void enterNotOperator_(DDLStatementParser.NotOperator_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#notOperator_}.
	 * @param ctx the parse tree
	 */
	void exitNotOperator_(DDLStatementParser.NotOperator_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#booleanPrimary}.
	 * @param ctx the parse tree
	 */
	void enterBooleanPrimary(DDLStatementParser.BooleanPrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#booleanPrimary}.
	 * @param ctx the parse tree
	 */
	void exitBooleanPrimary(DDLStatementParser.BooleanPrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void enterComparisonOperator(DDLStatementParser.ComparisonOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void exitComparisonOperator(DDLStatementParser.ComparisonOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(DDLStatementParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(DDLStatementParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#bitExpr}.
	 * @param ctx the parse tree
	 */
	void enterBitExpr(DDLStatementParser.BitExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#bitExpr}.
	 * @param ctx the parse tree
	 */
	void exitBitExpr(DDLStatementParser.BitExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpr(DDLStatementParser.SimpleExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpr(DDLStatementParser.SimpleExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(DDLStatementParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(DDLStatementParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunction(DDLStatementParser.AggregationFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunction(DDLStatementParser.AggregationFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunctionName(DDLStatementParser.AggregationFunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunctionName(DDLStatementParser.AggregationFunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#distinct}.
	 * @param ctx the parse tree
	 */
	void enterDistinct(DDLStatementParser.DistinctContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#distinct}.
	 * @param ctx the parse tree
	 */
	void exitDistinct(DDLStatementParser.DistinctContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#overClause_}.
	 * @param ctx the parse tree
	 */
	void enterOverClause_(DDLStatementParser.OverClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#overClause_}.
	 * @param ctx the parse tree
	 */
	void exitOverClause_(DDLStatementParser.OverClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#windowSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterWindowSpecification_(DDLStatementParser.WindowSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#windowSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitWindowSpecification_(DDLStatementParser.WindowSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionClause_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionClause_(DDLStatementParser.PartitionClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionClause_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionClause_(DDLStatementParser.PartitionClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#frameClause_}.
	 * @param ctx the parse tree
	 */
	void enterFrameClause_(DDLStatementParser.FrameClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#frameClause_}.
	 * @param ctx the parse tree
	 */
	void exitFrameClause_(DDLStatementParser.FrameClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#frameStart_}.
	 * @param ctx the parse tree
	 */
	void enterFrameStart_(DDLStatementParser.FrameStart_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#frameStart_}.
	 * @param ctx the parse tree
	 */
	void exitFrameStart_(DDLStatementParser.FrameStart_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#frameEnd_}.
	 * @param ctx the parse tree
	 */
	void enterFrameEnd_(DDLStatementParser.FrameEnd_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#frameEnd_}.
	 * @param ctx the parse tree
	 */
	void exitFrameEnd_(DDLStatementParser.FrameEnd_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#frameBetween_}.
	 * @param ctx the parse tree
	 */
	void enterFrameBetween_(DDLStatementParser.FrameBetween_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#frameBetween_}.
	 * @param ctx the parse tree
	 */
	void exitFrameBetween_(DDLStatementParser.FrameBetween_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#specialFunction}.
	 * @param ctx the parse tree
	 */
	void enterSpecialFunction(DDLStatementParser.SpecialFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#specialFunction}.
	 * @param ctx the parse tree
	 */
	void exitSpecialFunction(DDLStatementParser.SpecialFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 */
	void enterGroupConcatFunction(DDLStatementParser.GroupConcatFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 */
	void exitGroupConcatFunction(DDLStatementParser.GroupConcatFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#windowFunction}.
	 * @param ctx the parse tree
	 */
	void enterWindowFunction(DDLStatementParser.WindowFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#windowFunction}.
	 * @param ctx the parse tree
	 */
	void exitWindowFunction(DDLStatementParser.WindowFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#castFunction}.
	 * @param ctx the parse tree
	 */
	void enterCastFunction(DDLStatementParser.CastFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#castFunction}.
	 * @param ctx the parse tree
	 */
	void exitCastFunction(DDLStatementParser.CastFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#convertFunction}.
	 * @param ctx the parse tree
	 */
	void enterConvertFunction(DDLStatementParser.ConvertFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#convertFunction}.
	 * @param ctx the parse tree
	 */
	void exitConvertFunction(DDLStatementParser.ConvertFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#positionFunction}.
	 * @param ctx the parse tree
	 */
	void enterPositionFunction(DDLStatementParser.PositionFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#positionFunction}.
	 * @param ctx the parse tree
	 */
	void exitPositionFunction(DDLStatementParser.PositionFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#substringFunction}.
	 * @param ctx the parse tree
	 */
	void enterSubstringFunction(DDLStatementParser.SubstringFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#substringFunction}.
	 * @param ctx the parse tree
	 */
	void exitSubstringFunction(DDLStatementParser.SubstringFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#extractFunction}.
	 * @param ctx the parse tree
	 */
	void enterExtractFunction(DDLStatementParser.ExtractFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#extractFunction}.
	 * @param ctx the parse tree
	 */
	void exitExtractFunction(DDLStatementParser.ExtractFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#charFunction}.
	 * @param ctx the parse tree
	 */
	void enterCharFunction(DDLStatementParser.CharFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#charFunction}.
	 * @param ctx the parse tree
	 */
	void exitCharFunction(DDLStatementParser.CharFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#trimFunction_}.
	 * @param ctx the parse tree
	 */
	void enterTrimFunction_(DDLStatementParser.TrimFunction_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#trimFunction_}.
	 * @param ctx the parse tree
	 */
	void exitTrimFunction_(DDLStatementParser.TrimFunction_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#valuesFunction_}.
	 * @param ctx the parse tree
	 */
	void enterValuesFunction_(DDLStatementParser.ValuesFunction_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#valuesFunction_}.
	 * @param ctx the parse tree
	 */
	void exitValuesFunction_(DDLStatementParser.ValuesFunction_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#weightStringFunction}.
	 * @param ctx the parse tree
	 */
	void enterWeightStringFunction(DDLStatementParser.WeightStringFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#weightStringFunction}.
	 * @param ctx the parse tree
	 */
	void exitWeightStringFunction(DDLStatementParser.WeightStringFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#levelClause_}.
	 * @param ctx the parse tree
	 */
	void enterLevelClause_(DDLStatementParser.LevelClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#levelClause_}.
	 * @param ctx the parse tree
	 */
	void exitLevelClause_(DDLStatementParser.LevelClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 */
	void enterLevelInWeightListElement_(DDLStatementParser.LevelInWeightListElement_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 */
	void exitLevelInWeightListElement_(DDLStatementParser.LevelInWeightListElement_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#regularFunction}.
	 * @param ctx the parse tree
	 */
	void enterRegularFunction(DDLStatementParser.RegularFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#regularFunction}.
	 * @param ctx the parse tree
	 */
	void exitRegularFunction(DDLStatementParser.RegularFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 */
	void enterRegularFunctionName_(DDLStatementParser.RegularFunctionName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 */
	void exitRegularFunctionName_(DDLStatementParser.RegularFunctionName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#matchExpression_}.
	 * @param ctx the parse tree
	 */
	void enterMatchExpression_(DDLStatementParser.MatchExpression_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#matchExpression_}.
	 * @param ctx the parse tree
	 */
	void exitMatchExpression_(DDLStatementParser.MatchExpression_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 */
	void enterMatchSearchModifier_(DDLStatementParser.MatchSearchModifier_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 */
	void exitMatchSearchModifier_(DDLStatementParser.MatchSearchModifier_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#caseExpression}.
	 * @param ctx the parse tree
	 */
	void enterCaseExpression(DDLStatementParser.CaseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#caseExpression}.
	 * @param ctx the parse tree
	 */
	void exitCaseExpression(DDLStatementParser.CaseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#caseWhen_}.
	 * @param ctx the parse tree
	 */
	void enterCaseWhen_(DDLStatementParser.CaseWhen_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#caseWhen_}.
	 * @param ctx the parse tree
	 */
	void exitCaseWhen_(DDLStatementParser.CaseWhen_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#caseElse_}.
	 * @param ctx the parse tree
	 */
	void enterCaseElse_(DDLStatementParser.CaseElse_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#caseElse_}.
	 * @param ctx the parse tree
	 */
	void exitCaseElse_(DDLStatementParser.CaseElse_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#intervalExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntervalExpression(DDLStatementParser.IntervalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#intervalExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntervalExpression(DDLStatementParser.IntervalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#intervalUnit_}.
	 * @param ctx the parse tree
	 */
	void enterIntervalUnit_(DDLStatementParser.IntervalUnit_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#intervalUnit_}.
	 * @param ctx the parse tree
	 */
	void exitIntervalUnit_(DDLStatementParser.IntervalUnit_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#subquery}.
	 * @param ctx the parse tree
	 */
	void enterSubquery(DDLStatementParser.SubqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#subquery}.
	 * @param ctx the parse tree
	 */
	void exitSubquery(DDLStatementParser.SubqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void enterOrderByClause(DDLStatementParser.OrderByClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void exitOrderByClause(DDLStatementParser.OrderByClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void enterOrderByItem(DDLStatementParser.OrderByItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void exitOrderByItem(DDLStatementParser.OrderByItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterDataType(DDLStatementParser.DataTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitDataType(DDLStatementParser.DataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dataTypeName}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeName(DDLStatementParser.DataTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dataTypeName}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeName(DDLStatementParser.DataTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#dataTypeLength}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeLength(DDLStatementParser.DataTypeLengthContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#dataTypeLength}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeLength(DDLStatementParser.DataTypeLengthContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#collectionOptions}.
	 * @param ctx the parse tree
	 */
	void enterCollectionOptions(DDLStatementParser.CollectionOptionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#collectionOptions}.
	 * @param ctx the parse tree
	 */
	void exitCollectionOptions(DDLStatementParser.CollectionOptionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#characterSet_}.
	 * @param ctx the parse tree
	 */
	void enterCharacterSet_(DDLStatementParser.CharacterSet_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#characterSet_}.
	 * @param ctx the parse tree
	 */
	void exitCharacterSet_(DDLStatementParser.CharacterSet_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#collateClause_}.
	 * @param ctx the parse tree
	 */
	void enterCollateClause_(DDLStatementParser.CollateClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#collateClause_}.
	 * @param ctx the parse tree
	 */
	void exitCollateClause_(DDLStatementParser.CollateClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 */
	void enterIgnoredIdentifier_(DDLStatementParser.IgnoredIdentifier_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 */
	void exitIgnoredIdentifier_(DDLStatementParser.IgnoredIdentifier_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 */
	void enterIgnoredIdentifiers_(DDLStatementParser.IgnoredIdentifiers_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 */
	void exitIgnoredIdentifiers_(DDLStatementParser.IgnoredIdentifiers_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#insert}.
	 * @param ctx the parse tree
	 */
	void enterInsert(DDLStatementParser.InsertContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#insert}.
	 * @param ctx the parse tree
	 */
	void exitInsert(DDLStatementParser.InsertContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#insertSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterInsertSpecification_(DDLStatementParser.InsertSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#insertSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitInsertSpecification_(DDLStatementParser.InsertSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#insertValuesClause}.
	 * @param ctx the parse tree
	 */
	void enterInsertValuesClause(DDLStatementParser.InsertValuesClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#insertValuesClause}.
	 * @param ctx the parse tree
	 */
	void exitInsertValuesClause(DDLStatementParser.InsertValuesClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#insertSelectClause}.
	 * @param ctx the parse tree
	 */
	void enterInsertSelectClause(DDLStatementParser.InsertSelectClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#insertSelectClause}.
	 * @param ctx the parse tree
	 */
	void exitInsertSelectClause(DDLStatementParser.InsertSelectClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#onDuplicateKeyClause}.
	 * @param ctx the parse tree
	 */
	void enterOnDuplicateKeyClause(DDLStatementParser.OnDuplicateKeyClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#onDuplicateKeyClause}.
	 * @param ctx the parse tree
	 */
	void exitOnDuplicateKeyClause(DDLStatementParser.OnDuplicateKeyClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#replace}.
	 * @param ctx the parse tree
	 */
	void enterReplace(DDLStatementParser.ReplaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#replace}.
	 * @param ctx the parse tree
	 */
	void exitReplace(DDLStatementParser.ReplaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#replaceSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterReplaceSpecification_(DDLStatementParser.ReplaceSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#replaceSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitReplaceSpecification_(DDLStatementParser.ReplaceSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#update}.
	 * @param ctx the parse tree
	 */
	void enterUpdate(DDLStatementParser.UpdateContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#update}.
	 * @param ctx the parse tree
	 */
	void exitUpdate(DDLStatementParser.UpdateContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#updateSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterUpdateSpecification_(DDLStatementParser.UpdateSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#updateSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitUpdateSpecification_(DDLStatementParser.UpdateSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(DDLStatementParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(DDLStatementParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#setAssignmentsClause}.
	 * @param ctx the parse tree
	 */
	void enterSetAssignmentsClause(DDLStatementParser.SetAssignmentsClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#setAssignmentsClause}.
	 * @param ctx the parse tree
	 */
	void exitSetAssignmentsClause(DDLStatementParser.SetAssignmentsClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#assignmentValues}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentValues(DDLStatementParser.AssignmentValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#assignmentValues}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentValues(DDLStatementParser.AssignmentValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#assignmentValue}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentValue(DDLStatementParser.AssignmentValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#assignmentValue}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentValue(DDLStatementParser.AssignmentValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#blobValue}.
	 * @param ctx the parse tree
	 */
	void enterBlobValue(DDLStatementParser.BlobValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#blobValue}.
	 * @param ctx the parse tree
	 */
	void exitBlobValue(DDLStatementParser.BlobValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#delete}.
	 * @param ctx the parse tree
	 */
	void enterDelete(DDLStatementParser.DeleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#delete}.
	 * @param ctx the parse tree
	 */
	void exitDelete(DDLStatementParser.DeleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#deleteSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterDeleteSpecification_(DDLStatementParser.DeleteSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#deleteSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitDeleteSpecification_(DDLStatementParser.DeleteSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#singleTableClause}.
	 * @param ctx the parse tree
	 */
	void enterSingleTableClause(DDLStatementParser.SingleTableClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#singleTableClause}.
	 * @param ctx the parse tree
	 */
	void exitSingleTableClause(DDLStatementParser.SingleTableClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#multipleTablesClause}.
	 * @param ctx the parse tree
	 */
	void enterMultipleTablesClause(DDLStatementParser.MultipleTablesClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#multipleTablesClause}.
	 * @param ctx the parse tree
	 */
	void exitMultipleTablesClause(DDLStatementParser.MultipleTablesClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#multipleTableNames}.
	 * @param ctx the parse tree
	 */
	void enterMultipleTableNames(DDLStatementParser.MultipleTableNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#multipleTableNames}.
	 * @param ctx the parse tree
	 */
	void exitMultipleTableNames(DDLStatementParser.MultipleTableNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#select}.
	 * @param ctx the parse tree
	 */
	void enterSelect(DDLStatementParser.SelectContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#select}.
	 * @param ctx the parse tree
	 */
	void exitSelect(DDLStatementParser.SelectContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#call}.
	 * @param ctx the parse tree
	 */
	void enterCall(DDLStatementParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#call}.
	 * @param ctx the parse tree
	 */
	void exitCall(DDLStatementParser.CallContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#doStatement}.
	 * @param ctx the parse tree
	 */
	void enterDoStatement(DDLStatementParser.DoStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#doStatement}.
	 * @param ctx the parse tree
	 */
	void exitDoStatement(DDLStatementParser.DoStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#handlerStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerStatement(DDLStatementParser.HandlerStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#handlerStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerStatement(DDLStatementParser.HandlerStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerOpenStatement(DDLStatementParser.HandlerOpenStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerOpenStatement(DDLStatementParser.HandlerOpenStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerReadIndexStatement(DDLStatementParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerReadIndexStatement(DDLStatementParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerReadStatement(DDLStatementParser.HandlerReadStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerReadStatement(DDLStatementParser.HandlerReadStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerCloseStatement(DDLStatementParser.HandlerCloseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerCloseStatement(DDLStatementParser.HandlerCloseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void enterImportStatement(DDLStatementParser.ImportStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void exitImportStatement(DDLStatementParser.ImportStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#loadDataStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoadDataStatement(DDLStatementParser.LoadDataStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#loadDataStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoadDataStatement(DDLStatementParser.LoadDataStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoadXmlStatement(DDLStatementParser.LoadXmlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoadXmlStatement(DDLStatementParser.LoadXmlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#withClause_}.
	 * @param ctx the parse tree
	 */
	void enterWithClause_(DDLStatementParser.WithClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#withClause_}.
	 * @param ctx the parse tree
	 */
	void exitWithClause_(DDLStatementParser.WithClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#cteClause_}.
	 * @param ctx the parse tree
	 */
	void enterCteClause_(DDLStatementParser.CteClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#cteClause_}.
	 * @param ctx the parse tree
	 */
	void exitCteClause_(DDLStatementParser.CteClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#unionClause}.
	 * @param ctx the parse tree
	 */
	void enterUnionClause(DDLStatementParser.UnionClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#unionClause}.
	 * @param ctx the parse tree
	 */
	void exitUnionClause(DDLStatementParser.UnionClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void enterSelectClause(DDLStatementParser.SelectClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void exitSelectClause(DDLStatementParser.SelectClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#selectSpecification}.
	 * @param ctx the parse tree
	 */
	void enterSelectSpecification(DDLStatementParser.SelectSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#selectSpecification}.
	 * @param ctx the parse tree
	 */
	void exitSelectSpecification(DDLStatementParser.SelectSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#duplicateSpecification}.
	 * @param ctx the parse tree
	 */
	void enterDuplicateSpecification(DDLStatementParser.DuplicateSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#duplicateSpecification}.
	 * @param ctx the parse tree
	 */
	void exitDuplicateSpecification(DDLStatementParser.DuplicateSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#projections}.
	 * @param ctx the parse tree
	 */
	void enterProjections(DDLStatementParser.ProjectionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#projections}.
	 * @param ctx the parse tree
	 */
	void exitProjections(DDLStatementParser.ProjectionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#projection}.
	 * @param ctx the parse tree
	 */
	void enterProjection(DDLStatementParser.ProjectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#projection}.
	 * @param ctx the parse tree
	 */
	void exitProjection(DDLStatementParser.ProjectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#alias}.
	 * @param ctx the parse tree
	 */
	void enterAlias(DDLStatementParser.AliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#alias}.
	 * @param ctx the parse tree
	 */
	void exitAlias(DDLStatementParser.AliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#unqualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void enterUnqualifiedShorthand(DDLStatementParser.UnqualifiedShorthandContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#unqualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void exitUnqualifiedShorthand(DDLStatementParser.UnqualifiedShorthandContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#qualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedShorthand(DDLStatementParser.QualifiedShorthandContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#qualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedShorthand(DDLStatementParser.QualifiedShorthandContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void enterFromClause(DDLStatementParser.FromClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void exitFromClause(DDLStatementParser.FromClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableReferences}.
	 * @param ctx the parse tree
	 */
	void enterTableReferences(DDLStatementParser.TableReferencesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableReferences}.
	 * @param ctx the parse tree
	 */
	void exitTableReferences(DDLStatementParser.TableReferencesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#escapedTableReference}.
	 * @param ctx the parse tree
	 */
	void enterEscapedTableReference(DDLStatementParser.EscapedTableReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#escapedTableReference}.
	 * @param ctx the parse tree
	 */
	void exitEscapedTableReference(DDLStatementParser.EscapedTableReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableReference}.
	 * @param ctx the parse tree
	 */
	void enterTableReference(DDLStatementParser.TableReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableReference}.
	 * @param ctx the parse tree
	 */
	void exitTableReference(DDLStatementParser.TableReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#tableFactor}.
	 * @param ctx the parse tree
	 */
	void enterTableFactor(DDLStatementParser.TableFactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#tableFactor}.
	 * @param ctx the parse tree
	 */
	void exitTableFactor(DDLStatementParser.TableFactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#partitionNames_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionNames_(DDLStatementParser.PartitionNames_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#partitionNames_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionNames_(DDLStatementParser.PartitionNames_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#indexHintList_}.
	 * @param ctx the parse tree
	 */
	void enterIndexHintList_(DDLStatementParser.IndexHintList_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#indexHintList_}.
	 * @param ctx the parse tree
	 */
	void exitIndexHintList_(DDLStatementParser.IndexHintList_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#indexHint_}.
	 * @param ctx the parse tree
	 */
	void enterIndexHint_(DDLStatementParser.IndexHint_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#indexHint_}.
	 * @param ctx the parse tree
	 */
	void exitIndexHint_(DDLStatementParser.IndexHint_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#joinedTable}.
	 * @param ctx the parse tree
	 */
	void enterJoinedTable(DDLStatementParser.JoinedTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#joinedTable}.
	 * @param ctx the parse tree
	 */
	void exitJoinedTable(DDLStatementParser.JoinedTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#joinSpecification}.
	 * @param ctx the parse tree
	 */
	void enterJoinSpecification(DDLStatementParser.JoinSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#joinSpecification}.
	 * @param ctx the parse tree
	 */
	void exitJoinSpecification(DDLStatementParser.JoinSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void enterWhereClause(DDLStatementParser.WhereClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void exitWhereClause(DDLStatementParser.WhereClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#groupByClause}.
	 * @param ctx the parse tree
	 */
	void enterGroupByClause(DDLStatementParser.GroupByClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#groupByClause}.
	 * @param ctx the parse tree
	 */
	void exitGroupByClause(DDLStatementParser.GroupByClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#havingClause}.
	 * @param ctx the parse tree
	 */
	void enterHavingClause(DDLStatementParser.HavingClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#havingClause}.
	 * @param ctx the parse tree
	 */
	void exitHavingClause(DDLStatementParser.HavingClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#limitClause}.
	 * @param ctx the parse tree
	 */
	void enterLimitClause(DDLStatementParser.LimitClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#limitClause}.
	 * @param ctx the parse tree
	 */
	void exitLimitClause(DDLStatementParser.LimitClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#limitRowCount}.
	 * @param ctx the parse tree
	 */
	void enterLimitRowCount(DDLStatementParser.LimitRowCountContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#limitRowCount}.
	 * @param ctx the parse tree
	 */
	void exitLimitRowCount(DDLStatementParser.LimitRowCountContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#limitOffset}.
	 * @param ctx the parse tree
	 */
	void enterLimitOffset(DDLStatementParser.LimitOffsetContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#limitOffset}.
	 * @param ctx the parse tree
	 */
	void exitLimitOffset(DDLStatementParser.LimitOffsetContext ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#windowClause_}.
	 * @param ctx the parse tree
	 */
	void enterWindowClause_(DDLStatementParser.WindowClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#windowClause_}.
	 * @param ctx the parse tree
	 */
	void exitWindowClause_(DDLStatementParser.WindowClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#windowItem_}.
	 * @param ctx the parse tree
	 */
	void enterWindowItem_(DDLStatementParser.WindowItem_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#windowItem_}.
	 * @param ctx the parse tree
	 */
	void exitWindowItem_(DDLStatementParser.WindowItem_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#selectLinesInto_}.
	 * @param ctx the parse tree
	 */
	void enterSelectLinesInto_(DDLStatementParser.SelectLinesInto_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#selectLinesInto_}.
	 * @param ctx the parse tree
	 */
	void exitSelectLinesInto_(DDLStatementParser.SelectLinesInto_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#selectFieldsInto_}.
	 * @param ctx the parse tree
	 */
	void enterSelectFieldsInto_(DDLStatementParser.SelectFieldsInto_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#selectFieldsInto_}.
	 * @param ctx the parse tree
	 */
	void exitSelectFieldsInto_(DDLStatementParser.SelectFieldsInto_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#selectIntoExpression_}.
	 * @param ctx the parse tree
	 */
	void enterSelectIntoExpression_(DDLStatementParser.SelectIntoExpression_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#selectIntoExpression_}.
	 * @param ctx the parse tree
	 */
	void exitSelectIntoExpression_(DDLStatementParser.SelectIntoExpression_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DDLStatementParser#lockClause}.
	 * @param ctx the parse tree
	 */
	void enterLockClause(DDLStatementParser.LockClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DDLStatementParser#lockClause}.
	 * @param ctx the parse tree
	 */
	void exitLockClause(DDLStatementParser.LockClauseContext ctx);
}