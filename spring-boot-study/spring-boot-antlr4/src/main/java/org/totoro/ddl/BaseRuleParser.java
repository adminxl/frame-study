package org.totoro.ddl;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/BaseRule.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BaseRuleParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, AND_=2, OR_=3, NOT_=4, TILDE_=5, VERTICAL_BAR_=6, AMPERSAND_=7, 
		SIGNED_LEFT_SHIFT_=8, SIGNED_RIGHT_SHIFT_=9, CARET_=10, MOD_=11, COLON_=12, 
		PLUS_=13, MINUS_=14, ASTERISK_=15, SLASH_=16, BACKSLASH_=17, DOT_=18, 
		DOT_ASTERISK_=19, SAFE_EQ_=20, DEQ_=21, EQ_=22, NEQ_=23, GT_=24, GTE_=25, 
		LT_=26, LTE_=27, POUND_=28, LP_=29, RP_=30, LBE_=31, RBE_=32, LBT_=33, 
		RBT_=34, COMMA_=35, DQ_=36, SQ_=37, BQ_=38, QUESTION_=39, AT_=40, SEMI_=41, 
		WS=42, SELECT=43, INSERT=44, UPDATE=45, DELETE=46, CREATE=47, ALTER=48, 
		DROP=49, TRUNCATE=50, SCHEMA=51, GRANT=52, REVOKE=53, ADD=54, SET=55, 
		TABLE=56, COLUMN=57, INDEX=58, CONSTRAINT=59, PRIMARY=60, UNIQUE=61, FOREIGN=62, 
		KEY=63, POSITION=64, PRECISION=65, FUNCTION=66, TRIGGER=67, PROCEDURE=68, 
		VIEW=69, INTO=70, VALUES=71, WITH=72, UNION=73, DISTINCT=74, CASE=75, 
		WHEN=76, CAST=77, TRIM=78, SUBSTRING=79, FROM=80, NATURAL=81, JOIN=82, 
		FULL=83, INNER=84, OUTER=85, LEFT=86, RIGHT=87, CROSS=88, USING=89, WHERE=90, 
		AS=91, ON=92, IF=93, ELSE=94, THEN=95, FOR=96, TO=97, AND=98, OR=99, IS=100, 
		NOT=101, NULL=102, TRUE=103, FALSE=104, EXISTS=105, BETWEEN=106, IN=107, 
		ALL=108, ANY=109, LIKE=110, ORDER=111, GROUP=112, BY=113, ASC=114, DESC=115, 
		HAVING=116, LIMIT=117, OFFSET=118, BEGIN=119, COMMIT=120, ROLLBACK=121, 
		SAVEPOINT=122, BOOLEAN=123, DOUBLE=124, CHAR=125, CHARACTER=126, ARRAY=127, 
		INTERVAL=128, DATE=129, TIME=130, TIMESTAMP=131, LOCALTIME=132, LOCALTIMESTAMP=133, 
		YEAR=134, QUARTER=135, MONTH=136, WEEK=137, DAY=138, HOUR=139, MINUTE=140, 
		SECOND=141, MICROSECOND=142, MAX=143, MIN=144, SUM=145, COUNT=146, AVG=147, 
		DEFAULT=148, CURRENT=149, ENABLE=150, DISABLE=151, CALL=152, INSTANCE=153, 
		PRESERVE=154, DO=155, DEFINER=156, CURRENT_USER=157, SQL=158, CASCADED=159, 
		LOCAL=160, CLOSE=161, OPEN=162, NEXT=163, NAME=164, COLLATION=165, NAMES=166, 
		INTEGER=167, REAL=168, DECIMAL=169, TYPE=170, INT=171, SMALLINT=172, TINYINT=173, 
		MEDIUMINT=174, BIGINT=175, NUMERIC=176, FLOAT=177, DATETIME=178, FOR_GENERATOR=179, 
		USE=180, DESCRIBE=181, SHOW=182, DATABASES=183, DATABASE=184, SCHEMAS=185, 
		TABLES=186, TABLESPACE=187, COLUMNS=188, FIELDS=189, INDEXES=190, STATUS=191, 
		REPLACE=192, MODIFY=193, DISTINCTROW=194, VALUE=195, DUPLICATE=196, FIRST=197, 
		LAST=198, AFTER=199, OJ=200, WINDOW=201, MOD=202, DIV=203, XOR=204, REGEXP=205, 
		RLIKE=206, ACCOUNT=207, USER=208, ROLE=209, START=210, TRANSACTION=211, 
		ROW=212, ROWS=213, WITHOUT=214, BINARY=215, ESCAPE=216, GENERATED=217, 
		PARTITION=218, SUBPARTITION=219, STORAGE=220, STORED=221, SUPER=222, SUBSTR=223, 
		TEMPORARY=224, THAN=225, TRAILING=226, UNBOUNDED=227, UNLOCK=228, UNSIGNED=229, 
		SIGNED=230, UPGRADE=231, USAGE=232, VALIDATION=233, VIRTUAL=234, ROLLUP=235, 
		SOUNDS=236, UNKNOWN=237, OFF=238, ALWAYS=239, CASCADE=240, CHECK=241, 
		COMMITTED=242, LEVEL=243, NO=244, OPTION=245, PASSWORD=246, PRIVILEGES=247, 
		READ=248, WRITE=249, REFERENCES=250, ACTION=251, ALGORITHM=252, ANALYZE=253, 
		AUTOCOMMIT=254, MAXVALUE=255, BOTH=256, BTREE=257, CHAIN=258, CHANGE=259, 
		CHARSET=260, CHECKSUM=261, CIPHER=262, CLIENT=263, COALESCE=264, COLLATE=265, 
		COMMENT=266, COMPACT=267, COMPRESSED=268, COMPRESSION=269, CONNECTION=270, 
		CONSISTENT=271, CONVERT=272, COPY=273, DATA=274, DELAYED=275, DIRECTORY=276, 
		DISCARD=277, DISK=278, DYNAMIC=279, ENCRYPTION=280, END=281, ENGINE=282, 
		EVENT=283, EXCEPT=284, EXCHANGE=285, EXCLUSIVE=286, EXECUTE=287, EXTRACT=288, 
		FILE=289, FIXED=290, FOLLOWING=291, FORCE=292, FULLTEXT=293, GLOBAL=294, 
		HASH=295, IDENTIFIED=296, IGNORE=297, IMPORT_=298, INPLACE=299, KEYS=300, 
		LEADING=301, LESS=302, LINEAR=303, LOCK=304, MATCH=305, MEMORY=306, NONE=307, 
		NOW=308, OFFLINE=309, ONLINE=310, OPTIMIZE=311, OVER=312, PARSER=313, 
		PARTIAL=314, PARTITIONING=315, PERSIST=316, PRECEDING=317, PROCESS=318, 
		PROXY=319, QUICK=320, RANGE=321, REBUILD=322, RECURSIVE=323, REDUNDANT=324, 
		RELEASE=325, RELOAD=326, REMOVE=327, RENAME=328, REORGANIZE=329, REPAIR=330, 
		REPLICATION=331, REQUIRE=332, RESTRICT=333, REVERSE=334, ROUTINE=335, 
		SEPARATOR=336, SESSION=337, SHARED=338, SHUTDOWN=339, SIMPLE=340, SLAVE=341, 
		SPATIAL=342, ZEROFILL=343, VISIBLE=344, INVISIBLE=345, INSTANT=346, ENFORCED=347, 
		AGAINST=348, LANGUAGE=349, MODE=350, QUERY=351, EXTENDED=352, EXPANSION=353, 
		VARIANCE=354, MAX_ROWS=355, MIN_ROWS=356, HIGH_PRIORITY=357, LOW_PRIORITY=358, 
		SQL_BIG_RESULT=359, SQL_BUFFER_RESULT=360, SQL_CACHE=361, SQL_CALC_FOUND_ROWS=362, 
		SQL_NO_CACHE=363, SQL_SMALL_RESULT=364, STATS_AUTO_RECALC=365, STATS_PERSISTENT=366, 
		STATS_SAMPLE_PAGES=367, ROLE_ADMIN=368, ROW_FORMAT=369, SET_USER_ID=370, 
		REPLICATION_SLAVE_ADMIN=371, GROUP_REPLICATION_ADMIN=372, STRAIGHT_JOIN=373, 
		WEIGHT_STRING=374, COLUMN_FORMAT=375, CONNECTION_ADMIN=376, FIREWALL_ADMIN=377, 
		FIREWALL_USER=378, INSERT_METHOD=379, KEY_BLOCK_SIZE=380, PACK_KEYS=381, 
		PERSIST_ONLY=382, BIT_AND=383, BIT_OR=384, BIT_XOR=385, GROUP_CONCAT=386, 
		JSON_ARRAYAGG=387, JSON_OBJECTAGG=388, STD=389, STDDEV=390, STDDEV_POP=391, 
		STDDEV_SAMP=392, VAR_POP=393, VAR_SAMP=394, AUDIT_ADMIN=395, AUTO_INCREMENT=396, 
		AVG_ROW_LENGTH=397, BINLOG_ADMIN=398, DELAY_KEY_WRITE=399, ENCRYPTION_KEY_ADMIN=400, 
		SYSTEM_VARIABLES_ADMIN=401, VERSION_TOKEN_ADMIN=402, CURRENT_TIMESTAMP=403, 
		YEAR_MONTH=404, DAY_HOUR=405, DAY_MINUTE=406, DAY_SECOND=407, DAY_MICROSECOND=408, 
		HOUR_MINUTE=409, HOUR_SECOND=410, HOUR_MICROSECOND=411, MINUTE_SECOND=412, 
		MINUTE_MICROSECOND=413, SECOND_MICROSECOND=414, UL_BINARY=415, ROTATE=416, 
		MASTER=417, BINLOG=418, ERROR=419, SCHEDULE=420, COMPLETION=421, EVERY=422, 
		STARTS=423, ENDS=424, HOST=425, SOCKET=426, PORT=427, SERVER=428, WRAPPER=429, 
		OPTIONS=430, OWNER=431, DETERMINISTIC=432, RETURNS=433, CONTAINS=434, 
		READS=435, MODIFIES=436, SECURITY=437, INVOKER=438, OUT=439, INOUT=440, 
		TEMPTABLE=441, MERGE=442, UNDEFINED=443, DATAFILE=444, FILE_BLOCK_SIZE=445, 
		EXTENT_SIZE=446, INITIAL_SIZE=447, AUTOEXTEND_SIZE=448, MAX_SIZE=449, 
		NODEGROUP=450, WAIT=451, LOGFILE=452, UNDOFILE=453, UNDO_BUFFER_SIZE=454, 
		REDO_BUFFER_SIZE=455, HANDLER=456, PREV=457, ORGANIZATION=458, DEFINITION=459, 
		DESCRIPTION=460, REFERENCE=461, FOLLOWS=462, PRECEDES=463, IMPORT=464, 
		LOAD=465, CONCURRENT=466, INFILE=467, LINES=468, STARTING=469, TERMINATED=470, 
		OPTIONALLY=471, ENCLOSED=472, ESCAPED=473, XML=474, UNDO=475, DUMPFILE=476, 
		OUTFILE=477, SHARE=478, LOGS=479, EVENTS=480, BEFORE=481, EACH=482, MUTEX=483, 
		ENGINES=484, ERRORS=485, CODE=486, GRANTS=487, PLUGINS=488, PROCESSLIST=489, 
		BLOCK=490, IO=491, CONTEXT=492, SWITCHES=493, CPU=494, IPC=495, PAGE=496, 
		FAULTS=497, SOURCE=498, SWAPS=499, PROFILE=500, PROFILES=501, RELAYLOG=502, 
		CHANNEL=503, VARIABLES=504, WARNINGS=505, SSL=506, CLONE=507, AGGREGATE=508, 
		STRING=509, SONAME=510, INSTALL=511, COMPONENT=512, PLUGIN=513, UNINSTALL=514, 
		NO_WRITE_TO_BINLOG=515, HISTOGRAM=516, BUCKETS=517, FAST=518, MEDIUM=519, 
		USE_FRM=520, RESOURCE=521, VCPU=522, THREAD_PRIORITY=523, SYSTEM=524, 
		EXPIRE=525, NEVER=526, HISTORY=527, OPTIONAL=528, REUSE=529, MAX_QUERIES_PER_HOUR=530, 
		MAX_UPDATES_PER_HOUR=531, MAX_CONNECTIONS_PER_HOUR=532, MAX_USER_CONNECTIONS=533, 
		RETAIN=534, RANDOM=535, OLD=536, X509=537, ISSUER=538, SUBJECT=539, CACHE=540, 
		GENERAL=541, OPTIMIZER_COSTS=542, SLOW=543, USER_RESOURCES=544, EXPORT=545, 
		RELAY=546, HOSTS=547, KILL=548, FLUSH=549, RESET=550, RESTART=551, UNIX_TIMESTAMP=552, 
		LOWER=553, UPPER=554, ADDDATE=555, ADDTIME=556, DATE_ADD=557, DATE_SUB=558, 
		DATEDIFF=559, DATE_FORMAT=560, DAYNAME=561, DAYOFMONTH=562, DAYOFWEEK=563, 
		DAYOFYEAR=564, STR_TO_DATE=565, TIMEDIFF=566, TIMESTAMPADD=567, TIMESTAMPDIFF=568, 
		TIME_FORMAT=569, TIME_TO_SEC=570, AES_DECRYPT=571, AES_ENCRYPT=572, FROM_BASE64=573, 
		TO_BASE64=574, GEOMCOLLECTION=575, GEOMETRYCOLLECTION=576, LINESTRING=577, 
		MULTILINESTRING=578, MULTIPOINT=579, MULTIPOLYGON=580, POINT=581, POLYGON=582, 
		ST_AREA=583, ST_ASBINARY=584, ST_ASGEOJSON=585, ST_ASTEXT=586, ST_ASWKB=587, 
		ST_ASWKT=588, ST_BUFFER=589, ST_BUFFER_STRATEGY=590, ST_CENTROID=591, 
		ST_CONTAINS=592, ST_CONVEXHULL=593, ST_CROSSES=594, ST_DIFFERENCE=595, 
		ST_DIMENSION=596, ST_DISJOINT=597, ST_DISTANCE=598, ST_DISTANCE_SPHERE=599, 
		ST_ENDPOINT=600, ST_ENVELOPE=601, ST_EQUALS=602, ST_EXTERIORRING=603, 
		ST_GEOHASH=604, ST_GEOMCOLLFROMTEXT=605, ST_GEOMCOLLFROMTXT=606, ST_GEOMCOLLFROMWKB=607, 
		ST_GEOMETRYCOLLECTIONFROMTEXT=608, ST_GEOMETRYCOLLECTIONFROMWKB=609, ST_GEOMETRYFROMTEXT=610, 
		ST_GEOMETRYFROMWKB=611, ST_GEOMETRYN=612, ST_GEOMETRYTYPE=613, ST_GEOMFROMGEOJSON=614, 
		ST_GEOMFROMTEXT=615, ST_GEOMFROMWKB=616, ST_INTERIORRINGN=617, ST_INTERSECTION=618, 
		ST_INTERSECTS=619, ST_ISCLOSED=620, ST_ISEMPTY=621, ST_ISSIMPLE=622, ST_ISVALID=623, 
		ST_LATFROMGEOHASH=624, ST_LATITUDE=625, ST_LENGTH=626, ST_LINEFROMTEXT=627, 
		ST_LINEFROMWKB=628, ST_LINESTRINGFROMTEXT=629, ST_LINESTRINGFROMWKB=630, 
		ST_LONGFROMGEOHASH=631, ST_LONGITUDE=632, ST_MAKEENVELOPE=633, ST_MLINEFROMTEXT=634, 
		ST_MLINEFROMWKB=635, ST_MULTILINESTRINGFROMTEXT=636, ST_MULTILINESTRINGFROMWKB=637, 
		ST_MPOINTFROMTEXT=638, ST_MPOINTFROMWKB=639, ST_MULTIPOINTFROMTEXT=640, 
		ST_MULTIPOINTFROMWKB=641, ST_MPOLYFROMTEXT=642, ST_MPOLYFROMWKB=643, ST_MULTIPOLYGONFROMTEXT=644, 
		ST_MULTIPOLYGONFROMWKB=645, ST_NUMGEOMETRIES=646, ST_NUMINTERIORRING=647, 
		ST_NUMINTERIORRINGS=648, ST_NUMPOINTS=649, ST_OVERLAPS=650, ST_POINTFROMGEOHASH=651, 
		ST_POINTFROMTEXT=652, ST_POINTFROMWKB=653, ST_POINTN=654, ST_POLYFROMTEXT=655, 
		ST_POLYFROMWKB=656, ST_POLYGONFROMTEXT=657, ST_POLYGONFROMWKB=658, ST_SIMPLIFY=659, 
		ST_SRID=660, ST_STARTPOINT=661, ST_SWAPXY=662, ST_SYMDIFFERENCE=663, ST_TOUCHES=664, 
		ST_TRANSFORM=665, ST_UNION=666, ST_VALIDATE=667, ST_WITHIN=668, ST_X=669, 
		ST_Y=670, BIT=671, BOOL=672, DEC=673, VARCHAR=674, VARBINARY=675, TINYBLOB=676, 
		TINYTEXT=677, BLOB=678, TEXT=679, MEDIUMBLOB=680, MEDIUMTEXT=681, LONGBLOB=682, 
		LONGTEXT=683, ENUM=684, GEOMETRY=685, JSON=686, IO_THREAD=687, SQL_THREAD=688, 
		SQL_BEFORE_GTIDS=689, SQL_AFTER_GTIDS=690, MASTER_LOG_FILE=691, MASTER_LOG_POS=692, 
		RELAY_LOG_FILE=693, RELAY_LOG_POS=694, SQL_AFTER_MTS_GAPS=695, UNTIL=696, 
		DEFAULT_AUTH=697, PLUGIN_DIR=698, STOP=699, IDENTIFIER_=700, STRING_=701, 
		NUMBER_=702, HEX_DIGIT_=703, BIT_NUM_=704, INNODB_=705, TLS_=706, Y_N_=707, 
		NOT_SUPPORT_=708, FILESIZE_LITERAL=709;
	public static final int
		RULE_parameterMarker = 0, RULE_literals = 1, RULE_stringLiterals = 2, 
		RULE_numberLiterals = 3, RULE_dateTimeLiterals = 4, RULE_hexadecimalLiterals = 5, 
		RULE_bitValueLiterals = 6, RULE_booleanLiterals = 7, RULE_nullValueLiterals = 8, 
		RULE_characterSetName_ = 9, RULE_collationName_ = 10, RULE_identifier = 11, 
		RULE_unreservedWord = 12, RULE_variable = 13, RULE_schemaName = 14, RULE_tableName = 15, 
		RULE_columnName = 16, RULE_indexName = 17, RULE_userName = 18, RULE_eventName = 19, 
		RULE_serverName = 20, RULE_wrapperName = 21, RULE_functionName = 22, RULE_viewName = 23, 
		RULE_owner = 24, RULE_name = 25, RULE_tableNames = 26, RULE_columnNames = 27, 
		RULE_groupName = 28, RULE_shardLibraryName = 29, RULE_componentName = 30, 
		RULE_pluginName = 31, RULE_hostName = 32, RULE_port = 33, RULE_cloneInstance = 34, 
		RULE_cloneDir = 35, RULE_channelName = 36, RULE_logName = 37, RULE_roleName = 38, 
		RULE_engineName = 39, RULE_triggerName = 40, RULE_triggerTime = 41, RULE_userOrRole = 42, 
		RULE_partitionName = 43, RULE_triggerEvent = 44, RULE_triggerOrder = 45, 
		RULE_expr = 46, RULE_logicalOperator = 47, RULE_notOperator_ = 48, RULE_booleanPrimary = 49, 
		RULE_comparisonOperator = 50, RULE_predicate = 51, RULE_bitExpr = 52, 
		RULE_simpleExpr = 53, RULE_functionCall = 54, RULE_aggregationFunction = 55, 
		RULE_aggregationFunctionName = 56, RULE_distinct = 57, RULE_overClause_ = 58, 
		RULE_windowSpecification_ = 59, RULE_partitionClause_ = 60, RULE_frameClause_ = 61, 
		RULE_frameStart_ = 62, RULE_frameEnd_ = 63, RULE_frameBetween_ = 64, RULE_specialFunction = 65, 
		RULE_groupConcatFunction = 66, RULE_windowFunction = 67, RULE_castFunction = 68, 
		RULE_convertFunction = 69, RULE_positionFunction = 70, RULE_substringFunction = 71, 
		RULE_extractFunction = 72, RULE_charFunction = 73, RULE_trimFunction_ = 74, 
		RULE_valuesFunction_ = 75, RULE_weightStringFunction = 76, RULE_levelClause_ = 77, 
		RULE_levelInWeightListElement_ = 78, RULE_regularFunction = 79, RULE_regularFunctionName_ = 80, 
		RULE_matchExpression_ = 81, RULE_matchSearchModifier_ = 82, RULE_caseExpression = 83, 
		RULE_caseWhen_ = 84, RULE_caseElse_ = 85, RULE_intervalExpression = 86, 
		RULE_intervalUnit_ = 87, RULE_subquery = 88, RULE_orderByClause = 89, 
		RULE_orderByItem = 90, RULE_dataType = 91, RULE_dataTypeName = 92, RULE_dataTypeLength = 93, 
		RULE_collectionOptions = 94, RULE_characterSet_ = 95, RULE_collateClause_ = 96, 
		RULE_ignoredIdentifier_ = 97, RULE_ignoredIdentifiers_ = 98;
	private static String[] makeRuleNames() {
		return new String[] {
			"parameterMarker", "literals", "stringLiterals", "numberLiterals", "dateTimeLiterals", 
			"hexadecimalLiterals", "bitValueLiterals", "booleanLiterals", "nullValueLiterals", 
			"characterSetName_", "collationName_", "identifier", "unreservedWord", 
			"variable", "schemaName", "tableName", "columnName", "indexName", "userName", 
			"eventName", "serverName", "wrapperName", "functionName", "viewName", 
			"owner", "name", "tableNames", "columnNames", "groupName", "shardLibraryName", 
			"componentName", "pluginName", "hostName", "port", "cloneInstance", "cloneDir", 
			"channelName", "logName", "roleName", "engineName", "triggerName", "triggerTime", 
			"userOrRole", "partitionName", "triggerEvent", "triggerOrder", "expr", 
			"logicalOperator", "notOperator_", "booleanPrimary", "comparisonOperator", 
			"predicate", "bitExpr", "simpleExpr", "functionCall", "aggregationFunction", 
			"aggregationFunctionName", "distinct", "overClause_", "windowSpecification_", 
			"partitionClause_", "frameClause_", "frameStart_", "frameEnd_", "frameBetween_", 
			"specialFunction", "groupConcatFunction", "windowFunction", "castFunction", 
			"convertFunction", "positionFunction", "substringFunction", "extractFunction", 
			"charFunction", "trimFunction_", "valuesFunction_", "weightStringFunction", 
			"levelClause_", "levelInWeightListElement_", "regularFunction", "regularFunctionName_", 
			"matchExpression_", "matchSearchModifier_", "caseExpression", "caseWhen_", 
			"caseElse_", "intervalExpression", "intervalUnit_", "subquery", "orderByClause", 
			"orderByItem", "dataType", "dataTypeName", "dataTypeLength", "collectionOptions", 
			"characterSet_", "collateClause_", "ignoredIdentifier_", "ignoredIdentifiers_"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'Default does not match anything'", "'&&'", "'||'", "'!'", "'~'", 
			"'|'", "'&'", "'<<'", "'>>'", "'^'", "'%'", "':'", "'+'", "'-'", "'*'", 
			"'/'", "'\\'", "'.'", "'.*'", "'<=>'", "'=='", "'='", null, "'>'", "'>='", 
			"'<'", "'<='", "'#'", "'('", "')'", "'{'", "'}'", "'['", "']'", "','", 
			"'\"'", "'''", "'`'", "'?'", "'@'", "';'", null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"'DO NOT MATCH ANY THING, JUST FOR GENERATOR'", null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, "'INNODB'", "'TLS'", null, "'not support'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, "AND_", "OR_", "NOT_", "TILDE_", "VERTICAL_BAR_", "AMPERSAND_", 
			"SIGNED_LEFT_SHIFT_", "SIGNED_RIGHT_SHIFT_", "CARET_", "MOD_", "COLON_", 
			"PLUS_", "MINUS_", "ASTERISK_", "SLASH_", "BACKSLASH_", "DOT_", "DOT_ASTERISK_", 
			"SAFE_EQ_", "DEQ_", "EQ_", "NEQ_", "GT_", "GTE_", "LT_", "LTE_", "POUND_", 
			"LP_", "RP_", "LBE_", "RBE_", "LBT_", "RBT_", "COMMA_", "DQ_", "SQ_", 
			"BQ_", "QUESTION_", "AT_", "SEMI_", "WS", "SELECT", "INSERT", "UPDATE", 
			"DELETE", "CREATE", "ALTER", "DROP", "TRUNCATE", "SCHEMA", "GRANT", "REVOKE", 
			"ADD", "SET", "TABLE", "COLUMN", "INDEX", "CONSTRAINT", "PRIMARY", "UNIQUE", 
			"FOREIGN", "KEY", "POSITION", "PRECISION", "FUNCTION", "TRIGGER", "PROCEDURE", 
			"VIEW", "INTO", "VALUES", "WITH", "UNION", "DISTINCT", "CASE", "WHEN", 
			"CAST", "TRIM", "SUBSTRING", "FROM", "NATURAL", "JOIN", "FULL", "INNER", 
			"OUTER", "LEFT", "RIGHT", "CROSS", "USING", "WHERE", "AS", "ON", "IF", 
			"ELSE", "THEN", "FOR", "TO", "AND", "OR", "IS", "NOT", "NULL", "TRUE", 
			"FALSE", "EXISTS", "BETWEEN", "IN", "ALL", "ANY", "LIKE", "ORDER", "GROUP", 
			"BY", "ASC", "DESC", "HAVING", "LIMIT", "OFFSET", "BEGIN", "COMMIT", 
			"ROLLBACK", "SAVEPOINT", "BOOLEAN", "DOUBLE", "CHAR", "CHARACTER", "ARRAY", 
			"INTERVAL", "DATE", "TIME", "TIMESTAMP", "LOCALTIME", "LOCALTIMESTAMP", 
			"YEAR", "QUARTER", "MONTH", "WEEK", "DAY", "HOUR", "MINUTE", "SECOND", 
			"MICROSECOND", "MAX", "MIN", "SUM", "COUNT", "AVG", "DEFAULT", "CURRENT", 
			"ENABLE", "DISABLE", "CALL", "INSTANCE", "PRESERVE", "DO", "DEFINER", 
			"CURRENT_USER", "SQL", "CASCADED", "LOCAL", "CLOSE", "OPEN", "NEXT", 
			"NAME", "COLLATION", "NAMES", "INTEGER", "REAL", "DECIMAL", "TYPE", "INT", 
			"SMALLINT", "TINYINT", "MEDIUMINT", "BIGINT", "NUMERIC", "FLOAT", "DATETIME", 
			"FOR_GENERATOR", "USE", "DESCRIBE", "SHOW", "DATABASES", "DATABASE", 
			"SCHEMAS", "TABLES", "TABLESPACE", "COLUMNS", "FIELDS", "INDEXES", "STATUS", 
			"REPLACE", "MODIFY", "DISTINCTROW", "VALUE", "DUPLICATE", "FIRST", "LAST", 
			"AFTER", "OJ", "WINDOW", "MOD", "DIV", "XOR", "REGEXP", "RLIKE", "ACCOUNT", 
			"USER", "ROLE", "START", "TRANSACTION", "ROW", "ROWS", "WITHOUT", "BINARY", 
			"ESCAPE", "GENERATED", "PARTITION", "SUBPARTITION", "STORAGE", "STORED", 
			"SUPER", "SUBSTR", "TEMPORARY", "THAN", "TRAILING", "UNBOUNDED", "UNLOCK", 
			"UNSIGNED", "SIGNED", "UPGRADE", "USAGE", "VALIDATION", "VIRTUAL", "ROLLUP", 
			"SOUNDS", "UNKNOWN", "OFF", "ALWAYS", "CASCADE", "CHECK", "COMMITTED", 
			"LEVEL", "NO", "OPTION", "PASSWORD", "PRIVILEGES", "READ", "WRITE", "REFERENCES", 
			"ACTION", "ALGORITHM", "ANALYZE", "AUTOCOMMIT", "MAXVALUE", "BOTH", "BTREE", 
			"CHAIN", "CHANGE", "CHARSET", "CHECKSUM", "CIPHER", "CLIENT", "COALESCE", 
			"COLLATE", "COMMENT", "COMPACT", "COMPRESSED", "COMPRESSION", "CONNECTION", 
			"CONSISTENT", "CONVERT", "COPY", "DATA", "DELAYED", "DIRECTORY", "DISCARD", 
			"DISK", "DYNAMIC", "ENCRYPTION", "END", "ENGINE", "EVENT", "EXCEPT", 
			"EXCHANGE", "EXCLUSIVE", "EXECUTE", "EXTRACT", "FILE", "FIXED", "FOLLOWING", 
			"FORCE", "FULLTEXT", "GLOBAL", "HASH", "IDENTIFIED", "IGNORE", "IMPORT_", 
			"INPLACE", "KEYS", "LEADING", "LESS", "LINEAR", "LOCK", "MATCH", "MEMORY", 
			"NONE", "NOW", "OFFLINE", "ONLINE", "OPTIMIZE", "OVER", "PARSER", "PARTIAL", 
			"PARTITIONING", "PERSIST", "PRECEDING", "PROCESS", "PROXY", "QUICK", 
			"RANGE", "REBUILD", "RECURSIVE", "REDUNDANT", "RELEASE", "RELOAD", "REMOVE", 
			"RENAME", "REORGANIZE", "REPAIR", "REPLICATION", "REQUIRE", "RESTRICT", 
			"REVERSE", "ROUTINE", "SEPARATOR", "SESSION", "SHARED", "SHUTDOWN", "SIMPLE", 
			"SLAVE", "SPATIAL", "ZEROFILL", "VISIBLE", "INVISIBLE", "INSTANT", "ENFORCED", 
			"AGAINST", "LANGUAGE", "MODE", "QUERY", "EXTENDED", "EXPANSION", "VARIANCE", 
			"MAX_ROWS", "MIN_ROWS", "HIGH_PRIORITY", "LOW_PRIORITY", "SQL_BIG_RESULT", 
			"SQL_BUFFER_RESULT", "SQL_CACHE", "SQL_CALC_FOUND_ROWS", "SQL_NO_CACHE", 
			"SQL_SMALL_RESULT", "STATS_AUTO_RECALC", "STATS_PERSISTENT", "STATS_SAMPLE_PAGES", 
			"ROLE_ADMIN", "ROW_FORMAT", "SET_USER_ID", "REPLICATION_SLAVE_ADMIN", 
			"GROUP_REPLICATION_ADMIN", "STRAIGHT_JOIN", "WEIGHT_STRING", "COLUMN_FORMAT", 
			"CONNECTION_ADMIN", "FIREWALL_ADMIN", "FIREWALL_USER", "INSERT_METHOD", 
			"KEY_BLOCK_SIZE", "PACK_KEYS", "PERSIST_ONLY", "BIT_AND", "BIT_OR", "BIT_XOR", 
			"GROUP_CONCAT", "JSON_ARRAYAGG", "JSON_OBJECTAGG", "STD", "STDDEV", "STDDEV_POP", 
			"STDDEV_SAMP", "VAR_POP", "VAR_SAMP", "AUDIT_ADMIN", "AUTO_INCREMENT", 
			"AVG_ROW_LENGTH", "BINLOG_ADMIN", "DELAY_KEY_WRITE", "ENCRYPTION_KEY_ADMIN", 
			"SYSTEM_VARIABLES_ADMIN", "VERSION_TOKEN_ADMIN", "CURRENT_TIMESTAMP", 
			"YEAR_MONTH", "DAY_HOUR", "DAY_MINUTE", "DAY_SECOND", "DAY_MICROSECOND", 
			"HOUR_MINUTE", "HOUR_SECOND", "HOUR_MICROSECOND", "MINUTE_SECOND", "MINUTE_MICROSECOND", 
			"SECOND_MICROSECOND", "UL_BINARY", "ROTATE", "MASTER", "BINLOG", "ERROR", 
			"SCHEDULE", "COMPLETION", "EVERY", "STARTS", "ENDS", "HOST", "SOCKET", 
			"PORT", "SERVER", "WRAPPER", "OPTIONS", "OWNER", "DETERMINISTIC", "RETURNS", 
			"CONTAINS", "READS", "MODIFIES", "SECURITY", "INVOKER", "OUT", "INOUT", 
			"TEMPTABLE", "MERGE", "UNDEFINED", "DATAFILE", "FILE_BLOCK_SIZE", "EXTENT_SIZE", 
			"INITIAL_SIZE", "AUTOEXTEND_SIZE", "MAX_SIZE", "NODEGROUP", "WAIT", "LOGFILE", 
			"UNDOFILE", "UNDO_BUFFER_SIZE", "REDO_BUFFER_SIZE", "HANDLER", "PREV", 
			"ORGANIZATION", "DEFINITION", "DESCRIPTION", "REFERENCE", "FOLLOWS", 
			"PRECEDES", "IMPORT", "LOAD", "CONCURRENT", "INFILE", "LINES", "STARTING", 
			"TERMINATED", "OPTIONALLY", "ENCLOSED", "ESCAPED", "XML", "UNDO", "DUMPFILE", 
			"OUTFILE", "SHARE", "LOGS", "EVENTS", "BEFORE", "EACH", "MUTEX", "ENGINES", 
			"ERRORS", "CODE", "GRANTS", "PLUGINS", "PROCESSLIST", "BLOCK", "IO", 
			"CONTEXT", "SWITCHES", "CPU", "IPC", "PAGE", "FAULTS", "SOURCE", "SWAPS", 
			"PROFILE", "PROFILES", "RELAYLOG", "CHANNEL", "VARIABLES", "WARNINGS", 
			"SSL", "CLONE", "AGGREGATE", "STRING", "SONAME", "INSTALL", "COMPONENT", 
			"PLUGIN", "UNINSTALL", "NO_WRITE_TO_BINLOG", "HISTOGRAM", "BUCKETS", 
			"FAST", "MEDIUM", "USE_FRM", "RESOURCE", "VCPU", "THREAD_PRIORITY", "SYSTEM", 
			"EXPIRE", "NEVER", "HISTORY", "OPTIONAL", "REUSE", "MAX_QUERIES_PER_HOUR", 
			"MAX_UPDATES_PER_HOUR", "MAX_CONNECTIONS_PER_HOUR", "MAX_USER_CONNECTIONS", 
			"RETAIN", "RANDOM", "OLD", "X509", "ISSUER", "SUBJECT", "CACHE", "GENERAL", 
			"OPTIMIZER_COSTS", "SLOW", "USER_RESOURCES", "EXPORT", "RELAY", "HOSTS", 
			"KILL", "FLUSH", "RESET", "RESTART", "UNIX_TIMESTAMP", "LOWER", "UPPER", 
			"ADDDATE", "ADDTIME", "DATE_ADD", "DATE_SUB", "DATEDIFF", "DATE_FORMAT", 
			"DAYNAME", "DAYOFMONTH", "DAYOFWEEK", "DAYOFYEAR", "STR_TO_DATE", "TIMEDIFF", 
			"TIMESTAMPADD", "TIMESTAMPDIFF", "TIME_FORMAT", "TIME_TO_SEC", "AES_DECRYPT", 
			"AES_ENCRYPT", "FROM_BASE64", "TO_BASE64", "GEOMCOLLECTION", "GEOMETRYCOLLECTION", 
			"LINESTRING", "MULTILINESTRING", "MULTIPOINT", "MULTIPOLYGON", "POINT", 
			"POLYGON", "ST_AREA", "ST_ASBINARY", "ST_ASGEOJSON", "ST_ASTEXT", "ST_ASWKB", 
			"ST_ASWKT", "ST_BUFFER", "ST_BUFFER_STRATEGY", "ST_CENTROID", "ST_CONTAINS", 
			"ST_CONVEXHULL", "ST_CROSSES", "ST_DIFFERENCE", "ST_DIMENSION", "ST_DISJOINT", 
			"ST_DISTANCE", "ST_DISTANCE_SPHERE", "ST_ENDPOINT", "ST_ENVELOPE", "ST_EQUALS", 
			"ST_EXTERIORRING", "ST_GEOHASH", "ST_GEOMCOLLFROMTEXT", "ST_GEOMCOLLFROMTXT", 
			"ST_GEOMCOLLFROMWKB", "ST_GEOMETRYCOLLECTIONFROMTEXT", "ST_GEOMETRYCOLLECTIONFROMWKB", 
			"ST_GEOMETRYFROMTEXT", "ST_GEOMETRYFROMWKB", "ST_GEOMETRYN", "ST_GEOMETRYTYPE", 
			"ST_GEOMFROMGEOJSON", "ST_GEOMFROMTEXT", "ST_GEOMFROMWKB", "ST_INTERIORRINGN", 
			"ST_INTERSECTION", "ST_INTERSECTS", "ST_ISCLOSED", "ST_ISEMPTY", "ST_ISSIMPLE", 
			"ST_ISVALID", "ST_LATFROMGEOHASH", "ST_LATITUDE", "ST_LENGTH", "ST_LINEFROMTEXT", 
			"ST_LINEFROMWKB", "ST_LINESTRINGFROMTEXT", "ST_LINESTRINGFROMWKB", "ST_LONGFROMGEOHASH", 
			"ST_LONGITUDE", "ST_MAKEENVELOPE", "ST_MLINEFROMTEXT", "ST_MLINEFROMWKB", 
			"ST_MULTILINESTRINGFROMTEXT", "ST_MULTILINESTRINGFROMWKB", "ST_MPOINTFROMTEXT", 
			"ST_MPOINTFROMWKB", "ST_MULTIPOINTFROMTEXT", "ST_MULTIPOINTFROMWKB", 
			"ST_MPOLYFROMTEXT", "ST_MPOLYFROMWKB", "ST_MULTIPOLYGONFROMTEXT", "ST_MULTIPOLYGONFROMWKB", 
			"ST_NUMGEOMETRIES", "ST_NUMINTERIORRING", "ST_NUMINTERIORRINGS", "ST_NUMPOINTS", 
			"ST_OVERLAPS", "ST_POINTFROMGEOHASH", "ST_POINTFROMTEXT", "ST_POINTFROMWKB", 
			"ST_POINTN", "ST_POLYFROMTEXT", "ST_POLYFROMWKB", "ST_POLYGONFROMTEXT", 
			"ST_POLYGONFROMWKB", "ST_SIMPLIFY", "ST_SRID", "ST_STARTPOINT", "ST_SWAPXY", 
			"ST_SYMDIFFERENCE", "ST_TOUCHES", "ST_TRANSFORM", "ST_UNION", "ST_VALIDATE", 
			"ST_WITHIN", "ST_X", "ST_Y", "BIT", "BOOL", "DEC", "VARCHAR", "VARBINARY", 
			"TINYBLOB", "TINYTEXT", "BLOB", "TEXT", "MEDIUMBLOB", "MEDIUMTEXT", "LONGBLOB", 
			"LONGTEXT", "ENUM", "GEOMETRY", "JSON", "IO_THREAD", "SQL_THREAD", "SQL_BEFORE_GTIDS", 
			"SQL_AFTER_GTIDS", "MASTER_LOG_FILE", "MASTER_LOG_POS", "RELAY_LOG_FILE", 
			"RELAY_LOG_POS", "SQL_AFTER_MTS_GAPS", "UNTIL", "DEFAULT_AUTH", "PLUGIN_DIR", 
			"STOP", "IDENTIFIER_", "STRING_", "NUMBER_", "HEX_DIGIT_", "BIT_NUM_", 
			"INNODB_", "TLS_", "Y_N_", "NOT_SUPPORT_", "FILESIZE_LITERAL"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "BaseRule.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BaseRuleParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ParameterMarkerContext extends ParserRuleContext {
		public TerminalNode QUESTION_() { return getToken(BaseRuleParser.QUESTION_, 0); }
		public ParameterMarkerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterMarker; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterParameterMarker(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitParameterMarker(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitParameterMarker(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterMarkerContext parameterMarker() throws RecognitionException {
		ParameterMarkerContext _localctx = new ParameterMarkerContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parameterMarker);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198);
			match(QUESTION_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralsContext extends ParserRuleContext {
		public StringLiteralsContext stringLiterals() {
			return getRuleContext(StringLiteralsContext.class,0);
		}
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public DateTimeLiteralsContext dateTimeLiterals() {
			return getRuleContext(DateTimeLiteralsContext.class,0);
		}
		public HexadecimalLiteralsContext hexadecimalLiterals() {
			return getRuleContext(HexadecimalLiteralsContext.class,0);
		}
		public BitValueLiteralsContext bitValueLiterals() {
			return getRuleContext(BitValueLiteralsContext.class,0);
		}
		public BooleanLiteralsContext booleanLiterals() {
			return getRuleContext(BooleanLiteralsContext.class,0);
		}
		public NullValueLiteralsContext nullValueLiterals() {
			return getRuleContext(NullValueLiteralsContext.class,0);
		}
		public LiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralsContext literals() throws RecognitionException {
		LiteralsContext _localctx = new LiteralsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_literals);
		try {
			setState(207);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(200);
				stringLiterals();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(201);
				numberLiterals();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(202);
				dateTimeLiterals();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(203);
				hexadecimalLiterals();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(204);
				bitValueLiterals();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(205);
				booleanLiterals();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(206);
				nullValueLiterals();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralsContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public CharacterSetName_Context characterSetName_() {
			return getRuleContext(CharacterSetName_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public StringLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterStringLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitStringLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitStringLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringLiteralsContext stringLiterals() throws RecognitionException {
		StringLiteralsContext _localctx = new StringLiteralsContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_stringLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER_) {
				{
				setState(209);
				characterSetName_();
				}
			}

			setState(212);
			match(STRING_);
			setState(214);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(213);
				collateClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberLiteralsContext extends ParserRuleContext {
		public TerminalNode NUMBER_() { return getToken(BaseRuleParser.NUMBER_, 0); }
		public TerminalNode MINUS_() { return getToken(BaseRuleParser.MINUS_, 0); }
		public NumberLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numberLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterNumberLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitNumberLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitNumberLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberLiteralsContext numberLiterals() throws RecognitionException {
		NumberLiteralsContext _localctx = new NumberLiteralsContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_numberLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(217);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==MINUS_) {
				{
				setState(216);
				match(MINUS_);
				}
			}

			setState(219);
			match(NUMBER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateTimeLiteralsContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public TerminalNode DATE() { return getToken(BaseRuleParser.DATE, 0); }
		public TerminalNode TIME() { return getToken(BaseRuleParser.TIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(BaseRuleParser.TIMESTAMP, 0); }
		public TerminalNode LBE_() { return getToken(BaseRuleParser.LBE_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode RBE_() { return getToken(BaseRuleParser.RBE_, 0); }
		public DateTimeLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateTimeLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterDateTimeLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitDateTimeLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitDateTimeLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateTimeLiteralsContext dateTimeLiterals() throws RecognitionException {
		DateTimeLiteralsContext _localctx = new DateTimeLiteralsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_dateTimeLiterals);
		int _la;
		try {
			setState(228);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DATE:
			case TIME:
			case TIMESTAMP:
				enterOuterAlt(_localctx, 1);
				{
				setState(221);
				_la = _input.LA(1);
				if ( !(((((_la - 129)) & ~0x3f) == 0 && ((1L << (_la - 129)) & ((1L << (DATE - 129)) | (1L << (TIME - 129)) | (1L << (TIMESTAMP - 129)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(222);
				match(STRING_);
				}
				break;
			case LBE_:
				enterOuterAlt(_localctx, 2);
				{
				setState(223);
				match(LBE_);
				setState(224);
				identifier();
				setState(225);
				match(STRING_);
				setState(226);
				match(RBE_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HexadecimalLiteralsContext extends ParserRuleContext {
		public TerminalNode HEX_DIGIT_() { return getToken(BaseRuleParser.HEX_DIGIT_, 0); }
		public CharacterSetName_Context characterSetName_() {
			return getRuleContext(CharacterSetName_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public HexadecimalLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hexadecimalLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterHexadecimalLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitHexadecimalLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitHexadecimalLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HexadecimalLiteralsContext hexadecimalLiterals() throws RecognitionException {
		HexadecimalLiteralsContext _localctx = new HexadecimalLiteralsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_hexadecimalLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(231);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER_) {
				{
				setState(230);
				characterSetName_();
				}
			}

			setState(233);
			match(HEX_DIGIT_);
			setState(235);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(234);
				collateClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitValueLiteralsContext extends ParserRuleContext {
		public TerminalNode BIT_NUM_() { return getToken(BaseRuleParser.BIT_NUM_, 0); }
		public CharacterSetName_Context characterSetName_() {
			return getRuleContext(CharacterSetName_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public BitValueLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitValueLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterBitValueLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitBitValueLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitBitValueLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BitValueLiteralsContext bitValueLiterals() throws RecognitionException {
		BitValueLiteralsContext _localctx = new BitValueLiteralsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_bitValueLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER_) {
				{
				setState(237);
				characterSetName_();
				}
			}

			setState(240);
			match(BIT_NUM_);
			setState(242);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				{
				setState(241);
				collateClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanLiteralsContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(BaseRuleParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(BaseRuleParser.FALSE, 0); }
		public BooleanLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterBooleanLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitBooleanLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitBooleanLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanLiteralsContext booleanLiterals() throws RecognitionException {
		BooleanLiteralsContext _localctx = new BooleanLiteralsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_booleanLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(244);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullValueLiteralsContext extends ParserRuleContext {
		public TerminalNode NULL() { return getToken(BaseRuleParser.NULL, 0); }
		public NullValueLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullValueLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterNullValueLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitNullValueLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitNullValueLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullValueLiteralsContext nullValueLiterals() throws RecognitionException {
		NullValueLiteralsContext _localctx = new NullValueLiteralsContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_nullValueLiterals);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(246);
			match(NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharacterSetName_Context extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public CharacterSetName_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_characterSetName_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCharacterSetName_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCharacterSetName_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCharacterSetName_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharacterSetName_Context characterSetName_() throws RecognitionException {
		CharacterSetName_Context _localctx = new CharacterSetName_Context(_ctx, getState());
		enterRule(_localctx, 18, RULE_characterSetName_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollationName_Context extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public CollationName_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collationName_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCollationName_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCollationName_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCollationName_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollationName_Context collationName_() throws RecognitionException {
		CollationName_Context _localctx = new CollationName_Context(_ctx, getState());
		enterRule(_localctx, 20, RULE_collationName_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(250);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public UnreservedWordContext unreservedWord() {
			return getRuleContext(UnreservedWordContext.class,0);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_identifier);
		try {
			setState(254);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(252);
				match(IDENTIFIER_);
				}
				break;
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
				enterOuterAlt(_localctx, 2);
				{
				setState(253);
				unreservedWord();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnreservedWordContext extends ParserRuleContext {
		public TerminalNode ACCOUNT() { return getToken(BaseRuleParser.ACCOUNT, 0); }
		public TerminalNode ACTION() { return getToken(BaseRuleParser.ACTION, 0); }
		public TerminalNode AFTER() { return getToken(BaseRuleParser.AFTER, 0); }
		public TerminalNode ALGORITHM() { return getToken(BaseRuleParser.ALGORITHM, 0); }
		public TerminalNode ALWAYS() { return getToken(BaseRuleParser.ALWAYS, 0); }
		public TerminalNode ANY() { return getToken(BaseRuleParser.ANY, 0); }
		public TerminalNode AUTO_INCREMENT() { return getToken(BaseRuleParser.AUTO_INCREMENT, 0); }
		public TerminalNode AVG_ROW_LENGTH() { return getToken(BaseRuleParser.AVG_ROW_LENGTH, 0); }
		public TerminalNode BEGIN() { return getToken(BaseRuleParser.BEGIN, 0); }
		public TerminalNode BTREE() { return getToken(BaseRuleParser.BTREE, 0); }
		public TerminalNode CHAIN() { return getToken(BaseRuleParser.CHAIN, 0); }
		public TerminalNode CHARSET() { return getToken(BaseRuleParser.CHARSET, 0); }
		public TerminalNode CHECKSUM() { return getToken(BaseRuleParser.CHECKSUM, 0); }
		public TerminalNode CIPHER() { return getToken(BaseRuleParser.CIPHER, 0); }
		public TerminalNode CLIENT() { return getToken(BaseRuleParser.CLIENT, 0); }
		public TerminalNode COALESCE() { return getToken(BaseRuleParser.COALESCE, 0); }
		public TerminalNode COLUMNS() { return getToken(BaseRuleParser.COLUMNS, 0); }
		public TerminalNode COLUMN_FORMAT() { return getToken(BaseRuleParser.COLUMN_FORMAT, 0); }
		public TerminalNode COMMENT() { return getToken(BaseRuleParser.COMMENT, 0); }
		public TerminalNode COMMIT() { return getToken(BaseRuleParser.COMMIT, 0); }
		public TerminalNode COMMITTED() { return getToken(BaseRuleParser.COMMITTED, 0); }
		public TerminalNode COMPACT() { return getToken(BaseRuleParser.COMPACT, 0); }
		public TerminalNode COMPRESSED() { return getToken(BaseRuleParser.COMPRESSED, 0); }
		public TerminalNode COMPRESSION() { return getToken(BaseRuleParser.COMPRESSION, 0); }
		public TerminalNode CONNECTION() { return getToken(BaseRuleParser.CONNECTION, 0); }
		public TerminalNode CONSISTENT() { return getToken(BaseRuleParser.CONSISTENT, 0); }
		public TerminalNode CURRENT() { return getToken(BaseRuleParser.CURRENT, 0); }
		public TerminalNode DATA() { return getToken(BaseRuleParser.DATA, 0); }
		public TerminalNode DATE() { return getToken(BaseRuleParser.DATE, 0); }
		public TerminalNode DELAY_KEY_WRITE() { return getToken(BaseRuleParser.DELAY_KEY_WRITE, 0); }
		public TerminalNode DISABLE() { return getToken(BaseRuleParser.DISABLE, 0); }
		public TerminalNode DISCARD() { return getToken(BaseRuleParser.DISCARD, 0); }
		public TerminalNode DISK() { return getToken(BaseRuleParser.DISK, 0); }
		public TerminalNode DUPLICATE() { return getToken(BaseRuleParser.DUPLICATE, 0); }
		public TerminalNode ENABLE() { return getToken(BaseRuleParser.ENABLE, 0); }
		public TerminalNode ENCRYPTION() { return getToken(BaseRuleParser.ENCRYPTION, 0); }
		public TerminalNode ENFORCED() { return getToken(BaseRuleParser.ENFORCED, 0); }
		public TerminalNode END() { return getToken(BaseRuleParser.END, 0); }
		public TerminalNode ENGINE() { return getToken(BaseRuleParser.ENGINE, 0); }
		public TerminalNode ESCAPE() { return getToken(BaseRuleParser.ESCAPE, 0); }
		public TerminalNode EVENT() { return getToken(BaseRuleParser.EVENT, 0); }
		public TerminalNode EXCHANGE() { return getToken(BaseRuleParser.EXCHANGE, 0); }
		public TerminalNode EXECUTE() { return getToken(BaseRuleParser.EXECUTE, 0); }
		public TerminalNode FILE() { return getToken(BaseRuleParser.FILE, 0); }
		public TerminalNode FIRST() { return getToken(BaseRuleParser.FIRST, 0); }
		public TerminalNode FIXED() { return getToken(BaseRuleParser.FIXED, 0); }
		public TerminalNode FOLLOWING() { return getToken(BaseRuleParser.FOLLOWING, 0); }
		public TerminalNode GLOBAL() { return getToken(BaseRuleParser.GLOBAL, 0); }
		public TerminalNode HASH() { return getToken(BaseRuleParser.HASH, 0); }
		public TerminalNode IMPORT_() { return getToken(BaseRuleParser.IMPORT_, 0); }
		public TerminalNode INSERT_METHOD() { return getToken(BaseRuleParser.INSERT_METHOD, 0); }
		public TerminalNode INVISIBLE() { return getToken(BaseRuleParser.INVISIBLE, 0); }
		public TerminalNode KEY_BLOCK_SIZE() { return getToken(BaseRuleParser.KEY_BLOCK_SIZE, 0); }
		public TerminalNode LAST() { return getToken(BaseRuleParser.LAST, 0); }
		public TerminalNode LESS() { return getToken(BaseRuleParser.LESS, 0); }
		public TerminalNode LEVEL() { return getToken(BaseRuleParser.LEVEL, 0); }
		public TerminalNode MAX_ROWS() { return getToken(BaseRuleParser.MAX_ROWS, 0); }
		public TerminalNode MEMORY() { return getToken(BaseRuleParser.MEMORY, 0); }
		public TerminalNode MIN_ROWS() { return getToken(BaseRuleParser.MIN_ROWS, 0); }
		public TerminalNode MODIFY() { return getToken(BaseRuleParser.MODIFY, 0); }
		public TerminalNode NO() { return getToken(BaseRuleParser.NO, 0); }
		public TerminalNode NONE() { return getToken(BaseRuleParser.NONE, 0); }
		public TerminalNode OFFSET() { return getToken(BaseRuleParser.OFFSET, 0); }
		public TerminalNode PACK_KEYS() { return getToken(BaseRuleParser.PACK_KEYS, 0); }
		public TerminalNode PARSER() { return getToken(BaseRuleParser.PARSER, 0); }
		public TerminalNode PARTIAL() { return getToken(BaseRuleParser.PARTIAL, 0); }
		public TerminalNode PARTITIONING() { return getToken(BaseRuleParser.PARTITIONING, 0); }
		public TerminalNode PASSWORD() { return getToken(BaseRuleParser.PASSWORD, 0); }
		public TerminalNode PERSIST() { return getToken(BaseRuleParser.PERSIST, 0); }
		public TerminalNode PERSIST_ONLY() { return getToken(BaseRuleParser.PERSIST_ONLY, 0); }
		public TerminalNode PRECEDING() { return getToken(BaseRuleParser.PRECEDING, 0); }
		public TerminalNode PRIVILEGES() { return getToken(BaseRuleParser.PRIVILEGES, 0); }
		public TerminalNode PROCESS() { return getToken(BaseRuleParser.PROCESS, 0); }
		public TerminalNode PROXY() { return getToken(BaseRuleParser.PROXY, 0); }
		public TerminalNode QUICK() { return getToken(BaseRuleParser.QUICK, 0); }
		public TerminalNode REBUILD() { return getToken(BaseRuleParser.REBUILD, 0); }
		public TerminalNode REDUNDANT() { return getToken(BaseRuleParser.REDUNDANT, 0); }
		public TerminalNode RELOAD() { return getToken(BaseRuleParser.RELOAD, 0); }
		public TerminalNode REMOVE() { return getToken(BaseRuleParser.REMOVE, 0); }
		public TerminalNode REORGANIZE() { return getToken(BaseRuleParser.REORGANIZE, 0); }
		public TerminalNode REPAIR() { return getToken(BaseRuleParser.REPAIR, 0); }
		public TerminalNode REVERSE() { return getToken(BaseRuleParser.REVERSE, 0); }
		public TerminalNode ROLLBACK() { return getToken(BaseRuleParser.ROLLBACK, 0); }
		public TerminalNode ROLLUP() { return getToken(BaseRuleParser.ROLLUP, 0); }
		public TerminalNode ROW_FORMAT() { return getToken(BaseRuleParser.ROW_FORMAT, 0); }
		public TerminalNode SAVEPOINT() { return getToken(BaseRuleParser.SAVEPOINT, 0); }
		public TerminalNode SESSION() { return getToken(BaseRuleParser.SESSION, 0); }
		public TerminalNode SHUTDOWN() { return getToken(BaseRuleParser.SHUTDOWN, 0); }
		public TerminalNode SIMPLE() { return getToken(BaseRuleParser.SIMPLE, 0); }
		public TerminalNode SLAVE() { return getToken(BaseRuleParser.SLAVE, 0); }
		public TerminalNode SOUNDS() { return getToken(BaseRuleParser.SOUNDS, 0); }
		public TerminalNode SQL_BIG_RESULT() { return getToken(BaseRuleParser.SQL_BIG_RESULT, 0); }
		public TerminalNode SQL_BUFFER_RESULT() { return getToken(BaseRuleParser.SQL_BUFFER_RESULT, 0); }
		public TerminalNode SQL_CACHE() { return getToken(BaseRuleParser.SQL_CACHE, 0); }
		public TerminalNode SQL_NO_CACHE() { return getToken(BaseRuleParser.SQL_NO_CACHE, 0); }
		public TerminalNode START() { return getToken(BaseRuleParser.START, 0); }
		public TerminalNode STATS_AUTO_RECALC() { return getToken(BaseRuleParser.STATS_AUTO_RECALC, 0); }
		public TerminalNode STATS_PERSISTENT() { return getToken(BaseRuleParser.STATS_PERSISTENT, 0); }
		public TerminalNode STATS_SAMPLE_PAGES() { return getToken(BaseRuleParser.STATS_SAMPLE_PAGES, 0); }
		public TerminalNode STORAGE() { return getToken(BaseRuleParser.STORAGE, 0); }
		public TerminalNode SUBPARTITION() { return getToken(BaseRuleParser.SUBPARTITION, 0); }
		public TerminalNode SUPER() { return getToken(BaseRuleParser.SUPER, 0); }
		public TerminalNode TABLES() { return getToken(BaseRuleParser.TABLES, 0); }
		public TerminalNode TABLESPACE() { return getToken(BaseRuleParser.TABLESPACE, 0); }
		public TerminalNode TEMPORARY() { return getToken(BaseRuleParser.TEMPORARY, 0); }
		public TerminalNode THAN() { return getToken(BaseRuleParser.THAN, 0); }
		public TerminalNode TIME() { return getToken(BaseRuleParser.TIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(BaseRuleParser.TIMESTAMP, 0); }
		public TerminalNode TRANSACTION() { return getToken(BaseRuleParser.TRANSACTION, 0); }
		public TerminalNode TRUNCATE() { return getToken(BaseRuleParser.TRUNCATE, 0); }
		public TerminalNode UNBOUNDED() { return getToken(BaseRuleParser.UNBOUNDED, 0); }
		public TerminalNode UNKNOWN() { return getToken(BaseRuleParser.UNKNOWN, 0); }
		public TerminalNode UPGRADE() { return getToken(BaseRuleParser.UPGRADE, 0); }
		public TerminalNode VALIDATION() { return getToken(BaseRuleParser.VALIDATION, 0); }
		public TerminalNode VALUE() { return getToken(BaseRuleParser.VALUE, 0); }
		public TerminalNode VIEW() { return getToken(BaseRuleParser.VIEW, 0); }
		public TerminalNode VISIBLE() { return getToken(BaseRuleParser.VISIBLE, 0); }
		public TerminalNode WEIGHT_STRING() { return getToken(BaseRuleParser.WEIGHT_STRING, 0); }
		public TerminalNode WITHOUT() { return getToken(BaseRuleParser.WITHOUT, 0); }
		public TerminalNode MICROSECOND() { return getToken(BaseRuleParser.MICROSECOND, 0); }
		public TerminalNode SECOND() { return getToken(BaseRuleParser.SECOND, 0); }
		public TerminalNode MINUTE() { return getToken(BaseRuleParser.MINUTE, 0); }
		public TerminalNode HOUR() { return getToken(BaseRuleParser.HOUR, 0); }
		public TerminalNode DAY() { return getToken(BaseRuleParser.DAY, 0); }
		public TerminalNode WEEK() { return getToken(BaseRuleParser.WEEK, 0); }
		public TerminalNode MONTH() { return getToken(BaseRuleParser.MONTH, 0); }
		public TerminalNode QUARTER() { return getToken(BaseRuleParser.QUARTER, 0); }
		public TerminalNode YEAR() { return getToken(BaseRuleParser.YEAR, 0); }
		public TerminalNode AGAINST() { return getToken(BaseRuleParser.AGAINST, 0); }
		public TerminalNode LANGUAGE() { return getToken(BaseRuleParser.LANGUAGE, 0); }
		public TerminalNode MODE() { return getToken(BaseRuleParser.MODE, 0); }
		public TerminalNode QUERY() { return getToken(BaseRuleParser.QUERY, 0); }
		public TerminalNode EXPANSION() { return getToken(BaseRuleParser.EXPANSION, 0); }
		public TerminalNode BOOLEAN() { return getToken(BaseRuleParser.BOOLEAN, 0); }
		public TerminalNode MAX() { return getToken(BaseRuleParser.MAX, 0); }
		public TerminalNode MIN() { return getToken(BaseRuleParser.MIN, 0); }
		public TerminalNode SUM() { return getToken(BaseRuleParser.SUM, 0); }
		public TerminalNode COUNT() { return getToken(BaseRuleParser.COUNT, 0); }
		public TerminalNode AVG() { return getToken(BaseRuleParser.AVG, 0); }
		public TerminalNode BIT_AND() { return getToken(BaseRuleParser.BIT_AND, 0); }
		public TerminalNode BIT_OR() { return getToken(BaseRuleParser.BIT_OR, 0); }
		public TerminalNode BIT_XOR() { return getToken(BaseRuleParser.BIT_XOR, 0); }
		public TerminalNode GROUP_CONCAT() { return getToken(BaseRuleParser.GROUP_CONCAT, 0); }
		public TerminalNode JSON_ARRAYAGG() { return getToken(BaseRuleParser.JSON_ARRAYAGG, 0); }
		public TerminalNode JSON_OBJECTAGG() { return getToken(BaseRuleParser.JSON_OBJECTAGG, 0); }
		public TerminalNode STD() { return getToken(BaseRuleParser.STD, 0); }
		public TerminalNode STDDEV() { return getToken(BaseRuleParser.STDDEV, 0); }
		public TerminalNode STDDEV_POP() { return getToken(BaseRuleParser.STDDEV_POP, 0); }
		public TerminalNode STDDEV_SAMP() { return getToken(BaseRuleParser.STDDEV_SAMP, 0); }
		public TerminalNode VAR_POP() { return getToken(BaseRuleParser.VAR_POP, 0); }
		public TerminalNode VAR_SAMP() { return getToken(BaseRuleParser.VAR_SAMP, 0); }
		public TerminalNode VARIANCE() { return getToken(BaseRuleParser.VARIANCE, 0); }
		public TerminalNode EXTENDED() { return getToken(BaseRuleParser.EXTENDED, 0); }
		public TerminalNode STATUS() { return getToken(BaseRuleParser.STATUS, 0); }
		public TerminalNode FIELDS() { return getToken(BaseRuleParser.FIELDS, 0); }
		public TerminalNode INDEXES() { return getToken(BaseRuleParser.INDEXES, 0); }
		public TerminalNode USER() { return getToken(BaseRuleParser.USER, 0); }
		public TerminalNode ROLE() { return getToken(BaseRuleParser.ROLE, 0); }
		public TerminalNode OJ() { return getToken(BaseRuleParser.OJ, 0); }
		public TerminalNode AUTOCOMMIT() { return getToken(BaseRuleParser.AUTOCOMMIT, 0); }
		public TerminalNode OFF() { return getToken(BaseRuleParser.OFF, 0); }
		public TerminalNode ROTATE() { return getToken(BaseRuleParser.ROTATE, 0); }
		public TerminalNode INSTANCE() { return getToken(BaseRuleParser.INSTANCE, 0); }
		public TerminalNode MASTER() { return getToken(BaseRuleParser.MASTER, 0); }
		public TerminalNode BINLOG() { return getToken(BaseRuleParser.BINLOG, 0); }
		public TerminalNode ERROR() { return getToken(BaseRuleParser.ERROR, 0); }
		public TerminalNode SCHEDULE() { return getToken(BaseRuleParser.SCHEDULE, 0); }
		public TerminalNode COMPLETION() { return getToken(BaseRuleParser.COMPLETION, 0); }
		public TerminalNode DO() { return getToken(BaseRuleParser.DO, 0); }
		public TerminalNode DEFINER() { return getToken(BaseRuleParser.DEFINER, 0); }
		public TerminalNode EVERY() { return getToken(BaseRuleParser.EVERY, 0); }
		public TerminalNode HOST() { return getToken(BaseRuleParser.HOST, 0); }
		public TerminalNode SOCKET() { return getToken(BaseRuleParser.SOCKET, 0); }
		public TerminalNode OWNER() { return getToken(BaseRuleParser.OWNER, 0); }
		public TerminalNode PORT() { return getToken(BaseRuleParser.PORT, 0); }
		public TerminalNode RETURNS() { return getToken(BaseRuleParser.RETURNS, 0); }
		public TerminalNode CONTAINS() { return getToken(BaseRuleParser.CONTAINS, 0); }
		public TerminalNode SECURITY() { return getToken(BaseRuleParser.SECURITY, 0); }
		public TerminalNode INVOKER() { return getToken(BaseRuleParser.INVOKER, 0); }
		public TerminalNode UNDEFINED() { return getToken(BaseRuleParser.UNDEFINED, 0); }
		public TerminalNode MERGE() { return getToken(BaseRuleParser.MERGE, 0); }
		public TerminalNode TEMPTABLE() { return getToken(BaseRuleParser.TEMPTABLE, 0); }
		public TerminalNode CASCADED() { return getToken(BaseRuleParser.CASCADED, 0); }
		public TerminalNode LOCAL() { return getToken(BaseRuleParser.LOCAL, 0); }
		public TerminalNode SERVER() { return getToken(BaseRuleParser.SERVER, 0); }
		public TerminalNode WRAPPER() { return getToken(BaseRuleParser.WRAPPER, 0); }
		public TerminalNode OPTIONS() { return getToken(BaseRuleParser.OPTIONS, 0); }
		public TerminalNode DATAFILE() { return getToken(BaseRuleParser.DATAFILE, 0); }
		public TerminalNode FILE_BLOCK_SIZE() { return getToken(BaseRuleParser.FILE_BLOCK_SIZE, 0); }
		public TerminalNode EXTENT_SIZE() { return getToken(BaseRuleParser.EXTENT_SIZE, 0); }
		public TerminalNode INITIAL_SIZE() { return getToken(BaseRuleParser.INITIAL_SIZE, 0); }
		public TerminalNode AUTOEXTEND_SIZE() { return getToken(BaseRuleParser.AUTOEXTEND_SIZE, 0); }
		public TerminalNode MAX_SIZE() { return getToken(BaseRuleParser.MAX_SIZE, 0); }
		public TerminalNode NODEGROUP() { return getToken(BaseRuleParser.NODEGROUP, 0); }
		public TerminalNode WAIT() { return getToken(BaseRuleParser.WAIT, 0); }
		public TerminalNode LOGFILE() { return getToken(BaseRuleParser.LOGFILE, 0); }
		public TerminalNode UNDOFILE() { return getToken(BaseRuleParser.UNDOFILE, 0); }
		public TerminalNode UNDO_BUFFER_SIZE() { return getToken(BaseRuleParser.UNDO_BUFFER_SIZE, 0); }
		public TerminalNode REDO_BUFFER_SIZE() { return getToken(BaseRuleParser.REDO_BUFFER_SIZE, 0); }
		public TerminalNode DEFINITION() { return getToken(BaseRuleParser.DEFINITION, 0); }
		public TerminalNode ORGANIZATION() { return getToken(BaseRuleParser.ORGANIZATION, 0); }
		public TerminalNode DESCRIPTION() { return getToken(BaseRuleParser.DESCRIPTION, 0); }
		public TerminalNode REFERENCE() { return getToken(BaseRuleParser.REFERENCE, 0); }
		public TerminalNode FOLLOWS() { return getToken(BaseRuleParser.FOLLOWS, 0); }
		public TerminalNode PRECEDES() { return getToken(BaseRuleParser.PRECEDES, 0); }
		public TerminalNode NAME() { return getToken(BaseRuleParser.NAME, 0); }
		public TerminalNode CLOSE() { return getToken(BaseRuleParser.CLOSE, 0); }
		public TerminalNode OPEN() { return getToken(BaseRuleParser.OPEN, 0); }
		public TerminalNode NEXT() { return getToken(BaseRuleParser.NEXT, 0); }
		public TerminalNode HANDLER() { return getToken(BaseRuleParser.HANDLER, 0); }
		public TerminalNode PREV() { return getToken(BaseRuleParser.PREV, 0); }
		public TerminalNode IMPORT() { return getToken(BaseRuleParser.IMPORT, 0); }
		public TerminalNode CONCURRENT() { return getToken(BaseRuleParser.CONCURRENT, 0); }
		public TerminalNode XML() { return getToken(BaseRuleParser.XML, 0); }
		public TerminalNode POSITION() { return getToken(BaseRuleParser.POSITION, 0); }
		public TerminalNode SHARE() { return getToken(BaseRuleParser.SHARE, 0); }
		public TerminalNode DUMPFILE() { return getToken(BaseRuleParser.DUMPFILE, 0); }
		public TerminalNode CLONE() { return getToken(BaseRuleParser.CLONE, 0); }
		public TerminalNode AGGREGATE() { return getToken(BaseRuleParser.AGGREGATE, 0); }
		public TerminalNode INSTALL() { return getToken(BaseRuleParser.INSTALL, 0); }
		public TerminalNode UNINSTALL() { return getToken(BaseRuleParser.UNINSTALL, 0); }
		public TerminalNode COMPONENT() { return getToken(BaseRuleParser.COMPONENT, 0); }
		public TerminalNode RESOURCE() { return getToken(BaseRuleParser.RESOURCE, 0); }
		public TerminalNode FLUSH() { return getToken(BaseRuleParser.FLUSH, 0); }
		public TerminalNode RESET() { return getToken(BaseRuleParser.RESET, 0); }
		public TerminalNode RESTART() { return getToken(BaseRuleParser.RESTART, 0); }
		public TerminalNode HOSTS() { return getToken(BaseRuleParser.HOSTS, 0); }
		public TerminalNode RELAY() { return getToken(BaseRuleParser.RELAY, 0); }
		public TerminalNode EXPORT() { return getToken(BaseRuleParser.EXPORT, 0); }
		public TerminalNode USER_RESOURCES() { return getToken(BaseRuleParser.USER_RESOURCES, 0); }
		public TerminalNode SLOW() { return getToken(BaseRuleParser.SLOW, 0); }
		public TerminalNode GENERAL() { return getToken(BaseRuleParser.GENERAL, 0); }
		public TerminalNode CACHE() { return getToken(BaseRuleParser.CACHE, 0); }
		public TerminalNode SUBJECT() { return getToken(BaseRuleParser.SUBJECT, 0); }
		public TerminalNode ISSUER() { return getToken(BaseRuleParser.ISSUER, 0); }
		public TerminalNode OLD() { return getToken(BaseRuleParser.OLD, 0); }
		public TerminalNode RANDOM() { return getToken(BaseRuleParser.RANDOM, 0); }
		public TerminalNode RETAIN() { return getToken(BaseRuleParser.RETAIN, 0); }
		public TerminalNode MAX_USER_CONNECTIONS() { return getToken(BaseRuleParser.MAX_USER_CONNECTIONS, 0); }
		public TerminalNode MAX_CONNECTIONS_PER_HOUR() { return getToken(BaseRuleParser.MAX_CONNECTIONS_PER_HOUR, 0); }
		public TerminalNode MAX_UPDATES_PER_HOUR() { return getToken(BaseRuleParser.MAX_UPDATES_PER_HOUR, 0); }
		public TerminalNode MAX_QUERIES_PER_HOUR() { return getToken(BaseRuleParser.MAX_QUERIES_PER_HOUR, 0); }
		public TerminalNode REUSE() { return getToken(BaseRuleParser.REUSE, 0); }
		public TerminalNode OPTIONAL() { return getToken(BaseRuleParser.OPTIONAL, 0); }
		public TerminalNode HISTORY() { return getToken(BaseRuleParser.HISTORY, 0); }
		public TerminalNode NEVER() { return getToken(BaseRuleParser.NEVER, 0); }
		public TerminalNode EXPIRE() { return getToken(BaseRuleParser.EXPIRE, 0); }
		public TerminalNode TYPE() { return getToken(BaseRuleParser.TYPE, 0); }
		public TerminalNode CONTEXT() { return getToken(BaseRuleParser.CONTEXT, 0); }
		public TerminalNode CODE() { return getToken(BaseRuleParser.CODE, 0); }
		public TerminalNode CHANNEL() { return getToken(BaseRuleParser.CHANNEL, 0); }
		public TerminalNode SOURCE() { return getToken(BaseRuleParser.SOURCE, 0); }
		public TerminalNode IO_THREAD() { return getToken(BaseRuleParser.IO_THREAD, 0); }
		public TerminalNode SQL_THREAD() { return getToken(BaseRuleParser.SQL_THREAD, 0); }
		public TerminalNode SQL_BEFORE_GTIDS() { return getToken(BaseRuleParser.SQL_BEFORE_GTIDS, 0); }
		public TerminalNode SQL_AFTER_GTIDS() { return getToken(BaseRuleParser.SQL_AFTER_GTIDS, 0); }
		public TerminalNode MASTER_LOG_FILE() { return getToken(BaseRuleParser.MASTER_LOG_FILE, 0); }
		public TerminalNode MASTER_LOG_POS() { return getToken(BaseRuleParser.MASTER_LOG_POS, 0); }
		public TerminalNode RELAY_LOG_FILE() { return getToken(BaseRuleParser.RELAY_LOG_FILE, 0); }
		public TerminalNode RELAY_LOG_POS() { return getToken(BaseRuleParser.RELAY_LOG_POS, 0); }
		public TerminalNode SQL_AFTER_MTS_GAPS() { return getToken(BaseRuleParser.SQL_AFTER_MTS_GAPS, 0); }
		public TerminalNode UNTIL() { return getToken(BaseRuleParser.UNTIL, 0); }
		public TerminalNode DEFAULT_AUTH() { return getToken(BaseRuleParser.DEFAULT_AUTH, 0); }
		public TerminalNode PLUGIN_DIR() { return getToken(BaseRuleParser.PLUGIN_DIR, 0); }
		public TerminalNode STOP() { return getToken(BaseRuleParser.STOP, 0); }
		public TerminalNode SIGNED() { return getToken(BaseRuleParser.SIGNED, 0); }
		public UnreservedWordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unreservedWord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterUnreservedWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitUnreservedWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitUnreservedWord(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnreservedWordContext unreservedWord() throws RecognitionException {
		UnreservedWordContext _localctx = new UnreservedWordContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_unreservedWord);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256);
			_la = _input.LA(1);
			if ( !(((((_la - 50)) & ~0x3f) == 0 && ((1L << (_la - 50)) & ((1L << (TRUNCATE - 50)) | (1L << (POSITION - 50)) | (1L << (VIEW - 50)) | (1L << (ANY - 50)))) != 0) || ((((_la - 118)) & ~0x3f) == 0 && ((1L << (_la - 118)) & ((1L << (OFFSET - 118)) | (1L << (BEGIN - 118)) | (1L << (COMMIT - 118)) | (1L << (ROLLBACK - 118)) | (1L << (SAVEPOINT - 118)) | (1L << (BOOLEAN - 118)) | (1L << (DATE - 118)) | (1L << (TIME - 118)) | (1L << (TIMESTAMP - 118)) | (1L << (YEAR - 118)) | (1L << (QUARTER - 118)) | (1L << (MONTH - 118)) | (1L << (WEEK - 118)) | (1L << (DAY - 118)) | (1L << (HOUR - 118)) | (1L << (MINUTE - 118)) | (1L << (SECOND - 118)) | (1L << (MICROSECOND - 118)) | (1L << (MAX - 118)) | (1L << (MIN - 118)) | (1L << (SUM - 118)) | (1L << (COUNT - 118)) | (1L << (AVG - 118)) | (1L << (CURRENT - 118)) | (1L << (ENABLE - 118)) | (1L << (DISABLE - 118)) | (1L << (INSTANCE - 118)) | (1L << (DO - 118)) | (1L << (DEFINER - 118)) | (1L << (CASCADED - 118)) | (1L << (LOCAL - 118)) | (1L << (CLOSE - 118)) | (1L << (OPEN - 118)) | (1L << (NEXT - 118)) | (1L << (NAME - 118)) | (1L << (TYPE - 118)))) != 0) || ((((_la - 186)) & ~0x3f) == 0 && ((1L << (_la - 186)) & ((1L << (TABLES - 186)) | (1L << (TABLESPACE - 186)) | (1L << (COLUMNS - 186)) | (1L << (FIELDS - 186)) | (1L << (INDEXES - 186)) | (1L << (STATUS - 186)) | (1L << (MODIFY - 186)) | (1L << (VALUE - 186)) | (1L << (DUPLICATE - 186)) | (1L << (FIRST - 186)) | (1L << (LAST - 186)) | (1L << (AFTER - 186)) | (1L << (OJ - 186)) | (1L << (ACCOUNT - 186)) | (1L << (USER - 186)) | (1L << (ROLE - 186)) | (1L << (START - 186)) | (1L << (TRANSACTION - 186)) | (1L << (WITHOUT - 186)) | (1L << (ESCAPE - 186)) | (1L << (SUBPARTITION - 186)) | (1L << (STORAGE - 186)) | (1L << (SUPER - 186)) | (1L << (TEMPORARY - 186)) | (1L << (THAN - 186)) | (1L << (UNBOUNDED - 186)) | (1L << (SIGNED - 186)) | (1L << (UPGRADE - 186)) | (1L << (VALIDATION - 186)) | (1L << (ROLLUP - 186)) | (1L << (SOUNDS - 186)) | (1L << (UNKNOWN - 186)) | (1L << (OFF - 186)) | (1L << (ALWAYS - 186)) | (1L << (COMMITTED - 186)) | (1L << (LEVEL - 186)) | (1L << (NO - 186)) | (1L << (PASSWORD - 186)) | (1L << (PRIVILEGES - 186)))) != 0) || ((((_la - 251)) & ~0x3f) == 0 && ((1L << (_la - 251)) & ((1L << (ACTION - 251)) | (1L << (ALGORITHM - 251)) | (1L << (AUTOCOMMIT - 251)) | (1L << (BTREE - 251)) | (1L << (CHAIN - 251)) | (1L << (CHARSET - 251)) | (1L << (CHECKSUM - 251)) | (1L << (CIPHER - 251)) | (1L << (CLIENT - 251)) | (1L << (COALESCE - 251)) | (1L << (COMMENT - 251)) | (1L << (COMPACT - 251)) | (1L << (COMPRESSED - 251)) | (1L << (COMPRESSION - 251)) | (1L << (CONNECTION - 251)) | (1L << (CONSISTENT - 251)) | (1L << (DATA - 251)) | (1L << (DISCARD - 251)) | (1L << (DISK - 251)) | (1L << (ENCRYPTION - 251)) | (1L << (END - 251)) | (1L << (ENGINE - 251)) | (1L << (EVENT - 251)) | (1L << (EXCHANGE - 251)) | (1L << (EXECUTE - 251)) | (1L << (FILE - 251)) | (1L << (FIXED - 251)) | (1L << (FOLLOWING - 251)) | (1L << (GLOBAL - 251)) | (1L << (HASH - 251)) | (1L << (IMPORT_ - 251)) | (1L << (LESS - 251)) | (1L << (MEMORY - 251)) | (1L << (NONE - 251)) | (1L << (PARSER - 251)) | (1L << (PARTIAL - 251)))) != 0) || ((((_la - 315)) & ~0x3f) == 0 && ((1L << (_la - 315)) & ((1L << (PARTITIONING - 315)) | (1L << (PERSIST - 315)) | (1L << (PRECEDING - 315)) | (1L << (PROCESS - 315)) | (1L << (PROXY - 315)) | (1L << (QUICK - 315)) | (1L << (REBUILD - 315)) | (1L << (REDUNDANT - 315)) | (1L << (RELOAD - 315)) | (1L << (REMOVE - 315)) | (1L << (REORGANIZE - 315)) | (1L << (REPAIR - 315)) | (1L << (REVERSE - 315)) | (1L << (SESSION - 315)) | (1L << (SHUTDOWN - 315)) | (1L << (SIMPLE - 315)) | (1L << (SLAVE - 315)) | (1L << (VISIBLE - 315)) | (1L << (INVISIBLE - 315)) | (1L << (ENFORCED - 315)) | (1L << (AGAINST - 315)) | (1L << (LANGUAGE - 315)) | (1L << (MODE - 315)) | (1L << (QUERY - 315)) | (1L << (EXTENDED - 315)) | (1L << (EXPANSION - 315)) | (1L << (VARIANCE - 315)) | (1L << (MAX_ROWS - 315)) | (1L << (MIN_ROWS - 315)) | (1L << (SQL_BIG_RESULT - 315)) | (1L << (SQL_BUFFER_RESULT - 315)) | (1L << (SQL_CACHE - 315)) | (1L << (SQL_NO_CACHE - 315)) | (1L << (STATS_AUTO_RECALC - 315)) | (1L << (STATS_PERSISTENT - 315)) | (1L << (STATS_SAMPLE_PAGES - 315)) | (1L << (ROW_FORMAT - 315)) | (1L << (WEIGHT_STRING - 315)) | (1L << (COLUMN_FORMAT - 315)))) != 0) || ((((_la - 379)) & ~0x3f) == 0 && ((1L << (_la - 379)) & ((1L << (INSERT_METHOD - 379)) | (1L << (KEY_BLOCK_SIZE - 379)) | (1L << (PACK_KEYS - 379)) | (1L << (PERSIST_ONLY - 379)) | (1L << (BIT_AND - 379)) | (1L << (BIT_OR - 379)) | (1L << (BIT_XOR - 379)) | (1L << (GROUP_CONCAT - 379)) | (1L << (JSON_ARRAYAGG - 379)) | (1L << (JSON_OBJECTAGG - 379)) | (1L << (STD - 379)) | (1L << (STDDEV - 379)) | (1L << (STDDEV_POP - 379)) | (1L << (STDDEV_SAMP - 379)) | (1L << (VAR_POP - 379)) | (1L << (VAR_SAMP - 379)) | (1L << (AUTO_INCREMENT - 379)) | (1L << (AVG_ROW_LENGTH - 379)) | (1L << (DELAY_KEY_WRITE - 379)) | (1L << (ROTATE - 379)) | (1L << (MASTER - 379)) | (1L << (BINLOG - 379)) | (1L << (ERROR - 379)) | (1L << (SCHEDULE - 379)) | (1L << (COMPLETION - 379)) | (1L << (EVERY - 379)) | (1L << (HOST - 379)) | (1L << (SOCKET - 379)) | (1L << (PORT - 379)) | (1L << (SERVER - 379)) | (1L << (WRAPPER - 379)) | (1L << (OPTIONS - 379)) | (1L << (OWNER - 379)) | (1L << (RETURNS - 379)) | (1L << (CONTAINS - 379)) | (1L << (SECURITY - 379)) | (1L << (INVOKER - 379)) | (1L << (TEMPTABLE - 379)) | (1L << (MERGE - 379)))) != 0) || ((((_la - 443)) & ~0x3f) == 0 && ((1L << (_la - 443)) & ((1L << (UNDEFINED - 443)) | (1L << (DATAFILE - 443)) | (1L << (FILE_BLOCK_SIZE - 443)) | (1L << (EXTENT_SIZE - 443)) | (1L << (INITIAL_SIZE - 443)) | (1L << (AUTOEXTEND_SIZE - 443)) | (1L << (MAX_SIZE - 443)) | (1L << (NODEGROUP - 443)) | (1L << (WAIT - 443)) | (1L << (LOGFILE - 443)) | (1L << (UNDOFILE - 443)) | (1L << (UNDO_BUFFER_SIZE - 443)) | (1L << (REDO_BUFFER_SIZE - 443)) | (1L << (HANDLER - 443)) | (1L << (PREV - 443)) | (1L << (ORGANIZATION - 443)) | (1L << (DEFINITION - 443)) | (1L << (DESCRIPTION - 443)) | (1L << (REFERENCE - 443)) | (1L << (FOLLOWS - 443)) | (1L << (PRECEDES - 443)) | (1L << (IMPORT - 443)) | (1L << (CONCURRENT - 443)) | (1L << (XML - 443)) | (1L << (DUMPFILE - 443)) | (1L << (SHARE - 443)) | (1L << (CODE - 443)) | (1L << (CONTEXT - 443)) | (1L << (SOURCE - 443)) | (1L << (CHANNEL - 443)))) != 0) || ((((_la - 507)) & ~0x3f) == 0 && ((1L << (_la - 507)) & ((1L << (CLONE - 507)) | (1L << (AGGREGATE - 507)) | (1L << (INSTALL - 507)) | (1L << (COMPONENT - 507)) | (1L << (UNINSTALL - 507)) | (1L << (RESOURCE - 507)) | (1L << (EXPIRE - 507)) | (1L << (NEVER - 507)) | (1L << (HISTORY - 507)) | (1L << (OPTIONAL - 507)) | (1L << (REUSE - 507)) | (1L << (MAX_QUERIES_PER_HOUR - 507)) | (1L << (MAX_UPDATES_PER_HOUR - 507)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 507)) | (1L << (MAX_USER_CONNECTIONS - 507)) | (1L << (RETAIN - 507)) | (1L << (RANDOM - 507)) | (1L << (OLD - 507)) | (1L << (ISSUER - 507)) | (1L << (SUBJECT - 507)) | (1L << (CACHE - 507)) | (1L << (GENERAL - 507)) | (1L << (SLOW - 507)) | (1L << (USER_RESOURCES - 507)) | (1L << (EXPORT - 507)) | (1L << (RELAY - 507)) | (1L << (HOSTS - 507)) | (1L << (FLUSH - 507)) | (1L << (RESET - 507)) | (1L << (RESTART - 507)))) != 0) || ((((_la - 687)) & ~0x3f) == 0 && ((1L << (_la - 687)) & ((1L << (IO_THREAD - 687)) | (1L << (SQL_THREAD - 687)) | (1L << (SQL_BEFORE_GTIDS - 687)) | (1L << (SQL_AFTER_GTIDS - 687)) | (1L << (MASTER_LOG_FILE - 687)) | (1L << (MASTER_LOG_POS - 687)) | (1L << (RELAY_LOG_FILE - 687)) | (1L << (RELAY_LOG_POS - 687)) | (1L << (SQL_AFTER_MTS_GAPS - 687)) | (1L << (UNTIL - 687)) | (1L << (DEFAULT_AUTH - 687)) | (1L << (PLUGIN_DIR - 687)) | (1L << (STOP - 687)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<TerminalNode> AT_() { return getTokens(BaseRuleParser.AT_); }
		public TerminalNode AT_(int i) {
			return getToken(BaseRuleParser.AT_, i);
		}
		public TerminalNode DOT_() { return getToken(BaseRuleParser.DOT_, 0); }
		public TerminalNode GLOBAL() { return getToken(BaseRuleParser.GLOBAL, 0); }
		public TerminalNode PERSIST() { return getToken(BaseRuleParser.PERSIST, 0); }
		public TerminalNode PERSIST_ONLY() { return getToken(BaseRuleParser.PERSIST_ONLY, 0); }
		public TerminalNode SESSION() { return getToken(BaseRuleParser.SESSION, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_variable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==AT_) {
				{
				setState(259);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
				case 1:
					{
					setState(258);
					match(AT_);
					}
					break;
				}
				setState(261);
				match(AT_);
				}
			}

			setState(265);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				setState(264);
				_la = _input.LA(1);
				if ( !(((((_la - 294)) & ~0x3f) == 0 && ((1L << (_la - 294)) & ((1L << (GLOBAL - 294)) | (1L << (PERSIST - 294)) | (1L << (SESSION - 294)))) != 0) || _la==PERSIST_ONLY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
			setState(268);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOT_) {
				{
				setState(267);
				match(DOT_);
				}
			}

			setState(270);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SchemaNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public SchemaNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_schemaName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterSchemaName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitSchemaName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitSchemaName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SchemaNameContext schemaName() throws RecognitionException {
		SchemaNameContext _localctx = new SchemaNameContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_schemaName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(BaseRuleParser.DOT_, 0); }
		public TableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTableName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTableName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTableName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableNameContext tableName() throws RecognitionException {
		TableNameContext _localctx = new TableNameContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_tableName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(277);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(274);
				owner();
				setState(275);
				match(DOT_);
				}
				break;
			}
			setState(279);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(BaseRuleParser.DOT_, 0); }
		public ColumnNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterColumnName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitColumnName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitColumnName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNameContext columnName() throws RecognitionException {
		ColumnNameContext _localctx = new ColumnNameContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_columnName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(284);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(281);
				owner();
				setState(282);
				match(DOT_);
				}
				break;
			}
			setState(286);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IndexNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public IndexNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterIndexName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitIndexName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitIndexName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexNameContext indexName() throws RecognitionException {
		IndexNameContext _localctx = new IndexNameContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_indexName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(288);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UserNameContext extends ParserRuleContext {
		public List<TerminalNode> STRING_() { return getTokens(BaseRuleParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(BaseRuleParser.STRING_, i);
		}
		public TerminalNode AT_() { return getToken(BaseRuleParser.AT_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public UserNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_userName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterUserName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitUserName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitUserName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UserNameContext userName() throws RecognitionException {
		UserNameContext _localctx = new UserNameContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_userName);
		try {
			setState(295);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(290);
				match(STRING_);
				setState(291);
				match(AT_);
				setState(292);
				match(STRING_);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(293);
				identifier();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(294);
				match(STRING_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EventNameContext extends ParserRuleContext {
		public TerminalNode AT_() { return getToken(BaseRuleParser.AT_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(BaseRuleParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(BaseRuleParser.STRING_, i);
		}
		public List<TerminalNode> IDENTIFIER_() { return getTokens(BaseRuleParser.IDENTIFIER_); }
		public TerminalNode IDENTIFIER_(int i) {
			return getToken(BaseRuleParser.IDENTIFIER_, i);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public EventNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eventName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterEventName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitEventName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitEventName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EventNameContext eventName() throws RecognitionException {
		EventNameContext _localctx = new EventNameContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_eventName);
		int _la;
		try {
			setState(303);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(297);
				_la = _input.LA(1);
				if ( !(_la==IDENTIFIER_ || _la==STRING_) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(298);
				match(AT_);
				{
				setState(299);
				match(STRING_);
				setState(300);
				match(IDENTIFIER_);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(301);
				identifier();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(302);
				match(STRING_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ServerNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public ServerNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_serverName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterServerName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitServerName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitServerName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ServerNameContext serverName() throws RecognitionException {
		ServerNameContext _localctx = new ServerNameContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_serverName);
		try {
			setState(307);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(305);
				identifier();
				}
				break;
			case STRING_:
				enterOuterAlt(_localctx, 2);
				{
				setState(306);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WrapperNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public WrapperNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wrapperName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterWrapperName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitWrapperName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitWrapperName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WrapperNameContext wrapperName() throws RecognitionException {
		WrapperNameContext _localctx = new WrapperNameContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_wrapperName);
		try {
			setState(311);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(309);
				identifier();
				}
				break;
			case STRING_:
				enterOuterAlt(_localctx, 2);
				{
				setState(310);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(BaseRuleParser.DOT_, 0); }
		public FunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitFunctionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitFunctionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionNameContext functionName() throws RecognitionException {
		FunctionNameContext _localctx = new FunctionNameContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_functionName);
		try {
			setState(320);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(313);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(317);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
				case 1:
					{
					setState(314);
					owner();
					setState(315);
					match(DOT_);
					}
					break;
				}
				setState(319);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ViewNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(BaseRuleParser.DOT_, 0); }
		public ViewNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_viewName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterViewName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitViewName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitViewName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ViewNameContext viewName() throws RecognitionException {
		ViewNameContext _localctx = new ViewNameContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_viewName);
		try {
			setState(329);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(322);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(326);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
				case 1:
					{
					setState(323);
					owner();
					setState(324);
					match(DOT_);
					}
					break;
				}
				setState(328);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OwnerContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OwnerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_owner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterOwner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitOwner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitOwner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OwnerContext owner() throws RecognitionException {
		OwnerContext _localctx = new OwnerContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_owner);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(331);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(333);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNamesContext extends ParserRuleContext {
		public List<TableNameContext> tableName() {
			return getRuleContexts(TableNameContext.class);
		}
		public TableNameContext tableName(int i) {
			return getRuleContext(TableNameContext.class,i);
		}
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TableNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTableNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTableNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTableNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableNamesContext tableNames() throws RecognitionException {
		TableNamesContext _localctx = new TableNamesContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_tableNames);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(336);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(335);
				match(LP_);
				}
			}

			setState(338);
			tableName();
			setState(343);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(339);
				match(COMMA_);
				setState(340);
				tableName();
				}
				}
				setState(345);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(347);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RP_) {
				{
				setState(346);
				match(RP_);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNamesContext extends ParserRuleContext {
		public List<ColumnNameContext> columnName() {
			return getRuleContexts(ColumnNameContext.class);
		}
		public ColumnNameContext columnName(int i) {
			return getRuleContext(ColumnNameContext.class,i);
		}
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public ColumnNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterColumnNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitColumnNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitColumnNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNamesContext columnNames() throws RecognitionException {
		ColumnNamesContext _localctx = new ColumnNamesContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_columnNames);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(350);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(349);
				match(LP_);
				}
			}

			setState(352);
			columnName();
			setState(357);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(353);
				match(COMMA_);
				setState(354);
				columnName();
				}
				}
				setState(359);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(361);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RP_) {
				{
				setState(360);
				match(RP_);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public GroupNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterGroupName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitGroupName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitGroupName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupNameContext groupName() throws RecognitionException {
		GroupNameContext _localctx = new GroupNameContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_groupName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(363);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShardLibraryNameContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public ShardLibraryNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shardLibraryName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterShardLibraryName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitShardLibraryName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitShardLibraryName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShardLibraryNameContext shardLibraryName() throws RecognitionException {
		ShardLibraryNameContext _localctx = new ShardLibraryNameContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_shardLibraryName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(365);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComponentNameContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public ComponentNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_componentName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterComponentName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitComponentName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitComponentName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComponentNameContext componentName() throws RecognitionException {
		ComponentNameContext _localctx = new ComponentNameContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_componentName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(367);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PluginNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public PluginNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pluginName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterPluginName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitPluginName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitPluginName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PluginNameContext pluginName() throws RecognitionException {
		PluginNameContext _localctx = new PluginNameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_pluginName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(369);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HostNameContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public HostNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hostName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterHostName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitHostName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitHostName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HostNameContext hostName() throws RecognitionException {
		HostNameContext _localctx = new HostNameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_hostName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(371);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PortContext extends ParserRuleContext {
		public TerminalNode NUMBER_() { return getToken(BaseRuleParser.NUMBER_, 0); }
		public PortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_port; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterPort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitPort(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitPort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PortContext port() throws RecognitionException {
		PortContext _localctx = new PortContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_port);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(373);
			match(NUMBER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloneInstanceContext extends ParserRuleContext {
		public UserNameContext userName() {
			return getRuleContext(UserNameContext.class,0);
		}
		public TerminalNode AT_() { return getToken(BaseRuleParser.AT_, 0); }
		public HostNameContext hostName() {
			return getRuleContext(HostNameContext.class,0);
		}
		public TerminalNode COLON_() { return getToken(BaseRuleParser.COLON_, 0); }
		public PortContext port() {
			return getRuleContext(PortContext.class,0);
		}
		public CloneInstanceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cloneInstance; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCloneInstance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCloneInstance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCloneInstance(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloneInstanceContext cloneInstance() throws RecognitionException {
		CloneInstanceContext _localctx = new CloneInstanceContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_cloneInstance);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(375);
			userName();
			setState(376);
			match(AT_);
			setState(377);
			hostName();
			setState(378);
			match(COLON_);
			setState(379);
			port();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloneDirContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public CloneDirContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cloneDir; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCloneDir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCloneDir(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCloneDir(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloneDirContext cloneDir() throws RecognitionException {
		CloneDirContext _localctx = new CloneDirContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_cloneDir);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(381);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChannelNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public ChannelNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_channelName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterChannelName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitChannelName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitChannelName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ChannelNameContext channelName() throws RecognitionException {
		ChannelNameContext _localctx = new ChannelNameContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_channelName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public LogNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterLogName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitLogName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitLogName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogNameContext logName() throws RecognitionException {
		LogNameContext _localctx = new LogNameContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_logName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(385);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoleNameContext extends ParserRuleContext {
		public TerminalNode AT_() { return getToken(BaseRuleParser.AT_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(BaseRuleParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(BaseRuleParser.STRING_, i);
		}
		public List<TerminalNode> IDENTIFIER_() { return getTokens(BaseRuleParser.IDENTIFIER_); }
		public TerminalNode IDENTIFIER_(int i) {
			return getToken(BaseRuleParser.IDENTIFIER_, i);
		}
		public RoleNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roleName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterRoleName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitRoleName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitRoleName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RoleNameContext roleName() throws RecognitionException {
		RoleNameContext _localctx = new RoleNameContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_roleName);
		int _la;
		try {
			setState(392);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(387);
				_la = _input.LA(1);
				if ( !(_la==IDENTIFIER_ || _la==STRING_) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(388);
				match(AT_);
				{
				setState(389);
				match(STRING_);
				setState(390);
				match(IDENTIFIER_);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(391);
				match(IDENTIFIER_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EngineNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public EngineNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_engineName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterEngineName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitEngineName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitEngineName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EngineNameContext engineName() throws RecognitionException {
		EngineNameContext _localctx = new EngineNameContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_engineName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(394);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public TriggerNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTriggerName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTriggerName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTriggerName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerNameContext triggerName() throws RecognitionException {
		TriggerNameContext _localctx = new TriggerNameContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_triggerName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(396);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerTimeContext extends ParserRuleContext {
		public TerminalNode BEFORE() { return getToken(BaseRuleParser.BEFORE, 0); }
		public TerminalNode AFTER() { return getToken(BaseRuleParser.AFTER, 0); }
		public TriggerTimeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerTime; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTriggerTime(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTriggerTime(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTriggerTime(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerTimeContext triggerTime() throws RecognitionException {
		TriggerTimeContext _localctx = new TriggerTimeContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_triggerTime);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(398);
			_la = _input.LA(1);
			if ( !(_la==AFTER || _la==BEFORE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UserOrRoleContext extends ParserRuleContext {
		public UserNameContext userName() {
			return getRuleContext(UserNameContext.class,0);
		}
		public RoleNameContext roleName() {
			return getRuleContext(RoleNameContext.class,0);
		}
		public UserOrRoleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_userOrRole; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterUserOrRole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitUserOrRole(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitUserOrRole(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UserOrRoleContext userOrRole() throws RecognitionException {
		UserOrRoleContext _localctx = new UserOrRoleContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_userOrRole);
		try {
			setState(402);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(400);
				userName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(401);
				roleName();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PartitionNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(BaseRuleParser.IDENTIFIER_, 0); }
		public PartitionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_partitionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterPartitionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitPartitionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitPartitionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PartitionNameContext partitionName() throws RecognitionException {
		PartitionNameContext _localctx = new PartitionNameContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_partitionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(404);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerEventContext extends ParserRuleContext {
		public TerminalNode INSERT() { return getToken(BaseRuleParser.INSERT, 0); }
		public TerminalNode UPDATE() { return getToken(BaseRuleParser.UPDATE, 0); }
		public TerminalNode DELETE() { return getToken(BaseRuleParser.DELETE, 0); }
		public TriggerEventContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerEvent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTriggerEvent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTriggerEvent(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTriggerEvent(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerEventContext triggerEvent() throws RecognitionException {
		TriggerEventContext _localctx = new TriggerEventContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_triggerEvent);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(406);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INSERT) | (1L << UPDATE) | (1L << DELETE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerOrderContext extends ParserRuleContext {
		public TriggerNameContext triggerName() {
			return getRuleContext(TriggerNameContext.class,0);
		}
		public TerminalNode FOLLOWS() { return getToken(BaseRuleParser.FOLLOWS, 0); }
		public TerminalNode PRECEDES() { return getToken(BaseRuleParser.PRECEDES, 0); }
		public TriggerOrderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerOrder; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTriggerOrder(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTriggerOrder(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTriggerOrder(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerOrderContext triggerOrder() throws RecognitionException {
		TriggerOrderContext _localctx = new TriggerOrderContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_triggerOrder);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(408);
			_la = _input.LA(1);
			if ( !(_la==FOLLOWS || _la==PRECEDES) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(409);
			triggerName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public NotOperator_Context notOperator_() {
			return getRuleContext(NotOperator_Context.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public BooleanPrimaryContext booleanPrimary() {
			return getRuleContext(BooleanPrimaryContext.class,0);
		}
		public LogicalOperatorContext logicalOperator() {
			return getRuleContext(LogicalOperatorContext.class,0);
		}
		public TerminalNode XOR() { return getToken(BaseRuleParser.XOR, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 92;
		enterRecursionRule(_localctx, 92, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(420);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				{
				setState(412);
				notOperator_();
				setState(413);
				expr(3);
				}
				break;
			case 2:
				{
				setState(415);
				match(LP_);
				setState(416);
				expr(0);
				setState(417);
				match(RP_);
				}
				break;
			case 3:
				{
				setState(419);
				booleanPrimary(0);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(431);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(429);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(422);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(423);
						logicalOperator();
						setState(424);
						expr(6);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(426);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(427);
						match(XOR);
						setState(428);
						expr(5);
						}
						break;
					}
					} 
				}
				setState(433);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LogicalOperatorContext extends ParserRuleContext {
		public TerminalNode OR() { return getToken(BaseRuleParser.OR, 0); }
		public TerminalNode OR_() { return getToken(BaseRuleParser.OR_, 0); }
		public TerminalNode AND() { return getToken(BaseRuleParser.AND, 0); }
		public TerminalNode AND_() { return getToken(BaseRuleParser.AND_, 0); }
		public LogicalOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterLogicalOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitLogicalOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitLogicalOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperatorContext logicalOperator() throws RecognitionException {
		LogicalOperatorContext _localctx = new LogicalOperatorContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_logicalOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(434);
			_la = _input.LA(1);
			if ( !(_la==AND_ || _la==OR_ || _la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotOperator_Context extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(BaseRuleParser.NOT, 0); }
		public TerminalNode NOT_() { return getToken(BaseRuleParser.NOT_, 0); }
		public NotOperator_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notOperator_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterNotOperator_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitNotOperator_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitNotOperator_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotOperator_Context notOperator_() throws RecognitionException {
		NotOperator_Context _localctx = new NotOperator_Context(_ctx, getState());
		enterRule(_localctx, 96, RULE_notOperator_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(436);
			_la = _input.LA(1);
			if ( !(_la==NOT_ || _la==NOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanPrimaryContext extends ParserRuleContext {
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public BooleanPrimaryContext booleanPrimary() {
			return getRuleContext(BooleanPrimaryContext.class,0);
		}
		public TerminalNode IS() { return getToken(BaseRuleParser.IS, 0); }
		public TerminalNode TRUE() { return getToken(BaseRuleParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(BaseRuleParser.FALSE, 0); }
		public TerminalNode UNKNOWN() { return getToken(BaseRuleParser.UNKNOWN, 0); }
		public TerminalNode NULL() { return getToken(BaseRuleParser.NULL, 0); }
		public TerminalNode NOT() { return getToken(BaseRuleParser.NOT, 0); }
		public TerminalNode SAFE_EQ_() { return getToken(BaseRuleParser.SAFE_EQ_, 0); }
		public ComparisonOperatorContext comparisonOperator() {
			return getRuleContext(ComparisonOperatorContext.class,0);
		}
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode ALL() { return getToken(BaseRuleParser.ALL, 0); }
		public TerminalNode ANY() { return getToken(BaseRuleParser.ANY, 0); }
		public BooleanPrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanPrimary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterBooleanPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitBooleanPrimary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitBooleanPrimary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanPrimaryContext booleanPrimary() throws RecognitionException {
		return booleanPrimary(0);
	}

	private BooleanPrimaryContext booleanPrimary(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BooleanPrimaryContext _localctx = new BooleanPrimaryContext(_ctx, _parentState);
		BooleanPrimaryContext _prevctx = _localctx;
		int _startState = 98;
		enterRecursionRule(_localctx, 98, RULE_booleanPrimary, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(439);
			predicate();
			}
			_ctx.stop = _input.LT(-1);
			setState(461);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(459);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
					case 1:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(441);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(442);
						match(IS);
						setState(444);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==NOT) {
							{
							setState(443);
							match(NOT);
							}
						}

						setState(446);
						_la = _input.LA(1);
						if ( !(((((_la - 102)) & ~0x3f) == 0 && ((1L << (_la - 102)) & ((1L << (NULL - 102)) | (1L << (TRUE - 102)) | (1L << (FALSE - 102)))) != 0) || _la==UNKNOWN) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						break;
					case 2:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(447);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(448);
						match(SAFE_EQ_);
						setState(449);
						predicate();
						}
						break;
					case 3:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(450);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(451);
						comparisonOperator();
						setState(452);
						predicate();
						}
						break;
					case 4:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(454);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(455);
						comparisonOperator();
						setState(456);
						_la = _input.LA(1);
						if ( !(_la==ALL || _la==ANY) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(457);
						subquery();
						}
						break;
					}
					} 
				}
				setState(463);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ComparisonOperatorContext extends ParserRuleContext {
		public TerminalNode EQ_() { return getToken(BaseRuleParser.EQ_, 0); }
		public TerminalNode GTE_() { return getToken(BaseRuleParser.GTE_, 0); }
		public TerminalNode GT_() { return getToken(BaseRuleParser.GT_, 0); }
		public TerminalNode LTE_() { return getToken(BaseRuleParser.LTE_, 0); }
		public TerminalNode LT_() { return getToken(BaseRuleParser.LT_, 0); }
		public TerminalNode NEQ_() { return getToken(BaseRuleParser.NEQ_, 0); }
		public ComparisonOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparisonOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterComparisonOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitComparisonOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitComparisonOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparisonOperatorContext comparisonOperator() throws RecognitionException {
		ComparisonOperatorContext _localctx = new ComparisonOperatorContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_comparisonOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(464);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ_) | (1L << NEQ_) | (1L << GT_) | (1L << GTE_) | (1L << LT_) | (1L << LTE_))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateContext extends ParserRuleContext {
		public List<BitExprContext> bitExpr() {
			return getRuleContexts(BitExprContext.class);
		}
		public BitExprContext bitExpr(int i) {
			return getRuleContext(BitExprContext.class,i);
		}
		public TerminalNode IN() { return getToken(BaseRuleParser.IN, 0); }
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode NOT() { return getToken(BaseRuleParser.NOT, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public TerminalNode BETWEEN() { return getToken(BaseRuleParser.BETWEEN, 0); }
		public TerminalNode AND() { return getToken(BaseRuleParser.AND, 0); }
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public TerminalNode SOUNDS() { return getToken(BaseRuleParser.SOUNDS, 0); }
		public TerminalNode LIKE() { return getToken(BaseRuleParser.LIKE, 0); }
		public List<SimpleExprContext> simpleExpr() {
			return getRuleContexts(SimpleExprContext.class);
		}
		public SimpleExprContext simpleExpr(int i) {
			return getRuleContext(SimpleExprContext.class,i);
		}
		public TerminalNode ESCAPE() { return getToken(BaseRuleParser.ESCAPE, 0); }
		public TerminalNode REGEXP() { return getToken(BaseRuleParser.REGEXP, 0); }
		public TerminalNode RLIKE() { return getToken(BaseRuleParser.RLIKE, 0); }
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterPredicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitPredicate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitPredicate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_predicate);
		int _la;
		try {
			setState(521);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(466);
				bitExpr(0);
				setState(468);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(467);
					match(NOT);
					}
				}

				setState(470);
				match(IN);
				setState(471);
				subquery();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(473);
				bitExpr(0);
				setState(475);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(474);
					match(NOT);
					}
				}

				setState(477);
				match(IN);
				setState(478);
				match(LP_);
				setState(479);
				expr(0);
				setState(484);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(480);
					match(COMMA_);
					setState(481);
					expr(0);
					}
					}
					setState(486);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(487);
				match(RP_);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(489);
				bitExpr(0);
				setState(491);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(490);
					match(NOT);
					}
				}

				setState(493);
				match(BETWEEN);
				setState(494);
				bitExpr(0);
				setState(495);
				match(AND);
				setState(496);
				predicate();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(498);
				bitExpr(0);
				setState(499);
				match(SOUNDS);
				setState(500);
				match(LIKE);
				setState(501);
				bitExpr(0);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(503);
				bitExpr(0);
				setState(505);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(504);
					match(NOT);
					}
				}

				setState(507);
				match(LIKE);
				setState(508);
				simpleExpr(0);
				setState(511);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
				case 1:
					{
					setState(509);
					match(ESCAPE);
					setState(510);
					simpleExpr(0);
					}
					break;
				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(513);
				bitExpr(0);
				setState(515);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(514);
					match(NOT);
					}
				}

				setState(517);
				_la = _input.LA(1);
				if ( !(_la==REGEXP || _la==RLIKE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(518);
				bitExpr(0);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(520);
				bitExpr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitExprContext extends ParserRuleContext {
		public SimpleExprContext simpleExpr() {
			return getRuleContext(SimpleExprContext.class,0);
		}
		public List<BitExprContext> bitExpr() {
			return getRuleContexts(BitExprContext.class);
		}
		public BitExprContext bitExpr(int i) {
			return getRuleContext(BitExprContext.class,i);
		}
		public TerminalNode VERTICAL_BAR_() { return getToken(BaseRuleParser.VERTICAL_BAR_, 0); }
		public TerminalNode AMPERSAND_() { return getToken(BaseRuleParser.AMPERSAND_, 0); }
		public TerminalNode SIGNED_LEFT_SHIFT_() { return getToken(BaseRuleParser.SIGNED_LEFT_SHIFT_, 0); }
		public TerminalNode SIGNED_RIGHT_SHIFT_() { return getToken(BaseRuleParser.SIGNED_RIGHT_SHIFT_, 0); }
		public TerminalNode PLUS_() { return getToken(BaseRuleParser.PLUS_, 0); }
		public TerminalNode MINUS_() { return getToken(BaseRuleParser.MINUS_, 0); }
		public TerminalNode ASTERISK_() { return getToken(BaseRuleParser.ASTERISK_, 0); }
		public TerminalNode SLASH_() { return getToken(BaseRuleParser.SLASH_, 0); }
		public TerminalNode DIV() { return getToken(BaseRuleParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(BaseRuleParser.MOD, 0); }
		public TerminalNode MOD_() { return getToken(BaseRuleParser.MOD_, 0); }
		public TerminalNode CARET_() { return getToken(BaseRuleParser.CARET_, 0); }
		public IntervalExpressionContext intervalExpression() {
			return getRuleContext(IntervalExpressionContext.class,0);
		}
		public BitExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterBitExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitBitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitBitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BitExprContext bitExpr() throws RecognitionException {
		return bitExpr(0);
	}

	private BitExprContext bitExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BitExprContext _localctx = new BitExprContext(_ctx, _parentState);
		BitExprContext _prevctx = _localctx;
		int _startState = 104;
		enterRecursionRule(_localctx, 104, RULE_bitExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(524);
			simpleExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(570);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(568);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
					case 1:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(526);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(527);
						match(VERTICAL_BAR_);
						setState(528);
						bitExpr(16);
						}
						break;
					case 2:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(529);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(530);
						match(AMPERSAND_);
						setState(531);
						bitExpr(15);
						}
						break;
					case 3:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(532);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(533);
						match(SIGNED_LEFT_SHIFT_);
						setState(534);
						bitExpr(14);
						}
						break;
					case 4:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(535);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(536);
						match(SIGNED_RIGHT_SHIFT_);
						setState(537);
						bitExpr(13);
						}
						break;
					case 5:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(538);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(539);
						match(PLUS_);
						setState(540);
						bitExpr(12);
						}
						break;
					case 6:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(541);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(542);
						match(MINUS_);
						setState(543);
						bitExpr(11);
						}
						break;
					case 7:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(544);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(545);
						match(ASTERISK_);
						setState(546);
						bitExpr(10);
						}
						break;
					case 8:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(547);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(548);
						match(SLASH_);
						setState(549);
						bitExpr(9);
						}
						break;
					case 9:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(550);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(551);
						match(DIV);
						setState(552);
						bitExpr(8);
						}
						break;
					case 10:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(553);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(554);
						match(MOD);
						setState(555);
						bitExpr(7);
						}
						break;
					case 11:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(556);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(557);
						match(MOD_);
						setState(558);
						bitExpr(6);
						}
						break;
					case 12:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(559);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(560);
						match(CARET_);
						setState(561);
						bitExpr(5);
						}
						break;
					case 13:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(562);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(563);
						match(PLUS_);
						setState(564);
						intervalExpression();
						}
						break;
					case 14:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(565);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(566);
						match(MINUS_);
						setState(567);
						intervalExpression();
						}
						break;
					}
					} 
				}
				setState(572);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SimpleExprContext extends ParserRuleContext {
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ParameterMarkerContext parameterMarker() {
			return getRuleContext(ParameterMarkerContext.class,0);
		}
		public LiteralsContext literals() {
			return getRuleContext(LiteralsContext.class,0);
		}
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public List<SimpleExprContext> simpleExpr() {
			return getRuleContexts(SimpleExprContext.class);
		}
		public SimpleExprContext simpleExpr(int i) {
			return getRuleContext(SimpleExprContext.class,i);
		}
		public TerminalNode PLUS_() { return getToken(BaseRuleParser.PLUS_, 0); }
		public TerminalNode MINUS_() { return getToken(BaseRuleParser.MINUS_, 0); }
		public TerminalNode TILDE_() { return getToken(BaseRuleParser.TILDE_, 0); }
		public TerminalNode NOT_() { return getToken(BaseRuleParser.NOT_, 0); }
		public TerminalNode BINARY() { return getToken(BaseRuleParser.BINARY, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TerminalNode ROW() { return getToken(BaseRuleParser.ROW, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode EXISTS() { return getToken(BaseRuleParser.EXISTS, 0); }
		public TerminalNode LBE_() { return getToken(BaseRuleParser.LBE_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode RBE_() { return getToken(BaseRuleParser.RBE_, 0); }
		public MatchExpression_Context matchExpression_() {
			return getRuleContext(MatchExpression_Context.class,0);
		}
		public CaseExpressionContext caseExpression() {
			return getRuleContext(CaseExpressionContext.class,0);
		}
		public IntervalExpressionContext intervalExpression() {
			return getRuleContext(IntervalExpressionContext.class,0);
		}
		public TerminalNode OR_() { return getToken(BaseRuleParser.OR_, 0); }
		public TerminalNode COLLATE() { return getToken(BaseRuleParser.COLLATE, 0); }
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public SimpleExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterSimpleExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitSimpleExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitSimpleExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleExprContext simpleExpr() throws RecognitionException {
		return simpleExpr(0);
	}

	private SimpleExprContext simpleExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SimpleExprContext _localctx = new SimpleExprContext(_ctx, _parentState);
		SimpleExprContext _prevctx = _localctx;
		int _startState = 106;
		enterRecursionRule(_localctx, 106, RULE_simpleExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(607);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
			case 1:
				{
				setState(574);
				functionCall();
				}
				break;
			case 2:
				{
				setState(575);
				parameterMarker();
				}
				break;
			case 3:
				{
				setState(576);
				literals();
				}
				break;
			case 4:
				{
				setState(577);
				columnName();
				}
				break;
			case 5:
				{
				setState(578);
				variable();
				}
				break;
			case 6:
				{
				setState(579);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT_) | (1L << TILDE_) | (1L << PLUS_) | (1L << MINUS_))) != 0) || _la==BINARY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(580);
				simpleExpr(7);
				}
				break;
			case 7:
				{
				setState(582);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ROW) {
					{
					setState(581);
					match(ROW);
					}
				}

				setState(584);
				match(LP_);
				setState(585);
				expr(0);
				setState(590);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(586);
					match(COMMA_);
					setState(587);
					expr(0);
					}
					}
					setState(592);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(593);
				match(RP_);
				}
				break;
			case 8:
				{
				setState(596);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXISTS) {
					{
					setState(595);
					match(EXISTS);
					}
				}

				setState(598);
				subquery();
				}
				break;
			case 9:
				{
				setState(599);
				match(LBE_);
				setState(600);
				identifier();
				setState(601);
				expr(0);
				setState(602);
				match(RBE_);
				}
				break;
			case 10:
				{
				setState(604);
				matchExpression_();
				}
				break;
			case 11:
				{
				setState(605);
				caseExpression();
				}
				break;
			case 12:
				{
				setState(606);
				intervalExpression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(620);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(618);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
					case 1:
						{
						_localctx = new SimpleExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_simpleExpr);
						setState(609);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(610);
						match(OR_);
						setState(611);
						simpleExpr(9);
						}
						break;
					case 2:
						{
						_localctx = new SimpleExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_simpleExpr);
						setState(612);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(613);
						match(COLLATE);
						setState(616);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case STRING_:
							{
							setState(614);
							match(STRING_);
							}
							break;
						case TRUNCATE:
						case POSITION:
						case VIEW:
						case ANY:
						case OFFSET:
						case BEGIN:
						case COMMIT:
						case ROLLBACK:
						case SAVEPOINT:
						case BOOLEAN:
						case DATE:
						case TIME:
						case TIMESTAMP:
						case YEAR:
						case QUARTER:
						case MONTH:
						case WEEK:
						case DAY:
						case HOUR:
						case MINUTE:
						case SECOND:
						case MICROSECOND:
						case MAX:
						case MIN:
						case SUM:
						case COUNT:
						case AVG:
						case CURRENT:
						case ENABLE:
						case DISABLE:
						case INSTANCE:
						case DO:
						case DEFINER:
						case CASCADED:
						case LOCAL:
						case CLOSE:
						case OPEN:
						case NEXT:
						case NAME:
						case TYPE:
						case TABLES:
						case TABLESPACE:
						case COLUMNS:
						case FIELDS:
						case INDEXES:
						case STATUS:
						case MODIFY:
						case VALUE:
						case DUPLICATE:
						case FIRST:
						case LAST:
						case AFTER:
						case OJ:
						case ACCOUNT:
						case USER:
						case ROLE:
						case START:
						case TRANSACTION:
						case WITHOUT:
						case ESCAPE:
						case SUBPARTITION:
						case STORAGE:
						case SUPER:
						case TEMPORARY:
						case THAN:
						case UNBOUNDED:
						case SIGNED:
						case UPGRADE:
						case VALIDATION:
						case ROLLUP:
						case SOUNDS:
						case UNKNOWN:
						case OFF:
						case ALWAYS:
						case COMMITTED:
						case LEVEL:
						case NO:
						case PASSWORD:
						case PRIVILEGES:
						case ACTION:
						case ALGORITHM:
						case AUTOCOMMIT:
						case BTREE:
						case CHAIN:
						case CHARSET:
						case CHECKSUM:
						case CIPHER:
						case CLIENT:
						case COALESCE:
						case COMMENT:
						case COMPACT:
						case COMPRESSED:
						case COMPRESSION:
						case CONNECTION:
						case CONSISTENT:
						case DATA:
						case DISCARD:
						case DISK:
						case ENCRYPTION:
						case END:
						case ENGINE:
						case EVENT:
						case EXCHANGE:
						case EXECUTE:
						case FILE:
						case FIXED:
						case FOLLOWING:
						case GLOBAL:
						case HASH:
						case IMPORT_:
						case LESS:
						case MEMORY:
						case NONE:
						case PARSER:
						case PARTIAL:
						case PARTITIONING:
						case PERSIST:
						case PRECEDING:
						case PROCESS:
						case PROXY:
						case QUICK:
						case REBUILD:
						case REDUNDANT:
						case RELOAD:
						case REMOVE:
						case REORGANIZE:
						case REPAIR:
						case REVERSE:
						case SESSION:
						case SHUTDOWN:
						case SIMPLE:
						case SLAVE:
						case VISIBLE:
						case INVISIBLE:
						case ENFORCED:
						case AGAINST:
						case LANGUAGE:
						case MODE:
						case QUERY:
						case EXTENDED:
						case EXPANSION:
						case VARIANCE:
						case MAX_ROWS:
						case MIN_ROWS:
						case SQL_BIG_RESULT:
						case SQL_BUFFER_RESULT:
						case SQL_CACHE:
						case SQL_NO_CACHE:
						case STATS_AUTO_RECALC:
						case STATS_PERSISTENT:
						case STATS_SAMPLE_PAGES:
						case ROW_FORMAT:
						case WEIGHT_STRING:
						case COLUMN_FORMAT:
						case INSERT_METHOD:
						case KEY_BLOCK_SIZE:
						case PACK_KEYS:
						case PERSIST_ONLY:
						case BIT_AND:
						case BIT_OR:
						case BIT_XOR:
						case GROUP_CONCAT:
						case JSON_ARRAYAGG:
						case JSON_OBJECTAGG:
						case STD:
						case STDDEV:
						case STDDEV_POP:
						case STDDEV_SAMP:
						case VAR_POP:
						case VAR_SAMP:
						case AUTO_INCREMENT:
						case AVG_ROW_LENGTH:
						case DELAY_KEY_WRITE:
						case ROTATE:
						case MASTER:
						case BINLOG:
						case ERROR:
						case SCHEDULE:
						case COMPLETION:
						case EVERY:
						case HOST:
						case SOCKET:
						case PORT:
						case SERVER:
						case WRAPPER:
						case OPTIONS:
						case OWNER:
						case RETURNS:
						case CONTAINS:
						case SECURITY:
						case INVOKER:
						case TEMPTABLE:
						case MERGE:
						case UNDEFINED:
						case DATAFILE:
						case FILE_BLOCK_SIZE:
						case EXTENT_SIZE:
						case INITIAL_SIZE:
						case AUTOEXTEND_SIZE:
						case MAX_SIZE:
						case NODEGROUP:
						case WAIT:
						case LOGFILE:
						case UNDOFILE:
						case UNDO_BUFFER_SIZE:
						case REDO_BUFFER_SIZE:
						case HANDLER:
						case PREV:
						case ORGANIZATION:
						case DEFINITION:
						case DESCRIPTION:
						case REFERENCE:
						case FOLLOWS:
						case PRECEDES:
						case IMPORT:
						case CONCURRENT:
						case XML:
						case DUMPFILE:
						case SHARE:
						case CODE:
						case CONTEXT:
						case SOURCE:
						case CHANNEL:
						case CLONE:
						case AGGREGATE:
						case INSTALL:
						case COMPONENT:
						case UNINSTALL:
						case RESOURCE:
						case EXPIRE:
						case NEVER:
						case HISTORY:
						case OPTIONAL:
						case REUSE:
						case MAX_QUERIES_PER_HOUR:
						case MAX_UPDATES_PER_HOUR:
						case MAX_CONNECTIONS_PER_HOUR:
						case MAX_USER_CONNECTIONS:
						case RETAIN:
						case RANDOM:
						case OLD:
						case ISSUER:
						case SUBJECT:
						case CACHE:
						case GENERAL:
						case SLOW:
						case USER_RESOURCES:
						case EXPORT:
						case RELAY:
						case HOSTS:
						case FLUSH:
						case RESET:
						case RESTART:
						case IO_THREAD:
						case SQL_THREAD:
						case SQL_BEFORE_GTIDS:
						case SQL_AFTER_GTIDS:
						case MASTER_LOG_FILE:
						case MASTER_LOG_POS:
						case RELAY_LOG_FILE:
						case RELAY_LOG_POS:
						case SQL_AFTER_MTS_GAPS:
						case UNTIL:
						case DEFAULT_AUTH:
						case PLUGIN_DIR:
						case STOP:
						case IDENTIFIER_:
							{
							setState(615);
							identifier();
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						}
						break;
					}
					} 
				}
				setState(622);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public AggregationFunctionContext aggregationFunction() {
			return getRuleContext(AggregationFunctionContext.class,0);
		}
		public SpecialFunctionContext specialFunction() {
			return getRuleContext(SpecialFunctionContext.class,0);
		}
		public RegularFunctionContext regularFunction() {
			return getRuleContext(RegularFunctionContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitFunctionCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_functionCall);
		try {
			setState(626);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(623);
				aggregationFunction();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(624);
				specialFunction();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(625);
				regularFunction();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationFunctionContext extends ParserRuleContext {
		public AggregationFunctionNameContext aggregationFunctionName() {
			return getRuleContext(AggregationFunctionNameContext.class,0);
		}
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public DistinctContext distinct() {
			return getRuleContext(DistinctContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASTERISK_() { return getToken(BaseRuleParser.ASTERISK_, 0); }
		public OverClause_Context overClause_() {
			return getRuleContext(OverClause_Context.class,0);
		}
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public AggregationFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterAggregationFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitAggregationFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitAggregationFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregationFunctionContext aggregationFunction() throws RecognitionException {
		AggregationFunctionContext _localctx = new AggregationFunctionContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_aggregationFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(628);
			aggregationFunctionName();
			setState(629);
			match(LP_);
			setState(631);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DISTINCT) {
				{
				setState(630);
				distinct();
				}
			}

			setState(642);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(633);
				expr(0);
				setState(638);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(634);
					match(COMMA_);
					setState(635);
					expr(0);
					}
					}
					setState(640);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ASTERISK_:
				{
				setState(641);
				match(ASTERISK_);
				}
				break;
			case RP_:
				break;
			default:
				break;
			}
			setState(644);
			match(RP_);
			setState(646);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,59,_ctx) ) {
			case 1:
				{
				setState(645);
				overClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationFunctionNameContext extends ParserRuleContext {
		public TerminalNode MAX() { return getToken(BaseRuleParser.MAX, 0); }
		public TerminalNode MIN() { return getToken(BaseRuleParser.MIN, 0); }
		public TerminalNode SUM() { return getToken(BaseRuleParser.SUM, 0); }
		public TerminalNode COUNT() { return getToken(BaseRuleParser.COUNT, 0); }
		public TerminalNode AVG() { return getToken(BaseRuleParser.AVG, 0); }
		public TerminalNode BIT_AND() { return getToken(BaseRuleParser.BIT_AND, 0); }
		public TerminalNode BIT_OR() { return getToken(BaseRuleParser.BIT_OR, 0); }
		public TerminalNode BIT_XOR() { return getToken(BaseRuleParser.BIT_XOR, 0); }
		public TerminalNode JSON_ARRAYAGG() { return getToken(BaseRuleParser.JSON_ARRAYAGG, 0); }
		public TerminalNode JSON_OBJECTAGG() { return getToken(BaseRuleParser.JSON_OBJECTAGG, 0); }
		public TerminalNode STD() { return getToken(BaseRuleParser.STD, 0); }
		public TerminalNode STDDEV() { return getToken(BaseRuleParser.STDDEV, 0); }
		public TerminalNode STDDEV_POP() { return getToken(BaseRuleParser.STDDEV_POP, 0); }
		public TerminalNode STDDEV_SAMP() { return getToken(BaseRuleParser.STDDEV_SAMP, 0); }
		public TerminalNode VAR_POP() { return getToken(BaseRuleParser.VAR_POP, 0); }
		public TerminalNode VAR_SAMP() { return getToken(BaseRuleParser.VAR_SAMP, 0); }
		public TerminalNode VARIANCE() { return getToken(BaseRuleParser.VARIANCE, 0); }
		public AggregationFunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationFunctionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterAggregationFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitAggregationFunctionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitAggregationFunctionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregationFunctionNameContext aggregationFunctionName() throws RecognitionException {
		AggregationFunctionNameContext _localctx = new AggregationFunctionNameContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_aggregationFunctionName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(648);
			_la = _input.LA(1);
			if ( !(((((_la - 143)) & ~0x3f) == 0 && ((1L << (_la - 143)) & ((1L << (MAX - 143)) | (1L << (MIN - 143)) | (1L << (SUM - 143)) | (1L << (COUNT - 143)) | (1L << (AVG - 143)))) != 0) || ((((_la - 354)) & ~0x3f) == 0 && ((1L << (_la - 354)) & ((1L << (VARIANCE - 354)) | (1L << (BIT_AND - 354)) | (1L << (BIT_OR - 354)) | (1L << (BIT_XOR - 354)) | (1L << (JSON_ARRAYAGG - 354)) | (1L << (JSON_OBJECTAGG - 354)) | (1L << (STD - 354)) | (1L << (STDDEV - 354)) | (1L << (STDDEV_POP - 354)) | (1L << (STDDEV_SAMP - 354)) | (1L << (VAR_POP - 354)) | (1L << (VAR_SAMP - 354)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DistinctContext extends ParserRuleContext {
		public TerminalNode DISTINCT() { return getToken(BaseRuleParser.DISTINCT, 0); }
		public DistinctContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_distinct; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterDistinct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitDistinct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitDistinct(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DistinctContext distinct() throws RecognitionException {
		DistinctContext _localctx = new DistinctContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_distinct);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(650);
			match(DISTINCT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OverClause_Context extends ParserRuleContext {
		public TerminalNode OVER() { return getToken(BaseRuleParser.OVER, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public WindowSpecification_Context windowSpecification_() {
			return getRuleContext(WindowSpecification_Context.class,0);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OverClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_overClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterOverClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitOverClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitOverClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OverClause_Context overClause_() throws RecognitionException {
		OverClause_Context _localctx = new OverClause_Context(_ctx, getState());
		enterRule(_localctx, 116, RULE_overClause_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(652);
			match(OVER);
			setState(658);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LP_:
				{
				setState(653);
				match(LP_);
				setState(654);
				windowSpecification_();
				setState(655);
				match(RP_);
				}
				break;
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				{
				setState(657);
				identifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowSpecification_Context extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public PartitionClause_Context partitionClause_() {
			return getRuleContext(PartitionClause_Context.class,0);
		}
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public FrameClause_Context frameClause_() {
			return getRuleContext(FrameClause_Context.class,0);
		}
		public WindowSpecification_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowSpecification_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterWindowSpecification_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitWindowSpecification_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitWindowSpecification_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowSpecification_Context windowSpecification_() throws RecognitionException {
		WindowSpecification_Context _localctx = new WindowSpecification_Context(_ctx, getState());
		enterRule(_localctx, 118, RULE_windowSpecification_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(661);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 50)) & ~0x3f) == 0 && ((1L << (_la - 50)) & ((1L << (TRUNCATE - 50)) | (1L << (POSITION - 50)) | (1L << (VIEW - 50)) | (1L << (ANY - 50)))) != 0) || ((((_la - 118)) & ~0x3f) == 0 && ((1L << (_la - 118)) & ((1L << (OFFSET - 118)) | (1L << (BEGIN - 118)) | (1L << (COMMIT - 118)) | (1L << (ROLLBACK - 118)) | (1L << (SAVEPOINT - 118)) | (1L << (BOOLEAN - 118)) | (1L << (DATE - 118)) | (1L << (TIME - 118)) | (1L << (TIMESTAMP - 118)) | (1L << (YEAR - 118)) | (1L << (QUARTER - 118)) | (1L << (MONTH - 118)) | (1L << (WEEK - 118)) | (1L << (DAY - 118)) | (1L << (HOUR - 118)) | (1L << (MINUTE - 118)) | (1L << (SECOND - 118)) | (1L << (MICROSECOND - 118)) | (1L << (MAX - 118)) | (1L << (MIN - 118)) | (1L << (SUM - 118)) | (1L << (COUNT - 118)) | (1L << (AVG - 118)) | (1L << (CURRENT - 118)) | (1L << (ENABLE - 118)) | (1L << (DISABLE - 118)) | (1L << (INSTANCE - 118)) | (1L << (DO - 118)) | (1L << (DEFINER - 118)) | (1L << (CASCADED - 118)) | (1L << (LOCAL - 118)) | (1L << (CLOSE - 118)) | (1L << (OPEN - 118)) | (1L << (NEXT - 118)) | (1L << (NAME - 118)) | (1L << (TYPE - 118)))) != 0) || ((((_la - 186)) & ~0x3f) == 0 && ((1L << (_la - 186)) & ((1L << (TABLES - 186)) | (1L << (TABLESPACE - 186)) | (1L << (COLUMNS - 186)) | (1L << (FIELDS - 186)) | (1L << (INDEXES - 186)) | (1L << (STATUS - 186)) | (1L << (MODIFY - 186)) | (1L << (VALUE - 186)) | (1L << (DUPLICATE - 186)) | (1L << (FIRST - 186)) | (1L << (LAST - 186)) | (1L << (AFTER - 186)) | (1L << (OJ - 186)) | (1L << (ACCOUNT - 186)) | (1L << (USER - 186)) | (1L << (ROLE - 186)) | (1L << (START - 186)) | (1L << (TRANSACTION - 186)) | (1L << (WITHOUT - 186)) | (1L << (ESCAPE - 186)) | (1L << (SUBPARTITION - 186)) | (1L << (STORAGE - 186)) | (1L << (SUPER - 186)) | (1L << (TEMPORARY - 186)) | (1L << (THAN - 186)) | (1L << (UNBOUNDED - 186)) | (1L << (SIGNED - 186)) | (1L << (UPGRADE - 186)) | (1L << (VALIDATION - 186)) | (1L << (ROLLUP - 186)) | (1L << (SOUNDS - 186)) | (1L << (UNKNOWN - 186)) | (1L << (OFF - 186)) | (1L << (ALWAYS - 186)) | (1L << (COMMITTED - 186)) | (1L << (LEVEL - 186)) | (1L << (NO - 186)) | (1L << (PASSWORD - 186)) | (1L << (PRIVILEGES - 186)))) != 0) || ((((_la - 251)) & ~0x3f) == 0 && ((1L << (_la - 251)) & ((1L << (ACTION - 251)) | (1L << (ALGORITHM - 251)) | (1L << (AUTOCOMMIT - 251)) | (1L << (BTREE - 251)) | (1L << (CHAIN - 251)) | (1L << (CHARSET - 251)) | (1L << (CHECKSUM - 251)) | (1L << (CIPHER - 251)) | (1L << (CLIENT - 251)) | (1L << (COALESCE - 251)) | (1L << (COMMENT - 251)) | (1L << (COMPACT - 251)) | (1L << (COMPRESSED - 251)) | (1L << (COMPRESSION - 251)) | (1L << (CONNECTION - 251)) | (1L << (CONSISTENT - 251)) | (1L << (DATA - 251)) | (1L << (DISCARD - 251)) | (1L << (DISK - 251)) | (1L << (ENCRYPTION - 251)) | (1L << (END - 251)) | (1L << (ENGINE - 251)) | (1L << (EVENT - 251)) | (1L << (EXCHANGE - 251)) | (1L << (EXECUTE - 251)) | (1L << (FILE - 251)) | (1L << (FIXED - 251)) | (1L << (FOLLOWING - 251)) | (1L << (GLOBAL - 251)) | (1L << (HASH - 251)) | (1L << (IMPORT_ - 251)) | (1L << (LESS - 251)) | (1L << (MEMORY - 251)) | (1L << (NONE - 251)) | (1L << (PARSER - 251)) | (1L << (PARTIAL - 251)))) != 0) || ((((_la - 315)) & ~0x3f) == 0 && ((1L << (_la - 315)) & ((1L << (PARTITIONING - 315)) | (1L << (PERSIST - 315)) | (1L << (PRECEDING - 315)) | (1L << (PROCESS - 315)) | (1L << (PROXY - 315)) | (1L << (QUICK - 315)) | (1L << (REBUILD - 315)) | (1L << (REDUNDANT - 315)) | (1L << (RELOAD - 315)) | (1L << (REMOVE - 315)) | (1L << (REORGANIZE - 315)) | (1L << (REPAIR - 315)) | (1L << (REVERSE - 315)) | (1L << (SESSION - 315)) | (1L << (SHUTDOWN - 315)) | (1L << (SIMPLE - 315)) | (1L << (SLAVE - 315)) | (1L << (VISIBLE - 315)) | (1L << (INVISIBLE - 315)) | (1L << (ENFORCED - 315)) | (1L << (AGAINST - 315)) | (1L << (LANGUAGE - 315)) | (1L << (MODE - 315)) | (1L << (QUERY - 315)) | (1L << (EXTENDED - 315)) | (1L << (EXPANSION - 315)) | (1L << (VARIANCE - 315)) | (1L << (MAX_ROWS - 315)) | (1L << (MIN_ROWS - 315)) | (1L << (SQL_BIG_RESULT - 315)) | (1L << (SQL_BUFFER_RESULT - 315)) | (1L << (SQL_CACHE - 315)) | (1L << (SQL_NO_CACHE - 315)) | (1L << (STATS_AUTO_RECALC - 315)) | (1L << (STATS_PERSISTENT - 315)) | (1L << (STATS_SAMPLE_PAGES - 315)) | (1L << (ROW_FORMAT - 315)) | (1L << (WEIGHT_STRING - 315)) | (1L << (COLUMN_FORMAT - 315)))) != 0) || ((((_la - 379)) & ~0x3f) == 0 && ((1L << (_la - 379)) & ((1L << (INSERT_METHOD - 379)) | (1L << (KEY_BLOCK_SIZE - 379)) | (1L << (PACK_KEYS - 379)) | (1L << (PERSIST_ONLY - 379)) | (1L << (BIT_AND - 379)) | (1L << (BIT_OR - 379)) | (1L << (BIT_XOR - 379)) | (1L << (GROUP_CONCAT - 379)) | (1L << (JSON_ARRAYAGG - 379)) | (1L << (JSON_OBJECTAGG - 379)) | (1L << (STD - 379)) | (1L << (STDDEV - 379)) | (1L << (STDDEV_POP - 379)) | (1L << (STDDEV_SAMP - 379)) | (1L << (VAR_POP - 379)) | (1L << (VAR_SAMP - 379)) | (1L << (AUTO_INCREMENT - 379)) | (1L << (AVG_ROW_LENGTH - 379)) | (1L << (DELAY_KEY_WRITE - 379)) | (1L << (ROTATE - 379)) | (1L << (MASTER - 379)) | (1L << (BINLOG - 379)) | (1L << (ERROR - 379)) | (1L << (SCHEDULE - 379)) | (1L << (COMPLETION - 379)) | (1L << (EVERY - 379)) | (1L << (HOST - 379)) | (1L << (SOCKET - 379)) | (1L << (PORT - 379)) | (1L << (SERVER - 379)) | (1L << (WRAPPER - 379)) | (1L << (OPTIONS - 379)) | (1L << (OWNER - 379)) | (1L << (RETURNS - 379)) | (1L << (CONTAINS - 379)) | (1L << (SECURITY - 379)) | (1L << (INVOKER - 379)) | (1L << (TEMPTABLE - 379)) | (1L << (MERGE - 379)))) != 0) || ((((_la - 443)) & ~0x3f) == 0 && ((1L << (_la - 443)) & ((1L << (UNDEFINED - 443)) | (1L << (DATAFILE - 443)) | (1L << (FILE_BLOCK_SIZE - 443)) | (1L << (EXTENT_SIZE - 443)) | (1L << (INITIAL_SIZE - 443)) | (1L << (AUTOEXTEND_SIZE - 443)) | (1L << (MAX_SIZE - 443)) | (1L << (NODEGROUP - 443)) | (1L << (WAIT - 443)) | (1L << (LOGFILE - 443)) | (1L << (UNDOFILE - 443)) | (1L << (UNDO_BUFFER_SIZE - 443)) | (1L << (REDO_BUFFER_SIZE - 443)) | (1L << (HANDLER - 443)) | (1L << (PREV - 443)) | (1L << (ORGANIZATION - 443)) | (1L << (DEFINITION - 443)) | (1L << (DESCRIPTION - 443)) | (1L << (REFERENCE - 443)) | (1L << (FOLLOWS - 443)) | (1L << (PRECEDES - 443)) | (1L << (IMPORT - 443)) | (1L << (CONCURRENT - 443)) | (1L << (XML - 443)) | (1L << (DUMPFILE - 443)) | (1L << (SHARE - 443)) | (1L << (CODE - 443)) | (1L << (CONTEXT - 443)) | (1L << (SOURCE - 443)) | (1L << (CHANNEL - 443)))) != 0) || ((((_la - 507)) & ~0x3f) == 0 && ((1L << (_la - 507)) & ((1L << (CLONE - 507)) | (1L << (AGGREGATE - 507)) | (1L << (INSTALL - 507)) | (1L << (COMPONENT - 507)) | (1L << (UNINSTALL - 507)) | (1L << (RESOURCE - 507)) | (1L << (EXPIRE - 507)) | (1L << (NEVER - 507)) | (1L << (HISTORY - 507)) | (1L << (OPTIONAL - 507)) | (1L << (REUSE - 507)) | (1L << (MAX_QUERIES_PER_HOUR - 507)) | (1L << (MAX_UPDATES_PER_HOUR - 507)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 507)) | (1L << (MAX_USER_CONNECTIONS - 507)) | (1L << (RETAIN - 507)) | (1L << (RANDOM - 507)) | (1L << (OLD - 507)) | (1L << (ISSUER - 507)) | (1L << (SUBJECT - 507)) | (1L << (CACHE - 507)) | (1L << (GENERAL - 507)) | (1L << (SLOW - 507)) | (1L << (USER_RESOURCES - 507)) | (1L << (EXPORT - 507)) | (1L << (RELAY - 507)) | (1L << (HOSTS - 507)) | (1L << (FLUSH - 507)) | (1L << (RESET - 507)) | (1L << (RESTART - 507)))) != 0) || ((((_la - 687)) & ~0x3f) == 0 && ((1L << (_la - 687)) & ((1L << (IO_THREAD - 687)) | (1L << (SQL_THREAD - 687)) | (1L << (SQL_BEFORE_GTIDS - 687)) | (1L << (SQL_AFTER_GTIDS - 687)) | (1L << (MASTER_LOG_FILE - 687)) | (1L << (MASTER_LOG_POS - 687)) | (1L << (RELAY_LOG_FILE - 687)) | (1L << (RELAY_LOG_POS - 687)) | (1L << (SQL_AFTER_MTS_GAPS - 687)) | (1L << (UNTIL - 687)) | (1L << (DEFAULT_AUTH - 687)) | (1L << (PLUGIN_DIR - 687)) | (1L << (STOP - 687)) | (1L << (IDENTIFIER_ - 687)))) != 0)) {
				{
				setState(660);
				identifier();
				}
			}

			setState(664);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARTITION) {
				{
				setState(663);
				partitionClause_();
				}
			}

			setState(667);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(666);
				orderByClause();
				}
			}

			setState(670);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ROWS || _la==RANGE) {
				{
				setState(669);
				frameClause_();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PartitionClause_Context extends ParserRuleContext {
		public TerminalNode PARTITION() { return getToken(BaseRuleParser.PARTITION, 0); }
		public TerminalNode BY() { return getToken(BaseRuleParser.BY, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public PartitionClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_partitionClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterPartitionClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitPartitionClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitPartitionClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PartitionClause_Context partitionClause_() throws RecognitionException {
		PartitionClause_Context _localctx = new PartitionClause_Context(_ctx, getState());
		enterRule(_localctx, 120, RULE_partitionClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(672);
			match(PARTITION);
			setState(673);
			match(BY);
			setState(674);
			expr(0);
			setState(679);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(675);
				match(COMMA_);
				setState(676);
				expr(0);
				}
				}
				setState(681);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameClause_Context extends ParserRuleContext {
		public TerminalNode ROWS() { return getToken(BaseRuleParser.ROWS, 0); }
		public TerminalNode RANGE() { return getToken(BaseRuleParser.RANGE, 0); }
		public FrameStart_Context frameStart_() {
			return getRuleContext(FrameStart_Context.class,0);
		}
		public FrameBetween_Context frameBetween_() {
			return getRuleContext(FrameBetween_Context.class,0);
		}
		public FrameClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterFrameClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitFrameClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitFrameClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameClause_Context frameClause_() throws RecognitionException {
		FrameClause_Context _localctx = new FrameClause_Context(_ctx, getState());
		enterRule(_localctx, 122, RULE_frameClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(682);
			_la = _input.LA(1);
			if ( !(_la==ROWS || _la==RANGE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(685);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(683);
				frameStart_();
				}
				break;
			case BETWEEN:
				{
				setState(684);
				frameBetween_();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameStart_Context extends ParserRuleContext {
		public TerminalNode CURRENT() { return getToken(BaseRuleParser.CURRENT, 0); }
		public TerminalNode ROW() { return getToken(BaseRuleParser.ROW, 0); }
		public TerminalNode UNBOUNDED() { return getToken(BaseRuleParser.UNBOUNDED, 0); }
		public TerminalNode PRECEDING() { return getToken(BaseRuleParser.PRECEDING, 0); }
		public TerminalNode FOLLOWING() { return getToken(BaseRuleParser.FOLLOWING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FrameStart_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameStart_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterFrameStart_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitFrameStart_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitFrameStart_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameStart_Context frameStart_() throws RecognitionException {
		FrameStart_Context _localctx = new FrameStart_Context(_ctx, getState());
		enterRule(_localctx, 124, RULE_frameStart_);
		try {
			setState(699);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(687);
				match(CURRENT);
				setState(688);
				match(ROW);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(689);
				match(UNBOUNDED);
				setState(690);
				match(PRECEDING);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(691);
				match(UNBOUNDED);
				setState(692);
				match(FOLLOWING);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(693);
				expr(0);
				setState(694);
				match(PRECEDING);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(696);
				expr(0);
				setState(697);
				match(FOLLOWING);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameEnd_Context extends ParserRuleContext {
		public FrameStart_Context frameStart_() {
			return getRuleContext(FrameStart_Context.class,0);
		}
		public FrameEnd_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameEnd_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterFrameEnd_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitFrameEnd_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitFrameEnd_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameEnd_Context frameEnd_() throws RecognitionException {
		FrameEnd_Context _localctx = new FrameEnd_Context(_ctx, getState());
		enterRule(_localctx, 126, RULE_frameEnd_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(701);
			frameStart_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameBetween_Context extends ParserRuleContext {
		public TerminalNode BETWEEN() { return getToken(BaseRuleParser.BETWEEN, 0); }
		public FrameStart_Context frameStart_() {
			return getRuleContext(FrameStart_Context.class,0);
		}
		public TerminalNode AND() { return getToken(BaseRuleParser.AND, 0); }
		public FrameEnd_Context frameEnd_() {
			return getRuleContext(FrameEnd_Context.class,0);
		}
		public FrameBetween_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameBetween_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterFrameBetween_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitFrameBetween_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitFrameBetween_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameBetween_Context frameBetween_() throws RecognitionException {
		FrameBetween_Context _localctx = new FrameBetween_Context(_ctx, getState());
		enterRule(_localctx, 128, RULE_frameBetween_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(703);
			match(BETWEEN);
			setState(704);
			frameStart_();
			setState(705);
			match(AND);
			setState(706);
			frameEnd_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpecialFunctionContext extends ParserRuleContext {
		public GroupConcatFunctionContext groupConcatFunction() {
			return getRuleContext(GroupConcatFunctionContext.class,0);
		}
		public WindowFunctionContext windowFunction() {
			return getRuleContext(WindowFunctionContext.class,0);
		}
		public CastFunctionContext castFunction() {
			return getRuleContext(CastFunctionContext.class,0);
		}
		public ConvertFunctionContext convertFunction() {
			return getRuleContext(ConvertFunctionContext.class,0);
		}
		public PositionFunctionContext positionFunction() {
			return getRuleContext(PositionFunctionContext.class,0);
		}
		public SubstringFunctionContext substringFunction() {
			return getRuleContext(SubstringFunctionContext.class,0);
		}
		public ExtractFunctionContext extractFunction() {
			return getRuleContext(ExtractFunctionContext.class,0);
		}
		public CharFunctionContext charFunction() {
			return getRuleContext(CharFunctionContext.class,0);
		}
		public TrimFunction_Context trimFunction_() {
			return getRuleContext(TrimFunction_Context.class,0);
		}
		public WeightStringFunctionContext weightStringFunction() {
			return getRuleContext(WeightStringFunctionContext.class,0);
		}
		public ValuesFunction_Context valuesFunction_() {
			return getRuleContext(ValuesFunction_Context.class,0);
		}
		public SpecialFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_specialFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterSpecialFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitSpecialFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitSpecialFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpecialFunctionContext specialFunction() throws RecognitionException {
		SpecialFunctionContext _localctx = new SpecialFunctionContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_specialFunction);
		try {
			setState(719);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,68,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(708);
				groupConcatFunction();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(709);
				windowFunction();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(710);
				castFunction();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(711);
				convertFunction();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(712);
				positionFunction();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(713);
				substringFunction();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(714);
				extractFunction();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(715);
				charFunction();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(716);
				trimFunction_();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(717);
				weightStringFunction();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(718);
				valuesFunction_();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupConcatFunctionContext extends ParserRuleContext {
		public TerminalNode GROUP_CONCAT() { return getToken(BaseRuleParser.GROUP_CONCAT, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public DistinctContext distinct() {
			return getRuleContext(DistinctContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASTERISK_() { return getToken(BaseRuleParser.ASTERISK_, 0); }
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public TerminalNode SEPARATOR() { return getToken(BaseRuleParser.SEPARATOR, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public GroupConcatFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupConcatFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterGroupConcatFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitGroupConcatFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitGroupConcatFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupConcatFunctionContext groupConcatFunction() throws RecognitionException {
		GroupConcatFunctionContext _localctx = new GroupConcatFunctionContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_groupConcatFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(721);
			match(GROUP_CONCAT);
			setState(722);
			match(LP_);
			setState(724);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DISTINCT) {
				{
				setState(723);
				distinct();
				}
			}

			setState(735);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(726);
				expr(0);
				setState(731);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(727);
					match(COMMA_);
					setState(728);
					expr(0);
					}
					}
					setState(733);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ASTERISK_:
				{
				setState(734);
				match(ASTERISK_);
				}
				break;
			case RP_:
			case ORDER:
			case SEPARATOR:
				break;
			default:
				break;
			}
			setState(738);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(737);
				orderByClause();
				}
			}

			setState(742);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEPARATOR) {
				{
				setState(740);
				match(SEPARATOR);
				setState(741);
				expr(0);
				}
			}

			setState(744);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowFunctionContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public OverClause_Context overClause_() {
			return getRuleContext(OverClause_Context.class,0);
		}
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public WindowFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterWindowFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitWindowFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitWindowFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowFunctionContext windowFunction() throws RecognitionException {
		WindowFunctionContext _localctx = new WindowFunctionContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_windowFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(746);
			identifier();
			setState(747);
			match(LP_);
			setState(748);
			expr(0);
			setState(753);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(749);
				match(COMMA_);
				setState(750);
				expr(0);
				}
				}
				setState(755);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(756);
			match(RP_);
			setState(757);
			overClause_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastFunctionContext extends ParserRuleContext {
		public TerminalNode CAST() { return getToken(BaseRuleParser.CAST, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode AS() { return getToken(BaseRuleParser.AS, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public CastFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_castFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCastFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCastFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCastFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CastFunctionContext castFunction() throws RecognitionException {
		CastFunctionContext _localctx = new CastFunctionContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_castFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(759);
			match(CAST);
			setState(760);
			match(LP_);
			setState(761);
			expr(0);
			setState(762);
			match(AS);
			setState(763);
			dataType();
			setState(764);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConvertFunctionContext extends ParserRuleContext {
		public TerminalNode CONVERT() { return getToken(BaseRuleParser.CONVERT, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode COMMA_() { return getToken(BaseRuleParser.COMMA_, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TerminalNode USING() { return getToken(BaseRuleParser.USING, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ConvertFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_convertFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterConvertFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitConvertFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitConvertFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConvertFunctionContext convertFunction() throws RecognitionException {
		ConvertFunctionContext _localctx = new ConvertFunctionContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_convertFunction);
		try {
			setState(780);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(766);
				match(CONVERT);
				setState(767);
				match(LP_);
				setState(768);
				expr(0);
				setState(769);
				match(COMMA_);
				setState(770);
				dataType();
				setState(771);
				match(RP_);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(773);
				match(CONVERT);
				setState(774);
				match(LP_);
				setState(775);
				expr(0);
				setState(776);
				match(USING);
				setState(777);
				identifier();
				setState(778);
				match(RP_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PositionFunctionContext extends ParserRuleContext {
		public TerminalNode POSITION() { return getToken(BaseRuleParser.POSITION, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode IN() { return getToken(BaseRuleParser.IN, 0); }
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public PositionFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_positionFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterPositionFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitPositionFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitPositionFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PositionFunctionContext positionFunction() throws RecognitionException {
		PositionFunctionContext _localctx = new PositionFunctionContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_positionFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(782);
			match(POSITION);
			setState(783);
			match(LP_);
			setState(784);
			expr(0);
			setState(785);
			match(IN);
			setState(786);
			expr(0);
			setState(787);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubstringFunctionContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode FROM() { return getToken(BaseRuleParser.FROM, 0); }
		public List<TerminalNode> NUMBER_() { return getTokens(BaseRuleParser.NUMBER_); }
		public TerminalNode NUMBER_(int i) {
			return getToken(BaseRuleParser.NUMBER_, i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TerminalNode SUBSTRING() { return getToken(BaseRuleParser.SUBSTRING, 0); }
		public TerminalNode SUBSTR() { return getToken(BaseRuleParser.SUBSTR, 0); }
		public TerminalNode FOR() { return getToken(BaseRuleParser.FOR, 0); }
		public SubstringFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_substringFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterSubstringFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitSubstringFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitSubstringFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubstringFunctionContext substringFunction() throws RecognitionException {
		SubstringFunctionContext _localctx = new SubstringFunctionContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_substringFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(789);
			_la = _input.LA(1);
			if ( !(_la==SUBSTRING || _la==SUBSTR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(790);
			match(LP_);
			setState(791);
			expr(0);
			setState(792);
			match(FROM);
			setState(793);
			match(NUMBER_);
			setState(796);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FOR) {
				{
				setState(794);
				match(FOR);
				setState(795);
				match(NUMBER_);
				}
			}

			setState(798);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtractFunctionContext extends ParserRuleContext {
		public TerminalNode EXTRACT() { return getToken(BaseRuleParser.EXTRACT, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode FROM() { return getToken(BaseRuleParser.FROM, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public ExtractFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extractFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterExtractFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitExtractFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitExtractFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExtractFunctionContext extractFunction() throws RecognitionException {
		ExtractFunctionContext _localctx = new ExtractFunctionContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_extractFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(800);
			match(EXTRACT);
			setState(801);
			match(LP_);
			setState(802);
			identifier();
			setState(803);
			match(FROM);
			setState(804);
			expr(0);
			setState(805);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharFunctionContext extends ParserRuleContext {
		public TerminalNode CHAR() { return getToken(BaseRuleParser.CHAR, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public TerminalNode USING() { return getToken(BaseRuleParser.USING, 0); }
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public CharFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCharFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCharFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCharFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharFunctionContext charFunction() throws RecognitionException {
		CharFunctionContext _localctx = new CharFunctionContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_charFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(807);
			match(CHAR);
			setState(808);
			match(LP_);
			setState(809);
			expr(0);
			setState(814);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(810);
				match(COMMA_);
				setState(811);
				expr(0);
				}
				}
				setState(816);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(819);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==USING) {
				{
				setState(817);
				match(USING);
				setState(818);
				ignoredIdentifier_();
				}
			}

			setState(821);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TrimFunction_Context extends ParserRuleContext {
		public TerminalNode TRIM() { return getToken(BaseRuleParser.TRIM, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(BaseRuleParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(BaseRuleParser.STRING_, i);
		}
		public TerminalNode FROM() { return getToken(BaseRuleParser.FROM, 0); }
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TerminalNode LEADING() { return getToken(BaseRuleParser.LEADING, 0); }
		public TerminalNode BOTH() { return getToken(BaseRuleParser.BOTH, 0); }
		public TerminalNode TRAILING() { return getToken(BaseRuleParser.TRAILING, 0); }
		public TrimFunction_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trimFunction_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterTrimFunction_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitTrimFunction_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitTrimFunction_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TrimFunction_Context trimFunction_() throws RecognitionException {
		TrimFunction_Context _localctx = new TrimFunction_Context(_ctx, getState());
		enterRule(_localctx, 148, RULE_trimFunction_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(823);
			match(TRIM);
			setState(824);
			match(LP_);
			setState(825);
			_la = _input.LA(1);
			if ( !(_la==TRAILING || _la==BOTH || _la==LEADING) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(826);
			match(STRING_);
			setState(827);
			match(FROM);
			setState(828);
			match(STRING_);
			setState(829);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValuesFunction_Context extends ParserRuleContext {
		public TerminalNode VALUES() { return getToken(BaseRuleParser.VALUES, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public ValuesFunction_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valuesFunction_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterValuesFunction_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitValuesFunction_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitValuesFunction_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValuesFunction_Context valuesFunction_() throws RecognitionException {
		ValuesFunction_Context _localctx = new ValuesFunction_Context(_ctx, getState());
		enterRule(_localctx, 150, RULE_valuesFunction_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(831);
			match(VALUES);
			setState(832);
			match(LP_);
			setState(833);
			columnName();
			setState(834);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WeightStringFunctionContext extends ParserRuleContext {
		public TerminalNode WEIGHT_STRING() { return getToken(BaseRuleParser.WEIGHT_STRING, 0); }
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TerminalNode AS() { return getToken(BaseRuleParser.AS, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public LevelClause_Context levelClause_() {
			return getRuleContext(LevelClause_Context.class,0);
		}
		public WeightStringFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_weightStringFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterWeightStringFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitWeightStringFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitWeightStringFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WeightStringFunctionContext weightStringFunction() throws RecognitionException {
		WeightStringFunctionContext _localctx = new WeightStringFunctionContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_weightStringFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(836);
			match(WEIGHT_STRING);
			setState(837);
			match(LP_);
			setState(838);
			expr(0);
			setState(841);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==AS) {
				{
				setState(839);
				match(AS);
				setState(840);
				dataType();
				}
			}

			setState(844);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LEVEL) {
				{
				setState(843);
				levelClause_();
				}
			}

			setState(846);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LevelClause_Context extends ParserRuleContext {
		public TerminalNode LEVEL() { return getToken(BaseRuleParser.LEVEL, 0); }
		public List<LevelInWeightListElement_Context> levelInWeightListElement_() {
			return getRuleContexts(LevelInWeightListElement_Context.class);
		}
		public LevelInWeightListElement_Context levelInWeightListElement_(int i) {
			return getRuleContext(LevelInWeightListElement_Context.class,i);
		}
		public List<TerminalNode> NUMBER_() { return getTokens(BaseRuleParser.NUMBER_); }
		public TerminalNode NUMBER_(int i) {
			return getToken(BaseRuleParser.NUMBER_, i);
		}
		public TerminalNode MINUS_() { return getToken(BaseRuleParser.MINUS_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public LevelClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_levelClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterLevelClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitLevelClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitLevelClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LevelClause_Context levelClause_() throws RecognitionException {
		LevelClause_Context _localctx = new LevelClause_Context(_ctx, getState());
		enterRule(_localctx, 154, RULE_levelClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(848);
			match(LEVEL);
			setState(860);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				{
				setState(849);
				levelInWeightListElement_();
				setState(854);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(850);
					match(COMMA_);
					setState(851);
					levelInWeightListElement_();
					}
					}
					setState(856);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				{
				setState(857);
				match(NUMBER_);
				setState(858);
				match(MINUS_);
				setState(859);
				match(NUMBER_);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LevelInWeightListElement_Context extends ParserRuleContext {
		public TerminalNode NUMBER_() { return getToken(BaseRuleParser.NUMBER_, 0); }
		public TerminalNode REVERSE() { return getToken(BaseRuleParser.REVERSE, 0); }
		public TerminalNode ASC() { return getToken(BaseRuleParser.ASC, 0); }
		public TerminalNode DESC() { return getToken(BaseRuleParser.DESC, 0); }
		public LevelInWeightListElement_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_levelInWeightListElement_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterLevelInWeightListElement_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitLevelInWeightListElement_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitLevelInWeightListElement_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LevelInWeightListElement_Context levelInWeightListElement_() throws RecognitionException {
		LevelInWeightListElement_Context _localctx = new LevelInWeightListElement_Context(_ctx, getState());
		enterRule(_localctx, 156, RULE_levelInWeightListElement_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(862);
			match(NUMBER_);
			setState(864);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASC || _la==DESC) {
				{
				setState(863);
				_la = _input.LA(1);
				if ( !(_la==ASC || _la==DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(867);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==REVERSE) {
				{
				setState(866);
				match(REVERSE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegularFunctionContext extends ParserRuleContext {
		public RegularFunctionName_Context regularFunctionName_() {
			return getRuleContext(RegularFunctionName_Context.class,0);
		}
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASTERISK_() { return getToken(BaseRuleParser.ASTERISK_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public RegularFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_regularFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterRegularFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitRegularFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitRegularFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegularFunctionContext regularFunction() throws RecognitionException {
		RegularFunctionContext _localctx = new RegularFunctionContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_regularFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(869);
			regularFunctionName_();
			setState(870);
			match(LP_);
			setState(880);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(871);
				expr(0);
				setState(876);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(872);
					match(COMMA_);
					setState(873);
					expr(0);
					}
					}
					setState(878);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ASTERISK_:
				{
				setState(879);
				match(ASTERISK_);
				}
				break;
			case RP_:
				break;
			default:
				break;
			}
			setState(882);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegularFunctionName_Context extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode IF() { return getToken(BaseRuleParser.IF, 0); }
		public TerminalNode CURRENT_TIMESTAMP() { return getToken(BaseRuleParser.CURRENT_TIMESTAMP, 0); }
		public TerminalNode UNIX_TIMESTAMP() { return getToken(BaseRuleParser.UNIX_TIMESTAMP, 0); }
		public TerminalNode LOCALTIME() { return getToken(BaseRuleParser.LOCALTIME, 0); }
		public TerminalNode LOCALTIMESTAMP() { return getToken(BaseRuleParser.LOCALTIMESTAMP, 0); }
		public TerminalNode NOW() { return getToken(BaseRuleParser.NOW, 0); }
		public TerminalNode REPLACE() { return getToken(BaseRuleParser.REPLACE, 0); }
		public TerminalNode INTERVAL() { return getToken(BaseRuleParser.INTERVAL, 0); }
		public TerminalNode SUBSTRING() { return getToken(BaseRuleParser.SUBSTRING, 0); }
		public TerminalNode MOD() { return getToken(BaseRuleParser.MOD, 0); }
		public TerminalNode DATABASE() { return getToken(BaseRuleParser.DATABASE, 0); }
		public TerminalNode LEFT() { return getToken(BaseRuleParser.LEFT, 0); }
		public TerminalNode RIGHT() { return getToken(BaseRuleParser.RIGHT, 0); }
		public TerminalNode LOWER() { return getToken(BaseRuleParser.LOWER, 0); }
		public TerminalNode UPPER() { return getToken(BaseRuleParser.UPPER, 0); }
		public TerminalNode DATE() { return getToken(BaseRuleParser.DATE, 0); }
		public TerminalNode DATEDIFF() { return getToken(BaseRuleParser.DATEDIFF, 0); }
		public TerminalNode DATE_FORMAT() { return getToken(BaseRuleParser.DATE_FORMAT, 0); }
		public TerminalNode DAY() { return getToken(BaseRuleParser.DAY, 0); }
		public TerminalNode DAYNAME() { return getToken(BaseRuleParser.DAYNAME, 0); }
		public TerminalNode DAYOFMONTH() { return getToken(BaseRuleParser.DAYOFMONTH, 0); }
		public TerminalNode DAYOFWEEK() { return getToken(BaseRuleParser.DAYOFWEEK, 0); }
		public TerminalNode DAYOFYEAR() { return getToken(BaseRuleParser.DAYOFYEAR, 0); }
		public TerminalNode GEOMCOLLECTION() { return getToken(BaseRuleParser.GEOMCOLLECTION, 0); }
		public TerminalNode GEOMETRYCOLLECTION() { return getToken(BaseRuleParser.GEOMETRYCOLLECTION, 0); }
		public TerminalNode LINESTRING() { return getToken(BaseRuleParser.LINESTRING, 0); }
		public TerminalNode MULTILINESTRING() { return getToken(BaseRuleParser.MULTILINESTRING, 0); }
		public TerminalNode MULTIPOINT() { return getToken(BaseRuleParser.MULTIPOINT, 0); }
		public TerminalNode MULTIPOLYGON() { return getToken(BaseRuleParser.MULTIPOLYGON, 0); }
		public TerminalNode POINT() { return getToken(BaseRuleParser.POINT, 0); }
		public TerminalNode POLYGON() { return getToken(BaseRuleParser.POLYGON, 0); }
		public TerminalNode STR_TO_DATE() { return getToken(BaseRuleParser.STR_TO_DATE, 0); }
		public TerminalNode ST_AREA() { return getToken(BaseRuleParser.ST_AREA, 0); }
		public TerminalNode ST_ASBINARY() { return getToken(BaseRuleParser.ST_ASBINARY, 0); }
		public TerminalNode ST_ASGEOJSON() { return getToken(BaseRuleParser.ST_ASGEOJSON, 0); }
		public TerminalNode ST_ASTEXT() { return getToken(BaseRuleParser.ST_ASTEXT, 0); }
		public TerminalNode ST_ASWKB() { return getToken(BaseRuleParser.ST_ASWKB, 0); }
		public TerminalNode ST_ASWKT() { return getToken(BaseRuleParser.ST_ASWKT, 0); }
		public TerminalNode ST_BUFFER() { return getToken(BaseRuleParser.ST_BUFFER, 0); }
		public TerminalNode ST_BUFFER_STRATEGY() { return getToken(BaseRuleParser.ST_BUFFER_STRATEGY, 0); }
		public TerminalNode ST_CENTROID() { return getToken(BaseRuleParser.ST_CENTROID, 0); }
		public TerminalNode ST_CONTAINS() { return getToken(BaseRuleParser.ST_CONTAINS, 0); }
		public TerminalNode ST_CONVEXHULL() { return getToken(BaseRuleParser.ST_CONVEXHULL, 0); }
		public TerminalNode ST_CROSSES() { return getToken(BaseRuleParser.ST_CROSSES, 0); }
		public TerminalNode ST_DIFFERENCE() { return getToken(BaseRuleParser.ST_DIFFERENCE, 0); }
		public TerminalNode ST_DIMENSION() { return getToken(BaseRuleParser.ST_DIMENSION, 0); }
		public TerminalNode ST_DISJOINT() { return getToken(BaseRuleParser.ST_DISJOINT, 0); }
		public TerminalNode ST_DISTANCE() { return getToken(BaseRuleParser.ST_DISTANCE, 0); }
		public TerminalNode ST_DISTANCE_SPHERE() { return getToken(BaseRuleParser.ST_DISTANCE_SPHERE, 0); }
		public TerminalNode ST_ENDPOINT() { return getToken(BaseRuleParser.ST_ENDPOINT, 0); }
		public TerminalNode ST_ENVELOPE() { return getToken(BaseRuleParser.ST_ENVELOPE, 0); }
		public TerminalNode ST_EQUALS() { return getToken(BaseRuleParser.ST_EQUALS, 0); }
		public TerminalNode ST_EXTERIORRING() { return getToken(BaseRuleParser.ST_EXTERIORRING, 0); }
		public TerminalNode ST_GEOHASH() { return getToken(BaseRuleParser.ST_GEOHASH, 0); }
		public TerminalNode ST_GEOMCOLLFROMTEXT() { return getToken(BaseRuleParser.ST_GEOMCOLLFROMTEXT, 0); }
		public TerminalNode ST_GEOMCOLLFROMTXT() { return getToken(BaseRuleParser.ST_GEOMCOLLFROMTXT, 0); }
		public TerminalNode ST_GEOMCOLLFROMWKB() { return getToken(BaseRuleParser.ST_GEOMCOLLFROMWKB, 0); }
		public TerminalNode ST_GEOMETRYCOLLECTIONFROMTEXT() { return getToken(BaseRuleParser.ST_GEOMETRYCOLLECTIONFROMTEXT, 0); }
		public TerminalNode ST_GEOMETRYCOLLECTIONFROMWKB() { return getToken(BaseRuleParser.ST_GEOMETRYCOLLECTIONFROMWKB, 0); }
		public TerminalNode ST_GEOMETRYFROMTEXT() { return getToken(BaseRuleParser.ST_GEOMETRYFROMTEXT, 0); }
		public TerminalNode ST_GEOMETRYFROMWKB() { return getToken(BaseRuleParser.ST_GEOMETRYFROMWKB, 0); }
		public TerminalNode ST_GEOMETRYN() { return getToken(BaseRuleParser.ST_GEOMETRYN, 0); }
		public TerminalNode ST_GEOMETRYTYPE() { return getToken(BaseRuleParser.ST_GEOMETRYTYPE, 0); }
		public TerminalNode ST_GEOMFROMGEOJSON() { return getToken(BaseRuleParser.ST_GEOMFROMGEOJSON, 0); }
		public TerminalNode ST_GEOMFROMTEXT() { return getToken(BaseRuleParser.ST_GEOMFROMTEXT, 0); }
		public TerminalNode ST_GEOMFROMWKB() { return getToken(BaseRuleParser.ST_GEOMFROMWKB, 0); }
		public TerminalNode ST_INTERIORRINGN() { return getToken(BaseRuleParser.ST_INTERIORRINGN, 0); }
		public TerminalNode ST_INTERSECTION() { return getToken(BaseRuleParser.ST_INTERSECTION, 0); }
		public TerminalNode ST_INTERSECTS() { return getToken(BaseRuleParser.ST_INTERSECTS, 0); }
		public TerminalNode ST_ISCLOSED() { return getToken(BaseRuleParser.ST_ISCLOSED, 0); }
		public TerminalNode ST_ISEMPTY() { return getToken(BaseRuleParser.ST_ISEMPTY, 0); }
		public TerminalNode ST_ISSIMPLE() { return getToken(BaseRuleParser.ST_ISSIMPLE, 0); }
		public TerminalNode ST_ISVALID() { return getToken(BaseRuleParser.ST_ISVALID, 0); }
		public TerminalNode ST_LATFROMGEOHASH() { return getToken(BaseRuleParser.ST_LATFROMGEOHASH, 0); }
		public TerminalNode ST_LATITUDE() { return getToken(BaseRuleParser.ST_LATITUDE, 0); }
		public TerminalNode ST_LENGTH() { return getToken(BaseRuleParser.ST_LENGTH, 0); }
		public TerminalNode ST_LINEFROMTEXT() { return getToken(BaseRuleParser.ST_LINEFROMTEXT, 0); }
		public TerminalNode ST_LINEFROMWKB() { return getToken(BaseRuleParser.ST_LINEFROMWKB, 0); }
		public TerminalNode ST_LINESTRINGFROMTEXT() { return getToken(BaseRuleParser.ST_LINESTRINGFROMTEXT, 0); }
		public TerminalNode ST_LINESTRINGFROMWKB() { return getToken(BaseRuleParser.ST_LINESTRINGFROMWKB, 0); }
		public TerminalNode ST_LONGFROMGEOHASH() { return getToken(BaseRuleParser.ST_LONGFROMGEOHASH, 0); }
		public TerminalNode ST_LONGITUDE() { return getToken(BaseRuleParser.ST_LONGITUDE, 0); }
		public TerminalNode ST_MAKEENVELOPE() { return getToken(BaseRuleParser.ST_MAKEENVELOPE, 0); }
		public TerminalNode ST_MLINEFROMTEXT() { return getToken(BaseRuleParser.ST_MLINEFROMTEXT, 0); }
		public TerminalNode ST_MLINEFROMWKB() { return getToken(BaseRuleParser.ST_MLINEFROMWKB, 0); }
		public TerminalNode ST_MULTILINESTRINGFROMTEXT() { return getToken(BaseRuleParser.ST_MULTILINESTRINGFROMTEXT, 0); }
		public TerminalNode ST_MULTILINESTRINGFROMWKB() { return getToken(BaseRuleParser.ST_MULTILINESTRINGFROMWKB, 0); }
		public TerminalNode ST_MPOINTFROMTEXT() { return getToken(BaseRuleParser.ST_MPOINTFROMTEXT, 0); }
		public TerminalNode ST_MPOINTFROMWKB() { return getToken(BaseRuleParser.ST_MPOINTFROMWKB, 0); }
		public TerminalNode ST_MULTIPOINTFROMTEXT() { return getToken(BaseRuleParser.ST_MULTIPOINTFROMTEXT, 0); }
		public TerminalNode ST_MULTIPOINTFROMWKB() { return getToken(BaseRuleParser.ST_MULTIPOINTFROMWKB, 0); }
		public TerminalNode ST_MPOLYFROMTEXT() { return getToken(BaseRuleParser.ST_MPOLYFROMTEXT, 0); }
		public TerminalNode ST_MPOLYFROMWKB() { return getToken(BaseRuleParser.ST_MPOLYFROMWKB, 0); }
		public TerminalNode ST_MULTIPOLYGONFROMTEXT() { return getToken(BaseRuleParser.ST_MULTIPOLYGONFROMTEXT, 0); }
		public TerminalNode ST_MULTIPOLYGONFROMWKB() { return getToken(BaseRuleParser.ST_MULTIPOLYGONFROMWKB, 0); }
		public TerminalNode ST_NUMGEOMETRIES() { return getToken(BaseRuleParser.ST_NUMGEOMETRIES, 0); }
		public TerminalNode ST_NUMINTERIORRING() { return getToken(BaseRuleParser.ST_NUMINTERIORRING, 0); }
		public TerminalNode ST_NUMINTERIORRINGS() { return getToken(BaseRuleParser.ST_NUMINTERIORRINGS, 0); }
		public TerminalNode ST_NUMPOINTS() { return getToken(BaseRuleParser.ST_NUMPOINTS, 0); }
		public TerminalNode ST_OVERLAPS() { return getToken(BaseRuleParser.ST_OVERLAPS, 0); }
		public TerminalNode ST_POINTFROMGEOHASH() { return getToken(BaseRuleParser.ST_POINTFROMGEOHASH, 0); }
		public TerminalNode ST_POINTFROMTEXT() { return getToken(BaseRuleParser.ST_POINTFROMTEXT, 0); }
		public TerminalNode ST_POINTFROMWKB() { return getToken(BaseRuleParser.ST_POINTFROMWKB, 0); }
		public TerminalNode ST_POINTN() { return getToken(BaseRuleParser.ST_POINTN, 0); }
		public TerminalNode ST_POLYFROMTEXT() { return getToken(BaseRuleParser.ST_POLYFROMTEXT, 0); }
		public TerminalNode ST_POLYFROMWKB() { return getToken(BaseRuleParser.ST_POLYFROMWKB, 0); }
		public TerminalNode ST_POLYGONFROMTEXT() { return getToken(BaseRuleParser.ST_POLYGONFROMTEXT, 0); }
		public TerminalNode ST_POLYGONFROMWKB() { return getToken(BaseRuleParser.ST_POLYGONFROMWKB, 0); }
		public TerminalNode ST_SIMPLIFY() { return getToken(BaseRuleParser.ST_SIMPLIFY, 0); }
		public TerminalNode ST_SRID() { return getToken(BaseRuleParser.ST_SRID, 0); }
		public TerminalNode ST_STARTPOINT() { return getToken(BaseRuleParser.ST_STARTPOINT, 0); }
		public TerminalNode ST_SWAPXY() { return getToken(BaseRuleParser.ST_SWAPXY, 0); }
		public TerminalNode ST_SYMDIFFERENCE() { return getToken(BaseRuleParser.ST_SYMDIFFERENCE, 0); }
		public TerminalNode ST_TOUCHES() { return getToken(BaseRuleParser.ST_TOUCHES, 0); }
		public TerminalNode ST_TRANSFORM() { return getToken(BaseRuleParser.ST_TRANSFORM, 0); }
		public TerminalNode ST_UNION() { return getToken(BaseRuleParser.ST_UNION, 0); }
		public TerminalNode ST_VALIDATE() { return getToken(BaseRuleParser.ST_VALIDATE, 0); }
		public TerminalNode ST_WITHIN() { return getToken(BaseRuleParser.ST_WITHIN, 0); }
		public TerminalNode ST_X() { return getToken(BaseRuleParser.ST_X, 0); }
		public TerminalNode ST_Y() { return getToken(BaseRuleParser.ST_Y, 0); }
		public TerminalNode TIME() { return getToken(BaseRuleParser.TIME, 0); }
		public TerminalNode TIMEDIFF() { return getToken(BaseRuleParser.TIMEDIFF, 0); }
		public TerminalNode TIMESTAMP() { return getToken(BaseRuleParser.TIMESTAMP, 0); }
		public TerminalNode TIMESTAMPADD() { return getToken(BaseRuleParser.TIMESTAMPADD, 0); }
		public TerminalNode TIMESTAMPDIFF() { return getToken(BaseRuleParser.TIMESTAMPDIFF, 0); }
		public TerminalNode TIME_FORMAT() { return getToken(BaseRuleParser.TIME_FORMAT, 0); }
		public TerminalNode TIME_TO_SEC() { return getToken(BaseRuleParser.TIME_TO_SEC, 0); }
		public TerminalNode AES_DECRYPT() { return getToken(BaseRuleParser.AES_DECRYPT, 0); }
		public TerminalNode AES_ENCRYPT() { return getToken(BaseRuleParser.AES_ENCRYPT, 0); }
		public TerminalNode FROM_BASE64() { return getToken(BaseRuleParser.FROM_BASE64, 0); }
		public TerminalNode TO_BASE64() { return getToken(BaseRuleParser.TO_BASE64, 0); }
		public TerminalNode ADDDATE() { return getToken(BaseRuleParser.ADDDATE, 0); }
		public TerminalNode ADDTIME() { return getToken(BaseRuleParser.ADDTIME, 0); }
		public TerminalNode DATE_ADD() { return getToken(BaseRuleParser.DATE_ADD, 0); }
		public TerminalNode DATE_SUB() { return getToken(BaseRuleParser.DATE_SUB, 0); }
		public RegularFunctionName_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_regularFunctionName_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterRegularFunctionName_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitRegularFunctionName_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitRegularFunctionName_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegularFunctionName_Context regularFunctionName_() throws RecognitionException {
		RegularFunctionName_Context _localctx = new RegularFunctionName_Context(_ctx, getState());
		enterRule(_localctx, 160, RULE_regularFunctionName_);
		try {
			setState(1022);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(884);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(885);
				match(IF);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(886);
				match(CURRENT_TIMESTAMP);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(887);
				match(UNIX_TIMESTAMP);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(888);
				match(LOCALTIME);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(889);
				match(LOCALTIMESTAMP);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(890);
				match(NOW);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(891);
				match(REPLACE);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(892);
				match(INTERVAL);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(893);
				match(SUBSTRING);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(894);
				match(MOD);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(895);
				match(DATABASE);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(896);
				match(LEFT);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(897);
				match(RIGHT);
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(898);
				match(LOWER);
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(899);
				match(UPPER);
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(900);
				match(DATE);
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(901);
				match(DATEDIFF);
				}
				break;
			case 19:
				enterOuterAlt(_localctx, 19);
				{
				setState(902);
				match(DATE_FORMAT);
				}
				break;
			case 20:
				enterOuterAlt(_localctx, 20);
				{
				setState(903);
				match(DAY);
				}
				break;
			case 21:
				enterOuterAlt(_localctx, 21);
				{
				setState(904);
				match(DAYNAME);
				}
				break;
			case 22:
				enterOuterAlt(_localctx, 22);
				{
				setState(905);
				match(DAYOFMONTH);
				}
				break;
			case 23:
				enterOuterAlt(_localctx, 23);
				{
				setState(906);
				match(DAYOFWEEK);
				}
				break;
			case 24:
				enterOuterAlt(_localctx, 24);
				{
				setState(907);
				match(DAYOFYEAR);
				}
				break;
			case 25:
				enterOuterAlt(_localctx, 25);
				{
				setState(908);
				match(GEOMCOLLECTION);
				}
				break;
			case 26:
				enterOuterAlt(_localctx, 26);
				{
				setState(909);
				match(GEOMETRYCOLLECTION);
				}
				break;
			case 27:
				enterOuterAlt(_localctx, 27);
				{
				setState(910);
				match(LINESTRING);
				}
				break;
			case 28:
				enterOuterAlt(_localctx, 28);
				{
				setState(911);
				match(MULTILINESTRING);
				}
				break;
			case 29:
				enterOuterAlt(_localctx, 29);
				{
				setState(912);
				match(MULTIPOINT);
				}
				break;
			case 30:
				enterOuterAlt(_localctx, 30);
				{
				setState(913);
				match(MULTIPOLYGON);
				}
				break;
			case 31:
				enterOuterAlt(_localctx, 31);
				{
				setState(914);
				match(POINT);
				}
				break;
			case 32:
				enterOuterAlt(_localctx, 32);
				{
				setState(915);
				match(POLYGON);
				}
				break;
			case 33:
				enterOuterAlt(_localctx, 33);
				{
				setState(916);
				match(STR_TO_DATE);
				}
				break;
			case 34:
				enterOuterAlt(_localctx, 34);
				{
				setState(917);
				match(ST_AREA);
				}
				break;
			case 35:
				enterOuterAlt(_localctx, 35);
				{
				setState(918);
				match(ST_ASBINARY);
				}
				break;
			case 36:
				enterOuterAlt(_localctx, 36);
				{
				setState(919);
				match(ST_ASGEOJSON);
				}
				break;
			case 37:
				enterOuterAlt(_localctx, 37);
				{
				setState(920);
				match(ST_ASTEXT);
				}
				break;
			case 38:
				enterOuterAlt(_localctx, 38);
				{
				setState(921);
				match(ST_ASWKB);
				}
				break;
			case 39:
				enterOuterAlt(_localctx, 39);
				{
				setState(922);
				match(ST_ASWKT);
				}
				break;
			case 40:
				enterOuterAlt(_localctx, 40);
				{
				setState(923);
				match(ST_BUFFER);
				}
				break;
			case 41:
				enterOuterAlt(_localctx, 41);
				{
				setState(924);
				match(ST_BUFFER_STRATEGY);
				}
				break;
			case 42:
				enterOuterAlt(_localctx, 42);
				{
				setState(925);
				match(ST_CENTROID);
				}
				break;
			case 43:
				enterOuterAlt(_localctx, 43);
				{
				setState(926);
				match(ST_CONTAINS);
				}
				break;
			case 44:
				enterOuterAlt(_localctx, 44);
				{
				setState(927);
				match(ST_CONVEXHULL);
				}
				break;
			case 45:
				enterOuterAlt(_localctx, 45);
				{
				setState(928);
				match(ST_CROSSES);
				}
				break;
			case 46:
				enterOuterAlt(_localctx, 46);
				{
				setState(929);
				match(ST_DIFFERENCE);
				}
				break;
			case 47:
				enterOuterAlt(_localctx, 47);
				{
				setState(930);
				match(ST_DIMENSION);
				}
				break;
			case 48:
				enterOuterAlt(_localctx, 48);
				{
				setState(931);
				match(ST_DISJOINT);
				}
				break;
			case 49:
				enterOuterAlt(_localctx, 49);
				{
				setState(932);
				match(ST_DISTANCE);
				}
				break;
			case 50:
				enterOuterAlt(_localctx, 50);
				{
				setState(933);
				match(ST_DISTANCE_SPHERE);
				}
				break;
			case 51:
				enterOuterAlt(_localctx, 51);
				{
				setState(934);
				match(ST_ENDPOINT);
				}
				break;
			case 52:
				enterOuterAlt(_localctx, 52);
				{
				setState(935);
				match(ST_ENVELOPE);
				}
				break;
			case 53:
				enterOuterAlt(_localctx, 53);
				{
				setState(936);
				match(ST_EQUALS);
				}
				break;
			case 54:
				enterOuterAlt(_localctx, 54);
				{
				setState(937);
				match(ST_EXTERIORRING);
				}
				break;
			case 55:
				enterOuterAlt(_localctx, 55);
				{
				setState(938);
				match(ST_GEOHASH);
				}
				break;
			case 56:
				enterOuterAlt(_localctx, 56);
				{
				setState(939);
				match(ST_GEOMCOLLFROMTEXT);
				}
				break;
			case 57:
				enterOuterAlt(_localctx, 57);
				{
				setState(940);
				match(ST_GEOMCOLLFROMTXT);
				}
				break;
			case 58:
				enterOuterAlt(_localctx, 58);
				{
				setState(941);
				match(ST_GEOMCOLLFROMWKB);
				}
				break;
			case 59:
				enterOuterAlt(_localctx, 59);
				{
				setState(942);
				match(ST_GEOMETRYCOLLECTIONFROMTEXT);
				}
				break;
			case 60:
				enterOuterAlt(_localctx, 60);
				{
				setState(943);
				match(ST_GEOMETRYCOLLECTIONFROMWKB);
				}
				break;
			case 61:
				enterOuterAlt(_localctx, 61);
				{
				setState(944);
				match(ST_GEOMETRYFROMTEXT);
				}
				break;
			case 62:
				enterOuterAlt(_localctx, 62);
				{
				setState(945);
				match(ST_GEOMETRYFROMWKB);
				}
				break;
			case 63:
				enterOuterAlt(_localctx, 63);
				{
				setState(946);
				match(ST_GEOMETRYN);
				}
				break;
			case 64:
				enterOuterAlt(_localctx, 64);
				{
				setState(947);
				match(ST_GEOMETRYTYPE);
				}
				break;
			case 65:
				enterOuterAlt(_localctx, 65);
				{
				setState(948);
				match(ST_GEOMFROMGEOJSON);
				}
				break;
			case 66:
				enterOuterAlt(_localctx, 66);
				{
				setState(949);
				match(ST_GEOMFROMTEXT);
				}
				break;
			case 67:
				enterOuterAlt(_localctx, 67);
				{
				setState(950);
				match(ST_GEOMFROMWKB);
				}
				break;
			case 68:
				enterOuterAlt(_localctx, 68);
				{
				setState(951);
				match(ST_INTERIORRINGN);
				}
				break;
			case 69:
				enterOuterAlt(_localctx, 69);
				{
				setState(952);
				match(ST_INTERSECTION);
				}
				break;
			case 70:
				enterOuterAlt(_localctx, 70);
				{
				setState(953);
				match(ST_INTERSECTS);
				}
				break;
			case 71:
				enterOuterAlt(_localctx, 71);
				{
				setState(954);
				match(ST_ISCLOSED);
				}
				break;
			case 72:
				enterOuterAlt(_localctx, 72);
				{
				setState(955);
				match(ST_ISEMPTY);
				}
				break;
			case 73:
				enterOuterAlt(_localctx, 73);
				{
				setState(956);
				match(ST_ISSIMPLE);
				}
				break;
			case 74:
				enterOuterAlt(_localctx, 74);
				{
				setState(957);
				match(ST_ISVALID);
				}
				break;
			case 75:
				enterOuterAlt(_localctx, 75);
				{
				setState(958);
				match(ST_LATFROMGEOHASH);
				}
				break;
			case 76:
				enterOuterAlt(_localctx, 76);
				{
				setState(959);
				match(ST_LATITUDE);
				}
				break;
			case 77:
				enterOuterAlt(_localctx, 77);
				{
				setState(960);
				match(ST_LENGTH);
				}
				break;
			case 78:
				enterOuterAlt(_localctx, 78);
				{
				setState(961);
				match(ST_LINEFROMTEXT);
				}
				break;
			case 79:
				enterOuterAlt(_localctx, 79);
				{
				setState(962);
				match(ST_LINEFROMWKB);
				}
				break;
			case 80:
				enterOuterAlt(_localctx, 80);
				{
				setState(963);
				match(ST_LINESTRINGFROMTEXT);
				}
				break;
			case 81:
				enterOuterAlt(_localctx, 81);
				{
				setState(964);
				match(ST_LINESTRINGFROMWKB);
				}
				break;
			case 82:
				enterOuterAlt(_localctx, 82);
				{
				setState(965);
				match(ST_LONGFROMGEOHASH);
				}
				break;
			case 83:
				enterOuterAlt(_localctx, 83);
				{
				setState(966);
				match(ST_LONGITUDE);
				}
				break;
			case 84:
				enterOuterAlt(_localctx, 84);
				{
				setState(967);
				match(ST_MAKEENVELOPE);
				}
				break;
			case 85:
				enterOuterAlt(_localctx, 85);
				{
				setState(968);
				match(ST_MLINEFROMTEXT);
				}
				break;
			case 86:
				enterOuterAlt(_localctx, 86);
				{
				setState(969);
				match(ST_MLINEFROMWKB);
				}
				break;
			case 87:
				enterOuterAlt(_localctx, 87);
				{
				setState(970);
				match(ST_MULTILINESTRINGFROMTEXT);
				}
				break;
			case 88:
				enterOuterAlt(_localctx, 88);
				{
				setState(971);
				match(ST_MULTILINESTRINGFROMWKB);
				}
				break;
			case 89:
				enterOuterAlt(_localctx, 89);
				{
				setState(972);
				match(ST_MPOINTFROMTEXT);
				}
				break;
			case 90:
				enterOuterAlt(_localctx, 90);
				{
				setState(973);
				match(ST_MPOINTFROMWKB);
				}
				break;
			case 91:
				enterOuterAlt(_localctx, 91);
				{
				setState(974);
				match(ST_MULTIPOINTFROMTEXT);
				}
				break;
			case 92:
				enterOuterAlt(_localctx, 92);
				{
				setState(975);
				match(ST_MULTIPOINTFROMWKB);
				}
				break;
			case 93:
				enterOuterAlt(_localctx, 93);
				{
				setState(976);
				match(ST_MPOLYFROMTEXT);
				}
				break;
			case 94:
				enterOuterAlt(_localctx, 94);
				{
				setState(977);
				match(ST_MPOLYFROMWKB);
				}
				break;
			case 95:
				enterOuterAlt(_localctx, 95);
				{
				setState(978);
				match(ST_MULTIPOLYGONFROMTEXT);
				}
				break;
			case 96:
				enterOuterAlt(_localctx, 96);
				{
				setState(979);
				match(ST_MULTIPOLYGONFROMWKB);
				}
				break;
			case 97:
				enterOuterAlt(_localctx, 97);
				{
				setState(980);
				match(ST_NUMGEOMETRIES);
				}
				break;
			case 98:
				enterOuterAlt(_localctx, 98);
				{
				setState(981);
				match(ST_NUMINTERIORRING);
				}
				break;
			case 99:
				enterOuterAlt(_localctx, 99);
				{
				setState(982);
				match(ST_NUMINTERIORRINGS);
				}
				break;
			case 100:
				enterOuterAlt(_localctx, 100);
				{
				setState(983);
				match(ST_NUMPOINTS);
				}
				break;
			case 101:
				enterOuterAlt(_localctx, 101);
				{
				setState(984);
				match(ST_OVERLAPS);
				}
				break;
			case 102:
				enterOuterAlt(_localctx, 102);
				{
				setState(985);
				match(ST_POINTFROMGEOHASH);
				}
				break;
			case 103:
				enterOuterAlt(_localctx, 103);
				{
				setState(986);
				match(ST_POINTFROMTEXT);
				}
				break;
			case 104:
				enterOuterAlt(_localctx, 104);
				{
				setState(987);
				match(ST_POINTFROMWKB);
				}
				break;
			case 105:
				enterOuterAlt(_localctx, 105);
				{
				setState(988);
				match(ST_POINTN);
				}
				break;
			case 106:
				enterOuterAlt(_localctx, 106);
				{
				setState(989);
				match(ST_POLYFROMTEXT);
				}
				break;
			case 107:
				enterOuterAlt(_localctx, 107);
				{
				setState(990);
				match(ST_POLYFROMWKB);
				}
				break;
			case 108:
				enterOuterAlt(_localctx, 108);
				{
				setState(991);
				match(ST_POLYGONFROMTEXT);
				}
				break;
			case 109:
				enterOuterAlt(_localctx, 109);
				{
				setState(992);
				match(ST_POLYGONFROMWKB);
				}
				break;
			case 110:
				enterOuterAlt(_localctx, 110);
				{
				setState(993);
				match(ST_SIMPLIFY);
				}
				break;
			case 111:
				enterOuterAlt(_localctx, 111);
				{
				setState(994);
				match(ST_SRID);
				}
				break;
			case 112:
				enterOuterAlt(_localctx, 112);
				{
				setState(995);
				match(ST_STARTPOINT);
				}
				break;
			case 113:
				enterOuterAlt(_localctx, 113);
				{
				setState(996);
				match(ST_SWAPXY);
				}
				break;
			case 114:
				enterOuterAlt(_localctx, 114);
				{
				setState(997);
				match(ST_SYMDIFFERENCE);
				}
				break;
			case 115:
				enterOuterAlt(_localctx, 115);
				{
				setState(998);
				match(ST_TOUCHES);
				}
				break;
			case 116:
				enterOuterAlt(_localctx, 116);
				{
				setState(999);
				match(ST_TRANSFORM);
				}
				break;
			case 117:
				enterOuterAlt(_localctx, 117);
				{
				setState(1000);
				match(ST_UNION);
				}
				break;
			case 118:
				enterOuterAlt(_localctx, 118);
				{
				setState(1001);
				match(ST_VALIDATE);
				}
				break;
			case 119:
				enterOuterAlt(_localctx, 119);
				{
				setState(1002);
				match(ST_WITHIN);
				}
				break;
			case 120:
				enterOuterAlt(_localctx, 120);
				{
				setState(1003);
				match(ST_X);
				}
				break;
			case 121:
				enterOuterAlt(_localctx, 121);
				{
				setState(1004);
				match(ST_Y);
				}
				break;
			case 122:
				enterOuterAlt(_localctx, 122);
				{
				setState(1005);
				match(TIME);
				}
				break;
			case 123:
				enterOuterAlt(_localctx, 123);
				{
				setState(1006);
				match(TIMEDIFF);
				}
				break;
			case 124:
				enterOuterAlt(_localctx, 124);
				{
				setState(1007);
				match(TIMESTAMP);
				}
				break;
			case 125:
				enterOuterAlt(_localctx, 125);
				{
				setState(1008);
				match(TIMESTAMPADD);
				}
				break;
			case 126:
				enterOuterAlt(_localctx, 126);
				{
				setState(1009);
				match(TIMESTAMPDIFF);
				}
				break;
			case 127:
				enterOuterAlt(_localctx, 127);
				{
				setState(1010);
				match(TIME_FORMAT);
				}
				break;
			case 128:
				enterOuterAlt(_localctx, 128);
				{
				setState(1011);
				match(TIME_TO_SEC);
				}
				break;
			case 129:
				enterOuterAlt(_localctx, 129);
				{
				setState(1012);
				match(AES_DECRYPT);
				}
				break;
			case 130:
				enterOuterAlt(_localctx, 130);
				{
				setState(1013);
				match(AES_ENCRYPT);
				}
				break;
			case 131:
				enterOuterAlt(_localctx, 131);
				{
				setState(1014);
				match(FROM_BASE64);
				}
				break;
			case 132:
				enterOuterAlt(_localctx, 132);
				{
				setState(1015);
				match(TO_BASE64);
				}
				break;
			case 133:
				enterOuterAlt(_localctx, 133);
				{
				setState(1016);
				match(ADDDATE);
				}
				break;
			case 134:
				enterOuterAlt(_localctx, 134);
				{
				setState(1017);
				match(ADDTIME);
				}
				break;
			case 135:
				enterOuterAlt(_localctx, 135);
				{
				setState(1018);
				match(DATE);
				}
				break;
			case 136:
				enterOuterAlt(_localctx, 136);
				{
				setState(1019);
				match(DATE_ADD);
				}
				break;
			case 137:
				enterOuterAlt(_localctx, 137);
				{
				setState(1020);
				match(DATE_SUB);
				}
				break;
			case 138:
				enterOuterAlt(_localctx, 138);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchExpression_Context extends ParserRuleContext {
		public TerminalNode MATCH() { return getToken(BaseRuleParser.MATCH, 0); }
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public TerminalNode AGAINST() { return getToken(BaseRuleParser.AGAINST, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public MatchSearchModifier_Context matchSearchModifier_() {
			return getRuleContext(MatchSearchModifier_Context.class,0);
		}
		public MatchExpression_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matchExpression_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterMatchExpression_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitMatchExpression_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitMatchExpression_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatchExpression_Context matchExpression_() throws RecognitionException {
		MatchExpression_Context _localctx = new MatchExpression_Context(_ctx, getState());
		enterRule(_localctx, 162, RULE_matchExpression_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1024);
			match(MATCH);
			setState(1025);
			columnNames();
			setState(1026);
			match(AGAINST);
			{
			setState(1027);
			expr(0);
			setState(1029);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,88,_ctx) ) {
			case 1:
				{
				setState(1028);
				matchSearchModifier_();
				}
				break;
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchSearchModifier_Context extends ParserRuleContext {
		public TerminalNode IN() { return getToken(BaseRuleParser.IN, 0); }
		public TerminalNode NATURAL() { return getToken(BaseRuleParser.NATURAL, 0); }
		public TerminalNode LANGUAGE() { return getToken(BaseRuleParser.LANGUAGE, 0); }
		public TerminalNode MODE() { return getToken(BaseRuleParser.MODE, 0); }
		public TerminalNode WITH() { return getToken(BaseRuleParser.WITH, 0); }
		public TerminalNode QUERY() { return getToken(BaseRuleParser.QUERY, 0); }
		public TerminalNode EXPANSION() { return getToken(BaseRuleParser.EXPANSION, 0); }
		public TerminalNode BOOLEAN() { return getToken(BaseRuleParser.BOOLEAN, 0); }
		public MatchSearchModifier_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matchSearchModifier_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterMatchSearchModifier_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitMatchSearchModifier_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitMatchSearchModifier_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatchSearchModifier_Context matchSearchModifier_() throws RecognitionException {
		MatchSearchModifier_Context _localctx = new MatchSearchModifier_Context(_ctx, getState());
		enterRule(_localctx, 164, RULE_matchSearchModifier_);
		try {
			setState(1048);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,89,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1031);
				match(IN);
				setState(1032);
				match(NATURAL);
				setState(1033);
				match(LANGUAGE);
				setState(1034);
				match(MODE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1035);
				match(IN);
				setState(1036);
				match(NATURAL);
				setState(1037);
				match(LANGUAGE);
				setState(1038);
				match(MODE);
				setState(1039);
				match(WITH);
				setState(1040);
				match(QUERY);
				setState(1041);
				match(EXPANSION);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1042);
				match(IN);
				setState(1043);
				match(BOOLEAN);
				setState(1044);
				match(MODE);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1045);
				match(WITH);
				setState(1046);
				match(QUERY);
				setState(1047);
				match(EXPANSION);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseExpressionContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(BaseRuleParser.CASE, 0); }
		public TerminalNode END() { return getToken(BaseRuleParser.END, 0); }
		public SimpleExprContext simpleExpr() {
			return getRuleContext(SimpleExprContext.class,0);
		}
		public List<CaseWhen_Context> caseWhen_() {
			return getRuleContexts(CaseWhen_Context.class);
		}
		public CaseWhen_Context caseWhen_(int i) {
			return getRuleContext(CaseWhen_Context.class,i);
		}
		public CaseElse_Context caseElse_() {
			return getRuleContext(CaseElse_Context.class,0);
		}
		public CaseExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCaseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCaseExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCaseExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseExpressionContext caseExpression() throws RecognitionException {
		CaseExpressionContext _localctx = new CaseExpressionContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_caseExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1050);
			match(CASE);
			setState(1052);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << NOT_) | (1L << TILDE_) | (1L << PLUS_) | (1L << MINUS_) | (1L << DOT_) | (1L << LP_) | (1L << LBE_) | (1L << QUESTION_) | (1L << AT_) | (1L << TRUNCATE))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (POSITION - 64)) | (1L << (VIEW - 64)) | (1L << (VALUES - 64)) | (1L << (CASE - 64)) | (1L << (CAST - 64)) | (1L << (TRIM - 64)) | (1L << (SUBSTRING - 64)) | (1L << (LEFT - 64)) | (1L << (RIGHT - 64)) | (1L << (IF - 64)) | (1L << (NULL - 64)) | (1L << (TRUE - 64)) | (1L << (FALSE - 64)) | (1L << (EXISTS - 64)) | (1L << (ANY - 64)) | (1L << (OFFSET - 64)) | (1L << (BEGIN - 64)) | (1L << (COMMIT - 64)) | (1L << (ROLLBACK - 64)) | (1L << (SAVEPOINT - 64)) | (1L << (BOOLEAN - 64)) | (1L << (CHAR - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (INTERVAL - 128)) | (1L << (DATE - 128)) | (1L << (TIME - 128)) | (1L << (TIMESTAMP - 128)) | (1L << (LOCALTIME - 128)) | (1L << (LOCALTIMESTAMP - 128)) | (1L << (YEAR - 128)) | (1L << (QUARTER - 128)) | (1L << (MONTH - 128)) | (1L << (WEEK - 128)) | (1L << (DAY - 128)) | (1L << (HOUR - 128)) | (1L << (MINUTE - 128)) | (1L << (SECOND - 128)) | (1L << (MICROSECOND - 128)) | (1L << (MAX - 128)) | (1L << (MIN - 128)) | (1L << (SUM - 128)) | (1L << (COUNT - 128)) | (1L << (AVG - 128)) | (1L << (CURRENT - 128)) | (1L << (ENABLE - 128)) | (1L << (DISABLE - 128)) | (1L << (INSTANCE - 128)) | (1L << (DO - 128)) | (1L << (DEFINER - 128)) | (1L << (CASCADED - 128)) | (1L << (LOCAL - 128)) | (1L << (CLOSE - 128)) | (1L << (OPEN - 128)) | (1L << (NEXT - 128)) | (1L << (NAME - 128)) | (1L << (TYPE - 128)) | (1L << (DATABASE - 128)) | (1L << (TABLES - 128)) | (1L << (TABLESPACE - 128)) | (1L << (COLUMNS - 128)) | (1L << (FIELDS - 128)) | (1L << (INDEXES - 128)) | (1L << (STATUS - 128)))) != 0) || ((((_la - 192)) & ~0x3f) == 0 && ((1L << (_la - 192)) & ((1L << (REPLACE - 192)) | (1L << (MODIFY - 192)) | (1L << (VALUE - 192)) | (1L << (DUPLICATE - 192)) | (1L << (FIRST - 192)) | (1L << (LAST - 192)) | (1L << (AFTER - 192)) | (1L << (OJ - 192)) | (1L << (MOD - 192)) | (1L << (ACCOUNT - 192)) | (1L << (USER - 192)) | (1L << (ROLE - 192)) | (1L << (START - 192)) | (1L << (TRANSACTION - 192)) | (1L << (ROW - 192)) | (1L << (WITHOUT - 192)) | (1L << (BINARY - 192)) | (1L << (ESCAPE - 192)) | (1L << (SUBPARTITION - 192)) | (1L << (STORAGE - 192)) | (1L << (SUPER - 192)) | (1L << (SUBSTR - 192)) | (1L << (TEMPORARY - 192)) | (1L << (THAN - 192)) | (1L << (UNBOUNDED - 192)) | (1L << (SIGNED - 192)) | (1L << (UPGRADE - 192)) | (1L << (VALIDATION - 192)) | (1L << (ROLLUP - 192)) | (1L << (SOUNDS - 192)) | (1L << (UNKNOWN - 192)) | (1L << (OFF - 192)) | (1L << (ALWAYS - 192)) | (1L << (COMMITTED - 192)) | (1L << (LEVEL - 192)) | (1L << (NO - 192)) | (1L << (PASSWORD - 192)) | (1L << (PRIVILEGES - 192)) | (1L << (ACTION - 192)) | (1L << (ALGORITHM - 192)) | (1L << (AUTOCOMMIT - 192)))) != 0) || ((((_la - 257)) & ~0x3f) == 0 && ((1L << (_la - 257)) & ((1L << (BTREE - 257)) | (1L << (CHAIN - 257)) | (1L << (CHARSET - 257)) | (1L << (CHECKSUM - 257)) | (1L << (CIPHER - 257)) | (1L << (CLIENT - 257)) | (1L << (COALESCE - 257)) | (1L << (COMMENT - 257)) | (1L << (COMPACT - 257)) | (1L << (COMPRESSED - 257)) | (1L << (COMPRESSION - 257)) | (1L << (CONNECTION - 257)) | (1L << (CONSISTENT - 257)) | (1L << (CONVERT - 257)) | (1L << (DATA - 257)) | (1L << (DISCARD - 257)) | (1L << (DISK - 257)) | (1L << (ENCRYPTION - 257)) | (1L << (END - 257)) | (1L << (ENGINE - 257)) | (1L << (EVENT - 257)) | (1L << (EXCHANGE - 257)) | (1L << (EXECUTE - 257)) | (1L << (EXTRACT - 257)) | (1L << (FILE - 257)) | (1L << (FIXED - 257)) | (1L << (FOLLOWING - 257)) | (1L << (GLOBAL - 257)) | (1L << (HASH - 257)) | (1L << (IMPORT_ - 257)) | (1L << (LESS - 257)) | (1L << (MATCH - 257)) | (1L << (MEMORY - 257)) | (1L << (NONE - 257)) | (1L << (NOW - 257)) | (1L << (PARSER - 257)) | (1L << (PARTIAL - 257)) | (1L << (PARTITIONING - 257)) | (1L << (PERSIST - 257)) | (1L << (PRECEDING - 257)) | (1L << (PROCESS - 257)) | (1L << (PROXY - 257)) | (1L << (QUICK - 257)))) != 0) || ((((_la - 322)) & ~0x3f) == 0 && ((1L << (_la - 322)) & ((1L << (REBUILD - 322)) | (1L << (REDUNDANT - 322)) | (1L << (RELOAD - 322)) | (1L << (REMOVE - 322)) | (1L << (REORGANIZE - 322)) | (1L << (REPAIR - 322)) | (1L << (REVERSE - 322)) | (1L << (SESSION - 322)) | (1L << (SHUTDOWN - 322)) | (1L << (SIMPLE - 322)) | (1L << (SLAVE - 322)) | (1L << (VISIBLE - 322)) | (1L << (INVISIBLE - 322)) | (1L << (ENFORCED - 322)) | (1L << (AGAINST - 322)) | (1L << (LANGUAGE - 322)) | (1L << (MODE - 322)) | (1L << (QUERY - 322)) | (1L << (EXTENDED - 322)) | (1L << (EXPANSION - 322)) | (1L << (VARIANCE - 322)) | (1L << (MAX_ROWS - 322)) | (1L << (MIN_ROWS - 322)) | (1L << (SQL_BIG_RESULT - 322)) | (1L << (SQL_BUFFER_RESULT - 322)) | (1L << (SQL_CACHE - 322)) | (1L << (SQL_NO_CACHE - 322)) | (1L << (STATS_AUTO_RECALC - 322)) | (1L << (STATS_PERSISTENT - 322)) | (1L << (STATS_SAMPLE_PAGES - 322)) | (1L << (ROW_FORMAT - 322)) | (1L << (WEIGHT_STRING - 322)) | (1L << (COLUMN_FORMAT - 322)) | (1L << (INSERT_METHOD - 322)) | (1L << (KEY_BLOCK_SIZE - 322)) | (1L << (PACK_KEYS - 322)) | (1L << (PERSIST_ONLY - 322)) | (1L << (BIT_AND - 322)) | (1L << (BIT_OR - 322)) | (1L << (BIT_XOR - 322)))) != 0) || ((((_la - 386)) & ~0x3f) == 0 && ((1L << (_la - 386)) & ((1L << (GROUP_CONCAT - 386)) | (1L << (JSON_ARRAYAGG - 386)) | (1L << (JSON_OBJECTAGG - 386)) | (1L << (STD - 386)) | (1L << (STDDEV - 386)) | (1L << (STDDEV_POP - 386)) | (1L << (STDDEV_SAMP - 386)) | (1L << (VAR_POP - 386)) | (1L << (VAR_SAMP - 386)) | (1L << (AUTO_INCREMENT - 386)) | (1L << (AVG_ROW_LENGTH - 386)) | (1L << (DELAY_KEY_WRITE - 386)) | (1L << (CURRENT_TIMESTAMP - 386)) | (1L << (ROTATE - 386)) | (1L << (MASTER - 386)) | (1L << (BINLOG - 386)) | (1L << (ERROR - 386)) | (1L << (SCHEDULE - 386)) | (1L << (COMPLETION - 386)) | (1L << (EVERY - 386)) | (1L << (HOST - 386)) | (1L << (SOCKET - 386)) | (1L << (PORT - 386)) | (1L << (SERVER - 386)) | (1L << (WRAPPER - 386)) | (1L << (OPTIONS - 386)) | (1L << (OWNER - 386)) | (1L << (RETURNS - 386)) | (1L << (CONTAINS - 386)) | (1L << (SECURITY - 386)) | (1L << (INVOKER - 386)) | (1L << (TEMPTABLE - 386)) | (1L << (MERGE - 386)) | (1L << (UNDEFINED - 386)) | (1L << (DATAFILE - 386)) | (1L << (FILE_BLOCK_SIZE - 386)) | (1L << (EXTENT_SIZE - 386)) | (1L << (INITIAL_SIZE - 386)) | (1L << (AUTOEXTEND_SIZE - 386)) | (1L << (MAX_SIZE - 386)))) != 0) || ((((_la - 450)) & ~0x3f) == 0 && ((1L << (_la - 450)) & ((1L << (NODEGROUP - 450)) | (1L << (WAIT - 450)) | (1L << (LOGFILE - 450)) | (1L << (UNDOFILE - 450)) | (1L << (UNDO_BUFFER_SIZE - 450)) | (1L << (REDO_BUFFER_SIZE - 450)) | (1L << (HANDLER - 450)) | (1L << (PREV - 450)) | (1L << (ORGANIZATION - 450)) | (1L << (DEFINITION - 450)) | (1L << (DESCRIPTION - 450)) | (1L << (REFERENCE - 450)) | (1L << (FOLLOWS - 450)) | (1L << (PRECEDES - 450)) | (1L << (IMPORT - 450)) | (1L << (CONCURRENT - 450)) | (1L << (XML - 450)) | (1L << (DUMPFILE - 450)) | (1L << (SHARE - 450)) | (1L << (CODE - 450)) | (1L << (CONTEXT - 450)) | (1L << (SOURCE - 450)) | (1L << (CHANNEL - 450)) | (1L << (CLONE - 450)) | (1L << (AGGREGATE - 450)) | (1L << (INSTALL - 450)) | (1L << (COMPONENT - 450)))) != 0) || ((((_la - 514)) & ~0x3f) == 0 && ((1L << (_la - 514)) & ((1L << (UNINSTALL - 514)) | (1L << (RESOURCE - 514)) | (1L << (EXPIRE - 514)) | (1L << (NEVER - 514)) | (1L << (HISTORY - 514)) | (1L << (OPTIONAL - 514)) | (1L << (REUSE - 514)) | (1L << (MAX_QUERIES_PER_HOUR - 514)) | (1L << (MAX_UPDATES_PER_HOUR - 514)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 514)) | (1L << (MAX_USER_CONNECTIONS - 514)) | (1L << (RETAIN - 514)) | (1L << (RANDOM - 514)) | (1L << (OLD - 514)) | (1L << (ISSUER - 514)) | (1L << (SUBJECT - 514)) | (1L << (CACHE - 514)) | (1L << (GENERAL - 514)) | (1L << (SLOW - 514)) | (1L << (USER_RESOURCES - 514)) | (1L << (EXPORT - 514)) | (1L << (RELAY - 514)) | (1L << (HOSTS - 514)) | (1L << (FLUSH - 514)) | (1L << (RESET - 514)) | (1L << (RESTART - 514)) | (1L << (UNIX_TIMESTAMP - 514)) | (1L << (LOWER - 514)) | (1L << (UPPER - 514)) | (1L << (ADDDATE - 514)) | (1L << (ADDTIME - 514)) | (1L << (DATE_ADD - 514)) | (1L << (DATE_SUB - 514)) | (1L << (DATEDIFF - 514)) | (1L << (DATE_FORMAT - 514)) | (1L << (DAYNAME - 514)) | (1L << (DAYOFMONTH - 514)) | (1L << (DAYOFWEEK - 514)) | (1L << (DAYOFYEAR - 514)) | (1L << (STR_TO_DATE - 514)) | (1L << (TIMEDIFF - 514)) | (1L << (TIMESTAMPADD - 514)) | (1L << (TIMESTAMPDIFF - 514)) | (1L << (TIME_FORMAT - 514)) | (1L << (TIME_TO_SEC - 514)) | (1L << (AES_DECRYPT - 514)) | (1L << (AES_ENCRYPT - 514)) | (1L << (FROM_BASE64 - 514)) | (1L << (TO_BASE64 - 514)) | (1L << (GEOMCOLLECTION - 514)) | (1L << (GEOMETRYCOLLECTION - 514)) | (1L << (LINESTRING - 514)))) != 0) || ((((_la - 578)) & ~0x3f) == 0 && ((1L << (_la - 578)) & ((1L << (MULTILINESTRING - 578)) | (1L << (MULTIPOINT - 578)) | (1L << (MULTIPOLYGON - 578)) | (1L << (POINT - 578)) | (1L << (POLYGON - 578)) | (1L << (ST_AREA - 578)) | (1L << (ST_ASBINARY - 578)) | (1L << (ST_ASGEOJSON - 578)) | (1L << (ST_ASTEXT - 578)) | (1L << (ST_ASWKB - 578)) | (1L << (ST_ASWKT - 578)) | (1L << (ST_BUFFER - 578)) | (1L << (ST_BUFFER_STRATEGY - 578)) | (1L << (ST_CENTROID - 578)) | (1L << (ST_CONTAINS - 578)) | (1L << (ST_CONVEXHULL - 578)) | (1L << (ST_CROSSES - 578)) | (1L << (ST_DIFFERENCE - 578)) | (1L << (ST_DIMENSION - 578)) | (1L << (ST_DISJOINT - 578)) | (1L << (ST_DISTANCE - 578)) | (1L << (ST_DISTANCE_SPHERE - 578)) | (1L << (ST_ENDPOINT - 578)) | (1L << (ST_ENVELOPE - 578)) | (1L << (ST_EQUALS - 578)) | (1L << (ST_EXTERIORRING - 578)) | (1L << (ST_GEOHASH - 578)) | (1L << (ST_GEOMCOLLFROMTEXT - 578)) | (1L << (ST_GEOMCOLLFROMTXT - 578)) | (1L << (ST_GEOMCOLLFROMWKB - 578)) | (1L << (ST_GEOMETRYCOLLECTIONFROMTEXT - 578)) | (1L << (ST_GEOMETRYCOLLECTIONFROMWKB - 578)) | (1L << (ST_GEOMETRYFROMTEXT - 578)) | (1L << (ST_GEOMETRYFROMWKB - 578)) | (1L << (ST_GEOMETRYN - 578)) | (1L << (ST_GEOMETRYTYPE - 578)) | (1L << (ST_GEOMFROMGEOJSON - 578)) | (1L << (ST_GEOMFROMTEXT - 578)) | (1L << (ST_GEOMFROMWKB - 578)) | (1L << (ST_INTERIORRINGN - 578)) | (1L << (ST_INTERSECTION - 578)) | (1L << (ST_INTERSECTS - 578)) | (1L << (ST_ISCLOSED - 578)) | (1L << (ST_ISEMPTY - 578)) | (1L << (ST_ISSIMPLE - 578)) | (1L << (ST_ISVALID - 578)) | (1L << (ST_LATFROMGEOHASH - 578)) | (1L << (ST_LATITUDE - 578)) | (1L << (ST_LENGTH - 578)) | (1L << (ST_LINEFROMTEXT - 578)) | (1L << (ST_LINEFROMWKB - 578)) | (1L << (ST_LINESTRINGFROMTEXT - 578)) | (1L << (ST_LINESTRINGFROMWKB - 578)) | (1L << (ST_LONGFROMGEOHASH - 578)) | (1L << (ST_LONGITUDE - 578)) | (1L << (ST_MAKEENVELOPE - 578)) | (1L << (ST_MLINEFROMTEXT - 578)) | (1L << (ST_MLINEFROMWKB - 578)) | (1L << (ST_MULTILINESTRINGFROMTEXT - 578)) | (1L << (ST_MULTILINESTRINGFROMWKB - 578)) | (1L << (ST_MPOINTFROMTEXT - 578)) | (1L << (ST_MPOINTFROMWKB - 578)) | (1L << (ST_MULTIPOINTFROMTEXT - 578)) | (1L << (ST_MULTIPOINTFROMWKB - 578)))) != 0) || ((((_la - 642)) & ~0x3f) == 0 && ((1L << (_la - 642)) & ((1L << (ST_MPOLYFROMTEXT - 642)) | (1L << (ST_MPOLYFROMWKB - 642)) | (1L << (ST_MULTIPOLYGONFROMTEXT - 642)) | (1L << (ST_MULTIPOLYGONFROMWKB - 642)) | (1L << (ST_NUMGEOMETRIES - 642)) | (1L << (ST_NUMINTERIORRING - 642)) | (1L << (ST_NUMINTERIORRINGS - 642)) | (1L << (ST_NUMPOINTS - 642)) | (1L << (ST_OVERLAPS - 642)) | (1L << (ST_POINTFROMGEOHASH - 642)) | (1L << (ST_POINTFROMTEXT - 642)) | (1L << (ST_POINTFROMWKB - 642)) | (1L << (ST_POINTN - 642)) | (1L << (ST_POLYFROMTEXT - 642)) | (1L << (ST_POLYFROMWKB - 642)) | (1L << (ST_POLYGONFROMTEXT - 642)) | (1L << (ST_POLYGONFROMWKB - 642)) | (1L << (ST_SIMPLIFY - 642)) | (1L << (ST_SRID - 642)) | (1L << (ST_STARTPOINT - 642)) | (1L << (ST_SWAPXY - 642)) | (1L << (ST_SYMDIFFERENCE - 642)) | (1L << (ST_TOUCHES - 642)) | (1L << (ST_TRANSFORM - 642)) | (1L << (ST_UNION - 642)) | (1L << (ST_VALIDATE - 642)) | (1L << (ST_WITHIN - 642)) | (1L << (ST_X - 642)) | (1L << (ST_Y - 642)) | (1L << (IO_THREAD - 642)) | (1L << (SQL_THREAD - 642)) | (1L << (SQL_BEFORE_GTIDS - 642)) | (1L << (SQL_AFTER_GTIDS - 642)) | (1L << (MASTER_LOG_FILE - 642)) | (1L << (MASTER_LOG_POS - 642)) | (1L << (RELAY_LOG_FILE - 642)) | (1L << (RELAY_LOG_POS - 642)) | (1L << (SQL_AFTER_MTS_GAPS - 642)) | (1L << (UNTIL - 642)) | (1L << (DEFAULT_AUTH - 642)) | (1L << (PLUGIN_DIR - 642)) | (1L << (STOP - 642)) | (1L << (IDENTIFIER_ - 642)) | (1L << (STRING_ - 642)) | (1L << (NUMBER_ - 642)) | (1L << (HEX_DIGIT_ - 642)) | (1L << (BIT_NUM_ - 642)))) != 0)) {
				{
				setState(1051);
				simpleExpr(0);
				}
			}

			setState(1055); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1054);
				caseWhen_();
				}
				}
				setState(1057); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==WHEN );
			setState(1060);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(1059);
				caseElse_();
				}
			}

			setState(1062);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseWhen_Context extends ParserRuleContext {
		public TerminalNode WHEN() { return getToken(BaseRuleParser.WHEN, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode THEN() { return getToken(BaseRuleParser.THEN, 0); }
		public CaseWhen_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseWhen_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCaseWhen_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCaseWhen_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCaseWhen_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseWhen_Context caseWhen_() throws RecognitionException {
		CaseWhen_Context _localctx = new CaseWhen_Context(_ctx, getState());
		enterRule(_localctx, 168, RULE_caseWhen_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1064);
			match(WHEN);
			setState(1065);
			expr(0);
			setState(1066);
			match(THEN);
			setState(1067);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseElse_Context extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(BaseRuleParser.ELSE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public CaseElse_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseElse_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCaseElse_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCaseElse_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCaseElse_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseElse_Context caseElse_() throws RecognitionException {
		CaseElse_Context _localctx = new CaseElse_Context(_ctx, getState());
		enterRule(_localctx, 170, RULE_caseElse_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1069);
			match(ELSE);
			setState(1070);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalExpressionContext extends ParserRuleContext {
		public TerminalNode INTERVAL() { return getToken(BaseRuleParser.INTERVAL, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public IntervalUnit_Context intervalUnit_() {
			return getRuleContext(IntervalUnit_Context.class,0);
		}
		public IntervalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intervalExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterIntervalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitIntervalExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitIntervalExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntervalExpressionContext intervalExpression() throws RecognitionException {
		IntervalExpressionContext _localctx = new IntervalExpressionContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_intervalExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1072);
			match(INTERVAL);
			setState(1073);
			expr(0);
			setState(1074);
			intervalUnit_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalUnit_Context extends ParserRuleContext {
		public TerminalNode MICROSECOND() { return getToken(BaseRuleParser.MICROSECOND, 0); }
		public TerminalNode SECOND() { return getToken(BaseRuleParser.SECOND, 0); }
		public TerminalNode MINUTE() { return getToken(BaseRuleParser.MINUTE, 0); }
		public TerminalNode HOUR() { return getToken(BaseRuleParser.HOUR, 0); }
		public TerminalNode DAY() { return getToken(BaseRuleParser.DAY, 0); }
		public TerminalNode WEEK() { return getToken(BaseRuleParser.WEEK, 0); }
		public TerminalNode MONTH() { return getToken(BaseRuleParser.MONTH, 0); }
		public TerminalNode QUARTER() { return getToken(BaseRuleParser.QUARTER, 0); }
		public TerminalNode YEAR() { return getToken(BaseRuleParser.YEAR, 0); }
		public TerminalNode SECOND_MICROSECOND() { return getToken(BaseRuleParser.SECOND_MICROSECOND, 0); }
		public TerminalNode MINUTE_MICROSECOND() { return getToken(BaseRuleParser.MINUTE_MICROSECOND, 0); }
		public TerminalNode MINUTE_SECOND() { return getToken(BaseRuleParser.MINUTE_SECOND, 0); }
		public TerminalNode HOUR_MICROSECOND() { return getToken(BaseRuleParser.HOUR_MICROSECOND, 0); }
		public TerminalNode HOUR_SECOND() { return getToken(BaseRuleParser.HOUR_SECOND, 0); }
		public TerminalNode HOUR_MINUTE() { return getToken(BaseRuleParser.HOUR_MINUTE, 0); }
		public TerminalNode DAY_MICROSECOND() { return getToken(BaseRuleParser.DAY_MICROSECOND, 0); }
		public TerminalNode DAY_SECOND() { return getToken(BaseRuleParser.DAY_SECOND, 0); }
		public TerminalNode DAY_MINUTE() { return getToken(BaseRuleParser.DAY_MINUTE, 0); }
		public TerminalNode DAY_HOUR() { return getToken(BaseRuleParser.DAY_HOUR, 0); }
		public TerminalNode YEAR_MONTH() { return getToken(BaseRuleParser.YEAR_MONTH, 0); }
		public IntervalUnit_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intervalUnit_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterIntervalUnit_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitIntervalUnit_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitIntervalUnit_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntervalUnit_Context intervalUnit_() throws RecognitionException {
		IntervalUnit_Context _localctx = new IntervalUnit_Context(_ctx, getState());
		enterRule(_localctx, 174, RULE_intervalUnit_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1076);
			_la = _input.LA(1);
			if ( !(((((_la - 134)) & ~0x3f) == 0 && ((1L << (_la - 134)) & ((1L << (YEAR - 134)) | (1L << (QUARTER - 134)) | (1L << (MONTH - 134)) | (1L << (WEEK - 134)) | (1L << (DAY - 134)) | (1L << (HOUR - 134)) | (1L << (MINUTE - 134)) | (1L << (SECOND - 134)) | (1L << (MICROSECOND - 134)))) != 0) || ((((_la - 404)) & ~0x3f) == 0 && ((1L << (_la - 404)) & ((1L << (YEAR_MONTH - 404)) | (1L << (DAY_HOUR - 404)) | (1L << (DAY_MINUTE - 404)) | (1L << (DAY_SECOND - 404)) | (1L << (DAY_MICROSECOND - 404)) | (1L << (HOUR_MINUTE - 404)) | (1L << (HOUR_SECOND - 404)) | (1L << (HOUR_MICROSECOND - 404)) | (1L << (MINUTE_SECOND - 404)) | (1L << (MINUTE_MICROSECOND - 404)) | (1L << (SECOND_MICROSECOND - 404)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubqueryContext extends ParserRuleContext {
		public SubqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterSubquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitSubquery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitSubquery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubqueryContext subquery() throws RecognitionException {
		SubqueryContext _localctx = new SubqueryContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_subquery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1078);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByClauseContext extends ParserRuleContext {
		public TerminalNode ORDER() { return getToken(BaseRuleParser.ORDER, 0); }
		public TerminalNode BY() { return getToken(BaseRuleParser.BY, 0); }
		public List<OrderByItemContext> orderByItem() {
			return getRuleContexts(OrderByItemContext.class);
		}
		public OrderByItemContext orderByItem(int i) {
			return getRuleContext(OrderByItemContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public OrderByClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterOrderByClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitOrderByClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitOrderByClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByClauseContext orderByClause() throws RecognitionException {
		OrderByClauseContext _localctx = new OrderByClauseContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_orderByClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1080);
			match(ORDER);
			setState(1081);
			match(BY);
			setState(1082);
			orderByItem();
			setState(1087);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1083);
				match(COMMA_);
				setState(1084);
				orderByItem();
				}
				}
				setState(1089);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByItemContext extends ParserRuleContext {
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ASC() { return getToken(BaseRuleParser.ASC, 0); }
		public TerminalNode DESC() { return getToken(BaseRuleParser.DESC, 0); }
		public OrderByItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterOrderByItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitOrderByItem(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitOrderByItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByItemContext orderByItem() throws RecognitionException {
		OrderByItemContext _localctx = new OrderByItemContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_orderByItem);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1093);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,94,_ctx) ) {
			case 1:
				{
				setState(1090);
				columnName();
				}
				break;
			case 2:
				{
				setState(1091);
				numberLiterals();
				}
				break;
			case 3:
				{
				setState(1092);
				expr(0);
				}
				break;
			}
			setState(1096);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASC || _la==DESC) {
				{
				setState(1095);
				_la = _input.LA(1);
				if ( !(_la==ASC || _la==DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeContext extends ParserRuleContext {
		public DataTypeNameContext dataTypeName() {
			return getRuleContext(DataTypeNameContext.class,0);
		}
		public DataTypeLengthContext dataTypeLength() {
			return getRuleContext(DataTypeLengthContext.class,0);
		}
		public CharacterSet_Context characterSet_() {
			return getRuleContext(CharacterSet_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public TerminalNode ZEROFILL() { return getToken(BaseRuleParser.ZEROFILL, 0); }
		public TerminalNode UNSIGNED() { return getToken(BaseRuleParser.UNSIGNED, 0); }
		public TerminalNode SIGNED() { return getToken(BaseRuleParser.SIGNED, 0); }
		public CollectionOptionsContext collectionOptions() {
			return getRuleContext(CollectionOptionsContext.class,0);
		}
		public DataTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterDataType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitDataType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitDataType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeContext dataType() throws RecognitionException {
		DataTypeContext _localctx = new DataTypeContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_dataType);
		int _la;
		try {
			setState(1122);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1098);
				dataTypeName();
				setState(1100);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP_) {
					{
					setState(1099);
					dataTypeLength();
					}
				}

				setState(1103);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CHAR || _la==CHARACTER) {
					{
					setState(1102);
					characterSet_();
					}
				}

				setState(1106);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLLATE) {
					{
					setState(1105);
					collateClause_();
					}
				}

				setState(1109);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==UNSIGNED || _la==SIGNED) {
					{
					setState(1108);
					_la = _input.LA(1);
					if ( !(_la==UNSIGNED || _la==SIGNED) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1112);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ZEROFILL) {
					{
					setState(1111);
					match(ZEROFILL);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1114);
				dataTypeName();
				setState(1115);
				collectionOptions();
				setState(1117);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CHAR || _la==CHARACTER) {
					{
					setState(1116);
					characterSet_();
					}
				}

				setState(1120);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLLATE) {
					{
					setState(1119);
					collateClause_();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeNameContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(BaseRuleParser.INTEGER, 0); }
		public TerminalNode INT() { return getToken(BaseRuleParser.INT, 0); }
		public TerminalNode SMALLINT() { return getToken(BaseRuleParser.SMALLINT, 0); }
		public TerminalNode TINYINT() { return getToken(BaseRuleParser.TINYINT, 0); }
		public TerminalNode MEDIUMINT() { return getToken(BaseRuleParser.MEDIUMINT, 0); }
		public TerminalNode BIGINT() { return getToken(BaseRuleParser.BIGINT, 0); }
		public TerminalNode DECIMAL() { return getToken(BaseRuleParser.DECIMAL, 0); }
		public TerminalNode NUMERIC() { return getToken(BaseRuleParser.NUMERIC, 0); }
		public TerminalNode FLOAT() { return getToken(BaseRuleParser.FLOAT, 0); }
		public TerminalNode DOUBLE() { return getToken(BaseRuleParser.DOUBLE, 0); }
		public TerminalNode BIT() { return getToken(BaseRuleParser.BIT, 0); }
		public TerminalNode BOOL() { return getToken(BaseRuleParser.BOOL, 0); }
		public TerminalNode BOOLEAN() { return getToken(BaseRuleParser.BOOLEAN, 0); }
		public TerminalNode DEC() { return getToken(BaseRuleParser.DEC, 0); }
		public TerminalNode DATE() { return getToken(BaseRuleParser.DATE, 0); }
		public TerminalNode DATETIME() { return getToken(BaseRuleParser.DATETIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(BaseRuleParser.TIMESTAMP, 0); }
		public TerminalNode TIME() { return getToken(BaseRuleParser.TIME, 0); }
		public TerminalNode YEAR() { return getToken(BaseRuleParser.YEAR, 0); }
		public TerminalNode CHAR() { return getToken(BaseRuleParser.CHAR, 0); }
		public TerminalNode VARCHAR() { return getToken(BaseRuleParser.VARCHAR, 0); }
		public TerminalNode BINARY() { return getToken(BaseRuleParser.BINARY, 0); }
		public TerminalNode VARBINARY() { return getToken(BaseRuleParser.VARBINARY, 0); }
		public TerminalNode TINYBLOB() { return getToken(BaseRuleParser.TINYBLOB, 0); }
		public TerminalNode TINYTEXT() { return getToken(BaseRuleParser.TINYTEXT, 0); }
		public TerminalNode BLOB() { return getToken(BaseRuleParser.BLOB, 0); }
		public TerminalNode TEXT() { return getToken(BaseRuleParser.TEXT, 0); }
		public TerminalNode MEDIUMBLOB() { return getToken(BaseRuleParser.MEDIUMBLOB, 0); }
		public TerminalNode MEDIUMTEXT() { return getToken(BaseRuleParser.MEDIUMTEXT, 0); }
		public TerminalNode LONGBLOB() { return getToken(BaseRuleParser.LONGBLOB, 0); }
		public TerminalNode LONGTEXT() { return getToken(BaseRuleParser.LONGTEXT, 0); }
		public TerminalNode ENUM() { return getToken(BaseRuleParser.ENUM, 0); }
		public TerminalNode SET() { return getToken(BaseRuleParser.SET, 0); }
		public TerminalNode GEOMETRY() { return getToken(BaseRuleParser.GEOMETRY, 0); }
		public TerminalNode POINT() { return getToken(BaseRuleParser.POINT, 0); }
		public TerminalNode LINESTRING() { return getToken(BaseRuleParser.LINESTRING, 0); }
		public TerminalNode POLYGON() { return getToken(BaseRuleParser.POLYGON, 0); }
		public TerminalNode MULTIPOINT() { return getToken(BaseRuleParser.MULTIPOINT, 0); }
		public TerminalNode MULTILINESTRING() { return getToken(BaseRuleParser.MULTILINESTRING, 0); }
		public TerminalNode MULTIPOLYGON() { return getToken(BaseRuleParser.MULTIPOLYGON, 0); }
		public TerminalNode GEOMETRYCOLLECTION() { return getToken(BaseRuleParser.GEOMETRYCOLLECTION, 0); }
		public TerminalNode JSON() { return getToken(BaseRuleParser.JSON, 0); }
		public DataTypeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataTypeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterDataTypeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitDataTypeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitDataTypeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeNameContext dataTypeName() throws RecognitionException {
		DataTypeNameContext _localctx = new DataTypeNameContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_dataTypeName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1124);
			_la = _input.LA(1);
			if ( !(_la==SET || ((((_la - 123)) & ~0x3f) == 0 && ((1L << (_la - 123)) & ((1L << (BOOLEAN - 123)) | (1L << (DOUBLE - 123)) | (1L << (CHAR - 123)) | (1L << (DATE - 123)) | (1L << (TIME - 123)) | (1L << (TIMESTAMP - 123)) | (1L << (YEAR - 123)) | (1L << (INTEGER - 123)) | (1L << (DECIMAL - 123)) | (1L << (INT - 123)) | (1L << (SMALLINT - 123)) | (1L << (TINYINT - 123)) | (1L << (MEDIUMINT - 123)) | (1L << (BIGINT - 123)) | (1L << (NUMERIC - 123)) | (1L << (FLOAT - 123)) | (1L << (DATETIME - 123)))) != 0) || _la==BINARY || ((((_la - 576)) & ~0x3f) == 0 && ((1L << (_la - 576)) & ((1L << (GEOMETRYCOLLECTION - 576)) | (1L << (LINESTRING - 576)) | (1L << (MULTILINESTRING - 576)) | (1L << (MULTIPOINT - 576)) | (1L << (MULTIPOLYGON - 576)) | (1L << (POINT - 576)) | (1L << (POLYGON - 576)))) != 0) || ((((_la - 671)) & ~0x3f) == 0 && ((1L << (_la - 671)) & ((1L << (BIT - 671)) | (1L << (BOOL - 671)) | (1L << (DEC - 671)) | (1L << (VARCHAR - 671)) | (1L << (VARBINARY - 671)) | (1L << (TINYBLOB - 671)) | (1L << (TINYTEXT - 671)) | (1L << (BLOB - 671)) | (1L << (TEXT - 671)) | (1L << (MEDIUMBLOB - 671)) | (1L << (MEDIUMTEXT - 671)) | (1L << (LONGBLOB - 671)) | (1L << (LONGTEXT - 671)) | (1L << (ENUM - 671)) | (1L << (GEOMETRY - 671)) | (1L << (JSON - 671)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeLengthContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<TerminalNode> NUMBER_() { return getTokens(BaseRuleParser.NUMBER_); }
		public TerminalNode NUMBER_(int i) {
			return getToken(BaseRuleParser.NUMBER_, i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public TerminalNode COMMA_() { return getToken(BaseRuleParser.COMMA_, 0); }
		public DataTypeLengthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataTypeLength; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterDataTypeLength(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitDataTypeLength(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitDataTypeLength(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeLengthContext dataTypeLength() throws RecognitionException {
		DataTypeLengthContext _localctx = new DataTypeLengthContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_dataTypeLength);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1126);
			match(LP_);
			setState(1127);
			match(NUMBER_);
			setState(1130);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA_) {
				{
				setState(1128);
				match(COMMA_);
				setState(1129);
				match(NUMBER_);
				}
			}

			setState(1132);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollectionOptionsContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(BaseRuleParser.LP_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(BaseRuleParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(BaseRuleParser.STRING_, i);
		}
		public TerminalNode RP_() { return getToken(BaseRuleParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public CollectionOptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collectionOptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCollectionOptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCollectionOptions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCollectionOptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollectionOptionsContext collectionOptions() throws RecognitionException {
		CollectionOptionsContext _localctx = new CollectionOptionsContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_collectionOptions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1134);
			match(LP_);
			setState(1135);
			match(STRING_);
			setState(1140);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1136);
				match(COMMA_);
				setState(1137);
				match(STRING_);
				}
				}
				setState(1142);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1143);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharacterSet_Context extends ParserRuleContext {
		public TerminalNode SET() { return getToken(BaseRuleParser.SET, 0); }
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public TerminalNode CHARACTER() { return getToken(BaseRuleParser.CHARACTER, 0); }
		public TerminalNode CHAR() { return getToken(BaseRuleParser.CHAR, 0); }
		public TerminalNode EQ_() { return getToken(BaseRuleParser.EQ_, 0); }
		public CharacterSet_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_characterSet_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCharacterSet_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCharacterSet_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCharacterSet_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharacterSet_Context characterSet_() throws RecognitionException {
		CharacterSet_Context _localctx = new CharacterSet_Context(_ctx, getState());
		enterRule(_localctx, 190, RULE_characterSet_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1145);
			_la = _input.LA(1);
			if ( !(_la==CHAR || _la==CHARACTER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1146);
			match(SET);
			setState(1148);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EQ_) {
				{
				setState(1147);
				match(EQ_);
				}
			}

			setState(1150);
			ignoredIdentifier_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollateClause_Context extends ParserRuleContext {
		public TerminalNode COLLATE() { return getToken(BaseRuleParser.COLLATE, 0); }
		public TerminalNode STRING_() { return getToken(BaseRuleParser.STRING_, 0); }
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public TerminalNode EQ_() { return getToken(BaseRuleParser.EQ_, 0); }
		public CollateClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collateClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterCollateClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitCollateClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitCollateClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollateClause_Context collateClause_() throws RecognitionException {
		CollateClause_Context _localctx = new CollateClause_Context(_ctx, getState());
		enterRule(_localctx, 192, RULE_collateClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1152);
			match(COLLATE);
			setState(1154);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EQ_) {
				{
				setState(1153);
				match(EQ_);
				}
			}

			setState(1158);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING_:
				{
				setState(1156);
				match(STRING_);
				}
				break;
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				{
				setState(1157);
				ignoredIdentifier_();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IgnoredIdentifier_Context extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode DOT_() { return getToken(BaseRuleParser.DOT_, 0); }
		public IgnoredIdentifier_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ignoredIdentifier_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterIgnoredIdentifier_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitIgnoredIdentifier_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitIgnoredIdentifier_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IgnoredIdentifier_Context ignoredIdentifier_() throws RecognitionException {
		IgnoredIdentifier_Context _localctx = new IgnoredIdentifier_Context(_ctx, getState());
		enterRule(_localctx, 194, RULE_ignoredIdentifier_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1160);
			identifier();
			setState(1163);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,109,_ctx) ) {
			case 1:
				{
				setState(1161);
				match(DOT_);
				setState(1162);
				identifier();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IgnoredIdentifiers_Context extends ParserRuleContext {
		public List<IgnoredIdentifier_Context> ignoredIdentifier_() {
			return getRuleContexts(IgnoredIdentifier_Context.class);
		}
		public IgnoredIdentifier_Context ignoredIdentifier_(int i) {
			return getRuleContext(IgnoredIdentifier_Context.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(BaseRuleParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(BaseRuleParser.COMMA_, i);
		}
		public IgnoredIdentifiers_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ignoredIdentifiers_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).enterIgnoredIdentifiers_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof BaseRuleListener ) ((BaseRuleListener)listener).exitIgnoredIdentifiers_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof BaseRuleVisitor ) return ((BaseRuleVisitor<? extends T>)visitor).visitIgnoredIdentifiers_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IgnoredIdentifiers_Context ignoredIdentifiers_() throws RecognitionException {
		IgnoredIdentifiers_Context _localctx = new IgnoredIdentifiers_Context(_ctx, getState());
		enterRule(_localctx, 196, RULE_ignoredIdentifiers_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1165);
			ignoredIdentifier_();
			setState(1170);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1166);
				match(COMMA_);
				setState(1167);
				ignoredIdentifier_();
				}
				}
				setState(1172);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 46:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 49:
			return booleanPrimary_sempred((BooleanPrimaryContext)_localctx, predIndex);
		case 52:
			return bitExpr_sempred((BitExprContext)_localctx, predIndex);
		case 53:
			return simpleExpr_sempred((SimpleExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 5);
		case 1:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean booleanPrimary_sempred(BooleanPrimaryContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 5);
		case 3:
			return precpred(_ctx, 4);
		case 4:
			return precpred(_ctx, 3);
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean bitExpr_sempred(BitExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 15);
		case 7:
			return precpred(_ctx, 14);
		case 8:
			return precpred(_ctx, 13);
		case 9:
			return precpred(_ctx, 12);
		case 10:
			return precpred(_ctx, 11);
		case 11:
			return precpred(_ctx, 10);
		case 12:
			return precpred(_ctx, 9);
		case 13:
			return precpred(_ctx, 8);
		case 14:
			return precpred(_ctx, 7);
		case 15:
			return precpred(_ctx, 6);
		case 16:
			return precpred(_ctx, 5);
		case 17:
			return precpred(_ctx, 4);
		case 18:
			return precpred(_ctx, 3);
		case 19:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean simpleExpr_sempred(SimpleExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 20:
			return precpred(_ctx, 8);
		case 21:
			return precpred(_ctx, 10);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u02c7\u0498\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\u00d2"+
		"\n\3\3\4\5\4\u00d5\n\4\3\4\3\4\5\4\u00d9\n\4\3\5\5\5\u00dc\n\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u00e7\n\6\3\7\5\7\u00ea\n\7\3\7\3\7\5"+
		"\7\u00ee\n\7\3\b\5\b\u00f1\n\b\3\b\3\b\5\b\u00f5\n\b\3\t\3\t\3\n\3\n\3"+
		"\13\3\13\3\f\3\f\3\r\3\r\5\r\u0101\n\r\3\16\3\16\3\17\5\17\u0106\n\17"+
		"\3\17\5\17\u0109\n\17\3\17\5\17\u010c\n\17\3\17\5\17\u010f\n\17\3\17\3"+
		"\17\3\20\3\20\3\21\3\21\3\21\5\21\u0118\n\21\3\21\3\21\3\22\3\22\3\22"+
		"\5\22\u011f\n\22\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3\24\3\24\5\24\u012a"+
		"\n\24\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u0132\n\25\3\26\3\26\5\26\u0136"+
		"\n\26\3\27\3\27\5\27\u013a\n\27\3\30\3\30\3\30\3\30\5\30\u0140\n\30\3"+
		"\30\5\30\u0143\n\30\3\31\3\31\3\31\3\31\5\31\u0149\n\31\3\31\5\31\u014c"+
		"\n\31\3\32\3\32\3\33\3\33\3\34\5\34\u0153\n\34\3\34\3\34\3\34\7\34\u0158"+
		"\n\34\f\34\16\34\u015b\13\34\3\34\5\34\u015e\n\34\3\35\5\35\u0161\n\35"+
		"\3\35\3\35\3\35\7\35\u0166\n\35\f\35\16\35\u0169\13\35\3\35\5\35\u016c"+
		"\n\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3$\3$\3$\3$"+
		"\3%\3%\3&\3&\3\'\3\'\3(\3(\3(\3(\3(\5(\u018b\n(\3)\3)\3*\3*\3+\3+\3,\3"+
		",\5,\u0195\n,\3-\3-\3.\3.\3/\3/\3/\3\60\3\60\3\60\3\60\3\60\3\60\3\60"+
		"\3\60\3\60\5\60\u01a7\n\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\7\60\u01b0"+
		"\n\60\f\60\16\60\u01b3\13\60\3\61\3\61\3\62\3\62\3\63\3\63\3\63\3\63\3"+
		"\63\3\63\5\63\u01bf\n\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63"+
		"\3\63\3\63\3\63\3\63\7\63\u01ce\n\63\f\63\16\63\u01d1\13\63\3\64\3\64"+
		"\3\65\3\65\5\65\u01d7\n\65\3\65\3\65\3\65\3\65\3\65\5\65\u01de\n\65\3"+
		"\65\3\65\3\65\3\65\3\65\7\65\u01e5\n\65\f\65\16\65\u01e8\13\65\3\65\3"+
		"\65\3\65\3\65\5\65\u01ee\n\65\3\65\3\65\3\65\3\65\3\65\3\65\3\65\3\65"+
		"\3\65\3\65\3\65\3\65\5\65\u01fc\n\65\3\65\3\65\3\65\3\65\5\65\u0202\n"+
		"\65\3\65\3\65\5\65\u0206\n\65\3\65\3\65\3\65\3\65\5\65\u020c\n\65\3\66"+
		"\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66"+
		"\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66"+
		"\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66"+
		"\3\66\3\66\7\66\u023b\n\66\f\66\16\66\u023e\13\66\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\5\67\u0249\n\67\3\67\3\67\3\67\3\67\7\67\u024f"+
		"\n\67\f\67\16\67\u0252\13\67\3\67\3\67\3\67\5\67\u0257\n\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u0262\n\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\5\67\u026b\n\67\7\67\u026d\n\67\f\67\16\67\u0270\13\67"+
		"\38\38\38\58\u0275\n8\39\39\39\59\u027a\n9\39\39\39\79\u027f\n9\f9\16"+
		"9\u0282\139\39\59\u0285\n9\39\39\59\u0289\n9\3:\3:\3;\3;\3<\3<\3<\3<\3"+
		"<\3<\5<\u0295\n<\3=\5=\u0298\n=\3=\5=\u029b\n=\3=\5=\u029e\n=\3=\5=\u02a1"+
		"\n=\3>\3>\3>\3>\3>\7>\u02a8\n>\f>\16>\u02ab\13>\3?\3?\3?\5?\u02b0\n?\3"+
		"@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\5@\u02be\n@\3A\3A\3B\3B\3B\3B\3B\3"+
		"C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\5C\u02d2\nC\3D\3D\3D\5D\u02d7\nD\3D\3"+
		"D\3D\7D\u02dc\nD\fD\16D\u02df\13D\3D\5D\u02e2\nD\3D\5D\u02e5\nD\3D\3D"+
		"\5D\u02e9\nD\3D\3D\3E\3E\3E\3E\3E\7E\u02f2\nE\fE\16E\u02f5\13E\3E\3E\3"+
		"E\3F\3F\3F\3F\3F\3F\3F\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\5G\u030f"+
		"\nG\3H\3H\3H\3H\3H\3H\3H\3I\3I\3I\3I\3I\3I\3I\5I\u031f\nI\3I\3I\3J\3J"+
		"\3J\3J\3J\3J\3J\3K\3K\3K\3K\3K\7K\u032f\nK\fK\16K\u0332\13K\3K\3K\5K\u0336"+
		"\nK\3K\3K\3L\3L\3L\3L\3L\3L\3L\3L\3M\3M\3M\3M\3M\3N\3N\3N\3N\3N\5N\u034c"+
		"\nN\3N\5N\u034f\nN\3N\3N\3O\3O\3O\3O\7O\u0357\nO\fO\16O\u035a\13O\3O\3"+
		"O\3O\5O\u035f\nO\3P\3P\5P\u0363\nP\3P\5P\u0366\nP\3Q\3Q\3Q\3Q\3Q\7Q\u036d"+
		"\nQ\fQ\16Q\u0370\13Q\3Q\5Q\u0373\nQ\3Q\3Q\3R\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\5R\u0401\nR\3S\3S\3S\3S\3S\5"+
		"S\u0408\nS\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\3T\5T\u041b"+
		"\nT\3U\3U\5U\u041f\nU\3U\6U\u0422\nU\rU\16U\u0423\3U\5U\u0427\nU\3U\3"+
		"U\3V\3V\3V\3V\3V\3W\3W\3W\3X\3X\3X\3X\3Y\3Y\3Z\3Z\3[\3[\3[\3[\3[\7[\u0440"+
		"\n[\f[\16[\u0443\13[\3\\\3\\\3\\\5\\\u0448\n\\\3\\\5\\\u044b\n\\\3]\3"+
		"]\5]\u044f\n]\3]\5]\u0452\n]\3]\5]\u0455\n]\3]\5]\u0458\n]\3]\5]\u045b"+
		"\n]\3]\3]\3]\5]\u0460\n]\3]\5]\u0463\n]\5]\u0465\n]\3^\3^\3_\3_\3_\3_"+
		"\5_\u046d\n_\3_\3_\3`\3`\3`\3`\7`\u0475\n`\f`\16`\u0478\13`\3`\3`\3a\3"+
		"a\3a\5a\u047f\na\3a\3a\3b\3b\5b\u0485\nb\3b\3b\5b\u0489\nb\3c\3c\3c\5"+
		"c\u048e\nc\3d\3d\3d\7d\u0493\nd\fd\16d\u0496\13d\3d\2\6^djle\2\4\6\b\n"+
		"\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\"+
		"^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a\u008c\u008e\u0090"+
		"\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8"+
		"\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8\u00ba\u00bc\u00be\u00c0"+
		"\u00c2\u00c4\u00c6\2\32\3\2\u0083\u0085\3\2ijT\2\64\64BBGGoox}\u0083\u0085"+
		"\u0088\u0095\u0097\u0099\u009b\u009b\u009d\u009e\u00a1\u00a6\u00ac\u00ac"+
		"\u00bc\u00c1\u00c3\u00c3\u00c5\u00ca\u00d1\u00d5\u00d8\u00d8\u00da\u00da"+
		"\u00dd\u00de\u00e0\u00e0\u00e2\u00e3\u00e5\u00e5\u00e8\u00e9\u00eb\u00eb"+
		"\u00ed\u00f1\u00f4\u00f6\u00f8\u00f9\u00fd\u00fe\u0100\u0100\u0103\u0104"+
		"\u0106\u010a\u010c\u0111\u0114\u0114\u0117\u0118\u011a\u011d\u011f\u011f"+
		"\u0121\u0121\u0123\u0125\u0128\u0129\u012c\u012c\u0130\u0130\u0134\u0135"+
		"\u013b\u0142\u0144\u0144\u0146\u0146\u0148\u0149\u014b\u014c\u0150\u0150"+
		"\u0153\u0153\u0155\u0157\u015a\u015b\u015d\u0166\u0169\u016b\u016d\u016d"+
		"\u016f\u0171\u0173\u0173\u0178\u0179\u017d\u018c\u018e\u018f\u0191\u0191"+
		"\u01a2\u01a8\u01ab\u01b1\u01b3\u01b4\u01b7\u01b8\u01bb\u01d2\u01d4\u01d4"+
		"\u01dc\u01dc\u01de\u01de\u01e0\u01e0\u01e8\u01e8\u01ee\u01ee\u01f4\u01f4"+
		"\u01f9\u01f9\u01fd\u01fe\u0201\u0202\u0204\u0204\u020b\u020b\u020f\u021a"+
		"\u021c\u021f\u0221\u0225\u0227\u0229\u02b1\u02bd\6\2\u0128\u0128\u013e"+
		"\u013e\u0153\u0153\u0180\u0180\3\2\u02be\u02bf\4\2\u00c9\u00c9\u01e3\u01e3"+
		"\3\2.\60\3\2\u01d0\u01d1\4\2\4\5de\4\2\6\6gg\4\2hj\u00ef\u00ef\3\2no\3"+
		"\2\30\35\3\2\u00cf\u00d0\5\2\6\7\17\20\u00d9\u00d9\6\2\u0091\u0095\u0164"+
		"\u0164\u0181\u0183\u0185\u018c\4\2\u00d7\u00d7\u0143\u0143\4\2QQ\u00e1"+
		"\u00e1\5\2\u00e4\u00e4\u0102\u0102\u012f\u012f\3\2tu\4\2\u0088\u0090\u0196"+
		"\u01a0\3\2\u00e7\u00e8\f\299}\177\u0083\u0085\u0088\u0088\u00a9\u00a9"+
		"\u00ab\u00ab\u00ad\u00b4\u00d9\u00d9\u0242\u0248\u02a1\u02b0\3\2\177\u0080"+
		"\2\u0563\2\u00c8\3\2\2\2\4\u00d1\3\2\2\2\6\u00d4\3\2\2\2\b\u00db\3\2\2"+
		"\2\n\u00e6\3\2\2\2\f\u00e9\3\2\2\2\16\u00f0\3\2\2\2\20\u00f6\3\2\2\2\22"+
		"\u00f8\3\2\2\2\24\u00fa\3\2\2\2\26\u00fc\3\2\2\2\30\u0100\3\2\2\2\32\u0102"+
		"\3\2\2\2\34\u0108\3\2\2\2\36\u0112\3\2\2\2 \u0117\3\2\2\2\"\u011e\3\2"+
		"\2\2$\u0122\3\2\2\2&\u0129\3\2\2\2(\u0131\3\2\2\2*\u0135\3\2\2\2,\u0139"+
		"\3\2\2\2.\u0142\3\2\2\2\60\u014b\3\2\2\2\62\u014d\3\2\2\2\64\u014f\3\2"+
		"\2\2\66\u0152\3\2\2\28\u0160\3\2\2\2:\u016d\3\2\2\2<\u016f\3\2\2\2>\u0171"+
		"\3\2\2\2@\u0173\3\2\2\2B\u0175\3\2\2\2D\u0177\3\2\2\2F\u0179\3\2\2\2H"+
		"\u017f\3\2\2\2J\u0181\3\2\2\2L\u0183\3\2\2\2N\u018a\3\2\2\2P\u018c\3\2"+
		"\2\2R\u018e\3\2\2\2T\u0190\3\2\2\2V\u0194\3\2\2\2X\u0196\3\2\2\2Z\u0198"+
		"\3\2\2\2\\\u019a\3\2\2\2^\u01a6\3\2\2\2`\u01b4\3\2\2\2b\u01b6\3\2\2\2"+
		"d\u01b8\3\2\2\2f\u01d2\3\2\2\2h\u020b\3\2\2\2j\u020d\3\2\2\2l\u0261\3"+
		"\2\2\2n\u0274\3\2\2\2p\u0276\3\2\2\2r\u028a\3\2\2\2t\u028c\3\2\2\2v\u028e"+
		"\3\2\2\2x\u0297\3\2\2\2z\u02a2\3\2\2\2|\u02ac\3\2\2\2~\u02bd\3\2\2\2\u0080"+
		"\u02bf\3\2\2\2\u0082\u02c1\3\2\2\2\u0084\u02d1\3\2\2\2\u0086\u02d3\3\2"+
		"\2\2\u0088\u02ec\3\2\2\2\u008a\u02f9\3\2\2\2\u008c\u030e\3\2\2\2\u008e"+
		"\u0310\3\2\2\2\u0090\u0317\3\2\2\2\u0092\u0322\3\2\2\2\u0094\u0329\3\2"+
		"\2\2\u0096\u0339\3\2\2\2\u0098\u0341\3\2\2\2\u009a\u0346\3\2\2\2\u009c"+
		"\u0352\3\2\2\2\u009e\u0360\3\2\2\2\u00a0\u0367\3\2\2\2\u00a2\u0400\3\2"+
		"\2\2\u00a4\u0402\3\2\2\2\u00a6\u041a\3\2\2\2\u00a8\u041c\3\2\2\2\u00aa"+
		"\u042a\3\2\2\2\u00ac\u042f\3\2\2\2\u00ae\u0432\3\2\2\2\u00b0\u0436\3\2"+
		"\2\2\u00b2\u0438\3\2\2\2\u00b4\u043a\3\2\2\2\u00b6\u0447\3\2\2\2\u00b8"+
		"\u0464\3\2\2\2\u00ba\u0466\3\2\2\2\u00bc\u0468\3\2\2\2\u00be\u0470\3\2"+
		"\2\2\u00c0\u047b\3\2\2\2\u00c2\u0482\3\2\2\2\u00c4\u048a\3\2\2\2\u00c6"+
		"\u048f\3\2\2\2\u00c8\u00c9\7)\2\2\u00c9\3\3\2\2\2\u00ca\u00d2\5\6\4\2"+
		"\u00cb\u00d2\5\b\5\2\u00cc\u00d2\5\n\6\2\u00cd\u00d2\5\f\7\2\u00ce\u00d2"+
		"\5\16\b\2\u00cf\u00d2\5\20\t\2\u00d0\u00d2\5\22\n\2\u00d1\u00ca\3\2\2"+
		"\2\u00d1\u00cb\3\2\2\2\u00d1\u00cc\3\2\2\2\u00d1\u00cd\3\2\2\2\u00d1\u00ce"+
		"\3\2\2\2\u00d1\u00cf\3\2\2\2\u00d1\u00d0\3\2\2\2\u00d2\5\3\2\2\2\u00d3"+
		"\u00d5\5\24\13\2\u00d4\u00d3\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6\3"+
		"\2\2\2\u00d6\u00d8\7\u02bf\2\2\u00d7\u00d9\5\u00c2b\2\u00d8\u00d7\3\2"+
		"\2\2\u00d8\u00d9\3\2\2\2\u00d9\7\3\2\2\2\u00da\u00dc\7\20\2\2\u00db\u00da"+
		"\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00de\7\u02c0\2"+
		"\2\u00de\t\3\2\2\2\u00df\u00e0\t\2\2\2\u00e0\u00e7\7\u02bf\2\2\u00e1\u00e2"+
		"\7!\2\2\u00e2\u00e3\5\30\r\2\u00e3\u00e4\7\u02bf\2\2\u00e4\u00e5\7\"\2"+
		"\2\u00e5\u00e7\3\2\2\2\u00e6\u00df\3\2\2\2\u00e6\u00e1\3\2\2\2\u00e7\13"+
		"\3\2\2\2\u00e8\u00ea\5\24\13\2\u00e9\u00e8\3\2\2\2\u00e9\u00ea\3\2\2\2"+
		"\u00ea\u00eb\3\2\2\2\u00eb\u00ed\7\u02c1\2\2\u00ec\u00ee\5\u00c2b\2\u00ed"+
		"\u00ec\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\r\3\2\2\2\u00ef\u00f1\5\24\13"+
		"\2\u00f0\u00ef\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f4"+
		"\7\u02c2\2\2\u00f3\u00f5\5\u00c2b\2\u00f4\u00f3\3\2\2\2\u00f4\u00f5\3"+
		"\2\2\2\u00f5\17\3\2\2\2\u00f6\u00f7\t\3\2\2\u00f7\21\3\2\2\2\u00f8\u00f9"+
		"\7h\2\2\u00f9\23\3\2\2\2\u00fa\u00fb\7\u02be\2\2\u00fb\25\3\2\2\2\u00fc"+
		"\u00fd\7\u02be\2\2\u00fd\27\3\2\2\2\u00fe\u0101\7\u02be\2\2\u00ff\u0101"+
		"\5\32\16\2\u0100\u00fe\3\2\2\2\u0100\u00ff\3\2\2\2\u0101\31\3\2\2\2\u0102"+
		"\u0103\t\4\2\2\u0103\33\3\2\2\2\u0104\u0106\7*\2\2\u0105\u0104\3\2\2\2"+
		"\u0105\u0106\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u0109\7*\2\2\u0108\u0105"+
		"\3\2\2\2\u0108\u0109\3\2\2\2\u0109\u010b\3\2\2\2\u010a\u010c\t\5\2\2\u010b"+
		"\u010a\3\2\2\2\u010b\u010c\3\2\2\2\u010c\u010e\3\2\2\2\u010d\u010f\7\24"+
		"\2\2\u010e\u010d\3\2\2\2\u010e\u010f\3\2\2\2\u010f\u0110\3\2\2\2\u0110"+
		"\u0111\5\30\r\2\u0111\35\3\2\2\2\u0112\u0113\5\30\r\2\u0113\37\3\2\2\2"+
		"\u0114\u0115\5\62\32\2\u0115\u0116\7\24\2\2\u0116\u0118\3\2\2\2\u0117"+
		"\u0114\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u0119\3\2\2\2\u0119\u011a\5\64"+
		"\33\2\u011a!\3\2\2\2\u011b\u011c\5\62\32\2\u011c\u011d\7\24\2\2\u011d"+
		"\u011f\3\2\2\2\u011e\u011b\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u0120\3\2"+
		"\2\2\u0120\u0121\5\64\33\2\u0121#\3\2\2\2\u0122\u0123\5\30\r\2\u0123%"+
		"\3\2\2\2\u0124\u0125\7\u02bf\2\2\u0125\u0126\7*\2\2\u0126\u012a\7\u02bf"+
		"\2\2\u0127\u012a\5\30\r\2\u0128\u012a\7\u02bf\2\2\u0129\u0124\3\2\2\2"+
		"\u0129\u0127\3\2\2\2\u0129\u0128\3\2\2\2\u012a\'\3\2\2\2\u012b\u012c\t"+
		"\6\2\2\u012c\u012d\7*\2\2\u012d\u012e\7\u02bf\2\2\u012e\u0132\7\u02be"+
		"\2\2\u012f\u0132\5\30\r\2\u0130\u0132\7\u02bf\2\2\u0131\u012b\3\2\2\2"+
		"\u0131\u012f\3\2\2\2\u0131\u0130\3\2\2\2\u0132)\3\2\2\2\u0133\u0136\5"+
		"\30\r\2\u0134\u0136\7\u02bf\2\2\u0135\u0133\3\2\2\2\u0135\u0134\3\2\2"+
		"\2\u0136+\3\2\2\2\u0137\u013a\5\30\r\2\u0138\u013a\7\u02bf\2\2\u0139\u0137"+
		"\3\2\2\2\u0139\u0138\3\2\2\2\u013a-\3\2\2\2\u013b\u0143\5\30\r\2\u013c"+
		"\u013d\5\62\32\2\u013d\u013e\7\24\2\2\u013e\u0140\3\2\2\2\u013f\u013c"+
		"\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u0143\5\30\r\2"+
		"\u0142\u013b\3\2\2\2\u0142\u013f\3\2\2\2\u0143/\3\2\2\2\u0144\u014c\5"+
		"\30\r\2\u0145\u0146\5\62\32\2\u0146\u0147\7\24\2\2\u0147\u0149\3\2\2\2"+
		"\u0148\u0145\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u014c"+
		"\5\30\r\2\u014b\u0144\3\2\2\2\u014b\u0148\3\2\2\2\u014c\61\3\2\2\2\u014d"+
		"\u014e\5\30\r\2\u014e\63\3\2\2\2\u014f\u0150\5\30\r\2\u0150\65\3\2\2\2"+
		"\u0151\u0153\7\37\2\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0154"+
		"\3\2\2\2\u0154\u0159\5 \21\2\u0155\u0156\7%\2\2\u0156\u0158\5 \21\2\u0157"+
		"\u0155\3\2\2\2\u0158\u015b\3\2\2\2\u0159\u0157\3\2\2\2\u0159\u015a\3\2"+
		"\2\2\u015a\u015d\3\2\2\2\u015b\u0159\3\2\2\2\u015c\u015e\7 \2\2\u015d"+
		"\u015c\3\2\2\2\u015d\u015e\3\2\2\2\u015e\67\3\2\2\2\u015f\u0161\7\37\2"+
		"\2\u0160\u015f\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0162\3\2\2\2\u0162\u0167"+
		"\5\"\22\2\u0163\u0164\7%\2\2\u0164\u0166\5\"\22\2\u0165\u0163\3\2\2\2"+
		"\u0166\u0169\3\2\2\2\u0167\u0165\3\2\2\2\u0167\u0168\3\2\2\2\u0168\u016b"+
		"\3\2\2\2\u0169\u0167\3\2\2\2\u016a\u016c\7 \2\2\u016b\u016a\3\2\2\2\u016b"+
		"\u016c\3\2\2\2\u016c9\3\2\2\2\u016d\u016e\7\u02be\2\2\u016e;\3\2\2\2\u016f"+
		"\u0170\7\u02bf\2\2\u0170=\3\2\2\2\u0171\u0172\7\u02bf\2\2\u0172?\3\2\2"+
		"\2\u0173\u0174\7\u02be\2\2\u0174A\3\2\2\2\u0175\u0176\7\u02bf\2\2\u0176"+
		"C\3\2\2\2\u0177\u0178\7\u02c0\2\2\u0178E\3\2\2\2\u0179\u017a\5&\24\2\u017a"+
		"\u017b\7*\2\2\u017b\u017c\5B\"\2\u017c\u017d\7\16\2\2\u017d\u017e\5D#"+
		"\2\u017eG\3\2\2\2\u017f\u0180\7\u02be\2\2\u0180I\3\2\2\2\u0181\u0182\7"+
		"\u02be\2\2\u0182K\3\2\2\2\u0183\u0184\5\30\r\2\u0184M\3\2\2\2\u0185\u0186"+
		"\t\6\2\2\u0186\u0187\7*\2\2\u0187\u0188\7\u02bf\2\2\u0188\u018b\7\u02be"+
		"\2\2\u0189\u018b\7\u02be\2\2\u018a\u0185\3\2\2\2\u018a\u0189\3\2\2\2\u018b"+
		"O\3\2\2\2\u018c\u018d\7\u02be\2\2\u018dQ\3\2\2\2\u018e\u018f\7\u02be\2"+
		"\2\u018fS\3\2\2\2\u0190\u0191\t\7\2\2\u0191U\3\2\2\2\u0192\u0195\5&\24"+
		"\2\u0193\u0195\5N(\2\u0194\u0192\3\2\2\2\u0194\u0193\3\2\2\2\u0195W\3"+
		"\2\2\2\u0196\u0197\7\u02be\2\2\u0197Y\3\2\2\2\u0198\u0199\t\b\2\2\u0199"+
		"[\3\2\2\2\u019a\u019b\t\t\2\2\u019b\u019c\5R*\2\u019c]\3\2\2\2\u019d\u019e"+
		"\b\60\1\2\u019e\u019f\5b\62\2\u019f\u01a0\5^\60\5\u01a0\u01a7\3\2\2\2"+
		"\u01a1\u01a2\7\37\2\2\u01a2\u01a3\5^\60\2\u01a3\u01a4\7 \2\2\u01a4\u01a7"+
		"\3\2\2\2\u01a5\u01a7\5d\63\2\u01a6\u019d\3\2\2\2\u01a6\u01a1\3\2\2\2\u01a6"+
		"\u01a5\3\2\2\2\u01a7\u01b1\3\2\2\2\u01a8\u01a9\f\7\2\2\u01a9\u01aa\5`"+
		"\61\2\u01aa\u01ab\5^\60\b\u01ab\u01b0\3\2\2\2\u01ac\u01ad\f\6\2\2\u01ad"+
		"\u01ae\7\u00ce\2\2\u01ae\u01b0\5^\60\7\u01af\u01a8\3\2\2\2\u01af\u01ac"+
		"\3\2\2\2\u01b0\u01b3\3\2\2\2\u01b1\u01af\3\2\2\2\u01b1\u01b2\3\2\2\2\u01b2"+
		"_\3\2\2\2\u01b3\u01b1\3\2\2\2\u01b4\u01b5\t\n\2\2\u01b5a\3\2\2\2\u01b6"+
		"\u01b7\t\13\2\2\u01b7c\3\2\2\2\u01b8\u01b9\b\63\1\2\u01b9\u01ba\5h\65"+
		"\2\u01ba\u01cf\3\2\2\2\u01bb\u01bc\f\7\2\2\u01bc\u01be\7f\2\2\u01bd\u01bf"+
		"\7g\2\2\u01be\u01bd\3\2\2\2\u01be\u01bf\3\2\2\2\u01bf\u01c0\3\2\2\2\u01c0"+
		"\u01ce\t\f\2\2\u01c1\u01c2\f\6\2\2\u01c2\u01c3\7\26\2\2\u01c3\u01ce\5"+
		"h\65\2\u01c4\u01c5\f\5\2\2\u01c5\u01c6\5f\64\2\u01c6\u01c7\5h\65\2\u01c7"+
		"\u01ce\3\2\2\2\u01c8\u01c9\f\4\2\2\u01c9\u01ca\5f\64\2\u01ca\u01cb\t\r"+
		"\2\2\u01cb\u01cc\5\u00b2Z\2\u01cc\u01ce\3\2\2\2\u01cd\u01bb\3\2\2\2\u01cd"+
		"\u01c1\3\2\2\2\u01cd\u01c4\3\2\2\2\u01cd\u01c8\3\2\2\2\u01ce\u01d1\3\2"+
		"\2\2\u01cf\u01cd\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0e\3\2\2\2\u01d1\u01cf"+
		"\3\2\2\2\u01d2\u01d3\t\16\2\2\u01d3g\3\2\2\2\u01d4\u01d6\5j\66\2\u01d5"+
		"\u01d7\7g\2\2\u01d6\u01d5\3\2\2\2\u01d6\u01d7\3\2\2\2\u01d7\u01d8\3\2"+
		"\2\2\u01d8\u01d9\7m\2\2\u01d9\u01da\5\u00b2Z\2\u01da\u020c\3\2\2\2\u01db"+
		"\u01dd\5j\66\2\u01dc\u01de\7g\2\2\u01dd\u01dc\3\2\2\2\u01dd\u01de\3\2"+
		"\2\2\u01de\u01df\3\2\2\2\u01df\u01e0\7m\2\2\u01e0\u01e1\7\37\2\2\u01e1"+
		"\u01e6\5^\60\2\u01e2\u01e3\7%\2\2\u01e3\u01e5\5^\60\2\u01e4\u01e2\3\2"+
		"\2\2\u01e5\u01e8\3\2\2\2\u01e6\u01e4\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7"+
		"\u01e9\3\2\2\2\u01e8\u01e6\3\2\2\2\u01e9\u01ea\7 \2\2\u01ea\u020c\3\2"+
		"\2\2\u01eb\u01ed\5j\66\2\u01ec\u01ee\7g\2\2\u01ed\u01ec\3\2\2\2\u01ed"+
		"\u01ee\3\2\2\2\u01ee\u01ef\3\2\2\2\u01ef\u01f0\7l\2\2\u01f0\u01f1\5j\66"+
		"\2\u01f1\u01f2\7d\2\2\u01f2\u01f3\5h\65\2\u01f3\u020c\3\2\2\2\u01f4\u01f5"+
		"\5j\66\2\u01f5\u01f6\7\u00ee\2\2\u01f6\u01f7\7p\2\2\u01f7\u01f8\5j\66"+
		"\2\u01f8\u020c\3\2\2\2\u01f9\u01fb\5j\66\2\u01fa\u01fc\7g\2\2\u01fb\u01fa"+
		"\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc\u01fd\3\2\2\2\u01fd\u01fe\7p\2\2\u01fe"+
		"\u0201\5l\67\2\u01ff\u0200\7\u00da\2\2\u0200\u0202\5l\67\2\u0201\u01ff"+
		"\3\2\2\2\u0201\u0202\3\2\2\2\u0202\u020c\3\2\2\2\u0203\u0205\5j\66\2\u0204"+
		"\u0206\7g\2\2\u0205\u0204\3\2\2\2\u0205\u0206\3\2\2\2\u0206\u0207\3\2"+
		"\2\2\u0207\u0208\t\17\2\2\u0208\u0209\5j\66\2\u0209\u020c\3\2\2\2\u020a"+
		"\u020c\5j\66\2\u020b\u01d4\3\2\2\2\u020b\u01db\3\2\2\2\u020b\u01eb\3\2"+
		"\2\2\u020b\u01f4\3\2\2\2\u020b\u01f9\3\2\2\2\u020b\u0203\3\2\2\2\u020b"+
		"\u020a\3\2\2\2\u020ci\3\2\2\2\u020d\u020e\b\66\1\2\u020e\u020f\5l\67\2"+
		"\u020f\u023c\3\2\2\2\u0210\u0211\f\21\2\2\u0211\u0212\7\b\2\2\u0212\u023b"+
		"\5j\66\22\u0213\u0214\f\20\2\2\u0214\u0215\7\t\2\2\u0215\u023b\5j\66\21"+
		"\u0216\u0217\f\17\2\2\u0217\u0218\7\n\2\2\u0218\u023b\5j\66\20\u0219\u021a"+
		"\f\16\2\2\u021a\u021b\7\13\2\2\u021b\u023b\5j\66\17\u021c\u021d\f\r\2"+
		"\2\u021d\u021e\7\17\2\2\u021e\u023b\5j\66\16\u021f\u0220\f\f\2\2\u0220"+
		"\u0221\7\20\2\2\u0221\u023b\5j\66\r\u0222\u0223\f\13\2\2\u0223\u0224\7"+
		"\21\2\2\u0224\u023b\5j\66\f\u0225\u0226\f\n\2\2\u0226\u0227\7\22\2\2\u0227"+
		"\u023b\5j\66\13\u0228\u0229\f\t\2\2\u0229\u022a\7\u00cd\2\2\u022a\u023b"+
		"\5j\66\n\u022b\u022c\f\b\2\2\u022c\u022d\7\u00cc\2\2\u022d\u023b\5j\66"+
		"\t\u022e\u022f\f\7\2\2\u022f\u0230\7\r\2\2\u0230\u023b\5j\66\b\u0231\u0232"+
		"\f\6\2\2\u0232\u0233\7\f\2\2\u0233\u023b\5j\66\7\u0234\u0235\f\5\2\2\u0235"+
		"\u0236\7\17\2\2\u0236\u023b\5\u00aeX\2\u0237\u0238\f\4\2\2\u0238\u0239"+
		"\7\20\2\2\u0239\u023b\5\u00aeX\2\u023a\u0210\3\2\2\2\u023a\u0213\3\2\2"+
		"\2\u023a\u0216\3\2\2\2\u023a\u0219\3\2\2\2\u023a\u021c\3\2\2\2\u023a\u021f"+
		"\3\2\2\2\u023a\u0222\3\2\2\2\u023a\u0225\3\2\2\2\u023a\u0228\3\2\2\2\u023a"+
		"\u022b\3\2\2\2\u023a\u022e\3\2\2\2\u023a\u0231\3\2\2\2\u023a\u0234\3\2"+
		"\2\2\u023a\u0237\3\2\2\2\u023b\u023e\3\2\2\2\u023c\u023a\3\2\2\2\u023c"+
		"\u023d\3\2\2\2\u023dk\3\2\2\2\u023e\u023c\3\2\2\2\u023f\u0240\b\67\1\2"+
		"\u0240\u0262\5n8\2\u0241\u0262\5\2\2\2\u0242\u0262\5\4\3\2\u0243\u0262"+
		"\5\"\22\2\u0244\u0262\5\34\17\2\u0245\u0246\t\20\2\2\u0246\u0262\5l\67"+
		"\t\u0247\u0249\7\u00d6\2\2\u0248\u0247\3\2\2\2\u0248\u0249\3\2\2\2\u0249"+
		"\u024a\3\2\2\2\u024a\u024b\7\37\2\2\u024b\u0250\5^\60\2\u024c\u024d\7"+
		"%\2\2\u024d\u024f\5^\60\2\u024e\u024c\3\2\2\2\u024f\u0252\3\2\2\2\u0250"+
		"\u024e\3\2\2\2\u0250\u0251\3\2\2\2\u0251\u0253\3\2\2\2\u0252\u0250\3\2"+
		"\2\2\u0253\u0254\7 \2\2\u0254\u0262\3\2\2\2\u0255\u0257\7k\2\2\u0256\u0255"+
		"\3\2\2\2\u0256\u0257\3\2\2\2\u0257\u0258\3\2\2\2\u0258\u0262\5\u00b2Z"+
		"\2\u0259\u025a\7!\2\2\u025a\u025b\5\30\r\2\u025b\u025c\5^\60\2\u025c\u025d"+
		"\7\"\2\2\u025d\u0262\3\2\2\2\u025e\u0262\5\u00a4S\2\u025f\u0262\5\u00a8"+
		"U\2\u0260\u0262\5\u00aeX\2\u0261\u023f\3\2\2\2\u0261\u0241\3\2\2\2\u0261"+
		"\u0242\3\2\2\2\u0261\u0243\3\2\2\2\u0261\u0244\3\2\2\2\u0261\u0245\3\2"+
		"\2\2\u0261\u0248\3\2\2\2\u0261\u0256\3\2\2\2\u0261\u0259\3\2\2\2\u0261"+
		"\u025e\3\2\2\2\u0261\u025f\3\2\2\2\u0261\u0260\3\2\2\2\u0262\u026e\3\2"+
		"\2\2\u0263\u0264\f\n\2\2\u0264\u0265\7\5\2\2\u0265\u026d\5l\67\13\u0266"+
		"\u0267\f\f\2\2\u0267\u026a\7\u010b\2\2\u0268\u026b\7\u02bf\2\2\u0269\u026b"+
		"\5\30\r\2\u026a\u0268\3\2\2\2\u026a\u0269\3\2\2\2\u026b\u026d\3\2\2\2"+
		"\u026c\u0263\3\2\2\2\u026c\u0266\3\2\2\2\u026d\u0270\3\2\2\2\u026e\u026c"+
		"\3\2\2\2\u026e\u026f\3\2\2\2\u026fm\3\2\2\2\u0270\u026e\3\2\2\2\u0271"+
		"\u0275\5p9\2\u0272\u0275\5\u0084C\2\u0273\u0275\5\u00a0Q\2\u0274\u0271"+
		"\3\2\2\2\u0274\u0272\3\2\2\2\u0274\u0273\3\2\2\2\u0275o\3\2\2\2\u0276"+
		"\u0277\5r:\2\u0277\u0279\7\37\2\2\u0278\u027a\5t;\2\u0279\u0278\3\2\2"+
		"\2\u0279\u027a\3\2\2\2\u027a\u0284\3\2\2\2\u027b\u0280\5^\60\2\u027c\u027d"+
		"\7%\2\2\u027d\u027f\5^\60\2\u027e\u027c\3\2\2\2\u027f\u0282\3\2\2\2\u0280"+
		"\u027e\3\2\2\2\u0280\u0281\3\2\2\2\u0281\u0285\3\2\2\2\u0282\u0280\3\2"+
		"\2\2\u0283\u0285\7\21\2\2\u0284\u027b\3\2\2\2\u0284\u0283\3\2\2\2\u0284"+
		"\u0285\3\2\2\2\u0285\u0286\3\2\2\2\u0286\u0288\7 \2\2\u0287\u0289\5v<"+
		"\2\u0288\u0287\3\2\2\2\u0288\u0289\3\2\2\2\u0289q\3\2\2\2\u028a\u028b"+
		"\t\21\2\2\u028bs\3\2\2\2\u028c\u028d\7L\2\2\u028du\3\2\2\2\u028e\u0294"+
		"\7\u013a\2\2\u028f\u0290\7\37\2\2\u0290\u0291\5x=\2\u0291\u0292\7 \2\2"+
		"\u0292\u0295\3\2\2\2\u0293\u0295\5\30\r\2\u0294\u028f\3\2\2\2\u0294\u0293"+
		"\3\2\2\2\u0295w\3\2\2\2\u0296\u0298\5\30\r\2\u0297\u0296\3\2\2\2\u0297"+
		"\u0298\3\2\2\2\u0298\u029a\3\2\2\2\u0299\u029b\5z>\2\u029a\u0299\3\2\2"+
		"\2\u029a\u029b\3\2\2\2\u029b\u029d\3\2\2\2\u029c\u029e\5\u00b4[\2\u029d"+
		"\u029c\3\2\2\2\u029d\u029e\3\2\2\2\u029e\u02a0\3\2\2\2\u029f\u02a1\5|"+
		"?\2\u02a0\u029f\3\2\2\2\u02a0\u02a1\3\2\2\2\u02a1y\3\2\2\2\u02a2\u02a3"+
		"\7\u00dc\2\2\u02a3\u02a4\7s\2\2\u02a4\u02a9\5^\60\2\u02a5\u02a6\7%\2\2"+
		"\u02a6\u02a8\5^\60\2\u02a7\u02a5\3\2\2\2\u02a8\u02ab\3\2\2\2\u02a9\u02a7"+
		"\3\2\2\2\u02a9\u02aa\3\2\2\2\u02aa{\3\2\2\2\u02ab\u02a9\3\2\2\2\u02ac"+
		"\u02af\t\22\2\2\u02ad\u02b0\5~@\2\u02ae\u02b0\5\u0082B\2\u02af\u02ad\3"+
		"\2\2\2\u02af\u02ae\3\2\2\2\u02b0}\3\2\2\2\u02b1\u02b2\7\u0097\2\2\u02b2"+
		"\u02be\7\u00d6\2\2\u02b3\u02b4\7\u00e5\2\2\u02b4\u02be\7\u013f\2\2\u02b5"+
		"\u02b6\7\u00e5\2\2\u02b6\u02be\7\u0125\2\2\u02b7\u02b8\5^\60\2\u02b8\u02b9"+
		"\7\u013f\2\2\u02b9\u02be\3\2\2\2\u02ba\u02bb\5^\60\2\u02bb\u02bc\7\u0125"+
		"\2\2\u02bc\u02be\3\2\2\2\u02bd\u02b1\3\2\2\2\u02bd\u02b3\3\2\2\2\u02bd"+
		"\u02b5\3\2\2\2\u02bd\u02b7\3\2\2\2\u02bd\u02ba\3\2\2\2\u02be\177\3\2\2"+
		"\2\u02bf\u02c0\5~@\2\u02c0\u0081\3\2\2\2\u02c1\u02c2\7l\2\2\u02c2\u02c3"+
		"\5~@\2\u02c3\u02c4\7d\2\2\u02c4\u02c5\5\u0080A\2\u02c5\u0083\3\2\2\2\u02c6"+
		"\u02d2\5\u0086D\2\u02c7\u02d2\5\u0088E\2\u02c8\u02d2\5\u008aF\2\u02c9"+
		"\u02d2\5\u008cG\2\u02ca\u02d2\5\u008eH\2\u02cb\u02d2\5\u0090I\2\u02cc"+
		"\u02d2\5\u0092J\2\u02cd\u02d2\5\u0094K\2\u02ce\u02d2\5\u0096L\2\u02cf"+
		"\u02d2\5\u009aN\2\u02d0\u02d2\5\u0098M\2\u02d1\u02c6\3\2\2\2\u02d1\u02c7"+
		"\3\2\2\2\u02d1\u02c8\3\2\2\2\u02d1\u02c9\3\2\2\2\u02d1\u02ca\3\2\2\2\u02d1"+
		"\u02cb\3\2\2\2\u02d1\u02cc\3\2\2\2\u02d1\u02cd\3\2\2\2\u02d1\u02ce\3\2"+
		"\2\2\u02d1\u02cf\3\2\2\2\u02d1\u02d0\3\2\2\2\u02d2\u0085\3\2\2\2\u02d3"+
		"\u02d4\7\u0184\2\2\u02d4\u02d6\7\37\2\2\u02d5\u02d7\5t;\2\u02d6\u02d5"+
		"\3\2\2\2\u02d6\u02d7\3\2\2\2\u02d7\u02e1\3\2\2\2\u02d8\u02dd\5^\60\2\u02d9"+
		"\u02da\7%\2\2\u02da\u02dc\5^\60\2\u02db\u02d9\3\2\2\2\u02dc\u02df\3\2"+
		"\2\2\u02dd\u02db\3\2\2\2\u02dd\u02de\3\2\2\2\u02de\u02e2\3\2\2\2\u02df"+
		"\u02dd\3\2\2\2\u02e0\u02e2\7\21\2\2\u02e1\u02d8\3\2\2\2\u02e1\u02e0\3"+
		"\2\2\2\u02e1\u02e2\3\2\2\2\u02e2\u02e4\3\2\2\2\u02e3\u02e5\5\u00b4[\2"+
		"\u02e4\u02e3\3\2\2\2\u02e4\u02e5\3\2\2\2\u02e5\u02e8\3\2\2\2\u02e6\u02e7"+
		"\7\u0152\2\2\u02e7\u02e9\5^\60\2\u02e8\u02e6\3\2\2\2\u02e8\u02e9\3\2\2"+
		"\2\u02e9\u02ea\3\2\2\2\u02ea\u02eb\7 \2\2\u02eb\u0087\3\2\2\2\u02ec\u02ed"+
		"\5\30\r\2\u02ed\u02ee\7\37\2\2\u02ee\u02f3\5^\60\2\u02ef\u02f0\7%\2\2"+
		"\u02f0\u02f2\5^\60\2\u02f1\u02ef\3\2\2\2\u02f2\u02f5\3\2\2\2\u02f3\u02f1"+
		"\3\2\2\2\u02f3\u02f4\3\2\2\2\u02f4\u02f6\3\2\2\2\u02f5\u02f3\3\2\2\2\u02f6"+
		"\u02f7\7 \2\2\u02f7\u02f8\5v<\2\u02f8\u0089\3\2\2\2\u02f9\u02fa\7O\2\2"+
		"\u02fa\u02fb\7\37\2\2\u02fb\u02fc\5^\60\2\u02fc\u02fd\7]\2\2\u02fd\u02fe"+
		"\5\u00b8]\2\u02fe\u02ff\7 \2\2\u02ff\u008b\3\2\2\2\u0300\u0301\7\u0112"+
		"\2\2\u0301\u0302\7\37\2\2\u0302\u0303\5^\60\2\u0303\u0304\7%\2\2\u0304"+
		"\u0305\5\u00b8]\2\u0305\u0306\7 \2\2\u0306\u030f\3\2\2\2\u0307\u0308\7"+
		"\u0112\2\2\u0308\u0309\7\37\2\2\u0309\u030a\5^\60\2\u030a\u030b\7[\2\2"+
		"\u030b\u030c\5\30\r\2\u030c\u030d\7 \2\2\u030d\u030f\3\2\2\2\u030e\u0300"+
		"\3\2\2\2\u030e\u0307\3\2\2\2\u030f\u008d\3\2\2\2\u0310\u0311\7B\2\2\u0311"+
		"\u0312\7\37\2\2\u0312\u0313\5^\60\2\u0313\u0314\7m\2\2\u0314\u0315\5^"+
		"\60\2\u0315\u0316\7 \2\2\u0316\u008f\3\2\2\2\u0317\u0318\t\23\2\2\u0318"+
		"\u0319\7\37\2\2\u0319\u031a\5^\60\2\u031a\u031b\7R\2\2\u031b\u031e\7\u02c0"+
		"\2\2\u031c\u031d\7b\2\2\u031d\u031f\7\u02c0\2\2\u031e\u031c\3\2\2\2\u031e"+
		"\u031f\3\2\2\2\u031f\u0320\3\2\2\2\u0320\u0321\7 \2\2\u0321\u0091\3\2"+
		"\2\2\u0322\u0323\7\u0122\2\2\u0323\u0324\7\37\2\2\u0324\u0325\5\30\r\2"+
		"\u0325\u0326\7R\2\2\u0326\u0327\5^\60\2\u0327\u0328\7 \2\2\u0328\u0093"+
		"\3\2\2\2\u0329\u032a\7\177\2\2\u032a\u032b\7\37\2\2\u032b\u0330\5^\60"+
		"\2\u032c\u032d\7%\2\2\u032d\u032f\5^\60\2\u032e\u032c\3\2\2\2\u032f\u0332"+
		"\3\2\2\2\u0330\u032e\3\2\2\2\u0330\u0331\3\2\2\2\u0331\u0335\3\2\2\2\u0332"+
		"\u0330\3\2\2\2\u0333\u0334\7[\2\2\u0334\u0336\5\u00c4c\2\u0335\u0333\3"+
		"\2\2\2\u0335\u0336\3\2\2\2\u0336\u0337\3\2\2\2\u0337\u0338\7 \2\2\u0338"+
		"\u0095\3\2\2\2\u0339\u033a\7P\2\2\u033a\u033b\7\37\2\2\u033b\u033c\t\24"+
		"\2\2\u033c\u033d\7\u02bf\2\2\u033d\u033e\7R\2\2\u033e\u033f\7\u02bf\2"+
		"\2\u033f\u0340\7 \2\2\u0340\u0097\3\2\2\2\u0341\u0342\7I\2\2\u0342\u0343"+
		"\7\37\2\2\u0343\u0344\5\"\22\2\u0344\u0345\7 \2\2\u0345\u0099\3\2\2\2"+
		"\u0346\u0347\7\u0178\2\2\u0347\u0348\7\37\2\2\u0348\u034b\5^\60\2\u0349"+
		"\u034a\7]\2\2\u034a\u034c\5\u00b8]\2\u034b\u0349\3\2\2\2\u034b\u034c\3"+
		"\2\2\2\u034c\u034e\3\2\2\2\u034d\u034f\5\u009cO\2\u034e\u034d\3\2\2\2"+
		"\u034e\u034f\3\2\2\2\u034f\u0350\3\2\2\2\u0350\u0351\7 \2\2\u0351\u009b"+
		"\3\2\2\2\u0352\u035e\7\u00f5\2\2\u0353\u0358\5\u009eP\2\u0354\u0355\7"+
		"%\2\2\u0355\u0357\5\u009eP\2\u0356\u0354\3\2\2\2\u0357\u035a\3\2\2\2\u0358"+
		"\u0356\3\2\2\2\u0358\u0359\3\2\2\2\u0359\u035f\3\2\2\2\u035a\u0358\3\2"+
		"\2\2\u035b\u035c\7\u02c0\2\2\u035c\u035d\7\20\2\2\u035d\u035f\7\u02c0"+
		"\2\2\u035e\u0353\3\2\2\2\u035e\u035b\3\2\2\2\u035f\u009d\3\2\2\2\u0360"+
		"\u0362\7\u02c0\2\2\u0361\u0363\t\25\2\2\u0362\u0361\3\2\2\2\u0362\u0363"+
		"\3\2\2\2\u0363\u0365\3\2\2\2\u0364\u0366\7\u0150\2\2\u0365\u0364\3\2\2"+
		"\2\u0365\u0366\3\2\2\2\u0366\u009f\3\2\2\2\u0367\u0368\5\u00a2R\2\u0368"+
		"\u0372\7\37\2\2\u0369\u036e\5^\60\2\u036a\u036b\7%\2\2\u036b\u036d\5^"+
		"\60\2\u036c\u036a\3\2\2\2\u036d\u0370\3\2\2\2\u036e\u036c\3\2\2\2\u036e"+
		"\u036f\3\2\2\2\u036f\u0373\3\2\2\2\u0370\u036e\3\2\2\2\u0371\u0373\7\21"+
		"\2\2\u0372\u0369\3\2\2\2\u0372\u0371\3\2\2\2\u0372\u0373\3\2\2\2\u0373"+
		"\u0374\3\2\2\2\u0374\u0375\7 \2\2\u0375\u00a1\3\2\2\2\u0376\u0401\5\30"+
		"\r\2\u0377\u0401\7_\2\2\u0378\u0401\7\u0195\2\2\u0379\u0401\7\u022a\2"+
		"\2\u037a\u0401\7\u0086\2\2\u037b\u0401\7\u0087\2\2\u037c\u0401\7\u0136"+
		"\2\2\u037d\u0401\7\u00c2\2\2\u037e\u0401\7\u0082\2\2\u037f\u0401\7Q\2"+
		"\2\u0380\u0401\7\u00cc\2\2\u0381\u0401\7\u00ba\2\2\u0382\u0401\7X\2\2"+
		"\u0383\u0401\7Y\2\2\u0384\u0401\7\u022b\2\2\u0385\u0401\7\u022c\2\2\u0386"+
		"\u0401\7\u0083\2\2\u0387\u0401\7\u0231\2\2\u0388\u0401\7\u0232\2\2\u0389"+
		"\u0401\7\u008c\2\2\u038a\u0401\7\u0233\2\2\u038b\u0401\7\u0234\2\2\u038c"+
		"\u0401\7\u0235\2\2\u038d\u0401\7\u0236\2\2\u038e\u0401\7\u0241\2\2\u038f"+
		"\u0401\7\u0242\2\2\u0390\u0401\7\u0243\2\2\u0391\u0401\7\u0244\2\2\u0392"+
		"\u0401\7\u0245\2\2\u0393\u0401\7\u0246\2\2\u0394\u0401\7\u0247\2\2\u0395"+
		"\u0401\7\u0248\2\2\u0396\u0401\7\u0237\2\2\u0397\u0401\7\u0249\2\2\u0398"+
		"\u0401\7\u024a\2\2\u0399\u0401\7\u024b\2\2\u039a\u0401\7\u024c\2\2\u039b"+
		"\u0401\7\u024d\2\2\u039c\u0401\7\u024e\2\2\u039d\u0401\7\u024f\2\2\u039e"+
		"\u0401\7\u0250\2\2\u039f\u0401\7\u0251\2\2\u03a0\u0401\7\u0252\2\2\u03a1"+
		"\u0401\7\u0253\2\2\u03a2\u0401\7\u0254\2\2\u03a3\u0401\7\u0255\2\2\u03a4"+
		"\u0401\7\u0256\2\2\u03a5\u0401\7\u0257\2\2\u03a6\u0401\7\u0258\2\2\u03a7"+
		"\u0401\7\u0259\2\2\u03a8\u0401\7\u025a\2\2\u03a9\u0401\7\u025b\2\2\u03aa"+
		"\u0401\7\u025c\2\2\u03ab\u0401\7\u025d\2\2\u03ac\u0401\7\u025e\2\2\u03ad"+
		"\u0401\7\u025f\2\2\u03ae\u0401\7\u0260\2\2\u03af\u0401\7\u0261\2\2\u03b0"+
		"\u0401\7\u0262\2\2\u03b1\u0401\7\u0263\2\2\u03b2\u0401\7\u0264\2\2\u03b3"+
		"\u0401\7\u0265\2\2\u03b4\u0401\7\u0266\2\2\u03b5\u0401\7\u0267\2\2\u03b6"+
		"\u0401\7\u0268\2\2\u03b7\u0401\7\u0269\2\2\u03b8\u0401\7\u026a\2\2\u03b9"+
		"\u0401\7\u026b\2\2\u03ba\u0401\7\u026c\2\2\u03bb\u0401\7\u026d\2\2\u03bc"+
		"\u0401\7\u026e\2\2\u03bd\u0401\7\u026f\2\2\u03be\u0401\7\u0270\2\2\u03bf"+
		"\u0401\7\u0271\2\2\u03c0\u0401\7\u0272\2\2\u03c1\u0401\7\u0273\2\2\u03c2"+
		"\u0401\7\u0274\2\2\u03c3\u0401\7\u0275\2\2\u03c4\u0401\7\u0276\2\2\u03c5"+
		"\u0401\7\u0277\2\2\u03c6\u0401\7\u0278\2\2\u03c7\u0401\7\u0279\2\2\u03c8"+
		"\u0401\7\u027a\2\2\u03c9\u0401\7\u027b\2\2\u03ca\u0401\7\u027c\2\2\u03cb"+
		"\u0401\7\u027d\2\2\u03cc\u0401\7\u027e\2\2\u03cd\u0401\7\u027f\2\2\u03ce"+
		"\u0401\7\u0280\2\2\u03cf\u0401\7\u0281\2\2\u03d0\u0401\7\u0282\2\2\u03d1"+
		"\u0401\7\u0283\2\2\u03d2\u0401\7\u0284\2\2\u03d3\u0401\7\u0285\2\2\u03d4"+
		"\u0401\7\u0286\2\2\u03d5\u0401\7\u0287\2\2\u03d6\u0401\7\u0288\2\2\u03d7"+
		"\u0401\7\u0289\2\2\u03d8\u0401\7\u028a\2\2\u03d9\u0401\7\u028b\2\2\u03da"+
		"\u0401\7\u028c\2\2\u03db\u0401\7\u028d\2\2\u03dc\u0401\7\u028e\2\2\u03dd"+
		"\u0401\7\u028f\2\2\u03de\u0401\7\u0290\2\2\u03df\u0401\7\u0291\2\2\u03e0"+
		"\u0401\7\u0292\2\2\u03e1\u0401\7\u0293\2\2\u03e2\u0401\7\u0294\2\2\u03e3"+
		"\u0401\7\u0295\2\2\u03e4\u0401\7\u0296\2\2\u03e5\u0401\7\u0297\2\2\u03e6"+
		"\u0401\7\u0298\2\2\u03e7\u0401\7\u0299\2\2\u03e8\u0401\7\u029a\2\2\u03e9"+
		"\u0401\7\u029b\2\2\u03ea\u0401\7\u029c\2\2\u03eb\u0401\7\u029d\2\2\u03ec"+
		"\u0401\7\u029e\2\2\u03ed\u0401\7\u029f\2\2\u03ee\u0401\7\u02a0\2\2\u03ef"+
		"\u0401\7\u0084\2\2\u03f0\u0401\7\u0238\2\2\u03f1\u0401\7\u0085\2\2\u03f2"+
		"\u0401\7\u0239\2\2\u03f3\u0401\7\u023a\2\2\u03f4\u0401\7\u023b\2\2\u03f5"+
		"\u0401\7\u023c\2\2\u03f6\u0401\7\u023d\2\2\u03f7\u0401\7\u023e\2\2\u03f8"+
		"\u0401\7\u023f\2\2\u03f9\u0401\7\u0240\2\2\u03fa\u0401\7\u022d\2\2\u03fb"+
		"\u0401\7\u022e\2\2\u03fc\u0401\7\u0083\2\2\u03fd\u0401\7\u022f\2\2\u03fe"+
		"\u0401\7\u0230\2\2\u03ff\u0401\3\2\2\2\u0400\u0376\3\2\2\2\u0400\u0377"+
		"\3\2\2\2\u0400\u0378\3\2\2\2\u0400\u0379\3\2\2\2\u0400\u037a\3\2\2\2\u0400"+
		"\u037b\3\2\2\2\u0400\u037c\3\2\2\2\u0400\u037d\3\2\2\2\u0400\u037e\3\2"+
		"\2\2\u0400\u037f\3\2\2\2\u0400\u0380\3\2\2\2\u0400\u0381\3\2\2\2\u0400"+
		"\u0382\3\2\2\2\u0400\u0383\3\2\2\2\u0400\u0384\3\2\2\2\u0400\u0385\3\2"+
		"\2\2\u0400\u0386\3\2\2\2\u0400\u0387\3\2\2\2\u0400\u0388\3\2\2\2\u0400"+
		"\u0389\3\2\2\2\u0400\u038a\3\2\2\2\u0400\u038b\3\2\2\2\u0400\u038c\3\2"+
		"\2\2\u0400\u038d\3\2\2\2\u0400\u038e\3\2\2\2\u0400\u038f\3\2\2\2\u0400"+
		"\u0390\3\2\2\2\u0400\u0391\3\2\2\2\u0400\u0392\3\2\2\2\u0400\u0393\3\2"+
		"\2\2\u0400\u0394\3\2\2\2\u0400\u0395\3\2\2\2\u0400\u0396\3\2\2\2\u0400"+
		"\u0397\3\2\2\2\u0400\u0398\3\2\2\2\u0400\u0399\3\2\2\2\u0400\u039a\3\2"+
		"\2\2\u0400\u039b\3\2\2\2\u0400\u039c\3\2\2\2\u0400\u039d\3\2\2\2\u0400"+
		"\u039e\3\2\2\2\u0400\u039f\3\2\2\2\u0400\u03a0\3\2\2\2\u0400\u03a1\3\2"+
		"\2\2\u0400\u03a2\3\2\2\2\u0400\u03a3\3\2\2\2\u0400\u03a4\3\2\2\2\u0400"+
		"\u03a5\3\2\2\2\u0400\u03a6\3\2\2\2\u0400\u03a7\3\2\2\2\u0400\u03a8\3\2"+
		"\2\2\u0400\u03a9\3\2\2\2\u0400\u03aa\3\2\2\2\u0400\u03ab\3\2\2\2\u0400"+
		"\u03ac\3\2\2\2\u0400\u03ad\3\2\2\2\u0400\u03ae\3\2\2\2\u0400\u03af\3\2"+
		"\2\2\u0400\u03b0\3\2\2\2\u0400\u03b1\3\2\2\2\u0400\u03b2\3\2\2\2\u0400"+
		"\u03b3\3\2\2\2\u0400\u03b4\3\2\2\2\u0400\u03b5\3\2\2\2\u0400\u03b6\3\2"+
		"\2\2\u0400\u03b7\3\2\2\2\u0400\u03b8\3\2\2\2\u0400\u03b9\3\2\2\2\u0400"+
		"\u03ba\3\2\2\2\u0400\u03bb\3\2\2\2\u0400\u03bc\3\2\2\2\u0400\u03bd\3\2"+
		"\2\2\u0400\u03be\3\2\2\2\u0400\u03bf\3\2\2\2\u0400\u03c0\3\2\2\2\u0400"+
		"\u03c1\3\2\2\2\u0400\u03c2\3\2\2\2\u0400\u03c3\3\2\2\2\u0400\u03c4\3\2"+
		"\2\2\u0400\u03c5\3\2\2\2\u0400\u03c6\3\2\2\2\u0400\u03c7\3\2\2\2\u0400"+
		"\u03c8\3\2\2\2\u0400\u03c9\3\2\2\2\u0400\u03ca\3\2\2\2\u0400\u03cb\3\2"+
		"\2\2\u0400\u03cc\3\2\2\2\u0400\u03cd\3\2\2\2\u0400\u03ce\3\2\2\2\u0400"+
		"\u03cf\3\2\2\2\u0400\u03d0\3\2\2\2\u0400\u03d1\3\2\2\2\u0400\u03d2\3\2"+
		"\2\2\u0400\u03d3\3\2\2\2\u0400\u03d4\3\2\2\2\u0400\u03d5\3\2\2\2\u0400"+
		"\u03d6\3\2\2\2\u0400\u03d7\3\2\2\2\u0400\u03d8\3\2\2\2\u0400\u03d9\3\2"+
		"\2\2\u0400\u03da\3\2\2\2\u0400\u03db\3\2\2\2\u0400\u03dc\3\2\2\2\u0400"+
		"\u03dd\3\2\2\2\u0400\u03de\3\2\2\2\u0400\u03df\3\2\2\2\u0400\u03e0\3\2"+
		"\2\2\u0400\u03e1\3\2\2\2\u0400\u03e2\3\2\2\2\u0400\u03e3\3\2\2\2\u0400"+
		"\u03e4\3\2\2\2\u0400\u03e5\3\2\2\2\u0400\u03e6\3\2\2\2\u0400\u03e7\3\2"+
		"\2\2\u0400\u03e8\3\2\2\2\u0400\u03e9\3\2\2\2\u0400\u03ea\3\2\2\2\u0400"+
		"\u03eb\3\2\2\2\u0400\u03ec\3\2\2\2\u0400\u03ed\3\2\2\2\u0400\u03ee\3\2"+
		"\2\2\u0400\u03ef\3\2\2\2\u0400\u03f0\3\2\2\2\u0400\u03f1\3\2\2\2\u0400"+
		"\u03f2\3\2\2\2\u0400\u03f3\3\2\2\2\u0400\u03f4\3\2\2\2\u0400\u03f5\3\2"+
		"\2\2\u0400\u03f6\3\2\2\2\u0400\u03f7\3\2\2\2\u0400\u03f8\3\2\2\2\u0400"+
		"\u03f9\3\2\2\2\u0400\u03fa\3\2\2\2\u0400\u03fb\3\2\2\2\u0400\u03fc\3\2"+
		"\2\2\u0400\u03fd\3\2\2\2\u0400\u03fe\3\2\2\2\u0400\u03ff\3\2\2\2\u0401"+
		"\u00a3\3\2\2\2\u0402\u0403\7\u0133\2\2\u0403\u0404\58\35\2\u0404\u0405"+
		"\7\u015e\2\2\u0405\u0407\5^\60\2\u0406\u0408\5\u00a6T\2\u0407\u0406\3"+
		"\2\2\2\u0407\u0408\3\2\2\2\u0408\u00a5\3\2\2\2\u0409\u040a\7m\2\2\u040a"+
		"\u040b\7S\2\2\u040b\u040c\7\u015f\2\2\u040c\u041b\7\u0160\2\2\u040d\u040e"+
		"\7m\2\2\u040e\u040f\7S\2\2\u040f\u0410\7\u015f\2\2\u0410\u0411\7\u0160"+
		"\2\2\u0411\u0412\7J\2\2\u0412\u0413\7\u0161\2\2\u0413\u041b\7\u0163\2"+
		"\2\u0414\u0415\7m\2\2\u0415\u0416\7}\2\2\u0416\u041b\7\u0160\2\2\u0417"+
		"\u0418\7J\2\2\u0418\u0419\7\u0161\2\2\u0419\u041b\7\u0163\2\2\u041a\u0409"+
		"\3\2\2\2\u041a\u040d\3\2\2\2\u041a\u0414\3\2\2\2\u041a\u0417\3\2\2\2\u041b"+
		"\u00a7\3\2\2\2\u041c\u041e\7M\2\2\u041d\u041f\5l\67\2\u041e\u041d\3\2"+
		"\2\2\u041e\u041f\3\2\2\2\u041f\u0421\3\2\2\2\u0420\u0422\5\u00aaV\2\u0421"+
		"\u0420\3\2\2\2\u0422\u0423\3\2\2\2\u0423\u0421\3\2\2\2\u0423\u0424\3\2"+
		"\2\2\u0424\u0426\3\2\2\2\u0425\u0427\5\u00acW\2\u0426\u0425\3\2\2\2\u0426"+
		"\u0427\3\2\2\2\u0427\u0428\3\2\2\2\u0428\u0429\7\u011b\2\2\u0429\u00a9"+
		"\3\2\2\2\u042a\u042b\7N\2\2\u042b\u042c\5^\60\2\u042c\u042d\7a\2\2\u042d"+
		"\u042e\5^\60\2\u042e\u00ab\3\2\2\2\u042f\u0430\7`\2\2\u0430\u0431\5^\60"+
		"\2\u0431\u00ad\3\2\2\2\u0432\u0433\7\u0082\2\2\u0433\u0434\5^\60\2\u0434"+
		"\u0435\5\u00b0Y\2\u0435\u00af\3\2\2\2\u0436\u0437\t\26\2\2\u0437\u00b1"+
		"\3\2\2\2\u0438\u0439\7\3\2\2\u0439\u00b3\3\2\2\2\u043a\u043b\7q\2\2\u043b"+
		"\u043c\7s\2\2\u043c\u0441\5\u00b6\\\2\u043d\u043e\7%\2\2\u043e\u0440\5"+
		"\u00b6\\\2\u043f\u043d\3\2\2\2\u0440\u0443\3\2\2\2\u0441\u043f\3\2\2\2"+
		"\u0441\u0442\3\2\2\2\u0442\u00b5\3\2\2\2\u0443\u0441\3\2\2\2\u0444\u0448"+
		"\5\"\22\2\u0445\u0448\5\b\5\2\u0446\u0448\5^\60\2\u0447\u0444\3\2\2\2"+
		"\u0447\u0445\3\2\2\2\u0447\u0446\3\2\2\2\u0448\u044a\3\2\2\2\u0449\u044b"+
		"\t\25\2\2\u044a\u0449\3\2\2\2\u044a\u044b\3\2\2\2\u044b\u00b7\3\2\2\2"+
		"\u044c\u044e\5\u00ba^\2\u044d\u044f\5\u00bc_\2\u044e\u044d\3\2\2\2\u044e"+
		"\u044f\3\2\2\2\u044f\u0451\3\2\2\2\u0450\u0452\5\u00c0a\2\u0451\u0450"+
		"\3\2\2\2\u0451\u0452\3\2\2\2\u0452\u0454\3\2\2\2\u0453\u0455\5\u00c2b"+
		"\2\u0454\u0453\3\2\2\2\u0454\u0455\3\2\2\2\u0455\u0457\3\2\2\2\u0456\u0458"+
		"\t\27\2\2\u0457\u0456\3\2\2\2\u0457\u0458\3\2\2\2\u0458\u045a\3\2\2\2"+
		"\u0459\u045b\7\u0159\2\2\u045a\u0459\3\2\2\2\u045a\u045b\3\2\2\2\u045b"+
		"\u0465\3\2\2\2\u045c\u045d\5\u00ba^\2\u045d\u045f\5\u00be`\2\u045e\u0460"+
		"\5\u00c0a\2\u045f\u045e\3\2\2\2\u045f\u0460\3\2\2\2\u0460\u0462\3\2\2"+
		"\2\u0461\u0463\5\u00c2b\2\u0462\u0461\3\2\2\2\u0462\u0463\3\2\2\2\u0463"+
		"\u0465\3\2\2\2\u0464\u044c\3\2\2\2\u0464\u045c\3\2\2\2\u0465\u00b9\3\2"+
		"\2\2\u0466\u0467\t\30\2\2\u0467\u00bb\3\2\2\2\u0468\u0469\7\37\2\2\u0469"+
		"\u046c\7\u02c0\2\2\u046a\u046b\7%\2\2\u046b\u046d\7\u02c0\2\2\u046c\u046a"+
		"\3\2\2\2\u046c\u046d\3\2\2\2\u046d\u046e\3\2\2\2\u046e\u046f\7 \2\2\u046f"+
		"\u00bd\3\2\2\2\u0470\u0471\7\37\2\2\u0471\u0476\7\u02bf\2\2\u0472\u0473"+
		"\7%\2\2\u0473\u0475\7\u02bf\2\2\u0474\u0472\3\2\2\2\u0475\u0478\3\2\2"+
		"\2\u0476\u0474\3\2\2\2\u0476\u0477\3\2\2\2\u0477\u0479\3\2\2\2\u0478\u0476"+
		"\3\2\2\2\u0479\u047a\7 \2\2\u047a\u00bf\3\2\2\2\u047b\u047c\t\31\2\2\u047c"+
		"\u047e\79\2\2\u047d\u047f\7\30\2\2\u047e\u047d\3\2\2\2\u047e\u047f\3\2"+
		"\2\2\u047f\u0480\3\2\2\2\u0480\u0481\5\u00c4c\2\u0481\u00c1\3\2\2\2\u0482"+
		"\u0484\7\u010b\2\2\u0483\u0485\7\30\2\2\u0484\u0483\3\2\2\2\u0484\u0485"+
		"\3\2\2\2\u0485\u0488\3\2\2\2\u0486\u0489\7\u02bf\2\2\u0487\u0489\5\u00c4"+
		"c\2\u0488\u0486\3\2\2\2\u0488\u0487\3\2\2\2\u0489\u00c3\3\2\2\2\u048a"+
		"\u048d\5\30\r\2\u048b\u048c\7\24\2\2\u048c\u048e\5\30\r\2\u048d\u048b"+
		"\3\2\2\2\u048d\u048e\3\2\2\2\u048e\u00c5\3\2\2\2\u048f\u0494\5\u00c4c"+
		"\2\u0490\u0491\7%\2\2\u0491\u0493\5\u00c4c\2\u0492\u0490\3\2\2\2\u0493"+
		"\u0496\3\2\2\2\u0494\u0492\3\2\2\2\u0494\u0495\3\2\2\2\u0495\u00c7\3\2"+
		"\2\2\u0496\u0494\3\2\2\2q\u00d1\u00d4\u00d8\u00db\u00e6\u00e9\u00ed\u00f0"+
		"\u00f4\u0100\u0105\u0108\u010b\u010e\u0117\u011e\u0129\u0131\u0135\u0139"+
		"\u013f\u0142\u0148\u014b\u0152\u0159\u015d\u0160\u0167\u016b\u018a\u0194"+
		"\u01a6\u01af\u01b1\u01be\u01cd\u01cf\u01d6\u01dd\u01e6\u01ed\u01fb\u0201"+
		"\u0205\u020b\u023a\u023c\u0248\u0250\u0256\u0261\u026a\u026c\u026e\u0274"+
		"\u0279\u0280\u0284\u0288\u0294\u0297\u029a\u029d\u02a0\u02a9\u02af\u02bd"+
		"\u02d1\u02d6\u02dd\u02e1\u02e4\u02e8\u02f3\u030e\u031e\u0330\u0335\u034b"+
		"\u034e\u0358\u035e\u0362\u0365\u036e\u0372\u0400\u0407\u041a\u041e\u0423"+
		"\u0426\u0441\u0447\u044a\u044e\u0451\u0454\u0457\u045a\u045f\u0462\u0464"+
		"\u046c\u0476\u047e\u0484\u0488\u048d\u0494";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}