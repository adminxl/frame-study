package org.totoro.ddl;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/DDLStatement.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link DDLStatementParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface DDLStatementVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createTable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTable(DDLStatementParser.CreateTableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterTable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterTable(DDLStatementParser.AlterTableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropTable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropTable(DDLStatementParser.DropTableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropIndex}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropIndex(DDLStatementParser.DropIndexContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#truncateTable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTruncateTable(DDLStatementParser.TruncateTableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createIndex}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateIndex(DDLStatementParser.CreateIndexContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createDatabase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateDatabase(DDLStatementParser.CreateDatabaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterDatabase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterDatabase(DDLStatementParser.AlterDatabaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createDatabaseSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateDatabaseSpecification_(DDLStatementParser.CreateDatabaseSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropDatabase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropDatabase(DDLStatementParser.DropDatabaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterInstance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterInstance(DDLStatementParser.AlterInstanceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#instanceAction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstanceAction(DDLStatementParser.InstanceActionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createEvent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateEvent(DDLStatementParser.CreateEventContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterEvent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterEvent(DDLStatementParser.AlterEventContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropEvent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropEvent(DDLStatementParser.DropEventContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateFunction(DDLStatementParser.CreateFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterFunction(DDLStatementParser.AlterFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropFunction(DDLStatementParser.DropFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createProcedure}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateProcedure(DDLStatementParser.CreateProcedureContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterProcedure}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterProcedure(DDLStatementParser.AlterProcedureContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropProcedure}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropProcedure(DDLStatementParser.DropProcedureContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createServer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateServer(DDLStatementParser.CreateServerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterServer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterServer(DDLStatementParser.AlterServerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropServer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropServer(DDLStatementParser.DropServerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createView}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateView(DDLStatementParser.CreateViewContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterView}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterView(DDLStatementParser.AlterViewContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropView}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropView(DDLStatementParser.DropViewContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createTablespaceInnodb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTablespaceInnodb(DDLStatementParser.CreateTablespaceInnodbContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createTablespaceNdb}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTablespaceNdb(DDLStatementParser.CreateTablespaceNdbContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterTablespace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterTablespace(DDLStatementParser.AlterTablespaceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropTablespace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropTablespace(DDLStatementParser.DropTablespaceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createLogfileGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateLogfileGroup(DDLStatementParser.CreateLogfileGroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterLogfileGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterLogfileGroup(DDLStatementParser.AlterLogfileGroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropLogfileGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropLogfileGroup(DDLStatementParser.DropLogfileGroupContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createTrigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTrigger(DDLStatementParser.CreateTriggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createTableSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateTableSpecification_(DDLStatementParser.CreateTableSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableNotExistClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableNotExistClause_(DDLStatementParser.TableNotExistClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createDefinitionClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateDefinitionClause(DDLStatementParser.CreateDefinitionClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateDefinition(DDLStatementParser.CreateDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#columnDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnDefinition(DDLStatementParser.ColumnDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#storageOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStorageOption(DDLStatementParser.StorageOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#generatedOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGeneratedOption(DDLStatementParser.GeneratedOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dataTypeGenericOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeGenericOption(DDLStatementParser.DataTypeGenericOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#checkConstraintDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCheckConstraintDefinition(DDLStatementParser.CheckConstraintDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#referenceDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReferenceDefinition(DDLStatementParser.ReferenceDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#referenceOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReferenceOption_(DDLStatementParser.ReferenceOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#indexDefinition_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexDefinition_(DDLStatementParser.IndexDefinition_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#indexType_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexType_(DDLStatementParser.IndexType_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#keyParts_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyParts_(DDLStatementParser.KeyParts_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#keyPart_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyPart_(DDLStatementParser.KeyPart_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#indexOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexOption_(DDLStatementParser.IndexOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#constraintDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraintDefinition(DDLStatementParser.ConstraintDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#primaryKeyOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryKeyOption(DDLStatementParser.PrimaryKeyOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#primaryKey}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryKey(DDLStatementParser.PrimaryKeyContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#uniqueOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUniqueOption_(DDLStatementParser.UniqueOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#foreignKeyOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeignKeyOption(DDLStatementParser.ForeignKeyOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createLikeClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateLikeClause(DDLStatementParser.CreateLikeClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#createIndexSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateIndexSpecification_(DDLStatementParser.CreateIndexSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterDefinitionClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterDefinitionClause(DDLStatementParser.AlterDefinitionClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alterSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterSpecification(DDLStatementParser.AlterSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableOptions_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableOptions_(DDLStatementParser.TableOptions_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableOption_(DDLStatementParser.TableOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#addColumnSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddColumnSpecification(DDLStatementParser.AddColumnSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#firstOrAfterColumn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstOrAfterColumn(DDLStatementParser.FirstOrAfterColumnContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#addIndexSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddIndexSpecification(DDLStatementParser.AddIndexSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#addConstraintSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddConstraintSpecification(DDLStatementParser.AddConstraintSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#changeColumnSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChangeColumnSpecification(DDLStatementParser.ChangeColumnSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#modifyColumnSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModifyColumnSpecification(DDLStatementParser.ModifyColumnSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropColumnSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropColumnSpecification(DDLStatementParser.DropColumnSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropIndexSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropIndexSpecification(DDLStatementParser.DropIndexSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropPrimaryKeySpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropPrimaryKeySpecification(DDLStatementParser.DropPrimaryKeySpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#renameColumnSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRenameColumnSpecification(DDLStatementParser.RenameColumnSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#renameIndexSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRenameIndexSpecification(DDLStatementParser.RenameIndexSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#renameTableSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRenameTableSpecification(DDLStatementParser.RenameTableSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionDefinitions_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionDefinitions_(DDLStatementParser.PartitionDefinitions_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionDefinition_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionDefinition_(DDLStatementParser.PartitionDefinition_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionLessThanValue_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionLessThanValue_(DDLStatementParser.PartitionLessThanValue_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionValueList_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionValueList_(DDLStatementParser.PartitionValueList_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionDefinitionOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionDefinitionOption_(DDLStatementParser.PartitionDefinitionOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#subpartitionDefinition_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubpartitionDefinition_(DDLStatementParser.SubpartitionDefinition_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropTableSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropTableSpecification_(DDLStatementParser.DropTableSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableExistClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableExistClause_(DDLStatementParser.TableExistClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dropIndexSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDropIndexSpecification_(DDLStatementParser.DropIndexSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#ownerStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOwnerStatement(DDLStatementParser.OwnerStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#scheduleExpression_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScheduleExpression_(DDLStatementParser.ScheduleExpression_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#timestampValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimestampValue(DDLStatementParser.TimestampValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#routineBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoutineBody(DDLStatementParser.RoutineBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#serverOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitServerOption_(DDLStatementParser.ServerOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#routineOption_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoutineOption_(DDLStatementParser.RoutineOption_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#procedureParameter_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcedureParameter_(DDLStatementParser.ProcedureParameter_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#fileSizeLiteral_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFileSizeLiteral_(DDLStatementParser.FileSizeLiteral_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#parameterMarker}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterMarker(DDLStatementParser.ParameterMarkerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#literals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiterals(DDLStatementParser.LiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#stringLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiterals(DDLStatementParser.StringLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#numberLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberLiterals(DDLStatementParser.NumberLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateTimeLiterals(DDLStatementParser.DateTimeLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHexadecimalLiterals(DDLStatementParser.HexadecimalLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitValueLiterals(DDLStatementParser.BitValueLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#booleanLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanLiterals(DDLStatementParser.BooleanLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullValueLiterals(DDLStatementParser.NullValueLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#characterSetName_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacterSetName_(DDLStatementParser.CharacterSetName_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#collationName_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollationName_(DDLStatementParser.CollationName_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(DDLStatementParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#unreservedWord}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnreservedWord(DDLStatementParser.UnreservedWordContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(DDLStatementParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#schemaName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSchemaName(DDLStatementParser.SchemaNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableName(DDLStatementParser.TableNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#columnName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnName(DDLStatementParser.ColumnNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#indexName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexName(DDLStatementParser.IndexNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#userName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUserName(DDLStatementParser.UserNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#eventName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEventName(DDLStatementParser.EventNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#serverName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitServerName(DDLStatementParser.ServerNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#wrapperName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWrapperName(DDLStatementParser.WrapperNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#functionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionName(DDLStatementParser.FunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#viewName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitViewName(DDLStatementParser.ViewNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#owner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOwner(DDLStatementParser.OwnerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(DDLStatementParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableNames(DDLStatementParser.TableNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#columnNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnNames(DDLStatementParser.ColumnNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#groupName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupName(DDLStatementParser.GroupNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#shardLibraryName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShardLibraryName(DDLStatementParser.ShardLibraryNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#componentName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComponentName(DDLStatementParser.ComponentNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#pluginName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPluginName(DDLStatementParser.PluginNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#hostName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHostName(DDLStatementParser.HostNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#port}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPort(DDLStatementParser.PortContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#cloneInstance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloneInstance(DDLStatementParser.CloneInstanceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#cloneDir}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloneDir(DDLStatementParser.CloneDirContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#channelName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChannelName(DDLStatementParser.ChannelNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#logName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogName(DDLStatementParser.LogNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#roleName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoleName(DDLStatementParser.RoleNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#engineName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEngineName(DDLStatementParser.EngineNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#triggerName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerName(DDLStatementParser.TriggerNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#triggerTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerTime(DDLStatementParser.TriggerTimeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#userOrRole}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUserOrRole(DDLStatementParser.UserOrRoleContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionName(DDLStatementParser.PartitionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#triggerEvent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerEvent(DDLStatementParser.TriggerEventContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#triggerOrder}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerOrder(DDLStatementParser.TriggerOrderContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(DDLStatementParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#logicalOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOperator(DDLStatementParser.LogicalOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#notOperator_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotOperator_(DDLStatementParser.NotOperator_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#booleanPrimary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanPrimary(DDLStatementParser.BooleanPrimaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#comparisonOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonOperator(DDLStatementParser.ComparisonOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#predicate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(DDLStatementParser.PredicateContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#bitExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitExpr(DDLStatementParser.BitExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#simpleExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpr(DDLStatementParser.SimpleExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(DDLStatementParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#aggregationFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunction(DDLStatementParser.AggregationFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunctionName(DDLStatementParser.AggregationFunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#distinct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDistinct(DDLStatementParser.DistinctContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#overClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverClause_(DDLStatementParser.OverClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#windowSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowSpecification_(DDLStatementParser.WindowSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionClause_(DDLStatementParser.PartitionClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#frameClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameClause_(DDLStatementParser.FrameClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#frameStart_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameStart_(DDLStatementParser.FrameStart_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#frameEnd_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameEnd_(DDLStatementParser.FrameEnd_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#frameBetween_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameBetween_(DDLStatementParser.FrameBetween_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#specialFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecialFunction(DDLStatementParser.SpecialFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupConcatFunction(DDLStatementParser.GroupConcatFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#windowFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowFunction(DDLStatementParser.WindowFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#castFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCastFunction(DDLStatementParser.CastFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#convertFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConvertFunction(DDLStatementParser.ConvertFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#positionFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPositionFunction(DDLStatementParser.PositionFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#substringFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstringFunction(DDLStatementParser.SubstringFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#extractFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractFunction(DDLStatementParser.ExtractFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#charFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharFunction(DDLStatementParser.CharFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#trimFunction_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrimFunction_(DDLStatementParser.TrimFunction_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#valuesFunction_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuesFunction_(DDLStatementParser.ValuesFunction_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#weightStringFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWeightStringFunction(DDLStatementParser.WeightStringFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#levelClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLevelClause_(DDLStatementParser.LevelClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLevelInWeightListElement_(DDLStatementParser.LevelInWeightListElement_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#regularFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegularFunction(DDLStatementParser.RegularFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegularFunctionName_(DDLStatementParser.RegularFunctionName_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#matchExpression_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatchExpression_(DDLStatementParser.MatchExpression_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatchSearchModifier_(DDLStatementParser.MatchSearchModifier_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#caseExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseExpression(DDLStatementParser.CaseExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#caseWhen_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseWhen_(DDLStatementParser.CaseWhen_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#caseElse_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseElse_(DDLStatementParser.CaseElse_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#intervalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalExpression(DDLStatementParser.IntervalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#intervalUnit_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalUnit_(DDLStatementParser.IntervalUnit_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubquery(DDLStatementParser.SubqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#orderByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrderByClause(DDLStatementParser.OrderByClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#orderByItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrderByItem(DDLStatementParser.OrderByItemContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(DDLStatementParser.DataTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dataTypeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeName(DDLStatementParser.DataTypeNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#dataTypeLength}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeLength(DDLStatementParser.DataTypeLengthContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#collectionOptions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollectionOptions(DDLStatementParser.CollectionOptionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#characterSet_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacterSet_(DDLStatementParser.CharacterSet_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#collateClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollateClause_(DDLStatementParser.CollateClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIgnoredIdentifier_(DDLStatementParser.IgnoredIdentifier_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIgnoredIdentifiers_(DDLStatementParser.IgnoredIdentifiers_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#insert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert(DDLStatementParser.InsertContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#insertSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertSpecification_(DDLStatementParser.InsertSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#insertValuesClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertValuesClause(DDLStatementParser.InsertValuesClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#insertSelectClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertSelectClause(DDLStatementParser.InsertSelectClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#onDuplicateKeyClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOnDuplicateKeyClause(DDLStatementParser.OnDuplicateKeyClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#replace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(DDLStatementParser.ReplaceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#replaceSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplaceSpecification_(DDLStatementParser.ReplaceSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#update}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate(DDLStatementParser.UpdateContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#updateSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdateSpecification_(DDLStatementParser.UpdateSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(DDLStatementParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#setAssignmentsClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetAssignmentsClause(DDLStatementParser.SetAssignmentsClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#assignmentValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentValues(DDLStatementParser.AssignmentValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#assignmentValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentValue(DDLStatementParser.AssignmentValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#blobValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlobValue(DDLStatementParser.BlobValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#delete}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelete(DDLStatementParser.DeleteContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#deleteSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeleteSpecification_(DDLStatementParser.DeleteSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#singleTableClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleTableClause(DDLStatementParser.SingleTableClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#multipleTablesClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleTablesClause(DDLStatementParser.MultipleTablesClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#multipleTableNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleTableNames(DDLStatementParser.MultipleTableNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#select}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect(DDLStatementParser.SelectContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(DDLStatementParser.CallContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#doStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoStatement(DDLStatementParser.DoStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#handlerStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerStatement(DDLStatementParser.HandlerStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerOpenStatement(DDLStatementParser.HandlerOpenStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerReadIndexStatement(DDLStatementParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerReadStatement(DDLStatementParser.HandlerReadStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerCloseStatement(DDLStatementParser.HandlerCloseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#importStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportStatement(DDLStatementParser.ImportStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#loadDataStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoadDataStatement(DDLStatementParser.LoadDataStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoadXmlStatement(DDLStatementParser.LoadXmlStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#withClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWithClause_(DDLStatementParser.WithClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#cteClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCteClause_(DDLStatementParser.CteClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#unionClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnionClause(DDLStatementParser.UnionClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#selectClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectClause(DDLStatementParser.SelectClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#selectSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectSpecification(DDLStatementParser.SelectSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#duplicateSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDuplicateSpecification(DDLStatementParser.DuplicateSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#projections}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProjections(DDLStatementParser.ProjectionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#projection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProjection(DDLStatementParser.ProjectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlias(DDLStatementParser.AliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#unqualifiedShorthand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnqualifiedShorthand(DDLStatementParser.UnqualifiedShorthandContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#qualifiedShorthand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedShorthand(DDLStatementParser.QualifiedShorthandContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#fromClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFromClause(DDLStatementParser.FromClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableReferences}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableReferences(DDLStatementParser.TableReferencesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#escapedTableReference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEscapedTableReference(DDLStatementParser.EscapedTableReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableReference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableReference(DDLStatementParser.TableReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#tableFactor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableFactor(DDLStatementParser.TableFactorContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#partitionNames_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionNames_(DDLStatementParser.PartitionNames_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#indexHintList_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexHintList_(DDLStatementParser.IndexHintList_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#indexHint_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexHint_(DDLStatementParser.IndexHint_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#joinedTable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoinedTable(DDLStatementParser.JoinedTableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#joinSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoinSpecification(DDLStatementParser.JoinSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#whereClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereClause(DDLStatementParser.WhereClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#groupByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupByClause(DDLStatementParser.GroupByClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#havingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHavingClause(DDLStatementParser.HavingClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#limitClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitClause(DDLStatementParser.LimitClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#limitRowCount}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitRowCount(DDLStatementParser.LimitRowCountContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#limitOffset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitOffset(DDLStatementParser.LimitOffsetContext ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#windowClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowClause_(DDLStatementParser.WindowClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#windowItem_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowItem_(DDLStatementParser.WindowItem_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#selectLinesInto_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectLinesInto_(DDLStatementParser.SelectLinesInto_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#selectFieldsInto_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectFieldsInto_(DDLStatementParser.SelectFieldsInto_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#selectIntoExpression_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectIntoExpression_(DDLStatementParser.SelectIntoExpression_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DDLStatementParser#lockClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLockClause(DDLStatementParser.LockClauseContext ctx);
}