package org.totoro.ddl;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/BaseRule.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BaseRuleParser}.
 */
public interface BaseRuleListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#parameterMarker}.
	 * @param ctx the parse tree
	 */
	void enterParameterMarker(BaseRuleParser.ParameterMarkerContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#parameterMarker}.
	 * @param ctx the parse tree
	 */
	void exitParameterMarker(BaseRuleParser.ParameterMarkerContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#literals}.
	 * @param ctx the parse tree
	 */
	void enterLiterals(BaseRuleParser.LiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#literals}.
	 * @param ctx the parse tree
	 */
	void exitLiterals(BaseRuleParser.LiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#stringLiterals}.
	 * @param ctx the parse tree
	 */
	void enterStringLiterals(BaseRuleParser.StringLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#stringLiterals}.
	 * @param ctx the parse tree
	 */
	void exitStringLiterals(BaseRuleParser.StringLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#numberLiterals}.
	 * @param ctx the parse tree
	 */
	void enterNumberLiterals(BaseRuleParser.NumberLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#numberLiterals}.
	 * @param ctx the parse tree
	 */
	void exitNumberLiterals(BaseRuleParser.NumberLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 */
	void enterDateTimeLiterals(BaseRuleParser.DateTimeLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 */
	void exitDateTimeLiterals(BaseRuleParser.DateTimeLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 */
	void enterHexadecimalLiterals(BaseRuleParser.HexadecimalLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 */
	void exitHexadecimalLiterals(BaseRuleParser.HexadecimalLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 */
	void enterBitValueLiterals(BaseRuleParser.BitValueLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 */
	void exitBitValueLiterals(BaseRuleParser.BitValueLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#booleanLiterals}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiterals(BaseRuleParser.BooleanLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#booleanLiterals}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiterals(BaseRuleParser.BooleanLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 */
	void enterNullValueLiterals(BaseRuleParser.NullValueLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 */
	void exitNullValueLiterals(BaseRuleParser.NullValueLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#characterSetName_}.
	 * @param ctx the parse tree
	 */
	void enterCharacterSetName_(BaseRuleParser.CharacterSetName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#characterSetName_}.
	 * @param ctx the parse tree
	 */
	void exitCharacterSetName_(BaseRuleParser.CharacterSetName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#collationName_}.
	 * @param ctx the parse tree
	 */
	void enterCollationName_(BaseRuleParser.CollationName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#collationName_}.
	 * @param ctx the parse tree
	 */
	void exitCollationName_(BaseRuleParser.CollationName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(BaseRuleParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(BaseRuleParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#unreservedWord}.
	 * @param ctx the parse tree
	 */
	void enterUnreservedWord(BaseRuleParser.UnreservedWordContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#unreservedWord}.
	 * @param ctx the parse tree
	 */
	void exitUnreservedWord(BaseRuleParser.UnreservedWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(BaseRuleParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(BaseRuleParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#schemaName}.
	 * @param ctx the parse tree
	 */
	void enterSchemaName(BaseRuleParser.SchemaNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#schemaName}.
	 * @param ctx the parse tree
	 */
	void exitSchemaName(BaseRuleParser.SchemaNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#tableName}.
	 * @param ctx the parse tree
	 */
	void enterTableName(BaseRuleParser.TableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#tableName}.
	 * @param ctx the parse tree
	 */
	void exitTableName(BaseRuleParser.TableNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#columnName}.
	 * @param ctx the parse tree
	 */
	void enterColumnName(BaseRuleParser.ColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#columnName}.
	 * @param ctx the parse tree
	 */
	void exitColumnName(BaseRuleParser.ColumnNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#indexName}.
	 * @param ctx the parse tree
	 */
	void enterIndexName(BaseRuleParser.IndexNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#indexName}.
	 * @param ctx the parse tree
	 */
	void exitIndexName(BaseRuleParser.IndexNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#userName}.
	 * @param ctx the parse tree
	 */
	void enterUserName(BaseRuleParser.UserNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#userName}.
	 * @param ctx the parse tree
	 */
	void exitUserName(BaseRuleParser.UserNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#eventName}.
	 * @param ctx the parse tree
	 */
	void enterEventName(BaseRuleParser.EventNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#eventName}.
	 * @param ctx the parse tree
	 */
	void exitEventName(BaseRuleParser.EventNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#serverName}.
	 * @param ctx the parse tree
	 */
	void enterServerName(BaseRuleParser.ServerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#serverName}.
	 * @param ctx the parse tree
	 */
	void exitServerName(BaseRuleParser.ServerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#wrapperName}.
	 * @param ctx the parse tree
	 */
	void enterWrapperName(BaseRuleParser.WrapperNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#wrapperName}.
	 * @param ctx the parse tree
	 */
	void exitWrapperName(BaseRuleParser.WrapperNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(BaseRuleParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(BaseRuleParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#viewName}.
	 * @param ctx the parse tree
	 */
	void enterViewName(BaseRuleParser.ViewNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#viewName}.
	 * @param ctx the parse tree
	 */
	void exitViewName(BaseRuleParser.ViewNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#owner}.
	 * @param ctx the parse tree
	 */
	void enterOwner(BaseRuleParser.OwnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#owner}.
	 * @param ctx the parse tree
	 */
	void exitOwner(BaseRuleParser.OwnerContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(BaseRuleParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(BaseRuleParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#tableNames}.
	 * @param ctx the parse tree
	 */
	void enterTableNames(BaseRuleParser.TableNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#tableNames}.
	 * @param ctx the parse tree
	 */
	void exitTableNames(BaseRuleParser.TableNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#columnNames}.
	 * @param ctx the parse tree
	 */
	void enterColumnNames(BaseRuleParser.ColumnNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#columnNames}.
	 * @param ctx the parse tree
	 */
	void exitColumnNames(BaseRuleParser.ColumnNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#groupName}.
	 * @param ctx the parse tree
	 */
	void enterGroupName(BaseRuleParser.GroupNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#groupName}.
	 * @param ctx the parse tree
	 */
	void exitGroupName(BaseRuleParser.GroupNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#shardLibraryName}.
	 * @param ctx the parse tree
	 */
	void enterShardLibraryName(BaseRuleParser.ShardLibraryNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#shardLibraryName}.
	 * @param ctx the parse tree
	 */
	void exitShardLibraryName(BaseRuleParser.ShardLibraryNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#componentName}.
	 * @param ctx the parse tree
	 */
	void enterComponentName(BaseRuleParser.ComponentNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#componentName}.
	 * @param ctx the parse tree
	 */
	void exitComponentName(BaseRuleParser.ComponentNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#pluginName}.
	 * @param ctx the parse tree
	 */
	void enterPluginName(BaseRuleParser.PluginNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#pluginName}.
	 * @param ctx the parse tree
	 */
	void exitPluginName(BaseRuleParser.PluginNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#hostName}.
	 * @param ctx the parse tree
	 */
	void enterHostName(BaseRuleParser.HostNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#hostName}.
	 * @param ctx the parse tree
	 */
	void exitHostName(BaseRuleParser.HostNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#port}.
	 * @param ctx the parse tree
	 */
	void enterPort(BaseRuleParser.PortContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#port}.
	 * @param ctx the parse tree
	 */
	void exitPort(BaseRuleParser.PortContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#cloneInstance}.
	 * @param ctx the parse tree
	 */
	void enterCloneInstance(BaseRuleParser.CloneInstanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#cloneInstance}.
	 * @param ctx the parse tree
	 */
	void exitCloneInstance(BaseRuleParser.CloneInstanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#cloneDir}.
	 * @param ctx the parse tree
	 */
	void enterCloneDir(BaseRuleParser.CloneDirContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#cloneDir}.
	 * @param ctx the parse tree
	 */
	void exitCloneDir(BaseRuleParser.CloneDirContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#channelName}.
	 * @param ctx the parse tree
	 */
	void enterChannelName(BaseRuleParser.ChannelNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#channelName}.
	 * @param ctx the parse tree
	 */
	void exitChannelName(BaseRuleParser.ChannelNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#logName}.
	 * @param ctx the parse tree
	 */
	void enterLogName(BaseRuleParser.LogNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#logName}.
	 * @param ctx the parse tree
	 */
	void exitLogName(BaseRuleParser.LogNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#roleName}.
	 * @param ctx the parse tree
	 */
	void enterRoleName(BaseRuleParser.RoleNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#roleName}.
	 * @param ctx the parse tree
	 */
	void exitRoleName(BaseRuleParser.RoleNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#engineName}.
	 * @param ctx the parse tree
	 */
	void enterEngineName(BaseRuleParser.EngineNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#engineName}.
	 * @param ctx the parse tree
	 */
	void exitEngineName(BaseRuleParser.EngineNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#triggerName}.
	 * @param ctx the parse tree
	 */
	void enterTriggerName(BaseRuleParser.TriggerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#triggerName}.
	 * @param ctx the parse tree
	 */
	void exitTriggerName(BaseRuleParser.TriggerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#triggerTime}.
	 * @param ctx the parse tree
	 */
	void enterTriggerTime(BaseRuleParser.TriggerTimeContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#triggerTime}.
	 * @param ctx the parse tree
	 */
	void exitTriggerTime(BaseRuleParser.TriggerTimeContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#userOrRole}.
	 * @param ctx the parse tree
	 */
	void enterUserOrRole(BaseRuleParser.UserOrRoleContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#userOrRole}.
	 * @param ctx the parse tree
	 */
	void exitUserOrRole(BaseRuleParser.UserOrRoleContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#partitionName}.
	 * @param ctx the parse tree
	 */
	void enterPartitionName(BaseRuleParser.PartitionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#partitionName}.
	 * @param ctx the parse tree
	 */
	void exitPartitionName(BaseRuleParser.PartitionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#triggerEvent}.
	 * @param ctx the parse tree
	 */
	void enterTriggerEvent(BaseRuleParser.TriggerEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#triggerEvent}.
	 * @param ctx the parse tree
	 */
	void exitTriggerEvent(BaseRuleParser.TriggerEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#triggerOrder}.
	 * @param ctx the parse tree
	 */
	void enterTriggerOrder(BaseRuleParser.TriggerOrderContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#triggerOrder}.
	 * @param ctx the parse tree
	 */
	void exitTriggerOrder(BaseRuleParser.TriggerOrderContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(BaseRuleParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(BaseRuleParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperator(BaseRuleParser.LogicalOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperator(BaseRuleParser.LogicalOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#notOperator_}.
	 * @param ctx the parse tree
	 */
	void enterNotOperator_(BaseRuleParser.NotOperator_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#notOperator_}.
	 * @param ctx the parse tree
	 */
	void exitNotOperator_(BaseRuleParser.NotOperator_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#booleanPrimary}.
	 * @param ctx the parse tree
	 */
	void enterBooleanPrimary(BaseRuleParser.BooleanPrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#booleanPrimary}.
	 * @param ctx the parse tree
	 */
	void exitBooleanPrimary(BaseRuleParser.BooleanPrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void enterComparisonOperator(BaseRuleParser.ComparisonOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void exitComparisonOperator(BaseRuleParser.ComparisonOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(BaseRuleParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(BaseRuleParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#bitExpr}.
	 * @param ctx the parse tree
	 */
	void enterBitExpr(BaseRuleParser.BitExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#bitExpr}.
	 * @param ctx the parse tree
	 */
	void exitBitExpr(BaseRuleParser.BitExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpr(BaseRuleParser.SimpleExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpr(BaseRuleParser.SimpleExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(BaseRuleParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(BaseRuleParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunction(BaseRuleParser.AggregationFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunction(BaseRuleParser.AggregationFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunctionName(BaseRuleParser.AggregationFunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunctionName(BaseRuleParser.AggregationFunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#distinct}.
	 * @param ctx the parse tree
	 */
	void enterDistinct(BaseRuleParser.DistinctContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#distinct}.
	 * @param ctx the parse tree
	 */
	void exitDistinct(BaseRuleParser.DistinctContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#overClause_}.
	 * @param ctx the parse tree
	 */
	void enterOverClause_(BaseRuleParser.OverClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#overClause_}.
	 * @param ctx the parse tree
	 */
	void exitOverClause_(BaseRuleParser.OverClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#windowSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterWindowSpecification_(BaseRuleParser.WindowSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#windowSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitWindowSpecification_(BaseRuleParser.WindowSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#partitionClause_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionClause_(BaseRuleParser.PartitionClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#partitionClause_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionClause_(BaseRuleParser.PartitionClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#frameClause_}.
	 * @param ctx the parse tree
	 */
	void enterFrameClause_(BaseRuleParser.FrameClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#frameClause_}.
	 * @param ctx the parse tree
	 */
	void exitFrameClause_(BaseRuleParser.FrameClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#frameStart_}.
	 * @param ctx the parse tree
	 */
	void enterFrameStart_(BaseRuleParser.FrameStart_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#frameStart_}.
	 * @param ctx the parse tree
	 */
	void exitFrameStart_(BaseRuleParser.FrameStart_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#frameEnd_}.
	 * @param ctx the parse tree
	 */
	void enterFrameEnd_(BaseRuleParser.FrameEnd_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#frameEnd_}.
	 * @param ctx the parse tree
	 */
	void exitFrameEnd_(BaseRuleParser.FrameEnd_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#frameBetween_}.
	 * @param ctx the parse tree
	 */
	void enterFrameBetween_(BaseRuleParser.FrameBetween_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#frameBetween_}.
	 * @param ctx the parse tree
	 */
	void exitFrameBetween_(BaseRuleParser.FrameBetween_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#specialFunction}.
	 * @param ctx the parse tree
	 */
	void enterSpecialFunction(BaseRuleParser.SpecialFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#specialFunction}.
	 * @param ctx the parse tree
	 */
	void exitSpecialFunction(BaseRuleParser.SpecialFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 */
	void enterGroupConcatFunction(BaseRuleParser.GroupConcatFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 */
	void exitGroupConcatFunction(BaseRuleParser.GroupConcatFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#windowFunction}.
	 * @param ctx the parse tree
	 */
	void enterWindowFunction(BaseRuleParser.WindowFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#windowFunction}.
	 * @param ctx the parse tree
	 */
	void exitWindowFunction(BaseRuleParser.WindowFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#castFunction}.
	 * @param ctx the parse tree
	 */
	void enterCastFunction(BaseRuleParser.CastFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#castFunction}.
	 * @param ctx the parse tree
	 */
	void exitCastFunction(BaseRuleParser.CastFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#convertFunction}.
	 * @param ctx the parse tree
	 */
	void enterConvertFunction(BaseRuleParser.ConvertFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#convertFunction}.
	 * @param ctx the parse tree
	 */
	void exitConvertFunction(BaseRuleParser.ConvertFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#positionFunction}.
	 * @param ctx the parse tree
	 */
	void enterPositionFunction(BaseRuleParser.PositionFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#positionFunction}.
	 * @param ctx the parse tree
	 */
	void exitPositionFunction(BaseRuleParser.PositionFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#substringFunction}.
	 * @param ctx the parse tree
	 */
	void enterSubstringFunction(BaseRuleParser.SubstringFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#substringFunction}.
	 * @param ctx the parse tree
	 */
	void exitSubstringFunction(BaseRuleParser.SubstringFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#extractFunction}.
	 * @param ctx the parse tree
	 */
	void enterExtractFunction(BaseRuleParser.ExtractFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#extractFunction}.
	 * @param ctx the parse tree
	 */
	void exitExtractFunction(BaseRuleParser.ExtractFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#charFunction}.
	 * @param ctx the parse tree
	 */
	void enterCharFunction(BaseRuleParser.CharFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#charFunction}.
	 * @param ctx the parse tree
	 */
	void exitCharFunction(BaseRuleParser.CharFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#trimFunction_}.
	 * @param ctx the parse tree
	 */
	void enterTrimFunction_(BaseRuleParser.TrimFunction_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#trimFunction_}.
	 * @param ctx the parse tree
	 */
	void exitTrimFunction_(BaseRuleParser.TrimFunction_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#valuesFunction_}.
	 * @param ctx the parse tree
	 */
	void enterValuesFunction_(BaseRuleParser.ValuesFunction_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#valuesFunction_}.
	 * @param ctx the parse tree
	 */
	void exitValuesFunction_(BaseRuleParser.ValuesFunction_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#weightStringFunction}.
	 * @param ctx the parse tree
	 */
	void enterWeightStringFunction(BaseRuleParser.WeightStringFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#weightStringFunction}.
	 * @param ctx the parse tree
	 */
	void exitWeightStringFunction(BaseRuleParser.WeightStringFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#levelClause_}.
	 * @param ctx the parse tree
	 */
	void enterLevelClause_(BaseRuleParser.LevelClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#levelClause_}.
	 * @param ctx the parse tree
	 */
	void exitLevelClause_(BaseRuleParser.LevelClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 */
	void enterLevelInWeightListElement_(BaseRuleParser.LevelInWeightListElement_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 */
	void exitLevelInWeightListElement_(BaseRuleParser.LevelInWeightListElement_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#regularFunction}.
	 * @param ctx the parse tree
	 */
	void enterRegularFunction(BaseRuleParser.RegularFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#regularFunction}.
	 * @param ctx the parse tree
	 */
	void exitRegularFunction(BaseRuleParser.RegularFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 */
	void enterRegularFunctionName_(BaseRuleParser.RegularFunctionName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 */
	void exitRegularFunctionName_(BaseRuleParser.RegularFunctionName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#matchExpression_}.
	 * @param ctx the parse tree
	 */
	void enterMatchExpression_(BaseRuleParser.MatchExpression_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#matchExpression_}.
	 * @param ctx the parse tree
	 */
	void exitMatchExpression_(BaseRuleParser.MatchExpression_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 */
	void enterMatchSearchModifier_(BaseRuleParser.MatchSearchModifier_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 */
	void exitMatchSearchModifier_(BaseRuleParser.MatchSearchModifier_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#caseExpression}.
	 * @param ctx the parse tree
	 */
	void enterCaseExpression(BaseRuleParser.CaseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#caseExpression}.
	 * @param ctx the parse tree
	 */
	void exitCaseExpression(BaseRuleParser.CaseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#caseWhen_}.
	 * @param ctx the parse tree
	 */
	void enterCaseWhen_(BaseRuleParser.CaseWhen_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#caseWhen_}.
	 * @param ctx the parse tree
	 */
	void exitCaseWhen_(BaseRuleParser.CaseWhen_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#caseElse_}.
	 * @param ctx the parse tree
	 */
	void enterCaseElse_(BaseRuleParser.CaseElse_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#caseElse_}.
	 * @param ctx the parse tree
	 */
	void exitCaseElse_(BaseRuleParser.CaseElse_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#intervalExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntervalExpression(BaseRuleParser.IntervalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#intervalExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntervalExpression(BaseRuleParser.IntervalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#intervalUnit_}.
	 * @param ctx the parse tree
	 */
	void enterIntervalUnit_(BaseRuleParser.IntervalUnit_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#intervalUnit_}.
	 * @param ctx the parse tree
	 */
	void exitIntervalUnit_(BaseRuleParser.IntervalUnit_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#subquery}.
	 * @param ctx the parse tree
	 */
	void enterSubquery(BaseRuleParser.SubqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#subquery}.
	 * @param ctx the parse tree
	 */
	void exitSubquery(BaseRuleParser.SubqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void enterOrderByClause(BaseRuleParser.OrderByClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void exitOrderByClause(BaseRuleParser.OrderByClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void enterOrderByItem(BaseRuleParser.OrderByItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void exitOrderByItem(BaseRuleParser.OrderByItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterDataType(BaseRuleParser.DataTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitDataType(BaseRuleParser.DataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#dataTypeName}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeName(BaseRuleParser.DataTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#dataTypeName}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeName(BaseRuleParser.DataTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#dataTypeLength}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeLength(BaseRuleParser.DataTypeLengthContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#dataTypeLength}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeLength(BaseRuleParser.DataTypeLengthContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#collectionOptions}.
	 * @param ctx the parse tree
	 */
	void enterCollectionOptions(BaseRuleParser.CollectionOptionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#collectionOptions}.
	 * @param ctx the parse tree
	 */
	void exitCollectionOptions(BaseRuleParser.CollectionOptionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#characterSet_}.
	 * @param ctx the parse tree
	 */
	void enterCharacterSet_(BaseRuleParser.CharacterSet_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#characterSet_}.
	 * @param ctx the parse tree
	 */
	void exitCharacterSet_(BaseRuleParser.CharacterSet_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#collateClause_}.
	 * @param ctx the parse tree
	 */
	void enterCollateClause_(BaseRuleParser.CollateClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#collateClause_}.
	 * @param ctx the parse tree
	 */
	void exitCollateClause_(BaseRuleParser.CollateClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 */
	void enterIgnoredIdentifier_(BaseRuleParser.IgnoredIdentifier_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 */
	void exitIgnoredIdentifier_(BaseRuleParser.IgnoredIdentifier_Context ctx);
	/**
	 * Enter a parse tree produced by {@link BaseRuleParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 */
	void enterIgnoredIdentifiers_(BaseRuleParser.IgnoredIdentifiers_Context ctx);
	/**
	 * Exit a parse tree produced by {@link BaseRuleParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 */
	void exitIgnoredIdentifiers_(BaseRuleParser.IgnoredIdentifiers_Context ctx);
}