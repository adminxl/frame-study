package org.totoro;

import com.sun.tools.javac.parser.JavacParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.totoro.dml.*;

/**
 * 插件：ANTLR v4 grammar plugin
 * <p>
 * 参考：https://www.jianshu.com/p/21f2afca65e8
 * https://cloud.tencent.com/developer/article/1556983
 * https://cloud.tencent.com/developer/article/1571188
 * https://cloud.tencent.com/developer/article/1556983
 * <p>
 * https://github.com/antlr/grammars-v4
 *
 * @author daocr
 * @date 2020/3/24
 */
public class DmlApp {

    public static void main(String[] args) {
        String sql = "select id,name from user_info where id=1 and name = 张三";
        //对每一个输入的字符串，构造一个 CodePointCharStream
        CodePointCharStream codePointCharStream = CharStreams.fromString(sql);

//        ANTLRInputStream input = new ANTLRInputStream(sql);  //将输入转成antlr的input流 老版本

        //用 cpcs 构造词法分析器 lexer，词法分析的作用是产生记号
        DMLStatementLexer lexer = new DMLStatementLexer(codePointCharStream);  //词法分析

        //用词法分析器 lexer 构造一个记号流 tokens
        CommonTokenStream tokens = new CommonTokenStream(lexer);  //转成token流

        DMLStatementParser parser = new DMLStatementParser(tokens); // 语法分析


        DMLStatementBaseVisitor<String> dmlStatementBaseVisitor = new DMLStatementBaseVisitor();


        String visit = dmlStatementBaseVisitor.visit(parser.select());

        System.out.println("\n" +visit);


//        ParseTreeWalker walker = new ParseTreeWalker();
//        DMLStatementBaseListener evalByListener = new DMLStatementBaseListener();
//        walker.walk(evalByListener, parser.select());



    }
}
