package org.totoro;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.totoro.ddl.DDLStatementLexer;
import org.totoro.ddl.DDLStatementParser;

/**
 * Hello world!
 *
 * @see https://www.jianshu.com/p/21f2afca65e8
 */
public class DdlApp {
    public static void main(String[] args) {

        String sql = "drop table  user";
        CodePointCharStream codePointCharStream = CharStreams.fromString(sql);
//        ANTLRInputStream input = new ANTLRInputStream(sql);  //将输入转成antlr的input流 老版本
        DDLStatementLexer lexer = new DDLStatementLexer(codePointCharStream);  //词法分析
        CommonTokenStream tokens = new CommonTokenStream(lexer);  //转成token流
        DDLStatementParser parser = new DDLStatementParser(tokens); // 语法分析
        DDLStatementParser.DropTableContext dropTableContext = parser.dropTable();//获取某一个规则树，这里获取的是最外层的规则，也可以通过sql()获取sql规则树......

        System.out.println(" \n" + dropTableContext.toStringTree(parser)); //打印规则数
    }
}
