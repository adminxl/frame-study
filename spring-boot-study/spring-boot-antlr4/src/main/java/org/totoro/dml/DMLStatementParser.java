package org.totoro.dml;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/DMLStatement.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DMLStatementParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		AND_=1, OR_=2, NOT_=3, TILDE_=4, VERTICAL_BAR_=5, AMPERSAND_=6, SIGNED_LEFT_SHIFT_=7, 
		SIGNED_RIGHT_SHIFT_=8, CARET_=9, MOD_=10, COLON_=11, PLUS_=12, MINUS_=13, 
		ASTERISK_=14, SLASH_=15, BACKSLASH_=16, DOT_=17, DOT_ASTERISK_=18, SAFE_EQ_=19, 
		DEQ_=20, EQ_=21, NEQ_=22, GT_=23, GTE_=24, LT_=25, LTE_=26, POUND_=27, 
		LP_=28, RP_=29, LBE_=30, RBE_=31, LBT_=32, RBT_=33, COMMA_=34, DQ_=35, 
		SQ_=36, BQ_=37, QUESTION_=38, AT_=39, SEMI_=40, WS=41, SELECT=42, INSERT=43, 
		UPDATE=44, DELETE=45, CREATE=46, ALTER=47, DROP=48, TRUNCATE=49, SCHEMA=50, 
		GRANT=51, REVOKE=52, ADD=53, SET=54, TABLE=55, COLUMN=56, INDEX=57, CONSTRAINT=58, 
		PRIMARY=59, UNIQUE=60, FOREIGN=61, KEY=62, POSITION=63, PRECISION=64, 
		FUNCTION=65, TRIGGER=66, PROCEDURE=67, VIEW=68, INTO=69, VALUES=70, WITH=71, 
		UNION=72, DISTINCT=73, CASE=74, WHEN=75, CAST=76, TRIM=77, SUBSTRING=78, 
		FROM=79, NATURAL=80, JOIN=81, FULL=82, INNER=83, OUTER=84, LEFT=85, RIGHT=86, 
		CROSS=87, USING=88, WHERE=89, AS=90, ON=91, IF=92, ELSE=93, THEN=94, FOR=95, 
		TO=96, AND=97, OR=98, IS=99, NOT=100, NULL=101, TRUE=102, FALSE=103, EXISTS=104, 
		BETWEEN=105, IN=106, ALL=107, ANY=108, LIKE=109, ORDER=110, GROUP=111, 
		BY=112, ASC=113, DESC=114, HAVING=115, LIMIT=116, OFFSET=117, BEGIN=118, 
		COMMIT=119, ROLLBACK=120, SAVEPOINT=121, BOOLEAN=122, DOUBLE=123, CHAR=124, 
		CHARACTER=125, ARRAY=126, INTERVAL=127, DATE=128, TIME=129, TIMESTAMP=130, 
		LOCALTIME=131, LOCALTIMESTAMP=132, YEAR=133, QUARTER=134, MONTH=135, WEEK=136, 
		DAY=137, HOUR=138, MINUTE=139, SECOND=140, MICROSECOND=141, MAX=142, MIN=143, 
		SUM=144, COUNT=145, AVG=146, DEFAULT=147, CURRENT=148, ENABLE=149, DISABLE=150, 
		CALL=151, INSTANCE=152, PRESERVE=153, DO=154, DEFINER=155, CURRENT_USER=156, 
		SQL=157, CASCADED=158, LOCAL=159, CLOSE=160, OPEN=161, NEXT=162, NAME=163, 
		COLLATION=164, NAMES=165, INTEGER=166, REAL=167, DECIMAL=168, TYPE=169, 
		INT=170, SMALLINT=171, TINYINT=172, MEDIUMINT=173, BIGINT=174, NUMERIC=175, 
		FLOAT=176, DATETIME=177, FOR_GENERATOR=178, USE=179, DESCRIBE=180, SHOW=181, 
		DATABASES=182, DATABASE=183, SCHEMAS=184, TABLES=185, TABLESPACE=186, 
		COLUMNS=187, FIELDS=188, INDEXES=189, STATUS=190, REPLACE=191, MODIFY=192, 
		DISTINCTROW=193, VALUE=194, DUPLICATE=195, FIRST=196, LAST=197, AFTER=198, 
		OJ=199, WINDOW=200, MOD=201, DIV=202, XOR=203, REGEXP=204, RLIKE=205, 
		ACCOUNT=206, USER=207, ROLE=208, START=209, TRANSACTION=210, ROW=211, 
		ROWS=212, WITHOUT=213, BINARY=214, ESCAPE=215, GENERATED=216, PARTITION=217, 
		SUBPARTITION=218, STORAGE=219, STORED=220, SUPER=221, SUBSTR=222, TEMPORARY=223, 
		THAN=224, TRAILING=225, UNBOUNDED=226, UNLOCK=227, UNSIGNED=228, SIGNED=229, 
		UPGRADE=230, USAGE=231, VALIDATION=232, VIRTUAL=233, ROLLUP=234, SOUNDS=235, 
		UNKNOWN=236, OFF=237, ALWAYS=238, CASCADE=239, CHECK=240, COMMITTED=241, 
		LEVEL=242, NO=243, OPTION=244, PASSWORD=245, PRIVILEGES=246, READ=247, 
		WRITE=248, REFERENCES=249, ACTION=250, ALGORITHM=251, ANALYZE=252, AUTOCOMMIT=253, 
		MAXVALUE=254, BOTH=255, BTREE=256, CHAIN=257, CHANGE=258, CHARSET=259, 
		CHECKSUM=260, CIPHER=261, CLIENT=262, COALESCE=263, COLLATE=264, COMMENT=265, 
		COMPACT=266, COMPRESSED=267, COMPRESSION=268, CONNECTION=269, CONSISTENT=270, 
		CONVERT=271, COPY=272, DATA=273, DELAYED=274, DIRECTORY=275, DISCARD=276, 
		DISK=277, DYNAMIC=278, ENCRYPTION=279, END=280, ENGINE=281, EVENT=282, 
		EXCEPT=283, EXCHANGE=284, EXCLUSIVE=285, EXECUTE=286, EXTRACT=287, FILE=288, 
		FIXED=289, FOLLOWING=290, FORCE=291, FULLTEXT=292, GLOBAL=293, HASH=294, 
		IDENTIFIED=295, IGNORE=296, IMPORT_=297, INPLACE=298, KEYS=299, LEADING=300, 
		LESS=301, LINEAR=302, LOCK=303, MATCH=304, MEMORY=305, NONE=306, NOW=307, 
		OFFLINE=308, ONLINE=309, OPTIMIZE=310, OVER=311, PARSER=312, PARTIAL=313, 
		PARTITIONING=314, PERSIST=315, PRECEDING=316, PROCESS=317, PROXY=318, 
		QUICK=319, RANGE=320, REBUILD=321, RECURSIVE=322, REDUNDANT=323, RELEASE=324, 
		RELOAD=325, REMOVE=326, RENAME=327, REORGANIZE=328, REPAIR=329, REPLICATION=330, 
		REQUIRE=331, RESTRICT=332, REVERSE=333, ROUTINE=334, SEPARATOR=335, SESSION=336, 
		SHARED=337, SHUTDOWN=338, SIMPLE=339, SLAVE=340, SPATIAL=341, ZEROFILL=342, 
		VISIBLE=343, INVISIBLE=344, INSTANT=345, ENFORCED=346, AGAINST=347, LANGUAGE=348, 
		MODE=349, QUERY=350, EXTENDED=351, EXPANSION=352, VARIANCE=353, MAX_ROWS=354, 
		MIN_ROWS=355, HIGH_PRIORITY=356, LOW_PRIORITY=357, SQL_BIG_RESULT=358, 
		SQL_BUFFER_RESULT=359, SQL_CACHE=360, SQL_CALC_FOUND_ROWS=361, SQL_NO_CACHE=362, 
		SQL_SMALL_RESULT=363, STATS_AUTO_RECALC=364, STATS_PERSISTENT=365, STATS_SAMPLE_PAGES=366, 
		ROLE_ADMIN=367, ROW_FORMAT=368, SET_USER_ID=369, REPLICATION_SLAVE_ADMIN=370, 
		GROUP_REPLICATION_ADMIN=371, STRAIGHT_JOIN=372, WEIGHT_STRING=373, COLUMN_FORMAT=374, 
		CONNECTION_ADMIN=375, FIREWALL_ADMIN=376, FIREWALL_USER=377, INSERT_METHOD=378, 
		KEY_BLOCK_SIZE=379, PACK_KEYS=380, PERSIST_ONLY=381, BIT_AND=382, BIT_OR=383, 
		BIT_XOR=384, GROUP_CONCAT=385, JSON_ARRAYAGG=386, JSON_OBJECTAGG=387, 
		STD=388, STDDEV=389, STDDEV_POP=390, STDDEV_SAMP=391, VAR_POP=392, VAR_SAMP=393, 
		AUDIT_ADMIN=394, AUTO_INCREMENT=395, AVG_ROW_LENGTH=396, BINLOG_ADMIN=397, 
		DELAY_KEY_WRITE=398, ENCRYPTION_KEY_ADMIN=399, SYSTEM_VARIABLES_ADMIN=400, 
		VERSION_TOKEN_ADMIN=401, CURRENT_TIMESTAMP=402, YEAR_MONTH=403, DAY_HOUR=404, 
		DAY_MINUTE=405, DAY_SECOND=406, DAY_MICROSECOND=407, HOUR_MINUTE=408, 
		HOUR_SECOND=409, HOUR_MICROSECOND=410, MINUTE_SECOND=411, MINUTE_MICROSECOND=412, 
		SECOND_MICROSECOND=413, UL_BINARY=414, ROTATE=415, MASTER=416, BINLOG=417, 
		ERROR=418, SCHEDULE=419, COMPLETION=420, EVERY=421, STARTS=422, ENDS=423, 
		HOST=424, SOCKET=425, PORT=426, SERVER=427, WRAPPER=428, OPTIONS=429, 
		OWNER=430, DETERMINISTIC=431, RETURNS=432, CONTAINS=433, READS=434, MODIFIES=435, 
		SECURITY=436, INVOKER=437, OUT=438, INOUT=439, TEMPTABLE=440, MERGE=441, 
		UNDEFINED=442, DATAFILE=443, FILE_BLOCK_SIZE=444, EXTENT_SIZE=445, INITIAL_SIZE=446, 
		AUTOEXTEND_SIZE=447, MAX_SIZE=448, NODEGROUP=449, WAIT=450, LOGFILE=451, 
		UNDOFILE=452, UNDO_BUFFER_SIZE=453, REDO_BUFFER_SIZE=454, HANDLER=455, 
		PREV=456, ORGANIZATION=457, DEFINITION=458, DESCRIPTION=459, REFERENCE=460, 
		FOLLOWS=461, PRECEDES=462, IMPORT=463, LOAD=464, CONCURRENT=465, INFILE=466, 
		LINES=467, STARTING=468, TERMINATED=469, OPTIONALLY=470, ENCLOSED=471, 
		ESCAPED=472, XML=473, UNDO=474, DUMPFILE=475, OUTFILE=476, SHARE=477, 
		LOGS=478, EVENTS=479, BEFORE=480, EACH=481, MUTEX=482, ENGINES=483, ERRORS=484, 
		CODE=485, GRANTS=486, PLUGINS=487, PROCESSLIST=488, BLOCK=489, IO=490, 
		CONTEXT=491, SWITCHES=492, CPU=493, IPC=494, PAGE=495, FAULTS=496, SOURCE=497, 
		SWAPS=498, PROFILE=499, PROFILES=500, RELAYLOG=501, CHANNEL=502, VARIABLES=503, 
		WARNINGS=504, SSL=505, CLONE=506, AGGREGATE=507, STRING=508, SONAME=509, 
		INSTALL=510, COMPONENT=511, PLUGIN=512, UNINSTALL=513, NO_WRITE_TO_BINLOG=514, 
		HISTOGRAM=515, BUCKETS=516, FAST=517, MEDIUM=518, USE_FRM=519, RESOURCE=520, 
		VCPU=521, THREAD_PRIORITY=522, SYSTEM=523, EXPIRE=524, NEVER=525, HISTORY=526, 
		OPTIONAL=527, REUSE=528, MAX_QUERIES_PER_HOUR=529, MAX_UPDATES_PER_HOUR=530, 
		MAX_CONNECTIONS_PER_HOUR=531, MAX_USER_CONNECTIONS=532, RETAIN=533, RANDOM=534, 
		OLD=535, X509=536, ISSUER=537, SUBJECT=538, CACHE=539, GENERAL=540, OPTIMIZER_COSTS=541, 
		SLOW=542, USER_RESOURCES=543, EXPORT=544, RELAY=545, HOSTS=546, KILL=547, 
		FLUSH=548, RESET=549, RESTART=550, UNIX_TIMESTAMP=551, LOWER=552, UPPER=553, 
		ADDDATE=554, ADDTIME=555, DATE_ADD=556, DATE_SUB=557, DATEDIFF=558, DATE_FORMAT=559, 
		DAYNAME=560, DAYOFMONTH=561, DAYOFWEEK=562, DAYOFYEAR=563, STR_TO_DATE=564, 
		TIMEDIFF=565, TIMESTAMPADD=566, TIMESTAMPDIFF=567, TIME_FORMAT=568, TIME_TO_SEC=569, 
		AES_DECRYPT=570, AES_ENCRYPT=571, FROM_BASE64=572, TO_BASE64=573, GEOMCOLLECTION=574, 
		GEOMETRYCOLLECTION=575, LINESTRING=576, MULTILINESTRING=577, MULTIPOINT=578, 
		MULTIPOLYGON=579, POINT=580, POLYGON=581, ST_AREA=582, ST_ASBINARY=583, 
		ST_ASGEOJSON=584, ST_ASTEXT=585, ST_ASWKB=586, ST_ASWKT=587, ST_BUFFER=588, 
		ST_BUFFER_STRATEGY=589, ST_CENTROID=590, ST_CONTAINS=591, ST_CONVEXHULL=592, 
		ST_CROSSES=593, ST_DIFFERENCE=594, ST_DIMENSION=595, ST_DISJOINT=596, 
		ST_DISTANCE=597, ST_DISTANCE_SPHERE=598, ST_ENDPOINT=599, ST_ENVELOPE=600, 
		ST_EQUALS=601, ST_EXTERIORRING=602, ST_GEOHASH=603, ST_GEOMCOLLFROMTEXT=604, 
		ST_GEOMCOLLFROMTXT=605, ST_GEOMCOLLFROMWKB=606, ST_GEOMETRYCOLLECTIONFROMTEXT=607, 
		ST_GEOMETRYCOLLECTIONFROMWKB=608, ST_GEOMETRYFROMTEXT=609, ST_GEOMETRYFROMWKB=610, 
		ST_GEOMETRYN=611, ST_GEOMETRYTYPE=612, ST_GEOMFROMGEOJSON=613, ST_GEOMFROMTEXT=614, 
		ST_GEOMFROMWKB=615, ST_INTERIORRINGN=616, ST_INTERSECTION=617, ST_INTERSECTS=618, 
		ST_ISCLOSED=619, ST_ISEMPTY=620, ST_ISSIMPLE=621, ST_ISVALID=622, ST_LATFROMGEOHASH=623, 
		ST_LATITUDE=624, ST_LENGTH=625, ST_LINEFROMTEXT=626, ST_LINEFROMWKB=627, 
		ST_LINESTRINGFROMTEXT=628, ST_LINESTRINGFROMWKB=629, ST_LONGFROMGEOHASH=630, 
		ST_LONGITUDE=631, ST_MAKEENVELOPE=632, ST_MLINEFROMTEXT=633, ST_MLINEFROMWKB=634, 
		ST_MULTILINESTRINGFROMTEXT=635, ST_MULTILINESTRINGFROMWKB=636, ST_MPOINTFROMTEXT=637, 
		ST_MPOINTFROMWKB=638, ST_MULTIPOINTFROMTEXT=639, ST_MULTIPOINTFROMWKB=640, 
		ST_MPOLYFROMTEXT=641, ST_MPOLYFROMWKB=642, ST_MULTIPOLYGONFROMTEXT=643, 
		ST_MULTIPOLYGONFROMWKB=644, ST_NUMGEOMETRIES=645, ST_NUMINTERIORRING=646, 
		ST_NUMINTERIORRINGS=647, ST_NUMPOINTS=648, ST_OVERLAPS=649, ST_POINTFROMGEOHASH=650, 
		ST_POINTFROMTEXT=651, ST_POINTFROMWKB=652, ST_POINTN=653, ST_POLYFROMTEXT=654, 
		ST_POLYFROMWKB=655, ST_POLYGONFROMTEXT=656, ST_POLYGONFROMWKB=657, ST_SIMPLIFY=658, 
		ST_SRID=659, ST_STARTPOINT=660, ST_SWAPXY=661, ST_SYMDIFFERENCE=662, ST_TOUCHES=663, 
		ST_TRANSFORM=664, ST_UNION=665, ST_VALIDATE=666, ST_WITHIN=667, ST_X=668, 
		ST_Y=669, BIT=670, BOOL=671, DEC=672, VARCHAR=673, VARBINARY=674, TINYBLOB=675, 
		TINYTEXT=676, BLOB=677, TEXT=678, MEDIUMBLOB=679, MEDIUMTEXT=680, LONGBLOB=681, 
		LONGTEXT=682, ENUM=683, GEOMETRY=684, JSON=685, IO_THREAD=686, SQL_THREAD=687, 
		SQL_BEFORE_GTIDS=688, SQL_AFTER_GTIDS=689, MASTER_LOG_FILE=690, MASTER_LOG_POS=691, 
		RELAY_LOG_FILE=692, RELAY_LOG_POS=693, SQL_AFTER_MTS_GAPS=694, UNTIL=695, 
		DEFAULT_AUTH=696, PLUGIN_DIR=697, STOP=698, IDENTIFIER_=699, STRING_=700, 
		NUMBER_=701, HEX_DIGIT_=702, BIT_NUM_=703, INNODB_=704, TLS_=705, Y_N_=706, 
		NOT_SUPPORT_=707, FILESIZE_LITERAL=708;
	public static final int
		RULE_insert = 0, RULE_insertSpecification_ = 1, RULE_insertValuesClause = 2, 
		RULE_insertSelectClause = 3, RULE_onDuplicateKeyClause = 4, RULE_replace = 5, 
		RULE_replaceSpecification_ = 6, RULE_update = 7, RULE_updateSpecification_ = 8, 
		RULE_assignment = 9, RULE_setAssignmentsClause = 10, RULE_assignmentValues = 11, 
		RULE_assignmentValue = 12, RULE_blobValue = 13, RULE_delete = 14, RULE_deleteSpecification_ = 15, 
		RULE_singleTableClause = 16, RULE_multipleTablesClause = 17, RULE_multipleTableNames = 18, 
		RULE_select = 19, RULE_call = 20, RULE_doStatement = 21, RULE_handlerStatement = 22, 
		RULE_handlerOpenStatement = 23, RULE_handlerReadIndexStatement = 24, RULE_handlerReadStatement = 25, 
		RULE_handlerCloseStatement = 26, RULE_importStatement = 27, RULE_loadDataStatement = 28, 
		RULE_loadXmlStatement = 29, RULE_withClause_ = 30, RULE_cteClause_ = 31, 
		RULE_unionClause = 32, RULE_selectClause = 33, RULE_selectSpecification = 34, 
		RULE_duplicateSpecification = 35, RULE_projections = 36, RULE_projection = 37, 
		RULE_alias = 38, RULE_unqualifiedShorthand = 39, RULE_qualifiedShorthand = 40, 
		RULE_fromClause = 41, RULE_tableReferences = 42, RULE_escapedTableReference = 43, 
		RULE_tableReference = 44, RULE_tableFactor = 45, RULE_partitionNames_ = 46, 
		RULE_indexHintList_ = 47, RULE_indexHint_ = 48, RULE_joinedTable = 49, 
		RULE_joinSpecification = 50, RULE_whereClause = 51, RULE_groupByClause = 52, 
		RULE_havingClause = 53, RULE_limitClause = 54, RULE_limitRowCount = 55, 
		RULE_limitOffset = 56, RULE_windowClause_ = 57, RULE_windowItem_ = 58, 
		RULE_subquery = 59, RULE_selectLinesInto_ = 60, RULE_selectFieldsInto_ = 61, 
		RULE_selectIntoExpression_ = 62, RULE_lockClause = 63, RULE_parameterMarker = 64, 
		RULE_literals = 65, RULE_stringLiterals = 66, RULE_numberLiterals = 67, 
		RULE_dateTimeLiterals = 68, RULE_hexadecimalLiterals = 69, RULE_bitValueLiterals = 70, 
		RULE_booleanLiterals = 71, RULE_nullValueLiterals = 72, RULE_characterSetName_ = 73, 
		RULE_collationName_ = 74, RULE_identifier = 75, RULE_unreservedWord = 76, 
		RULE_variable = 77, RULE_schemaName = 78, RULE_tableName = 79, RULE_columnName = 80, 
		RULE_indexName = 81, RULE_userName = 82, RULE_eventName = 83, RULE_serverName = 84, 
		RULE_wrapperName = 85, RULE_functionName = 86, RULE_viewName = 87, RULE_owner = 88, 
		RULE_name = 89, RULE_tableNames = 90, RULE_columnNames = 91, RULE_groupName = 92, 
		RULE_shardLibraryName = 93, RULE_componentName = 94, RULE_pluginName = 95, 
		RULE_hostName = 96, RULE_port = 97, RULE_cloneInstance = 98, RULE_cloneDir = 99, 
		RULE_channelName = 100, RULE_logName = 101, RULE_roleName = 102, RULE_engineName = 103, 
		RULE_triggerName = 104, RULE_triggerTime = 105, RULE_userOrRole = 106, 
		RULE_partitionName = 107, RULE_triggerEvent = 108, RULE_triggerOrder = 109, 
		RULE_expr = 110, RULE_logicalOperator = 111, RULE_notOperator_ = 112, 
		RULE_booleanPrimary = 113, RULE_comparisonOperator = 114, RULE_predicate = 115, 
		RULE_bitExpr = 116, RULE_simpleExpr = 117, RULE_functionCall = 118, RULE_aggregationFunction = 119, 
		RULE_aggregationFunctionName = 120, RULE_distinct = 121, RULE_overClause_ = 122, 
		RULE_windowSpecification_ = 123, RULE_partitionClause_ = 124, RULE_frameClause_ = 125, 
		RULE_frameStart_ = 126, RULE_frameEnd_ = 127, RULE_frameBetween_ = 128, 
		RULE_specialFunction = 129, RULE_groupConcatFunction = 130, RULE_windowFunction = 131, 
		RULE_castFunction = 132, RULE_convertFunction = 133, RULE_positionFunction = 134, 
		RULE_substringFunction = 135, RULE_extractFunction = 136, RULE_charFunction = 137, 
		RULE_trimFunction_ = 138, RULE_valuesFunction_ = 139, RULE_weightStringFunction = 140, 
		RULE_levelClause_ = 141, RULE_levelInWeightListElement_ = 142, RULE_regularFunction = 143, 
		RULE_regularFunctionName_ = 144, RULE_matchExpression_ = 145, RULE_matchSearchModifier_ = 146, 
		RULE_caseExpression = 147, RULE_caseWhen_ = 148, RULE_caseElse_ = 149, 
		RULE_intervalExpression = 150, RULE_intervalUnit_ = 151, RULE_orderByClause = 152, 
		RULE_orderByItem = 153, RULE_dataType = 154, RULE_dataTypeName = 155, 
		RULE_dataTypeLength = 156, RULE_collectionOptions = 157, RULE_characterSet_ = 158, 
		RULE_collateClause_ = 159, RULE_ignoredIdentifier_ = 160, RULE_ignoredIdentifiers_ = 161;
	private static String[] makeRuleNames() {
		return new String[] {
			"insert", "insertSpecification_", "insertValuesClause", "insertSelectClause", 
			"onDuplicateKeyClause", "replace", "replaceSpecification_", "update", 
			"updateSpecification_", "assignment", "setAssignmentsClause", "assignmentValues", 
			"assignmentValue", "blobValue", "delete", "deleteSpecification_", "singleTableClause", 
			"multipleTablesClause", "multipleTableNames", "select", "call", "doStatement", 
			"handlerStatement", "handlerOpenStatement", "handlerReadIndexStatement", 
			"handlerReadStatement", "handlerCloseStatement", "importStatement", "loadDataStatement", 
			"loadXmlStatement", "withClause_", "cteClause_", "unionClause", "selectClause", 
			"selectSpecification", "duplicateSpecification", "projections", "projection", 
			"alias", "unqualifiedShorthand", "qualifiedShorthand", "fromClause", 
			"tableReferences", "escapedTableReference", "tableReference", "tableFactor", 
			"partitionNames_", "indexHintList_", "indexHint_", "joinedTable", "joinSpecification", 
			"whereClause", "groupByClause", "havingClause", "limitClause", "limitRowCount", 
			"limitOffset", "windowClause_", "windowItem_", "subquery", "selectLinesInto_", 
			"selectFieldsInto_", "selectIntoExpression_", "lockClause", "parameterMarker", 
			"literals", "stringLiterals", "numberLiterals", "dateTimeLiterals", "hexadecimalLiterals", 
			"bitValueLiterals", "booleanLiterals", "nullValueLiterals", "characterSetName_", 
			"collationName_", "identifier", "unreservedWord", "variable", "schemaName", 
			"tableName", "columnName", "indexName", "userName", "eventName", "serverName", 
			"wrapperName", "functionName", "viewName", "owner", "name", "tableNames", 
			"columnNames", "groupName", "shardLibraryName", "componentName", "pluginName", 
			"hostName", "port", "cloneInstance", "cloneDir", "channelName", "logName", 
			"roleName", "engineName", "triggerName", "triggerTime", "userOrRole", 
			"partitionName", "triggerEvent", "triggerOrder", "expr", "logicalOperator", 
			"notOperator_", "booleanPrimary", "comparisonOperator", "predicate", 
			"bitExpr", "simpleExpr", "functionCall", "aggregationFunction", "aggregationFunctionName", 
			"distinct", "overClause_", "windowSpecification_", "partitionClause_", 
			"frameClause_", "frameStart_", "frameEnd_", "frameBetween_", "specialFunction", 
			"groupConcatFunction", "windowFunction", "castFunction", "convertFunction", 
			"positionFunction", "substringFunction", "extractFunction", "charFunction", 
			"trimFunction_", "valuesFunction_", "weightStringFunction", "levelClause_", 
			"levelInWeightListElement_", "regularFunction", "regularFunctionName_", 
			"matchExpression_", "matchSearchModifier_", "caseExpression", "caseWhen_", 
			"caseElse_", "intervalExpression", "intervalUnit_", "orderByClause", 
			"orderByItem", "dataType", "dataTypeName", "dataTypeLength", "collectionOptions", 
			"characterSet_", "collateClause_", "ignoredIdentifier_", "ignoredIdentifiers_"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'&&'", "'||'", "'!'", "'~'", "'|'", "'&'", "'<<'", "'>>'", "'^'", 
			"'%'", "':'", "'+'", "'-'", "'*'", "'/'", "'\\'", "'.'", "'.*'", "'<=>'", 
			"'=='", "'='", null, "'>'", "'>='", "'<'", "'<='", "'#'", "'('", "')'", 
			"'{'", "'}'", "'['", "']'", "','", "'\"'", "'''", "'`'", "'?'", "'@'", 
			"';'", null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "'DO NOT MATCH ANY THING, JUST FOR GENERATOR'", 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, "'INNODB'", "'TLS'", 
			null, "'not support'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "AND_", "OR_", "NOT_", "TILDE_", "VERTICAL_BAR_", "AMPERSAND_", 
			"SIGNED_LEFT_SHIFT_", "SIGNED_RIGHT_SHIFT_", "CARET_", "MOD_", "COLON_", 
			"PLUS_", "MINUS_", "ASTERISK_", "SLASH_", "BACKSLASH_", "DOT_", "DOT_ASTERISK_", 
			"SAFE_EQ_", "DEQ_", "EQ_", "NEQ_", "GT_", "GTE_", "LT_", "LTE_", "POUND_", 
			"LP_", "RP_", "LBE_", "RBE_", "LBT_", "RBT_", "COMMA_", "DQ_", "SQ_", 
			"BQ_", "QUESTION_", "AT_", "SEMI_", "WS", "SELECT", "INSERT", "UPDATE", 
			"DELETE", "CREATE", "ALTER", "DROP", "TRUNCATE", "SCHEMA", "GRANT", "REVOKE", 
			"ADD", "SET", "TABLE", "COLUMN", "INDEX", "CONSTRAINT", "PRIMARY", "UNIQUE", 
			"FOREIGN", "KEY", "POSITION", "PRECISION", "FUNCTION", "TRIGGER", "PROCEDURE", 
			"VIEW", "INTO", "VALUES", "WITH", "UNION", "DISTINCT", "CASE", "WHEN", 
			"CAST", "TRIM", "SUBSTRING", "FROM", "NATURAL", "JOIN", "FULL", "INNER", 
			"OUTER", "LEFT", "RIGHT", "CROSS", "USING", "WHERE", "AS", "ON", "IF", 
			"ELSE", "THEN", "FOR", "TO", "AND", "OR", "IS", "NOT", "NULL", "TRUE", 
			"FALSE", "EXISTS", "BETWEEN", "IN", "ALL", "ANY", "LIKE", "ORDER", "GROUP", 
			"BY", "ASC", "DESC", "HAVING", "LIMIT", "OFFSET", "BEGIN", "COMMIT", 
			"ROLLBACK", "SAVEPOINT", "BOOLEAN", "DOUBLE", "CHAR", "CHARACTER", "ARRAY", 
			"INTERVAL", "DATE", "TIME", "TIMESTAMP", "LOCALTIME", "LOCALTIMESTAMP", 
			"YEAR", "QUARTER", "MONTH", "WEEK", "DAY", "HOUR", "MINUTE", "SECOND", 
			"MICROSECOND", "MAX", "MIN", "SUM", "COUNT", "AVG", "DEFAULT", "CURRENT", 
			"ENABLE", "DISABLE", "CALL", "INSTANCE", "PRESERVE", "DO", "DEFINER", 
			"CURRENT_USER", "SQL", "CASCADED", "LOCAL", "CLOSE", "OPEN", "NEXT", 
			"NAME", "COLLATION", "NAMES", "INTEGER", "REAL", "DECIMAL", "TYPE", "INT", 
			"SMALLINT", "TINYINT", "MEDIUMINT", "BIGINT", "NUMERIC", "FLOAT", "DATETIME", 
			"FOR_GENERATOR", "USE", "DESCRIBE", "SHOW", "DATABASES", "DATABASE", 
			"SCHEMAS", "TABLES", "TABLESPACE", "COLUMNS", "FIELDS", "INDEXES", "STATUS", 
			"REPLACE", "MODIFY", "DISTINCTROW", "VALUE", "DUPLICATE", "FIRST", "LAST", 
			"AFTER", "OJ", "WINDOW", "MOD", "DIV", "XOR", "REGEXP", "RLIKE", "ACCOUNT", 
			"USER", "ROLE", "START", "TRANSACTION", "ROW", "ROWS", "WITHOUT", "BINARY", 
			"ESCAPE", "GENERATED", "PARTITION", "SUBPARTITION", "STORAGE", "STORED", 
			"SUPER", "SUBSTR", "TEMPORARY", "THAN", "TRAILING", "UNBOUNDED", "UNLOCK", 
			"UNSIGNED", "SIGNED", "UPGRADE", "USAGE", "VALIDATION", "VIRTUAL", "ROLLUP", 
			"SOUNDS", "UNKNOWN", "OFF", "ALWAYS", "CASCADE", "CHECK", "COMMITTED", 
			"LEVEL", "NO", "OPTION", "PASSWORD", "PRIVILEGES", "READ", "WRITE", "REFERENCES", 
			"ACTION", "ALGORITHM", "ANALYZE", "AUTOCOMMIT", "MAXVALUE", "BOTH", "BTREE", 
			"CHAIN", "CHANGE", "CHARSET", "CHECKSUM", "CIPHER", "CLIENT", "COALESCE", 
			"COLLATE", "COMMENT", "COMPACT", "COMPRESSED", "COMPRESSION", "CONNECTION", 
			"CONSISTENT", "CONVERT", "COPY", "DATA", "DELAYED", "DIRECTORY", "DISCARD", 
			"DISK", "DYNAMIC", "ENCRYPTION", "END", "ENGINE", "EVENT", "EXCEPT", 
			"EXCHANGE", "EXCLUSIVE", "EXECUTE", "EXTRACT", "FILE", "FIXED", "FOLLOWING", 
			"FORCE", "FULLTEXT", "GLOBAL", "HASH", "IDENTIFIED", "IGNORE", "IMPORT_", 
			"INPLACE", "KEYS", "LEADING", "LESS", "LINEAR", "LOCK", "MATCH", "MEMORY", 
			"NONE", "NOW", "OFFLINE", "ONLINE", "OPTIMIZE", "OVER", "PARSER", "PARTIAL", 
			"PARTITIONING", "PERSIST", "PRECEDING", "PROCESS", "PROXY", "QUICK", 
			"RANGE", "REBUILD", "RECURSIVE", "REDUNDANT", "RELEASE", "RELOAD", "REMOVE", 
			"RENAME", "REORGANIZE", "REPAIR", "REPLICATION", "REQUIRE", "RESTRICT", 
			"REVERSE", "ROUTINE", "SEPARATOR", "SESSION", "SHARED", "SHUTDOWN", "SIMPLE", 
			"SLAVE", "SPATIAL", "ZEROFILL", "VISIBLE", "INVISIBLE", "INSTANT", "ENFORCED", 
			"AGAINST", "LANGUAGE", "MODE", "QUERY", "EXTENDED", "EXPANSION", "VARIANCE", 
			"MAX_ROWS", "MIN_ROWS", "HIGH_PRIORITY", "LOW_PRIORITY", "SQL_BIG_RESULT", 
			"SQL_BUFFER_RESULT", "SQL_CACHE", "SQL_CALC_FOUND_ROWS", "SQL_NO_CACHE", 
			"SQL_SMALL_RESULT", "STATS_AUTO_RECALC", "STATS_PERSISTENT", "STATS_SAMPLE_PAGES", 
			"ROLE_ADMIN", "ROW_FORMAT", "SET_USER_ID", "REPLICATION_SLAVE_ADMIN", 
			"GROUP_REPLICATION_ADMIN", "STRAIGHT_JOIN", "WEIGHT_STRING", "COLUMN_FORMAT", 
			"CONNECTION_ADMIN", "FIREWALL_ADMIN", "FIREWALL_USER", "INSERT_METHOD", 
			"KEY_BLOCK_SIZE", "PACK_KEYS", "PERSIST_ONLY", "BIT_AND", "BIT_OR", "BIT_XOR", 
			"GROUP_CONCAT", "JSON_ARRAYAGG", "JSON_OBJECTAGG", "STD", "STDDEV", "STDDEV_POP", 
			"STDDEV_SAMP", "VAR_POP", "VAR_SAMP", "AUDIT_ADMIN", "AUTO_INCREMENT", 
			"AVG_ROW_LENGTH", "BINLOG_ADMIN", "DELAY_KEY_WRITE", "ENCRYPTION_KEY_ADMIN", 
			"SYSTEM_VARIABLES_ADMIN", "VERSION_TOKEN_ADMIN", "CURRENT_TIMESTAMP", 
			"YEAR_MONTH", "DAY_HOUR", "DAY_MINUTE", "DAY_SECOND", "DAY_MICROSECOND", 
			"HOUR_MINUTE", "HOUR_SECOND", "HOUR_MICROSECOND", "MINUTE_SECOND", "MINUTE_MICROSECOND", 
			"SECOND_MICROSECOND", "UL_BINARY", "ROTATE", "MASTER", "BINLOG", "ERROR", 
			"SCHEDULE", "COMPLETION", "EVERY", "STARTS", "ENDS", "HOST", "SOCKET", 
			"PORT", "SERVER", "WRAPPER", "OPTIONS", "OWNER", "DETERMINISTIC", "RETURNS", 
			"CONTAINS", "READS", "MODIFIES", "SECURITY", "INVOKER", "OUT", "INOUT", 
			"TEMPTABLE", "MERGE", "UNDEFINED", "DATAFILE", "FILE_BLOCK_SIZE", "EXTENT_SIZE", 
			"INITIAL_SIZE", "AUTOEXTEND_SIZE", "MAX_SIZE", "NODEGROUP", "WAIT", "LOGFILE", 
			"UNDOFILE", "UNDO_BUFFER_SIZE", "REDO_BUFFER_SIZE", "HANDLER", "PREV", 
			"ORGANIZATION", "DEFINITION", "DESCRIPTION", "REFERENCE", "FOLLOWS", 
			"PRECEDES", "IMPORT", "LOAD", "CONCURRENT", "INFILE", "LINES", "STARTING", 
			"TERMINATED", "OPTIONALLY", "ENCLOSED", "ESCAPED", "XML", "UNDO", "DUMPFILE", 
			"OUTFILE", "SHARE", "LOGS", "EVENTS", "BEFORE", "EACH", "MUTEX", "ENGINES", 
			"ERRORS", "CODE", "GRANTS", "PLUGINS", "PROCESSLIST", "BLOCK", "IO", 
			"CONTEXT", "SWITCHES", "CPU", "IPC", "PAGE", "FAULTS", "SOURCE", "SWAPS", 
			"PROFILE", "PROFILES", "RELAYLOG", "CHANNEL", "VARIABLES", "WARNINGS", 
			"SSL", "CLONE", "AGGREGATE", "STRING", "SONAME", "INSTALL", "COMPONENT", 
			"PLUGIN", "UNINSTALL", "NO_WRITE_TO_BINLOG", "HISTOGRAM", "BUCKETS", 
			"FAST", "MEDIUM", "USE_FRM", "RESOURCE", "VCPU", "THREAD_PRIORITY", "SYSTEM", 
			"EXPIRE", "NEVER", "HISTORY", "OPTIONAL", "REUSE", "MAX_QUERIES_PER_HOUR", 
			"MAX_UPDATES_PER_HOUR", "MAX_CONNECTIONS_PER_HOUR", "MAX_USER_CONNECTIONS", 
			"RETAIN", "RANDOM", "OLD", "X509", "ISSUER", "SUBJECT", "CACHE", "GENERAL", 
			"OPTIMIZER_COSTS", "SLOW", "USER_RESOURCES", "EXPORT", "RELAY", "HOSTS", 
			"KILL", "FLUSH", "RESET", "RESTART", "UNIX_TIMESTAMP", "LOWER", "UPPER", 
			"ADDDATE", "ADDTIME", "DATE_ADD", "DATE_SUB", "DATEDIFF", "DATE_FORMAT", 
			"DAYNAME", "DAYOFMONTH", "DAYOFWEEK", "DAYOFYEAR", "STR_TO_DATE", "TIMEDIFF", 
			"TIMESTAMPADD", "TIMESTAMPDIFF", "TIME_FORMAT", "TIME_TO_SEC", "AES_DECRYPT", 
			"AES_ENCRYPT", "FROM_BASE64", "TO_BASE64", "GEOMCOLLECTION", "GEOMETRYCOLLECTION", 
			"LINESTRING", "MULTILINESTRING", "MULTIPOINT", "MULTIPOLYGON", "POINT", 
			"POLYGON", "ST_AREA", "ST_ASBINARY", "ST_ASGEOJSON", "ST_ASTEXT", "ST_ASWKB", 
			"ST_ASWKT", "ST_BUFFER", "ST_BUFFER_STRATEGY", "ST_CENTROID", "ST_CONTAINS", 
			"ST_CONVEXHULL", "ST_CROSSES", "ST_DIFFERENCE", "ST_DIMENSION", "ST_DISJOINT", 
			"ST_DISTANCE", "ST_DISTANCE_SPHERE", "ST_ENDPOINT", "ST_ENVELOPE", "ST_EQUALS", 
			"ST_EXTERIORRING", "ST_GEOHASH", "ST_GEOMCOLLFROMTEXT", "ST_GEOMCOLLFROMTXT", 
			"ST_GEOMCOLLFROMWKB", "ST_GEOMETRYCOLLECTIONFROMTEXT", "ST_GEOMETRYCOLLECTIONFROMWKB", 
			"ST_GEOMETRYFROMTEXT", "ST_GEOMETRYFROMWKB", "ST_GEOMETRYN", "ST_GEOMETRYTYPE", 
			"ST_GEOMFROMGEOJSON", "ST_GEOMFROMTEXT", "ST_GEOMFROMWKB", "ST_INTERIORRINGN", 
			"ST_INTERSECTION", "ST_INTERSECTS", "ST_ISCLOSED", "ST_ISEMPTY", "ST_ISSIMPLE", 
			"ST_ISVALID", "ST_LATFROMGEOHASH", "ST_LATITUDE", "ST_LENGTH", "ST_LINEFROMTEXT", 
			"ST_LINEFROMWKB", "ST_LINESTRINGFROMTEXT", "ST_LINESTRINGFROMWKB", "ST_LONGFROMGEOHASH", 
			"ST_LONGITUDE", "ST_MAKEENVELOPE", "ST_MLINEFROMTEXT", "ST_MLINEFROMWKB", 
			"ST_MULTILINESTRINGFROMTEXT", "ST_MULTILINESTRINGFROMWKB", "ST_MPOINTFROMTEXT", 
			"ST_MPOINTFROMWKB", "ST_MULTIPOINTFROMTEXT", "ST_MULTIPOINTFROMWKB", 
			"ST_MPOLYFROMTEXT", "ST_MPOLYFROMWKB", "ST_MULTIPOLYGONFROMTEXT", "ST_MULTIPOLYGONFROMWKB", 
			"ST_NUMGEOMETRIES", "ST_NUMINTERIORRING", "ST_NUMINTERIORRINGS", "ST_NUMPOINTS", 
			"ST_OVERLAPS", "ST_POINTFROMGEOHASH", "ST_POINTFROMTEXT", "ST_POINTFROMWKB", 
			"ST_POINTN", "ST_POLYFROMTEXT", "ST_POLYFROMWKB", "ST_POLYGONFROMTEXT", 
			"ST_POLYGONFROMWKB", "ST_SIMPLIFY", "ST_SRID", "ST_STARTPOINT", "ST_SWAPXY", 
			"ST_SYMDIFFERENCE", "ST_TOUCHES", "ST_TRANSFORM", "ST_UNION", "ST_VALIDATE", 
			"ST_WITHIN", "ST_X", "ST_Y", "BIT", "BOOL", "DEC", "VARCHAR", "VARBINARY", 
			"TINYBLOB", "TINYTEXT", "BLOB", "TEXT", "MEDIUMBLOB", "MEDIUMTEXT", "LONGBLOB", 
			"LONGTEXT", "ENUM", "GEOMETRY", "JSON", "IO_THREAD", "SQL_THREAD", "SQL_BEFORE_GTIDS", 
			"SQL_AFTER_GTIDS", "MASTER_LOG_FILE", "MASTER_LOG_POS", "RELAY_LOG_FILE", 
			"RELAY_LOG_POS", "SQL_AFTER_MTS_GAPS", "UNTIL", "DEFAULT_AUTH", "PLUGIN_DIR", 
			"STOP", "IDENTIFIER_", "STRING_", "NUMBER_", "HEX_DIGIT_", "BIT_NUM_", 
			"INNODB_", "TLS_", "Y_N_", "NOT_SUPPORT_", "FILESIZE_LITERAL"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "DMLStatement.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public DMLStatementParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class InsertContext extends ParserRuleContext {
		public TerminalNode INSERT() { return getToken(DMLStatementParser.INSERT, 0); }
		public InsertSpecification_Context insertSpecification_() {
			return getRuleContext(InsertSpecification_Context.class,0);
		}
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public InsertValuesClauseContext insertValuesClause() {
			return getRuleContext(InsertValuesClauseContext.class,0);
		}
		public SetAssignmentsClauseContext setAssignmentsClause() {
			return getRuleContext(SetAssignmentsClauseContext.class,0);
		}
		public InsertSelectClauseContext insertSelectClause() {
			return getRuleContext(InsertSelectClauseContext.class,0);
		}
		public TerminalNode INTO() { return getToken(DMLStatementParser.INTO, 0); }
		public PartitionNames_Context partitionNames_() {
			return getRuleContext(PartitionNames_Context.class,0);
		}
		public OnDuplicateKeyClauseContext onDuplicateKeyClause() {
			return getRuleContext(OnDuplicateKeyClauseContext.class,0);
		}
		public InsertContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insert; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterInsert(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitInsert(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitInsert(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsertContext insert() throws RecognitionException {
		InsertContext _localctx = new InsertContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_insert);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(324);
			match(INSERT);
			setState(325);
			insertSpecification_();
			setState(327);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INTO) {
				{
				setState(326);
				match(INTO);
				}
			}

			setState(329);
			tableName();
			setState(331);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARTITION) {
				{
				setState(330);
				partitionNames_();
				}
			}

			setState(336);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(333);
				insertValuesClause();
				}
				break;
			case 2:
				{
				setState(334);
				setAssignmentsClause();
				}
				break;
			case 3:
				{
				setState(335);
				insertSelectClause();
				}
				break;
			}
			setState(339);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ON) {
				{
				setState(338);
				onDuplicateKeyClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertSpecification_Context extends ParserRuleContext {
		public TerminalNode IGNORE() { return getToken(DMLStatementParser.IGNORE, 0); }
		public TerminalNode LOW_PRIORITY() { return getToken(DMLStatementParser.LOW_PRIORITY, 0); }
		public TerminalNode DELAYED() { return getToken(DMLStatementParser.DELAYED, 0); }
		public TerminalNode HIGH_PRIORITY() { return getToken(DMLStatementParser.HIGH_PRIORITY, 0); }
		public InsertSpecification_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertSpecification_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterInsertSpecification_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitInsertSpecification_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitInsertSpecification_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsertSpecification_Context insertSpecification_() throws RecognitionException {
		InsertSpecification_Context _localctx = new InsertSpecification_Context(_ctx, getState());
		enterRule(_localctx, 2, RULE_insertSpecification_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(342);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DELAYED || _la==HIGH_PRIORITY || _la==LOW_PRIORITY) {
				{
				setState(341);
				_la = _input.LA(1);
				if ( !(_la==DELAYED || _la==HIGH_PRIORITY || _la==LOW_PRIORITY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(345);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IGNORE) {
				{
				setState(344);
				match(IGNORE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertValuesClauseContext extends ParserRuleContext {
		public List<AssignmentValuesContext> assignmentValues() {
			return getRuleContexts(AssignmentValuesContext.class);
		}
		public AssignmentValuesContext assignmentValues(int i) {
			return getRuleContext(AssignmentValuesContext.class,i);
		}
		public TerminalNode VALUES() { return getToken(DMLStatementParser.VALUES, 0); }
		public TerminalNode VALUE() { return getToken(DMLStatementParser.VALUE, 0); }
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public InsertValuesClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertValuesClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterInsertValuesClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitInsertValuesClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitInsertValuesClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsertValuesClauseContext insertValuesClause() throws RecognitionException {
		InsertValuesClauseContext _localctx = new InsertValuesClauseContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_insertValuesClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(348);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(347);
				columnNames();
				}
				break;
			}
			setState(350);
			_la = _input.LA(1);
			if ( !(_la==VALUES || _la==VALUE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(351);
			assignmentValues();
			setState(356);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(352);
				match(COMMA_);
				setState(353);
				assignmentValues();
				}
				}
				setState(358);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertSelectClauseContext extends ParserRuleContext {
		public SelectContext select() {
			return getRuleContext(SelectContext.class,0);
		}
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public InsertSelectClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertSelectClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterInsertSelectClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitInsertSelectClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitInsertSelectClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsertSelectClauseContext insertSelectClause() throws RecognitionException {
		InsertSelectClauseContext _localctx = new InsertSelectClauseContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_insertSelectClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 28)) & ~0x3f) == 0 && ((1L << (_la - 28)) & ((1L << (LP_ - 28)) | (1L << (TRUNCATE - 28)) | (1L << (POSITION - 28)) | (1L << (VIEW - 28)))) != 0) || ((((_la - 108)) & ~0x3f) == 0 && ((1L << (_la - 108)) & ((1L << (ANY - 108)) | (1L << (OFFSET - 108)) | (1L << (BEGIN - 108)) | (1L << (COMMIT - 108)) | (1L << (ROLLBACK - 108)) | (1L << (SAVEPOINT - 108)) | (1L << (BOOLEAN - 108)) | (1L << (DATE - 108)) | (1L << (TIME - 108)) | (1L << (TIMESTAMP - 108)) | (1L << (YEAR - 108)) | (1L << (QUARTER - 108)) | (1L << (MONTH - 108)) | (1L << (WEEK - 108)) | (1L << (DAY - 108)) | (1L << (HOUR - 108)) | (1L << (MINUTE - 108)) | (1L << (SECOND - 108)) | (1L << (MICROSECOND - 108)) | (1L << (MAX - 108)) | (1L << (MIN - 108)) | (1L << (SUM - 108)) | (1L << (COUNT - 108)) | (1L << (AVG - 108)) | (1L << (CURRENT - 108)) | (1L << (ENABLE - 108)) | (1L << (DISABLE - 108)) | (1L << (INSTANCE - 108)) | (1L << (DO - 108)) | (1L << (DEFINER - 108)) | (1L << (CASCADED - 108)) | (1L << (LOCAL - 108)) | (1L << (CLOSE - 108)) | (1L << (OPEN - 108)) | (1L << (NEXT - 108)) | (1L << (NAME - 108)) | (1L << (TYPE - 108)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)))) != 0)) {
				{
				setState(359);
				columnNames();
				}
			}

			setState(362);
			select();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OnDuplicateKeyClauseContext extends ParserRuleContext {
		public TerminalNode ON() { return getToken(DMLStatementParser.ON, 0); }
		public TerminalNode DUPLICATE() { return getToken(DMLStatementParser.DUPLICATE, 0); }
		public TerminalNode KEY() { return getToken(DMLStatementParser.KEY, 0); }
		public TerminalNode UPDATE() { return getToken(DMLStatementParser.UPDATE, 0); }
		public List<AssignmentContext> assignment() {
			return getRuleContexts(AssignmentContext.class);
		}
		public AssignmentContext assignment(int i) {
			return getRuleContext(AssignmentContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public OnDuplicateKeyClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_onDuplicateKeyClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterOnDuplicateKeyClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitOnDuplicateKeyClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitOnDuplicateKeyClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OnDuplicateKeyClauseContext onDuplicateKeyClause() throws RecognitionException {
		OnDuplicateKeyClauseContext _localctx = new OnDuplicateKeyClauseContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_onDuplicateKeyClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(364);
			match(ON);
			setState(365);
			match(DUPLICATE);
			setState(366);
			match(KEY);
			setState(367);
			match(UPDATE);
			setState(368);
			assignment();
			setState(373);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(369);
				match(COMMA_);
				setState(370);
				assignment();
				}
				}
				setState(375);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReplaceContext extends ParserRuleContext {
		public TerminalNode REPLACE() { return getToken(DMLStatementParser.REPLACE, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public InsertValuesClauseContext insertValuesClause() {
			return getRuleContext(InsertValuesClauseContext.class,0);
		}
		public SetAssignmentsClauseContext setAssignmentsClause() {
			return getRuleContext(SetAssignmentsClauseContext.class,0);
		}
		public InsertSelectClauseContext insertSelectClause() {
			return getRuleContext(InsertSelectClauseContext.class,0);
		}
		public ReplaceSpecification_Context replaceSpecification_() {
			return getRuleContext(ReplaceSpecification_Context.class,0);
		}
		public TerminalNode INTO() { return getToken(DMLStatementParser.INTO, 0); }
		public PartitionNames_Context partitionNames_() {
			return getRuleContext(PartitionNames_Context.class,0);
		}
		public ReplaceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_replace; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterReplace(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitReplace(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitReplace(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReplaceContext replace() throws RecognitionException {
		ReplaceContext _localctx = new ReplaceContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_replace);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(376);
			match(REPLACE);
			setState(378);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DELAYED || _la==LOW_PRIORITY) {
				{
				setState(377);
				replaceSpecification_();
				}
			}

			setState(381);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INTO) {
				{
				setState(380);
				match(INTO);
				}
			}

			setState(383);
			tableName();
			setState(385);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARTITION) {
				{
				setState(384);
				partitionNames_();
				}
			}

			setState(390);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(387);
				insertValuesClause();
				}
				break;
			case 2:
				{
				setState(388);
				setAssignmentsClause();
				}
				break;
			case 3:
				{
				setState(389);
				insertSelectClause();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReplaceSpecification_Context extends ParserRuleContext {
		public TerminalNode LOW_PRIORITY() { return getToken(DMLStatementParser.LOW_PRIORITY, 0); }
		public TerminalNode DELAYED() { return getToken(DMLStatementParser.DELAYED, 0); }
		public ReplaceSpecification_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_replaceSpecification_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterReplaceSpecification_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitReplaceSpecification_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitReplaceSpecification_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReplaceSpecification_Context replaceSpecification_() throws RecognitionException {
		ReplaceSpecification_Context _localctx = new ReplaceSpecification_Context(_ctx, getState());
		enterRule(_localctx, 12, RULE_replaceSpecification_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(392);
			_la = _input.LA(1);
			if ( !(_la==DELAYED || _la==LOW_PRIORITY) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpdateContext extends ParserRuleContext {
		public TerminalNode UPDATE() { return getToken(DMLStatementParser.UPDATE, 0); }
		public UpdateSpecification_Context updateSpecification_() {
			return getRuleContext(UpdateSpecification_Context.class,0);
		}
		public TableReferencesContext tableReferences() {
			return getRuleContext(TableReferencesContext.class,0);
		}
		public SetAssignmentsClauseContext setAssignmentsClause() {
			return getRuleContext(SetAssignmentsClauseContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public LimitClauseContext limitClause() {
			return getRuleContext(LimitClauseContext.class,0);
		}
		public UpdateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUpdate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUpdate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUpdate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UpdateContext update() throws RecognitionException {
		UpdateContext _localctx = new UpdateContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_update);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(394);
			match(UPDATE);
			setState(395);
			updateSpecification_();
			setState(396);
			tableReferences();
			setState(397);
			setAssignmentsClause();
			setState(399);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(398);
				whereClause();
				}
			}

			setState(402);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(401);
				orderByClause();
				}
			}

			setState(405);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(404);
				limitClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpdateSpecification_Context extends ParserRuleContext {
		public TerminalNode LOW_PRIORITY() { return getToken(DMLStatementParser.LOW_PRIORITY, 0); }
		public TerminalNode IGNORE() { return getToken(DMLStatementParser.IGNORE, 0); }
		public UpdateSpecification_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_updateSpecification_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUpdateSpecification_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUpdateSpecification_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUpdateSpecification_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UpdateSpecification_Context updateSpecification_() throws RecognitionException {
		UpdateSpecification_Context _localctx = new UpdateSpecification_Context(_ctx, getState());
		enterRule(_localctx, 16, RULE_updateSpecification_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(408);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOW_PRIORITY) {
				{
				setState(407);
				match(LOW_PRIORITY);
				}
			}

			setState(411);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IGNORE) {
				{
				setState(410);
				match(IGNORE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public TerminalNode EQ_() { return getToken(DMLStatementParser.EQ_, 0); }
		public AssignmentValueContext assignmentValue() {
			return getRuleContext(AssignmentValueContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(413);
			columnName();
			setState(414);
			match(EQ_);
			setState(415);
			assignmentValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetAssignmentsClauseContext extends ParserRuleContext {
		public TerminalNode SET() { return getToken(DMLStatementParser.SET, 0); }
		public List<AssignmentContext> assignment() {
			return getRuleContexts(AssignmentContext.class);
		}
		public AssignmentContext assignment(int i) {
			return getRuleContext(AssignmentContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public SetAssignmentsClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setAssignmentsClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSetAssignmentsClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSetAssignmentsClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSetAssignmentsClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetAssignmentsClauseContext setAssignmentsClause() throws RecognitionException {
		SetAssignmentsClauseContext _localctx = new SetAssignmentsClauseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_setAssignmentsClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(417);
			match(SET);
			setState(418);
			assignment();
			setState(423);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(419);
				match(COMMA_);
				setState(420);
				assignment();
				}
				}
				setState(425);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentValuesContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<AssignmentValueContext> assignmentValue() {
			return getRuleContexts(AssignmentValueContext.class);
		}
		public AssignmentValueContext assignmentValue(int i) {
			return getRuleContext(AssignmentValueContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public AssignmentValuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentValues; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterAssignmentValues(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitAssignmentValues(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitAssignmentValues(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentValuesContext assignmentValues() throws RecognitionException {
		AssignmentValuesContext _localctx = new AssignmentValuesContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_assignmentValues);
		int _la;
		try {
			setState(439);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(426);
				match(LP_);
				setState(427);
				assignmentValue();
				setState(432);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(428);
					match(COMMA_);
					setState(429);
					assignmentValue();
					}
					}
					setState(434);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(435);
				match(RP_);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(437);
				match(LP_);
				setState(438);
				match(RP_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentValueContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode DEFAULT() { return getToken(DMLStatementParser.DEFAULT, 0); }
		public BlobValueContext blobValue() {
			return getRuleContext(BlobValueContext.class,0);
		}
		public AssignmentValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterAssignmentValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitAssignmentValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitAssignmentValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentValueContext assignmentValue() throws RecognitionException {
		AssignmentValueContext _localctx = new AssignmentValueContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_assignmentValue);
		try {
			setState(444);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				enterOuterAlt(_localctx, 1);
				{
				setState(441);
				expr(0);
				}
				break;
			case DEFAULT:
				enterOuterAlt(_localctx, 2);
				{
				setState(442);
				match(DEFAULT);
				}
				break;
			case UL_BINARY:
				enterOuterAlt(_localctx, 3);
				{
				setState(443);
				blobValue();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlobValueContext extends ParserRuleContext {
		public TerminalNode UL_BINARY() { return getToken(DMLStatementParser.UL_BINARY, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public BlobValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blobValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterBlobValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitBlobValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitBlobValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlobValueContext blobValue() throws RecognitionException {
		BlobValueContext _localctx = new BlobValueContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_blobValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(446);
			match(UL_BINARY);
			setState(447);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteContext extends ParserRuleContext {
		public TerminalNode DELETE() { return getToken(DMLStatementParser.DELETE, 0); }
		public DeleteSpecification_Context deleteSpecification_() {
			return getRuleContext(DeleteSpecification_Context.class,0);
		}
		public SingleTableClauseContext singleTableClause() {
			return getRuleContext(SingleTableClauseContext.class,0);
		}
		public MultipleTablesClauseContext multipleTablesClause() {
			return getRuleContext(MultipleTablesClauseContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public DeleteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_delete; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDelete(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDelete(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDelete(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeleteContext delete() throws RecognitionException {
		DeleteContext _localctx = new DeleteContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_delete);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(449);
			match(DELETE);
			setState(450);
			deleteSpecification_();
			setState(453);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				{
				setState(451);
				singleTableClause();
				}
				break;
			case 2:
				{
				setState(452);
				multipleTablesClause();
				}
				break;
			}
			setState(456);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(455);
				whereClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteSpecification_Context extends ParserRuleContext {
		public TerminalNode LOW_PRIORITY() { return getToken(DMLStatementParser.LOW_PRIORITY, 0); }
		public TerminalNode QUICK() { return getToken(DMLStatementParser.QUICK, 0); }
		public TerminalNode IGNORE() { return getToken(DMLStatementParser.IGNORE, 0); }
		public DeleteSpecification_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteSpecification_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDeleteSpecification_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDeleteSpecification_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDeleteSpecification_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeleteSpecification_Context deleteSpecification_() throws RecognitionException {
		DeleteSpecification_Context _localctx = new DeleteSpecification_Context(_ctx, getState());
		enterRule(_localctx, 30, RULE_deleteSpecification_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(459);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOW_PRIORITY) {
				{
				setState(458);
				match(LOW_PRIORITY);
				}
			}

			setState(462);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
				setState(461);
				match(QUICK);
				}
				break;
			}
			setState(465);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IGNORE) {
				{
				setState(464);
				match(IGNORE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleTableClauseContext extends ParserRuleContext {
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public AliasContext alias() {
			return getRuleContext(AliasContext.class,0);
		}
		public PartitionNames_Context partitionNames_() {
			return getRuleContext(PartitionNames_Context.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public SingleTableClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleTableClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSingleTableClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSingleTableClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSingleTableClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SingleTableClauseContext singleTableClause() throws RecognitionException {
		SingleTableClauseContext _localctx = new SingleTableClauseContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_singleTableClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(467);
			match(FROM);
			setState(468);
			tableName();
			setState(473);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (TRUNCATE - 49)) | (1L << (POSITION - 49)) | (1L << (VIEW - 49)) | (1L << (AS - 49)) | (1L << (ANY - 49)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (OFFSET - 117)) | (1L << (BEGIN - 117)) | (1L << (COMMIT - 117)) | (1L << (ROLLBACK - 117)) | (1L << (SAVEPOINT - 117)) | (1L << (BOOLEAN - 117)) | (1L << (DATE - 117)) | (1L << (TIME - 117)) | (1L << (TIMESTAMP - 117)) | (1L << (YEAR - 117)) | (1L << (QUARTER - 117)) | (1L << (MONTH - 117)) | (1L << (WEEK - 117)) | (1L << (DAY - 117)) | (1L << (HOUR - 117)) | (1L << (MINUTE - 117)) | (1L << (SECOND - 117)) | (1L << (MICROSECOND - 117)) | (1L << (MAX - 117)) | (1L << (MIN - 117)) | (1L << (SUM - 117)) | (1L << (COUNT - 117)) | (1L << (AVG - 117)) | (1L << (CURRENT - 117)) | (1L << (ENABLE - 117)) | (1L << (DISABLE - 117)) | (1L << (INSTANCE - 117)) | (1L << (DO - 117)) | (1L << (DEFINER - 117)) | (1L << (CASCADED - 117)) | (1L << (LOCAL - 117)) | (1L << (CLOSE - 117)) | (1L << (OPEN - 117)) | (1L << (NEXT - 117)) | (1L << (NAME - 117)) | (1L << (TYPE - 117)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)) | (1L << (STRING_ - 686)))) != 0)) {
				{
				setState(470);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AS) {
					{
					setState(469);
					match(AS);
					}
				}

				setState(472);
				alias();
				}
			}

			setState(476);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARTITION) {
				{
				setState(475);
				partitionNames_();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultipleTablesClauseContext extends ParserRuleContext {
		public MultipleTableNamesContext multipleTableNames() {
			return getRuleContext(MultipleTableNamesContext.class,0);
		}
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public TableReferencesContext tableReferences() {
			return getRuleContext(TableReferencesContext.class,0);
		}
		public TerminalNode USING() { return getToken(DMLStatementParser.USING, 0); }
		public MultipleTablesClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multipleTablesClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterMultipleTablesClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitMultipleTablesClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitMultipleTablesClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultipleTablesClauseContext multipleTablesClause() throws RecognitionException {
		MultipleTablesClauseContext _localctx = new MultipleTablesClauseContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_multipleTablesClause);
		try {
			setState(487);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(478);
				multipleTableNames();
				setState(479);
				match(FROM);
				setState(480);
				tableReferences();
				}
				break;
			case FROM:
				enterOuterAlt(_localctx, 2);
				{
				setState(482);
				match(FROM);
				setState(483);
				multipleTableNames();
				setState(484);
				match(USING);
				setState(485);
				tableReferences();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultipleTableNamesContext extends ParserRuleContext {
		public List<TableNameContext> tableName() {
			return getRuleContexts(TableNameContext.class);
		}
		public TableNameContext tableName(int i) {
			return getRuleContext(TableNameContext.class,i);
		}
		public List<TerminalNode> DOT_ASTERISK_() { return getTokens(DMLStatementParser.DOT_ASTERISK_); }
		public TerminalNode DOT_ASTERISK_(int i) {
			return getToken(DMLStatementParser.DOT_ASTERISK_, i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public MultipleTableNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multipleTableNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterMultipleTableNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitMultipleTableNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitMultipleTableNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultipleTableNamesContext multipleTableNames() throws RecognitionException {
		MultipleTableNamesContext _localctx = new MultipleTableNamesContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_multipleTableNames);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(489);
			tableName();
			setState(491);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOT_ASTERISK_) {
				{
				setState(490);
				match(DOT_ASTERISK_);
				}
			}

			setState(500);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(493);
				match(COMMA_);
				setState(494);
				tableName();
				setState(496);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==DOT_ASTERISK_) {
					{
					setState(495);
					match(DOT_ASTERISK_);
					}
				}

				}
				}
				setState(502);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectContext extends ParserRuleContext {
		public UnionClauseContext unionClause() {
			return getRuleContext(UnionClauseContext.class,0);
		}
		public WithClause_Context withClause_() {
			return getRuleContext(WithClause_Context.class,0);
		}
		public SelectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSelect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSelect(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSelect(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectContext select() throws RecognitionException {
		SelectContext _localctx = new SelectContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_select);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(504);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(503);
				withClause_();
				}
			}

			setState(506);
			unionClause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallContext extends ParserRuleContext {
		public TerminalNode CALL() { return getToken(DMLStatementParser.CALL, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public CallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallContext call() throws RecognitionException {
		CallContext _localctx = new CallContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(508);
			match(CALL);
			setState(509);
			identifier();
			setState(521);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(510);
				match(LP_);
				setState(511);
				expr(0);
				setState(516);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(512);
					match(COMMA_);
					setState(513);
					expr(0);
					}
					}
					setState(518);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(519);
				match(RP_);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoStatementContext extends ParserRuleContext {
		public TerminalNode DO() { return getToken(DMLStatementParser.DO, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode COMMA_() { return getToken(DMLStatementParser.COMMA_, 0); }
		public DoStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDoStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDoStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDoStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DoStatementContext doStatement() throws RecognitionException {
		DoStatementContext _localctx = new DoStatementContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_doStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(523);
			match(DO);
			setState(524);
			expr(0);
			setState(527);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA_) {
				{
				setState(525);
				match(COMMA_);
				setState(526);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HandlerStatementContext extends ParserRuleContext {
		public HandlerOpenStatementContext handlerOpenStatement() {
			return getRuleContext(HandlerOpenStatementContext.class,0);
		}
		public HandlerReadIndexStatementContext handlerReadIndexStatement() {
			return getRuleContext(HandlerReadIndexStatementContext.class,0);
		}
		public HandlerReadStatementContext handlerReadStatement() {
			return getRuleContext(HandlerReadStatementContext.class,0);
		}
		public HandlerCloseStatementContext handlerCloseStatement() {
			return getRuleContext(HandlerCloseStatementContext.class,0);
		}
		public HandlerStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_handlerStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHandlerStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHandlerStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHandlerStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HandlerStatementContext handlerStatement() throws RecognitionException {
		HandlerStatementContext _localctx = new HandlerStatementContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_handlerStatement);
		try {
			setState(533);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(529);
				handlerOpenStatement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(530);
				handlerReadIndexStatement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(531);
				handlerReadStatement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(532);
				handlerCloseStatement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HandlerOpenStatementContext extends ParserRuleContext {
		public TerminalNode HANDLER() { return getToken(DMLStatementParser.HANDLER, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode OPEN() { return getToken(DMLStatementParser.OPEN, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public HandlerOpenStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_handlerOpenStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHandlerOpenStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHandlerOpenStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHandlerOpenStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HandlerOpenStatementContext handlerOpenStatement() throws RecognitionException {
		HandlerOpenStatementContext _localctx = new HandlerOpenStatementContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_handlerOpenStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(535);
			match(HANDLER);
			setState(536);
			tableName();
			setState(537);
			match(OPEN);
			setState(542);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (TRUNCATE - 49)) | (1L << (POSITION - 49)) | (1L << (VIEW - 49)) | (1L << (AS - 49)) | (1L << (ANY - 49)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (OFFSET - 117)) | (1L << (BEGIN - 117)) | (1L << (COMMIT - 117)) | (1L << (ROLLBACK - 117)) | (1L << (SAVEPOINT - 117)) | (1L << (BOOLEAN - 117)) | (1L << (DATE - 117)) | (1L << (TIME - 117)) | (1L << (TIMESTAMP - 117)) | (1L << (YEAR - 117)) | (1L << (QUARTER - 117)) | (1L << (MONTH - 117)) | (1L << (WEEK - 117)) | (1L << (DAY - 117)) | (1L << (HOUR - 117)) | (1L << (MINUTE - 117)) | (1L << (SECOND - 117)) | (1L << (MICROSECOND - 117)) | (1L << (MAX - 117)) | (1L << (MIN - 117)) | (1L << (SUM - 117)) | (1L << (COUNT - 117)) | (1L << (AVG - 117)) | (1L << (CURRENT - 117)) | (1L << (ENABLE - 117)) | (1L << (DISABLE - 117)) | (1L << (INSTANCE - 117)) | (1L << (DO - 117)) | (1L << (DEFINER - 117)) | (1L << (CASCADED - 117)) | (1L << (LOCAL - 117)) | (1L << (CLOSE - 117)) | (1L << (OPEN - 117)) | (1L << (NEXT - 117)) | (1L << (NAME - 117)) | (1L << (TYPE - 117)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)))) != 0)) {
				{
				setState(539);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AS) {
					{
					setState(538);
					match(AS);
					}
				}

				setState(541);
				identifier();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HandlerReadIndexStatementContext extends ParserRuleContext {
		public TerminalNode HANDLER() { return getToken(DMLStatementParser.HANDLER, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode READ() { return getToken(DMLStatementParser.READ, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public ComparisonOperatorContext comparisonOperator() {
			return getRuleContext(ComparisonOperatorContext.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode FIRST() { return getToken(DMLStatementParser.FIRST, 0); }
		public TerminalNode NEXT() { return getToken(DMLStatementParser.NEXT, 0); }
		public TerminalNode PREV() { return getToken(DMLStatementParser.PREV, 0); }
		public TerminalNode LAST() { return getToken(DMLStatementParser.LAST, 0); }
		public TerminalNode WHERE() { return getToken(DMLStatementParser.WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode LIMIT() { return getToken(DMLStatementParser.LIMIT, 0); }
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public HandlerReadIndexStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_handlerReadIndexStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHandlerReadIndexStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHandlerReadIndexStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHandlerReadIndexStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HandlerReadIndexStatementContext handlerReadIndexStatement() throws RecognitionException {
		HandlerReadIndexStatementContext _localctx = new HandlerReadIndexStatementContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_handlerReadIndexStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(544);
			match(HANDLER);
			setState(545);
			tableName();
			setState(546);
			match(READ);
			setState(547);
			identifier();
			setState(554);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EQ_:
			case NEQ_:
			case GT_:
			case GTE_:
			case LT_:
			case LTE_:
				{
				setState(548);
				comparisonOperator();
				setState(549);
				match(LP_);
				setState(550);
				identifier();
				setState(551);
				match(RP_);
				}
				break;
			case NEXT:
			case FIRST:
			case LAST:
			case PREV:
				{
				setState(553);
				_la = _input.LA(1);
				if ( !(((((_la - 162)) & ~0x3f) == 0 && ((1L << (_la - 162)) & ((1L << (NEXT - 162)) | (1L << (FIRST - 162)) | (1L << (LAST - 162)))) != 0) || _la==PREV) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(558);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(556);
				match(WHERE);
				setState(557);
				expr(0);
				}
			}

			setState(562);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(560);
				match(LIMIT);
				setState(561);
				numberLiterals();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HandlerReadStatementContext extends ParserRuleContext {
		public TerminalNode HANDLER() { return getToken(DMLStatementParser.HANDLER, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode READ() { return getToken(DMLStatementParser.READ, 0); }
		public TerminalNode FIRST() { return getToken(DMLStatementParser.FIRST, 0); }
		public TerminalNode NEXT() { return getToken(DMLStatementParser.NEXT, 0); }
		public TerminalNode WHERE() { return getToken(DMLStatementParser.WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode LIMIT() { return getToken(DMLStatementParser.LIMIT, 0); }
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public HandlerReadStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_handlerReadStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHandlerReadStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHandlerReadStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHandlerReadStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HandlerReadStatementContext handlerReadStatement() throws RecognitionException {
		HandlerReadStatementContext _localctx = new HandlerReadStatementContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_handlerReadStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(564);
			match(HANDLER);
			setState(565);
			tableName();
			setState(566);
			match(READ);
			setState(567);
			_la = _input.LA(1);
			if ( !(_la==NEXT || _la==FIRST) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(570);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(568);
				match(WHERE);
				setState(569);
				expr(0);
				}
			}

			setState(574);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(572);
				match(LIMIT);
				setState(573);
				numberLiterals();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HandlerCloseStatementContext extends ParserRuleContext {
		public TerminalNode HANDLER() { return getToken(DMLStatementParser.HANDLER, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode CLOSE() { return getToken(DMLStatementParser.CLOSE, 0); }
		public HandlerCloseStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_handlerCloseStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHandlerCloseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHandlerCloseStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHandlerCloseStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HandlerCloseStatementContext handlerCloseStatement() throws RecognitionException {
		HandlerCloseStatementContext _localctx = new HandlerCloseStatementContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_handlerCloseStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(576);
			match(HANDLER);
			setState(577);
			tableName();
			setState(578);
			match(CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportStatementContext extends ParserRuleContext {
		public TerminalNode IMPORT() { return getToken(DMLStatementParser.IMPORT, 0); }
		public TerminalNode TABLE() { return getToken(DMLStatementParser.TABLE, 0); }
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public TerminalNode COMMA_() { return getToken(DMLStatementParser.COMMA_, 0); }
		public ImportStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterImportStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitImportStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitImportStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImportStatementContext importStatement() throws RecognitionException {
		ImportStatementContext _localctx = new ImportStatementContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_importStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(580);
			match(IMPORT);
			setState(581);
			match(TABLE);
			setState(582);
			match(FROM);
			setState(583);
			match(STRING_);
			setState(586);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA_) {
				{
				setState(584);
				match(COMMA_);
				setState(585);
				match(STRING_);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoadDataStatementContext extends ParserRuleContext {
		public TerminalNode LOAD() { return getToken(DMLStatementParser.LOAD, 0); }
		public TerminalNode DATA() { return getToken(DMLStatementParser.DATA, 0); }
		public TerminalNode INFILE() { return getToken(DMLStatementParser.INFILE, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public TerminalNode INTO() { return getToken(DMLStatementParser.INTO, 0); }
		public TerminalNode TABLE() { return getToken(DMLStatementParser.TABLE, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode LOCAL() { return getToken(DMLStatementParser.LOCAL, 0); }
		public TerminalNode PARTITION() { return getToken(DMLStatementParser.PARTITION, 0); }
		public List<TerminalNode> LP_() { return getTokens(DMLStatementParser.LP_); }
		public TerminalNode LP_(int i) {
			return getToken(DMLStatementParser.LP_, i);
		}
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> RP_() { return getTokens(DMLStatementParser.RP_); }
		public TerminalNode RP_(int i) {
			return getToken(DMLStatementParser.RP_, i);
		}
		public TerminalNode CHARACTER() { return getToken(DMLStatementParser.CHARACTER, 0); }
		public TerminalNode SET() { return getToken(DMLStatementParser.SET, 0); }
		public List<TerminalNode> LINES() { return getTokens(DMLStatementParser.LINES); }
		public TerminalNode LINES(int i) {
			return getToken(DMLStatementParser.LINES, i);
		}
		public List<TerminalNode> IGNORE() { return getTokens(DMLStatementParser.IGNORE); }
		public TerminalNode IGNORE(int i) {
			return getToken(DMLStatementParser.IGNORE, i);
		}
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public SetAssignmentsClauseContext setAssignmentsClause() {
			return getRuleContext(SetAssignmentsClauseContext.class,0);
		}
		public TerminalNode LOW_PRIORITY() { return getToken(DMLStatementParser.LOW_PRIORITY, 0); }
		public TerminalNode CONCURRENT() { return getToken(DMLStatementParser.CONCURRENT, 0); }
		public TerminalNode REPLACE() { return getToken(DMLStatementParser.REPLACE, 0); }
		public TerminalNode FIELDS() { return getToken(DMLStatementParser.FIELDS, 0); }
		public TerminalNode COLUMNS() { return getToken(DMLStatementParser.COLUMNS, 0); }
		public TerminalNode ROWS() { return getToken(DMLStatementParser.ROWS, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public List<SelectFieldsInto_Context> selectFieldsInto_() {
			return getRuleContexts(SelectFieldsInto_Context.class);
		}
		public SelectFieldsInto_Context selectFieldsInto_(int i) {
			return getRuleContext(SelectFieldsInto_Context.class,i);
		}
		public List<SelectLinesInto_Context> selectLinesInto_() {
			return getRuleContexts(SelectLinesInto_Context.class);
		}
		public SelectLinesInto_Context selectLinesInto_(int i) {
			return getRuleContext(SelectLinesInto_Context.class,i);
		}
		public LoadDataStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loadDataStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLoadDataStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLoadDataStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLoadDataStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoadDataStatementContext loadDataStatement() throws RecognitionException {
		LoadDataStatementContext _localctx = new LoadDataStatementContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_loadDataStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(588);
			match(LOAD);
			setState(589);
			match(DATA);
			setState(591);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOW_PRIORITY || _la==CONCURRENT) {
				{
				setState(590);
				_la = _input.LA(1);
				if ( !(_la==LOW_PRIORITY || _la==CONCURRENT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(594);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOCAL) {
				{
				setState(593);
				match(LOCAL);
				}
			}

			setState(596);
			match(INFILE);
			setState(597);
			match(STRING_);
			setState(599);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==REPLACE || _la==IGNORE) {
				{
				setState(598);
				_la = _input.LA(1);
				if ( !(_la==REPLACE || _la==IGNORE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(601);
			match(INTO);
			setState(602);
			match(TABLE);
			setState(603);
			tableName();
			setState(616);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARTITION) {
				{
				setState(604);
				match(PARTITION);
				setState(605);
				match(LP_);
				setState(606);
				identifier();
				setState(611);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(607);
					match(COMMA_);
					setState(608);
					identifier();
					}
					}
					setState(613);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(614);
				match(RP_);
				}
			}

			setState(621);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CHARACTER) {
				{
				setState(618);
				match(CHARACTER);
				setState(619);
				match(SET);
				setState(620);
				identifier();
				}
			}

			setState(629);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COLUMNS || _la==FIELDS) {
				{
				setState(623);
				_la = _input.LA(1);
				if ( !(_la==COLUMNS || _la==FIELDS) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(625); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(624);
					selectFieldsInto_();
					}
					}
					setState(627); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 469)) & ~0x3f) == 0 && ((1L << (_la - 469)) & ((1L << (TERMINATED - 469)) | (1L << (OPTIONALLY - 469)) | (1L << (ENCLOSED - 469)) | (1L << (ESCAPED - 469)))) != 0) );
				}
			}

			setState(637);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LINES) {
				{
				setState(631);
				match(LINES);
				setState(633); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(632);
					selectLinesInto_();
					}
					}
					setState(635); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==STARTING || _la==TERMINATED );
				}
			}

			setState(643);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IGNORE) {
				{
				setState(639);
				match(IGNORE);
				setState(640);
				numberLiterals();
				setState(641);
				_la = _input.LA(1);
				if ( !(_la==ROWS || _la==LINES) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(656);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(645);
				match(LP_);
				setState(646);
				identifier();
				setState(651);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(647);
					match(COMMA_);
					setState(648);
					identifier();
					}
					}
					setState(653);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(654);
				match(RP_);
				}
			}

			setState(659);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SET) {
				{
				setState(658);
				setAssignmentsClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoadXmlStatementContext extends ParserRuleContext {
		public TerminalNode LOAD() { return getToken(DMLStatementParser.LOAD, 0); }
		public TerminalNode XML() { return getToken(DMLStatementParser.XML, 0); }
		public TerminalNode INFILE() { return getToken(DMLStatementParser.INFILE, 0); }
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public TerminalNode INTO() { return getToken(DMLStatementParser.INTO, 0); }
		public TerminalNode TABLE() { return getToken(DMLStatementParser.TABLE, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode LOCAL() { return getToken(DMLStatementParser.LOCAL, 0); }
		public TerminalNode CHARACTER() { return getToken(DMLStatementParser.CHARACTER, 0); }
		public TerminalNode SET() { return getToken(DMLStatementParser.SET, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> ROWS() { return getTokens(DMLStatementParser.ROWS); }
		public TerminalNode ROWS(int i) {
			return getToken(DMLStatementParser.ROWS, i);
		}
		public TerminalNode IDENTIFIED() { return getToken(DMLStatementParser.IDENTIFIED, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public TerminalNode LT_() { return getToken(DMLStatementParser.LT_, 0); }
		public TerminalNode GT_() { return getToken(DMLStatementParser.GT_, 0); }
		public List<TerminalNode> IGNORE() { return getTokens(DMLStatementParser.IGNORE); }
		public TerminalNode IGNORE(int i) {
			return getToken(DMLStatementParser.IGNORE, i);
		}
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public SetAssignmentsClauseContext setAssignmentsClause() {
			return getRuleContext(SetAssignmentsClauseContext.class,0);
		}
		public TerminalNode LOW_PRIORITY() { return getToken(DMLStatementParser.LOW_PRIORITY, 0); }
		public TerminalNode CONCURRENT() { return getToken(DMLStatementParser.CONCURRENT, 0); }
		public TerminalNode REPLACE() { return getToken(DMLStatementParser.REPLACE, 0); }
		public TerminalNode LINES() { return getToken(DMLStatementParser.LINES, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public LoadXmlStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loadXmlStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLoadXmlStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLoadXmlStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLoadXmlStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LoadXmlStatementContext loadXmlStatement() throws RecognitionException {
		LoadXmlStatementContext _localctx = new LoadXmlStatementContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_loadXmlStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(661);
			match(LOAD);
			setState(662);
			match(XML);
			setState(664);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOW_PRIORITY || _la==CONCURRENT) {
				{
				setState(663);
				_la = _input.LA(1);
				if ( !(_la==LOW_PRIORITY || _la==CONCURRENT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(667);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LOCAL) {
				{
				setState(666);
				match(LOCAL);
				}
			}

			setState(669);
			match(INFILE);
			setState(670);
			match(STRING_);
			setState(672);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==REPLACE || _la==IGNORE) {
				{
				setState(671);
				_la = _input.LA(1);
				if ( !(_la==REPLACE || _la==IGNORE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(674);
			match(INTO);
			setState(675);
			match(TABLE);
			setState(676);
			tableName();
			setState(680);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CHARACTER) {
				{
				setState(677);
				match(CHARACTER);
				setState(678);
				match(SET);
				setState(679);
				identifier();
				}
			}

			setState(688);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ROWS) {
				{
				setState(682);
				match(ROWS);
				setState(683);
				match(IDENTIFIED);
				setState(684);
				match(BY);
				setState(685);
				match(LT_);
				setState(686);
				match(STRING_);
				setState(687);
				match(GT_);
				}
			}

			setState(694);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IGNORE) {
				{
				setState(690);
				match(IGNORE);
				setState(691);
				numberLiterals();
				setState(692);
				_la = _input.LA(1);
				if ( !(_la==ROWS || _la==LINES) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(707);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(696);
				match(LP_);
				setState(697);
				identifier();
				setState(702);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(698);
					match(COMMA_);
					setState(699);
					identifier();
					}
					}
					setState(704);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(705);
				match(RP_);
				}
			}

			setState(710);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SET) {
				{
				setState(709);
				setAssignmentsClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WithClause_Context extends ParserRuleContext {
		public TerminalNode WITH() { return getToken(DMLStatementParser.WITH, 0); }
		public List<CteClause_Context> cteClause_() {
			return getRuleContexts(CteClause_Context.class);
		}
		public CteClause_Context cteClause_(int i) {
			return getRuleContext(CteClause_Context.class,i);
		}
		public TerminalNode RECURSIVE() { return getToken(DMLStatementParser.RECURSIVE, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public WithClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_withClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWithClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWithClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWithClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WithClause_Context withClause_() throws RecognitionException {
		WithClause_Context _localctx = new WithClause_Context(_ctx, getState());
		enterRule(_localctx, 60, RULE_withClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(712);
			match(WITH);
			setState(714);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RECURSIVE) {
				{
				setState(713);
				match(RECURSIVE);
				}
			}

			setState(716);
			cteClause_();
			setState(721);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(717);
				match(COMMA_);
				setState(718);
				cteClause_();
				}
				}
				setState(723);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CteClause_Context extends ParserRuleContext {
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public CteClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cteClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCteClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCteClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCteClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CteClause_Context cteClause_() throws RecognitionException {
		CteClause_Context _localctx = new CteClause_Context(_ctx, getState());
		enterRule(_localctx, 62, RULE_cteClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(724);
			ignoredIdentifier_();
			setState(726);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 28)) & ~0x3f) == 0 && ((1L << (_la - 28)) & ((1L << (LP_ - 28)) | (1L << (TRUNCATE - 28)) | (1L << (POSITION - 28)) | (1L << (VIEW - 28)))) != 0) || ((((_la - 108)) & ~0x3f) == 0 && ((1L << (_la - 108)) & ((1L << (ANY - 108)) | (1L << (OFFSET - 108)) | (1L << (BEGIN - 108)) | (1L << (COMMIT - 108)) | (1L << (ROLLBACK - 108)) | (1L << (SAVEPOINT - 108)) | (1L << (BOOLEAN - 108)) | (1L << (DATE - 108)) | (1L << (TIME - 108)) | (1L << (TIMESTAMP - 108)) | (1L << (YEAR - 108)) | (1L << (QUARTER - 108)) | (1L << (MONTH - 108)) | (1L << (WEEK - 108)) | (1L << (DAY - 108)) | (1L << (HOUR - 108)) | (1L << (MINUTE - 108)) | (1L << (SECOND - 108)) | (1L << (MICROSECOND - 108)) | (1L << (MAX - 108)) | (1L << (MIN - 108)) | (1L << (SUM - 108)) | (1L << (COUNT - 108)) | (1L << (AVG - 108)) | (1L << (CURRENT - 108)) | (1L << (ENABLE - 108)) | (1L << (DISABLE - 108)) | (1L << (INSTANCE - 108)) | (1L << (DO - 108)) | (1L << (DEFINER - 108)) | (1L << (CASCADED - 108)) | (1L << (LOCAL - 108)) | (1L << (CLOSE - 108)) | (1L << (OPEN - 108)) | (1L << (NEXT - 108)) | (1L << (NAME - 108)) | (1L << (TYPE - 108)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)))) != 0)) {
				{
				setState(725);
				columnNames();
				}
			}

			setState(728);
			match(AS);
			setState(729);
			subquery();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnionClauseContext extends ParserRuleContext {
		public List<SelectClauseContext> selectClause() {
			return getRuleContexts(SelectClauseContext.class);
		}
		public SelectClauseContext selectClause(int i) {
			return getRuleContext(SelectClauseContext.class,i);
		}
		public List<TerminalNode> UNION() { return getTokens(DMLStatementParser.UNION); }
		public TerminalNode UNION(int i) {
			return getToken(DMLStatementParser.UNION, i);
		}
		public List<TerminalNode> ALL() { return getTokens(DMLStatementParser.ALL); }
		public TerminalNode ALL(int i) {
			return getToken(DMLStatementParser.ALL, i);
		}
		public List<TerminalNode> DISTINCT() { return getTokens(DMLStatementParser.DISTINCT); }
		public TerminalNode DISTINCT(int i) {
			return getToken(DMLStatementParser.DISTINCT, i);
		}
		public UnionClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unionClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUnionClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUnionClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUnionClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnionClauseContext unionClause() throws RecognitionException {
		UnionClauseContext _localctx = new UnionClauseContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_unionClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(731);
			selectClause();
			setState(739);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==UNION) {
				{
				{
				setState(732);
				match(UNION);
				setState(734);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==DISTINCT || _la==ALL) {
					{
					setState(733);
					_la = _input.LA(1);
					if ( !(_la==DISTINCT || _la==ALL) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(736);
				selectClause();
				}
				}
				setState(741);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectClauseContext extends ParserRuleContext {
		public TerminalNode SELECT() { return getToken(DMLStatementParser.SELECT, 0); }
		public ProjectionsContext projections() {
			return getRuleContext(ProjectionsContext.class,0);
		}
		public List<SelectSpecificationContext> selectSpecification() {
			return getRuleContexts(SelectSpecificationContext.class);
		}
		public SelectSpecificationContext selectSpecification(int i) {
			return getRuleContext(SelectSpecificationContext.class,i);
		}
		public FromClauseContext fromClause() {
			return getRuleContext(FromClauseContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public GroupByClauseContext groupByClause() {
			return getRuleContext(GroupByClauseContext.class,0);
		}
		public HavingClauseContext havingClause() {
			return getRuleContext(HavingClauseContext.class,0);
		}
		public WindowClause_Context windowClause_() {
			return getRuleContext(WindowClause_Context.class,0);
		}
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public LimitClauseContext limitClause() {
			return getRuleContext(LimitClauseContext.class,0);
		}
		public SelectIntoExpression_Context selectIntoExpression_() {
			return getRuleContext(SelectIntoExpression_Context.class,0);
		}
		public LockClauseContext lockClause() {
			return getRuleContext(LockClauseContext.class,0);
		}
		public SelectClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSelectClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSelectClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSelectClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectClauseContext selectClause() throws RecognitionException {
		SelectClauseContext _localctx = new SelectClauseContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_selectClause);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(742);
			match(SELECT);
			setState(746);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(743);
					selectSpecification();
					}
					} 
				}
				setState(748);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
			}
			setState(749);
			projections();
			setState(751);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FROM) {
				{
				setState(750);
				fromClause();
				}
			}

			setState(754);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WHERE) {
				{
				setState(753);
				whereClause();
				}
			}

			setState(757);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==GROUP) {
				{
				setState(756);
				groupByClause();
				}
			}

			setState(760);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==HAVING) {
				{
				setState(759);
				havingClause();
				}
			}

			setState(763);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WINDOW) {
				{
				setState(762);
				windowClause_();
				}
			}

			setState(766);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(765);
				orderByClause();
				}
			}

			setState(769);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(768);
				limitClause();
				}
			}

			setState(772);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==INTO) {
				{
				setState(771);
				selectIntoExpression_();
				}
			}

			setState(775);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FOR || _la==LOCK) {
				{
				setState(774);
				lockClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectSpecificationContext extends ParserRuleContext {
		public DuplicateSpecificationContext duplicateSpecification() {
			return getRuleContext(DuplicateSpecificationContext.class,0);
		}
		public TerminalNode HIGH_PRIORITY() { return getToken(DMLStatementParser.HIGH_PRIORITY, 0); }
		public TerminalNode STRAIGHT_JOIN() { return getToken(DMLStatementParser.STRAIGHT_JOIN, 0); }
		public TerminalNode SQL_SMALL_RESULT() { return getToken(DMLStatementParser.SQL_SMALL_RESULT, 0); }
		public TerminalNode SQL_BIG_RESULT() { return getToken(DMLStatementParser.SQL_BIG_RESULT, 0); }
		public TerminalNode SQL_BUFFER_RESULT() { return getToken(DMLStatementParser.SQL_BUFFER_RESULT, 0); }
		public TerminalNode SQL_CACHE() { return getToken(DMLStatementParser.SQL_CACHE, 0); }
		public TerminalNode SQL_NO_CACHE() { return getToken(DMLStatementParser.SQL_NO_CACHE, 0); }
		public TerminalNode SQL_CALC_FOUND_ROWS() { return getToken(DMLStatementParser.SQL_CALC_FOUND_ROWS, 0); }
		public SelectSpecificationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectSpecification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSelectSpecification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSelectSpecification(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSelectSpecification(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectSpecificationContext selectSpecification() throws RecognitionException {
		SelectSpecificationContext _localctx = new SelectSpecificationContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_selectSpecification);
		int _la;
		try {
			setState(785);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DISTINCT:
			case ALL:
			case DISTINCTROW:
				enterOuterAlt(_localctx, 1);
				{
				setState(777);
				duplicateSpecification();
				}
				break;
			case HIGH_PRIORITY:
				enterOuterAlt(_localctx, 2);
				{
				setState(778);
				match(HIGH_PRIORITY);
				}
				break;
			case STRAIGHT_JOIN:
				enterOuterAlt(_localctx, 3);
				{
				setState(779);
				match(STRAIGHT_JOIN);
				}
				break;
			case SQL_SMALL_RESULT:
				enterOuterAlt(_localctx, 4);
				{
				setState(780);
				match(SQL_SMALL_RESULT);
				}
				break;
			case SQL_BIG_RESULT:
				enterOuterAlt(_localctx, 5);
				{
				setState(781);
				match(SQL_BIG_RESULT);
				}
				break;
			case SQL_BUFFER_RESULT:
				enterOuterAlt(_localctx, 6);
				{
				setState(782);
				match(SQL_BUFFER_RESULT);
				}
				break;
			case SQL_CACHE:
			case SQL_NO_CACHE:
				enterOuterAlt(_localctx, 7);
				{
				setState(783);
				_la = _input.LA(1);
				if ( !(_la==SQL_CACHE || _la==SQL_NO_CACHE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case SQL_CALC_FOUND_ROWS:
				enterOuterAlt(_localctx, 8);
				{
				setState(784);
				match(SQL_CALC_FOUND_ROWS);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DuplicateSpecificationContext extends ParserRuleContext {
		public TerminalNode ALL() { return getToken(DMLStatementParser.ALL, 0); }
		public TerminalNode DISTINCT() { return getToken(DMLStatementParser.DISTINCT, 0); }
		public TerminalNode DISTINCTROW() { return getToken(DMLStatementParser.DISTINCTROW, 0); }
		public DuplicateSpecificationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_duplicateSpecification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDuplicateSpecification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDuplicateSpecification(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDuplicateSpecification(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DuplicateSpecificationContext duplicateSpecification() throws RecognitionException {
		DuplicateSpecificationContext _localctx = new DuplicateSpecificationContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_duplicateSpecification);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(787);
			_la = _input.LA(1);
			if ( !(_la==DISTINCT || _la==ALL || _la==DISTINCTROW) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProjectionsContext extends ParserRuleContext {
		public UnqualifiedShorthandContext unqualifiedShorthand() {
			return getRuleContext(UnqualifiedShorthandContext.class,0);
		}
		public List<ProjectionContext> projection() {
			return getRuleContexts(ProjectionContext.class);
		}
		public ProjectionContext projection(int i) {
			return getRuleContext(ProjectionContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public ProjectionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_projections; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterProjections(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitProjections(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitProjections(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProjectionsContext projections() throws RecognitionException {
		ProjectionsContext _localctx = new ProjectionsContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_projections);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(791);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ASTERISK_:
				{
				setState(789);
				unqualifiedShorthand();
				}
				break;
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(790);
				projection();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(797);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(793);
				match(COMMA_);
				setState(794);
				projection();
				}
				}
				setState(799);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProjectionContext extends ParserRuleContext {
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AliasContext alias() {
			return getRuleContext(AliasContext.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public QualifiedShorthandContext qualifiedShorthand() {
			return getRuleContext(QualifiedShorthandContext.class,0);
		}
		public ProjectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_projection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterProjection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitProjection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitProjection(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProjectionContext projection() throws RecognitionException {
		ProjectionContext _localctx = new ProjectionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_projection);
		int _la;
		try {
			setState(811);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,92,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(802);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,89,_ctx) ) {
				case 1:
					{
					setState(800);
					columnName();
					}
					break;
				case 2:
					{
					setState(801);
					expr(0);
					}
					break;
				}
				setState(808);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (TRUNCATE - 49)) | (1L << (POSITION - 49)) | (1L << (VIEW - 49)) | (1L << (AS - 49)) | (1L << (ANY - 49)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (OFFSET - 117)) | (1L << (BEGIN - 117)) | (1L << (COMMIT - 117)) | (1L << (ROLLBACK - 117)) | (1L << (SAVEPOINT - 117)) | (1L << (BOOLEAN - 117)) | (1L << (DATE - 117)) | (1L << (TIME - 117)) | (1L << (TIMESTAMP - 117)) | (1L << (YEAR - 117)) | (1L << (QUARTER - 117)) | (1L << (MONTH - 117)) | (1L << (WEEK - 117)) | (1L << (DAY - 117)) | (1L << (HOUR - 117)) | (1L << (MINUTE - 117)) | (1L << (SECOND - 117)) | (1L << (MICROSECOND - 117)) | (1L << (MAX - 117)) | (1L << (MIN - 117)) | (1L << (SUM - 117)) | (1L << (COUNT - 117)) | (1L << (AVG - 117)) | (1L << (CURRENT - 117)) | (1L << (ENABLE - 117)) | (1L << (DISABLE - 117)) | (1L << (INSTANCE - 117)) | (1L << (DO - 117)) | (1L << (DEFINER - 117)) | (1L << (CASCADED - 117)) | (1L << (LOCAL - 117)) | (1L << (CLOSE - 117)) | (1L << (OPEN - 117)) | (1L << (NEXT - 117)) | (1L << (NAME - 117)) | (1L << (TYPE - 117)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)) | (1L << (STRING_ - 686)))) != 0)) {
					{
					setState(805);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==AS) {
						{
						setState(804);
						match(AS);
						}
					}

					setState(807);
					alias();
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(810);
				qualifiedShorthand();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AliasContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public AliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterAlias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitAlias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitAlias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AliasContext alias() throws RecognitionException {
		AliasContext _localctx = new AliasContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_alias);
		try {
			setState(815);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(813);
				identifier();
				}
				break;
			case STRING_:
				enterOuterAlt(_localctx, 2);
				{
				setState(814);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnqualifiedShorthandContext extends ParserRuleContext {
		public TerminalNode ASTERISK_() { return getToken(DMLStatementParser.ASTERISK_, 0); }
		public UnqualifiedShorthandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unqualifiedShorthand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUnqualifiedShorthand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUnqualifiedShorthand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUnqualifiedShorthand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnqualifiedShorthandContext unqualifiedShorthand() throws RecognitionException {
		UnqualifiedShorthandContext _localctx = new UnqualifiedShorthandContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_unqualifiedShorthand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(817);
			match(ASTERISK_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualifiedShorthandContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode DOT_ASTERISK_() { return getToken(DMLStatementParser.DOT_ASTERISK_, 0); }
		public QualifiedShorthandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedShorthand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterQualifiedShorthand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitQualifiedShorthand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitQualifiedShorthand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QualifiedShorthandContext qualifiedShorthand() throws RecognitionException {
		QualifiedShorthandContext _localctx = new QualifiedShorthandContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_qualifiedShorthand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(819);
			identifier();
			setState(820);
			match(DOT_ASTERISK_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FromClauseContext extends ParserRuleContext {
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public TableReferencesContext tableReferences() {
			return getRuleContext(TableReferencesContext.class,0);
		}
		public FromClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fromClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFromClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFromClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFromClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FromClauseContext fromClause() throws RecognitionException {
		FromClauseContext _localctx = new FromClauseContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_fromClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(822);
			match(FROM);
			setState(823);
			tableReferences();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableReferencesContext extends ParserRuleContext {
		public List<EscapedTableReferenceContext> escapedTableReference() {
			return getRuleContexts(EscapedTableReferenceContext.class);
		}
		public EscapedTableReferenceContext escapedTableReference(int i) {
			return getRuleContext(EscapedTableReferenceContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TableReferencesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableReferences; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTableReferences(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTableReferences(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTableReferences(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableReferencesContext tableReferences() throws RecognitionException {
		TableReferencesContext _localctx = new TableReferencesContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_tableReferences);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(825);
			escapedTableReference();
			setState(830);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(826);
				match(COMMA_);
				setState(827);
				escapedTableReference();
				}
				}
				setState(832);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EscapedTableReferenceContext extends ParserRuleContext {
		public TableReferenceContext tableReference() {
			return getRuleContext(TableReferenceContext.class,0);
		}
		public TerminalNode LBE_() { return getToken(DMLStatementParser.LBE_, 0); }
		public TerminalNode OJ() { return getToken(DMLStatementParser.OJ, 0); }
		public TerminalNode RBE_() { return getToken(DMLStatementParser.RBE_, 0); }
		public EscapedTableReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_escapedTableReference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterEscapedTableReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitEscapedTableReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitEscapedTableReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EscapedTableReferenceContext escapedTableReference() throws RecognitionException {
		EscapedTableReferenceContext _localctx = new EscapedTableReferenceContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_escapedTableReference);
		try {
			setState(839);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LP_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(833);
				tableReference();
				}
				break;
			case LBE_:
				enterOuterAlt(_localctx, 2);
				{
				setState(834);
				match(LBE_);
				setState(835);
				match(OJ);
				setState(836);
				tableReference();
				setState(837);
				match(RBE_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableReferenceContext extends ParserRuleContext {
		public TableFactorContext tableFactor() {
			return getRuleContext(TableFactorContext.class,0);
		}
		public List<JoinedTableContext> joinedTable() {
			return getRuleContexts(JoinedTableContext.class);
		}
		public JoinedTableContext joinedTable(int i) {
			return getRuleContext(JoinedTableContext.class,i);
		}
		public TableReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableReference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTableReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTableReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTableReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableReferenceContext tableReference() throws RecognitionException {
		TableReferenceContext _localctx = new TableReferenceContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_tableReference);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(841);
			tableFactor();
			setState(845);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 80)) & ~0x3f) == 0 && ((1L << (_la - 80)) & ((1L << (NATURAL - 80)) | (1L << (JOIN - 80)) | (1L << (INNER - 80)) | (1L << (LEFT - 80)) | (1L << (RIGHT - 80)) | (1L << (CROSS - 80)))) != 0) || _la==STRAIGHT_JOIN) {
				{
				{
				setState(842);
				joinedTable();
				}
				}
				setState(847);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableFactorContext extends ParserRuleContext {
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public PartitionNames_Context partitionNames_() {
			return getRuleContext(PartitionNames_Context.class,0);
		}
		public AliasContext alias() {
			return getRuleContext(AliasContext.class,0);
		}
		public IndexHintList_Context indexHintList_() {
			return getRuleContext(IndexHintList_Context.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TableReferencesContext tableReferences() {
			return getRuleContext(TableReferencesContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TableFactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableFactor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTableFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTableFactor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTableFactor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableFactorContext tableFactor() throws RecognitionException {
		TableFactorContext _localctx = new TableFactorContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_tableFactor);
		int _la;
		try {
			setState(873);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(848);
				tableName();
				setState(850);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PARTITION) {
					{
					setState(849);
					partitionNames_();
					}
				}

				setState(856);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (TRUNCATE - 49)) | (1L << (POSITION - 49)) | (1L << (VIEW - 49)) | (1L << (AS - 49)) | (1L << (ANY - 49)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (OFFSET - 117)) | (1L << (BEGIN - 117)) | (1L << (COMMIT - 117)) | (1L << (ROLLBACK - 117)) | (1L << (SAVEPOINT - 117)) | (1L << (BOOLEAN - 117)) | (1L << (DATE - 117)) | (1L << (TIME - 117)) | (1L << (TIMESTAMP - 117)) | (1L << (YEAR - 117)) | (1L << (QUARTER - 117)) | (1L << (MONTH - 117)) | (1L << (WEEK - 117)) | (1L << (DAY - 117)) | (1L << (HOUR - 117)) | (1L << (MINUTE - 117)) | (1L << (SECOND - 117)) | (1L << (MICROSECOND - 117)) | (1L << (MAX - 117)) | (1L << (MIN - 117)) | (1L << (SUM - 117)) | (1L << (COUNT - 117)) | (1L << (AVG - 117)) | (1L << (CURRENT - 117)) | (1L << (ENABLE - 117)) | (1L << (DISABLE - 117)) | (1L << (INSTANCE - 117)) | (1L << (DO - 117)) | (1L << (DEFINER - 117)) | (1L << (CASCADED - 117)) | (1L << (LOCAL - 117)) | (1L << (CLOSE - 117)) | (1L << (OPEN - 117)) | (1L << (NEXT - 117)) | (1L << (NAME - 117)) | (1L << (TYPE - 117)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)) | (1L << (STRING_ - 686)))) != 0)) {
					{
					setState(853);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==AS) {
						{
						setState(852);
						match(AS);
						}
					}

					setState(855);
					alias();
					}
				}

				setState(859);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==USE || _la==FORCE || _la==IGNORE) {
					{
					setState(858);
					indexHintList_();
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(861);
				subquery();
				setState(863);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==AS) {
					{
					setState(862);
					match(AS);
					}
				}

				setState(865);
				alias();
				setState(867);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 28)) & ~0x3f) == 0 && ((1L << (_la - 28)) & ((1L << (LP_ - 28)) | (1L << (TRUNCATE - 28)) | (1L << (POSITION - 28)) | (1L << (VIEW - 28)))) != 0) || ((((_la - 108)) & ~0x3f) == 0 && ((1L << (_la - 108)) & ((1L << (ANY - 108)) | (1L << (OFFSET - 108)) | (1L << (BEGIN - 108)) | (1L << (COMMIT - 108)) | (1L << (ROLLBACK - 108)) | (1L << (SAVEPOINT - 108)) | (1L << (BOOLEAN - 108)) | (1L << (DATE - 108)) | (1L << (TIME - 108)) | (1L << (TIMESTAMP - 108)) | (1L << (YEAR - 108)) | (1L << (QUARTER - 108)) | (1L << (MONTH - 108)) | (1L << (WEEK - 108)) | (1L << (DAY - 108)) | (1L << (HOUR - 108)) | (1L << (MINUTE - 108)) | (1L << (SECOND - 108)) | (1L << (MICROSECOND - 108)) | (1L << (MAX - 108)) | (1L << (MIN - 108)) | (1L << (SUM - 108)) | (1L << (COUNT - 108)) | (1L << (AVG - 108)) | (1L << (CURRENT - 108)) | (1L << (ENABLE - 108)) | (1L << (DISABLE - 108)) | (1L << (INSTANCE - 108)) | (1L << (DO - 108)) | (1L << (DEFINER - 108)) | (1L << (CASCADED - 108)) | (1L << (LOCAL - 108)) | (1L << (CLOSE - 108)) | (1L << (OPEN - 108)) | (1L << (NEXT - 108)) | (1L << (NAME - 108)) | (1L << (TYPE - 108)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)))) != 0)) {
					{
					setState(866);
					columnNames();
					}
				}

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(869);
				match(LP_);
				setState(870);
				tableReferences();
				setState(871);
				match(RP_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PartitionNames_Context extends ParserRuleContext {
		public TerminalNode PARTITION() { return getToken(DMLStatementParser.PARTITION, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public PartitionNames_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_partitionNames_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPartitionNames_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPartitionNames_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPartitionNames_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PartitionNames_Context partitionNames_() throws RecognitionException {
		PartitionNames_Context _localctx = new PartitionNames_Context(_ctx, getState());
		enterRule(_localctx, 92, RULE_partitionNames_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(875);
			match(PARTITION);
			setState(876);
			match(LP_);
			setState(877);
			identifier();
			setState(882);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(878);
				match(COMMA_);
				setState(879);
				identifier();
				}
				}
				setState(884);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(885);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IndexHintList_Context extends ParserRuleContext {
		public List<IndexHint_Context> indexHint_() {
			return getRuleContexts(IndexHint_Context.class);
		}
		public IndexHint_Context indexHint_(int i) {
			return getRuleContext(IndexHint_Context.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public IndexHintList_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexHintList_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIndexHintList_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIndexHintList_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIndexHintList_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexHintList_Context indexHintList_() throws RecognitionException {
		IndexHintList_Context _localctx = new IndexHintList_Context(_ctx, getState());
		enterRule(_localctx, 94, RULE_indexHintList_);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(887);
			indexHint_();
			setState(892);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,105,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(888);
					match(COMMA_);
					setState(889);
					indexHint_();
					}
					} 
				}
				setState(894);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,105,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IndexHint_Context extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<IndexNameContext> indexName() {
			return getRuleContexts(IndexNameContext.class);
		}
		public IndexNameContext indexName(int i) {
			return getRuleContext(IndexNameContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode USE() { return getToken(DMLStatementParser.USE, 0); }
		public TerminalNode IGNORE() { return getToken(DMLStatementParser.IGNORE, 0); }
		public TerminalNode FORCE() { return getToken(DMLStatementParser.FORCE, 0); }
		public TerminalNode INDEX() { return getToken(DMLStatementParser.INDEX, 0); }
		public TerminalNode KEY() { return getToken(DMLStatementParser.KEY, 0); }
		public TerminalNode FOR() { return getToken(DMLStatementParser.FOR, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode JOIN() { return getToken(DMLStatementParser.JOIN, 0); }
		public TerminalNode ORDER() { return getToken(DMLStatementParser.ORDER, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public TerminalNode GROUP() { return getToken(DMLStatementParser.GROUP, 0); }
		public IndexHint_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexHint_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIndexHint_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIndexHint_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIndexHint_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexHint_Context indexHint_() throws RecognitionException {
		IndexHint_Context _localctx = new IndexHint_Context(_ctx, getState());
		enterRule(_localctx, 96, RULE_indexHint_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(895);
			_la = _input.LA(1);
			if ( !(_la==USE || _la==FORCE || _la==IGNORE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(896);
			_la = _input.LA(1);
			if ( !(_la==INDEX || _la==KEY) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(905);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FOR) {
				{
				setState(897);
				match(FOR);
				setState(903);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case JOIN:
					{
					setState(898);
					match(JOIN);
					}
					break;
				case ORDER:
					{
					setState(899);
					match(ORDER);
					setState(900);
					match(BY);
					}
					break;
				case GROUP:
					{
					setState(901);
					match(GROUP);
					setState(902);
					match(BY);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
			}

			setState(907);
			match(LP_);
			setState(908);
			indexName();
			setState(913);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(909);
				match(COMMA_);
				setState(910);
				indexName();
				}
				}
				setState(915);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(916);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JoinedTableContext extends ParserRuleContext {
		public TableFactorContext tableFactor() {
			return getRuleContext(TableFactorContext.class,0);
		}
		public TerminalNode JOIN() { return getToken(DMLStatementParser.JOIN, 0); }
		public TerminalNode STRAIGHT_JOIN() { return getToken(DMLStatementParser.STRAIGHT_JOIN, 0); }
		public JoinSpecificationContext joinSpecification() {
			return getRuleContext(JoinSpecificationContext.class,0);
		}
		public TerminalNode INNER() { return getToken(DMLStatementParser.INNER, 0); }
		public TerminalNode CROSS() { return getToken(DMLStatementParser.CROSS, 0); }
		public TerminalNode LEFT() { return getToken(DMLStatementParser.LEFT, 0); }
		public TerminalNode RIGHT() { return getToken(DMLStatementParser.RIGHT, 0); }
		public TerminalNode OUTER() { return getToken(DMLStatementParser.OUTER, 0); }
		public TerminalNode NATURAL() { return getToken(DMLStatementParser.NATURAL, 0); }
		public JoinedTableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_joinedTable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterJoinedTable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitJoinedTable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitJoinedTable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JoinedTableContext joinedTable() throws RecognitionException {
		JoinedTableContext _localctx = new JoinedTableContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_joinedTable);
		int _la;
		try {
			setState(945);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case JOIN:
			case INNER:
			case CROSS:
			case STRAIGHT_JOIN:
				enterOuterAlt(_localctx, 1);
				{
				setState(923);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case JOIN:
				case INNER:
				case CROSS:
					{
					setState(919);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==INNER || _la==CROSS) {
						{
						setState(918);
						_la = _input.LA(1);
						if ( !(_la==INNER || _la==CROSS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
					}

					setState(921);
					match(JOIN);
					}
					break;
				case STRAIGHT_JOIN:
					{
					setState(922);
					match(STRAIGHT_JOIN);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(925);
				tableFactor();
				setState(927);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,111,_ctx) ) {
				case 1:
					{
					setState(926);
					joinSpecification();
					}
					break;
				}
				}
				break;
			case LEFT:
			case RIGHT:
				enterOuterAlt(_localctx, 2);
				{
				setState(929);
				_la = _input.LA(1);
				if ( !(_la==LEFT || _la==RIGHT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(931);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OUTER) {
					{
					setState(930);
					match(OUTER);
					}
				}

				setState(933);
				match(JOIN);
				setState(934);
				tableFactor();
				setState(935);
				joinSpecification();
				}
				break;
			case NATURAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(937);
				match(NATURAL);
				setState(941);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INNER:
					{
					setState(938);
					match(INNER);
					}
					break;
				case LEFT:
				case RIGHT:
					{
					setState(939);
					_la = _input.LA(1);
					if ( !(_la==LEFT || _la==RIGHT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					{
					setState(940);
					match(OUTER);
					}
					}
					break;
				case JOIN:
					break;
				default:
					break;
				}
				setState(943);
				match(JOIN);
				setState(944);
				tableFactor();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JoinSpecificationContext extends ParserRuleContext {
		public TerminalNode ON() { return getToken(DMLStatementParser.ON, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode USING() { return getToken(DMLStatementParser.USING, 0); }
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public JoinSpecificationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_joinSpecification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterJoinSpecification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitJoinSpecification(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitJoinSpecification(this);
			else return visitor.visitChildren(this);
		}
	}

	public final JoinSpecificationContext joinSpecification() throws RecognitionException {
		JoinSpecificationContext _localctx = new JoinSpecificationContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_joinSpecification);
		try {
			setState(951);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ON:
				enterOuterAlt(_localctx, 1);
				{
				setState(947);
				match(ON);
				setState(948);
				expr(0);
				}
				break;
			case USING:
				enterOuterAlt(_localctx, 2);
				{
				setState(949);
				match(USING);
				setState(950);
				columnNames();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereClauseContext extends ParserRuleContext {
		public TerminalNode WHERE() { return getToken(DMLStatementParser.WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public WhereClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whereClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWhereClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWhereClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWhereClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhereClauseContext whereClause() throws RecognitionException {
		WhereClauseContext _localctx = new WhereClauseContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_whereClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(953);
			match(WHERE);
			setState(954);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupByClauseContext extends ParserRuleContext {
		public TerminalNode GROUP() { return getToken(DMLStatementParser.GROUP, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public List<OrderByItemContext> orderByItem() {
			return getRuleContexts(OrderByItemContext.class);
		}
		public OrderByItemContext orderByItem(int i) {
			return getRuleContext(OrderByItemContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode WITH() { return getToken(DMLStatementParser.WITH, 0); }
		public TerminalNode ROLLUP() { return getToken(DMLStatementParser.ROLLUP, 0); }
		public GroupByClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupByClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterGroupByClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitGroupByClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitGroupByClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupByClauseContext groupByClause() throws RecognitionException {
		GroupByClauseContext _localctx = new GroupByClauseContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_groupByClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(956);
			match(GROUP);
			setState(957);
			match(BY);
			setState(958);
			orderByItem();
			setState(963);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(959);
				match(COMMA_);
				setState(960);
				orderByItem();
				}
				}
				setState(965);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(968);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WITH) {
				{
				setState(966);
				match(WITH);
				setState(967);
				match(ROLLUP);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HavingClauseContext extends ParserRuleContext {
		public TerminalNode HAVING() { return getToken(DMLStatementParser.HAVING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public HavingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_havingClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHavingClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHavingClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHavingClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HavingClauseContext havingClause() throws RecognitionException {
		HavingClauseContext _localctx = new HavingClauseContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_havingClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(970);
			match(HAVING);
			setState(971);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitClauseContext extends ParserRuleContext {
		public TerminalNode LIMIT() { return getToken(DMLStatementParser.LIMIT, 0); }
		public LimitRowCountContext limitRowCount() {
			return getRuleContext(LimitRowCountContext.class,0);
		}
		public TerminalNode OFFSET() { return getToken(DMLStatementParser.OFFSET, 0); }
		public LimitOffsetContext limitOffset() {
			return getRuleContext(LimitOffsetContext.class,0);
		}
		public TerminalNode COMMA_() { return getToken(DMLStatementParser.COMMA_, 0); }
		public LimitClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limitClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLimitClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLimitClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLimitClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LimitClauseContext limitClause() throws RecognitionException {
		LimitClauseContext _localctx = new LimitClauseContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_limitClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(973);
			match(LIMIT);
			setState(984);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,119,_ctx) ) {
			case 1:
				{
				setState(977);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,118,_ctx) ) {
				case 1:
					{
					setState(974);
					limitOffset();
					setState(975);
					match(COMMA_);
					}
					break;
				}
				setState(979);
				limitRowCount();
				}
				break;
			case 2:
				{
				setState(980);
				limitRowCount();
				setState(981);
				match(OFFSET);
				setState(982);
				limitOffset();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitRowCountContext extends ParserRuleContext {
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public ParameterMarkerContext parameterMarker() {
			return getRuleContext(ParameterMarkerContext.class,0);
		}
		public LimitRowCountContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limitRowCount; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLimitRowCount(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLimitRowCount(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLimitRowCount(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LimitRowCountContext limitRowCount() throws RecognitionException {
		LimitRowCountContext _localctx = new LimitRowCountContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_limitRowCount);
		try {
			setState(988);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MINUS_:
			case NUMBER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(986);
				numberLiterals();
				}
				break;
			case QUESTION_:
				enterOuterAlt(_localctx, 2);
				{
				setState(987);
				parameterMarker();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitOffsetContext extends ParserRuleContext {
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public ParameterMarkerContext parameterMarker() {
			return getRuleContext(ParameterMarkerContext.class,0);
		}
		public LimitOffsetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limitOffset; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLimitOffset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLimitOffset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLimitOffset(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LimitOffsetContext limitOffset() throws RecognitionException {
		LimitOffsetContext _localctx = new LimitOffsetContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_limitOffset);
		try {
			setState(992);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MINUS_:
			case NUMBER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(990);
				numberLiterals();
				}
				break;
			case QUESTION_:
				enterOuterAlt(_localctx, 2);
				{
				setState(991);
				parameterMarker();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowClause_Context extends ParserRuleContext {
		public TerminalNode WINDOW() { return getToken(DMLStatementParser.WINDOW, 0); }
		public List<WindowItem_Context> windowItem_() {
			return getRuleContexts(WindowItem_Context.class);
		}
		public WindowItem_Context windowItem_(int i) {
			return getRuleContext(WindowItem_Context.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public WindowClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWindowClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWindowClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWindowClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowClause_Context windowClause_() throws RecognitionException {
		WindowClause_Context _localctx = new WindowClause_Context(_ctx, getState());
		enterRule(_localctx, 114, RULE_windowClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(994);
			match(WINDOW);
			setState(995);
			windowItem_();
			setState(1000);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(996);
				match(COMMA_);
				setState(997);
				windowItem_();
				}
				}
				setState(1002);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowItem_Context extends ParserRuleContext {
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public WindowSpecification_Context windowSpecification_() {
			return getRuleContext(WindowSpecification_Context.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public WindowItem_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowItem_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWindowItem_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWindowItem_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWindowItem_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowItem_Context windowItem_() throws RecognitionException {
		WindowItem_Context _localctx = new WindowItem_Context(_ctx, getState());
		enterRule(_localctx, 116, RULE_windowItem_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1003);
			ignoredIdentifier_();
			setState(1004);
			match(AS);
			setState(1005);
			match(LP_);
			setState(1006);
			windowSpecification_();
			setState(1007);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubqueryContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public UnionClauseContext unionClause() {
			return getRuleContext(UnionClauseContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public SubqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSubquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSubquery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSubquery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubqueryContext subquery() throws RecognitionException {
		SubqueryContext _localctx = new SubqueryContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_subquery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1009);
			match(LP_);
			setState(1010);
			unionClause();
			setState(1011);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectLinesInto_Context extends ParserRuleContext {
		public TerminalNode STARTING() { return getToken(DMLStatementParser.STARTING, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public TerminalNode TERMINATED() { return getToken(DMLStatementParser.TERMINATED, 0); }
		public SelectLinesInto_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectLinesInto_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSelectLinesInto_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSelectLinesInto_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSelectLinesInto_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectLinesInto_Context selectLinesInto_() throws RecognitionException {
		SelectLinesInto_Context _localctx = new SelectLinesInto_Context(_ctx, getState());
		enterRule(_localctx, 120, RULE_selectLinesInto_);
		try {
			setState(1019);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STARTING:
				enterOuterAlt(_localctx, 1);
				{
				setState(1013);
				match(STARTING);
				setState(1014);
				match(BY);
				setState(1015);
				match(STRING_);
				}
				break;
			case TERMINATED:
				enterOuterAlt(_localctx, 2);
				{
				setState(1016);
				match(TERMINATED);
				setState(1017);
				match(BY);
				setState(1018);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectFieldsInto_Context extends ParserRuleContext {
		public TerminalNode TERMINATED() { return getToken(DMLStatementParser.TERMINATED, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public TerminalNode ENCLOSED() { return getToken(DMLStatementParser.ENCLOSED, 0); }
		public TerminalNode OPTIONALLY() { return getToken(DMLStatementParser.OPTIONALLY, 0); }
		public TerminalNode ESCAPED() { return getToken(DMLStatementParser.ESCAPED, 0); }
		public SelectFieldsInto_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectFieldsInto_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSelectFieldsInto_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSelectFieldsInto_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSelectFieldsInto_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectFieldsInto_Context selectFieldsInto_() throws RecognitionException {
		SelectFieldsInto_Context _localctx = new SelectFieldsInto_Context(_ctx, getState());
		enterRule(_localctx, 122, RULE_selectFieldsInto_);
		int _la;
		try {
			setState(1033);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TERMINATED:
				enterOuterAlt(_localctx, 1);
				{
				setState(1021);
				match(TERMINATED);
				setState(1022);
				match(BY);
				setState(1023);
				match(STRING_);
				}
				break;
			case OPTIONALLY:
			case ENCLOSED:
				enterOuterAlt(_localctx, 2);
				{
				setState(1025);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPTIONALLY) {
					{
					setState(1024);
					match(OPTIONALLY);
					}
				}

				setState(1027);
				match(ENCLOSED);
				setState(1028);
				match(BY);
				setState(1029);
				match(STRING_);
				}
				break;
			case ESCAPED:
				enterOuterAlt(_localctx, 3);
				{
				setState(1030);
				match(ESCAPED);
				setState(1031);
				match(BY);
				setState(1032);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectIntoExpression_Context extends ParserRuleContext {
		public TerminalNode INTO() { return getToken(DMLStatementParser.INTO, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode DUMPFILE() { return getToken(DMLStatementParser.DUMPFILE, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public TerminalNode OUTFILE() { return getToken(DMLStatementParser.OUTFILE, 0); }
		public TerminalNode CHARACTER() { return getToken(DMLStatementParser.CHARACTER, 0); }
		public TerminalNode SET() { return getToken(DMLStatementParser.SET, 0); }
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public TerminalNode LINES() { return getToken(DMLStatementParser.LINES, 0); }
		public TerminalNode FIELDS() { return getToken(DMLStatementParser.FIELDS, 0); }
		public TerminalNode COLUMNS() { return getToken(DMLStatementParser.COLUMNS, 0); }
		public List<SelectFieldsInto_Context> selectFieldsInto_() {
			return getRuleContexts(SelectFieldsInto_Context.class);
		}
		public SelectFieldsInto_Context selectFieldsInto_(int i) {
			return getRuleContext(SelectFieldsInto_Context.class,i);
		}
		public List<SelectLinesInto_Context> selectLinesInto_() {
			return getRuleContexts(SelectLinesInto_Context.class);
		}
		public SelectLinesInto_Context selectLinesInto_(int i) {
			return getRuleContext(SelectLinesInto_Context.class,i);
		}
		public SelectIntoExpression_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectIntoExpression_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSelectIntoExpression_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSelectIntoExpression_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSelectIntoExpression_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectIntoExpression_Context selectIntoExpression_() throws RecognitionException {
		SelectIntoExpression_Context _localctx = new SelectIntoExpression_Context(_ctx, getState());
		enterRule(_localctx, 124, RULE_selectIntoExpression_);
		int _la;
		try {
			setState(1071);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,132,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1035);
				match(INTO);
				setState(1036);
				identifier();
				setState(1041);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1037);
					match(COMMA_);
					setState(1038);
					identifier();
					}
					}
					setState(1043);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1044);
				match(INTO);
				setState(1045);
				match(DUMPFILE);
				setState(1046);
				match(STRING_);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(1047);
				match(INTO);
				setState(1048);
				match(OUTFILE);
				setState(1049);
				match(STRING_);
				setState(1053);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CHARACTER) {
					{
					setState(1050);
					match(CHARACTER);
					setState(1051);
					match(SET);
					setState(1052);
					match(IDENTIFIER_);
					}
				}

				setState(1061);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLUMNS || _la==FIELDS) {
					{
					setState(1055);
					_la = _input.LA(1);
					if ( !(_la==COLUMNS || _la==FIELDS) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(1057); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1056);
						selectFieldsInto_();
						}
						}
						setState(1059); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( ((((_la - 469)) & ~0x3f) == 0 && ((1L << (_la - 469)) & ((1L << (TERMINATED - 469)) | (1L << (OPTIONALLY - 469)) | (1L << (ENCLOSED - 469)) | (1L << (ESCAPED - 469)))) != 0) );
					}
				}

				setState(1069);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LINES) {
					{
					setState(1063);
					match(LINES);
					setState(1065); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1064);
						selectLinesInto_();
						}
						}
						setState(1067); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==STARTING || _la==TERMINATED );
					}
				}

				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LockClauseContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(DMLStatementParser.FOR, 0); }
		public TerminalNode UPDATE() { return getToken(DMLStatementParser.UPDATE, 0); }
		public TerminalNode LOCK() { return getToken(DMLStatementParser.LOCK, 0); }
		public TerminalNode IN() { return getToken(DMLStatementParser.IN, 0); }
		public TerminalNode SHARE() { return getToken(DMLStatementParser.SHARE, 0); }
		public TerminalNode MODE() { return getToken(DMLStatementParser.MODE, 0); }
		public LockClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lockClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLockClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLockClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLockClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LockClauseContext lockClause() throws RecognitionException {
		LockClauseContext _localctx = new LockClauseContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_lockClause);
		try {
			setState(1079);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FOR:
				enterOuterAlt(_localctx, 1);
				{
				setState(1073);
				match(FOR);
				setState(1074);
				match(UPDATE);
				}
				break;
			case LOCK:
				enterOuterAlt(_localctx, 2);
				{
				setState(1075);
				match(LOCK);
				setState(1076);
				match(IN);
				setState(1077);
				match(SHARE);
				setState(1078);
				match(MODE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterMarkerContext extends ParserRuleContext {
		public TerminalNode QUESTION_() { return getToken(DMLStatementParser.QUESTION_, 0); }
		public ParameterMarkerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterMarker; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterParameterMarker(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitParameterMarker(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitParameterMarker(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterMarkerContext parameterMarker() throws RecognitionException {
		ParameterMarkerContext _localctx = new ParameterMarkerContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_parameterMarker);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1081);
			match(QUESTION_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralsContext extends ParserRuleContext {
		public StringLiteralsContext stringLiterals() {
			return getRuleContext(StringLiteralsContext.class,0);
		}
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public DateTimeLiteralsContext dateTimeLiterals() {
			return getRuleContext(DateTimeLiteralsContext.class,0);
		}
		public HexadecimalLiteralsContext hexadecimalLiterals() {
			return getRuleContext(HexadecimalLiteralsContext.class,0);
		}
		public BitValueLiteralsContext bitValueLiterals() {
			return getRuleContext(BitValueLiteralsContext.class,0);
		}
		public BooleanLiteralsContext booleanLiterals() {
			return getRuleContext(BooleanLiteralsContext.class,0);
		}
		public NullValueLiteralsContext nullValueLiterals() {
			return getRuleContext(NullValueLiteralsContext.class,0);
		}
		public LiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LiteralsContext literals() throws RecognitionException {
		LiteralsContext _localctx = new LiteralsContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_literals);
		try {
			setState(1090);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,134,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1083);
				stringLiterals();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1084);
				numberLiterals();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1085);
				dateTimeLiterals();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1086);
				hexadecimalLiterals();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1087);
				bitValueLiterals();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1088);
				booleanLiterals();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1089);
				nullValueLiterals();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralsContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public CharacterSetName_Context characterSetName_() {
			return getRuleContext(CharacterSetName_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public StringLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterStringLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitStringLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitStringLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StringLiteralsContext stringLiterals() throws RecognitionException {
		StringLiteralsContext _localctx = new StringLiteralsContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_stringLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1093);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER_) {
				{
				setState(1092);
				characterSetName_();
				}
			}

			setState(1095);
			match(STRING_);
			setState(1097);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,136,_ctx) ) {
			case 1:
				{
				setState(1096);
				collateClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberLiteralsContext extends ParserRuleContext {
		public TerminalNode NUMBER_() { return getToken(DMLStatementParser.NUMBER_, 0); }
		public TerminalNode MINUS_() { return getToken(DMLStatementParser.MINUS_, 0); }
		public NumberLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numberLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterNumberLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitNumberLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitNumberLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberLiteralsContext numberLiterals() throws RecognitionException {
		NumberLiteralsContext _localctx = new NumberLiteralsContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_numberLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1100);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==MINUS_) {
				{
				setState(1099);
				match(MINUS_);
				}
			}

			setState(1102);
			match(NUMBER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateTimeLiteralsContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public TerminalNode DATE() { return getToken(DMLStatementParser.DATE, 0); }
		public TerminalNode TIME() { return getToken(DMLStatementParser.TIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(DMLStatementParser.TIMESTAMP, 0); }
		public TerminalNode LBE_() { return getToken(DMLStatementParser.LBE_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode RBE_() { return getToken(DMLStatementParser.RBE_, 0); }
		public DateTimeLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateTimeLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDateTimeLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDateTimeLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDateTimeLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateTimeLiteralsContext dateTimeLiterals() throws RecognitionException {
		DateTimeLiteralsContext _localctx = new DateTimeLiteralsContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_dateTimeLiterals);
		int _la;
		try {
			setState(1111);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DATE:
			case TIME:
			case TIMESTAMP:
				enterOuterAlt(_localctx, 1);
				{
				setState(1104);
				_la = _input.LA(1);
				if ( !(((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (DATE - 128)) | (1L << (TIME - 128)) | (1L << (TIMESTAMP - 128)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1105);
				match(STRING_);
				}
				break;
			case LBE_:
				enterOuterAlt(_localctx, 2);
				{
				setState(1106);
				match(LBE_);
				setState(1107);
				identifier();
				setState(1108);
				match(STRING_);
				setState(1109);
				match(RBE_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HexadecimalLiteralsContext extends ParserRuleContext {
		public TerminalNode HEX_DIGIT_() { return getToken(DMLStatementParser.HEX_DIGIT_, 0); }
		public CharacterSetName_Context characterSetName_() {
			return getRuleContext(CharacterSetName_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public HexadecimalLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hexadecimalLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHexadecimalLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHexadecimalLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHexadecimalLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HexadecimalLiteralsContext hexadecimalLiterals() throws RecognitionException {
		HexadecimalLiteralsContext _localctx = new HexadecimalLiteralsContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_hexadecimalLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1114);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER_) {
				{
				setState(1113);
				characterSetName_();
				}
			}

			setState(1116);
			match(HEX_DIGIT_);
			setState(1118);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,140,_ctx) ) {
			case 1:
				{
				setState(1117);
				collateClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitValueLiteralsContext extends ParserRuleContext {
		public TerminalNode BIT_NUM_() { return getToken(DMLStatementParser.BIT_NUM_, 0); }
		public CharacterSetName_Context characterSetName_() {
			return getRuleContext(CharacterSetName_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public BitValueLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitValueLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterBitValueLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitBitValueLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitBitValueLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BitValueLiteralsContext bitValueLiterals() throws RecognitionException {
		BitValueLiteralsContext _localctx = new BitValueLiteralsContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_bitValueLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1121);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER_) {
				{
				setState(1120);
				characterSetName_();
				}
			}

			setState(1123);
			match(BIT_NUM_);
			setState(1125);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,142,_ctx) ) {
			case 1:
				{
				setState(1124);
				collateClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanLiteralsContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(DMLStatementParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(DMLStatementParser.FALSE, 0); }
		public BooleanLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterBooleanLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitBooleanLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitBooleanLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanLiteralsContext booleanLiterals() throws RecognitionException {
		BooleanLiteralsContext _localctx = new BooleanLiteralsContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_booleanLiterals);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1127);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NullValueLiteralsContext extends ParserRuleContext {
		public TerminalNode NULL() { return getToken(DMLStatementParser.NULL, 0); }
		public NullValueLiteralsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullValueLiterals; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterNullValueLiterals(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitNullValueLiterals(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitNullValueLiterals(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullValueLiteralsContext nullValueLiterals() throws RecognitionException {
		NullValueLiteralsContext _localctx = new NullValueLiteralsContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_nullValueLiterals);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1129);
			match(NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharacterSetName_Context extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public CharacterSetName_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_characterSetName_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCharacterSetName_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCharacterSetName_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCharacterSetName_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharacterSetName_Context characterSetName_() throws RecognitionException {
		CharacterSetName_Context _localctx = new CharacterSetName_Context(_ctx, getState());
		enterRule(_localctx, 146, RULE_characterSetName_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1131);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollationName_Context extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public CollationName_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collationName_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCollationName_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCollationName_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCollationName_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollationName_Context collationName_() throws RecognitionException {
		CollationName_Context _localctx = new CollationName_Context(_ctx, getState());
		enterRule(_localctx, 148, RULE_collationName_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1133);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public UnreservedWordContext unreservedWord() {
			return getRuleContext(UnreservedWordContext.class,0);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_identifier);
		try {
			setState(1137);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(1135);
				match(IDENTIFIER_);
				}
				break;
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
				enterOuterAlt(_localctx, 2);
				{
				setState(1136);
				unreservedWord();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnreservedWordContext extends ParserRuleContext {
		public TerminalNode ACCOUNT() { return getToken(DMLStatementParser.ACCOUNT, 0); }
		public TerminalNode ACTION() { return getToken(DMLStatementParser.ACTION, 0); }
		public TerminalNode AFTER() { return getToken(DMLStatementParser.AFTER, 0); }
		public TerminalNode ALGORITHM() { return getToken(DMLStatementParser.ALGORITHM, 0); }
		public TerminalNode ALWAYS() { return getToken(DMLStatementParser.ALWAYS, 0); }
		public TerminalNode ANY() { return getToken(DMLStatementParser.ANY, 0); }
		public TerminalNode AUTO_INCREMENT() { return getToken(DMLStatementParser.AUTO_INCREMENT, 0); }
		public TerminalNode AVG_ROW_LENGTH() { return getToken(DMLStatementParser.AVG_ROW_LENGTH, 0); }
		public TerminalNode BEGIN() { return getToken(DMLStatementParser.BEGIN, 0); }
		public TerminalNode BTREE() { return getToken(DMLStatementParser.BTREE, 0); }
		public TerminalNode CHAIN() { return getToken(DMLStatementParser.CHAIN, 0); }
		public TerminalNode CHARSET() { return getToken(DMLStatementParser.CHARSET, 0); }
		public TerminalNode CHECKSUM() { return getToken(DMLStatementParser.CHECKSUM, 0); }
		public TerminalNode CIPHER() { return getToken(DMLStatementParser.CIPHER, 0); }
		public TerminalNode CLIENT() { return getToken(DMLStatementParser.CLIENT, 0); }
		public TerminalNode COALESCE() { return getToken(DMLStatementParser.COALESCE, 0); }
		public TerminalNode COLUMNS() { return getToken(DMLStatementParser.COLUMNS, 0); }
		public TerminalNode COLUMN_FORMAT() { return getToken(DMLStatementParser.COLUMN_FORMAT, 0); }
		public TerminalNode COMMENT() { return getToken(DMLStatementParser.COMMENT, 0); }
		public TerminalNode COMMIT() { return getToken(DMLStatementParser.COMMIT, 0); }
		public TerminalNode COMMITTED() { return getToken(DMLStatementParser.COMMITTED, 0); }
		public TerminalNode COMPACT() { return getToken(DMLStatementParser.COMPACT, 0); }
		public TerminalNode COMPRESSED() { return getToken(DMLStatementParser.COMPRESSED, 0); }
		public TerminalNode COMPRESSION() { return getToken(DMLStatementParser.COMPRESSION, 0); }
		public TerminalNode CONNECTION() { return getToken(DMLStatementParser.CONNECTION, 0); }
		public TerminalNode CONSISTENT() { return getToken(DMLStatementParser.CONSISTENT, 0); }
		public TerminalNode CURRENT() { return getToken(DMLStatementParser.CURRENT, 0); }
		public TerminalNode DATA() { return getToken(DMLStatementParser.DATA, 0); }
		public TerminalNode DATE() { return getToken(DMLStatementParser.DATE, 0); }
		public TerminalNode DELAY_KEY_WRITE() { return getToken(DMLStatementParser.DELAY_KEY_WRITE, 0); }
		public TerminalNode DISABLE() { return getToken(DMLStatementParser.DISABLE, 0); }
		public TerminalNode DISCARD() { return getToken(DMLStatementParser.DISCARD, 0); }
		public TerminalNode DISK() { return getToken(DMLStatementParser.DISK, 0); }
		public TerminalNode DUPLICATE() { return getToken(DMLStatementParser.DUPLICATE, 0); }
		public TerminalNode ENABLE() { return getToken(DMLStatementParser.ENABLE, 0); }
		public TerminalNode ENCRYPTION() { return getToken(DMLStatementParser.ENCRYPTION, 0); }
		public TerminalNode ENFORCED() { return getToken(DMLStatementParser.ENFORCED, 0); }
		public TerminalNode END() { return getToken(DMLStatementParser.END, 0); }
		public TerminalNode ENGINE() { return getToken(DMLStatementParser.ENGINE, 0); }
		public TerminalNode ESCAPE() { return getToken(DMLStatementParser.ESCAPE, 0); }
		public TerminalNode EVENT() { return getToken(DMLStatementParser.EVENT, 0); }
		public TerminalNode EXCHANGE() { return getToken(DMLStatementParser.EXCHANGE, 0); }
		public TerminalNode EXECUTE() { return getToken(DMLStatementParser.EXECUTE, 0); }
		public TerminalNode FILE() { return getToken(DMLStatementParser.FILE, 0); }
		public TerminalNode FIRST() { return getToken(DMLStatementParser.FIRST, 0); }
		public TerminalNode FIXED() { return getToken(DMLStatementParser.FIXED, 0); }
		public TerminalNode FOLLOWING() { return getToken(DMLStatementParser.FOLLOWING, 0); }
		public TerminalNode GLOBAL() { return getToken(DMLStatementParser.GLOBAL, 0); }
		public TerminalNode HASH() { return getToken(DMLStatementParser.HASH, 0); }
		public TerminalNode IMPORT_() { return getToken(DMLStatementParser.IMPORT_, 0); }
		public TerminalNode INSERT_METHOD() { return getToken(DMLStatementParser.INSERT_METHOD, 0); }
		public TerminalNode INVISIBLE() { return getToken(DMLStatementParser.INVISIBLE, 0); }
		public TerminalNode KEY_BLOCK_SIZE() { return getToken(DMLStatementParser.KEY_BLOCK_SIZE, 0); }
		public TerminalNode LAST() { return getToken(DMLStatementParser.LAST, 0); }
		public TerminalNode LESS() { return getToken(DMLStatementParser.LESS, 0); }
		public TerminalNode LEVEL() { return getToken(DMLStatementParser.LEVEL, 0); }
		public TerminalNode MAX_ROWS() { return getToken(DMLStatementParser.MAX_ROWS, 0); }
		public TerminalNode MEMORY() { return getToken(DMLStatementParser.MEMORY, 0); }
		public TerminalNode MIN_ROWS() { return getToken(DMLStatementParser.MIN_ROWS, 0); }
		public TerminalNode MODIFY() { return getToken(DMLStatementParser.MODIFY, 0); }
		public TerminalNode NO() { return getToken(DMLStatementParser.NO, 0); }
		public TerminalNode NONE() { return getToken(DMLStatementParser.NONE, 0); }
		public TerminalNode OFFSET() { return getToken(DMLStatementParser.OFFSET, 0); }
		public TerminalNode PACK_KEYS() { return getToken(DMLStatementParser.PACK_KEYS, 0); }
		public TerminalNode PARSER() { return getToken(DMLStatementParser.PARSER, 0); }
		public TerminalNode PARTIAL() { return getToken(DMLStatementParser.PARTIAL, 0); }
		public TerminalNode PARTITIONING() { return getToken(DMLStatementParser.PARTITIONING, 0); }
		public TerminalNode PASSWORD() { return getToken(DMLStatementParser.PASSWORD, 0); }
		public TerminalNode PERSIST() { return getToken(DMLStatementParser.PERSIST, 0); }
		public TerminalNode PERSIST_ONLY() { return getToken(DMLStatementParser.PERSIST_ONLY, 0); }
		public TerminalNode PRECEDING() { return getToken(DMLStatementParser.PRECEDING, 0); }
		public TerminalNode PRIVILEGES() { return getToken(DMLStatementParser.PRIVILEGES, 0); }
		public TerminalNode PROCESS() { return getToken(DMLStatementParser.PROCESS, 0); }
		public TerminalNode PROXY() { return getToken(DMLStatementParser.PROXY, 0); }
		public TerminalNode QUICK() { return getToken(DMLStatementParser.QUICK, 0); }
		public TerminalNode REBUILD() { return getToken(DMLStatementParser.REBUILD, 0); }
		public TerminalNode REDUNDANT() { return getToken(DMLStatementParser.REDUNDANT, 0); }
		public TerminalNode RELOAD() { return getToken(DMLStatementParser.RELOAD, 0); }
		public TerminalNode REMOVE() { return getToken(DMLStatementParser.REMOVE, 0); }
		public TerminalNode REORGANIZE() { return getToken(DMLStatementParser.REORGANIZE, 0); }
		public TerminalNode REPAIR() { return getToken(DMLStatementParser.REPAIR, 0); }
		public TerminalNode REVERSE() { return getToken(DMLStatementParser.REVERSE, 0); }
		public TerminalNode ROLLBACK() { return getToken(DMLStatementParser.ROLLBACK, 0); }
		public TerminalNode ROLLUP() { return getToken(DMLStatementParser.ROLLUP, 0); }
		public TerminalNode ROW_FORMAT() { return getToken(DMLStatementParser.ROW_FORMAT, 0); }
		public TerminalNode SAVEPOINT() { return getToken(DMLStatementParser.SAVEPOINT, 0); }
		public TerminalNode SESSION() { return getToken(DMLStatementParser.SESSION, 0); }
		public TerminalNode SHUTDOWN() { return getToken(DMLStatementParser.SHUTDOWN, 0); }
		public TerminalNode SIMPLE() { return getToken(DMLStatementParser.SIMPLE, 0); }
		public TerminalNode SLAVE() { return getToken(DMLStatementParser.SLAVE, 0); }
		public TerminalNode SOUNDS() { return getToken(DMLStatementParser.SOUNDS, 0); }
		public TerminalNode SQL_BIG_RESULT() { return getToken(DMLStatementParser.SQL_BIG_RESULT, 0); }
		public TerminalNode SQL_BUFFER_RESULT() { return getToken(DMLStatementParser.SQL_BUFFER_RESULT, 0); }
		public TerminalNode SQL_CACHE() { return getToken(DMLStatementParser.SQL_CACHE, 0); }
		public TerminalNode SQL_NO_CACHE() { return getToken(DMLStatementParser.SQL_NO_CACHE, 0); }
		public TerminalNode START() { return getToken(DMLStatementParser.START, 0); }
		public TerminalNode STATS_AUTO_RECALC() { return getToken(DMLStatementParser.STATS_AUTO_RECALC, 0); }
		public TerminalNode STATS_PERSISTENT() { return getToken(DMLStatementParser.STATS_PERSISTENT, 0); }
		public TerminalNode STATS_SAMPLE_PAGES() { return getToken(DMLStatementParser.STATS_SAMPLE_PAGES, 0); }
		public TerminalNode STORAGE() { return getToken(DMLStatementParser.STORAGE, 0); }
		public TerminalNode SUBPARTITION() { return getToken(DMLStatementParser.SUBPARTITION, 0); }
		public TerminalNode SUPER() { return getToken(DMLStatementParser.SUPER, 0); }
		public TerminalNode TABLES() { return getToken(DMLStatementParser.TABLES, 0); }
		public TerminalNode TABLESPACE() { return getToken(DMLStatementParser.TABLESPACE, 0); }
		public TerminalNode TEMPORARY() { return getToken(DMLStatementParser.TEMPORARY, 0); }
		public TerminalNode THAN() { return getToken(DMLStatementParser.THAN, 0); }
		public TerminalNode TIME() { return getToken(DMLStatementParser.TIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(DMLStatementParser.TIMESTAMP, 0); }
		public TerminalNode TRANSACTION() { return getToken(DMLStatementParser.TRANSACTION, 0); }
		public TerminalNode TRUNCATE() { return getToken(DMLStatementParser.TRUNCATE, 0); }
		public TerminalNode UNBOUNDED() { return getToken(DMLStatementParser.UNBOUNDED, 0); }
		public TerminalNode UNKNOWN() { return getToken(DMLStatementParser.UNKNOWN, 0); }
		public TerminalNode UPGRADE() { return getToken(DMLStatementParser.UPGRADE, 0); }
		public TerminalNode VALIDATION() { return getToken(DMLStatementParser.VALIDATION, 0); }
		public TerminalNode VALUE() { return getToken(DMLStatementParser.VALUE, 0); }
		public TerminalNode VIEW() { return getToken(DMLStatementParser.VIEW, 0); }
		public TerminalNode VISIBLE() { return getToken(DMLStatementParser.VISIBLE, 0); }
		public TerminalNode WEIGHT_STRING() { return getToken(DMLStatementParser.WEIGHT_STRING, 0); }
		public TerminalNode WITHOUT() { return getToken(DMLStatementParser.WITHOUT, 0); }
		public TerminalNode MICROSECOND() { return getToken(DMLStatementParser.MICROSECOND, 0); }
		public TerminalNode SECOND() { return getToken(DMLStatementParser.SECOND, 0); }
		public TerminalNode MINUTE() { return getToken(DMLStatementParser.MINUTE, 0); }
		public TerminalNode HOUR() { return getToken(DMLStatementParser.HOUR, 0); }
		public TerminalNode DAY() { return getToken(DMLStatementParser.DAY, 0); }
		public TerminalNode WEEK() { return getToken(DMLStatementParser.WEEK, 0); }
		public TerminalNode MONTH() { return getToken(DMLStatementParser.MONTH, 0); }
		public TerminalNode QUARTER() { return getToken(DMLStatementParser.QUARTER, 0); }
		public TerminalNode YEAR() { return getToken(DMLStatementParser.YEAR, 0); }
		public TerminalNode AGAINST() { return getToken(DMLStatementParser.AGAINST, 0); }
		public TerminalNode LANGUAGE() { return getToken(DMLStatementParser.LANGUAGE, 0); }
		public TerminalNode MODE() { return getToken(DMLStatementParser.MODE, 0); }
		public TerminalNode QUERY() { return getToken(DMLStatementParser.QUERY, 0); }
		public TerminalNode EXPANSION() { return getToken(DMLStatementParser.EXPANSION, 0); }
		public TerminalNode BOOLEAN() { return getToken(DMLStatementParser.BOOLEAN, 0); }
		public TerminalNode MAX() { return getToken(DMLStatementParser.MAX, 0); }
		public TerminalNode MIN() { return getToken(DMLStatementParser.MIN, 0); }
		public TerminalNode SUM() { return getToken(DMLStatementParser.SUM, 0); }
		public TerminalNode COUNT() { return getToken(DMLStatementParser.COUNT, 0); }
		public TerminalNode AVG() { return getToken(DMLStatementParser.AVG, 0); }
		public TerminalNode BIT_AND() { return getToken(DMLStatementParser.BIT_AND, 0); }
		public TerminalNode BIT_OR() { return getToken(DMLStatementParser.BIT_OR, 0); }
		public TerminalNode BIT_XOR() { return getToken(DMLStatementParser.BIT_XOR, 0); }
		public TerminalNode GROUP_CONCAT() { return getToken(DMLStatementParser.GROUP_CONCAT, 0); }
		public TerminalNode JSON_ARRAYAGG() { return getToken(DMLStatementParser.JSON_ARRAYAGG, 0); }
		public TerminalNode JSON_OBJECTAGG() { return getToken(DMLStatementParser.JSON_OBJECTAGG, 0); }
		public TerminalNode STD() { return getToken(DMLStatementParser.STD, 0); }
		public TerminalNode STDDEV() { return getToken(DMLStatementParser.STDDEV, 0); }
		public TerminalNode STDDEV_POP() { return getToken(DMLStatementParser.STDDEV_POP, 0); }
		public TerminalNode STDDEV_SAMP() { return getToken(DMLStatementParser.STDDEV_SAMP, 0); }
		public TerminalNode VAR_POP() { return getToken(DMLStatementParser.VAR_POP, 0); }
		public TerminalNode VAR_SAMP() { return getToken(DMLStatementParser.VAR_SAMP, 0); }
		public TerminalNode VARIANCE() { return getToken(DMLStatementParser.VARIANCE, 0); }
		public TerminalNode EXTENDED() { return getToken(DMLStatementParser.EXTENDED, 0); }
		public TerminalNode STATUS() { return getToken(DMLStatementParser.STATUS, 0); }
		public TerminalNode FIELDS() { return getToken(DMLStatementParser.FIELDS, 0); }
		public TerminalNode INDEXES() { return getToken(DMLStatementParser.INDEXES, 0); }
		public TerminalNode USER() { return getToken(DMLStatementParser.USER, 0); }
		public TerminalNode ROLE() { return getToken(DMLStatementParser.ROLE, 0); }
		public TerminalNode OJ() { return getToken(DMLStatementParser.OJ, 0); }
		public TerminalNode AUTOCOMMIT() { return getToken(DMLStatementParser.AUTOCOMMIT, 0); }
		public TerminalNode OFF() { return getToken(DMLStatementParser.OFF, 0); }
		public TerminalNode ROTATE() { return getToken(DMLStatementParser.ROTATE, 0); }
		public TerminalNode INSTANCE() { return getToken(DMLStatementParser.INSTANCE, 0); }
		public TerminalNode MASTER() { return getToken(DMLStatementParser.MASTER, 0); }
		public TerminalNode BINLOG() { return getToken(DMLStatementParser.BINLOG, 0); }
		public TerminalNode ERROR() { return getToken(DMLStatementParser.ERROR, 0); }
		public TerminalNode SCHEDULE() { return getToken(DMLStatementParser.SCHEDULE, 0); }
		public TerminalNode COMPLETION() { return getToken(DMLStatementParser.COMPLETION, 0); }
		public TerminalNode DO() { return getToken(DMLStatementParser.DO, 0); }
		public TerminalNode DEFINER() { return getToken(DMLStatementParser.DEFINER, 0); }
		public TerminalNode EVERY() { return getToken(DMLStatementParser.EVERY, 0); }
		public TerminalNode HOST() { return getToken(DMLStatementParser.HOST, 0); }
		public TerminalNode SOCKET() { return getToken(DMLStatementParser.SOCKET, 0); }
		public TerminalNode OWNER() { return getToken(DMLStatementParser.OWNER, 0); }
		public TerminalNode PORT() { return getToken(DMLStatementParser.PORT, 0); }
		public TerminalNode RETURNS() { return getToken(DMLStatementParser.RETURNS, 0); }
		public TerminalNode CONTAINS() { return getToken(DMLStatementParser.CONTAINS, 0); }
		public TerminalNode SECURITY() { return getToken(DMLStatementParser.SECURITY, 0); }
		public TerminalNode INVOKER() { return getToken(DMLStatementParser.INVOKER, 0); }
		public TerminalNode UNDEFINED() { return getToken(DMLStatementParser.UNDEFINED, 0); }
		public TerminalNode MERGE() { return getToken(DMLStatementParser.MERGE, 0); }
		public TerminalNode TEMPTABLE() { return getToken(DMLStatementParser.TEMPTABLE, 0); }
		public TerminalNode CASCADED() { return getToken(DMLStatementParser.CASCADED, 0); }
		public TerminalNode LOCAL() { return getToken(DMLStatementParser.LOCAL, 0); }
		public TerminalNode SERVER() { return getToken(DMLStatementParser.SERVER, 0); }
		public TerminalNode WRAPPER() { return getToken(DMLStatementParser.WRAPPER, 0); }
		public TerminalNode OPTIONS() { return getToken(DMLStatementParser.OPTIONS, 0); }
		public TerminalNode DATAFILE() { return getToken(DMLStatementParser.DATAFILE, 0); }
		public TerminalNode FILE_BLOCK_SIZE() { return getToken(DMLStatementParser.FILE_BLOCK_SIZE, 0); }
		public TerminalNode EXTENT_SIZE() { return getToken(DMLStatementParser.EXTENT_SIZE, 0); }
		public TerminalNode INITIAL_SIZE() { return getToken(DMLStatementParser.INITIAL_SIZE, 0); }
		public TerminalNode AUTOEXTEND_SIZE() { return getToken(DMLStatementParser.AUTOEXTEND_SIZE, 0); }
		public TerminalNode MAX_SIZE() { return getToken(DMLStatementParser.MAX_SIZE, 0); }
		public TerminalNode NODEGROUP() { return getToken(DMLStatementParser.NODEGROUP, 0); }
		public TerminalNode WAIT() { return getToken(DMLStatementParser.WAIT, 0); }
		public TerminalNode LOGFILE() { return getToken(DMLStatementParser.LOGFILE, 0); }
		public TerminalNode UNDOFILE() { return getToken(DMLStatementParser.UNDOFILE, 0); }
		public TerminalNode UNDO_BUFFER_SIZE() { return getToken(DMLStatementParser.UNDO_BUFFER_SIZE, 0); }
		public TerminalNode REDO_BUFFER_SIZE() { return getToken(DMLStatementParser.REDO_BUFFER_SIZE, 0); }
		public TerminalNode DEFINITION() { return getToken(DMLStatementParser.DEFINITION, 0); }
		public TerminalNode ORGANIZATION() { return getToken(DMLStatementParser.ORGANIZATION, 0); }
		public TerminalNode DESCRIPTION() { return getToken(DMLStatementParser.DESCRIPTION, 0); }
		public TerminalNode REFERENCE() { return getToken(DMLStatementParser.REFERENCE, 0); }
		public TerminalNode FOLLOWS() { return getToken(DMLStatementParser.FOLLOWS, 0); }
		public TerminalNode PRECEDES() { return getToken(DMLStatementParser.PRECEDES, 0); }
		public TerminalNode NAME() { return getToken(DMLStatementParser.NAME, 0); }
		public TerminalNode CLOSE() { return getToken(DMLStatementParser.CLOSE, 0); }
		public TerminalNode OPEN() { return getToken(DMLStatementParser.OPEN, 0); }
		public TerminalNode NEXT() { return getToken(DMLStatementParser.NEXT, 0); }
		public TerminalNode HANDLER() { return getToken(DMLStatementParser.HANDLER, 0); }
		public TerminalNode PREV() { return getToken(DMLStatementParser.PREV, 0); }
		public TerminalNode IMPORT() { return getToken(DMLStatementParser.IMPORT, 0); }
		public TerminalNode CONCURRENT() { return getToken(DMLStatementParser.CONCURRENT, 0); }
		public TerminalNode XML() { return getToken(DMLStatementParser.XML, 0); }
		public TerminalNode POSITION() { return getToken(DMLStatementParser.POSITION, 0); }
		public TerminalNode SHARE() { return getToken(DMLStatementParser.SHARE, 0); }
		public TerminalNode DUMPFILE() { return getToken(DMLStatementParser.DUMPFILE, 0); }
		public TerminalNode CLONE() { return getToken(DMLStatementParser.CLONE, 0); }
		public TerminalNode AGGREGATE() { return getToken(DMLStatementParser.AGGREGATE, 0); }
		public TerminalNode INSTALL() { return getToken(DMLStatementParser.INSTALL, 0); }
		public TerminalNode UNINSTALL() { return getToken(DMLStatementParser.UNINSTALL, 0); }
		public TerminalNode COMPONENT() { return getToken(DMLStatementParser.COMPONENT, 0); }
		public TerminalNode RESOURCE() { return getToken(DMLStatementParser.RESOURCE, 0); }
		public TerminalNode FLUSH() { return getToken(DMLStatementParser.FLUSH, 0); }
		public TerminalNode RESET() { return getToken(DMLStatementParser.RESET, 0); }
		public TerminalNode RESTART() { return getToken(DMLStatementParser.RESTART, 0); }
		public TerminalNode HOSTS() { return getToken(DMLStatementParser.HOSTS, 0); }
		public TerminalNode RELAY() { return getToken(DMLStatementParser.RELAY, 0); }
		public TerminalNode EXPORT() { return getToken(DMLStatementParser.EXPORT, 0); }
		public TerminalNode USER_RESOURCES() { return getToken(DMLStatementParser.USER_RESOURCES, 0); }
		public TerminalNode SLOW() { return getToken(DMLStatementParser.SLOW, 0); }
		public TerminalNode GENERAL() { return getToken(DMLStatementParser.GENERAL, 0); }
		public TerminalNode CACHE() { return getToken(DMLStatementParser.CACHE, 0); }
		public TerminalNode SUBJECT() { return getToken(DMLStatementParser.SUBJECT, 0); }
		public TerminalNode ISSUER() { return getToken(DMLStatementParser.ISSUER, 0); }
		public TerminalNode OLD() { return getToken(DMLStatementParser.OLD, 0); }
		public TerminalNode RANDOM() { return getToken(DMLStatementParser.RANDOM, 0); }
		public TerminalNode RETAIN() { return getToken(DMLStatementParser.RETAIN, 0); }
		public TerminalNode MAX_USER_CONNECTIONS() { return getToken(DMLStatementParser.MAX_USER_CONNECTIONS, 0); }
		public TerminalNode MAX_CONNECTIONS_PER_HOUR() { return getToken(DMLStatementParser.MAX_CONNECTIONS_PER_HOUR, 0); }
		public TerminalNode MAX_UPDATES_PER_HOUR() { return getToken(DMLStatementParser.MAX_UPDATES_PER_HOUR, 0); }
		public TerminalNode MAX_QUERIES_PER_HOUR() { return getToken(DMLStatementParser.MAX_QUERIES_PER_HOUR, 0); }
		public TerminalNode REUSE() { return getToken(DMLStatementParser.REUSE, 0); }
		public TerminalNode OPTIONAL() { return getToken(DMLStatementParser.OPTIONAL, 0); }
		public TerminalNode HISTORY() { return getToken(DMLStatementParser.HISTORY, 0); }
		public TerminalNode NEVER() { return getToken(DMLStatementParser.NEVER, 0); }
		public TerminalNode EXPIRE() { return getToken(DMLStatementParser.EXPIRE, 0); }
		public TerminalNode TYPE() { return getToken(DMLStatementParser.TYPE, 0); }
		public TerminalNode CONTEXT() { return getToken(DMLStatementParser.CONTEXT, 0); }
		public TerminalNode CODE() { return getToken(DMLStatementParser.CODE, 0); }
		public TerminalNode CHANNEL() { return getToken(DMLStatementParser.CHANNEL, 0); }
		public TerminalNode SOURCE() { return getToken(DMLStatementParser.SOURCE, 0); }
		public TerminalNode IO_THREAD() { return getToken(DMLStatementParser.IO_THREAD, 0); }
		public TerminalNode SQL_THREAD() { return getToken(DMLStatementParser.SQL_THREAD, 0); }
		public TerminalNode SQL_BEFORE_GTIDS() { return getToken(DMLStatementParser.SQL_BEFORE_GTIDS, 0); }
		public TerminalNode SQL_AFTER_GTIDS() { return getToken(DMLStatementParser.SQL_AFTER_GTIDS, 0); }
		public TerminalNode MASTER_LOG_FILE() { return getToken(DMLStatementParser.MASTER_LOG_FILE, 0); }
		public TerminalNode MASTER_LOG_POS() { return getToken(DMLStatementParser.MASTER_LOG_POS, 0); }
		public TerminalNode RELAY_LOG_FILE() { return getToken(DMLStatementParser.RELAY_LOG_FILE, 0); }
		public TerminalNode RELAY_LOG_POS() { return getToken(DMLStatementParser.RELAY_LOG_POS, 0); }
		public TerminalNode SQL_AFTER_MTS_GAPS() { return getToken(DMLStatementParser.SQL_AFTER_MTS_GAPS, 0); }
		public TerminalNode UNTIL() { return getToken(DMLStatementParser.UNTIL, 0); }
		public TerminalNode DEFAULT_AUTH() { return getToken(DMLStatementParser.DEFAULT_AUTH, 0); }
		public TerminalNode PLUGIN_DIR() { return getToken(DMLStatementParser.PLUGIN_DIR, 0); }
		public TerminalNode STOP() { return getToken(DMLStatementParser.STOP, 0); }
		public TerminalNode SIGNED() { return getToken(DMLStatementParser.SIGNED, 0); }
		public UnreservedWordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unreservedWord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUnreservedWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUnreservedWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUnreservedWord(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnreservedWordContext unreservedWord() throws RecognitionException {
		UnreservedWordContext _localctx = new UnreservedWordContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_unreservedWord);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1139);
			_la = _input.LA(1);
			if ( !(((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (TRUNCATE - 49)) | (1L << (POSITION - 49)) | (1L << (VIEW - 49)) | (1L << (ANY - 49)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (OFFSET - 117)) | (1L << (BEGIN - 117)) | (1L << (COMMIT - 117)) | (1L << (ROLLBACK - 117)) | (1L << (SAVEPOINT - 117)) | (1L << (BOOLEAN - 117)) | (1L << (DATE - 117)) | (1L << (TIME - 117)) | (1L << (TIMESTAMP - 117)) | (1L << (YEAR - 117)) | (1L << (QUARTER - 117)) | (1L << (MONTH - 117)) | (1L << (WEEK - 117)) | (1L << (DAY - 117)) | (1L << (HOUR - 117)) | (1L << (MINUTE - 117)) | (1L << (SECOND - 117)) | (1L << (MICROSECOND - 117)) | (1L << (MAX - 117)) | (1L << (MIN - 117)) | (1L << (SUM - 117)) | (1L << (COUNT - 117)) | (1L << (AVG - 117)) | (1L << (CURRENT - 117)) | (1L << (ENABLE - 117)) | (1L << (DISABLE - 117)) | (1L << (INSTANCE - 117)) | (1L << (DO - 117)) | (1L << (DEFINER - 117)) | (1L << (CASCADED - 117)) | (1L << (LOCAL - 117)) | (1L << (CLOSE - 117)) | (1L << (OPEN - 117)) | (1L << (NEXT - 117)) | (1L << (NAME - 117)) | (1L << (TYPE - 117)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<TerminalNode> AT_() { return getTokens(DMLStatementParser.AT_); }
		public TerminalNode AT_(int i) {
			return getToken(DMLStatementParser.AT_, i);
		}
		public TerminalNode DOT_() { return getToken(DMLStatementParser.DOT_, 0); }
		public TerminalNode GLOBAL() { return getToken(DMLStatementParser.GLOBAL, 0); }
		public TerminalNode PERSIST() { return getToken(DMLStatementParser.PERSIST, 0); }
		public TerminalNode PERSIST_ONLY() { return getToken(DMLStatementParser.PERSIST_ONLY, 0); }
		public TerminalNode SESSION() { return getToken(DMLStatementParser.SESSION, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_variable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1145);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==AT_) {
				{
				setState(1142);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,144,_ctx) ) {
				case 1:
					{
					setState(1141);
					match(AT_);
					}
					break;
				}
				setState(1144);
				match(AT_);
				}
			}

			setState(1148);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,146,_ctx) ) {
			case 1:
				{
				setState(1147);
				_la = _input.LA(1);
				if ( !(((((_la - 293)) & ~0x3f) == 0 && ((1L << (_la - 293)) & ((1L << (GLOBAL - 293)) | (1L << (PERSIST - 293)) | (1L << (SESSION - 293)))) != 0) || _la==PERSIST_ONLY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
			setState(1151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOT_) {
				{
				setState(1150);
				match(DOT_);
				}
			}

			setState(1153);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SchemaNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public SchemaNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_schemaName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSchemaName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSchemaName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSchemaName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SchemaNameContext schemaName() throws RecognitionException {
		SchemaNameContext _localctx = new SchemaNameContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_schemaName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1155);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(DMLStatementParser.DOT_, 0); }
		public TableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTableName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTableName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTableName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableNameContext tableName() throws RecognitionException {
		TableNameContext _localctx = new TableNameContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_tableName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1160);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,148,_ctx) ) {
			case 1:
				{
				setState(1157);
				owner();
				setState(1158);
				match(DOT_);
				}
				break;
			}
			setState(1162);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(DMLStatementParser.DOT_, 0); }
		public ColumnNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterColumnName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitColumnName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitColumnName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNameContext columnName() throws RecognitionException {
		ColumnNameContext _localctx = new ColumnNameContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_columnName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1167);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,149,_ctx) ) {
			case 1:
				{
				setState(1164);
				owner();
				setState(1165);
				match(DOT_);
				}
				break;
			}
			setState(1169);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IndexNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public IndexNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIndexName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIndexName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIndexName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexNameContext indexName() throws RecognitionException {
		IndexNameContext _localctx = new IndexNameContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_indexName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1171);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UserNameContext extends ParserRuleContext {
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public TerminalNode AT_() { return getToken(DMLStatementParser.AT_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public UserNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_userName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUserName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUserName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUserName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UserNameContext userName() throws RecognitionException {
		UserNameContext _localctx = new UserNameContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_userName);
		try {
			setState(1178);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,150,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1173);
				match(STRING_);
				setState(1174);
				match(AT_);
				setState(1175);
				match(STRING_);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1176);
				identifier();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1177);
				match(STRING_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EventNameContext extends ParserRuleContext {
		public TerminalNode AT_() { return getToken(DMLStatementParser.AT_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public List<TerminalNode> IDENTIFIER_() { return getTokens(DMLStatementParser.IDENTIFIER_); }
		public TerminalNode IDENTIFIER_(int i) {
			return getToken(DMLStatementParser.IDENTIFIER_, i);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public EventNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eventName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterEventName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitEventName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitEventName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EventNameContext eventName() throws RecognitionException {
		EventNameContext _localctx = new EventNameContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_eventName);
		int _la;
		try {
			setState(1186);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1180);
				_la = _input.LA(1);
				if ( !(_la==IDENTIFIER_ || _la==STRING_) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1181);
				match(AT_);
				{
				setState(1182);
				match(STRING_);
				setState(1183);
				match(IDENTIFIER_);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1184);
				identifier();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1185);
				match(STRING_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ServerNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public ServerNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_serverName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterServerName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitServerName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitServerName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ServerNameContext serverName() throws RecognitionException {
		ServerNameContext _localctx = new ServerNameContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_serverName);
		try {
			setState(1190);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(1188);
				identifier();
				}
				break;
			case STRING_:
				enterOuterAlt(_localctx, 2);
				{
				setState(1189);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WrapperNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public WrapperNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wrapperName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWrapperName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWrapperName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWrapperName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WrapperNameContext wrapperName() throws RecognitionException {
		WrapperNameContext _localctx = new WrapperNameContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_wrapperName);
		try {
			setState(1194);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				enterOuterAlt(_localctx, 1);
				{
				setState(1192);
				identifier();
				}
				break;
			case STRING_:
				enterOuterAlt(_localctx, 2);
				{
				setState(1193);
				match(STRING_);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(DMLStatementParser.DOT_, 0); }
		public FunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFunctionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFunctionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionNameContext functionName() throws RecognitionException {
		FunctionNameContext _localctx = new FunctionNameContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_functionName);
		try {
			setState(1203);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,155,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1196);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1200);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,154,_ctx) ) {
				case 1:
					{
					setState(1197);
					owner();
					setState(1198);
					match(DOT_);
					}
					break;
				}
				setState(1202);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ViewNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OwnerContext owner() {
			return getRuleContext(OwnerContext.class,0);
		}
		public TerminalNode DOT_() { return getToken(DMLStatementParser.DOT_, 0); }
		public ViewNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_viewName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterViewName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitViewName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitViewName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ViewNameContext viewName() throws RecognitionException {
		ViewNameContext _localctx = new ViewNameContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_viewName);
		try {
			setState(1212);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,157,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1205);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1209);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,156,_ctx) ) {
				case 1:
					{
					setState(1206);
					owner();
					setState(1207);
					match(DOT_);
					}
					break;
				}
				setState(1211);
				identifier();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OwnerContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OwnerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_owner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterOwner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitOwner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitOwner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OwnerContext owner() throws RecognitionException {
		OwnerContext _localctx = new OwnerContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_owner);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1214);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1216);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNamesContext extends ParserRuleContext {
		public List<TableNameContext> tableName() {
			return getRuleContexts(TableNameContext.class);
		}
		public TableNameContext tableName(int i) {
			return getRuleContext(TableNameContext.class,i);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TableNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTableNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTableNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTableNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableNamesContext tableNames() throws RecognitionException {
		TableNamesContext _localctx = new TableNamesContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_tableNames);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1219);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(1218);
				match(LP_);
				}
			}

			setState(1221);
			tableName();
			setState(1226);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1222);
				match(COMMA_);
				setState(1223);
				tableName();
				}
				}
				setState(1228);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1230);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RP_) {
				{
				setState(1229);
				match(RP_);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNamesContext extends ParserRuleContext {
		public List<ColumnNameContext> columnName() {
			return getRuleContexts(ColumnNameContext.class);
		}
		public ColumnNameContext columnName(int i) {
			return getRuleContext(ColumnNameContext.class,i);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public ColumnNamesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnNames; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterColumnNames(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitColumnNames(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitColumnNames(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNamesContext columnNames() throws RecognitionException {
		ColumnNamesContext _localctx = new ColumnNamesContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_columnNames);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1233);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LP_) {
				{
				setState(1232);
				match(LP_);
				}
			}

			setState(1235);
			columnName();
			setState(1240);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,162,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1236);
					match(COMMA_);
					setState(1237);
					columnName();
					}
					} 
				}
				setState(1242);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,162,_ctx);
			}
			setState(1244);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,163,_ctx) ) {
			case 1:
				{
				setState(1243);
				match(RP_);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public GroupNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterGroupName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitGroupName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitGroupName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupNameContext groupName() throws RecognitionException {
		GroupNameContext _localctx = new GroupNameContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_groupName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1246);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShardLibraryNameContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public ShardLibraryNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shardLibraryName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterShardLibraryName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitShardLibraryName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitShardLibraryName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShardLibraryNameContext shardLibraryName() throws RecognitionException {
		ShardLibraryNameContext _localctx = new ShardLibraryNameContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_shardLibraryName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1248);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComponentNameContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public ComponentNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_componentName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterComponentName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitComponentName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitComponentName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComponentNameContext componentName() throws RecognitionException {
		ComponentNameContext _localctx = new ComponentNameContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_componentName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1250);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PluginNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public PluginNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pluginName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPluginName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPluginName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPluginName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PluginNameContext pluginName() throws RecognitionException {
		PluginNameContext _localctx = new PluginNameContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_pluginName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1252);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HostNameContext extends ParserRuleContext {
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public HostNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hostName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterHostName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitHostName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitHostName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HostNameContext hostName() throws RecognitionException {
		HostNameContext _localctx = new HostNameContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_hostName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1254);
			match(STRING_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PortContext extends ParserRuleContext {
		public TerminalNode NUMBER_() { return getToken(DMLStatementParser.NUMBER_, 0); }
		public PortContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_port; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPort(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPort(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPort(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PortContext port() throws RecognitionException {
		PortContext _localctx = new PortContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_port);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1256);
			match(NUMBER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloneInstanceContext extends ParserRuleContext {
		public UserNameContext userName() {
			return getRuleContext(UserNameContext.class,0);
		}
		public TerminalNode AT_() { return getToken(DMLStatementParser.AT_, 0); }
		public HostNameContext hostName() {
			return getRuleContext(HostNameContext.class,0);
		}
		public TerminalNode COLON_() { return getToken(DMLStatementParser.COLON_, 0); }
		public PortContext port() {
			return getRuleContext(PortContext.class,0);
		}
		public CloneInstanceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cloneInstance; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCloneInstance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCloneInstance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCloneInstance(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloneInstanceContext cloneInstance() throws RecognitionException {
		CloneInstanceContext _localctx = new CloneInstanceContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_cloneInstance);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1258);
			userName();
			setState(1259);
			match(AT_);
			setState(1260);
			hostName();
			setState(1261);
			match(COLON_);
			setState(1262);
			port();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CloneDirContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public CloneDirContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cloneDir; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCloneDir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCloneDir(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCloneDir(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CloneDirContext cloneDir() throws RecognitionException {
		CloneDirContext _localctx = new CloneDirContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_cloneDir);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1264);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChannelNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public ChannelNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_channelName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterChannelName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitChannelName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitChannelName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ChannelNameContext channelName() throws RecognitionException {
		ChannelNameContext _localctx = new ChannelNameContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_channelName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1266);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogNameContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public LogNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLogName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLogName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLogName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogNameContext logName() throws RecognitionException {
		LogNameContext _localctx = new LogNameContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_logName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1268);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RoleNameContext extends ParserRuleContext {
		public TerminalNode AT_() { return getToken(DMLStatementParser.AT_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public List<TerminalNode> IDENTIFIER_() { return getTokens(DMLStatementParser.IDENTIFIER_); }
		public TerminalNode IDENTIFIER_(int i) {
			return getToken(DMLStatementParser.IDENTIFIER_, i);
		}
		public RoleNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_roleName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterRoleName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitRoleName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitRoleName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RoleNameContext roleName() throws RecognitionException {
		RoleNameContext _localctx = new RoleNameContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_roleName);
		int _la;
		try {
			setState(1275);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,164,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1270);
				_la = _input.LA(1);
				if ( !(_la==IDENTIFIER_ || _la==STRING_) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1271);
				match(AT_);
				{
				setState(1272);
				match(STRING_);
				setState(1273);
				match(IDENTIFIER_);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1274);
				match(IDENTIFIER_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EngineNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public EngineNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_engineName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterEngineName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitEngineName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitEngineName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EngineNameContext engineName() throws RecognitionException {
		EngineNameContext _localctx = new EngineNameContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_engineName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1277);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public TriggerNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTriggerName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTriggerName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTriggerName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerNameContext triggerName() throws RecognitionException {
		TriggerNameContext _localctx = new TriggerNameContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_triggerName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1279);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerTimeContext extends ParserRuleContext {
		public TerminalNode BEFORE() { return getToken(DMLStatementParser.BEFORE, 0); }
		public TerminalNode AFTER() { return getToken(DMLStatementParser.AFTER, 0); }
		public TriggerTimeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerTime; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTriggerTime(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTriggerTime(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTriggerTime(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerTimeContext triggerTime() throws RecognitionException {
		TriggerTimeContext _localctx = new TriggerTimeContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_triggerTime);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1281);
			_la = _input.LA(1);
			if ( !(_la==AFTER || _la==BEFORE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UserOrRoleContext extends ParserRuleContext {
		public UserNameContext userName() {
			return getRuleContext(UserNameContext.class,0);
		}
		public RoleNameContext roleName() {
			return getRuleContext(RoleNameContext.class,0);
		}
		public UserOrRoleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_userOrRole; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterUserOrRole(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitUserOrRole(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitUserOrRole(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UserOrRoleContext userOrRole() throws RecognitionException {
		UserOrRoleContext _localctx = new UserOrRoleContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_userOrRole);
		try {
			setState(1285);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,165,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1283);
				userName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1284);
				roleName();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PartitionNameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER_() { return getToken(DMLStatementParser.IDENTIFIER_, 0); }
		public PartitionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_partitionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPartitionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPartitionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPartitionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PartitionNameContext partitionName() throws RecognitionException {
		PartitionNameContext _localctx = new PartitionNameContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_partitionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1287);
			match(IDENTIFIER_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerEventContext extends ParserRuleContext {
		public TerminalNode INSERT() { return getToken(DMLStatementParser.INSERT, 0); }
		public TerminalNode UPDATE() { return getToken(DMLStatementParser.UPDATE, 0); }
		public TerminalNode DELETE() { return getToken(DMLStatementParser.DELETE, 0); }
		public TriggerEventContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerEvent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTriggerEvent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTriggerEvent(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTriggerEvent(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerEventContext triggerEvent() throws RecognitionException {
		TriggerEventContext _localctx = new TriggerEventContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_triggerEvent);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1289);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INSERT) | (1L << UPDATE) | (1L << DELETE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriggerOrderContext extends ParserRuleContext {
		public TriggerNameContext triggerName() {
			return getRuleContext(TriggerNameContext.class,0);
		}
		public TerminalNode FOLLOWS() { return getToken(DMLStatementParser.FOLLOWS, 0); }
		public TerminalNode PRECEDES() { return getToken(DMLStatementParser.PRECEDES, 0); }
		public TriggerOrderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triggerOrder; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTriggerOrder(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTriggerOrder(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTriggerOrder(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TriggerOrderContext triggerOrder() throws RecognitionException {
		TriggerOrderContext _localctx = new TriggerOrderContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_triggerOrder);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1291);
			_la = _input.LA(1);
			if ( !(_la==FOLLOWS || _la==PRECEDES) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1292);
			triggerName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public NotOperator_Context notOperator_() {
			return getRuleContext(NotOperator_Context.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public BooleanPrimaryContext booleanPrimary() {
			return getRuleContext(BooleanPrimaryContext.class,0);
		}
		public LogicalOperatorContext logicalOperator() {
			return getRuleContext(LogicalOperatorContext.class,0);
		}
		public TerminalNode XOR() { return getToken(DMLStatementParser.XOR, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 220;
		enterRecursionRule(_localctx, 220, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1303);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,166,_ctx) ) {
			case 1:
				{
				setState(1295);
				notOperator_();
				setState(1296);
				expr(3);
				}
				break;
			case 2:
				{
				setState(1298);
				match(LP_);
				setState(1299);
				expr(0);
				setState(1300);
				match(RP_);
				}
				break;
			case 3:
				{
				setState(1302);
				booleanPrimary(0);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1314);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,168,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1312);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,167,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1305);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(1306);
						logicalOperator();
						setState(1307);
						expr(6);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1309);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(1310);
						match(XOR);
						setState(1311);
						expr(5);
						}
						break;
					}
					} 
				}
				setState(1316);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,168,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LogicalOperatorContext extends ParserRuleContext {
		public TerminalNode OR() { return getToken(DMLStatementParser.OR, 0); }
		public TerminalNode OR_() { return getToken(DMLStatementParser.OR_, 0); }
		public TerminalNode AND() { return getToken(DMLStatementParser.AND, 0); }
		public TerminalNode AND_() { return getToken(DMLStatementParser.AND_, 0); }
		public LogicalOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLogicalOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLogicalOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLogicalOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperatorContext logicalOperator() throws RecognitionException {
		LogicalOperatorContext _localctx = new LogicalOperatorContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_logicalOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1317);
			_la = _input.LA(1);
			if ( !(_la==AND_ || _la==OR_ || _la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotOperator_Context extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(DMLStatementParser.NOT, 0); }
		public TerminalNode NOT_() { return getToken(DMLStatementParser.NOT_, 0); }
		public NotOperator_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notOperator_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterNotOperator_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitNotOperator_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitNotOperator_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotOperator_Context notOperator_() throws RecognitionException {
		NotOperator_Context _localctx = new NotOperator_Context(_ctx, getState());
		enterRule(_localctx, 224, RULE_notOperator_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1319);
			_la = _input.LA(1);
			if ( !(_la==NOT_ || _la==NOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanPrimaryContext extends ParserRuleContext {
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public BooleanPrimaryContext booleanPrimary() {
			return getRuleContext(BooleanPrimaryContext.class,0);
		}
		public TerminalNode IS() { return getToken(DMLStatementParser.IS, 0); }
		public TerminalNode TRUE() { return getToken(DMLStatementParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(DMLStatementParser.FALSE, 0); }
		public TerminalNode UNKNOWN() { return getToken(DMLStatementParser.UNKNOWN, 0); }
		public TerminalNode NULL() { return getToken(DMLStatementParser.NULL, 0); }
		public TerminalNode NOT() { return getToken(DMLStatementParser.NOT, 0); }
		public TerminalNode SAFE_EQ_() { return getToken(DMLStatementParser.SAFE_EQ_, 0); }
		public ComparisonOperatorContext comparisonOperator() {
			return getRuleContext(ComparisonOperatorContext.class,0);
		}
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode ALL() { return getToken(DMLStatementParser.ALL, 0); }
		public TerminalNode ANY() { return getToken(DMLStatementParser.ANY, 0); }
		public BooleanPrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanPrimary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterBooleanPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitBooleanPrimary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitBooleanPrimary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanPrimaryContext booleanPrimary() throws RecognitionException {
		return booleanPrimary(0);
	}

	private BooleanPrimaryContext booleanPrimary(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BooleanPrimaryContext _localctx = new BooleanPrimaryContext(_ctx, _parentState);
		BooleanPrimaryContext _prevctx = _localctx;
		int _startState = 226;
		enterRecursionRule(_localctx, 226, RULE_booleanPrimary, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(1322);
			predicate();
			}
			_ctx.stop = _input.LT(-1);
			setState(1344);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,171,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1342);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,170,_ctx) ) {
					case 1:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(1324);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(1325);
						match(IS);
						setState(1327);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==NOT) {
							{
							setState(1326);
							match(NOT);
							}
						}

						setState(1329);
						_la = _input.LA(1);
						if ( !(((((_la - 101)) & ~0x3f) == 0 && ((1L << (_la - 101)) & ((1L << (NULL - 101)) | (1L << (TRUE - 101)) | (1L << (FALSE - 101)))) != 0) || _la==UNKNOWN) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						break;
					case 2:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(1330);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(1331);
						match(SAFE_EQ_);
						setState(1332);
						predicate();
						}
						break;
					case 3:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(1333);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(1334);
						comparisonOperator();
						setState(1335);
						predicate();
						}
						break;
					case 4:
						{
						_localctx = new BooleanPrimaryContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_booleanPrimary);
						setState(1337);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(1338);
						comparisonOperator();
						setState(1339);
						_la = _input.LA(1);
						if ( !(_la==ALL || _la==ANY) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1340);
						subquery();
						}
						break;
					}
					} 
				}
				setState(1346);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,171,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ComparisonOperatorContext extends ParserRuleContext {
		public TerminalNode EQ_() { return getToken(DMLStatementParser.EQ_, 0); }
		public TerminalNode GTE_() { return getToken(DMLStatementParser.GTE_, 0); }
		public TerminalNode GT_() { return getToken(DMLStatementParser.GT_, 0); }
		public TerminalNode LTE_() { return getToken(DMLStatementParser.LTE_, 0); }
		public TerminalNode LT_() { return getToken(DMLStatementParser.LT_, 0); }
		public TerminalNode NEQ_() { return getToken(DMLStatementParser.NEQ_, 0); }
		public ComparisonOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparisonOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterComparisonOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitComparisonOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitComparisonOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComparisonOperatorContext comparisonOperator() throws RecognitionException {
		ComparisonOperatorContext _localctx = new ComparisonOperatorContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_comparisonOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1347);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ_) | (1L << NEQ_) | (1L << GT_) | (1L << GTE_) | (1L << LT_) | (1L << LTE_))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicateContext extends ParserRuleContext {
		public List<BitExprContext> bitExpr() {
			return getRuleContexts(BitExprContext.class);
		}
		public BitExprContext bitExpr(int i) {
			return getRuleContext(BitExprContext.class,i);
		}
		public TerminalNode IN() { return getToken(DMLStatementParser.IN, 0); }
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode NOT() { return getToken(DMLStatementParser.NOT, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode BETWEEN() { return getToken(DMLStatementParser.BETWEEN, 0); }
		public TerminalNode AND() { return getToken(DMLStatementParser.AND, 0); }
		public PredicateContext predicate() {
			return getRuleContext(PredicateContext.class,0);
		}
		public TerminalNode SOUNDS() { return getToken(DMLStatementParser.SOUNDS, 0); }
		public TerminalNode LIKE() { return getToken(DMLStatementParser.LIKE, 0); }
		public List<SimpleExprContext> simpleExpr() {
			return getRuleContexts(SimpleExprContext.class);
		}
		public SimpleExprContext simpleExpr(int i) {
			return getRuleContext(SimpleExprContext.class,i);
		}
		public TerminalNode ESCAPE() { return getToken(DMLStatementParser.ESCAPE, 0); }
		public TerminalNode REGEXP() { return getToken(DMLStatementParser.REGEXP, 0); }
		public TerminalNode RLIKE() { return getToken(DMLStatementParser.RLIKE, 0); }
		public PredicateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPredicate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPredicate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPredicate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PredicateContext predicate() throws RecognitionException {
		PredicateContext _localctx = new PredicateContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_predicate);
		int _la;
		try {
			setState(1404);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,179,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1349);
				bitExpr(0);
				setState(1351);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(1350);
					match(NOT);
					}
				}

				setState(1353);
				match(IN);
				setState(1354);
				subquery();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1356);
				bitExpr(0);
				setState(1358);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(1357);
					match(NOT);
					}
				}

				setState(1360);
				match(IN);
				setState(1361);
				match(LP_);
				setState(1362);
				expr(0);
				setState(1367);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1363);
					match(COMMA_);
					setState(1364);
					expr(0);
					}
					}
					setState(1369);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1370);
				match(RP_);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1372);
				bitExpr(0);
				setState(1374);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(1373);
					match(NOT);
					}
				}

				setState(1376);
				match(BETWEEN);
				setState(1377);
				bitExpr(0);
				setState(1378);
				match(AND);
				setState(1379);
				predicate();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1381);
				bitExpr(0);
				setState(1382);
				match(SOUNDS);
				setState(1383);
				match(LIKE);
				setState(1384);
				bitExpr(0);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1386);
				bitExpr(0);
				setState(1388);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(1387);
					match(NOT);
					}
				}

				setState(1390);
				match(LIKE);
				setState(1391);
				simpleExpr(0);
				setState(1394);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,177,_ctx) ) {
				case 1:
					{
					setState(1392);
					match(ESCAPE);
					setState(1393);
					simpleExpr(0);
					}
					break;
				}
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1396);
				bitExpr(0);
				setState(1398);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NOT) {
					{
					setState(1397);
					match(NOT);
					}
				}

				setState(1400);
				_la = _input.LA(1);
				if ( !(_la==REGEXP || _la==RLIKE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1401);
				bitExpr(0);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1403);
				bitExpr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BitExprContext extends ParserRuleContext {
		public SimpleExprContext simpleExpr() {
			return getRuleContext(SimpleExprContext.class,0);
		}
		public List<BitExprContext> bitExpr() {
			return getRuleContexts(BitExprContext.class);
		}
		public BitExprContext bitExpr(int i) {
			return getRuleContext(BitExprContext.class,i);
		}
		public TerminalNode VERTICAL_BAR_() { return getToken(DMLStatementParser.VERTICAL_BAR_, 0); }
		public TerminalNode AMPERSAND_() { return getToken(DMLStatementParser.AMPERSAND_, 0); }
		public TerminalNode SIGNED_LEFT_SHIFT_() { return getToken(DMLStatementParser.SIGNED_LEFT_SHIFT_, 0); }
		public TerminalNode SIGNED_RIGHT_SHIFT_() { return getToken(DMLStatementParser.SIGNED_RIGHT_SHIFT_, 0); }
		public TerminalNode PLUS_() { return getToken(DMLStatementParser.PLUS_, 0); }
		public TerminalNode MINUS_() { return getToken(DMLStatementParser.MINUS_, 0); }
		public TerminalNode ASTERISK_() { return getToken(DMLStatementParser.ASTERISK_, 0); }
		public TerminalNode SLASH_() { return getToken(DMLStatementParser.SLASH_, 0); }
		public TerminalNode DIV() { return getToken(DMLStatementParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(DMLStatementParser.MOD, 0); }
		public TerminalNode MOD_() { return getToken(DMLStatementParser.MOD_, 0); }
		public TerminalNode CARET_() { return getToken(DMLStatementParser.CARET_, 0); }
		public IntervalExpressionContext intervalExpression() {
			return getRuleContext(IntervalExpressionContext.class,0);
		}
		public BitExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterBitExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitBitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitBitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BitExprContext bitExpr() throws RecognitionException {
		return bitExpr(0);
	}

	private BitExprContext bitExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BitExprContext _localctx = new BitExprContext(_ctx, _parentState);
		BitExprContext _prevctx = _localctx;
		int _startState = 232;
		enterRecursionRule(_localctx, 232, RULE_bitExpr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(1407);
			simpleExpr(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(1453);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,181,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1451);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,180,_ctx) ) {
					case 1:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1409);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(1410);
						match(VERTICAL_BAR_);
						setState(1411);
						bitExpr(16);
						}
						break;
					case 2:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1412);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(1413);
						match(AMPERSAND_);
						setState(1414);
						bitExpr(15);
						}
						break;
					case 3:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1415);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(1416);
						match(SIGNED_LEFT_SHIFT_);
						setState(1417);
						bitExpr(14);
						}
						break;
					case 4:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1418);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(1419);
						match(SIGNED_RIGHT_SHIFT_);
						setState(1420);
						bitExpr(13);
						}
						break;
					case 5:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1421);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(1422);
						match(PLUS_);
						setState(1423);
						bitExpr(12);
						}
						break;
					case 6:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1424);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(1425);
						match(MINUS_);
						setState(1426);
						bitExpr(11);
						}
						break;
					case 7:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1427);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(1428);
						match(ASTERISK_);
						setState(1429);
						bitExpr(10);
						}
						break;
					case 8:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1430);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(1431);
						match(SLASH_);
						setState(1432);
						bitExpr(9);
						}
						break;
					case 9:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1433);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(1434);
						match(DIV);
						setState(1435);
						bitExpr(8);
						}
						break;
					case 10:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1436);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(1437);
						match(MOD);
						setState(1438);
						bitExpr(7);
						}
						break;
					case 11:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1439);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(1440);
						match(MOD_);
						setState(1441);
						bitExpr(6);
						}
						break;
					case 12:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1442);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(1443);
						match(CARET_);
						setState(1444);
						bitExpr(5);
						}
						break;
					case 13:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1445);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(1446);
						match(PLUS_);
						setState(1447);
						intervalExpression();
						}
						break;
					case 14:
						{
						_localctx = new BitExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_bitExpr);
						setState(1448);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(1449);
						match(MINUS_);
						setState(1450);
						intervalExpression();
						}
						break;
					}
					} 
				}
				setState(1455);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,181,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SimpleExprContext extends ParserRuleContext {
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ParameterMarkerContext parameterMarker() {
			return getRuleContext(ParameterMarkerContext.class,0);
		}
		public LiteralsContext literals() {
			return getRuleContext(LiteralsContext.class,0);
		}
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public List<SimpleExprContext> simpleExpr() {
			return getRuleContexts(SimpleExprContext.class);
		}
		public SimpleExprContext simpleExpr(int i) {
			return getRuleContext(SimpleExprContext.class,i);
		}
		public TerminalNode PLUS_() { return getToken(DMLStatementParser.PLUS_, 0); }
		public TerminalNode MINUS_() { return getToken(DMLStatementParser.MINUS_, 0); }
		public TerminalNode TILDE_() { return getToken(DMLStatementParser.TILDE_, 0); }
		public TerminalNode NOT_() { return getToken(DMLStatementParser.NOT_, 0); }
		public TerminalNode BINARY() { return getToken(DMLStatementParser.BINARY, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode ROW() { return getToken(DMLStatementParser.ROW, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public SubqueryContext subquery() {
			return getRuleContext(SubqueryContext.class,0);
		}
		public TerminalNode EXISTS() { return getToken(DMLStatementParser.EXISTS, 0); }
		public TerminalNode LBE_() { return getToken(DMLStatementParser.LBE_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode RBE_() { return getToken(DMLStatementParser.RBE_, 0); }
		public MatchExpression_Context matchExpression_() {
			return getRuleContext(MatchExpression_Context.class,0);
		}
		public CaseExpressionContext caseExpression() {
			return getRuleContext(CaseExpressionContext.class,0);
		}
		public IntervalExpressionContext intervalExpression() {
			return getRuleContext(IntervalExpressionContext.class,0);
		}
		public TerminalNode OR_() { return getToken(DMLStatementParser.OR_, 0); }
		public TerminalNode COLLATE() { return getToken(DMLStatementParser.COLLATE, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public SimpleExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleExpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSimpleExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSimpleExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSimpleExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleExprContext simpleExpr() throws RecognitionException {
		return simpleExpr(0);
	}

	private SimpleExprContext simpleExpr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SimpleExprContext _localctx = new SimpleExprContext(_ctx, _parentState);
		SimpleExprContext _prevctx = _localctx;
		int _startState = 234;
		enterRecursionRule(_localctx, 234, RULE_simpleExpr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1490);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,185,_ctx) ) {
			case 1:
				{
				setState(1457);
				functionCall();
				}
				break;
			case 2:
				{
				setState(1458);
				parameterMarker();
				}
				break;
			case 3:
				{
				setState(1459);
				literals();
				}
				break;
			case 4:
				{
				setState(1460);
				columnName();
				}
				break;
			case 5:
				{
				setState(1461);
				variable();
				}
				break;
			case 6:
				{
				setState(1462);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT_) | (1L << TILDE_) | (1L << PLUS_) | (1L << MINUS_))) != 0) || _la==BINARY) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1463);
				simpleExpr(7);
				}
				break;
			case 7:
				{
				setState(1465);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ROW) {
					{
					setState(1464);
					match(ROW);
					}
				}

				setState(1467);
				match(LP_);
				setState(1468);
				expr(0);
				setState(1473);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1469);
					match(COMMA_);
					setState(1470);
					expr(0);
					}
					}
					setState(1475);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1476);
				match(RP_);
				}
				break;
			case 8:
				{
				setState(1479);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXISTS) {
					{
					setState(1478);
					match(EXISTS);
					}
				}

				setState(1481);
				subquery();
				}
				break;
			case 9:
				{
				setState(1482);
				match(LBE_);
				setState(1483);
				identifier();
				setState(1484);
				expr(0);
				setState(1485);
				match(RBE_);
				}
				break;
			case 10:
				{
				setState(1487);
				matchExpression_();
				}
				break;
			case 11:
				{
				setState(1488);
				caseExpression();
				}
				break;
			case 12:
				{
				setState(1489);
				intervalExpression();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1503);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,188,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1501);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,187,_ctx) ) {
					case 1:
						{
						_localctx = new SimpleExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_simpleExpr);
						setState(1492);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(1493);
						match(OR_);
						setState(1494);
						simpleExpr(9);
						}
						break;
					case 2:
						{
						_localctx = new SimpleExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_simpleExpr);
						setState(1495);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(1496);
						match(COLLATE);
						setState(1499);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case STRING_:
							{
							setState(1497);
							match(STRING_);
							}
							break;
						case TRUNCATE:
						case POSITION:
						case VIEW:
						case ANY:
						case OFFSET:
						case BEGIN:
						case COMMIT:
						case ROLLBACK:
						case SAVEPOINT:
						case BOOLEAN:
						case DATE:
						case TIME:
						case TIMESTAMP:
						case YEAR:
						case QUARTER:
						case MONTH:
						case WEEK:
						case DAY:
						case HOUR:
						case MINUTE:
						case SECOND:
						case MICROSECOND:
						case MAX:
						case MIN:
						case SUM:
						case COUNT:
						case AVG:
						case CURRENT:
						case ENABLE:
						case DISABLE:
						case INSTANCE:
						case DO:
						case DEFINER:
						case CASCADED:
						case LOCAL:
						case CLOSE:
						case OPEN:
						case NEXT:
						case NAME:
						case TYPE:
						case TABLES:
						case TABLESPACE:
						case COLUMNS:
						case FIELDS:
						case INDEXES:
						case STATUS:
						case MODIFY:
						case VALUE:
						case DUPLICATE:
						case FIRST:
						case LAST:
						case AFTER:
						case OJ:
						case ACCOUNT:
						case USER:
						case ROLE:
						case START:
						case TRANSACTION:
						case WITHOUT:
						case ESCAPE:
						case SUBPARTITION:
						case STORAGE:
						case SUPER:
						case TEMPORARY:
						case THAN:
						case UNBOUNDED:
						case SIGNED:
						case UPGRADE:
						case VALIDATION:
						case ROLLUP:
						case SOUNDS:
						case UNKNOWN:
						case OFF:
						case ALWAYS:
						case COMMITTED:
						case LEVEL:
						case NO:
						case PASSWORD:
						case PRIVILEGES:
						case ACTION:
						case ALGORITHM:
						case AUTOCOMMIT:
						case BTREE:
						case CHAIN:
						case CHARSET:
						case CHECKSUM:
						case CIPHER:
						case CLIENT:
						case COALESCE:
						case COMMENT:
						case COMPACT:
						case COMPRESSED:
						case COMPRESSION:
						case CONNECTION:
						case CONSISTENT:
						case DATA:
						case DISCARD:
						case DISK:
						case ENCRYPTION:
						case END:
						case ENGINE:
						case EVENT:
						case EXCHANGE:
						case EXECUTE:
						case FILE:
						case FIXED:
						case FOLLOWING:
						case GLOBAL:
						case HASH:
						case IMPORT_:
						case LESS:
						case MEMORY:
						case NONE:
						case PARSER:
						case PARTIAL:
						case PARTITIONING:
						case PERSIST:
						case PRECEDING:
						case PROCESS:
						case PROXY:
						case QUICK:
						case REBUILD:
						case REDUNDANT:
						case RELOAD:
						case REMOVE:
						case REORGANIZE:
						case REPAIR:
						case REVERSE:
						case SESSION:
						case SHUTDOWN:
						case SIMPLE:
						case SLAVE:
						case VISIBLE:
						case INVISIBLE:
						case ENFORCED:
						case AGAINST:
						case LANGUAGE:
						case MODE:
						case QUERY:
						case EXTENDED:
						case EXPANSION:
						case VARIANCE:
						case MAX_ROWS:
						case MIN_ROWS:
						case SQL_BIG_RESULT:
						case SQL_BUFFER_RESULT:
						case SQL_CACHE:
						case SQL_NO_CACHE:
						case STATS_AUTO_RECALC:
						case STATS_PERSISTENT:
						case STATS_SAMPLE_PAGES:
						case ROW_FORMAT:
						case WEIGHT_STRING:
						case COLUMN_FORMAT:
						case INSERT_METHOD:
						case KEY_BLOCK_SIZE:
						case PACK_KEYS:
						case PERSIST_ONLY:
						case BIT_AND:
						case BIT_OR:
						case BIT_XOR:
						case GROUP_CONCAT:
						case JSON_ARRAYAGG:
						case JSON_OBJECTAGG:
						case STD:
						case STDDEV:
						case STDDEV_POP:
						case STDDEV_SAMP:
						case VAR_POP:
						case VAR_SAMP:
						case AUTO_INCREMENT:
						case AVG_ROW_LENGTH:
						case DELAY_KEY_WRITE:
						case ROTATE:
						case MASTER:
						case BINLOG:
						case ERROR:
						case SCHEDULE:
						case COMPLETION:
						case EVERY:
						case HOST:
						case SOCKET:
						case PORT:
						case SERVER:
						case WRAPPER:
						case OPTIONS:
						case OWNER:
						case RETURNS:
						case CONTAINS:
						case SECURITY:
						case INVOKER:
						case TEMPTABLE:
						case MERGE:
						case UNDEFINED:
						case DATAFILE:
						case FILE_BLOCK_SIZE:
						case EXTENT_SIZE:
						case INITIAL_SIZE:
						case AUTOEXTEND_SIZE:
						case MAX_SIZE:
						case NODEGROUP:
						case WAIT:
						case LOGFILE:
						case UNDOFILE:
						case UNDO_BUFFER_SIZE:
						case REDO_BUFFER_SIZE:
						case HANDLER:
						case PREV:
						case ORGANIZATION:
						case DEFINITION:
						case DESCRIPTION:
						case REFERENCE:
						case FOLLOWS:
						case PRECEDES:
						case IMPORT:
						case CONCURRENT:
						case XML:
						case DUMPFILE:
						case SHARE:
						case CODE:
						case CONTEXT:
						case SOURCE:
						case CHANNEL:
						case CLONE:
						case AGGREGATE:
						case INSTALL:
						case COMPONENT:
						case UNINSTALL:
						case RESOURCE:
						case EXPIRE:
						case NEVER:
						case HISTORY:
						case OPTIONAL:
						case REUSE:
						case MAX_QUERIES_PER_HOUR:
						case MAX_UPDATES_PER_HOUR:
						case MAX_CONNECTIONS_PER_HOUR:
						case MAX_USER_CONNECTIONS:
						case RETAIN:
						case RANDOM:
						case OLD:
						case ISSUER:
						case SUBJECT:
						case CACHE:
						case GENERAL:
						case SLOW:
						case USER_RESOURCES:
						case EXPORT:
						case RELAY:
						case HOSTS:
						case FLUSH:
						case RESET:
						case RESTART:
						case IO_THREAD:
						case SQL_THREAD:
						case SQL_BEFORE_GTIDS:
						case SQL_AFTER_GTIDS:
						case MASTER_LOG_FILE:
						case MASTER_LOG_POS:
						case RELAY_LOG_FILE:
						case RELAY_LOG_POS:
						case SQL_AFTER_MTS_GAPS:
						case UNTIL:
						case DEFAULT_AUTH:
						case PLUGIN_DIR:
						case STOP:
						case IDENTIFIER_:
							{
							setState(1498);
							identifier();
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						}
						break;
					}
					} 
				}
				setState(1505);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,188,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public AggregationFunctionContext aggregationFunction() {
			return getRuleContext(AggregationFunctionContext.class,0);
		}
		public SpecialFunctionContext specialFunction() {
			return getRuleContext(SpecialFunctionContext.class,0);
		}
		public RegularFunctionContext regularFunction() {
			return getRuleContext(RegularFunctionContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFunctionCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFunctionCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_functionCall);
		try {
			setState(1509);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,189,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1506);
				aggregationFunction();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1507);
				specialFunction();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1508);
				regularFunction();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationFunctionContext extends ParserRuleContext {
		public AggregationFunctionNameContext aggregationFunctionName() {
			return getRuleContext(AggregationFunctionNameContext.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public DistinctContext distinct() {
			return getRuleContext(DistinctContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASTERISK_() { return getToken(DMLStatementParser.ASTERISK_, 0); }
		public OverClause_Context overClause_() {
			return getRuleContext(OverClause_Context.class,0);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public AggregationFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterAggregationFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitAggregationFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitAggregationFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregationFunctionContext aggregationFunction() throws RecognitionException {
		AggregationFunctionContext _localctx = new AggregationFunctionContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_aggregationFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1511);
			aggregationFunctionName();
			setState(1512);
			match(LP_);
			setState(1514);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DISTINCT) {
				{
				setState(1513);
				distinct();
				}
			}

			setState(1525);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(1516);
				expr(0);
				setState(1521);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1517);
					match(COMMA_);
					setState(1518);
					expr(0);
					}
					}
					setState(1523);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ASTERISK_:
				{
				setState(1524);
				match(ASTERISK_);
				}
				break;
			case RP_:
				break;
			default:
				break;
			}
			setState(1527);
			match(RP_);
			setState(1529);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,193,_ctx) ) {
			case 1:
				{
				setState(1528);
				overClause_();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregationFunctionNameContext extends ParserRuleContext {
		public TerminalNode MAX() { return getToken(DMLStatementParser.MAX, 0); }
		public TerminalNode MIN() { return getToken(DMLStatementParser.MIN, 0); }
		public TerminalNode SUM() { return getToken(DMLStatementParser.SUM, 0); }
		public TerminalNode COUNT() { return getToken(DMLStatementParser.COUNT, 0); }
		public TerminalNode AVG() { return getToken(DMLStatementParser.AVG, 0); }
		public TerminalNode BIT_AND() { return getToken(DMLStatementParser.BIT_AND, 0); }
		public TerminalNode BIT_OR() { return getToken(DMLStatementParser.BIT_OR, 0); }
		public TerminalNode BIT_XOR() { return getToken(DMLStatementParser.BIT_XOR, 0); }
		public TerminalNode JSON_ARRAYAGG() { return getToken(DMLStatementParser.JSON_ARRAYAGG, 0); }
		public TerminalNode JSON_OBJECTAGG() { return getToken(DMLStatementParser.JSON_OBJECTAGG, 0); }
		public TerminalNode STD() { return getToken(DMLStatementParser.STD, 0); }
		public TerminalNode STDDEV() { return getToken(DMLStatementParser.STDDEV, 0); }
		public TerminalNode STDDEV_POP() { return getToken(DMLStatementParser.STDDEV_POP, 0); }
		public TerminalNode STDDEV_SAMP() { return getToken(DMLStatementParser.STDDEV_SAMP, 0); }
		public TerminalNode VAR_POP() { return getToken(DMLStatementParser.VAR_POP, 0); }
		public TerminalNode VAR_SAMP() { return getToken(DMLStatementParser.VAR_SAMP, 0); }
		public TerminalNode VARIANCE() { return getToken(DMLStatementParser.VARIANCE, 0); }
		public AggregationFunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregationFunctionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterAggregationFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitAggregationFunctionName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitAggregationFunctionName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregationFunctionNameContext aggregationFunctionName() throws RecognitionException {
		AggregationFunctionNameContext _localctx = new AggregationFunctionNameContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_aggregationFunctionName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1531);
			_la = _input.LA(1);
			if ( !(((((_la - 142)) & ~0x3f) == 0 && ((1L << (_la - 142)) & ((1L << (MAX - 142)) | (1L << (MIN - 142)) | (1L << (SUM - 142)) | (1L << (COUNT - 142)) | (1L << (AVG - 142)))) != 0) || ((((_la - 353)) & ~0x3f) == 0 && ((1L << (_la - 353)) & ((1L << (VARIANCE - 353)) | (1L << (BIT_AND - 353)) | (1L << (BIT_OR - 353)) | (1L << (BIT_XOR - 353)) | (1L << (JSON_ARRAYAGG - 353)) | (1L << (JSON_OBJECTAGG - 353)) | (1L << (STD - 353)) | (1L << (STDDEV - 353)) | (1L << (STDDEV_POP - 353)) | (1L << (STDDEV_SAMP - 353)) | (1L << (VAR_POP - 353)) | (1L << (VAR_SAMP - 353)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DistinctContext extends ParserRuleContext {
		public TerminalNode DISTINCT() { return getToken(DMLStatementParser.DISTINCT, 0); }
		public DistinctContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_distinct; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDistinct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDistinct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDistinct(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DistinctContext distinct() throws RecognitionException {
		DistinctContext _localctx = new DistinctContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_distinct);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1533);
			match(DISTINCT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OverClause_Context extends ParserRuleContext {
		public TerminalNode OVER() { return getToken(DMLStatementParser.OVER, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public WindowSpecification_Context windowSpecification_() {
			return getRuleContext(WindowSpecification_Context.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public OverClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_overClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterOverClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitOverClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitOverClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OverClause_Context overClause_() throws RecognitionException {
		OverClause_Context _localctx = new OverClause_Context(_ctx, getState());
		enterRule(_localctx, 244, RULE_overClause_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1535);
			match(OVER);
			setState(1541);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LP_:
				{
				setState(1536);
				match(LP_);
				setState(1537);
				windowSpecification_();
				setState(1538);
				match(RP_);
				}
				break;
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				{
				setState(1540);
				identifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowSpecification_Context extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public PartitionClause_Context partitionClause_() {
			return getRuleContext(PartitionClause_Context.class,0);
		}
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public FrameClause_Context frameClause_() {
			return getRuleContext(FrameClause_Context.class,0);
		}
		public WindowSpecification_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowSpecification_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWindowSpecification_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWindowSpecification_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWindowSpecification_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowSpecification_Context windowSpecification_() throws RecognitionException {
		WindowSpecification_Context _localctx = new WindowSpecification_Context(_ctx, getState());
		enterRule(_localctx, 246, RULE_windowSpecification_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1544);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 49)) & ~0x3f) == 0 && ((1L << (_la - 49)) & ((1L << (TRUNCATE - 49)) | (1L << (POSITION - 49)) | (1L << (VIEW - 49)) | (1L << (ANY - 49)))) != 0) || ((((_la - 117)) & ~0x3f) == 0 && ((1L << (_la - 117)) & ((1L << (OFFSET - 117)) | (1L << (BEGIN - 117)) | (1L << (COMMIT - 117)) | (1L << (ROLLBACK - 117)) | (1L << (SAVEPOINT - 117)) | (1L << (BOOLEAN - 117)) | (1L << (DATE - 117)) | (1L << (TIME - 117)) | (1L << (TIMESTAMP - 117)) | (1L << (YEAR - 117)) | (1L << (QUARTER - 117)) | (1L << (MONTH - 117)) | (1L << (WEEK - 117)) | (1L << (DAY - 117)) | (1L << (HOUR - 117)) | (1L << (MINUTE - 117)) | (1L << (SECOND - 117)) | (1L << (MICROSECOND - 117)) | (1L << (MAX - 117)) | (1L << (MIN - 117)) | (1L << (SUM - 117)) | (1L << (COUNT - 117)) | (1L << (AVG - 117)) | (1L << (CURRENT - 117)) | (1L << (ENABLE - 117)) | (1L << (DISABLE - 117)) | (1L << (INSTANCE - 117)) | (1L << (DO - 117)) | (1L << (DEFINER - 117)) | (1L << (CASCADED - 117)) | (1L << (LOCAL - 117)) | (1L << (CLOSE - 117)) | (1L << (OPEN - 117)) | (1L << (NEXT - 117)) | (1L << (NAME - 117)) | (1L << (TYPE - 117)))) != 0) || ((((_la - 185)) & ~0x3f) == 0 && ((1L << (_la - 185)) & ((1L << (TABLES - 185)) | (1L << (TABLESPACE - 185)) | (1L << (COLUMNS - 185)) | (1L << (FIELDS - 185)) | (1L << (INDEXES - 185)) | (1L << (STATUS - 185)) | (1L << (MODIFY - 185)) | (1L << (VALUE - 185)) | (1L << (DUPLICATE - 185)) | (1L << (FIRST - 185)) | (1L << (LAST - 185)) | (1L << (AFTER - 185)) | (1L << (OJ - 185)) | (1L << (ACCOUNT - 185)) | (1L << (USER - 185)) | (1L << (ROLE - 185)) | (1L << (START - 185)) | (1L << (TRANSACTION - 185)) | (1L << (WITHOUT - 185)) | (1L << (ESCAPE - 185)) | (1L << (SUBPARTITION - 185)) | (1L << (STORAGE - 185)) | (1L << (SUPER - 185)) | (1L << (TEMPORARY - 185)) | (1L << (THAN - 185)) | (1L << (UNBOUNDED - 185)) | (1L << (SIGNED - 185)) | (1L << (UPGRADE - 185)) | (1L << (VALIDATION - 185)) | (1L << (ROLLUP - 185)) | (1L << (SOUNDS - 185)) | (1L << (UNKNOWN - 185)) | (1L << (OFF - 185)) | (1L << (ALWAYS - 185)) | (1L << (COMMITTED - 185)) | (1L << (LEVEL - 185)) | (1L << (NO - 185)) | (1L << (PASSWORD - 185)) | (1L << (PRIVILEGES - 185)))) != 0) || ((((_la - 250)) & ~0x3f) == 0 && ((1L << (_la - 250)) & ((1L << (ACTION - 250)) | (1L << (ALGORITHM - 250)) | (1L << (AUTOCOMMIT - 250)) | (1L << (BTREE - 250)) | (1L << (CHAIN - 250)) | (1L << (CHARSET - 250)) | (1L << (CHECKSUM - 250)) | (1L << (CIPHER - 250)) | (1L << (CLIENT - 250)) | (1L << (COALESCE - 250)) | (1L << (COMMENT - 250)) | (1L << (COMPACT - 250)) | (1L << (COMPRESSED - 250)) | (1L << (COMPRESSION - 250)) | (1L << (CONNECTION - 250)) | (1L << (CONSISTENT - 250)) | (1L << (DATA - 250)) | (1L << (DISCARD - 250)) | (1L << (DISK - 250)) | (1L << (ENCRYPTION - 250)) | (1L << (END - 250)) | (1L << (ENGINE - 250)) | (1L << (EVENT - 250)) | (1L << (EXCHANGE - 250)) | (1L << (EXECUTE - 250)) | (1L << (FILE - 250)) | (1L << (FIXED - 250)) | (1L << (FOLLOWING - 250)) | (1L << (GLOBAL - 250)) | (1L << (HASH - 250)) | (1L << (IMPORT_ - 250)) | (1L << (LESS - 250)) | (1L << (MEMORY - 250)) | (1L << (NONE - 250)) | (1L << (PARSER - 250)) | (1L << (PARTIAL - 250)))) != 0) || ((((_la - 314)) & ~0x3f) == 0 && ((1L << (_la - 314)) & ((1L << (PARTITIONING - 314)) | (1L << (PERSIST - 314)) | (1L << (PRECEDING - 314)) | (1L << (PROCESS - 314)) | (1L << (PROXY - 314)) | (1L << (QUICK - 314)) | (1L << (REBUILD - 314)) | (1L << (REDUNDANT - 314)) | (1L << (RELOAD - 314)) | (1L << (REMOVE - 314)) | (1L << (REORGANIZE - 314)) | (1L << (REPAIR - 314)) | (1L << (REVERSE - 314)) | (1L << (SESSION - 314)) | (1L << (SHUTDOWN - 314)) | (1L << (SIMPLE - 314)) | (1L << (SLAVE - 314)) | (1L << (VISIBLE - 314)) | (1L << (INVISIBLE - 314)) | (1L << (ENFORCED - 314)) | (1L << (AGAINST - 314)) | (1L << (LANGUAGE - 314)) | (1L << (MODE - 314)) | (1L << (QUERY - 314)) | (1L << (EXTENDED - 314)) | (1L << (EXPANSION - 314)) | (1L << (VARIANCE - 314)) | (1L << (MAX_ROWS - 314)) | (1L << (MIN_ROWS - 314)) | (1L << (SQL_BIG_RESULT - 314)) | (1L << (SQL_BUFFER_RESULT - 314)) | (1L << (SQL_CACHE - 314)) | (1L << (SQL_NO_CACHE - 314)) | (1L << (STATS_AUTO_RECALC - 314)) | (1L << (STATS_PERSISTENT - 314)) | (1L << (STATS_SAMPLE_PAGES - 314)) | (1L << (ROW_FORMAT - 314)) | (1L << (WEIGHT_STRING - 314)) | (1L << (COLUMN_FORMAT - 314)))) != 0) || ((((_la - 378)) & ~0x3f) == 0 && ((1L << (_la - 378)) & ((1L << (INSERT_METHOD - 378)) | (1L << (KEY_BLOCK_SIZE - 378)) | (1L << (PACK_KEYS - 378)) | (1L << (PERSIST_ONLY - 378)) | (1L << (BIT_AND - 378)) | (1L << (BIT_OR - 378)) | (1L << (BIT_XOR - 378)) | (1L << (GROUP_CONCAT - 378)) | (1L << (JSON_ARRAYAGG - 378)) | (1L << (JSON_OBJECTAGG - 378)) | (1L << (STD - 378)) | (1L << (STDDEV - 378)) | (1L << (STDDEV_POP - 378)) | (1L << (STDDEV_SAMP - 378)) | (1L << (VAR_POP - 378)) | (1L << (VAR_SAMP - 378)) | (1L << (AUTO_INCREMENT - 378)) | (1L << (AVG_ROW_LENGTH - 378)) | (1L << (DELAY_KEY_WRITE - 378)) | (1L << (ROTATE - 378)) | (1L << (MASTER - 378)) | (1L << (BINLOG - 378)) | (1L << (ERROR - 378)) | (1L << (SCHEDULE - 378)) | (1L << (COMPLETION - 378)) | (1L << (EVERY - 378)) | (1L << (HOST - 378)) | (1L << (SOCKET - 378)) | (1L << (PORT - 378)) | (1L << (SERVER - 378)) | (1L << (WRAPPER - 378)) | (1L << (OPTIONS - 378)) | (1L << (OWNER - 378)) | (1L << (RETURNS - 378)) | (1L << (CONTAINS - 378)) | (1L << (SECURITY - 378)) | (1L << (INVOKER - 378)) | (1L << (TEMPTABLE - 378)) | (1L << (MERGE - 378)))) != 0) || ((((_la - 442)) & ~0x3f) == 0 && ((1L << (_la - 442)) & ((1L << (UNDEFINED - 442)) | (1L << (DATAFILE - 442)) | (1L << (FILE_BLOCK_SIZE - 442)) | (1L << (EXTENT_SIZE - 442)) | (1L << (INITIAL_SIZE - 442)) | (1L << (AUTOEXTEND_SIZE - 442)) | (1L << (MAX_SIZE - 442)) | (1L << (NODEGROUP - 442)) | (1L << (WAIT - 442)) | (1L << (LOGFILE - 442)) | (1L << (UNDOFILE - 442)) | (1L << (UNDO_BUFFER_SIZE - 442)) | (1L << (REDO_BUFFER_SIZE - 442)) | (1L << (HANDLER - 442)) | (1L << (PREV - 442)) | (1L << (ORGANIZATION - 442)) | (1L << (DEFINITION - 442)) | (1L << (DESCRIPTION - 442)) | (1L << (REFERENCE - 442)) | (1L << (FOLLOWS - 442)) | (1L << (PRECEDES - 442)) | (1L << (IMPORT - 442)) | (1L << (CONCURRENT - 442)) | (1L << (XML - 442)) | (1L << (DUMPFILE - 442)) | (1L << (SHARE - 442)) | (1L << (CODE - 442)) | (1L << (CONTEXT - 442)) | (1L << (SOURCE - 442)) | (1L << (CHANNEL - 442)))) != 0) || ((((_la - 506)) & ~0x3f) == 0 && ((1L << (_la - 506)) & ((1L << (CLONE - 506)) | (1L << (AGGREGATE - 506)) | (1L << (INSTALL - 506)) | (1L << (COMPONENT - 506)) | (1L << (UNINSTALL - 506)) | (1L << (RESOURCE - 506)) | (1L << (EXPIRE - 506)) | (1L << (NEVER - 506)) | (1L << (HISTORY - 506)) | (1L << (OPTIONAL - 506)) | (1L << (REUSE - 506)) | (1L << (MAX_QUERIES_PER_HOUR - 506)) | (1L << (MAX_UPDATES_PER_HOUR - 506)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 506)) | (1L << (MAX_USER_CONNECTIONS - 506)) | (1L << (RETAIN - 506)) | (1L << (RANDOM - 506)) | (1L << (OLD - 506)) | (1L << (ISSUER - 506)) | (1L << (SUBJECT - 506)) | (1L << (CACHE - 506)) | (1L << (GENERAL - 506)) | (1L << (SLOW - 506)) | (1L << (USER_RESOURCES - 506)) | (1L << (EXPORT - 506)) | (1L << (RELAY - 506)) | (1L << (HOSTS - 506)) | (1L << (FLUSH - 506)) | (1L << (RESET - 506)) | (1L << (RESTART - 506)))) != 0) || ((((_la - 686)) & ~0x3f) == 0 && ((1L << (_la - 686)) & ((1L << (IO_THREAD - 686)) | (1L << (SQL_THREAD - 686)) | (1L << (SQL_BEFORE_GTIDS - 686)) | (1L << (SQL_AFTER_GTIDS - 686)) | (1L << (MASTER_LOG_FILE - 686)) | (1L << (MASTER_LOG_POS - 686)) | (1L << (RELAY_LOG_FILE - 686)) | (1L << (RELAY_LOG_POS - 686)) | (1L << (SQL_AFTER_MTS_GAPS - 686)) | (1L << (UNTIL - 686)) | (1L << (DEFAULT_AUTH - 686)) | (1L << (PLUGIN_DIR - 686)) | (1L << (STOP - 686)) | (1L << (IDENTIFIER_ - 686)))) != 0)) {
				{
				setState(1543);
				identifier();
				}
			}

			setState(1547);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARTITION) {
				{
				setState(1546);
				partitionClause_();
				}
			}

			setState(1550);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(1549);
				orderByClause();
				}
			}

			setState(1553);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ROWS || _la==RANGE) {
				{
				setState(1552);
				frameClause_();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PartitionClause_Context extends ParserRuleContext {
		public TerminalNode PARTITION() { return getToken(DMLStatementParser.PARTITION, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public PartitionClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_partitionClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPartitionClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPartitionClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPartitionClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PartitionClause_Context partitionClause_() throws RecognitionException {
		PartitionClause_Context _localctx = new PartitionClause_Context(_ctx, getState());
		enterRule(_localctx, 248, RULE_partitionClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1555);
			match(PARTITION);
			setState(1556);
			match(BY);
			setState(1557);
			expr(0);
			setState(1562);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1558);
				match(COMMA_);
				setState(1559);
				expr(0);
				}
				}
				setState(1564);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameClause_Context extends ParserRuleContext {
		public TerminalNode ROWS() { return getToken(DMLStatementParser.ROWS, 0); }
		public TerminalNode RANGE() { return getToken(DMLStatementParser.RANGE, 0); }
		public FrameStart_Context frameStart_() {
			return getRuleContext(FrameStart_Context.class,0);
		}
		public FrameBetween_Context frameBetween_() {
			return getRuleContext(FrameBetween_Context.class,0);
		}
		public FrameClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFrameClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFrameClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFrameClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameClause_Context frameClause_() throws RecognitionException {
		FrameClause_Context _localctx = new FrameClause_Context(_ctx, getState());
		enterRule(_localctx, 250, RULE_frameClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1565);
			_la = _input.LA(1);
			if ( !(_la==ROWS || _la==RANGE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1568);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(1566);
				frameStart_();
				}
				break;
			case BETWEEN:
				{
				setState(1567);
				frameBetween_();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameStart_Context extends ParserRuleContext {
		public TerminalNode CURRENT() { return getToken(DMLStatementParser.CURRENT, 0); }
		public TerminalNode ROW() { return getToken(DMLStatementParser.ROW, 0); }
		public TerminalNode UNBOUNDED() { return getToken(DMLStatementParser.UNBOUNDED, 0); }
		public TerminalNode PRECEDING() { return getToken(DMLStatementParser.PRECEDING, 0); }
		public TerminalNode FOLLOWING() { return getToken(DMLStatementParser.FOLLOWING, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public FrameStart_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameStart_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFrameStart_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFrameStart_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFrameStart_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameStart_Context frameStart_() throws RecognitionException {
		FrameStart_Context _localctx = new FrameStart_Context(_ctx, getState());
		enterRule(_localctx, 252, RULE_frameStart_);
		try {
			setState(1582);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,201,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1570);
				match(CURRENT);
				setState(1571);
				match(ROW);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1572);
				match(UNBOUNDED);
				setState(1573);
				match(PRECEDING);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1574);
				match(UNBOUNDED);
				setState(1575);
				match(FOLLOWING);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1576);
				expr(0);
				setState(1577);
				match(PRECEDING);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1579);
				expr(0);
				setState(1580);
				match(FOLLOWING);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameEnd_Context extends ParserRuleContext {
		public FrameStart_Context frameStart_() {
			return getRuleContext(FrameStart_Context.class,0);
		}
		public FrameEnd_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameEnd_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFrameEnd_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFrameEnd_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFrameEnd_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameEnd_Context frameEnd_() throws RecognitionException {
		FrameEnd_Context _localctx = new FrameEnd_Context(_ctx, getState());
		enterRule(_localctx, 254, RULE_frameEnd_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1584);
			frameStart_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FrameBetween_Context extends ParserRuleContext {
		public TerminalNode BETWEEN() { return getToken(DMLStatementParser.BETWEEN, 0); }
		public FrameStart_Context frameStart_() {
			return getRuleContext(FrameStart_Context.class,0);
		}
		public TerminalNode AND() { return getToken(DMLStatementParser.AND, 0); }
		public FrameEnd_Context frameEnd_() {
			return getRuleContext(FrameEnd_Context.class,0);
		}
		public FrameBetween_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_frameBetween_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterFrameBetween_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitFrameBetween_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitFrameBetween_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FrameBetween_Context frameBetween_() throws RecognitionException {
		FrameBetween_Context _localctx = new FrameBetween_Context(_ctx, getState());
		enterRule(_localctx, 256, RULE_frameBetween_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1586);
			match(BETWEEN);
			setState(1587);
			frameStart_();
			setState(1588);
			match(AND);
			setState(1589);
			frameEnd_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpecialFunctionContext extends ParserRuleContext {
		public GroupConcatFunctionContext groupConcatFunction() {
			return getRuleContext(GroupConcatFunctionContext.class,0);
		}
		public WindowFunctionContext windowFunction() {
			return getRuleContext(WindowFunctionContext.class,0);
		}
		public CastFunctionContext castFunction() {
			return getRuleContext(CastFunctionContext.class,0);
		}
		public ConvertFunctionContext convertFunction() {
			return getRuleContext(ConvertFunctionContext.class,0);
		}
		public PositionFunctionContext positionFunction() {
			return getRuleContext(PositionFunctionContext.class,0);
		}
		public SubstringFunctionContext substringFunction() {
			return getRuleContext(SubstringFunctionContext.class,0);
		}
		public ExtractFunctionContext extractFunction() {
			return getRuleContext(ExtractFunctionContext.class,0);
		}
		public CharFunctionContext charFunction() {
			return getRuleContext(CharFunctionContext.class,0);
		}
		public TrimFunction_Context trimFunction_() {
			return getRuleContext(TrimFunction_Context.class,0);
		}
		public WeightStringFunctionContext weightStringFunction() {
			return getRuleContext(WeightStringFunctionContext.class,0);
		}
		public ValuesFunction_Context valuesFunction_() {
			return getRuleContext(ValuesFunction_Context.class,0);
		}
		public SpecialFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_specialFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSpecialFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSpecialFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSpecialFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpecialFunctionContext specialFunction() throws RecognitionException {
		SpecialFunctionContext _localctx = new SpecialFunctionContext(_ctx, getState());
		enterRule(_localctx, 258, RULE_specialFunction);
		try {
			setState(1602);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,202,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1591);
				groupConcatFunction();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1592);
				windowFunction();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1593);
				castFunction();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1594);
				convertFunction();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1595);
				positionFunction();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1596);
				substringFunction();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1597);
				extractFunction();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1598);
				charFunction();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1599);
				trimFunction_();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1600);
				weightStringFunction();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1601);
				valuesFunction_();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupConcatFunctionContext extends ParserRuleContext {
		public TerminalNode GROUP_CONCAT() { return getToken(DMLStatementParser.GROUP_CONCAT, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public DistinctContext distinct() {
			return getRuleContext(DistinctContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASTERISK_() { return getToken(DMLStatementParser.ASTERISK_, 0); }
		public OrderByClauseContext orderByClause() {
			return getRuleContext(OrderByClauseContext.class,0);
		}
		public TerminalNode SEPARATOR() { return getToken(DMLStatementParser.SEPARATOR, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public GroupConcatFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupConcatFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterGroupConcatFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitGroupConcatFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitGroupConcatFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupConcatFunctionContext groupConcatFunction() throws RecognitionException {
		GroupConcatFunctionContext _localctx = new GroupConcatFunctionContext(_ctx, getState());
		enterRule(_localctx, 260, RULE_groupConcatFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1604);
			match(GROUP_CONCAT);
			setState(1605);
			match(LP_);
			setState(1607);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DISTINCT) {
				{
				setState(1606);
				distinct();
				}
			}

			setState(1618);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(1609);
				expr(0);
				setState(1614);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1610);
					match(COMMA_);
					setState(1611);
					expr(0);
					}
					}
					setState(1616);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ASTERISK_:
				{
				setState(1617);
				match(ASTERISK_);
				}
				break;
			case RP_:
			case ORDER:
			case SEPARATOR:
				break;
			default:
				break;
			}
			setState(1621);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ORDER) {
				{
				setState(1620);
				orderByClause();
				}
			}

			setState(1625);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEPARATOR) {
				{
				setState(1623);
				match(SEPARATOR);
				setState(1624);
				expr(0);
				}
			}

			setState(1627);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowFunctionContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public OverClause_Context overClause_() {
			return getRuleContext(OverClause_Context.class,0);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public WindowFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWindowFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWindowFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWindowFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowFunctionContext windowFunction() throws RecognitionException {
		WindowFunctionContext _localctx = new WindowFunctionContext(_ctx, getState());
		enterRule(_localctx, 262, RULE_windowFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1629);
			identifier();
			setState(1630);
			match(LP_);
			setState(1631);
			expr(0);
			setState(1636);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1632);
				match(COMMA_);
				setState(1633);
				expr(0);
				}
				}
				setState(1638);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1639);
			match(RP_);
			setState(1640);
			overClause_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastFunctionContext extends ParserRuleContext {
		public TerminalNode CAST() { return getToken(DMLStatementParser.CAST, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public CastFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_castFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCastFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCastFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCastFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CastFunctionContext castFunction() throws RecognitionException {
		CastFunctionContext _localctx = new CastFunctionContext(_ctx, getState());
		enterRule(_localctx, 264, RULE_castFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1642);
			match(CAST);
			setState(1643);
			match(LP_);
			setState(1644);
			expr(0);
			setState(1645);
			match(AS);
			setState(1646);
			dataType();
			setState(1647);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConvertFunctionContext extends ParserRuleContext {
		public TerminalNode CONVERT() { return getToken(DMLStatementParser.CONVERT, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode COMMA_() { return getToken(DMLStatementParser.COMMA_, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode USING() { return getToken(DMLStatementParser.USING, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ConvertFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_convertFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterConvertFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitConvertFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitConvertFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConvertFunctionContext convertFunction() throws RecognitionException {
		ConvertFunctionContext _localctx = new ConvertFunctionContext(_ctx, getState());
		enterRule(_localctx, 266, RULE_convertFunction);
		try {
			setState(1663);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,209,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1649);
				match(CONVERT);
				setState(1650);
				match(LP_);
				setState(1651);
				expr(0);
				setState(1652);
				match(COMMA_);
				setState(1653);
				dataType();
				setState(1654);
				match(RP_);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1656);
				match(CONVERT);
				setState(1657);
				match(LP_);
				setState(1658);
				expr(0);
				setState(1659);
				match(USING);
				setState(1660);
				identifier();
				setState(1661);
				match(RP_);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PositionFunctionContext extends ParserRuleContext {
		public TerminalNode POSITION() { return getToken(DMLStatementParser.POSITION, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode IN() { return getToken(DMLStatementParser.IN, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public PositionFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_positionFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterPositionFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitPositionFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitPositionFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PositionFunctionContext positionFunction() throws RecognitionException {
		PositionFunctionContext _localctx = new PositionFunctionContext(_ctx, getState());
		enterRule(_localctx, 268, RULE_positionFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1665);
			match(POSITION);
			setState(1666);
			match(LP_);
			setState(1667);
			expr(0);
			setState(1668);
			match(IN);
			setState(1669);
			expr(0);
			setState(1670);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubstringFunctionContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public List<TerminalNode> NUMBER_() { return getTokens(DMLStatementParser.NUMBER_); }
		public TerminalNode NUMBER_(int i) {
			return getToken(DMLStatementParser.NUMBER_, i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode SUBSTRING() { return getToken(DMLStatementParser.SUBSTRING, 0); }
		public TerminalNode SUBSTR() { return getToken(DMLStatementParser.SUBSTR, 0); }
		public TerminalNode FOR() { return getToken(DMLStatementParser.FOR, 0); }
		public SubstringFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_substringFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterSubstringFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitSubstringFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitSubstringFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubstringFunctionContext substringFunction() throws RecognitionException {
		SubstringFunctionContext _localctx = new SubstringFunctionContext(_ctx, getState());
		enterRule(_localctx, 270, RULE_substringFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1672);
			_la = _input.LA(1);
			if ( !(_la==SUBSTRING || _la==SUBSTR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1673);
			match(LP_);
			setState(1674);
			expr(0);
			setState(1675);
			match(FROM);
			setState(1676);
			match(NUMBER_);
			setState(1679);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FOR) {
				{
				setState(1677);
				match(FOR);
				setState(1678);
				match(NUMBER_);
				}
			}

			setState(1681);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExtractFunctionContext extends ParserRuleContext {
		public TerminalNode EXTRACT() { return getToken(DMLStatementParser.EXTRACT, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public ExtractFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_extractFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterExtractFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitExtractFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitExtractFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExtractFunctionContext extractFunction() throws RecognitionException {
		ExtractFunctionContext _localctx = new ExtractFunctionContext(_ctx, getState());
		enterRule(_localctx, 272, RULE_extractFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1683);
			match(EXTRACT);
			setState(1684);
			match(LP_);
			setState(1685);
			identifier();
			setState(1686);
			match(FROM);
			setState(1687);
			expr(0);
			setState(1688);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharFunctionContext extends ParserRuleContext {
		public TerminalNode CHAR() { return getToken(DMLStatementParser.CHAR, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public TerminalNode USING() { return getToken(DMLStatementParser.USING, 0); }
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public CharFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_charFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCharFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCharFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCharFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharFunctionContext charFunction() throws RecognitionException {
		CharFunctionContext _localctx = new CharFunctionContext(_ctx, getState());
		enterRule(_localctx, 274, RULE_charFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1690);
			match(CHAR);
			setState(1691);
			match(LP_);
			setState(1692);
			expr(0);
			setState(1697);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1693);
				match(COMMA_);
				setState(1694);
				expr(0);
				}
				}
				setState(1699);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1702);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==USING) {
				{
				setState(1700);
				match(USING);
				setState(1701);
				ignoredIdentifier_();
				}
			}

			setState(1704);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TrimFunction_Context extends ParserRuleContext {
		public TerminalNode TRIM() { return getToken(DMLStatementParser.TRIM, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public TerminalNode FROM() { return getToken(DMLStatementParser.FROM, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode LEADING() { return getToken(DMLStatementParser.LEADING, 0); }
		public TerminalNode BOTH() { return getToken(DMLStatementParser.BOTH, 0); }
		public TerminalNode TRAILING() { return getToken(DMLStatementParser.TRAILING, 0); }
		public TrimFunction_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trimFunction_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterTrimFunction_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitTrimFunction_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitTrimFunction_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TrimFunction_Context trimFunction_() throws RecognitionException {
		TrimFunction_Context _localctx = new TrimFunction_Context(_ctx, getState());
		enterRule(_localctx, 276, RULE_trimFunction_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1706);
			match(TRIM);
			setState(1707);
			match(LP_);
			setState(1708);
			_la = _input.LA(1);
			if ( !(_la==TRAILING || _la==BOTH || _la==LEADING) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1709);
			match(STRING_);
			setState(1710);
			match(FROM);
			setState(1711);
			match(STRING_);
			setState(1712);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValuesFunction_Context extends ParserRuleContext {
		public TerminalNode VALUES() { return getToken(DMLStatementParser.VALUES, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public ValuesFunction_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valuesFunction_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterValuesFunction_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitValuesFunction_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitValuesFunction_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValuesFunction_Context valuesFunction_() throws RecognitionException {
		ValuesFunction_Context _localctx = new ValuesFunction_Context(_ctx, getState());
		enterRule(_localctx, 278, RULE_valuesFunction_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1714);
			match(VALUES);
			setState(1715);
			match(LP_);
			setState(1716);
			columnName();
			setState(1717);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WeightStringFunctionContext extends ParserRuleContext {
		public TerminalNode WEIGHT_STRING() { return getToken(DMLStatementParser.WEIGHT_STRING, 0); }
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode AS() { return getToken(DMLStatementParser.AS, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public LevelClause_Context levelClause_() {
			return getRuleContext(LevelClause_Context.class,0);
		}
		public WeightStringFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_weightStringFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterWeightStringFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitWeightStringFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitWeightStringFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WeightStringFunctionContext weightStringFunction() throws RecognitionException {
		WeightStringFunctionContext _localctx = new WeightStringFunctionContext(_ctx, getState());
		enterRule(_localctx, 280, RULE_weightStringFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1719);
			match(WEIGHT_STRING);
			setState(1720);
			match(LP_);
			setState(1721);
			expr(0);
			setState(1724);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==AS) {
				{
				setState(1722);
				match(AS);
				setState(1723);
				dataType();
				}
			}

			setState(1727);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LEVEL) {
				{
				setState(1726);
				levelClause_();
				}
			}

			setState(1729);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LevelClause_Context extends ParserRuleContext {
		public TerminalNode LEVEL() { return getToken(DMLStatementParser.LEVEL, 0); }
		public List<LevelInWeightListElement_Context> levelInWeightListElement_() {
			return getRuleContexts(LevelInWeightListElement_Context.class);
		}
		public LevelInWeightListElement_Context levelInWeightListElement_(int i) {
			return getRuleContext(LevelInWeightListElement_Context.class,i);
		}
		public List<TerminalNode> NUMBER_() { return getTokens(DMLStatementParser.NUMBER_); }
		public TerminalNode NUMBER_(int i) {
			return getToken(DMLStatementParser.NUMBER_, i);
		}
		public TerminalNode MINUS_() { return getToken(DMLStatementParser.MINUS_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public LevelClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_levelClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLevelClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLevelClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLevelClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LevelClause_Context levelClause_() throws RecognitionException {
		LevelClause_Context _localctx = new LevelClause_Context(_ctx, getState());
		enterRule(_localctx, 282, RULE_levelClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1731);
			match(LEVEL);
			setState(1743);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,216,_ctx) ) {
			case 1:
				{
				setState(1732);
				levelInWeightListElement_();
				setState(1737);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1733);
					match(COMMA_);
					setState(1734);
					levelInWeightListElement_();
					}
					}
					setState(1739);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				{
				setState(1740);
				match(NUMBER_);
				setState(1741);
				match(MINUS_);
				setState(1742);
				match(NUMBER_);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LevelInWeightListElement_Context extends ParserRuleContext {
		public TerminalNode NUMBER_() { return getToken(DMLStatementParser.NUMBER_, 0); }
		public TerminalNode REVERSE() { return getToken(DMLStatementParser.REVERSE, 0); }
		public TerminalNode ASC() { return getToken(DMLStatementParser.ASC, 0); }
		public TerminalNode DESC() { return getToken(DMLStatementParser.DESC, 0); }
		public LevelInWeightListElement_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_levelInWeightListElement_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterLevelInWeightListElement_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitLevelInWeightListElement_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitLevelInWeightListElement_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LevelInWeightListElement_Context levelInWeightListElement_() throws RecognitionException {
		LevelInWeightListElement_Context _localctx = new LevelInWeightListElement_Context(_ctx, getState());
		enterRule(_localctx, 284, RULE_levelInWeightListElement_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1745);
			match(NUMBER_);
			setState(1747);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASC || _la==DESC) {
				{
				setState(1746);
				_la = _input.LA(1);
				if ( !(_la==ASC || _la==DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(1750);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==REVERSE) {
				{
				setState(1749);
				match(REVERSE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegularFunctionContext extends ParserRuleContext {
		public RegularFunctionName_Context regularFunctionName_() {
			return getRuleContext(RegularFunctionName_Context.class,0);
		}
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ASTERISK_() { return getToken(DMLStatementParser.ASTERISK_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public RegularFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_regularFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterRegularFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitRegularFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitRegularFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegularFunctionContext regularFunction() throws RecognitionException {
		RegularFunctionContext _localctx = new RegularFunctionContext(_ctx, getState());
		enterRule(_localctx, 286, RULE_regularFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1752);
			regularFunctionName_();
			setState(1753);
			match(LP_);
			setState(1763);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT_:
			case TILDE_:
			case PLUS_:
			case MINUS_:
			case DOT_:
			case LP_:
			case LBE_:
			case QUESTION_:
			case AT_:
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case VALUES:
			case CASE:
			case CAST:
			case TRIM:
			case SUBSTRING:
			case LEFT:
			case RIGHT:
			case IF:
			case NOT:
			case NULL:
			case TRUE:
			case FALSE:
			case EXISTS:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case CHAR:
			case INTERVAL:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case LOCALTIME:
			case LOCALTIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case DATABASE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case REPLACE:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case MOD:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case ROW:
			case WITHOUT:
			case BINARY:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case SUBSTR:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case CONVERT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case EXTRACT:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MATCH:
			case MEMORY:
			case NONE:
			case NOW:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case CURRENT_TIMESTAMP:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case UNIX_TIMESTAMP:
			case LOWER:
			case UPPER:
			case ADDDATE:
			case ADDTIME:
			case DATE_ADD:
			case DATE_SUB:
			case DATEDIFF:
			case DATE_FORMAT:
			case DAYNAME:
			case DAYOFMONTH:
			case DAYOFWEEK:
			case DAYOFYEAR:
			case STR_TO_DATE:
			case TIMEDIFF:
			case TIMESTAMPADD:
			case TIMESTAMPDIFF:
			case TIME_FORMAT:
			case TIME_TO_SEC:
			case AES_DECRYPT:
			case AES_ENCRYPT:
			case FROM_BASE64:
			case TO_BASE64:
			case GEOMCOLLECTION:
			case GEOMETRYCOLLECTION:
			case LINESTRING:
			case MULTILINESTRING:
			case MULTIPOINT:
			case MULTIPOLYGON:
			case POINT:
			case POLYGON:
			case ST_AREA:
			case ST_ASBINARY:
			case ST_ASGEOJSON:
			case ST_ASTEXT:
			case ST_ASWKB:
			case ST_ASWKT:
			case ST_BUFFER:
			case ST_BUFFER_STRATEGY:
			case ST_CENTROID:
			case ST_CONTAINS:
			case ST_CONVEXHULL:
			case ST_CROSSES:
			case ST_DIFFERENCE:
			case ST_DIMENSION:
			case ST_DISJOINT:
			case ST_DISTANCE:
			case ST_DISTANCE_SPHERE:
			case ST_ENDPOINT:
			case ST_ENVELOPE:
			case ST_EQUALS:
			case ST_EXTERIORRING:
			case ST_GEOHASH:
			case ST_GEOMCOLLFROMTEXT:
			case ST_GEOMCOLLFROMTXT:
			case ST_GEOMCOLLFROMWKB:
			case ST_GEOMETRYCOLLECTIONFROMTEXT:
			case ST_GEOMETRYCOLLECTIONFROMWKB:
			case ST_GEOMETRYFROMTEXT:
			case ST_GEOMETRYFROMWKB:
			case ST_GEOMETRYN:
			case ST_GEOMETRYTYPE:
			case ST_GEOMFROMGEOJSON:
			case ST_GEOMFROMTEXT:
			case ST_GEOMFROMWKB:
			case ST_INTERIORRINGN:
			case ST_INTERSECTION:
			case ST_INTERSECTS:
			case ST_ISCLOSED:
			case ST_ISEMPTY:
			case ST_ISSIMPLE:
			case ST_ISVALID:
			case ST_LATFROMGEOHASH:
			case ST_LATITUDE:
			case ST_LENGTH:
			case ST_LINEFROMTEXT:
			case ST_LINEFROMWKB:
			case ST_LINESTRINGFROMTEXT:
			case ST_LINESTRINGFROMWKB:
			case ST_LONGFROMGEOHASH:
			case ST_LONGITUDE:
			case ST_MAKEENVELOPE:
			case ST_MLINEFROMTEXT:
			case ST_MLINEFROMWKB:
			case ST_MULTILINESTRINGFROMTEXT:
			case ST_MULTILINESTRINGFROMWKB:
			case ST_MPOINTFROMTEXT:
			case ST_MPOINTFROMWKB:
			case ST_MULTIPOINTFROMTEXT:
			case ST_MULTIPOINTFROMWKB:
			case ST_MPOLYFROMTEXT:
			case ST_MPOLYFROMWKB:
			case ST_MULTIPOLYGONFROMTEXT:
			case ST_MULTIPOLYGONFROMWKB:
			case ST_NUMGEOMETRIES:
			case ST_NUMINTERIORRING:
			case ST_NUMINTERIORRINGS:
			case ST_NUMPOINTS:
			case ST_OVERLAPS:
			case ST_POINTFROMGEOHASH:
			case ST_POINTFROMTEXT:
			case ST_POINTFROMWKB:
			case ST_POINTN:
			case ST_POLYFROMTEXT:
			case ST_POLYFROMWKB:
			case ST_POLYGONFROMTEXT:
			case ST_POLYGONFROMWKB:
			case ST_SIMPLIFY:
			case ST_SRID:
			case ST_STARTPOINT:
			case ST_SWAPXY:
			case ST_SYMDIFFERENCE:
			case ST_TOUCHES:
			case ST_TRANSFORM:
			case ST_UNION:
			case ST_VALIDATE:
			case ST_WITHIN:
			case ST_X:
			case ST_Y:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
			case STRING_:
			case NUMBER_:
			case HEX_DIGIT_:
			case BIT_NUM_:
				{
				setState(1754);
				expr(0);
				setState(1759);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA_) {
					{
					{
					setState(1755);
					match(COMMA_);
					setState(1756);
					expr(0);
					}
					}
					setState(1761);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case ASTERISK_:
				{
				setState(1762);
				match(ASTERISK_);
				}
				break;
			case RP_:
				break;
			default:
				break;
			}
			setState(1765);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegularFunctionName_Context extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode IF() { return getToken(DMLStatementParser.IF, 0); }
		public TerminalNode CURRENT_TIMESTAMP() { return getToken(DMLStatementParser.CURRENT_TIMESTAMP, 0); }
		public TerminalNode UNIX_TIMESTAMP() { return getToken(DMLStatementParser.UNIX_TIMESTAMP, 0); }
		public TerminalNode LOCALTIME() { return getToken(DMLStatementParser.LOCALTIME, 0); }
		public TerminalNode LOCALTIMESTAMP() { return getToken(DMLStatementParser.LOCALTIMESTAMP, 0); }
		public TerminalNode NOW() { return getToken(DMLStatementParser.NOW, 0); }
		public TerminalNode REPLACE() { return getToken(DMLStatementParser.REPLACE, 0); }
		public TerminalNode INTERVAL() { return getToken(DMLStatementParser.INTERVAL, 0); }
		public TerminalNode SUBSTRING() { return getToken(DMLStatementParser.SUBSTRING, 0); }
		public TerminalNode MOD() { return getToken(DMLStatementParser.MOD, 0); }
		public TerminalNode DATABASE() { return getToken(DMLStatementParser.DATABASE, 0); }
		public TerminalNode LEFT() { return getToken(DMLStatementParser.LEFT, 0); }
		public TerminalNode RIGHT() { return getToken(DMLStatementParser.RIGHT, 0); }
		public TerminalNode LOWER() { return getToken(DMLStatementParser.LOWER, 0); }
		public TerminalNode UPPER() { return getToken(DMLStatementParser.UPPER, 0); }
		public TerminalNode DATE() { return getToken(DMLStatementParser.DATE, 0); }
		public TerminalNode DATEDIFF() { return getToken(DMLStatementParser.DATEDIFF, 0); }
		public TerminalNode DATE_FORMAT() { return getToken(DMLStatementParser.DATE_FORMAT, 0); }
		public TerminalNode DAY() { return getToken(DMLStatementParser.DAY, 0); }
		public TerminalNode DAYNAME() { return getToken(DMLStatementParser.DAYNAME, 0); }
		public TerminalNode DAYOFMONTH() { return getToken(DMLStatementParser.DAYOFMONTH, 0); }
		public TerminalNode DAYOFWEEK() { return getToken(DMLStatementParser.DAYOFWEEK, 0); }
		public TerminalNode DAYOFYEAR() { return getToken(DMLStatementParser.DAYOFYEAR, 0); }
		public TerminalNode GEOMCOLLECTION() { return getToken(DMLStatementParser.GEOMCOLLECTION, 0); }
		public TerminalNode GEOMETRYCOLLECTION() { return getToken(DMLStatementParser.GEOMETRYCOLLECTION, 0); }
		public TerminalNode LINESTRING() { return getToken(DMLStatementParser.LINESTRING, 0); }
		public TerminalNode MULTILINESTRING() { return getToken(DMLStatementParser.MULTILINESTRING, 0); }
		public TerminalNode MULTIPOINT() { return getToken(DMLStatementParser.MULTIPOINT, 0); }
		public TerminalNode MULTIPOLYGON() { return getToken(DMLStatementParser.MULTIPOLYGON, 0); }
		public TerminalNode POINT() { return getToken(DMLStatementParser.POINT, 0); }
		public TerminalNode POLYGON() { return getToken(DMLStatementParser.POLYGON, 0); }
		public TerminalNode STR_TO_DATE() { return getToken(DMLStatementParser.STR_TO_DATE, 0); }
		public TerminalNode ST_AREA() { return getToken(DMLStatementParser.ST_AREA, 0); }
		public TerminalNode ST_ASBINARY() { return getToken(DMLStatementParser.ST_ASBINARY, 0); }
		public TerminalNode ST_ASGEOJSON() { return getToken(DMLStatementParser.ST_ASGEOJSON, 0); }
		public TerminalNode ST_ASTEXT() { return getToken(DMLStatementParser.ST_ASTEXT, 0); }
		public TerminalNode ST_ASWKB() { return getToken(DMLStatementParser.ST_ASWKB, 0); }
		public TerminalNode ST_ASWKT() { return getToken(DMLStatementParser.ST_ASWKT, 0); }
		public TerminalNode ST_BUFFER() { return getToken(DMLStatementParser.ST_BUFFER, 0); }
		public TerminalNode ST_BUFFER_STRATEGY() { return getToken(DMLStatementParser.ST_BUFFER_STRATEGY, 0); }
		public TerminalNode ST_CENTROID() { return getToken(DMLStatementParser.ST_CENTROID, 0); }
		public TerminalNode ST_CONTAINS() { return getToken(DMLStatementParser.ST_CONTAINS, 0); }
		public TerminalNode ST_CONVEXHULL() { return getToken(DMLStatementParser.ST_CONVEXHULL, 0); }
		public TerminalNode ST_CROSSES() { return getToken(DMLStatementParser.ST_CROSSES, 0); }
		public TerminalNode ST_DIFFERENCE() { return getToken(DMLStatementParser.ST_DIFFERENCE, 0); }
		public TerminalNode ST_DIMENSION() { return getToken(DMLStatementParser.ST_DIMENSION, 0); }
		public TerminalNode ST_DISJOINT() { return getToken(DMLStatementParser.ST_DISJOINT, 0); }
		public TerminalNode ST_DISTANCE() { return getToken(DMLStatementParser.ST_DISTANCE, 0); }
		public TerminalNode ST_DISTANCE_SPHERE() { return getToken(DMLStatementParser.ST_DISTANCE_SPHERE, 0); }
		public TerminalNode ST_ENDPOINT() { return getToken(DMLStatementParser.ST_ENDPOINT, 0); }
		public TerminalNode ST_ENVELOPE() { return getToken(DMLStatementParser.ST_ENVELOPE, 0); }
		public TerminalNode ST_EQUALS() { return getToken(DMLStatementParser.ST_EQUALS, 0); }
		public TerminalNode ST_EXTERIORRING() { return getToken(DMLStatementParser.ST_EXTERIORRING, 0); }
		public TerminalNode ST_GEOHASH() { return getToken(DMLStatementParser.ST_GEOHASH, 0); }
		public TerminalNode ST_GEOMCOLLFROMTEXT() { return getToken(DMLStatementParser.ST_GEOMCOLLFROMTEXT, 0); }
		public TerminalNode ST_GEOMCOLLFROMTXT() { return getToken(DMLStatementParser.ST_GEOMCOLLFROMTXT, 0); }
		public TerminalNode ST_GEOMCOLLFROMWKB() { return getToken(DMLStatementParser.ST_GEOMCOLLFROMWKB, 0); }
		public TerminalNode ST_GEOMETRYCOLLECTIONFROMTEXT() { return getToken(DMLStatementParser.ST_GEOMETRYCOLLECTIONFROMTEXT, 0); }
		public TerminalNode ST_GEOMETRYCOLLECTIONFROMWKB() { return getToken(DMLStatementParser.ST_GEOMETRYCOLLECTIONFROMWKB, 0); }
		public TerminalNode ST_GEOMETRYFROMTEXT() { return getToken(DMLStatementParser.ST_GEOMETRYFROMTEXT, 0); }
		public TerminalNode ST_GEOMETRYFROMWKB() { return getToken(DMLStatementParser.ST_GEOMETRYFROMWKB, 0); }
		public TerminalNode ST_GEOMETRYN() { return getToken(DMLStatementParser.ST_GEOMETRYN, 0); }
		public TerminalNode ST_GEOMETRYTYPE() { return getToken(DMLStatementParser.ST_GEOMETRYTYPE, 0); }
		public TerminalNode ST_GEOMFROMGEOJSON() { return getToken(DMLStatementParser.ST_GEOMFROMGEOJSON, 0); }
		public TerminalNode ST_GEOMFROMTEXT() { return getToken(DMLStatementParser.ST_GEOMFROMTEXT, 0); }
		public TerminalNode ST_GEOMFROMWKB() { return getToken(DMLStatementParser.ST_GEOMFROMWKB, 0); }
		public TerminalNode ST_INTERIORRINGN() { return getToken(DMLStatementParser.ST_INTERIORRINGN, 0); }
		public TerminalNode ST_INTERSECTION() { return getToken(DMLStatementParser.ST_INTERSECTION, 0); }
		public TerminalNode ST_INTERSECTS() { return getToken(DMLStatementParser.ST_INTERSECTS, 0); }
		public TerminalNode ST_ISCLOSED() { return getToken(DMLStatementParser.ST_ISCLOSED, 0); }
		public TerminalNode ST_ISEMPTY() { return getToken(DMLStatementParser.ST_ISEMPTY, 0); }
		public TerminalNode ST_ISSIMPLE() { return getToken(DMLStatementParser.ST_ISSIMPLE, 0); }
		public TerminalNode ST_ISVALID() { return getToken(DMLStatementParser.ST_ISVALID, 0); }
		public TerminalNode ST_LATFROMGEOHASH() { return getToken(DMLStatementParser.ST_LATFROMGEOHASH, 0); }
		public TerminalNode ST_LATITUDE() { return getToken(DMLStatementParser.ST_LATITUDE, 0); }
		public TerminalNode ST_LENGTH() { return getToken(DMLStatementParser.ST_LENGTH, 0); }
		public TerminalNode ST_LINEFROMTEXT() { return getToken(DMLStatementParser.ST_LINEFROMTEXT, 0); }
		public TerminalNode ST_LINEFROMWKB() { return getToken(DMLStatementParser.ST_LINEFROMWKB, 0); }
		public TerminalNode ST_LINESTRINGFROMTEXT() { return getToken(DMLStatementParser.ST_LINESTRINGFROMTEXT, 0); }
		public TerminalNode ST_LINESTRINGFROMWKB() { return getToken(DMLStatementParser.ST_LINESTRINGFROMWKB, 0); }
		public TerminalNode ST_LONGFROMGEOHASH() { return getToken(DMLStatementParser.ST_LONGFROMGEOHASH, 0); }
		public TerminalNode ST_LONGITUDE() { return getToken(DMLStatementParser.ST_LONGITUDE, 0); }
		public TerminalNode ST_MAKEENVELOPE() { return getToken(DMLStatementParser.ST_MAKEENVELOPE, 0); }
		public TerminalNode ST_MLINEFROMTEXT() { return getToken(DMLStatementParser.ST_MLINEFROMTEXT, 0); }
		public TerminalNode ST_MLINEFROMWKB() { return getToken(DMLStatementParser.ST_MLINEFROMWKB, 0); }
		public TerminalNode ST_MULTILINESTRINGFROMTEXT() { return getToken(DMLStatementParser.ST_MULTILINESTRINGFROMTEXT, 0); }
		public TerminalNode ST_MULTILINESTRINGFROMWKB() { return getToken(DMLStatementParser.ST_MULTILINESTRINGFROMWKB, 0); }
		public TerminalNode ST_MPOINTFROMTEXT() { return getToken(DMLStatementParser.ST_MPOINTFROMTEXT, 0); }
		public TerminalNode ST_MPOINTFROMWKB() { return getToken(DMLStatementParser.ST_MPOINTFROMWKB, 0); }
		public TerminalNode ST_MULTIPOINTFROMTEXT() { return getToken(DMLStatementParser.ST_MULTIPOINTFROMTEXT, 0); }
		public TerminalNode ST_MULTIPOINTFROMWKB() { return getToken(DMLStatementParser.ST_MULTIPOINTFROMWKB, 0); }
		public TerminalNode ST_MPOLYFROMTEXT() { return getToken(DMLStatementParser.ST_MPOLYFROMTEXT, 0); }
		public TerminalNode ST_MPOLYFROMWKB() { return getToken(DMLStatementParser.ST_MPOLYFROMWKB, 0); }
		public TerminalNode ST_MULTIPOLYGONFROMTEXT() { return getToken(DMLStatementParser.ST_MULTIPOLYGONFROMTEXT, 0); }
		public TerminalNode ST_MULTIPOLYGONFROMWKB() { return getToken(DMLStatementParser.ST_MULTIPOLYGONFROMWKB, 0); }
		public TerminalNode ST_NUMGEOMETRIES() { return getToken(DMLStatementParser.ST_NUMGEOMETRIES, 0); }
		public TerminalNode ST_NUMINTERIORRING() { return getToken(DMLStatementParser.ST_NUMINTERIORRING, 0); }
		public TerminalNode ST_NUMINTERIORRINGS() { return getToken(DMLStatementParser.ST_NUMINTERIORRINGS, 0); }
		public TerminalNode ST_NUMPOINTS() { return getToken(DMLStatementParser.ST_NUMPOINTS, 0); }
		public TerminalNode ST_OVERLAPS() { return getToken(DMLStatementParser.ST_OVERLAPS, 0); }
		public TerminalNode ST_POINTFROMGEOHASH() { return getToken(DMLStatementParser.ST_POINTFROMGEOHASH, 0); }
		public TerminalNode ST_POINTFROMTEXT() { return getToken(DMLStatementParser.ST_POINTFROMTEXT, 0); }
		public TerminalNode ST_POINTFROMWKB() { return getToken(DMLStatementParser.ST_POINTFROMWKB, 0); }
		public TerminalNode ST_POINTN() { return getToken(DMLStatementParser.ST_POINTN, 0); }
		public TerminalNode ST_POLYFROMTEXT() { return getToken(DMLStatementParser.ST_POLYFROMTEXT, 0); }
		public TerminalNode ST_POLYFROMWKB() { return getToken(DMLStatementParser.ST_POLYFROMWKB, 0); }
		public TerminalNode ST_POLYGONFROMTEXT() { return getToken(DMLStatementParser.ST_POLYGONFROMTEXT, 0); }
		public TerminalNode ST_POLYGONFROMWKB() { return getToken(DMLStatementParser.ST_POLYGONFROMWKB, 0); }
		public TerminalNode ST_SIMPLIFY() { return getToken(DMLStatementParser.ST_SIMPLIFY, 0); }
		public TerminalNode ST_SRID() { return getToken(DMLStatementParser.ST_SRID, 0); }
		public TerminalNode ST_STARTPOINT() { return getToken(DMLStatementParser.ST_STARTPOINT, 0); }
		public TerminalNode ST_SWAPXY() { return getToken(DMLStatementParser.ST_SWAPXY, 0); }
		public TerminalNode ST_SYMDIFFERENCE() { return getToken(DMLStatementParser.ST_SYMDIFFERENCE, 0); }
		public TerminalNode ST_TOUCHES() { return getToken(DMLStatementParser.ST_TOUCHES, 0); }
		public TerminalNode ST_TRANSFORM() { return getToken(DMLStatementParser.ST_TRANSFORM, 0); }
		public TerminalNode ST_UNION() { return getToken(DMLStatementParser.ST_UNION, 0); }
		public TerminalNode ST_VALIDATE() { return getToken(DMLStatementParser.ST_VALIDATE, 0); }
		public TerminalNode ST_WITHIN() { return getToken(DMLStatementParser.ST_WITHIN, 0); }
		public TerminalNode ST_X() { return getToken(DMLStatementParser.ST_X, 0); }
		public TerminalNode ST_Y() { return getToken(DMLStatementParser.ST_Y, 0); }
		public TerminalNode TIME() { return getToken(DMLStatementParser.TIME, 0); }
		public TerminalNode TIMEDIFF() { return getToken(DMLStatementParser.TIMEDIFF, 0); }
		public TerminalNode TIMESTAMP() { return getToken(DMLStatementParser.TIMESTAMP, 0); }
		public TerminalNode TIMESTAMPADD() { return getToken(DMLStatementParser.TIMESTAMPADD, 0); }
		public TerminalNode TIMESTAMPDIFF() { return getToken(DMLStatementParser.TIMESTAMPDIFF, 0); }
		public TerminalNode TIME_FORMAT() { return getToken(DMLStatementParser.TIME_FORMAT, 0); }
		public TerminalNode TIME_TO_SEC() { return getToken(DMLStatementParser.TIME_TO_SEC, 0); }
		public TerminalNode AES_DECRYPT() { return getToken(DMLStatementParser.AES_DECRYPT, 0); }
		public TerminalNode AES_ENCRYPT() { return getToken(DMLStatementParser.AES_ENCRYPT, 0); }
		public TerminalNode FROM_BASE64() { return getToken(DMLStatementParser.FROM_BASE64, 0); }
		public TerminalNode TO_BASE64() { return getToken(DMLStatementParser.TO_BASE64, 0); }
		public TerminalNode ADDDATE() { return getToken(DMLStatementParser.ADDDATE, 0); }
		public TerminalNode ADDTIME() { return getToken(DMLStatementParser.ADDTIME, 0); }
		public TerminalNode DATE_ADD() { return getToken(DMLStatementParser.DATE_ADD, 0); }
		public TerminalNode DATE_SUB() { return getToken(DMLStatementParser.DATE_SUB, 0); }
		public RegularFunctionName_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_regularFunctionName_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterRegularFunctionName_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitRegularFunctionName_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitRegularFunctionName_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RegularFunctionName_Context regularFunctionName_() throws RecognitionException {
		RegularFunctionName_Context _localctx = new RegularFunctionName_Context(_ctx, getState());
		enterRule(_localctx, 288, RULE_regularFunctionName_);
		try {
			setState(1905);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,221,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1767);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1768);
				match(IF);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1769);
				match(CURRENT_TIMESTAMP);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1770);
				match(UNIX_TIMESTAMP);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1771);
				match(LOCALTIME);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1772);
				match(LOCALTIMESTAMP);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1773);
				match(NOW);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1774);
				match(REPLACE);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1775);
				match(INTERVAL);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1776);
				match(SUBSTRING);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1777);
				match(MOD);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1778);
				match(DATABASE);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(1779);
				match(LEFT);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(1780);
				match(RIGHT);
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(1781);
				match(LOWER);
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(1782);
				match(UPPER);
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(1783);
				match(DATE);
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(1784);
				match(DATEDIFF);
				}
				break;
			case 19:
				enterOuterAlt(_localctx, 19);
				{
				setState(1785);
				match(DATE_FORMAT);
				}
				break;
			case 20:
				enterOuterAlt(_localctx, 20);
				{
				setState(1786);
				match(DAY);
				}
				break;
			case 21:
				enterOuterAlt(_localctx, 21);
				{
				setState(1787);
				match(DAYNAME);
				}
				break;
			case 22:
				enterOuterAlt(_localctx, 22);
				{
				setState(1788);
				match(DAYOFMONTH);
				}
				break;
			case 23:
				enterOuterAlt(_localctx, 23);
				{
				setState(1789);
				match(DAYOFWEEK);
				}
				break;
			case 24:
				enterOuterAlt(_localctx, 24);
				{
				setState(1790);
				match(DAYOFYEAR);
				}
				break;
			case 25:
				enterOuterAlt(_localctx, 25);
				{
				setState(1791);
				match(GEOMCOLLECTION);
				}
				break;
			case 26:
				enterOuterAlt(_localctx, 26);
				{
				setState(1792);
				match(GEOMETRYCOLLECTION);
				}
				break;
			case 27:
				enterOuterAlt(_localctx, 27);
				{
				setState(1793);
				match(LINESTRING);
				}
				break;
			case 28:
				enterOuterAlt(_localctx, 28);
				{
				setState(1794);
				match(MULTILINESTRING);
				}
				break;
			case 29:
				enterOuterAlt(_localctx, 29);
				{
				setState(1795);
				match(MULTIPOINT);
				}
				break;
			case 30:
				enterOuterAlt(_localctx, 30);
				{
				setState(1796);
				match(MULTIPOLYGON);
				}
				break;
			case 31:
				enterOuterAlt(_localctx, 31);
				{
				setState(1797);
				match(POINT);
				}
				break;
			case 32:
				enterOuterAlt(_localctx, 32);
				{
				setState(1798);
				match(POLYGON);
				}
				break;
			case 33:
				enterOuterAlt(_localctx, 33);
				{
				setState(1799);
				match(STR_TO_DATE);
				}
				break;
			case 34:
				enterOuterAlt(_localctx, 34);
				{
				setState(1800);
				match(ST_AREA);
				}
				break;
			case 35:
				enterOuterAlt(_localctx, 35);
				{
				setState(1801);
				match(ST_ASBINARY);
				}
				break;
			case 36:
				enterOuterAlt(_localctx, 36);
				{
				setState(1802);
				match(ST_ASGEOJSON);
				}
				break;
			case 37:
				enterOuterAlt(_localctx, 37);
				{
				setState(1803);
				match(ST_ASTEXT);
				}
				break;
			case 38:
				enterOuterAlt(_localctx, 38);
				{
				setState(1804);
				match(ST_ASWKB);
				}
				break;
			case 39:
				enterOuterAlt(_localctx, 39);
				{
				setState(1805);
				match(ST_ASWKT);
				}
				break;
			case 40:
				enterOuterAlt(_localctx, 40);
				{
				setState(1806);
				match(ST_BUFFER);
				}
				break;
			case 41:
				enterOuterAlt(_localctx, 41);
				{
				setState(1807);
				match(ST_BUFFER_STRATEGY);
				}
				break;
			case 42:
				enterOuterAlt(_localctx, 42);
				{
				setState(1808);
				match(ST_CENTROID);
				}
				break;
			case 43:
				enterOuterAlt(_localctx, 43);
				{
				setState(1809);
				match(ST_CONTAINS);
				}
				break;
			case 44:
				enterOuterAlt(_localctx, 44);
				{
				setState(1810);
				match(ST_CONVEXHULL);
				}
				break;
			case 45:
				enterOuterAlt(_localctx, 45);
				{
				setState(1811);
				match(ST_CROSSES);
				}
				break;
			case 46:
				enterOuterAlt(_localctx, 46);
				{
				setState(1812);
				match(ST_DIFFERENCE);
				}
				break;
			case 47:
				enterOuterAlt(_localctx, 47);
				{
				setState(1813);
				match(ST_DIMENSION);
				}
				break;
			case 48:
				enterOuterAlt(_localctx, 48);
				{
				setState(1814);
				match(ST_DISJOINT);
				}
				break;
			case 49:
				enterOuterAlt(_localctx, 49);
				{
				setState(1815);
				match(ST_DISTANCE);
				}
				break;
			case 50:
				enterOuterAlt(_localctx, 50);
				{
				setState(1816);
				match(ST_DISTANCE_SPHERE);
				}
				break;
			case 51:
				enterOuterAlt(_localctx, 51);
				{
				setState(1817);
				match(ST_ENDPOINT);
				}
				break;
			case 52:
				enterOuterAlt(_localctx, 52);
				{
				setState(1818);
				match(ST_ENVELOPE);
				}
				break;
			case 53:
				enterOuterAlt(_localctx, 53);
				{
				setState(1819);
				match(ST_EQUALS);
				}
				break;
			case 54:
				enterOuterAlt(_localctx, 54);
				{
				setState(1820);
				match(ST_EXTERIORRING);
				}
				break;
			case 55:
				enterOuterAlt(_localctx, 55);
				{
				setState(1821);
				match(ST_GEOHASH);
				}
				break;
			case 56:
				enterOuterAlt(_localctx, 56);
				{
				setState(1822);
				match(ST_GEOMCOLLFROMTEXT);
				}
				break;
			case 57:
				enterOuterAlt(_localctx, 57);
				{
				setState(1823);
				match(ST_GEOMCOLLFROMTXT);
				}
				break;
			case 58:
				enterOuterAlt(_localctx, 58);
				{
				setState(1824);
				match(ST_GEOMCOLLFROMWKB);
				}
				break;
			case 59:
				enterOuterAlt(_localctx, 59);
				{
				setState(1825);
				match(ST_GEOMETRYCOLLECTIONFROMTEXT);
				}
				break;
			case 60:
				enterOuterAlt(_localctx, 60);
				{
				setState(1826);
				match(ST_GEOMETRYCOLLECTIONFROMWKB);
				}
				break;
			case 61:
				enterOuterAlt(_localctx, 61);
				{
				setState(1827);
				match(ST_GEOMETRYFROMTEXT);
				}
				break;
			case 62:
				enterOuterAlt(_localctx, 62);
				{
				setState(1828);
				match(ST_GEOMETRYFROMWKB);
				}
				break;
			case 63:
				enterOuterAlt(_localctx, 63);
				{
				setState(1829);
				match(ST_GEOMETRYN);
				}
				break;
			case 64:
				enterOuterAlt(_localctx, 64);
				{
				setState(1830);
				match(ST_GEOMETRYTYPE);
				}
				break;
			case 65:
				enterOuterAlt(_localctx, 65);
				{
				setState(1831);
				match(ST_GEOMFROMGEOJSON);
				}
				break;
			case 66:
				enterOuterAlt(_localctx, 66);
				{
				setState(1832);
				match(ST_GEOMFROMTEXT);
				}
				break;
			case 67:
				enterOuterAlt(_localctx, 67);
				{
				setState(1833);
				match(ST_GEOMFROMWKB);
				}
				break;
			case 68:
				enterOuterAlt(_localctx, 68);
				{
				setState(1834);
				match(ST_INTERIORRINGN);
				}
				break;
			case 69:
				enterOuterAlt(_localctx, 69);
				{
				setState(1835);
				match(ST_INTERSECTION);
				}
				break;
			case 70:
				enterOuterAlt(_localctx, 70);
				{
				setState(1836);
				match(ST_INTERSECTS);
				}
				break;
			case 71:
				enterOuterAlt(_localctx, 71);
				{
				setState(1837);
				match(ST_ISCLOSED);
				}
				break;
			case 72:
				enterOuterAlt(_localctx, 72);
				{
				setState(1838);
				match(ST_ISEMPTY);
				}
				break;
			case 73:
				enterOuterAlt(_localctx, 73);
				{
				setState(1839);
				match(ST_ISSIMPLE);
				}
				break;
			case 74:
				enterOuterAlt(_localctx, 74);
				{
				setState(1840);
				match(ST_ISVALID);
				}
				break;
			case 75:
				enterOuterAlt(_localctx, 75);
				{
				setState(1841);
				match(ST_LATFROMGEOHASH);
				}
				break;
			case 76:
				enterOuterAlt(_localctx, 76);
				{
				setState(1842);
				match(ST_LATITUDE);
				}
				break;
			case 77:
				enterOuterAlt(_localctx, 77);
				{
				setState(1843);
				match(ST_LENGTH);
				}
				break;
			case 78:
				enterOuterAlt(_localctx, 78);
				{
				setState(1844);
				match(ST_LINEFROMTEXT);
				}
				break;
			case 79:
				enterOuterAlt(_localctx, 79);
				{
				setState(1845);
				match(ST_LINEFROMWKB);
				}
				break;
			case 80:
				enterOuterAlt(_localctx, 80);
				{
				setState(1846);
				match(ST_LINESTRINGFROMTEXT);
				}
				break;
			case 81:
				enterOuterAlt(_localctx, 81);
				{
				setState(1847);
				match(ST_LINESTRINGFROMWKB);
				}
				break;
			case 82:
				enterOuterAlt(_localctx, 82);
				{
				setState(1848);
				match(ST_LONGFROMGEOHASH);
				}
				break;
			case 83:
				enterOuterAlt(_localctx, 83);
				{
				setState(1849);
				match(ST_LONGITUDE);
				}
				break;
			case 84:
				enterOuterAlt(_localctx, 84);
				{
				setState(1850);
				match(ST_MAKEENVELOPE);
				}
				break;
			case 85:
				enterOuterAlt(_localctx, 85);
				{
				setState(1851);
				match(ST_MLINEFROMTEXT);
				}
				break;
			case 86:
				enterOuterAlt(_localctx, 86);
				{
				setState(1852);
				match(ST_MLINEFROMWKB);
				}
				break;
			case 87:
				enterOuterAlt(_localctx, 87);
				{
				setState(1853);
				match(ST_MULTILINESTRINGFROMTEXT);
				}
				break;
			case 88:
				enterOuterAlt(_localctx, 88);
				{
				setState(1854);
				match(ST_MULTILINESTRINGFROMWKB);
				}
				break;
			case 89:
				enterOuterAlt(_localctx, 89);
				{
				setState(1855);
				match(ST_MPOINTFROMTEXT);
				}
				break;
			case 90:
				enterOuterAlt(_localctx, 90);
				{
				setState(1856);
				match(ST_MPOINTFROMWKB);
				}
				break;
			case 91:
				enterOuterAlt(_localctx, 91);
				{
				setState(1857);
				match(ST_MULTIPOINTFROMTEXT);
				}
				break;
			case 92:
				enterOuterAlt(_localctx, 92);
				{
				setState(1858);
				match(ST_MULTIPOINTFROMWKB);
				}
				break;
			case 93:
				enterOuterAlt(_localctx, 93);
				{
				setState(1859);
				match(ST_MPOLYFROMTEXT);
				}
				break;
			case 94:
				enterOuterAlt(_localctx, 94);
				{
				setState(1860);
				match(ST_MPOLYFROMWKB);
				}
				break;
			case 95:
				enterOuterAlt(_localctx, 95);
				{
				setState(1861);
				match(ST_MULTIPOLYGONFROMTEXT);
				}
				break;
			case 96:
				enterOuterAlt(_localctx, 96);
				{
				setState(1862);
				match(ST_MULTIPOLYGONFROMWKB);
				}
				break;
			case 97:
				enterOuterAlt(_localctx, 97);
				{
				setState(1863);
				match(ST_NUMGEOMETRIES);
				}
				break;
			case 98:
				enterOuterAlt(_localctx, 98);
				{
				setState(1864);
				match(ST_NUMINTERIORRING);
				}
				break;
			case 99:
				enterOuterAlt(_localctx, 99);
				{
				setState(1865);
				match(ST_NUMINTERIORRINGS);
				}
				break;
			case 100:
				enterOuterAlt(_localctx, 100);
				{
				setState(1866);
				match(ST_NUMPOINTS);
				}
				break;
			case 101:
				enterOuterAlt(_localctx, 101);
				{
				setState(1867);
				match(ST_OVERLAPS);
				}
				break;
			case 102:
				enterOuterAlt(_localctx, 102);
				{
				setState(1868);
				match(ST_POINTFROMGEOHASH);
				}
				break;
			case 103:
				enterOuterAlt(_localctx, 103);
				{
				setState(1869);
				match(ST_POINTFROMTEXT);
				}
				break;
			case 104:
				enterOuterAlt(_localctx, 104);
				{
				setState(1870);
				match(ST_POINTFROMWKB);
				}
				break;
			case 105:
				enterOuterAlt(_localctx, 105);
				{
				setState(1871);
				match(ST_POINTN);
				}
				break;
			case 106:
				enterOuterAlt(_localctx, 106);
				{
				setState(1872);
				match(ST_POLYFROMTEXT);
				}
				break;
			case 107:
				enterOuterAlt(_localctx, 107);
				{
				setState(1873);
				match(ST_POLYFROMWKB);
				}
				break;
			case 108:
				enterOuterAlt(_localctx, 108);
				{
				setState(1874);
				match(ST_POLYGONFROMTEXT);
				}
				break;
			case 109:
				enterOuterAlt(_localctx, 109);
				{
				setState(1875);
				match(ST_POLYGONFROMWKB);
				}
				break;
			case 110:
				enterOuterAlt(_localctx, 110);
				{
				setState(1876);
				match(ST_SIMPLIFY);
				}
				break;
			case 111:
				enterOuterAlt(_localctx, 111);
				{
				setState(1877);
				match(ST_SRID);
				}
				break;
			case 112:
				enterOuterAlt(_localctx, 112);
				{
				setState(1878);
				match(ST_STARTPOINT);
				}
				break;
			case 113:
				enterOuterAlt(_localctx, 113);
				{
				setState(1879);
				match(ST_SWAPXY);
				}
				break;
			case 114:
				enterOuterAlt(_localctx, 114);
				{
				setState(1880);
				match(ST_SYMDIFFERENCE);
				}
				break;
			case 115:
				enterOuterAlt(_localctx, 115);
				{
				setState(1881);
				match(ST_TOUCHES);
				}
				break;
			case 116:
				enterOuterAlt(_localctx, 116);
				{
				setState(1882);
				match(ST_TRANSFORM);
				}
				break;
			case 117:
				enterOuterAlt(_localctx, 117);
				{
				setState(1883);
				match(ST_UNION);
				}
				break;
			case 118:
				enterOuterAlt(_localctx, 118);
				{
				setState(1884);
				match(ST_VALIDATE);
				}
				break;
			case 119:
				enterOuterAlt(_localctx, 119);
				{
				setState(1885);
				match(ST_WITHIN);
				}
				break;
			case 120:
				enterOuterAlt(_localctx, 120);
				{
				setState(1886);
				match(ST_X);
				}
				break;
			case 121:
				enterOuterAlt(_localctx, 121);
				{
				setState(1887);
				match(ST_Y);
				}
				break;
			case 122:
				enterOuterAlt(_localctx, 122);
				{
				setState(1888);
				match(TIME);
				}
				break;
			case 123:
				enterOuterAlt(_localctx, 123);
				{
				setState(1889);
				match(TIMEDIFF);
				}
				break;
			case 124:
				enterOuterAlt(_localctx, 124);
				{
				setState(1890);
				match(TIMESTAMP);
				}
				break;
			case 125:
				enterOuterAlt(_localctx, 125);
				{
				setState(1891);
				match(TIMESTAMPADD);
				}
				break;
			case 126:
				enterOuterAlt(_localctx, 126);
				{
				setState(1892);
				match(TIMESTAMPDIFF);
				}
				break;
			case 127:
				enterOuterAlt(_localctx, 127);
				{
				setState(1893);
				match(TIME_FORMAT);
				}
				break;
			case 128:
				enterOuterAlt(_localctx, 128);
				{
				setState(1894);
				match(TIME_TO_SEC);
				}
				break;
			case 129:
				enterOuterAlt(_localctx, 129);
				{
				setState(1895);
				match(AES_DECRYPT);
				}
				break;
			case 130:
				enterOuterAlt(_localctx, 130);
				{
				setState(1896);
				match(AES_ENCRYPT);
				}
				break;
			case 131:
				enterOuterAlt(_localctx, 131);
				{
				setState(1897);
				match(FROM_BASE64);
				}
				break;
			case 132:
				enterOuterAlt(_localctx, 132);
				{
				setState(1898);
				match(TO_BASE64);
				}
				break;
			case 133:
				enterOuterAlt(_localctx, 133);
				{
				setState(1899);
				match(ADDDATE);
				}
				break;
			case 134:
				enterOuterAlt(_localctx, 134);
				{
				setState(1900);
				match(ADDTIME);
				}
				break;
			case 135:
				enterOuterAlt(_localctx, 135);
				{
				setState(1901);
				match(DATE);
				}
				break;
			case 136:
				enterOuterAlt(_localctx, 136);
				{
				setState(1902);
				match(DATE_ADD);
				}
				break;
			case 137:
				enterOuterAlt(_localctx, 137);
				{
				setState(1903);
				match(DATE_SUB);
				}
				break;
			case 138:
				enterOuterAlt(_localctx, 138);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchExpression_Context extends ParserRuleContext {
		public TerminalNode MATCH() { return getToken(DMLStatementParser.MATCH, 0); }
		public ColumnNamesContext columnNames() {
			return getRuleContext(ColumnNamesContext.class,0);
		}
		public TerminalNode AGAINST() { return getToken(DMLStatementParser.AGAINST, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public MatchSearchModifier_Context matchSearchModifier_() {
			return getRuleContext(MatchSearchModifier_Context.class,0);
		}
		public MatchExpression_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matchExpression_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterMatchExpression_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitMatchExpression_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitMatchExpression_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatchExpression_Context matchExpression_() throws RecognitionException {
		MatchExpression_Context _localctx = new MatchExpression_Context(_ctx, getState());
		enterRule(_localctx, 290, RULE_matchExpression_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1907);
			match(MATCH);
			setState(1908);
			columnNames();
			setState(1909);
			match(AGAINST);
			{
			setState(1910);
			expr(0);
			setState(1912);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,222,_ctx) ) {
			case 1:
				{
				setState(1911);
				matchSearchModifier_();
				}
				break;
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchSearchModifier_Context extends ParserRuleContext {
		public TerminalNode IN() { return getToken(DMLStatementParser.IN, 0); }
		public TerminalNode NATURAL() { return getToken(DMLStatementParser.NATURAL, 0); }
		public TerminalNode LANGUAGE() { return getToken(DMLStatementParser.LANGUAGE, 0); }
		public TerminalNode MODE() { return getToken(DMLStatementParser.MODE, 0); }
		public TerminalNode WITH() { return getToken(DMLStatementParser.WITH, 0); }
		public TerminalNode QUERY() { return getToken(DMLStatementParser.QUERY, 0); }
		public TerminalNode EXPANSION() { return getToken(DMLStatementParser.EXPANSION, 0); }
		public TerminalNode BOOLEAN() { return getToken(DMLStatementParser.BOOLEAN, 0); }
		public MatchSearchModifier_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matchSearchModifier_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterMatchSearchModifier_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitMatchSearchModifier_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitMatchSearchModifier_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatchSearchModifier_Context matchSearchModifier_() throws RecognitionException {
		MatchSearchModifier_Context _localctx = new MatchSearchModifier_Context(_ctx, getState());
		enterRule(_localctx, 292, RULE_matchSearchModifier_);
		try {
			setState(1931);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,223,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1914);
				match(IN);
				setState(1915);
				match(NATURAL);
				setState(1916);
				match(LANGUAGE);
				setState(1917);
				match(MODE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1918);
				match(IN);
				setState(1919);
				match(NATURAL);
				setState(1920);
				match(LANGUAGE);
				setState(1921);
				match(MODE);
				setState(1922);
				match(WITH);
				setState(1923);
				match(QUERY);
				setState(1924);
				match(EXPANSION);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1925);
				match(IN);
				setState(1926);
				match(BOOLEAN);
				setState(1927);
				match(MODE);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1928);
				match(WITH);
				setState(1929);
				match(QUERY);
				setState(1930);
				match(EXPANSION);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseExpressionContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(DMLStatementParser.CASE, 0); }
		public TerminalNode END() { return getToken(DMLStatementParser.END, 0); }
		public SimpleExprContext simpleExpr() {
			return getRuleContext(SimpleExprContext.class,0);
		}
		public List<CaseWhen_Context> caseWhen_() {
			return getRuleContexts(CaseWhen_Context.class);
		}
		public CaseWhen_Context caseWhen_(int i) {
			return getRuleContext(CaseWhen_Context.class,i);
		}
		public CaseElse_Context caseElse_() {
			return getRuleContext(CaseElse_Context.class,0);
		}
		public CaseExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCaseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCaseExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCaseExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseExpressionContext caseExpression() throws RecognitionException {
		CaseExpressionContext _localctx = new CaseExpressionContext(_ctx, getState());
		enterRule(_localctx, 294, RULE_caseExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1933);
			match(CASE);
			setState(1935);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOT_) | (1L << TILDE_) | (1L << PLUS_) | (1L << MINUS_) | (1L << DOT_) | (1L << LP_) | (1L << LBE_) | (1L << QUESTION_) | (1L << AT_) | (1L << TRUNCATE) | (1L << POSITION))) != 0) || ((((_la - 68)) & ~0x3f) == 0 && ((1L << (_la - 68)) & ((1L << (VIEW - 68)) | (1L << (VALUES - 68)) | (1L << (CASE - 68)) | (1L << (CAST - 68)) | (1L << (TRIM - 68)) | (1L << (SUBSTRING - 68)) | (1L << (LEFT - 68)) | (1L << (RIGHT - 68)) | (1L << (IF - 68)) | (1L << (NULL - 68)) | (1L << (TRUE - 68)) | (1L << (FALSE - 68)) | (1L << (EXISTS - 68)) | (1L << (ANY - 68)) | (1L << (OFFSET - 68)) | (1L << (BEGIN - 68)) | (1L << (COMMIT - 68)) | (1L << (ROLLBACK - 68)) | (1L << (SAVEPOINT - 68)) | (1L << (BOOLEAN - 68)) | (1L << (CHAR - 68)) | (1L << (INTERVAL - 68)) | (1L << (DATE - 68)) | (1L << (TIME - 68)) | (1L << (TIMESTAMP - 68)) | (1L << (LOCALTIME - 68)))) != 0) || ((((_la - 132)) & ~0x3f) == 0 && ((1L << (_la - 132)) & ((1L << (LOCALTIMESTAMP - 132)) | (1L << (YEAR - 132)) | (1L << (QUARTER - 132)) | (1L << (MONTH - 132)) | (1L << (WEEK - 132)) | (1L << (DAY - 132)) | (1L << (HOUR - 132)) | (1L << (MINUTE - 132)) | (1L << (SECOND - 132)) | (1L << (MICROSECOND - 132)) | (1L << (MAX - 132)) | (1L << (MIN - 132)) | (1L << (SUM - 132)) | (1L << (COUNT - 132)) | (1L << (AVG - 132)) | (1L << (CURRENT - 132)) | (1L << (ENABLE - 132)) | (1L << (DISABLE - 132)) | (1L << (INSTANCE - 132)) | (1L << (DO - 132)) | (1L << (DEFINER - 132)) | (1L << (CASCADED - 132)) | (1L << (LOCAL - 132)) | (1L << (CLOSE - 132)) | (1L << (OPEN - 132)) | (1L << (NEXT - 132)) | (1L << (NAME - 132)) | (1L << (TYPE - 132)) | (1L << (DATABASE - 132)) | (1L << (TABLES - 132)) | (1L << (TABLESPACE - 132)) | (1L << (COLUMNS - 132)) | (1L << (FIELDS - 132)) | (1L << (INDEXES - 132)) | (1L << (STATUS - 132)) | (1L << (REPLACE - 132)) | (1L << (MODIFY - 132)) | (1L << (VALUE - 132)) | (1L << (DUPLICATE - 132)))) != 0) || ((((_la - 196)) & ~0x3f) == 0 && ((1L << (_la - 196)) & ((1L << (FIRST - 196)) | (1L << (LAST - 196)) | (1L << (AFTER - 196)) | (1L << (OJ - 196)) | (1L << (MOD - 196)) | (1L << (ACCOUNT - 196)) | (1L << (USER - 196)) | (1L << (ROLE - 196)) | (1L << (START - 196)) | (1L << (TRANSACTION - 196)) | (1L << (ROW - 196)) | (1L << (WITHOUT - 196)) | (1L << (BINARY - 196)) | (1L << (ESCAPE - 196)) | (1L << (SUBPARTITION - 196)) | (1L << (STORAGE - 196)) | (1L << (SUPER - 196)) | (1L << (SUBSTR - 196)) | (1L << (TEMPORARY - 196)) | (1L << (THAN - 196)) | (1L << (UNBOUNDED - 196)) | (1L << (SIGNED - 196)) | (1L << (UPGRADE - 196)) | (1L << (VALIDATION - 196)) | (1L << (ROLLUP - 196)) | (1L << (SOUNDS - 196)) | (1L << (UNKNOWN - 196)) | (1L << (OFF - 196)) | (1L << (ALWAYS - 196)) | (1L << (COMMITTED - 196)) | (1L << (LEVEL - 196)) | (1L << (NO - 196)) | (1L << (PASSWORD - 196)) | (1L << (PRIVILEGES - 196)) | (1L << (ACTION - 196)) | (1L << (ALGORITHM - 196)) | (1L << (AUTOCOMMIT - 196)) | (1L << (BTREE - 196)) | (1L << (CHAIN - 196)) | (1L << (CHARSET - 196)))) != 0) || ((((_la - 260)) & ~0x3f) == 0 && ((1L << (_la - 260)) & ((1L << (CHECKSUM - 260)) | (1L << (CIPHER - 260)) | (1L << (CLIENT - 260)) | (1L << (COALESCE - 260)) | (1L << (COMMENT - 260)) | (1L << (COMPACT - 260)) | (1L << (COMPRESSED - 260)) | (1L << (COMPRESSION - 260)) | (1L << (CONNECTION - 260)) | (1L << (CONSISTENT - 260)) | (1L << (CONVERT - 260)) | (1L << (DATA - 260)) | (1L << (DISCARD - 260)) | (1L << (DISK - 260)) | (1L << (ENCRYPTION - 260)) | (1L << (END - 260)) | (1L << (ENGINE - 260)) | (1L << (EVENT - 260)) | (1L << (EXCHANGE - 260)) | (1L << (EXECUTE - 260)) | (1L << (EXTRACT - 260)) | (1L << (FILE - 260)) | (1L << (FIXED - 260)) | (1L << (FOLLOWING - 260)) | (1L << (GLOBAL - 260)) | (1L << (HASH - 260)) | (1L << (IMPORT_ - 260)) | (1L << (LESS - 260)) | (1L << (MATCH - 260)) | (1L << (MEMORY - 260)) | (1L << (NONE - 260)) | (1L << (NOW - 260)) | (1L << (PARSER - 260)) | (1L << (PARTIAL - 260)) | (1L << (PARTITIONING - 260)) | (1L << (PERSIST - 260)) | (1L << (PRECEDING - 260)) | (1L << (PROCESS - 260)) | (1L << (PROXY - 260)) | (1L << (QUICK - 260)) | (1L << (REBUILD - 260)) | (1L << (REDUNDANT - 260)))) != 0) || ((((_la - 325)) & ~0x3f) == 0 && ((1L << (_la - 325)) & ((1L << (RELOAD - 325)) | (1L << (REMOVE - 325)) | (1L << (REORGANIZE - 325)) | (1L << (REPAIR - 325)) | (1L << (REVERSE - 325)) | (1L << (SESSION - 325)) | (1L << (SHUTDOWN - 325)) | (1L << (SIMPLE - 325)) | (1L << (SLAVE - 325)) | (1L << (VISIBLE - 325)) | (1L << (INVISIBLE - 325)) | (1L << (ENFORCED - 325)) | (1L << (AGAINST - 325)) | (1L << (LANGUAGE - 325)) | (1L << (MODE - 325)) | (1L << (QUERY - 325)) | (1L << (EXTENDED - 325)) | (1L << (EXPANSION - 325)) | (1L << (VARIANCE - 325)) | (1L << (MAX_ROWS - 325)) | (1L << (MIN_ROWS - 325)) | (1L << (SQL_BIG_RESULT - 325)) | (1L << (SQL_BUFFER_RESULT - 325)) | (1L << (SQL_CACHE - 325)) | (1L << (SQL_NO_CACHE - 325)) | (1L << (STATS_AUTO_RECALC - 325)) | (1L << (STATS_PERSISTENT - 325)) | (1L << (STATS_SAMPLE_PAGES - 325)) | (1L << (ROW_FORMAT - 325)) | (1L << (WEIGHT_STRING - 325)) | (1L << (COLUMN_FORMAT - 325)) | (1L << (INSERT_METHOD - 325)) | (1L << (KEY_BLOCK_SIZE - 325)) | (1L << (PACK_KEYS - 325)) | (1L << (PERSIST_ONLY - 325)) | (1L << (BIT_AND - 325)) | (1L << (BIT_OR - 325)) | (1L << (BIT_XOR - 325)) | (1L << (GROUP_CONCAT - 325)) | (1L << (JSON_ARRAYAGG - 325)) | (1L << (JSON_OBJECTAGG - 325)) | (1L << (STD - 325)))) != 0) || ((((_la - 389)) & ~0x3f) == 0 && ((1L << (_la - 389)) & ((1L << (STDDEV - 389)) | (1L << (STDDEV_POP - 389)) | (1L << (STDDEV_SAMP - 389)) | (1L << (VAR_POP - 389)) | (1L << (VAR_SAMP - 389)) | (1L << (AUTO_INCREMENT - 389)) | (1L << (AVG_ROW_LENGTH - 389)) | (1L << (DELAY_KEY_WRITE - 389)) | (1L << (CURRENT_TIMESTAMP - 389)) | (1L << (ROTATE - 389)) | (1L << (MASTER - 389)) | (1L << (BINLOG - 389)) | (1L << (ERROR - 389)) | (1L << (SCHEDULE - 389)) | (1L << (COMPLETION - 389)) | (1L << (EVERY - 389)) | (1L << (HOST - 389)) | (1L << (SOCKET - 389)) | (1L << (PORT - 389)) | (1L << (SERVER - 389)) | (1L << (WRAPPER - 389)) | (1L << (OPTIONS - 389)) | (1L << (OWNER - 389)) | (1L << (RETURNS - 389)) | (1L << (CONTAINS - 389)) | (1L << (SECURITY - 389)) | (1L << (INVOKER - 389)) | (1L << (TEMPTABLE - 389)) | (1L << (MERGE - 389)) | (1L << (UNDEFINED - 389)) | (1L << (DATAFILE - 389)) | (1L << (FILE_BLOCK_SIZE - 389)) | (1L << (EXTENT_SIZE - 389)) | (1L << (INITIAL_SIZE - 389)) | (1L << (AUTOEXTEND_SIZE - 389)) | (1L << (MAX_SIZE - 389)) | (1L << (NODEGROUP - 389)) | (1L << (WAIT - 389)) | (1L << (LOGFILE - 389)) | (1L << (UNDOFILE - 389)))) != 0) || ((((_la - 453)) & ~0x3f) == 0 && ((1L << (_la - 453)) & ((1L << (UNDO_BUFFER_SIZE - 453)) | (1L << (REDO_BUFFER_SIZE - 453)) | (1L << (HANDLER - 453)) | (1L << (PREV - 453)) | (1L << (ORGANIZATION - 453)) | (1L << (DEFINITION - 453)) | (1L << (DESCRIPTION - 453)) | (1L << (REFERENCE - 453)) | (1L << (FOLLOWS - 453)) | (1L << (PRECEDES - 453)) | (1L << (IMPORT - 453)) | (1L << (CONCURRENT - 453)) | (1L << (XML - 453)) | (1L << (DUMPFILE - 453)) | (1L << (SHARE - 453)) | (1L << (CODE - 453)) | (1L << (CONTEXT - 453)) | (1L << (SOURCE - 453)) | (1L << (CHANNEL - 453)) | (1L << (CLONE - 453)) | (1L << (AGGREGATE - 453)) | (1L << (INSTALL - 453)) | (1L << (COMPONENT - 453)) | (1L << (UNINSTALL - 453)))) != 0) || ((((_la - 520)) & ~0x3f) == 0 && ((1L << (_la - 520)) & ((1L << (RESOURCE - 520)) | (1L << (EXPIRE - 520)) | (1L << (NEVER - 520)) | (1L << (HISTORY - 520)) | (1L << (OPTIONAL - 520)) | (1L << (REUSE - 520)) | (1L << (MAX_QUERIES_PER_HOUR - 520)) | (1L << (MAX_UPDATES_PER_HOUR - 520)) | (1L << (MAX_CONNECTIONS_PER_HOUR - 520)) | (1L << (MAX_USER_CONNECTIONS - 520)) | (1L << (RETAIN - 520)) | (1L << (RANDOM - 520)) | (1L << (OLD - 520)) | (1L << (ISSUER - 520)) | (1L << (SUBJECT - 520)) | (1L << (CACHE - 520)) | (1L << (GENERAL - 520)) | (1L << (SLOW - 520)) | (1L << (USER_RESOURCES - 520)) | (1L << (EXPORT - 520)) | (1L << (RELAY - 520)) | (1L << (HOSTS - 520)) | (1L << (FLUSH - 520)) | (1L << (RESET - 520)) | (1L << (RESTART - 520)) | (1L << (UNIX_TIMESTAMP - 520)) | (1L << (LOWER - 520)) | (1L << (UPPER - 520)) | (1L << (ADDDATE - 520)) | (1L << (ADDTIME - 520)) | (1L << (DATE_ADD - 520)) | (1L << (DATE_SUB - 520)) | (1L << (DATEDIFF - 520)) | (1L << (DATE_FORMAT - 520)) | (1L << (DAYNAME - 520)) | (1L << (DAYOFMONTH - 520)) | (1L << (DAYOFWEEK - 520)) | (1L << (DAYOFYEAR - 520)) | (1L << (STR_TO_DATE - 520)) | (1L << (TIMEDIFF - 520)) | (1L << (TIMESTAMPADD - 520)) | (1L << (TIMESTAMPDIFF - 520)) | (1L << (TIME_FORMAT - 520)) | (1L << (TIME_TO_SEC - 520)) | (1L << (AES_DECRYPT - 520)) | (1L << (AES_ENCRYPT - 520)) | (1L << (FROM_BASE64 - 520)) | (1L << (TO_BASE64 - 520)) | (1L << (GEOMCOLLECTION - 520)) | (1L << (GEOMETRYCOLLECTION - 520)) | (1L << (LINESTRING - 520)) | (1L << (MULTILINESTRING - 520)) | (1L << (MULTIPOINT - 520)) | (1L << (MULTIPOLYGON - 520)) | (1L << (POINT - 520)) | (1L << (POLYGON - 520)) | (1L << (ST_AREA - 520)) | (1L << (ST_ASBINARY - 520)))) != 0) || ((((_la - 584)) & ~0x3f) == 0 && ((1L << (_la - 584)) & ((1L << (ST_ASGEOJSON - 584)) | (1L << (ST_ASTEXT - 584)) | (1L << (ST_ASWKB - 584)) | (1L << (ST_ASWKT - 584)) | (1L << (ST_BUFFER - 584)) | (1L << (ST_BUFFER_STRATEGY - 584)) | (1L << (ST_CENTROID - 584)) | (1L << (ST_CONTAINS - 584)) | (1L << (ST_CONVEXHULL - 584)) | (1L << (ST_CROSSES - 584)) | (1L << (ST_DIFFERENCE - 584)) | (1L << (ST_DIMENSION - 584)) | (1L << (ST_DISJOINT - 584)) | (1L << (ST_DISTANCE - 584)) | (1L << (ST_DISTANCE_SPHERE - 584)) | (1L << (ST_ENDPOINT - 584)) | (1L << (ST_ENVELOPE - 584)) | (1L << (ST_EQUALS - 584)) | (1L << (ST_EXTERIORRING - 584)) | (1L << (ST_GEOHASH - 584)) | (1L << (ST_GEOMCOLLFROMTEXT - 584)) | (1L << (ST_GEOMCOLLFROMTXT - 584)) | (1L << (ST_GEOMCOLLFROMWKB - 584)) | (1L << (ST_GEOMETRYCOLLECTIONFROMTEXT - 584)) | (1L << (ST_GEOMETRYCOLLECTIONFROMWKB - 584)) | (1L << (ST_GEOMETRYFROMTEXT - 584)) | (1L << (ST_GEOMETRYFROMWKB - 584)) | (1L << (ST_GEOMETRYN - 584)) | (1L << (ST_GEOMETRYTYPE - 584)) | (1L << (ST_GEOMFROMGEOJSON - 584)) | (1L << (ST_GEOMFROMTEXT - 584)) | (1L << (ST_GEOMFROMWKB - 584)) | (1L << (ST_INTERIORRINGN - 584)) | (1L << (ST_INTERSECTION - 584)) | (1L << (ST_INTERSECTS - 584)) | (1L << (ST_ISCLOSED - 584)) | (1L << (ST_ISEMPTY - 584)) | (1L << (ST_ISSIMPLE - 584)) | (1L << (ST_ISVALID - 584)) | (1L << (ST_LATFROMGEOHASH - 584)) | (1L << (ST_LATITUDE - 584)) | (1L << (ST_LENGTH - 584)) | (1L << (ST_LINEFROMTEXT - 584)) | (1L << (ST_LINEFROMWKB - 584)) | (1L << (ST_LINESTRINGFROMTEXT - 584)) | (1L << (ST_LINESTRINGFROMWKB - 584)) | (1L << (ST_LONGFROMGEOHASH - 584)) | (1L << (ST_LONGITUDE - 584)) | (1L << (ST_MAKEENVELOPE - 584)) | (1L << (ST_MLINEFROMTEXT - 584)) | (1L << (ST_MLINEFROMWKB - 584)) | (1L << (ST_MULTILINESTRINGFROMTEXT - 584)) | (1L << (ST_MULTILINESTRINGFROMWKB - 584)) | (1L << (ST_MPOINTFROMTEXT - 584)) | (1L << (ST_MPOINTFROMWKB - 584)) | (1L << (ST_MULTIPOINTFROMTEXT - 584)) | (1L << (ST_MULTIPOINTFROMWKB - 584)) | (1L << (ST_MPOLYFROMTEXT - 584)) | (1L << (ST_MPOLYFROMWKB - 584)) | (1L << (ST_MULTIPOLYGONFROMTEXT - 584)) | (1L << (ST_MULTIPOLYGONFROMWKB - 584)) | (1L << (ST_NUMGEOMETRIES - 584)) | (1L << (ST_NUMINTERIORRING - 584)) | (1L << (ST_NUMINTERIORRINGS - 584)))) != 0) || ((((_la - 648)) & ~0x3f) == 0 && ((1L << (_la - 648)) & ((1L << (ST_NUMPOINTS - 648)) | (1L << (ST_OVERLAPS - 648)) | (1L << (ST_POINTFROMGEOHASH - 648)) | (1L << (ST_POINTFROMTEXT - 648)) | (1L << (ST_POINTFROMWKB - 648)) | (1L << (ST_POINTN - 648)) | (1L << (ST_POLYFROMTEXT - 648)) | (1L << (ST_POLYFROMWKB - 648)) | (1L << (ST_POLYGONFROMTEXT - 648)) | (1L << (ST_POLYGONFROMWKB - 648)) | (1L << (ST_SIMPLIFY - 648)) | (1L << (ST_SRID - 648)) | (1L << (ST_STARTPOINT - 648)) | (1L << (ST_SWAPXY - 648)) | (1L << (ST_SYMDIFFERENCE - 648)) | (1L << (ST_TOUCHES - 648)) | (1L << (ST_TRANSFORM - 648)) | (1L << (ST_UNION - 648)) | (1L << (ST_VALIDATE - 648)) | (1L << (ST_WITHIN - 648)) | (1L << (ST_X - 648)) | (1L << (ST_Y - 648)) | (1L << (IO_THREAD - 648)) | (1L << (SQL_THREAD - 648)) | (1L << (SQL_BEFORE_GTIDS - 648)) | (1L << (SQL_AFTER_GTIDS - 648)) | (1L << (MASTER_LOG_FILE - 648)) | (1L << (MASTER_LOG_POS - 648)) | (1L << (RELAY_LOG_FILE - 648)) | (1L << (RELAY_LOG_POS - 648)) | (1L << (SQL_AFTER_MTS_GAPS - 648)) | (1L << (UNTIL - 648)) | (1L << (DEFAULT_AUTH - 648)) | (1L << (PLUGIN_DIR - 648)) | (1L << (STOP - 648)) | (1L << (IDENTIFIER_ - 648)) | (1L << (STRING_ - 648)) | (1L << (NUMBER_ - 648)) | (1L << (HEX_DIGIT_ - 648)) | (1L << (BIT_NUM_ - 648)))) != 0)) {
				{
				setState(1934);
				simpleExpr(0);
				}
			}

			setState(1938); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1937);
				caseWhen_();
				}
				}
				setState(1940); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==WHEN );
			setState(1943);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(1942);
				caseElse_();
				}
			}

			setState(1945);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseWhen_Context extends ParserRuleContext {
		public TerminalNode WHEN() { return getToken(DMLStatementParser.WHEN, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode THEN() { return getToken(DMLStatementParser.THEN, 0); }
		public CaseWhen_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseWhen_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCaseWhen_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCaseWhen_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCaseWhen_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseWhen_Context caseWhen_() throws RecognitionException {
		CaseWhen_Context _localctx = new CaseWhen_Context(_ctx, getState());
		enterRule(_localctx, 296, RULE_caseWhen_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1947);
			match(WHEN);
			setState(1948);
			expr(0);
			setState(1949);
			match(THEN);
			setState(1950);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseElse_Context extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(DMLStatementParser.ELSE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public CaseElse_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseElse_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCaseElse_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCaseElse_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCaseElse_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CaseElse_Context caseElse_() throws RecognitionException {
		CaseElse_Context _localctx = new CaseElse_Context(_ctx, getState());
		enterRule(_localctx, 298, RULE_caseElse_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1952);
			match(ELSE);
			setState(1953);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalExpressionContext extends ParserRuleContext {
		public TerminalNode INTERVAL() { return getToken(DMLStatementParser.INTERVAL, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public IntervalUnit_Context intervalUnit_() {
			return getRuleContext(IntervalUnit_Context.class,0);
		}
		public IntervalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intervalExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIntervalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIntervalExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIntervalExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntervalExpressionContext intervalExpression() throws RecognitionException {
		IntervalExpressionContext _localctx = new IntervalExpressionContext(_ctx, getState());
		enterRule(_localctx, 300, RULE_intervalExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1955);
			match(INTERVAL);
			setState(1956);
			expr(0);
			setState(1957);
			intervalUnit_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalUnit_Context extends ParserRuleContext {
		public TerminalNode MICROSECOND() { return getToken(DMLStatementParser.MICROSECOND, 0); }
		public TerminalNode SECOND() { return getToken(DMLStatementParser.SECOND, 0); }
		public TerminalNode MINUTE() { return getToken(DMLStatementParser.MINUTE, 0); }
		public TerminalNode HOUR() { return getToken(DMLStatementParser.HOUR, 0); }
		public TerminalNode DAY() { return getToken(DMLStatementParser.DAY, 0); }
		public TerminalNode WEEK() { return getToken(DMLStatementParser.WEEK, 0); }
		public TerminalNode MONTH() { return getToken(DMLStatementParser.MONTH, 0); }
		public TerminalNode QUARTER() { return getToken(DMLStatementParser.QUARTER, 0); }
		public TerminalNode YEAR() { return getToken(DMLStatementParser.YEAR, 0); }
		public TerminalNode SECOND_MICROSECOND() { return getToken(DMLStatementParser.SECOND_MICROSECOND, 0); }
		public TerminalNode MINUTE_MICROSECOND() { return getToken(DMLStatementParser.MINUTE_MICROSECOND, 0); }
		public TerminalNode MINUTE_SECOND() { return getToken(DMLStatementParser.MINUTE_SECOND, 0); }
		public TerminalNode HOUR_MICROSECOND() { return getToken(DMLStatementParser.HOUR_MICROSECOND, 0); }
		public TerminalNode HOUR_SECOND() { return getToken(DMLStatementParser.HOUR_SECOND, 0); }
		public TerminalNode HOUR_MINUTE() { return getToken(DMLStatementParser.HOUR_MINUTE, 0); }
		public TerminalNode DAY_MICROSECOND() { return getToken(DMLStatementParser.DAY_MICROSECOND, 0); }
		public TerminalNode DAY_SECOND() { return getToken(DMLStatementParser.DAY_SECOND, 0); }
		public TerminalNode DAY_MINUTE() { return getToken(DMLStatementParser.DAY_MINUTE, 0); }
		public TerminalNode DAY_HOUR() { return getToken(DMLStatementParser.DAY_HOUR, 0); }
		public TerminalNode YEAR_MONTH() { return getToken(DMLStatementParser.YEAR_MONTH, 0); }
		public IntervalUnit_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intervalUnit_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIntervalUnit_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIntervalUnit_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIntervalUnit_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntervalUnit_Context intervalUnit_() throws RecognitionException {
		IntervalUnit_Context _localctx = new IntervalUnit_Context(_ctx, getState());
		enterRule(_localctx, 302, RULE_intervalUnit_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1959);
			_la = _input.LA(1);
			if ( !(((((_la - 133)) & ~0x3f) == 0 && ((1L << (_la - 133)) & ((1L << (YEAR - 133)) | (1L << (QUARTER - 133)) | (1L << (MONTH - 133)) | (1L << (WEEK - 133)) | (1L << (DAY - 133)) | (1L << (HOUR - 133)) | (1L << (MINUTE - 133)) | (1L << (SECOND - 133)) | (1L << (MICROSECOND - 133)))) != 0) || ((((_la - 403)) & ~0x3f) == 0 && ((1L << (_la - 403)) & ((1L << (YEAR_MONTH - 403)) | (1L << (DAY_HOUR - 403)) | (1L << (DAY_MINUTE - 403)) | (1L << (DAY_SECOND - 403)) | (1L << (DAY_MICROSECOND - 403)) | (1L << (HOUR_MINUTE - 403)) | (1L << (HOUR_SECOND - 403)) | (1L << (HOUR_MICROSECOND - 403)) | (1L << (MINUTE_SECOND - 403)) | (1L << (MINUTE_MICROSECOND - 403)) | (1L << (SECOND_MICROSECOND - 403)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByClauseContext extends ParserRuleContext {
		public TerminalNode ORDER() { return getToken(DMLStatementParser.ORDER, 0); }
		public TerminalNode BY() { return getToken(DMLStatementParser.BY, 0); }
		public List<OrderByItemContext> orderByItem() {
			return getRuleContexts(OrderByItemContext.class);
		}
		public OrderByItemContext orderByItem(int i) {
			return getRuleContext(OrderByItemContext.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public OrderByClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterOrderByClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitOrderByClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitOrderByClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByClauseContext orderByClause() throws RecognitionException {
		OrderByClauseContext _localctx = new OrderByClauseContext(_ctx, getState());
		enterRule(_localctx, 304, RULE_orderByClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1961);
			match(ORDER);
			setState(1962);
			match(BY);
			setState(1963);
			orderByItem();
			setState(1968);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(1964);
				match(COMMA_);
				setState(1965);
				orderByItem();
				}
				}
				setState(1970);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderByItemContext extends ParserRuleContext {
		public ColumnNameContext columnName() {
			return getRuleContext(ColumnNameContext.class,0);
		}
		public NumberLiteralsContext numberLiterals() {
			return getRuleContext(NumberLiteralsContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ASC() { return getToken(DMLStatementParser.ASC, 0); }
		public TerminalNode DESC() { return getToken(DMLStatementParser.DESC, 0); }
		public OrderByItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderByItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterOrderByItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitOrderByItem(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitOrderByItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrderByItemContext orderByItem() throws RecognitionException {
		OrderByItemContext _localctx = new OrderByItemContext(_ctx, getState());
		enterRule(_localctx, 306, RULE_orderByItem);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1974);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,228,_ctx) ) {
			case 1:
				{
				setState(1971);
				columnName();
				}
				break;
			case 2:
				{
				setState(1972);
				numberLiterals();
				}
				break;
			case 3:
				{
				setState(1973);
				expr(0);
				}
				break;
			}
			setState(1977);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASC || _la==DESC) {
				{
				setState(1976);
				_la = _input.LA(1);
				if ( !(_la==ASC || _la==DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeContext extends ParserRuleContext {
		public DataTypeNameContext dataTypeName() {
			return getRuleContext(DataTypeNameContext.class,0);
		}
		public DataTypeLengthContext dataTypeLength() {
			return getRuleContext(DataTypeLengthContext.class,0);
		}
		public CharacterSet_Context characterSet_() {
			return getRuleContext(CharacterSet_Context.class,0);
		}
		public CollateClause_Context collateClause_() {
			return getRuleContext(CollateClause_Context.class,0);
		}
		public TerminalNode ZEROFILL() { return getToken(DMLStatementParser.ZEROFILL, 0); }
		public TerminalNode UNSIGNED() { return getToken(DMLStatementParser.UNSIGNED, 0); }
		public TerminalNode SIGNED() { return getToken(DMLStatementParser.SIGNED, 0); }
		public CollectionOptionsContext collectionOptions() {
			return getRuleContext(CollectionOptionsContext.class,0);
		}
		public DataTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDataType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDataType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDataType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeContext dataType() throws RecognitionException {
		DataTypeContext _localctx = new DataTypeContext(_ctx, getState());
		enterRule(_localctx, 308, RULE_dataType);
		int _la;
		try {
			setState(2003);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,237,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1979);
				dataTypeName();
				setState(1981);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LP_) {
					{
					setState(1980);
					dataTypeLength();
					}
				}

				setState(1984);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CHAR || _la==CHARACTER) {
					{
					setState(1983);
					characterSet_();
					}
				}

				setState(1987);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLLATE) {
					{
					setState(1986);
					collateClause_();
					}
				}

				setState(1990);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==UNSIGNED || _la==SIGNED) {
					{
					setState(1989);
					_la = _input.LA(1);
					if ( !(_la==UNSIGNED || _la==SIGNED) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1993);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ZEROFILL) {
					{
					setState(1992);
					match(ZEROFILL);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1995);
				dataTypeName();
				setState(1996);
				collectionOptions();
				setState(1998);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CHAR || _la==CHARACTER) {
					{
					setState(1997);
					characterSet_();
					}
				}

				setState(2001);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLLATE) {
					{
					setState(2000);
					collateClause_();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeNameContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(DMLStatementParser.INTEGER, 0); }
		public TerminalNode INT() { return getToken(DMLStatementParser.INT, 0); }
		public TerminalNode SMALLINT() { return getToken(DMLStatementParser.SMALLINT, 0); }
		public TerminalNode TINYINT() { return getToken(DMLStatementParser.TINYINT, 0); }
		public TerminalNode MEDIUMINT() { return getToken(DMLStatementParser.MEDIUMINT, 0); }
		public TerminalNode BIGINT() { return getToken(DMLStatementParser.BIGINT, 0); }
		public TerminalNode DECIMAL() { return getToken(DMLStatementParser.DECIMAL, 0); }
		public TerminalNode NUMERIC() { return getToken(DMLStatementParser.NUMERIC, 0); }
		public TerminalNode FLOAT() { return getToken(DMLStatementParser.FLOAT, 0); }
		public TerminalNode DOUBLE() { return getToken(DMLStatementParser.DOUBLE, 0); }
		public TerminalNode BIT() { return getToken(DMLStatementParser.BIT, 0); }
		public TerminalNode BOOL() { return getToken(DMLStatementParser.BOOL, 0); }
		public TerminalNode BOOLEAN() { return getToken(DMLStatementParser.BOOLEAN, 0); }
		public TerminalNode DEC() { return getToken(DMLStatementParser.DEC, 0); }
		public TerminalNode DATE() { return getToken(DMLStatementParser.DATE, 0); }
		public TerminalNode DATETIME() { return getToken(DMLStatementParser.DATETIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(DMLStatementParser.TIMESTAMP, 0); }
		public TerminalNode TIME() { return getToken(DMLStatementParser.TIME, 0); }
		public TerminalNode YEAR() { return getToken(DMLStatementParser.YEAR, 0); }
		public TerminalNode CHAR() { return getToken(DMLStatementParser.CHAR, 0); }
		public TerminalNode VARCHAR() { return getToken(DMLStatementParser.VARCHAR, 0); }
		public TerminalNode BINARY() { return getToken(DMLStatementParser.BINARY, 0); }
		public TerminalNode VARBINARY() { return getToken(DMLStatementParser.VARBINARY, 0); }
		public TerminalNode TINYBLOB() { return getToken(DMLStatementParser.TINYBLOB, 0); }
		public TerminalNode TINYTEXT() { return getToken(DMLStatementParser.TINYTEXT, 0); }
		public TerminalNode BLOB() { return getToken(DMLStatementParser.BLOB, 0); }
		public TerminalNode TEXT() { return getToken(DMLStatementParser.TEXT, 0); }
		public TerminalNode MEDIUMBLOB() { return getToken(DMLStatementParser.MEDIUMBLOB, 0); }
		public TerminalNode MEDIUMTEXT() { return getToken(DMLStatementParser.MEDIUMTEXT, 0); }
		public TerminalNode LONGBLOB() { return getToken(DMLStatementParser.LONGBLOB, 0); }
		public TerminalNode LONGTEXT() { return getToken(DMLStatementParser.LONGTEXT, 0); }
		public TerminalNode ENUM() { return getToken(DMLStatementParser.ENUM, 0); }
		public TerminalNode SET() { return getToken(DMLStatementParser.SET, 0); }
		public TerminalNode GEOMETRY() { return getToken(DMLStatementParser.GEOMETRY, 0); }
		public TerminalNode POINT() { return getToken(DMLStatementParser.POINT, 0); }
		public TerminalNode LINESTRING() { return getToken(DMLStatementParser.LINESTRING, 0); }
		public TerminalNode POLYGON() { return getToken(DMLStatementParser.POLYGON, 0); }
		public TerminalNode MULTIPOINT() { return getToken(DMLStatementParser.MULTIPOINT, 0); }
		public TerminalNode MULTILINESTRING() { return getToken(DMLStatementParser.MULTILINESTRING, 0); }
		public TerminalNode MULTIPOLYGON() { return getToken(DMLStatementParser.MULTIPOLYGON, 0); }
		public TerminalNode GEOMETRYCOLLECTION() { return getToken(DMLStatementParser.GEOMETRYCOLLECTION, 0); }
		public TerminalNode JSON() { return getToken(DMLStatementParser.JSON, 0); }
		public DataTypeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataTypeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDataTypeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDataTypeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDataTypeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeNameContext dataTypeName() throws RecognitionException {
		DataTypeNameContext _localctx = new DataTypeNameContext(_ctx, getState());
		enterRule(_localctx, 310, RULE_dataTypeName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2005);
			_la = _input.LA(1);
			if ( !(_la==SET || ((((_la - 122)) & ~0x3f) == 0 && ((1L << (_la - 122)) & ((1L << (BOOLEAN - 122)) | (1L << (DOUBLE - 122)) | (1L << (CHAR - 122)) | (1L << (DATE - 122)) | (1L << (TIME - 122)) | (1L << (TIMESTAMP - 122)) | (1L << (YEAR - 122)) | (1L << (INTEGER - 122)) | (1L << (DECIMAL - 122)) | (1L << (INT - 122)) | (1L << (SMALLINT - 122)) | (1L << (TINYINT - 122)) | (1L << (MEDIUMINT - 122)) | (1L << (BIGINT - 122)) | (1L << (NUMERIC - 122)) | (1L << (FLOAT - 122)) | (1L << (DATETIME - 122)))) != 0) || _la==BINARY || ((((_la - 575)) & ~0x3f) == 0 && ((1L << (_la - 575)) & ((1L << (GEOMETRYCOLLECTION - 575)) | (1L << (LINESTRING - 575)) | (1L << (MULTILINESTRING - 575)) | (1L << (MULTIPOINT - 575)) | (1L << (MULTIPOLYGON - 575)) | (1L << (POINT - 575)) | (1L << (POLYGON - 575)))) != 0) || ((((_la - 670)) & ~0x3f) == 0 && ((1L << (_la - 670)) & ((1L << (BIT - 670)) | (1L << (BOOL - 670)) | (1L << (DEC - 670)) | (1L << (VARCHAR - 670)) | (1L << (VARBINARY - 670)) | (1L << (TINYBLOB - 670)) | (1L << (TINYTEXT - 670)) | (1L << (BLOB - 670)) | (1L << (TEXT - 670)) | (1L << (MEDIUMBLOB - 670)) | (1L << (MEDIUMTEXT - 670)) | (1L << (LONGBLOB - 670)) | (1L << (LONGTEXT - 670)) | (1L << (ENUM - 670)) | (1L << (GEOMETRY - 670)) | (1L << (JSON - 670)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeLengthContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<TerminalNode> NUMBER_() { return getTokens(DMLStatementParser.NUMBER_); }
		public TerminalNode NUMBER_(int i) {
			return getToken(DMLStatementParser.NUMBER_, i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public TerminalNode COMMA_() { return getToken(DMLStatementParser.COMMA_, 0); }
		public DataTypeLengthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataTypeLength; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterDataTypeLength(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitDataTypeLength(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitDataTypeLength(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeLengthContext dataTypeLength() throws RecognitionException {
		DataTypeLengthContext _localctx = new DataTypeLengthContext(_ctx, getState());
		enterRule(_localctx, 312, RULE_dataTypeLength);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2007);
			match(LP_);
			setState(2008);
			match(NUMBER_);
			setState(2011);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA_) {
				{
				setState(2009);
				match(COMMA_);
				setState(2010);
				match(NUMBER_);
				}
			}

			setState(2013);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollectionOptionsContext extends ParserRuleContext {
		public TerminalNode LP_() { return getToken(DMLStatementParser.LP_, 0); }
		public List<TerminalNode> STRING_() { return getTokens(DMLStatementParser.STRING_); }
		public TerminalNode STRING_(int i) {
			return getToken(DMLStatementParser.STRING_, i);
		}
		public TerminalNode RP_() { return getToken(DMLStatementParser.RP_, 0); }
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public CollectionOptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collectionOptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCollectionOptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCollectionOptions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCollectionOptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollectionOptionsContext collectionOptions() throws RecognitionException {
		CollectionOptionsContext _localctx = new CollectionOptionsContext(_ctx, getState());
		enterRule(_localctx, 314, RULE_collectionOptions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2015);
			match(LP_);
			setState(2016);
			match(STRING_);
			setState(2021);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(2017);
				match(COMMA_);
				setState(2018);
				match(STRING_);
				}
				}
				setState(2023);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2024);
			match(RP_);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CharacterSet_Context extends ParserRuleContext {
		public TerminalNode SET() { return getToken(DMLStatementParser.SET, 0); }
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public TerminalNode CHARACTER() { return getToken(DMLStatementParser.CHARACTER, 0); }
		public TerminalNode CHAR() { return getToken(DMLStatementParser.CHAR, 0); }
		public TerminalNode EQ_() { return getToken(DMLStatementParser.EQ_, 0); }
		public CharacterSet_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_characterSet_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCharacterSet_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCharacterSet_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCharacterSet_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CharacterSet_Context characterSet_() throws RecognitionException {
		CharacterSet_Context _localctx = new CharacterSet_Context(_ctx, getState());
		enterRule(_localctx, 316, RULE_characterSet_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2026);
			_la = _input.LA(1);
			if ( !(_la==CHAR || _la==CHARACTER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(2027);
			match(SET);
			setState(2029);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EQ_) {
				{
				setState(2028);
				match(EQ_);
				}
			}

			setState(2031);
			ignoredIdentifier_();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollateClause_Context extends ParserRuleContext {
		public TerminalNode COLLATE() { return getToken(DMLStatementParser.COLLATE, 0); }
		public TerminalNode STRING_() { return getToken(DMLStatementParser.STRING_, 0); }
		public IgnoredIdentifier_Context ignoredIdentifier_() {
			return getRuleContext(IgnoredIdentifier_Context.class,0);
		}
		public TerminalNode EQ_() { return getToken(DMLStatementParser.EQ_, 0); }
		public CollateClause_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collateClause_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterCollateClause_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitCollateClause_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitCollateClause_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CollateClause_Context collateClause_() throws RecognitionException {
		CollateClause_Context _localctx = new CollateClause_Context(_ctx, getState());
		enterRule(_localctx, 318, RULE_collateClause_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2033);
			match(COLLATE);
			setState(2035);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EQ_) {
				{
				setState(2034);
				match(EQ_);
				}
			}

			setState(2039);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING_:
				{
				setState(2037);
				match(STRING_);
				}
				break;
			case TRUNCATE:
			case POSITION:
			case VIEW:
			case ANY:
			case OFFSET:
			case BEGIN:
			case COMMIT:
			case ROLLBACK:
			case SAVEPOINT:
			case BOOLEAN:
			case DATE:
			case TIME:
			case TIMESTAMP:
			case YEAR:
			case QUARTER:
			case MONTH:
			case WEEK:
			case DAY:
			case HOUR:
			case MINUTE:
			case SECOND:
			case MICROSECOND:
			case MAX:
			case MIN:
			case SUM:
			case COUNT:
			case AVG:
			case CURRENT:
			case ENABLE:
			case DISABLE:
			case INSTANCE:
			case DO:
			case DEFINER:
			case CASCADED:
			case LOCAL:
			case CLOSE:
			case OPEN:
			case NEXT:
			case NAME:
			case TYPE:
			case TABLES:
			case TABLESPACE:
			case COLUMNS:
			case FIELDS:
			case INDEXES:
			case STATUS:
			case MODIFY:
			case VALUE:
			case DUPLICATE:
			case FIRST:
			case LAST:
			case AFTER:
			case OJ:
			case ACCOUNT:
			case USER:
			case ROLE:
			case START:
			case TRANSACTION:
			case WITHOUT:
			case ESCAPE:
			case SUBPARTITION:
			case STORAGE:
			case SUPER:
			case TEMPORARY:
			case THAN:
			case UNBOUNDED:
			case SIGNED:
			case UPGRADE:
			case VALIDATION:
			case ROLLUP:
			case SOUNDS:
			case UNKNOWN:
			case OFF:
			case ALWAYS:
			case COMMITTED:
			case LEVEL:
			case NO:
			case PASSWORD:
			case PRIVILEGES:
			case ACTION:
			case ALGORITHM:
			case AUTOCOMMIT:
			case BTREE:
			case CHAIN:
			case CHARSET:
			case CHECKSUM:
			case CIPHER:
			case CLIENT:
			case COALESCE:
			case COMMENT:
			case COMPACT:
			case COMPRESSED:
			case COMPRESSION:
			case CONNECTION:
			case CONSISTENT:
			case DATA:
			case DISCARD:
			case DISK:
			case ENCRYPTION:
			case END:
			case ENGINE:
			case EVENT:
			case EXCHANGE:
			case EXECUTE:
			case FILE:
			case FIXED:
			case FOLLOWING:
			case GLOBAL:
			case HASH:
			case IMPORT_:
			case LESS:
			case MEMORY:
			case NONE:
			case PARSER:
			case PARTIAL:
			case PARTITIONING:
			case PERSIST:
			case PRECEDING:
			case PROCESS:
			case PROXY:
			case QUICK:
			case REBUILD:
			case REDUNDANT:
			case RELOAD:
			case REMOVE:
			case REORGANIZE:
			case REPAIR:
			case REVERSE:
			case SESSION:
			case SHUTDOWN:
			case SIMPLE:
			case SLAVE:
			case VISIBLE:
			case INVISIBLE:
			case ENFORCED:
			case AGAINST:
			case LANGUAGE:
			case MODE:
			case QUERY:
			case EXTENDED:
			case EXPANSION:
			case VARIANCE:
			case MAX_ROWS:
			case MIN_ROWS:
			case SQL_BIG_RESULT:
			case SQL_BUFFER_RESULT:
			case SQL_CACHE:
			case SQL_NO_CACHE:
			case STATS_AUTO_RECALC:
			case STATS_PERSISTENT:
			case STATS_SAMPLE_PAGES:
			case ROW_FORMAT:
			case WEIGHT_STRING:
			case COLUMN_FORMAT:
			case INSERT_METHOD:
			case KEY_BLOCK_SIZE:
			case PACK_KEYS:
			case PERSIST_ONLY:
			case BIT_AND:
			case BIT_OR:
			case BIT_XOR:
			case GROUP_CONCAT:
			case JSON_ARRAYAGG:
			case JSON_OBJECTAGG:
			case STD:
			case STDDEV:
			case STDDEV_POP:
			case STDDEV_SAMP:
			case VAR_POP:
			case VAR_SAMP:
			case AUTO_INCREMENT:
			case AVG_ROW_LENGTH:
			case DELAY_KEY_WRITE:
			case ROTATE:
			case MASTER:
			case BINLOG:
			case ERROR:
			case SCHEDULE:
			case COMPLETION:
			case EVERY:
			case HOST:
			case SOCKET:
			case PORT:
			case SERVER:
			case WRAPPER:
			case OPTIONS:
			case OWNER:
			case RETURNS:
			case CONTAINS:
			case SECURITY:
			case INVOKER:
			case TEMPTABLE:
			case MERGE:
			case UNDEFINED:
			case DATAFILE:
			case FILE_BLOCK_SIZE:
			case EXTENT_SIZE:
			case INITIAL_SIZE:
			case AUTOEXTEND_SIZE:
			case MAX_SIZE:
			case NODEGROUP:
			case WAIT:
			case LOGFILE:
			case UNDOFILE:
			case UNDO_BUFFER_SIZE:
			case REDO_BUFFER_SIZE:
			case HANDLER:
			case PREV:
			case ORGANIZATION:
			case DEFINITION:
			case DESCRIPTION:
			case REFERENCE:
			case FOLLOWS:
			case PRECEDES:
			case IMPORT:
			case CONCURRENT:
			case XML:
			case DUMPFILE:
			case SHARE:
			case CODE:
			case CONTEXT:
			case SOURCE:
			case CHANNEL:
			case CLONE:
			case AGGREGATE:
			case INSTALL:
			case COMPONENT:
			case UNINSTALL:
			case RESOURCE:
			case EXPIRE:
			case NEVER:
			case HISTORY:
			case OPTIONAL:
			case REUSE:
			case MAX_QUERIES_PER_HOUR:
			case MAX_UPDATES_PER_HOUR:
			case MAX_CONNECTIONS_PER_HOUR:
			case MAX_USER_CONNECTIONS:
			case RETAIN:
			case RANDOM:
			case OLD:
			case ISSUER:
			case SUBJECT:
			case CACHE:
			case GENERAL:
			case SLOW:
			case USER_RESOURCES:
			case EXPORT:
			case RELAY:
			case HOSTS:
			case FLUSH:
			case RESET:
			case RESTART:
			case IO_THREAD:
			case SQL_THREAD:
			case SQL_BEFORE_GTIDS:
			case SQL_AFTER_GTIDS:
			case MASTER_LOG_FILE:
			case MASTER_LOG_POS:
			case RELAY_LOG_FILE:
			case RELAY_LOG_POS:
			case SQL_AFTER_MTS_GAPS:
			case UNTIL:
			case DEFAULT_AUTH:
			case PLUGIN_DIR:
			case STOP:
			case IDENTIFIER_:
				{
				setState(2038);
				ignoredIdentifier_();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IgnoredIdentifier_Context extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode DOT_() { return getToken(DMLStatementParser.DOT_, 0); }
		public IgnoredIdentifier_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ignoredIdentifier_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIgnoredIdentifier_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIgnoredIdentifier_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIgnoredIdentifier_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IgnoredIdentifier_Context ignoredIdentifier_() throws RecognitionException {
		IgnoredIdentifier_Context _localctx = new IgnoredIdentifier_Context(_ctx, getState());
		enterRule(_localctx, 320, RULE_ignoredIdentifier_);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2041);
			identifier();
			setState(2044);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,243,_ctx) ) {
			case 1:
				{
				setState(2042);
				match(DOT_);
				setState(2043);
				identifier();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IgnoredIdentifiers_Context extends ParserRuleContext {
		public List<IgnoredIdentifier_Context> ignoredIdentifier_() {
			return getRuleContexts(IgnoredIdentifier_Context.class);
		}
		public IgnoredIdentifier_Context ignoredIdentifier_(int i) {
			return getRuleContext(IgnoredIdentifier_Context.class,i);
		}
		public List<TerminalNode> COMMA_() { return getTokens(DMLStatementParser.COMMA_); }
		public TerminalNode COMMA_(int i) {
			return getToken(DMLStatementParser.COMMA_, i);
		}
		public IgnoredIdentifiers_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ignoredIdentifiers_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).enterIgnoredIdentifiers_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLStatementListener ) ((DMLStatementListener)listener).exitIgnoredIdentifiers_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof DMLStatementVisitor ) return ((DMLStatementVisitor<? extends T>)visitor).visitIgnoredIdentifiers_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IgnoredIdentifiers_Context ignoredIdentifiers_() throws RecognitionException {
		IgnoredIdentifiers_Context _localctx = new IgnoredIdentifiers_Context(_ctx, getState());
		enterRule(_localctx, 322, RULE_ignoredIdentifiers_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2046);
			ignoredIdentifier_();
			setState(2051);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA_) {
				{
				{
				setState(2047);
				match(COMMA_);
				setState(2048);
				ignoredIdentifier_();
				}
				}
				setState(2053);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 110:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 113:
			return booleanPrimary_sempred((BooleanPrimaryContext)_localctx, predIndex);
		case 116:
			return bitExpr_sempred((BitExprContext)_localctx, predIndex);
		case 117:
			return simpleExpr_sempred((SimpleExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 5);
		case 1:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean booleanPrimary_sempred(BooleanPrimaryContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 5);
		case 3:
			return precpred(_ctx, 4);
		case 4:
			return precpred(_ctx, 3);
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean bitExpr_sempred(BitExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 15);
		case 7:
			return precpred(_ctx, 14);
		case 8:
			return precpred(_ctx, 13);
		case 9:
			return precpred(_ctx, 12);
		case 10:
			return precpred(_ctx, 11);
		case 11:
			return precpred(_ctx, 10);
		case 12:
			return precpred(_ctx, 9);
		case 13:
			return precpred(_ctx, 8);
		case 14:
			return precpred(_ctx, 7);
		case 15:
			return precpred(_ctx, 6);
		case 16:
			return precpred(_ctx, 5);
		case 17:
			return precpred(_ctx, 4);
		case 18:
			return precpred(_ctx, 3);
		case 19:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean simpleExpr_sempred(SimpleExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 20:
			return precpred(_ctx, 8);
		case 21:
			return precpred(_ctx, 10);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u02c6\u0809\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080"+
		"\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084\t\u0084\4\u0085"+
		"\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087\4\u0088\t\u0088\4\u0089\t\u0089"+
		"\4\u008a\t\u008a\4\u008b\t\u008b\4\u008c\t\u008c\4\u008d\t\u008d\4\u008e"+
		"\t\u008e\4\u008f\t\u008f\4\u0090\t\u0090\4\u0091\t\u0091\4\u0092\t\u0092"+
		"\4\u0093\t\u0093\4\u0094\t\u0094\4\u0095\t\u0095\4\u0096\t\u0096\4\u0097"+
		"\t\u0097\4\u0098\t\u0098\4\u0099\t\u0099\4\u009a\t\u009a\4\u009b\t\u009b"+
		"\4\u009c\t\u009c\4\u009d\t\u009d\4\u009e\t\u009e\4\u009f\t\u009f\4\u00a0"+
		"\t\u00a0\4\u00a1\t\u00a1\4\u00a2\t\u00a2\4\u00a3\t\u00a3\3\2\3\2\3\2\5"+
		"\2\u014a\n\2\3\2\3\2\5\2\u014e\n\2\3\2\3\2\3\2\5\2\u0153\n\2\3\2\5\2\u0156"+
		"\n\2\3\3\5\3\u0159\n\3\3\3\5\3\u015c\n\3\3\4\5\4\u015f\n\4\3\4\3\4\3\4"+
		"\3\4\7\4\u0165\n\4\f\4\16\4\u0168\13\4\3\5\5\5\u016b\n\5\3\5\3\5\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\7\6\u0176\n\6\f\6\16\6\u0179\13\6\3\7\3\7\5\7\u017d"+
		"\n\7\3\7\5\7\u0180\n\7\3\7\3\7\5\7\u0184\n\7\3\7\3\7\3\7\5\7\u0189\n\7"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\5\t\u0192\n\t\3\t\5\t\u0195\n\t\3\t\5\t\u0198"+
		"\n\t\3\n\5\n\u019b\n\n\3\n\5\n\u019e\n\n\3\13\3\13\3\13\3\13\3\f\3\f\3"+
		"\f\3\f\7\f\u01a8\n\f\f\f\16\f\u01ab\13\f\3\r\3\r\3\r\3\r\7\r\u01b1\n\r"+
		"\f\r\16\r\u01b4\13\r\3\r\3\r\3\r\3\r\5\r\u01ba\n\r\3\16\3\16\3\16\5\16"+
		"\u01bf\n\16\3\17\3\17\3\17\3\20\3\20\3\20\3\20\5\20\u01c8\n\20\3\20\5"+
		"\20\u01cb\n\20\3\21\5\21\u01ce\n\21\3\21\5\21\u01d1\n\21\3\21\5\21\u01d4"+
		"\n\21\3\22\3\22\3\22\5\22\u01d9\n\22\3\22\5\22\u01dc\n\22\3\22\5\22\u01df"+
		"\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u01ea\n\23\3\24"+
		"\3\24\5\24\u01ee\n\24\3\24\3\24\3\24\5\24\u01f3\n\24\7\24\u01f5\n\24\f"+
		"\24\16\24\u01f8\13\24\3\25\5\25\u01fb\n\25\3\25\3\25\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\7\26\u0205\n\26\f\26\16\26\u0208\13\26\3\26\3\26\5\26\u020c"+
		"\n\26\3\27\3\27\3\27\3\27\5\27\u0212\n\27\3\30\3\30\3\30\3\30\5\30\u0218"+
		"\n\30\3\31\3\31\3\31\3\31\5\31\u021e\n\31\3\31\5\31\u0221\n\31\3\32\3"+
		"\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u022d\n\32\3\32\3\32"+
		"\5\32\u0231\n\32\3\32\3\32\5\32\u0235\n\32\3\33\3\33\3\33\3\33\3\33\3"+
		"\33\5\33\u023d\n\33\3\33\3\33\5\33\u0241\n\33\3\34\3\34\3\34\3\34\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\5\35\u024d\n\35\3\36\3\36\3\36\5\36\u0252\n"+
		"\36\3\36\5\36\u0255\n\36\3\36\3\36\3\36\5\36\u025a\n\36\3\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\3\36\7\36\u0264\n\36\f\36\16\36\u0267\13\36\3\36"+
		"\3\36\5\36\u026b\n\36\3\36\3\36\3\36\5\36\u0270\n\36\3\36\3\36\6\36\u0274"+
		"\n\36\r\36\16\36\u0275\5\36\u0278\n\36\3\36\3\36\6\36\u027c\n\36\r\36"+
		"\16\36\u027d\5\36\u0280\n\36\3\36\3\36\3\36\3\36\5\36\u0286\n\36\3\36"+
		"\3\36\3\36\3\36\7\36\u028c\n\36\f\36\16\36\u028f\13\36\3\36\3\36\5\36"+
		"\u0293\n\36\3\36\5\36\u0296\n\36\3\37\3\37\3\37\5\37\u029b\n\37\3\37\5"+
		"\37\u029e\n\37\3\37\3\37\3\37\5\37\u02a3\n\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\5\37\u02ab\n\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u02b3\n\37\3"+
		"\37\3\37\3\37\3\37\5\37\u02b9\n\37\3\37\3\37\3\37\3\37\7\37\u02bf\n\37"+
		"\f\37\16\37\u02c2\13\37\3\37\3\37\5\37\u02c6\n\37\3\37\5\37\u02c9\n\37"+
		"\3 \3 \5 \u02cd\n \3 \3 \3 \7 \u02d2\n \f \16 \u02d5\13 \3!\3!\5!\u02d9"+
		"\n!\3!\3!\3!\3\"\3\"\3\"\5\"\u02e1\n\"\3\"\7\"\u02e4\n\"\f\"\16\"\u02e7"+
		"\13\"\3#\3#\7#\u02eb\n#\f#\16#\u02ee\13#\3#\3#\5#\u02f2\n#\3#\5#\u02f5"+
		"\n#\3#\5#\u02f8\n#\3#\5#\u02fb\n#\3#\5#\u02fe\n#\3#\5#\u0301\n#\3#\5#"+
		"\u0304\n#\3#\5#\u0307\n#\3#\5#\u030a\n#\3$\3$\3$\3$\3$\3$\3$\3$\5$\u0314"+
		"\n$\3%\3%\3&\3&\5&\u031a\n&\3&\3&\7&\u031e\n&\f&\16&\u0321\13&\3\'\3\'"+
		"\5\'\u0325\n\'\3\'\5\'\u0328\n\'\3\'\5\'\u032b\n\'\3\'\5\'\u032e\n\'\3"+
		"(\3(\5(\u0332\n(\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3,\7,\u033f\n,\f,\16,\u0342"+
		"\13,\3-\3-\3-\3-\3-\3-\5-\u034a\n-\3.\3.\7.\u034e\n.\f.\16.\u0351\13."+
		"\3/\3/\5/\u0355\n/\3/\5/\u0358\n/\3/\5/\u035b\n/\3/\5/\u035e\n/\3/\3/"+
		"\5/\u0362\n/\3/\3/\5/\u0366\n/\3/\3/\3/\3/\5/\u036c\n/\3\60\3\60\3\60"+
		"\3\60\3\60\7\60\u0373\n\60\f\60\16\60\u0376\13\60\3\60\3\60\3\61\3\61"+
		"\3\61\7\61\u037d\n\61\f\61\16\61\u0380\13\61\3\62\3\62\3\62\3\62\3\62"+
		"\3\62\3\62\3\62\5\62\u038a\n\62\5\62\u038c\n\62\3\62\3\62\3\62\3\62\7"+
		"\62\u0392\n\62\f\62\16\62\u0395\13\62\3\62\3\62\3\63\5\63\u039a\n\63\3"+
		"\63\3\63\5\63\u039e\n\63\3\63\3\63\5\63\u03a2\n\63\3\63\3\63\5\63\u03a6"+
		"\n\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\5\63\u03b0\n\63\3\63\3\63"+
		"\5\63\u03b4\n\63\3\64\3\64\3\64\3\64\5\64\u03ba\n\64\3\65\3\65\3\65\3"+
		"\66\3\66\3\66\3\66\3\66\7\66\u03c4\n\66\f\66\16\66\u03c7\13\66\3\66\3"+
		"\66\5\66\u03cb\n\66\3\67\3\67\3\67\38\38\38\38\58\u03d4\n8\38\38\38\3"+
		"8\38\58\u03db\n8\39\39\59\u03df\n9\3:\3:\5:\u03e3\n:\3;\3;\3;\3;\7;\u03e9"+
		"\n;\f;\16;\u03ec\13;\3<\3<\3<\3<\3<\3<\3=\3=\3=\3=\3>\3>\3>\3>\3>\3>\5"+
		">\u03fe\n>\3?\3?\3?\3?\5?\u0404\n?\3?\3?\3?\3?\3?\3?\5?\u040c\n?\3@\3"+
		"@\3@\3@\7@\u0412\n@\f@\16@\u0415\13@\3@\3@\3@\3@\3@\3@\3@\3@\3@\5@\u0420"+
		"\n@\3@\3@\6@\u0424\n@\r@\16@\u0425\5@\u0428\n@\3@\3@\6@\u042c\n@\r@\16"+
		"@\u042d\5@\u0430\n@\5@\u0432\n@\3A\3A\3A\3A\3A\3A\5A\u043a\nA\3B\3B\3"+
		"C\3C\3C\3C\3C\3C\3C\5C\u0445\nC\3D\5D\u0448\nD\3D\3D\5D\u044c\nD\3E\5"+
		"E\u044f\nE\3E\3E\3F\3F\3F\3F\3F\3F\3F\5F\u045a\nF\3G\5G\u045d\nG\3G\3"+
		"G\5G\u0461\nG\3H\5H\u0464\nH\3H\3H\5H\u0468\nH\3I\3I\3J\3J\3K\3K\3L\3"+
		"L\3M\3M\5M\u0474\nM\3N\3N\3O\5O\u0479\nO\3O\5O\u047c\nO\3O\5O\u047f\n"+
		"O\3O\5O\u0482\nO\3O\3O\3P\3P\3Q\3Q\3Q\5Q\u048b\nQ\3Q\3Q\3R\3R\3R\5R\u0492"+
		"\nR\3R\3R\3S\3S\3T\3T\3T\3T\3T\5T\u049d\nT\3U\3U\3U\3U\3U\3U\5U\u04a5"+
		"\nU\3V\3V\5V\u04a9\nV\3W\3W\5W\u04ad\nW\3X\3X\3X\3X\5X\u04b3\nX\3X\5X"+
		"\u04b6\nX\3Y\3Y\3Y\3Y\5Y\u04bc\nY\3Y\5Y\u04bf\nY\3Z\3Z\3[\3[\3\\\5\\\u04c6"+
		"\n\\\3\\\3\\\3\\\7\\\u04cb\n\\\f\\\16\\\u04ce\13\\\3\\\5\\\u04d1\n\\\3"+
		"]\5]\u04d4\n]\3]\3]\3]\7]\u04d9\n]\f]\16]\u04dc\13]\3]\5]\u04df\n]\3^"+
		"\3^\3_\3_\3`\3`\3a\3a\3b\3b\3c\3c\3d\3d\3d\3d\3d\3d\3e\3e\3f\3f\3g\3g"+
		"\3h\3h\3h\3h\3h\5h\u04fe\nh\3i\3i\3j\3j\3k\3k\3l\3l\5l\u0508\nl\3m\3m"+
		"\3n\3n\3o\3o\3o\3p\3p\3p\3p\3p\3p\3p\3p\3p\5p\u051a\np\3p\3p\3p\3p\3p"+
		"\3p\3p\7p\u0523\np\fp\16p\u0526\13p\3q\3q\3r\3r\3s\3s\3s\3s\3s\3s\5s\u0532"+
		"\ns\3s\3s\3s\3s\3s\3s\3s\3s\3s\3s\3s\3s\3s\7s\u0541\ns\fs\16s\u0544\13"+
		"s\3t\3t\3u\3u\5u\u054a\nu\3u\3u\3u\3u\3u\5u\u0551\nu\3u\3u\3u\3u\3u\7"+
		"u\u0558\nu\fu\16u\u055b\13u\3u\3u\3u\3u\5u\u0561\nu\3u\3u\3u\3u\3u\3u"+
		"\3u\3u\3u\3u\3u\3u\5u\u056f\nu\3u\3u\3u\3u\5u\u0575\nu\3u\3u\5u\u0579"+
		"\nu\3u\3u\3u\3u\5u\u057f\nu\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v"+
		"\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v"+
		"\3v\3v\3v\3v\3v\3v\3v\3v\7v\u05ae\nv\fv\16v\u05b1\13v\3w\3w\3w\3w\3w\3"+
		"w\3w\3w\3w\5w\u05bc\nw\3w\3w\3w\3w\7w\u05c2\nw\fw\16w\u05c5\13w\3w\3w"+
		"\3w\5w\u05ca\nw\3w\3w\3w\3w\3w\3w\3w\3w\3w\5w\u05d5\nw\3w\3w\3w\3w\3w"+
		"\3w\3w\5w\u05de\nw\7w\u05e0\nw\fw\16w\u05e3\13w\3x\3x\3x\5x\u05e8\nx\3"+
		"y\3y\3y\5y\u05ed\ny\3y\3y\3y\7y\u05f2\ny\fy\16y\u05f5\13y\3y\5y\u05f8"+
		"\ny\3y\3y\5y\u05fc\ny\3z\3z\3{\3{\3|\3|\3|\3|\3|\3|\5|\u0608\n|\3}\5}"+
		"\u060b\n}\3}\5}\u060e\n}\3}\5}\u0611\n}\3}\5}\u0614\n}\3~\3~\3~\3~\3~"+
		"\7~\u061b\n~\f~\16~\u061e\13~\3\177\3\177\3\177\5\177\u0623\n\177\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080"+
		"\3\u0080\3\u0080\5\u0080\u0631\n\u0080\3\u0081\3\u0081\3\u0082\3\u0082"+
		"\3\u0082\3\u0082\3\u0082\3\u0083\3\u0083\3\u0083\3\u0083\3\u0083\3\u0083"+
		"\3\u0083\3\u0083\3\u0083\3\u0083\3\u0083\5\u0083\u0645\n\u0083\3\u0084"+
		"\3\u0084\3\u0084\5\u0084\u064a\n\u0084\3\u0084\3\u0084\3\u0084\7\u0084"+
		"\u064f\n\u0084\f\u0084\16\u0084\u0652\13\u0084\3\u0084\5\u0084\u0655\n"+
		"\u0084\3\u0084\5\u0084\u0658\n\u0084\3\u0084\3\u0084\5\u0084\u065c\n\u0084"+
		"\3\u0084\3\u0084\3\u0085\3\u0085\3\u0085\3\u0085\3\u0085\7\u0085\u0665"+
		"\n\u0085\f\u0085\16\u0085\u0668\13\u0085\3\u0085\3\u0085\3\u0085\3\u0086"+
		"\3\u0086\3\u0086\3\u0086\3\u0086\3\u0086\3\u0086\3\u0087\3\u0087\3\u0087"+
		"\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087"+
		"\3\u0087\3\u0087\5\u0087\u0682\n\u0087\3\u0088\3\u0088\3\u0088\3\u0088"+
		"\3\u0088\3\u0088\3\u0088\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089"+
		"\3\u0089\5\u0089\u0692\n\u0089\3\u0089\3\u0089\3\u008a\3\u008a\3\u008a"+
		"\3\u008a\3\u008a\3\u008a\3\u008a\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\7\u008b\u06a2\n\u008b\f\u008b\16\u008b\u06a5\13\u008b\3\u008b\3\u008b"+
		"\5\u008b\u06a9\n\u008b\3\u008b\3\u008b\3\u008c\3\u008c\3\u008c\3\u008c"+
		"\3\u008c\3\u008c\3\u008c\3\u008c\3\u008d\3\u008d\3\u008d\3\u008d\3\u008d"+
		"\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e\5\u008e\u06bf\n\u008e\3\u008e"+
		"\5\u008e\u06c2\n\u008e\3\u008e\3\u008e\3\u008f\3\u008f\3\u008f\3\u008f"+
		"\7\u008f\u06ca\n\u008f\f\u008f\16\u008f\u06cd\13\u008f\3\u008f\3\u008f"+
		"\3\u008f\5\u008f\u06d2\n\u008f\3\u0090\3\u0090\5\u0090\u06d6\n\u0090\3"+
		"\u0090\5\u0090\u06d9\n\u0090\3\u0091\3\u0091\3\u0091\3\u0091\3\u0091\7"+
		"\u0091\u06e0\n\u0091\f\u0091\16\u0091\u06e3\13\u0091\3\u0091\5\u0091\u06e6"+
		"\n\u0091\3\u0091\3\u0091\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092"+
		"\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\3\u0092\5\u0092\u0774\n\u0092"+
		"\3\u0093\3\u0093\3\u0093\3\u0093\3\u0093\5\u0093\u077b\n\u0093\3\u0094"+
		"\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094"+
		"\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\3\u0094\5\u0094\u078e"+
		"\n\u0094\3\u0095\3\u0095\5\u0095\u0792\n\u0095\3\u0095\6\u0095\u0795\n"+
		"\u0095\r\u0095\16\u0095\u0796\3\u0095\5\u0095\u079a\n\u0095\3\u0095\3"+
		"\u0095\3\u0096\3\u0096\3\u0096\3\u0096\3\u0096\3\u0097\3\u0097\3\u0097"+
		"\3\u0098\3\u0098\3\u0098\3\u0098\3\u0099\3\u0099\3\u009a\3\u009a\3\u009a"+
		"\3\u009a\3\u009a\7\u009a\u07b1\n\u009a\f\u009a\16\u009a\u07b4\13\u009a"+
		"\3\u009b\3\u009b\3\u009b\5\u009b\u07b9\n\u009b\3\u009b\5\u009b\u07bc\n"+
		"\u009b\3\u009c\3\u009c\5\u009c\u07c0\n\u009c\3\u009c\5\u009c\u07c3\n\u009c"+
		"\3\u009c\5\u009c\u07c6\n\u009c\3\u009c\5\u009c\u07c9\n\u009c\3\u009c\5"+
		"\u009c\u07cc\n\u009c\3\u009c\3\u009c\3\u009c\5\u009c\u07d1\n\u009c\3\u009c"+
		"\5\u009c\u07d4\n\u009c\5\u009c\u07d6\n\u009c\3\u009d\3\u009d\3\u009e\3"+
		"\u009e\3\u009e\3\u009e\5\u009e\u07de\n\u009e\3\u009e\3\u009e\3\u009f\3"+
		"\u009f\3\u009f\3\u009f\7\u009f\u07e6\n\u009f\f\u009f\16\u009f\u07e9\13"+
		"\u009f\3\u009f\3\u009f\3\u00a0\3\u00a0\3\u00a0\5\u00a0\u07f0\n\u00a0\3"+
		"\u00a0\3\u00a0\3\u00a1\3\u00a1\5\u00a1\u07f6\n\u00a1\3\u00a1\3\u00a1\5"+
		"\u00a1\u07fa\n\u00a1\3\u00a2\3\u00a2\3\u00a2\5\u00a2\u07ff\n\u00a2\3\u00a3"+
		"\3\u00a3\3\u00a3\7\u00a3\u0804\n\u00a3\f\u00a3\16\u00a3\u0807\13\u00a3"+
		"\3\u00a3\2\6\u00de\u00e4\u00ea\u00ec\u00a4\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|"+
		"~\u0080\u0082\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096"+
		"\u0098\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae"+
		"\u00b0\u00b2\u00b4\u00b6\u00b8\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6"+
		"\u00c8\u00ca\u00cc\u00ce\u00d0\u00d2\u00d4\u00d6\u00d8\u00da\u00dc\u00de"+
		"\u00e0\u00e2\u00e4\u00e6\u00e8\u00ea\u00ec\u00ee\u00f0\u00f2\u00f4\u00f6"+
		"\u00f8\u00fa\u00fc\u00fe\u0100\u0102\u0104\u0106\u0108\u010a\u010c\u010e"+
		"\u0110\u0112\u0114\u0116\u0118\u011a\u011c\u011e\u0120\u0122\u0124\u0126"+
		"\u0128\u012a\u012c\u012e\u0130\u0132\u0134\u0136\u0138\u013a\u013c\u013e"+
		"\u0140\u0142\u0144\2*\4\2\u0114\u0114\u0166\u0167\4\2HH\u00c4\u00c4\4"+
		"\2\u0114\u0114\u0167\u0167\5\2\u00a4\u00a4\u00c6\u00c7\u01ca\u01ca\4\2"+
		"\u00a4\u00a4\u00c6\u00c6\4\2\u0167\u0167\u01d3\u01d3\4\2\u00c1\u00c1\u012a"+
		"\u012a\3\2\u00bd\u00be\4\2\u00d6\u00d6\u01d5\u01d5\4\2KKmm\4\2\u016a\u016a"+
		"\u016c\u016c\5\2KKmm\u00c3\u00c3\5\2\u00b5\u00b5\u0125\u0125\u012a\u012a"+
		"\4\2;;@@\4\2UUYY\3\2WX\3\2\u0082\u0084\3\2hiT\2\63\63AAFFnnw|\u0082\u0084"+
		"\u0087\u0094\u0096\u0098\u009a\u009a\u009c\u009d\u00a0\u00a5\u00ab\u00ab"+
		"\u00bb\u00c0\u00c2\u00c2\u00c4\u00c9\u00d0\u00d4\u00d7\u00d7\u00d9\u00d9"+
		"\u00dc\u00dd\u00df\u00df\u00e1\u00e2\u00e4\u00e4\u00e7\u00e8\u00ea\u00ea"+
		"\u00ec\u00f0\u00f3\u00f5\u00f7\u00f8\u00fc\u00fd\u00ff\u00ff\u0102\u0103"+
		"\u0105\u0109\u010b\u0110\u0113\u0113\u0116\u0117\u0119\u011c\u011e\u011e"+
		"\u0120\u0120\u0122\u0124\u0127\u0128\u012b\u012b\u012f\u012f\u0133\u0134"+
		"\u013a\u0141\u0143\u0143\u0145\u0145\u0147\u0148\u014a\u014b\u014f\u014f"+
		"\u0152\u0152\u0154\u0156\u0159\u015a\u015c\u0165\u0168\u016a\u016c\u016c"+
		"\u016e\u0170\u0172\u0172\u0177\u0178\u017c\u018b\u018d\u018e\u0190\u0190"+
		"\u01a1\u01a7\u01aa\u01b0\u01b2\u01b3\u01b6\u01b7\u01ba\u01d1\u01d3\u01d3"+
		"\u01db\u01db\u01dd\u01dd\u01df\u01df\u01e7\u01e7\u01ed\u01ed\u01f3\u01f3"+
		"\u01f8\u01f8\u01fc\u01fd\u0200\u0201\u0203\u0203\u020a\u020a\u020e\u0219"+
		"\u021b\u021e\u0220\u0224\u0226\u0228\u02b0\u02bc\6\2\u0127\u0127\u013d"+
		"\u013d\u0152\u0152\u017f\u017f\3\2\u02bd\u02be\4\2\u00c8\u00c8\u01e2\u01e2"+
		"\3\2-/\3\2\u01cf\u01d0\4\2\3\4cd\4\2\5\5ff\4\2gi\u00ee\u00ee\3\2mn\3\2"+
		"\27\34\3\2\u00ce\u00cf\5\2\5\6\16\17\u00d8\u00d8\6\2\u0090\u0094\u0163"+
		"\u0163\u0180\u0182\u0184\u018b\4\2\u00d6\u00d6\u0142\u0142\4\2PP\u00e0"+
		"\u00e0\5\2\u00e3\u00e3\u0101\u0101\u012e\u012e\3\2st\4\2\u0087\u008f\u0195"+
		"\u019f\3\2\u00e6\u00e7\f\288|~\u0082\u0084\u0087\u0087\u00a8\u00a8\u00aa"+
		"\u00aa\u00ac\u00b3\u00d8\u00d8\u0241\u0247\u02a0\u02af\3\2~\177\2\u092c"+
		"\2\u0146\3\2\2\2\4\u0158\3\2\2\2\6\u015e\3\2\2\2\b\u016a\3\2\2\2\n\u016e"+
		"\3\2\2\2\f\u017a\3\2\2\2\16\u018a\3\2\2\2\20\u018c\3\2\2\2\22\u019a\3"+
		"\2\2\2\24\u019f\3\2\2\2\26\u01a3\3\2\2\2\30\u01b9\3\2\2\2\32\u01be\3\2"+
		"\2\2\34\u01c0\3\2\2\2\36\u01c3\3\2\2\2 \u01cd\3\2\2\2\"\u01d5\3\2\2\2"+
		"$\u01e9\3\2\2\2&\u01eb\3\2\2\2(\u01fa\3\2\2\2*\u01fe\3\2\2\2,\u020d\3"+
		"\2\2\2.\u0217\3\2\2\2\60\u0219\3\2\2\2\62\u0222\3\2\2\2\64\u0236\3\2\2"+
		"\2\66\u0242\3\2\2\28\u0246\3\2\2\2:\u024e\3\2\2\2<\u0297\3\2\2\2>\u02ca"+
		"\3\2\2\2@\u02d6\3\2\2\2B\u02dd\3\2\2\2D\u02e8\3\2\2\2F\u0313\3\2\2\2H"+
		"\u0315\3\2\2\2J\u0319\3\2\2\2L\u032d\3\2\2\2N\u0331\3\2\2\2P\u0333\3\2"+
		"\2\2R\u0335\3\2\2\2T\u0338\3\2\2\2V\u033b\3\2\2\2X\u0349\3\2\2\2Z\u034b"+
		"\3\2\2\2\\\u036b\3\2\2\2^\u036d\3\2\2\2`\u0379\3\2\2\2b\u0381\3\2\2\2"+
		"d\u03b3\3\2\2\2f\u03b9\3\2\2\2h\u03bb\3\2\2\2j\u03be\3\2\2\2l\u03cc\3"+
		"\2\2\2n\u03cf\3\2\2\2p\u03de\3\2\2\2r\u03e2\3\2\2\2t\u03e4\3\2\2\2v\u03ed"+
		"\3\2\2\2x\u03f3\3\2\2\2z\u03fd\3\2\2\2|\u040b\3\2\2\2~\u0431\3\2\2\2\u0080"+
		"\u0439\3\2\2\2\u0082\u043b\3\2\2\2\u0084\u0444\3\2\2\2\u0086\u0447\3\2"+
		"\2\2\u0088\u044e\3\2\2\2\u008a\u0459\3\2\2\2\u008c\u045c\3\2\2\2\u008e"+
		"\u0463\3\2\2\2\u0090\u0469\3\2\2\2\u0092\u046b\3\2\2\2\u0094\u046d\3\2"+
		"\2\2\u0096\u046f\3\2\2\2\u0098\u0473\3\2\2\2\u009a\u0475\3\2\2\2\u009c"+
		"\u047b\3\2\2\2\u009e\u0485\3\2\2\2\u00a0\u048a\3\2\2\2\u00a2\u0491\3\2"+
		"\2\2\u00a4\u0495\3\2\2\2\u00a6\u049c\3\2\2\2\u00a8\u04a4\3\2\2\2\u00aa"+
		"\u04a8\3\2\2\2\u00ac\u04ac\3\2\2\2\u00ae\u04b5\3\2\2\2\u00b0\u04be\3\2"+
		"\2\2\u00b2\u04c0\3\2\2\2\u00b4\u04c2\3\2\2\2\u00b6\u04c5\3\2\2\2\u00b8"+
		"\u04d3\3\2\2\2\u00ba\u04e0\3\2\2\2\u00bc\u04e2\3\2\2\2\u00be\u04e4\3\2"+
		"\2\2\u00c0\u04e6\3\2\2\2\u00c2\u04e8\3\2\2\2\u00c4\u04ea\3\2\2\2\u00c6"+
		"\u04ec\3\2\2\2\u00c8\u04f2\3\2\2\2\u00ca\u04f4\3\2\2\2\u00cc\u04f6\3\2"+
		"\2\2\u00ce\u04fd\3\2\2\2\u00d0\u04ff\3\2\2\2\u00d2\u0501\3\2\2\2\u00d4"+
		"\u0503\3\2\2\2\u00d6\u0507\3\2\2\2\u00d8\u0509\3\2\2\2\u00da\u050b\3\2"+
		"\2\2\u00dc\u050d\3\2\2\2\u00de\u0519\3\2\2\2\u00e0\u0527\3\2\2\2\u00e2"+
		"\u0529\3\2\2\2\u00e4\u052b\3\2\2\2\u00e6\u0545\3\2\2\2\u00e8\u057e\3\2"+
		"\2\2\u00ea\u0580\3\2\2\2\u00ec\u05d4\3\2\2\2\u00ee\u05e7\3\2\2\2\u00f0"+
		"\u05e9\3\2\2\2\u00f2\u05fd\3\2\2\2\u00f4\u05ff\3\2\2\2\u00f6\u0601\3\2"+
		"\2\2\u00f8\u060a\3\2\2\2\u00fa\u0615\3\2\2\2\u00fc\u061f\3\2\2\2\u00fe"+
		"\u0630\3\2\2\2\u0100\u0632\3\2\2\2\u0102\u0634\3\2\2\2\u0104\u0644\3\2"+
		"\2\2\u0106\u0646\3\2\2\2\u0108\u065f\3\2\2\2\u010a\u066c\3\2\2\2\u010c"+
		"\u0681\3\2\2\2\u010e\u0683\3\2\2\2\u0110\u068a\3\2\2\2\u0112\u0695\3\2"+
		"\2\2\u0114\u069c\3\2\2\2\u0116\u06ac\3\2\2\2\u0118\u06b4\3\2\2\2\u011a"+
		"\u06b9\3\2\2\2\u011c\u06c5\3\2\2\2\u011e\u06d3\3\2\2\2\u0120\u06da\3\2"+
		"\2\2\u0122\u0773\3\2\2\2\u0124\u0775\3\2\2\2\u0126\u078d\3\2\2\2\u0128"+
		"\u078f\3\2\2\2\u012a\u079d\3\2\2\2\u012c\u07a2\3\2\2\2\u012e\u07a5\3\2"+
		"\2\2\u0130\u07a9\3\2\2\2\u0132\u07ab\3\2\2\2\u0134\u07b8\3\2\2\2\u0136"+
		"\u07d5\3\2\2\2\u0138\u07d7\3\2\2\2\u013a\u07d9\3\2\2\2\u013c\u07e1\3\2"+
		"\2\2\u013e\u07ec\3\2\2\2\u0140\u07f3\3\2\2\2\u0142\u07fb\3\2\2\2\u0144"+
		"\u0800\3\2\2\2\u0146\u0147\7-\2\2\u0147\u0149\5\4\3\2\u0148\u014a\7G\2"+
		"\2\u0149\u0148\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014d"+
		"\5\u00a0Q\2\u014c\u014e\5^\60\2\u014d\u014c\3\2\2\2\u014d\u014e\3\2\2"+
		"\2\u014e\u0152\3\2\2\2\u014f\u0153\5\6\4\2\u0150\u0153\5\26\f\2\u0151"+
		"\u0153\5\b\5\2\u0152\u014f\3\2\2\2\u0152\u0150\3\2\2\2\u0152\u0151\3\2"+
		"\2\2\u0153\u0155\3\2\2\2\u0154\u0156\5\n\6\2\u0155\u0154\3\2\2\2\u0155"+
		"\u0156\3\2\2\2\u0156\3\3\2\2\2\u0157\u0159\t\2\2\2\u0158\u0157\3\2\2\2"+
		"\u0158\u0159\3\2\2\2\u0159\u015b\3\2\2\2\u015a\u015c\7\u012a\2\2\u015b"+
		"\u015a\3\2\2\2\u015b\u015c\3\2\2\2\u015c\5\3\2\2\2\u015d\u015f\5\u00b8"+
		"]\2\u015e\u015d\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u0160\3\2\2\2\u0160"+
		"\u0161\t\3\2\2\u0161\u0166\5\30\r\2\u0162\u0163\7$\2\2\u0163\u0165\5\30"+
		"\r\2\u0164\u0162\3\2\2\2\u0165\u0168\3\2\2\2\u0166\u0164\3\2\2\2\u0166"+
		"\u0167\3\2\2\2\u0167\7\3\2\2\2\u0168\u0166\3\2\2\2\u0169\u016b\5\u00b8"+
		"]\2\u016a\u0169\3\2\2\2\u016a\u016b\3\2\2\2\u016b\u016c\3\2\2\2\u016c"+
		"\u016d\5(\25\2\u016d\t\3\2\2\2\u016e\u016f\7]\2\2\u016f\u0170\7\u00c5"+
		"\2\2\u0170\u0171\7@\2\2\u0171\u0172\7.\2\2\u0172\u0177\5\24\13\2\u0173"+
		"\u0174\7$\2\2\u0174\u0176\5\24\13\2\u0175\u0173\3\2\2\2\u0176\u0179\3"+
		"\2\2\2\u0177\u0175\3\2\2\2\u0177\u0178\3\2\2\2\u0178\13\3\2\2\2\u0179"+
		"\u0177\3\2\2\2\u017a\u017c\7\u00c1\2\2\u017b\u017d\5\16\b\2\u017c\u017b"+
		"\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017f\3\2\2\2\u017e\u0180\7G\2\2\u017f"+
		"\u017e\3\2\2\2\u017f\u0180\3\2\2\2\u0180\u0181\3\2\2\2\u0181\u0183\5\u00a0"+
		"Q\2\u0182\u0184\5^\60\2\u0183\u0182\3\2\2\2\u0183\u0184\3\2\2\2\u0184"+
		"\u0188\3\2\2\2\u0185\u0189\5\6\4\2\u0186\u0189\5\26\f\2\u0187\u0189\5"+
		"\b\5\2\u0188\u0185\3\2\2\2\u0188\u0186\3\2\2\2\u0188\u0187\3\2\2\2\u0189"+
		"\r\3\2\2\2\u018a\u018b\t\4\2\2\u018b\17\3\2\2\2\u018c\u018d\7.\2\2\u018d"+
		"\u018e\5\22\n\2\u018e\u018f\5V,\2\u018f\u0191\5\26\f\2\u0190\u0192\5h"+
		"\65\2\u0191\u0190\3\2\2\2\u0191\u0192\3\2\2\2\u0192\u0194\3\2\2\2\u0193"+
		"\u0195\5\u0132\u009a\2\u0194\u0193\3\2\2\2\u0194\u0195\3\2\2\2\u0195\u0197"+
		"\3\2\2\2\u0196\u0198\5n8\2\u0197\u0196\3\2\2\2\u0197\u0198\3\2\2\2\u0198"+
		"\21\3\2\2\2\u0199\u019b\7\u0167\2\2\u019a\u0199\3\2\2\2\u019a\u019b\3"+
		"\2\2\2\u019b\u019d\3\2\2\2\u019c\u019e\7\u012a\2\2\u019d\u019c\3\2\2\2"+
		"\u019d\u019e\3\2\2\2\u019e\23\3\2\2\2\u019f\u01a0\5\u00a2R\2\u01a0\u01a1"+
		"\7\27\2\2\u01a1\u01a2\5\32\16\2\u01a2\25\3\2\2\2\u01a3\u01a4\78\2\2\u01a4"+
		"\u01a9\5\24\13\2\u01a5\u01a6\7$\2\2\u01a6\u01a8\5\24\13\2\u01a7\u01a5"+
		"\3\2\2\2\u01a8\u01ab\3\2\2\2\u01a9\u01a7\3\2\2\2\u01a9\u01aa\3\2\2\2\u01aa"+
		"\27\3\2\2\2\u01ab\u01a9\3\2\2\2\u01ac\u01ad\7\36\2\2\u01ad\u01b2\5\32"+
		"\16\2\u01ae\u01af\7$\2\2\u01af\u01b1\5\32\16\2\u01b0\u01ae\3\2\2\2\u01b1"+
		"\u01b4\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b5\3\2"+
		"\2\2\u01b4\u01b2\3\2\2\2\u01b5\u01b6\7\37\2\2\u01b6\u01ba\3\2\2\2\u01b7"+
		"\u01b8\7\36\2\2\u01b8\u01ba\7\37\2\2\u01b9\u01ac\3\2\2\2\u01b9\u01b7\3"+
		"\2\2\2\u01ba\31\3\2\2\2\u01bb\u01bf\5\u00dep\2\u01bc\u01bf\7\u0095\2\2"+
		"\u01bd\u01bf\5\34\17\2\u01be\u01bb\3\2\2\2\u01be\u01bc\3\2\2\2\u01be\u01bd"+
		"\3\2\2\2\u01bf\33\3\2\2\2\u01c0\u01c1\7\u01a0\2\2\u01c1\u01c2\7\u02be"+
		"\2\2\u01c2\35\3\2\2\2\u01c3\u01c4\7/\2\2\u01c4\u01c7\5 \21\2\u01c5\u01c8"+
		"\5\"\22\2\u01c6\u01c8\5$\23\2\u01c7\u01c5\3\2\2\2\u01c7\u01c6\3\2\2\2"+
		"\u01c8\u01ca\3\2\2\2\u01c9\u01cb\5h\65\2\u01ca\u01c9\3\2\2\2\u01ca\u01cb"+
		"\3\2\2\2\u01cb\37\3\2\2\2\u01cc\u01ce\7\u0167\2\2\u01cd\u01cc\3\2\2\2"+
		"\u01cd\u01ce\3\2\2\2\u01ce\u01d0\3\2\2\2\u01cf\u01d1\7\u0141\2\2\u01d0"+
		"\u01cf\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d3\3\2\2\2\u01d2\u01d4\7\u012a"+
		"\2\2\u01d3\u01d2\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d4!\3\2\2\2\u01d5\u01d6"+
		"\7Q\2\2\u01d6\u01db\5\u00a0Q\2\u01d7\u01d9\7\\\2\2\u01d8\u01d7\3\2\2\2"+
		"\u01d8\u01d9\3\2\2\2\u01d9\u01da\3\2\2\2\u01da\u01dc\5N(\2\u01db\u01d8"+
		"\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc\u01de\3\2\2\2\u01dd\u01df\5^\60\2\u01de"+
		"\u01dd\3\2\2\2\u01de\u01df\3\2\2\2\u01df#\3\2\2\2\u01e0\u01e1\5&\24\2"+
		"\u01e1\u01e2\7Q\2\2\u01e2\u01e3\5V,\2\u01e3\u01ea\3\2\2\2\u01e4\u01e5"+
		"\7Q\2\2\u01e5\u01e6\5&\24\2\u01e6\u01e7\7Z\2\2\u01e7\u01e8\5V,\2\u01e8"+
		"\u01ea\3\2\2\2\u01e9\u01e0\3\2\2\2\u01e9\u01e4\3\2\2\2\u01ea%\3\2\2\2"+
		"\u01eb\u01ed\5\u00a0Q\2\u01ec\u01ee\7\24\2\2\u01ed\u01ec\3\2\2\2\u01ed"+
		"\u01ee\3\2\2\2\u01ee\u01f6\3\2\2\2\u01ef\u01f0\7$\2\2\u01f0\u01f2\5\u00a0"+
		"Q\2\u01f1\u01f3\7\24\2\2\u01f2\u01f1\3\2\2\2\u01f2\u01f3\3\2\2\2\u01f3"+
		"\u01f5\3\2\2\2\u01f4\u01ef\3\2\2\2\u01f5\u01f8\3\2\2\2\u01f6\u01f4\3\2"+
		"\2\2\u01f6\u01f7\3\2\2\2\u01f7\'\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f9\u01fb"+
		"\5> \2\u01fa\u01f9\3\2\2\2\u01fa\u01fb\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc"+
		"\u01fd\5B\"\2\u01fd)\3\2\2\2\u01fe\u01ff\7\u0099\2\2\u01ff\u020b\5\u0098"+
		"M\2\u0200\u0201\7\36\2\2\u0201\u0206\5\u00dep\2\u0202\u0203\7$\2\2\u0203"+
		"\u0205\5\u00dep\2\u0204\u0202\3\2\2\2\u0205\u0208\3\2\2\2\u0206\u0204"+
		"\3\2\2\2\u0206\u0207\3\2\2\2\u0207\u0209\3\2\2\2\u0208\u0206\3\2\2\2\u0209"+
		"\u020a\7\37\2\2\u020a\u020c\3\2\2\2\u020b\u0200\3\2\2\2\u020b\u020c\3"+
		"\2\2\2\u020c+\3\2\2\2\u020d\u020e\7\u009c\2\2\u020e\u0211\5\u00dep\2\u020f"+
		"\u0210\7$\2\2\u0210\u0212\5\u00dep\2\u0211\u020f\3\2\2\2\u0211\u0212\3"+
		"\2\2\2\u0212-\3\2\2\2\u0213\u0218\5\60\31\2\u0214\u0218\5\62\32\2\u0215"+
		"\u0218\5\64\33\2\u0216\u0218\5\66\34\2\u0217\u0213\3\2\2\2\u0217\u0214"+
		"\3\2\2\2\u0217\u0215\3\2\2\2\u0217\u0216\3\2\2\2\u0218/\3\2\2\2\u0219"+
		"\u021a\7\u01c9\2\2\u021a\u021b\5\u00a0Q\2\u021b\u0220\7\u00a3\2\2\u021c"+
		"\u021e\7\\\2\2\u021d\u021c\3\2\2\2\u021d\u021e\3\2\2\2\u021e\u021f\3\2"+
		"\2\2\u021f\u0221\5\u0098M\2\u0220\u021d\3\2\2\2\u0220\u0221\3\2\2\2\u0221"+
		"\61\3\2\2\2\u0222\u0223\7\u01c9\2\2\u0223\u0224\5\u00a0Q\2\u0224\u0225"+
		"\7\u00f9\2\2\u0225\u022c\5\u0098M\2\u0226\u0227\5\u00e6t\2\u0227\u0228"+
		"\7\36\2\2\u0228\u0229\5\u0098M\2\u0229\u022a\7\37\2\2\u022a\u022d\3\2"+
		"\2\2\u022b\u022d\t\5\2\2\u022c\u0226\3\2\2\2\u022c\u022b\3\2\2\2\u022d"+
		"\u0230\3\2\2\2\u022e\u022f\7[\2\2\u022f\u0231\5\u00dep\2\u0230\u022e\3"+
		"\2\2\2\u0230\u0231\3\2\2\2\u0231\u0234\3\2\2\2\u0232\u0233\7v\2\2\u0233"+
		"\u0235\5\u0088E\2\u0234\u0232\3\2\2\2\u0234\u0235\3\2\2\2\u0235\63\3\2"+
		"\2\2\u0236\u0237\7\u01c9\2\2\u0237\u0238\5\u00a0Q\2\u0238\u0239\7\u00f9"+
		"\2\2\u0239\u023c\t\6\2\2\u023a\u023b\7[\2\2\u023b\u023d\5\u00dep\2\u023c"+
		"\u023a\3\2\2\2\u023c\u023d\3\2\2\2\u023d\u0240\3\2\2\2\u023e\u023f\7v"+
		"\2\2\u023f\u0241\5\u0088E\2\u0240\u023e\3\2\2\2\u0240\u0241\3\2\2\2\u0241"+
		"\65\3\2\2\2\u0242\u0243\7\u01c9\2\2\u0243\u0244\5\u00a0Q\2\u0244\u0245"+
		"\7\u00a2\2\2\u0245\67\3\2\2\2\u0246\u0247\7\u01d1\2\2\u0247\u0248\79\2"+
		"\2\u0248\u0249\7Q\2\2\u0249\u024c\7\u02be\2\2\u024a\u024b\7$\2\2\u024b"+
		"\u024d\7\u02be\2\2\u024c\u024a\3\2\2\2\u024c\u024d\3\2\2\2\u024d9\3\2"+
		"\2\2\u024e\u024f\7\u01d2\2\2\u024f\u0251\7\u0113\2\2\u0250\u0252\t\7\2"+
		"\2\u0251\u0250\3\2\2\2\u0251\u0252\3\2\2\2\u0252\u0254\3\2\2\2\u0253\u0255"+
		"\7\u00a1\2\2\u0254\u0253\3\2\2\2\u0254\u0255\3\2\2\2\u0255\u0256\3\2\2"+
		"\2\u0256\u0257\7\u01d4\2\2\u0257\u0259\7\u02be\2\2\u0258\u025a\t\b\2\2"+
		"\u0259\u0258\3\2\2\2\u0259\u025a\3\2\2\2\u025a\u025b\3\2\2\2\u025b\u025c"+
		"\7G\2\2\u025c\u025d\79\2\2\u025d\u026a\5\u00a0Q\2\u025e\u025f\7\u00db"+
		"\2\2\u025f\u0260\7\36\2\2\u0260\u0265\5\u0098M\2\u0261\u0262\7$\2\2\u0262"+
		"\u0264\5\u0098M\2\u0263\u0261\3\2\2\2\u0264\u0267\3\2\2\2\u0265\u0263"+
		"\3\2\2\2\u0265\u0266\3\2\2\2\u0266\u0268\3\2\2\2\u0267\u0265\3\2\2\2\u0268"+
		"\u0269\7\37\2\2\u0269\u026b\3\2\2\2\u026a\u025e\3\2\2\2\u026a\u026b\3"+
		"\2\2\2\u026b\u026f\3\2\2\2\u026c\u026d\7\177\2\2\u026d\u026e\78\2\2\u026e"+
		"\u0270\5\u0098M\2\u026f\u026c\3\2\2\2\u026f\u0270\3\2\2\2\u0270\u0277"+
		"\3\2\2\2\u0271\u0273\t\t\2\2\u0272\u0274\5|?\2\u0273\u0272\3\2\2\2\u0274"+
		"\u0275\3\2\2\2\u0275\u0273\3\2\2\2\u0275\u0276\3\2\2\2\u0276\u0278\3\2"+
		"\2\2\u0277\u0271\3\2\2\2\u0277\u0278\3\2\2\2\u0278\u027f\3\2\2\2\u0279"+
		"\u027b\7\u01d5\2\2\u027a\u027c\5z>\2\u027b\u027a\3\2\2\2\u027c\u027d\3"+
		"\2\2\2\u027d\u027b\3\2\2\2\u027d\u027e\3\2\2\2\u027e\u0280\3\2\2\2\u027f"+
		"\u0279\3\2\2\2\u027f\u0280\3\2\2\2\u0280\u0285\3\2\2\2\u0281\u0282\7\u012a"+
		"\2\2\u0282\u0283\5\u0088E\2\u0283\u0284\t\n\2\2\u0284\u0286\3\2\2\2\u0285"+
		"\u0281\3\2\2\2\u0285\u0286\3\2\2\2\u0286\u0292\3\2\2\2\u0287\u0288\7\36"+
		"\2\2\u0288\u028d\5\u0098M\2\u0289\u028a\7$\2\2\u028a\u028c\5\u0098M\2"+
		"\u028b\u0289\3\2\2\2\u028c\u028f\3\2\2\2\u028d\u028b\3\2\2\2\u028d\u028e"+
		"\3\2\2\2\u028e\u0290\3\2\2\2\u028f\u028d\3\2\2\2\u0290\u0291\7\37\2\2"+
		"\u0291\u0293\3\2\2\2\u0292\u0287\3\2\2\2\u0292\u0293\3\2\2\2\u0293\u0295"+
		"\3\2\2\2\u0294\u0296\5\26\f\2\u0295\u0294\3\2\2\2\u0295\u0296\3\2\2\2"+
		"\u0296;\3\2\2\2\u0297\u0298\7\u01d2\2\2\u0298\u029a\7\u01db\2\2\u0299"+
		"\u029b\t\7\2\2\u029a\u0299\3\2\2\2\u029a\u029b\3\2\2\2\u029b\u029d\3\2"+
		"\2\2\u029c\u029e\7\u00a1\2\2\u029d\u029c\3\2\2\2\u029d\u029e\3\2\2\2\u029e"+
		"\u029f\3\2\2\2\u029f\u02a0\7\u01d4\2\2\u02a0\u02a2\7\u02be\2\2\u02a1\u02a3"+
		"\t\b\2\2\u02a2\u02a1\3\2\2\2\u02a2\u02a3\3\2\2\2\u02a3\u02a4\3\2\2\2\u02a4"+
		"\u02a5\7G\2\2\u02a5\u02a6\79\2\2\u02a6\u02aa\5\u00a0Q\2\u02a7\u02a8\7"+
		"\177\2\2\u02a8\u02a9\78\2\2\u02a9\u02ab\5\u0098M\2\u02aa\u02a7\3\2\2\2"+
		"\u02aa\u02ab\3\2\2\2\u02ab\u02b2\3\2\2\2\u02ac\u02ad\7\u00d6\2\2\u02ad"+
		"\u02ae\7\u0129\2\2\u02ae\u02af\7r\2\2\u02af\u02b0\7\33\2\2\u02b0\u02b1"+
		"\7\u02be\2\2\u02b1\u02b3\7\31\2\2\u02b2\u02ac\3\2\2\2\u02b2\u02b3\3\2"+
		"\2\2\u02b3\u02b8\3\2\2\2\u02b4\u02b5\7\u012a\2\2\u02b5\u02b6\5\u0088E"+
		"\2\u02b6\u02b7\t\n\2\2\u02b7\u02b9\3\2\2\2\u02b8\u02b4\3\2\2\2\u02b8\u02b9"+
		"\3\2\2\2\u02b9\u02c5\3\2\2\2\u02ba\u02bb\7\36\2\2\u02bb\u02c0\5\u0098"+
		"M\2\u02bc\u02bd\7$\2\2\u02bd\u02bf\5\u0098M\2\u02be\u02bc\3\2\2\2\u02bf"+
		"\u02c2\3\2\2\2\u02c0\u02be\3\2\2\2\u02c0\u02c1\3\2\2\2\u02c1\u02c3\3\2"+
		"\2\2\u02c2\u02c0\3\2\2\2\u02c3\u02c4\7\37\2\2\u02c4\u02c6\3\2\2\2\u02c5"+
		"\u02ba\3\2\2\2\u02c5\u02c6\3\2\2\2\u02c6\u02c8\3\2\2\2\u02c7\u02c9\5\26"+
		"\f\2\u02c8\u02c7\3\2\2\2\u02c8\u02c9\3\2\2\2\u02c9=\3\2\2\2\u02ca\u02cc"+
		"\7I\2\2\u02cb\u02cd\7\u0144\2\2\u02cc\u02cb\3\2\2\2\u02cc\u02cd\3\2\2"+
		"\2\u02cd\u02ce\3\2\2\2\u02ce\u02d3\5@!\2\u02cf\u02d0\7$\2\2\u02d0\u02d2"+
		"\5@!\2\u02d1\u02cf\3\2\2\2\u02d2\u02d5\3\2\2\2\u02d3\u02d1\3\2\2\2\u02d3"+
		"\u02d4\3\2\2\2\u02d4?\3\2\2\2\u02d5\u02d3\3\2\2\2\u02d6\u02d8\5\u0142"+
		"\u00a2\2\u02d7\u02d9\5\u00b8]\2\u02d8\u02d7\3\2\2\2\u02d8\u02d9\3\2\2"+
		"\2\u02d9\u02da\3\2\2\2\u02da\u02db\7\\\2\2\u02db\u02dc\5x=\2\u02dcA\3"+
		"\2\2\2\u02dd\u02e5\5D#\2\u02de\u02e0\7J\2\2\u02df\u02e1\t\13\2\2\u02e0"+
		"\u02df\3\2\2\2\u02e0\u02e1\3\2\2\2\u02e1\u02e2\3\2\2\2\u02e2\u02e4\5D"+
		"#\2\u02e3\u02de\3\2\2\2\u02e4\u02e7\3\2\2\2\u02e5\u02e3\3\2\2\2\u02e5"+
		"\u02e6\3\2\2\2\u02e6C\3\2\2\2\u02e7\u02e5\3\2\2\2\u02e8\u02ec\7,\2\2\u02e9"+
		"\u02eb\5F$\2\u02ea\u02e9\3\2\2\2\u02eb\u02ee\3\2\2\2\u02ec\u02ea\3\2\2"+
		"\2\u02ec\u02ed\3\2\2\2\u02ed\u02ef\3\2\2\2\u02ee\u02ec\3\2\2\2\u02ef\u02f1"+
		"\5J&\2\u02f0\u02f2\5T+\2\u02f1\u02f0\3\2\2\2\u02f1\u02f2\3\2\2\2\u02f2"+
		"\u02f4\3\2\2\2\u02f3\u02f5\5h\65\2\u02f4\u02f3\3\2\2\2\u02f4\u02f5\3\2"+
		"\2\2\u02f5\u02f7\3\2\2\2\u02f6\u02f8\5j\66\2\u02f7\u02f6\3\2\2\2\u02f7"+
		"\u02f8\3\2\2\2\u02f8\u02fa\3\2\2\2\u02f9\u02fb\5l\67\2\u02fa\u02f9\3\2"+
		"\2\2\u02fa\u02fb\3\2\2\2\u02fb\u02fd\3\2\2\2\u02fc\u02fe\5t;\2\u02fd\u02fc"+
		"\3\2\2\2\u02fd\u02fe\3\2\2\2\u02fe\u0300\3\2\2\2\u02ff\u0301\5\u0132\u009a"+
		"\2\u0300\u02ff\3\2\2\2\u0300\u0301\3\2\2\2\u0301\u0303\3\2\2\2\u0302\u0304"+
		"\5n8\2\u0303\u0302\3\2\2\2\u0303\u0304\3\2\2\2\u0304\u0306\3\2\2\2\u0305"+
		"\u0307\5~@\2\u0306\u0305\3\2\2\2\u0306\u0307\3\2\2\2\u0307\u0309\3\2\2"+
		"\2\u0308\u030a\5\u0080A\2\u0309\u0308\3\2\2\2\u0309\u030a\3\2\2\2\u030a"+
		"E\3\2\2\2\u030b\u0314\5H%\2\u030c\u0314\7\u0166\2\2\u030d\u0314\7\u0176"+
		"\2\2\u030e\u0314\7\u016d\2\2\u030f\u0314\7\u0168\2\2\u0310\u0314\7\u0169"+
		"\2\2\u0311\u0314\t\f\2\2\u0312\u0314\7\u016b\2\2\u0313\u030b\3\2\2\2\u0313"+
		"\u030c\3\2\2\2\u0313\u030d\3\2\2\2\u0313\u030e\3\2\2\2\u0313\u030f\3\2"+
		"\2\2\u0313\u0310\3\2\2\2\u0313\u0311\3\2\2\2\u0313\u0312\3\2\2\2\u0314"+
		"G\3\2\2\2\u0315\u0316\t\r\2\2\u0316I\3\2\2\2\u0317\u031a\5P)\2\u0318\u031a"+
		"\5L\'\2\u0319\u0317\3\2\2\2\u0319\u0318\3\2\2\2\u031a\u031f\3\2\2\2\u031b"+
		"\u031c\7$\2\2\u031c\u031e\5L\'\2\u031d\u031b\3\2\2\2\u031e\u0321\3\2\2"+
		"\2\u031f\u031d\3\2\2\2\u031f\u0320\3\2\2\2\u0320K\3\2\2\2\u0321\u031f"+
		"\3\2\2\2\u0322\u0325\5\u00a2R\2\u0323\u0325\5\u00dep\2\u0324\u0322\3\2"+
		"\2\2\u0324\u0323\3\2\2\2\u0325\u032a\3\2\2\2\u0326\u0328\7\\\2\2\u0327"+
		"\u0326\3\2\2\2\u0327\u0328\3\2\2\2\u0328\u0329\3\2\2\2\u0329\u032b\5N"+
		"(\2\u032a\u0327\3\2\2\2\u032a\u032b\3\2\2\2\u032b\u032e\3\2\2\2\u032c"+
		"\u032e\5R*\2\u032d\u0324\3\2\2\2\u032d\u032c\3\2\2\2\u032eM\3\2\2\2\u032f"+
		"\u0332\5\u0098M\2\u0330\u0332\7\u02be\2\2\u0331\u032f\3\2\2\2\u0331\u0330"+
		"\3\2\2\2\u0332O\3\2\2\2\u0333\u0334\7\20\2\2\u0334Q\3\2\2\2\u0335\u0336"+
		"\5\u0098M\2\u0336\u0337\7\24\2\2\u0337S\3\2\2\2\u0338\u0339\7Q\2\2\u0339"+
		"\u033a\5V,\2\u033aU\3\2\2\2\u033b\u0340\5X-\2\u033c\u033d\7$\2\2\u033d"+
		"\u033f\5X-\2\u033e\u033c\3\2\2\2\u033f\u0342\3\2\2\2\u0340\u033e\3\2\2"+
		"\2\u0340\u0341\3\2\2\2\u0341W\3\2\2\2\u0342\u0340\3\2\2\2\u0343\u034a"+
		"\5Z.\2\u0344\u0345\7 \2\2\u0345\u0346\7\u00c9\2\2\u0346\u0347\5Z.\2\u0347"+
		"\u0348\7!\2\2\u0348\u034a\3\2\2\2\u0349\u0343\3\2\2\2\u0349\u0344\3\2"+
		"\2\2\u034aY\3\2\2\2\u034b\u034f\5\\/\2\u034c\u034e\5d\63\2\u034d\u034c"+
		"\3\2\2\2\u034e\u0351\3\2\2\2\u034f\u034d\3\2\2\2\u034f\u0350\3\2\2\2\u0350"+
		"[\3\2\2\2\u0351\u034f\3\2\2\2\u0352\u0354\5\u00a0Q\2\u0353\u0355\5^\60"+
		"\2\u0354\u0353\3\2\2\2\u0354\u0355\3\2\2\2\u0355\u035a\3\2\2\2\u0356\u0358"+
		"\7\\\2\2\u0357\u0356\3\2\2\2\u0357\u0358\3\2\2\2\u0358\u0359\3\2\2\2\u0359"+
		"\u035b\5N(\2\u035a\u0357\3\2\2\2\u035a\u035b\3\2\2\2\u035b\u035d\3\2\2"+
		"\2\u035c\u035e\5`\61\2\u035d\u035c\3\2\2\2\u035d\u035e\3\2\2\2\u035e\u036c"+
		"\3\2\2\2\u035f\u0361\5x=\2\u0360\u0362\7\\\2\2\u0361\u0360\3\2\2\2\u0361"+
		"\u0362\3\2\2\2\u0362\u0363\3\2\2\2\u0363\u0365\5N(\2\u0364\u0366\5\u00b8"+
		"]\2\u0365\u0364\3\2\2\2\u0365\u0366\3\2\2\2\u0366\u036c\3\2\2\2\u0367"+
		"\u0368\7\36\2\2\u0368\u0369\5V,\2\u0369\u036a\7\37\2\2\u036a\u036c\3\2"+
		"\2\2\u036b\u0352\3\2\2\2\u036b\u035f\3\2\2\2\u036b\u0367\3\2\2\2\u036c"+
		"]\3\2\2\2\u036d\u036e\7\u00db\2\2\u036e\u036f\7\36\2\2\u036f\u0374\5\u0098"+
		"M\2\u0370\u0371\7$\2\2\u0371\u0373\5\u0098M\2\u0372\u0370\3\2\2\2\u0373"+
		"\u0376\3\2\2\2\u0374\u0372\3\2\2\2\u0374\u0375\3\2\2\2\u0375\u0377\3\2"+
		"\2\2\u0376\u0374\3\2\2\2\u0377\u0378\7\37\2\2\u0378_\3\2\2\2\u0379\u037e"+
		"\5b\62\2\u037a\u037b\7$\2\2\u037b\u037d\5b\62\2\u037c\u037a\3\2\2\2\u037d"+
		"\u0380\3\2\2\2\u037e\u037c\3\2\2\2\u037e\u037f\3\2\2\2\u037fa\3\2\2\2"+
		"\u0380\u037e\3\2\2\2\u0381\u0382\t\16\2\2\u0382\u038b\t\17\2\2\u0383\u0389"+
		"\7a\2\2\u0384\u038a\7S\2\2\u0385\u0386\7p\2\2\u0386\u038a\7r\2\2\u0387"+
		"\u0388\7q\2\2\u0388\u038a\7r\2\2\u0389\u0384\3\2\2\2\u0389\u0385\3\2\2"+
		"\2\u0389\u0387\3\2\2\2\u038a\u038c\3\2\2\2\u038b\u0383\3\2\2\2\u038b\u038c"+
		"\3\2\2\2\u038c\u038d\3\2\2\2\u038d\u038e\7\36\2\2\u038e\u0393\5\u00a4"+
		"S\2\u038f\u0390\7$\2\2\u0390\u0392\5\u00a4S\2\u0391\u038f\3\2\2\2\u0392"+
		"\u0395\3\2\2\2\u0393\u0391\3\2\2\2\u0393\u0394\3\2\2\2\u0394\u0396\3\2"+
		"\2\2\u0395\u0393\3\2\2\2\u0396\u0397\7\37\2\2\u0397c\3\2\2\2\u0398\u039a"+
		"\t\20\2\2\u0399\u0398\3\2\2\2\u0399\u039a\3\2\2\2\u039a\u039b\3\2\2\2"+
		"\u039b\u039e\7S\2\2\u039c\u039e\7\u0176\2\2\u039d\u0399\3\2\2\2\u039d"+
		"\u039c\3\2\2\2\u039e\u039f\3\2\2\2\u039f\u03a1\5\\/\2\u03a0\u03a2\5f\64"+
		"\2\u03a1\u03a0\3\2\2\2\u03a1\u03a2\3\2\2\2\u03a2\u03b4\3\2\2\2\u03a3\u03a5"+
		"\t\21\2\2\u03a4\u03a6\7V\2\2\u03a5\u03a4\3\2\2\2\u03a5\u03a6\3\2\2\2\u03a6"+
		"\u03a7\3\2\2\2\u03a7\u03a8\7S\2\2\u03a8\u03a9\5\\/\2\u03a9\u03aa\5f\64"+
		"\2\u03aa\u03b4\3\2\2\2\u03ab\u03af\7R\2\2\u03ac\u03b0\7U\2\2\u03ad\u03ae"+
		"\t\21\2\2\u03ae\u03b0\7V\2\2\u03af\u03ac\3\2\2\2\u03af\u03ad\3\2\2\2\u03af"+
		"\u03b0\3\2\2\2\u03b0\u03b1\3\2\2\2\u03b1\u03b2\7S\2\2\u03b2\u03b4\5\\"+
		"/\2\u03b3\u039d\3\2\2\2\u03b3\u03a3\3\2\2\2\u03b3\u03ab\3\2\2\2\u03b4"+
		"e\3\2\2\2\u03b5\u03b6\7]\2\2\u03b6\u03ba\5\u00dep\2\u03b7\u03b8\7Z\2\2"+
		"\u03b8\u03ba\5\u00b8]\2\u03b9\u03b5\3\2\2\2\u03b9\u03b7\3\2\2\2\u03ba"+
		"g\3\2\2\2\u03bb\u03bc\7[\2\2\u03bc\u03bd\5\u00dep\2\u03bdi\3\2\2\2\u03be"+
		"\u03bf\7q\2\2\u03bf\u03c0\7r\2\2\u03c0\u03c5\5\u0134\u009b\2\u03c1\u03c2"+
		"\7$\2\2\u03c2\u03c4\5\u0134\u009b\2\u03c3\u03c1\3\2\2\2\u03c4\u03c7\3"+
		"\2\2\2\u03c5\u03c3\3\2\2\2\u03c5\u03c6\3\2\2\2\u03c6\u03ca\3\2\2\2\u03c7"+
		"\u03c5\3\2\2\2\u03c8\u03c9\7I\2\2\u03c9\u03cb\7\u00ec\2\2\u03ca\u03c8"+
		"\3\2\2\2\u03ca\u03cb\3\2\2\2\u03cbk\3\2\2\2\u03cc\u03cd\7u\2\2\u03cd\u03ce"+
		"\5\u00dep\2\u03cem\3\2\2\2\u03cf\u03da\7v\2\2\u03d0\u03d1\5r:\2\u03d1"+
		"\u03d2\7$\2\2\u03d2\u03d4\3\2\2\2\u03d3\u03d0\3\2\2\2\u03d3\u03d4\3\2"+
		"\2\2\u03d4\u03d5\3\2\2\2\u03d5\u03db\5p9\2\u03d6\u03d7\5p9\2\u03d7\u03d8"+
		"\7w\2\2\u03d8\u03d9\5r:\2\u03d9\u03db\3\2\2\2\u03da\u03d3\3\2\2\2\u03da"+
		"\u03d6\3\2\2\2\u03dbo\3\2\2\2\u03dc\u03df\5\u0088E\2\u03dd\u03df\5\u0082"+
		"B\2\u03de\u03dc\3\2\2\2\u03de\u03dd\3\2\2\2\u03dfq\3\2\2\2\u03e0\u03e3"+
		"\5\u0088E\2\u03e1\u03e3\5\u0082B\2\u03e2\u03e0\3\2\2\2\u03e2\u03e1\3\2"+
		"\2\2\u03e3s\3\2\2\2\u03e4\u03e5\7\u00ca\2\2\u03e5\u03ea\5v<\2\u03e6\u03e7"+
		"\7$\2\2\u03e7\u03e9\5v<\2\u03e8\u03e6\3\2\2\2\u03e9\u03ec\3\2\2\2\u03ea"+
		"\u03e8\3\2\2\2\u03ea\u03eb\3\2\2\2\u03ebu\3\2\2\2\u03ec\u03ea\3\2\2\2"+
		"\u03ed\u03ee\5\u0142\u00a2\2\u03ee\u03ef\7\\\2\2\u03ef\u03f0\7\36\2\2"+
		"\u03f0\u03f1\5\u00f8}\2\u03f1\u03f2\7\37\2\2\u03f2w\3\2\2\2\u03f3\u03f4"+
		"\7\36\2\2\u03f4\u03f5\5B\"\2\u03f5\u03f6\7\37\2\2\u03f6y\3\2\2\2\u03f7"+
		"\u03f8\7\u01d6\2\2\u03f8\u03f9\7r\2\2\u03f9\u03fe\7\u02be\2\2\u03fa\u03fb"+
		"\7\u01d7\2\2\u03fb\u03fc\7r\2\2\u03fc\u03fe\7\u02be\2\2\u03fd\u03f7\3"+
		"\2\2\2\u03fd\u03fa\3\2\2\2\u03fe{\3\2\2\2\u03ff\u0400\7\u01d7\2\2\u0400"+
		"\u0401\7r\2\2\u0401\u040c\7\u02be\2\2\u0402\u0404\7\u01d8\2\2\u0403\u0402"+
		"\3\2\2\2\u0403\u0404\3\2\2\2\u0404\u0405\3\2\2\2\u0405\u0406\7\u01d9\2"+
		"\2\u0406\u0407\7r\2\2\u0407\u040c\7\u02be\2\2\u0408\u0409\7\u01da\2\2"+
		"\u0409\u040a\7r\2\2\u040a\u040c\7\u02be\2\2\u040b\u03ff\3\2\2\2\u040b"+
		"\u0403\3\2\2\2\u040b\u0408\3\2\2\2\u040c}\3\2\2\2\u040d\u040e\7G\2\2\u040e"+
		"\u0413\5\u0098M\2\u040f\u0410\7$\2\2\u0410\u0412\5\u0098M\2\u0411\u040f"+
		"\3\2\2\2\u0412\u0415\3\2\2\2\u0413\u0411\3\2\2\2\u0413\u0414\3\2\2\2\u0414"+
		"\u0432\3\2\2\2\u0415\u0413\3\2\2\2\u0416\u0417\7G\2\2\u0417\u0418\7\u01dd"+
		"\2\2\u0418\u0432\7\u02be\2\2\u0419\u041a\7G\2\2\u041a\u041b\7\u01de\2"+
		"\2\u041b\u041f\7\u02be\2\2\u041c\u041d\7\177\2\2\u041d\u041e\78\2\2\u041e"+
		"\u0420\7\u02bd\2\2\u041f\u041c\3\2\2\2\u041f\u0420\3\2\2\2\u0420\u0427"+
		"\3\2\2\2\u0421\u0423\t\t\2\2\u0422\u0424\5|?\2\u0423\u0422\3\2\2\2\u0424"+
		"\u0425\3\2\2\2\u0425\u0423\3\2\2\2\u0425\u0426\3\2\2\2\u0426\u0428\3\2"+
		"\2\2\u0427\u0421\3\2\2\2\u0427\u0428\3\2\2\2\u0428\u042f\3\2\2\2\u0429"+
		"\u042b\7\u01d5\2\2\u042a\u042c\5z>\2\u042b\u042a\3\2\2\2\u042c\u042d\3"+
		"\2\2\2\u042d\u042b\3\2\2\2\u042d\u042e\3\2\2\2\u042e\u0430\3\2\2\2\u042f"+
		"\u0429\3\2\2\2\u042f\u0430\3\2\2\2\u0430\u0432\3\2\2\2\u0431\u040d\3\2"+
		"\2\2\u0431\u0416\3\2\2\2\u0431\u0419\3\2\2\2\u0432\177\3\2\2\2\u0433\u0434"+
		"\7a\2\2\u0434\u043a\7.\2\2\u0435\u0436\7\u0131\2\2\u0436\u0437\7l\2\2"+
		"\u0437\u0438\7\u01df\2\2\u0438\u043a\7\u015f\2\2\u0439\u0433\3\2\2\2\u0439"+
		"\u0435\3\2\2\2\u043a\u0081\3\2\2\2\u043b\u043c\7(\2\2\u043c\u0083\3\2"+
		"\2\2\u043d\u0445\5\u0086D\2\u043e\u0445\5\u0088E\2\u043f\u0445\5\u008a"+
		"F\2\u0440\u0445\5\u008cG\2\u0441\u0445\5\u008eH\2\u0442\u0445\5\u0090"+
		"I\2\u0443\u0445\5\u0092J\2\u0444\u043d\3\2\2\2\u0444\u043e\3\2\2\2\u0444"+
		"\u043f\3\2\2\2\u0444\u0440\3\2\2\2\u0444\u0441\3\2\2\2\u0444\u0442\3\2"+
		"\2\2\u0444\u0443\3\2\2\2\u0445\u0085\3\2\2\2\u0446\u0448\5\u0094K\2\u0447"+
		"\u0446\3\2\2\2\u0447\u0448\3\2\2\2\u0448\u0449\3\2\2\2\u0449\u044b\7\u02be"+
		"\2\2\u044a\u044c\5\u0140\u00a1\2\u044b\u044a\3\2\2\2\u044b\u044c\3\2\2"+
		"\2\u044c\u0087\3\2\2\2\u044d\u044f\7\17\2\2\u044e\u044d\3\2\2\2\u044e"+
		"\u044f\3\2\2\2\u044f\u0450\3\2\2\2\u0450\u0451\7\u02bf\2\2\u0451\u0089"+
		"\3\2\2\2\u0452\u0453\t\22\2\2\u0453\u045a\7\u02be\2\2\u0454\u0455\7 \2"+
		"\2\u0455\u0456\5\u0098M\2\u0456\u0457\7\u02be\2\2\u0457\u0458\7!\2\2\u0458"+
		"\u045a\3\2\2\2\u0459\u0452\3\2\2\2\u0459\u0454\3\2\2\2\u045a\u008b\3\2"+
		"\2\2\u045b\u045d\5\u0094K\2\u045c\u045b\3\2\2\2\u045c\u045d\3\2\2\2\u045d"+
		"\u045e\3\2\2\2\u045e\u0460\7\u02c0\2\2\u045f\u0461\5\u0140\u00a1\2\u0460"+
		"\u045f\3\2\2\2\u0460\u0461\3\2\2\2\u0461\u008d\3\2\2\2\u0462\u0464\5\u0094"+
		"K\2\u0463\u0462\3\2\2\2\u0463\u0464\3\2\2\2\u0464\u0465\3\2\2\2\u0465"+
		"\u0467\7\u02c1\2\2\u0466\u0468\5\u0140\u00a1\2\u0467\u0466\3\2\2\2\u0467"+
		"\u0468\3\2\2\2\u0468\u008f\3\2\2\2\u0469\u046a\t\23\2\2\u046a\u0091\3"+
		"\2\2\2\u046b\u046c\7g\2\2\u046c\u0093\3\2\2\2\u046d\u046e\7\u02bd\2\2"+
		"\u046e\u0095\3\2\2\2\u046f\u0470\7\u02bd\2\2\u0470\u0097\3\2\2\2\u0471"+
		"\u0474\7\u02bd\2\2\u0472\u0474\5\u009aN\2\u0473\u0471\3\2\2\2\u0473\u0472"+
		"\3\2\2\2\u0474\u0099\3\2\2\2\u0475\u0476\t\24\2\2\u0476\u009b\3\2\2\2"+
		"\u0477\u0479\7)\2\2\u0478\u0477\3\2\2\2\u0478\u0479\3\2\2\2\u0479\u047a"+
		"\3\2\2\2\u047a\u047c\7)\2\2\u047b\u0478\3\2\2\2\u047b\u047c\3\2\2\2\u047c"+
		"\u047e\3\2\2\2\u047d\u047f\t\25\2\2\u047e\u047d\3\2\2\2\u047e\u047f\3"+
		"\2\2\2\u047f\u0481\3\2\2\2\u0480\u0482\7\23\2\2\u0481\u0480\3\2\2\2\u0481"+
		"\u0482\3\2\2\2\u0482\u0483\3\2\2\2\u0483\u0484\5\u0098M\2\u0484\u009d"+
		"\3\2\2\2\u0485\u0486\5\u0098M\2\u0486\u009f\3\2\2\2\u0487\u0488\5\u00b2"+
		"Z\2\u0488\u0489\7\23\2\2\u0489\u048b\3\2\2\2\u048a\u0487\3\2\2\2\u048a"+
		"\u048b\3\2\2\2\u048b\u048c\3\2\2\2\u048c\u048d\5\u00b4[\2\u048d\u00a1"+
		"\3\2\2\2\u048e\u048f\5\u00b2Z\2\u048f\u0490\7\23\2\2\u0490\u0492\3\2\2"+
		"\2\u0491\u048e\3\2\2\2\u0491\u0492\3\2\2\2\u0492\u0493\3\2\2\2\u0493\u0494"+
		"\5\u00b4[\2\u0494\u00a3\3\2\2\2\u0495\u0496\5\u0098M\2\u0496\u00a5\3\2"+
		"\2\2\u0497\u0498\7\u02be\2\2\u0498\u0499\7)\2\2\u0499\u049d\7\u02be\2"+
		"\2\u049a\u049d\5\u0098M\2\u049b\u049d\7\u02be\2\2\u049c\u0497\3\2\2\2"+
		"\u049c\u049a\3\2\2\2\u049c\u049b\3\2\2\2\u049d\u00a7\3\2\2\2\u049e\u049f"+
		"\t\26\2\2\u049f\u04a0\7)\2\2\u04a0\u04a1\7\u02be\2\2\u04a1\u04a5\7\u02bd"+
		"\2\2\u04a2\u04a5\5\u0098M\2\u04a3\u04a5\7\u02be\2\2\u04a4\u049e\3\2\2"+
		"\2\u04a4\u04a2\3\2\2\2\u04a4\u04a3\3\2\2\2\u04a5\u00a9\3\2\2\2\u04a6\u04a9"+
		"\5\u0098M\2\u04a7\u04a9\7\u02be\2\2\u04a8\u04a6\3\2\2\2\u04a8\u04a7\3"+
		"\2\2\2\u04a9\u00ab\3\2\2\2\u04aa\u04ad\5\u0098M\2\u04ab\u04ad\7\u02be"+
		"\2\2\u04ac\u04aa\3\2\2\2\u04ac\u04ab\3\2\2\2\u04ad\u00ad\3\2\2\2\u04ae"+
		"\u04b6\5\u0098M\2\u04af\u04b0\5\u00b2Z\2\u04b0\u04b1\7\23\2\2\u04b1\u04b3"+
		"\3\2\2\2\u04b2\u04af\3\2\2\2\u04b2\u04b3\3\2\2\2\u04b3\u04b4\3\2\2\2\u04b4"+
		"\u04b6\5\u0098M\2\u04b5\u04ae\3\2\2\2\u04b5\u04b2\3\2\2\2\u04b6\u00af"+
		"\3\2\2\2\u04b7\u04bf\5\u0098M\2\u04b8\u04b9\5\u00b2Z\2\u04b9\u04ba\7\23"+
		"\2\2\u04ba\u04bc\3\2\2\2\u04bb\u04b8\3\2\2\2\u04bb\u04bc\3\2\2\2\u04bc"+
		"\u04bd\3\2\2\2\u04bd\u04bf\5\u0098M\2\u04be\u04b7\3\2\2\2\u04be\u04bb"+
		"\3\2\2\2\u04bf\u00b1\3\2\2\2\u04c0\u04c1\5\u0098M\2\u04c1\u00b3\3\2\2"+
		"\2\u04c2\u04c3\5\u0098M\2\u04c3\u00b5\3\2\2\2\u04c4\u04c6\7\36\2\2\u04c5"+
		"\u04c4\3\2\2\2\u04c5\u04c6\3\2\2\2\u04c6\u04c7\3\2\2\2\u04c7\u04cc\5\u00a0"+
		"Q\2\u04c8\u04c9\7$\2\2\u04c9\u04cb\5\u00a0Q\2\u04ca\u04c8\3\2\2\2\u04cb"+
		"\u04ce\3\2\2\2\u04cc\u04ca\3\2\2\2\u04cc\u04cd\3\2\2\2\u04cd\u04d0\3\2"+
		"\2\2\u04ce\u04cc\3\2\2\2\u04cf\u04d1\7\37\2\2\u04d0\u04cf\3\2\2\2\u04d0"+
		"\u04d1\3\2\2\2\u04d1\u00b7\3\2\2\2\u04d2\u04d4\7\36\2\2\u04d3\u04d2\3"+
		"\2\2\2\u04d3\u04d4\3\2\2\2\u04d4\u04d5\3\2\2\2\u04d5\u04da\5\u00a2R\2"+
		"\u04d6\u04d7\7$\2\2\u04d7\u04d9\5\u00a2R\2\u04d8\u04d6\3\2\2\2\u04d9\u04dc"+
		"\3\2\2\2\u04da\u04d8\3\2\2\2\u04da\u04db\3\2\2\2\u04db\u04de\3\2\2\2\u04dc"+
		"\u04da\3\2\2\2\u04dd\u04df\7\37\2\2\u04de\u04dd\3\2\2\2\u04de\u04df\3"+
		"\2\2\2\u04df\u00b9\3\2\2\2\u04e0\u04e1\7\u02bd\2\2\u04e1\u00bb\3\2\2\2"+
		"\u04e2\u04e3\7\u02be\2\2\u04e3\u00bd\3\2\2\2\u04e4\u04e5\7\u02be\2\2\u04e5"+
		"\u00bf\3\2\2\2\u04e6\u04e7\7\u02bd\2\2\u04e7\u00c1\3\2\2\2\u04e8\u04e9"+
		"\7\u02be\2\2\u04e9\u00c3\3\2\2\2\u04ea\u04eb\7\u02bf\2\2\u04eb\u00c5\3"+
		"\2\2\2\u04ec\u04ed\5\u00a6T\2\u04ed\u04ee\7)\2\2\u04ee\u04ef\5\u00c2b"+
		"\2\u04ef\u04f0\7\r\2\2\u04f0\u04f1\5\u00c4c\2\u04f1\u00c7\3\2\2\2\u04f2"+
		"\u04f3\7\u02bd\2\2\u04f3\u00c9\3\2\2\2\u04f4\u04f5\7\u02bd\2\2\u04f5\u00cb"+
		"\3\2\2\2\u04f6\u04f7\5\u0098M\2\u04f7\u00cd\3\2\2\2\u04f8\u04f9\t\26\2"+
		"\2\u04f9\u04fa\7)\2\2\u04fa\u04fb\7\u02be\2\2\u04fb\u04fe\7\u02bd\2\2"+
		"\u04fc\u04fe\7\u02bd\2\2\u04fd\u04f8\3\2\2\2\u04fd\u04fc\3\2\2\2\u04fe"+
		"\u00cf\3\2\2\2\u04ff\u0500\7\u02bd\2\2\u0500\u00d1\3\2\2\2\u0501\u0502"+
		"\7\u02bd\2\2\u0502\u00d3\3\2\2\2\u0503\u0504\t\27\2\2\u0504\u00d5\3\2"+
		"\2\2\u0505\u0508\5\u00a6T\2\u0506\u0508\5\u00ceh\2\u0507\u0505\3\2\2\2"+
		"\u0507\u0506\3\2\2\2\u0508\u00d7\3\2\2\2\u0509\u050a\7\u02bd\2\2\u050a"+
		"\u00d9\3\2\2\2\u050b\u050c\t\30\2\2\u050c\u00db\3\2\2\2\u050d\u050e\t"+
		"\31\2\2\u050e\u050f\5\u00d2j\2\u050f\u00dd\3\2\2\2\u0510\u0511\bp\1\2"+
		"\u0511\u0512\5\u00e2r\2\u0512\u0513\5\u00dep\5\u0513\u051a\3\2\2\2\u0514"+
		"\u0515\7\36\2\2\u0515\u0516\5\u00dep\2\u0516\u0517\7\37\2\2\u0517\u051a"+
		"\3\2\2\2\u0518\u051a\5\u00e4s\2\u0519\u0510\3\2\2\2\u0519\u0514\3\2\2"+
		"\2\u0519\u0518\3\2\2\2\u051a\u0524\3\2\2\2\u051b\u051c\f\7\2\2\u051c\u051d"+
		"\5\u00e0q\2\u051d\u051e\5\u00dep\b\u051e\u0523\3\2\2\2\u051f\u0520\f\6"+
		"\2\2\u0520\u0521\7\u00cd\2\2\u0521\u0523\5\u00dep\7\u0522\u051b\3\2\2"+
		"\2\u0522\u051f\3\2\2\2\u0523\u0526\3\2\2\2\u0524\u0522\3\2\2\2\u0524\u0525"+
		"\3\2\2\2\u0525\u00df\3\2\2\2\u0526\u0524\3\2\2\2\u0527\u0528\t\32\2\2"+
		"\u0528\u00e1\3\2\2\2\u0529\u052a\t\33\2\2\u052a\u00e3\3\2\2\2\u052b\u052c"+
		"\bs\1\2\u052c\u052d\5\u00e8u\2\u052d\u0542\3\2\2\2\u052e\u052f\f\7\2\2"+
		"\u052f\u0531\7e\2\2\u0530\u0532\7f\2\2\u0531\u0530\3\2\2\2\u0531\u0532"+
		"\3\2\2\2\u0532\u0533\3\2\2\2\u0533\u0541\t\34\2\2\u0534\u0535\f\6\2\2"+
		"\u0535\u0536\7\25\2\2\u0536\u0541\5\u00e8u\2\u0537\u0538\f\5\2\2\u0538"+
		"\u0539\5\u00e6t\2\u0539\u053a\5\u00e8u\2\u053a\u0541\3\2\2\2\u053b\u053c"+
		"\f\4\2\2\u053c\u053d\5\u00e6t\2\u053d\u053e\t\35\2\2\u053e\u053f\5x=\2"+
		"\u053f\u0541\3\2\2\2\u0540\u052e\3\2\2\2\u0540\u0534\3\2\2\2\u0540\u0537"+
		"\3\2\2\2\u0540\u053b\3\2\2\2\u0541\u0544\3\2\2\2\u0542\u0540\3\2\2\2\u0542"+
		"\u0543\3\2\2\2\u0543\u00e5\3\2\2\2\u0544\u0542\3\2\2\2\u0545\u0546\t\36"+
		"\2\2\u0546\u00e7\3\2\2\2\u0547\u0549\5\u00eav\2\u0548\u054a\7f\2\2\u0549"+
		"\u0548\3\2\2\2\u0549\u054a\3\2\2\2\u054a\u054b\3\2\2\2\u054b\u054c\7l"+
		"\2\2\u054c\u054d\5x=\2\u054d\u057f\3\2\2\2\u054e\u0550\5\u00eav\2\u054f"+
		"\u0551\7f\2\2\u0550\u054f\3\2\2\2\u0550\u0551\3\2\2\2\u0551\u0552\3\2"+
		"\2\2\u0552\u0553\7l\2\2\u0553\u0554\7\36\2\2\u0554\u0559\5\u00dep\2\u0555"+
		"\u0556\7$\2\2\u0556\u0558\5\u00dep\2\u0557\u0555\3\2\2\2\u0558\u055b\3"+
		"\2\2\2\u0559\u0557\3\2\2\2\u0559\u055a\3\2\2\2\u055a\u055c\3\2\2\2\u055b"+
		"\u0559\3\2\2\2\u055c\u055d\7\37\2\2\u055d\u057f\3\2\2\2\u055e\u0560\5"+
		"\u00eav\2\u055f\u0561\7f\2\2\u0560\u055f\3\2\2\2\u0560\u0561\3\2\2\2\u0561"+
		"\u0562\3\2\2\2\u0562\u0563\7k\2\2\u0563\u0564\5\u00eav\2\u0564\u0565\7"+
		"c\2\2\u0565\u0566\5\u00e8u\2\u0566\u057f\3\2\2\2\u0567\u0568\5\u00eav"+
		"\2\u0568\u0569\7\u00ed\2\2\u0569\u056a\7o\2\2\u056a\u056b\5\u00eav\2\u056b"+
		"\u057f\3\2\2\2\u056c\u056e\5\u00eav\2\u056d\u056f\7f\2\2\u056e\u056d\3"+
		"\2\2\2\u056e\u056f\3\2\2\2\u056f\u0570\3\2\2\2\u0570\u0571\7o\2\2\u0571"+
		"\u0574\5\u00ecw\2\u0572\u0573\7\u00d9\2\2\u0573\u0575\5\u00ecw\2\u0574"+
		"\u0572\3\2\2\2\u0574\u0575\3\2\2\2\u0575\u057f\3\2\2\2\u0576\u0578\5\u00ea"+
		"v\2\u0577\u0579\7f\2\2\u0578\u0577\3\2\2\2\u0578\u0579\3\2\2\2\u0579\u057a"+
		"\3\2\2\2\u057a\u057b\t\37\2\2\u057b\u057c\5\u00eav\2\u057c\u057f\3\2\2"+
		"\2\u057d\u057f\5\u00eav\2\u057e\u0547\3\2\2\2\u057e\u054e\3\2\2\2\u057e"+
		"\u055e\3\2\2\2\u057e\u0567\3\2\2\2\u057e\u056c\3\2\2\2\u057e\u0576\3\2"+
		"\2\2\u057e\u057d\3\2\2\2\u057f\u00e9\3\2\2\2\u0580\u0581\bv\1\2\u0581"+
		"\u0582\5\u00ecw\2\u0582\u05af\3\2\2\2\u0583\u0584\f\21\2\2\u0584\u0585"+
		"\7\7\2\2\u0585\u05ae\5\u00eav\22\u0586\u0587\f\20\2\2\u0587\u0588\7\b"+
		"\2\2\u0588\u05ae\5\u00eav\21\u0589\u058a\f\17\2\2\u058a\u058b\7\t\2\2"+
		"\u058b\u05ae\5\u00eav\20\u058c\u058d\f\16\2\2\u058d\u058e\7\n\2\2\u058e"+
		"\u05ae\5\u00eav\17\u058f\u0590\f\r\2\2\u0590\u0591\7\16\2\2\u0591\u05ae"+
		"\5\u00eav\16\u0592\u0593\f\f\2\2\u0593\u0594\7\17\2\2\u0594\u05ae\5\u00ea"+
		"v\r\u0595\u0596\f\13\2\2\u0596\u0597\7\20\2\2\u0597\u05ae\5\u00eav\f\u0598"+
		"\u0599\f\n\2\2\u0599\u059a\7\21\2\2\u059a\u05ae\5\u00eav\13\u059b\u059c"+
		"\f\t\2\2\u059c\u059d\7\u00cc\2\2\u059d\u05ae\5\u00eav\n\u059e\u059f\f"+
		"\b\2\2\u059f\u05a0\7\u00cb\2\2\u05a0\u05ae\5\u00eav\t\u05a1\u05a2\f\7"+
		"\2\2\u05a2\u05a3\7\f\2\2\u05a3\u05ae\5\u00eav\b\u05a4\u05a5\f\6\2\2\u05a5"+
		"\u05a6\7\13\2\2\u05a6\u05ae\5\u00eav\7\u05a7\u05a8\f\5\2\2\u05a8\u05a9"+
		"\7\16\2\2\u05a9\u05ae\5\u012e\u0098\2\u05aa\u05ab\f\4\2\2\u05ab\u05ac"+
		"\7\17\2\2\u05ac\u05ae\5\u012e\u0098\2\u05ad\u0583\3\2\2\2\u05ad\u0586"+
		"\3\2\2\2\u05ad\u0589\3\2\2\2\u05ad\u058c\3\2\2\2\u05ad\u058f\3\2\2\2\u05ad"+
		"\u0592\3\2\2\2\u05ad\u0595\3\2\2\2\u05ad\u0598\3\2\2\2\u05ad\u059b\3\2"+
		"\2\2\u05ad\u059e\3\2\2\2\u05ad\u05a1\3\2\2\2\u05ad\u05a4\3\2\2\2\u05ad"+
		"\u05a7\3\2\2\2\u05ad\u05aa\3\2\2\2\u05ae\u05b1\3\2\2\2\u05af\u05ad\3\2"+
		"\2\2\u05af\u05b0\3\2\2\2\u05b0\u00eb\3\2\2\2\u05b1\u05af\3\2\2\2\u05b2"+
		"\u05b3\bw\1\2\u05b3\u05d5\5\u00eex\2\u05b4\u05d5\5\u0082B\2\u05b5\u05d5"+
		"\5\u0084C\2\u05b6\u05d5\5\u00a2R\2\u05b7\u05d5\5\u009cO\2\u05b8\u05b9"+
		"\t \2\2\u05b9\u05d5\5\u00ecw\t\u05ba\u05bc\7\u00d5\2\2\u05bb\u05ba\3\2"+
		"\2\2\u05bb\u05bc\3\2\2\2\u05bc\u05bd\3\2\2\2\u05bd\u05be\7\36\2\2\u05be"+
		"\u05c3\5\u00dep\2\u05bf\u05c0\7$\2\2\u05c0\u05c2\5\u00dep\2\u05c1\u05bf"+
		"\3\2\2\2\u05c2\u05c5\3\2\2\2\u05c3\u05c1\3\2\2\2\u05c3\u05c4\3\2\2\2\u05c4"+
		"\u05c6\3\2\2\2\u05c5\u05c3\3\2\2\2\u05c6\u05c7\7\37\2\2\u05c7\u05d5\3"+
		"\2\2\2\u05c8\u05ca\7j\2\2\u05c9\u05c8\3\2\2\2\u05c9\u05ca\3\2\2\2\u05ca"+
		"\u05cb\3\2\2\2\u05cb\u05d5\5x=\2\u05cc\u05cd\7 \2\2\u05cd\u05ce\5\u0098"+
		"M\2\u05ce\u05cf\5\u00dep\2\u05cf\u05d0\7!\2\2\u05d0\u05d5\3\2\2\2\u05d1"+
		"\u05d5\5\u0124\u0093\2\u05d2\u05d5\5\u0128\u0095\2\u05d3\u05d5\5\u012e"+
		"\u0098\2\u05d4\u05b2\3\2\2\2\u05d4\u05b4\3\2\2\2\u05d4\u05b5\3\2\2\2\u05d4"+
		"\u05b6\3\2\2\2\u05d4\u05b7\3\2\2\2\u05d4\u05b8\3\2\2\2\u05d4\u05bb\3\2"+
		"\2\2\u05d4\u05c9\3\2\2\2\u05d4\u05cc\3\2\2\2\u05d4\u05d1\3\2\2\2\u05d4"+
		"\u05d2\3\2\2\2\u05d4\u05d3\3\2\2\2\u05d5\u05e1\3\2\2\2\u05d6\u05d7\f\n"+
		"\2\2\u05d7\u05d8\7\4\2\2\u05d8\u05e0\5\u00ecw\13\u05d9\u05da\f\f\2\2\u05da"+
		"\u05dd\7\u010a\2\2\u05db\u05de\7\u02be\2\2\u05dc\u05de\5\u0098M\2\u05dd"+
		"\u05db\3\2\2\2\u05dd\u05dc\3\2\2\2\u05de\u05e0\3\2\2\2\u05df\u05d6\3\2"+
		"\2\2\u05df\u05d9\3\2\2\2\u05e0\u05e3\3\2\2\2\u05e1\u05df\3\2\2\2\u05e1"+
		"\u05e2\3\2\2\2\u05e2\u00ed\3\2\2\2\u05e3\u05e1\3\2\2\2\u05e4\u05e8\5\u00f0"+
		"y\2\u05e5\u05e8\5\u0104\u0083\2\u05e6\u05e8\5\u0120\u0091\2\u05e7\u05e4"+
		"\3\2\2\2\u05e7\u05e5\3\2\2\2\u05e7\u05e6\3\2\2\2\u05e8\u00ef\3\2\2\2\u05e9"+
		"\u05ea\5\u00f2z\2\u05ea\u05ec\7\36\2\2\u05eb\u05ed\5\u00f4{\2\u05ec\u05eb"+
		"\3\2\2\2\u05ec\u05ed\3\2\2\2\u05ed\u05f7\3\2\2\2\u05ee\u05f3\5\u00dep"+
		"\2\u05ef\u05f0\7$\2\2\u05f0\u05f2\5\u00dep\2\u05f1\u05ef\3\2\2\2\u05f2"+
		"\u05f5\3\2\2\2\u05f3\u05f1\3\2\2\2\u05f3\u05f4\3\2\2\2\u05f4\u05f8\3\2"+
		"\2\2\u05f5\u05f3\3\2\2\2\u05f6\u05f8\7\20\2\2\u05f7\u05ee\3\2\2\2\u05f7"+
		"\u05f6\3\2\2\2\u05f7\u05f8\3\2\2\2\u05f8\u05f9\3\2\2\2\u05f9\u05fb\7\37"+
		"\2\2\u05fa\u05fc\5\u00f6|\2\u05fb\u05fa\3\2\2\2\u05fb\u05fc\3\2\2\2\u05fc"+
		"\u00f1\3\2\2\2\u05fd\u05fe\t!\2\2\u05fe\u00f3\3\2\2\2\u05ff\u0600\7K\2"+
		"\2\u0600\u00f5\3\2\2\2\u0601\u0607\7\u0139\2\2\u0602\u0603\7\36\2\2\u0603"+
		"\u0604\5\u00f8}\2\u0604\u0605\7\37\2\2\u0605\u0608\3\2\2\2\u0606\u0608"+
		"\5\u0098M\2\u0607\u0602\3\2\2\2\u0607\u0606\3\2\2\2\u0608\u00f7\3\2\2"+
		"\2\u0609\u060b\5\u0098M\2\u060a\u0609\3\2\2\2\u060a\u060b\3\2\2\2\u060b"+
		"\u060d\3\2\2\2\u060c\u060e\5\u00fa~\2\u060d\u060c\3\2\2\2\u060d\u060e"+
		"\3\2\2\2\u060e\u0610\3\2\2\2\u060f\u0611\5\u0132\u009a\2\u0610\u060f\3"+
		"\2\2\2\u0610\u0611\3\2\2\2\u0611\u0613\3\2\2\2\u0612\u0614\5\u00fc\177"+
		"\2\u0613\u0612\3\2\2\2\u0613\u0614\3\2\2\2\u0614\u00f9\3\2\2\2\u0615\u0616"+
		"\7\u00db\2\2\u0616\u0617\7r\2\2\u0617\u061c\5\u00dep\2\u0618\u0619\7$"+
		"\2\2\u0619\u061b\5\u00dep\2\u061a\u0618\3\2\2\2\u061b\u061e\3\2\2\2\u061c"+
		"\u061a\3\2\2\2\u061c\u061d\3\2\2\2\u061d\u00fb\3\2\2\2\u061e\u061c\3\2"+
		"\2\2\u061f\u0622\t\"\2\2\u0620\u0623\5\u00fe\u0080\2\u0621\u0623\5\u0102"+
		"\u0082\2\u0622\u0620\3\2\2\2\u0622\u0621\3\2\2\2\u0623\u00fd\3\2\2\2\u0624"+
		"\u0625\7\u0096\2\2\u0625\u0631\7\u00d5\2\2\u0626\u0627\7\u00e4\2\2\u0627"+
		"\u0631\7\u013e\2\2\u0628\u0629\7\u00e4\2\2\u0629\u0631\7\u0124\2\2\u062a"+
		"\u062b\5\u00dep\2\u062b\u062c\7\u013e\2\2\u062c\u0631\3\2\2\2\u062d\u062e"+
		"\5\u00dep\2\u062e\u062f\7\u0124\2\2\u062f\u0631\3\2\2\2\u0630\u0624\3"+
		"\2\2\2\u0630\u0626\3\2\2\2\u0630\u0628\3\2\2\2\u0630\u062a\3\2\2\2\u0630"+
		"\u062d\3\2\2\2\u0631\u00ff\3\2\2\2\u0632\u0633\5\u00fe\u0080\2\u0633\u0101"+
		"\3\2\2\2\u0634\u0635\7k\2\2\u0635\u0636\5\u00fe\u0080\2\u0636\u0637\7"+
		"c\2\2\u0637\u0638\5\u0100\u0081\2\u0638\u0103\3\2\2\2\u0639\u0645\5\u0106"+
		"\u0084\2\u063a\u0645\5\u0108\u0085\2\u063b\u0645\5\u010a\u0086\2\u063c"+
		"\u0645\5\u010c\u0087\2\u063d\u0645\5\u010e\u0088\2\u063e\u0645\5\u0110"+
		"\u0089\2\u063f\u0645\5\u0112\u008a\2\u0640\u0645\5\u0114\u008b\2\u0641"+
		"\u0645\5\u0116\u008c\2\u0642\u0645\5\u011a\u008e\2\u0643\u0645\5\u0118"+
		"\u008d\2\u0644\u0639\3\2\2\2\u0644\u063a\3\2\2\2\u0644\u063b\3\2\2\2\u0644"+
		"\u063c\3\2\2\2\u0644\u063d\3\2\2\2\u0644\u063e\3\2\2\2\u0644\u063f\3\2"+
		"\2\2\u0644\u0640\3\2\2\2\u0644\u0641\3\2\2\2\u0644\u0642\3\2\2\2\u0644"+
		"\u0643\3\2\2\2\u0645\u0105\3\2\2\2\u0646\u0647\7\u0183\2\2\u0647\u0649"+
		"\7\36\2\2\u0648\u064a\5\u00f4{\2\u0649\u0648\3\2\2\2\u0649\u064a\3\2\2"+
		"\2\u064a\u0654\3\2\2\2\u064b\u0650\5\u00dep\2\u064c\u064d\7$\2\2\u064d"+
		"\u064f\5\u00dep\2\u064e\u064c\3\2\2\2\u064f\u0652\3\2\2\2\u0650\u064e"+
		"\3\2\2\2\u0650\u0651\3\2\2\2\u0651\u0655\3\2\2\2\u0652\u0650\3\2\2\2\u0653"+
		"\u0655\7\20\2\2\u0654\u064b\3\2\2\2\u0654\u0653\3\2\2\2\u0654\u0655\3"+
		"\2\2\2\u0655\u0657\3\2\2\2\u0656\u0658\5\u0132\u009a\2\u0657\u0656\3\2"+
		"\2\2\u0657\u0658\3\2\2\2\u0658\u065b\3\2\2\2\u0659\u065a\7\u0151\2\2\u065a"+
		"\u065c\5\u00dep\2\u065b\u0659\3\2\2\2\u065b\u065c\3\2\2\2\u065c\u065d"+
		"\3\2\2\2\u065d\u065e\7\37\2\2\u065e\u0107\3\2\2\2\u065f\u0660\5\u0098"+
		"M\2\u0660\u0661\7\36\2\2\u0661\u0666\5\u00dep\2\u0662\u0663\7$\2\2\u0663"+
		"\u0665\5\u00dep\2\u0664\u0662\3\2\2\2\u0665\u0668\3\2\2\2\u0666\u0664"+
		"\3\2\2\2\u0666\u0667\3\2\2\2\u0667\u0669\3\2\2\2\u0668\u0666\3\2\2\2\u0669"+
		"\u066a\7\37\2\2\u066a\u066b\5\u00f6|\2\u066b\u0109\3\2\2\2\u066c\u066d"+
		"\7N\2\2\u066d\u066e\7\36\2\2\u066e\u066f\5\u00dep\2\u066f\u0670\7\\\2"+
		"\2\u0670\u0671\5\u0136\u009c\2\u0671\u0672\7\37\2\2\u0672\u010b\3\2\2"+
		"\2\u0673\u0674\7\u0111\2\2\u0674\u0675\7\36\2\2\u0675\u0676\5\u00dep\2"+
		"\u0676\u0677\7$\2\2\u0677\u0678\5\u0136\u009c\2\u0678\u0679\7\37\2\2\u0679"+
		"\u0682\3\2\2\2\u067a\u067b\7\u0111\2\2\u067b\u067c\7\36\2\2\u067c\u067d"+
		"\5\u00dep\2\u067d\u067e\7Z\2\2\u067e\u067f\5\u0098M\2\u067f\u0680\7\37"+
		"\2\2\u0680\u0682\3\2\2\2\u0681\u0673\3\2\2\2\u0681\u067a\3\2\2\2\u0682"+
		"\u010d\3\2\2\2\u0683\u0684\7A\2\2\u0684\u0685\7\36\2\2\u0685\u0686\5\u00de"+
		"p\2\u0686\u0687\7l\2\2\u0687\u0688\5\u00dep\2\u0688\u0689\7\37\2\2\u0689"+
		"\u010f\3\2\2\2\u068a\u068b\t#\2\2\u068b\u068c\7\36\2\2\u068c\u068d\5\u00de"+
		"p\2\u068d\u068e\7Q\2\2\u068e\u0691\7\u02bf\2\2\u068f\u0690\7a\2\2\u0690"+
		"\u0692\7\u02bf\2\2\u0691\u068f\3\2\2\2\u0691\u0692\3\2\2\2\u0692\u0693"+
		"\3\2\2\2\u0693\u0694\7\37\2\2\u0694\u0111\3\2\2\2\u0695\u0696\7\u0121"+
		"\2\2\u0696\u0697\7\36\2\2\u0697\u0698\5\u0098M\2\u0698\u0699\7Q\2\2\u0699"+
		"\u069a\5\u00dep\2\u069a\u069b\7\37\2\2\u069b\u0113\3\2\2\2\u069c\u069d"+
		"\7~\2\2\u069d\u069e\7\36\2\2\u069e\u06a3\5\u00dep\2\u069f\u06a0\7$\2\2"+
		"\u06a0\u06a2\5\u00dep\2\u06a1\u069f\3\2\2\2\u06a2\u06a5\3\2\2\2\u06a3"+
		"\u06a1\3\2\2\2\u06a3\u06a4\3\2\2\2\u06a4\u06a8\3\2\2\2\u06a5\u06a3\3\2"+
		"\2\2\u06a6\u06a7\7Z\2\2\u06a7\u06a9\5\u0142\u00a2\2\u06a8\u06a6\3\2\2"+
		"\2\u06a8\u06a9\3\2\2\2\u06a9\u06aa\3\2\2\2\u06aa\u06ab\7\37\2\2\u06ab"+
		"\u0115\3\2\2\2\u06ac\u06ad\7O\2\2\u06ad\u06ae\7\36\2\2\u06ae\u06af\t$"+
		"\2\2\u06af\u06b0\7\u02be\2\2\u06b0\u06b1\7Q\2\2\u06b1\u06b2\7\u02be\2"+
		"\2\u06b2\u06b3\7\37\2\2\u06b3\u0117\3\2\2\2\u06b4\u06b5\7H\2\2\u06b5\u06b6"+
		"\7\36\2\2\u06b6\u06b7\5\u00a2R\2\u06b7\u06b8\7\37\2\2\u06b8\u0119\3\2"+
		"\2\2\u06b9\u06ba\7\u0177\2\2\u06ba\u06bb\7\36\2\2\u06bb\u06be\5\u00de"+
		"p\2\u06bc\u06bd\7\\\2\2\u06bd\u06bf\5\u0136\u009c\2\u06be\u06bc\3\2\2"+
		"\2\u06be\u06bf\3\2\2\2\u06bf\u06c1\3\2\2\2\u06c0\u06c2\5\u011c\u008f\2"+
		"\u06c1\u06c0\3\2\2\2\u06c1\u06c2\3\2\2\2\u06c2\u06c3\3\2\2\2\u06c3\u06c4"+
		"\7\37\2\2\u06c4\u011b\3\2\2\2\u06c5\u06d1\7\u00f4\2\2\u06c6\u06cb\5\u011e"+
		"\u0090\2\u06c7\u06c8\7$\2\2\u06c8\u06ca\5\u011e\u0090\2\u06c9\u06c7\3"+
		"\2\2\2\u06ca\u06cd\3\2\2\2\u06cb\u06c9\3\2\2\2\u06cb\u06cc\3\2\2\2\u06cc"+
		"\u06d2\3\2\2\2\u06cd\u06cb\3\2\2\2\u06ce\u06cf\7\u02bf\2\2\u06cf\u06d0"+
		"\7\17\2\2\u06d0\u06d2\7\u02bf\2\2\u06d1\u06c6\3\2\2\2\u06d1\u06ce\3\2"+
		"\2\2\u06d2\u011d\3\2\2\2\u06d3\u06d5\7\u02bf\2\2\u06d4\u06d6\t%\2\2\u06d5"+
		"\u06d4\3\2\2\2\u06d5\u06d6\3\2\2\2\u06d6\u06d8\3\2\2\2\u06d7\u06d9\7\u014f"+
		"\2\2\u06d8\u06d7\3\2\2\2\u06d8\u06d9\3\2\2\2\u06d9\u011f\3\2\2\2\u06da"+
		"\u06db\5\u0122\u0092\2\u06db\u06e5\7\36\2\2\u06dc\u06e1\5\u00dep\2\u06dd"+
		"\u06de\7$\2\2\u06de\u06e0\5\u00dep\2\u06df\u06dd\3\2\2\2\u06e0\u06e3\3"+
		"\2\2\2\u06e1\u06df\3\2\2\2\u06e1\u06e2\3\2\2\2\u06e2\u06e6\3\2\2\2\u06e3"+
		"\u06e1\3\2\2\2\u06e4\u06e6\7\20\2\2\u06e5\u06dc\3\2\2\2\u06e5\u06e4\3"+
		"\2\2\2\u06e5\u06e6\3\2\2\2\u06e6\u06e7\3\2\2\2\u06e7\u06e8\7\37\2\2\u06e8"+
		"\u0121\3\2\2\2\u06e9\u0774\5\u0098M\2\u06ea\u0774\7^\2\2\u06eb\u0774\7"+
		"\u0194\2\2\u06ec\u0774\7\u0229\2\2\u06ed\u0774\7\u0085\2\2\u06ee\u0774"+
		"\7\u0086\2\2\u06ef\u0774\7\u0135\2\2\u06f0\u0774\7\u00c1\2\2\u06f1\u0774"+
		"\7\u0081\2\2\u06f2\u0774\7P\2\2\u06f3\u0774\7\u00cb\2\2\u06f4\u0774\7"+
		"\u00b9\2\2\u06f5\u0774\7W\2\2\u06f6\u0774\7X\2\2\u06f7\u0774\7\u022a\2"+
		"\2\u06f8\u0774\7\u022b\2\2\u06f9\u0774\7\u0082\2\2\u06fa\u0774\7\u0230"+
		"\2\2\u06fb\u0774\7\u0231\2\2\u06fc\u0774\7\u008b\2\2\u06fd\u0774\7\u0232"+
		"\2\2\u06fe\u0774\7\u0233\2\2\u06ff\u0774\7\u0234\2\2\u0700\u0774\7\u0235"+
		"\2\2\u0701\u0774\7\u0240\2\2\u0702\u0774\7\u0241\2\2\u0703\u0774\7\u0242"+
		"\2\2\u0704\u0774\7\u0243\2\2\u0705\u0774\7\u0244\2\2\u0706\u0774\7\u0245"+
		"\2\2\u0707\u0774\7\u0246\2\2\u0708\u0774\7\u0247\2\2\u0709\u0774\7\u0236"+
		"\2\2\u070a\u0774\7\u0248\2\2\u070b\u0774\7\u0249\2\2\u070c\u0774\7\u024a"+
		"\2\2\u070d\u0774\7\u024b\2\2\u070e\u0774\7\u024c\2\2\u070f\u0774\7\u024d"+
		"\2\2\u0710\u0774\7\u024e\2\2\u0711\u0774\7\u024f\2\2\u0712\u0774\7\u0250"+
		"\2\2\u0713\u0774\7\u0251\2\2\u0714\u0774\7\u0252\2\2\u0715\u0774\7\u0253"+
		"\2\2\u0716\u0774\7\u0254\2\2\u0717\u0774\7\u0255\2\2\u0718\u0774\7\u0256"+
		"\2\2\u0719\u0774\7\u0257\2\2\u071a\u0774\7\u0258\2\2\u071b\u0774\7\u0259"+
		"\2\2\u071c\u0774\7\u025a\2\2\u071d\u0774\7\u025b\2\2\u071e\u0774\7\u025c"+
		"\2\2\u071f\u0774\7\u025d\2\2\u0720\u0774\7\u025e\2\2\u0721\u0774\7\u025f"+
		"\2\2\u0722\u0774\7\u0260\2\2\u0723\u0774\7\u0261\2\2\u0724\u0774\7\u0262"+
		"\2\2\u0725\u0774\7\u0263\2\2\u0726\u0774\7\u0264\2\2\u0727\u0774\7\u0265"+
		"\2\2\u0728\u0774\7\u0266\2\2\u0729\u0774\7\u0267\2\2\u072a\u0774\7\u0268"+
		"\2\2\u072b\u0774\7\u0269\2\2\u072c\u0774\7\u026a\2\2\u072d\u0774\7\u026b"+
		"\2\2\u072e\u0774\7\u026c\2\2\u072f\u0774\7\u026d\2\2\u0730\u0774\7\u026e"+
		"\2\2\u0731\u0774\7\u026f\2\2\u0732\u0774\7\u0270\2\2\u0733\u0774\7\u0271"+
		"\2\2\u0734\u0774\7\u0272\2\2\u0735\u0774\7\u0273\2\2\u0736\u0774\7\u0274"+
		"\2\2\u0737\u0774\7\u0275\2\2\u0738\u0774\7\u0276\2\2\u0739\u0774\7\u0277"+
		"\2\2\u073a\u0774\7\u0278\2\2\u073b\u0774\7\u0279\2\2\u073c\u0774\7\u027a"+
		"\2\2\u073d\u0774\7\u027b\2\2\u073e\u0774\7\u027c\2\2\u073f\u0774\7\u027d"+
		"\2\2\u0740\u0774\7\u027e\2\2\u0741\u0774\7\u027f\2\2\u0742\u0774\7\u0280"+
		"\2\2\u0743\u0774\7\u0281\2\2\u0744\u0774\7\u0282\2\2\u0745\u0774\7\u0283"+
		"\2\2\u0746\u0774\7\u0284\2\2\u0747\u0774\7\u0285\2\2\u0748\u0774\7\u0286"+
		"\2\2\u0749\u0774\7\u0287\2\2\u074a\u0774\7\u0288\2\2\u074b\u0774\7\u0289"+
		"\2\2\u074c\u0774\7\u028a\2\2\u074d\u0774\7\u028b\2\2\u074e\u0774\7\u028c"+
		"\2\2\u074f\u0774\7\u028d\2\2\u0750\u0774\7\u028e\2\2\u0751\u0774\7\u028f"+
		"\2\2\u0752\u0774\7\u0290\2\2\u0753\u0774\7\u0291\2\2\u0754\u0774\7\u0292"+
		"\2\2\u0755\u0774\7\u0293\2\2\u0756\u0774\7\u0294\2\2\u0757\u0774\7\u0295"+
		"\2\2\u0758\u0774\7\u0296\2\2\u0759\u0774\7\u0297\2\2\u075a\u0774\7\u0298"+
		"\2\2\u075b\u0774\7\u0299\2\2\u075c\u0774\7\u029a\2\2\u075d\u0774\7\u029b"+
		"\2\2\u075e\u0774\7\u029c\2\2\u075f\u0774\7\u029d\2\2\u0760\u0774\7\u029e"+
		"\2\2\u0761\u0774\7\u029f\2\2\u0762\u0774\7\u0083\2\2\u0763\u0774\7\u0237"+
		"\2\2\u0764\u0774\7\u0084\2\2\u0765\u0774\7\u0238\2\2\u0766\u0774\7\u0239"+
		"\2\2\u0767\u0774\7\u023a\2\2\u0768\u0774\7\u023b\2\2\u0769\u0774\7\u023c"+
		"\2\2\u076a\u0774\7\u023d\2\2\u076b\u0774\7\u023e\2\2\u076c\u0774\7\u023f"+
		"\2\2\u076d\u0774\7\u022c\2\2\u076e\u0774\7\u022d\2\2\u076f\u0774\7\u0082"+
		"\2\2\u0770\u0774\7\u022e\2\2\u0771\u0774\7\u022f\2\2\u0772\u0774\3\2\2"+
		"\2\u0773\u06e9\3\2\2\2\u0773\u06ea\3\2\2\2\u0773\u06eb\3\2\2\2\u0773\u06ec"+
		"\3\2\2\2\u0773\u06ed\3\2\2\2\u0773\u06ee\3\2\2\2\u0773\u06ef\3\2\2\2\u0773"+
		"\u06f0\3\2\2\2\u0773\u06f1\3\2\2\2\u0773\u06f2\3\2\2\2\u0773\u06f3\3\2"+
		"\2\2\u0773\u06f4\3\2\2\2\u0773\u06f5\3\2\2\2\u0773\u06f6\3\2\2\2\u0773"+
		"\u06f7\3\2\2\2\u0773\u06f8\3\2\2\2\u0773\u06f9\3\2\2\2\u0773\u06fa\3\2"+
		"\2\2\u0773\u06fb\3\2\2\2\u0773\u06fc\3\2\2\2\u0773\u06fd\3\2\2\2\u0773"+
		"\u06fe\3\2\2\2\u0773\u06ff\3\2\2\2\u0773\u0700\3\2\2\2\u0773\u0701\3\2"+
		"\2\2\u0773\u0702\3\2\2\2\u0773\u0703\3\2\2\2\u0773\u0704\3\2\2\2\u0773"+
		"\u0705\3\2\2\2\u0773\u0706\3\2\2\2\u0773\u0707\3\2\2\2\u0773\u0708\3\2"+
		"\2\2\u0773\u0709\3\2\2\2\u0773\u070a\3\2\2\2\u0773\u070b\3\2\2\2\u0773"+
		"\u070c\3\2\2\2\u0773\u070d\3\2\2\2\u0773\u070e\3\2\2\2\u0773\u070f\3\2"+
		"\2\2\u0773\u0710\3\2\2\2\u0773\u0711\3\2\2\2\u0773\u0712\3\2\2\2\u0773"+
		"\u0713\3\2\2\2\u0773\u0714\3\2\2\2\u0773\u0715\3\2\2\2\u0773\u0716\3\2"+
		"\2\2\u0773\u0717\3\2\2\2\u0773\u0718\3\2\2\2\u0773\u0719\3\2\2\2\u0773"+
		"\u071a\3\2\2\2\u0773\u071b\3\2\2\2\u0773\u071c\3\2\2\2\u0773\u071d\3\2"+
		"\2\2\u0773\u071e\3\2\2\2\u0773\u071f\3\2\2\2\u0773\u0720\3\2\2\2\u0773"+
		"\u0721\3\2\2\2\u0773\u0722\3\2\2\2\u0773\u0723\3\2\2\2\u0773\u0724\3\2"+
		"\2\2\u0773\u0725\3\2\2\2\u0773\u0726\3\2\2\2\u0773\u0727\3\2\2\2\u0773"+
		"\u0728\3\2\2\2\u0773\u0729\3\2\2\2\u0773\u072a\3\2\2\2\u0773\u072b\3\2"+
		"\2\2\u0773\u072c\3\2\2\2\u0773\u072d\3\2\2\2\u0773\u072e\3\2\2\2\u0773"+
		"\u072f\3\2\2\2\u0773\u0730\3\2\2\2\u0773\u0731\3\2\2\2\u0773\u0732\3\2"+
		"\2\2\u0773\u0733\3\2\2\2\u0773\u0734\3\2\2\2\u0773\u0735\3\2\2\2\u0773"+
		"\u0736\3\2\2\2\u0773\u0737\3\2\2\2\u0773\u0738\3\2\2\2\u0773\u0739\3\2"+
		"\2\2\u0773\u073a\3\2\2\2\u0773\u073b\3\2\2\2\u0773\u073c\3\2\2\2\u0773"+
		"\u073d\3\2\2\2\u0773\u073e\3\2\2\2\u0773\u073f\3\2\2\2\u0773\u0740\3\2"+
		"\2\2\u0773\u0741\3\2\2\2\u0773\u0742\3\2\2\2\u0773\u0743\3\2\2\2\u0773"+
		"\u0744\3\2\2\2\u0773\u0745\3\2\2\2\u0773\u0746\3\2\2\2\u0773\u0747\3\2"+
		"\2\2\u0773\u0748\3\2\2\2\u0773\u0749\3\2\2\2\u0773\u074a\3\2\2\2\u0773"+
		"\u074b\3\2\2\2\u0773\u074c\3\2\2\2\u0773\u074d\3\2\2\2\u0773\u074e\3\2"+
		"\2\2\u0773\u074f\3\2\2\2\u0773\u0750\3\2\2\2\u0773\u0751\3\2\2\2\u0773"+
		"\u0752\3\2\2\2\u0773\u0753\3\2\2\2\u0773\u0754\3\2\2\2\u0773\u0755\3\2"+
		"\2\2\u0773\u0756\3\2\2\2\u0773\u0757\3\2\2\2\u0773\u0758\3\2\2\2\u0773"+
		"\u0759\3\2\2\2\u0773\u075a\3\2\2\2\u0773\u075b\3\2\2\2\u0773\u075c\3\2"+
		"\2\2\u0773\u075d\3\2\2\2\u0773\u075e\3\2\2\2\u0773\u075f\3\2\2\2\u0773"+
		"\u0760\3\2\2\2\u0773\u0761\3\2\2\2\u0773\u0762\3\2\2\2\u0773\u0763\3\2"+
		"\2\2\u0773\u0764\3\2\2\2\u0773\u0765\3\2\2\2\u0773\u0766\3\2\2\2\u0773"+
		"\u0767\3\2\2\2\u0773\u0768\3\2\2\2\u0773\u0769\3\2\2\2\u0773\u076a\3\2"+
		"\2\2\u0773\u076b\3\2\2\2\u0773\u076c\3\2\2\2\u0773\u076d\3\2\2\2\u0773"+
		"\u076e\3\2\2\2\u0773\u076f\3\2\2\2\u0773\u0770\3\2\2\2\u0773\u0771\3\2"+
		"\2\2\u0773\u0772\3\2\2\2\u0774\u0123\3\2\2\2\u0775\u0776\7\u0132\2\2\u0776"+
		"\u0777\5\u00b8]\2\u0777\u0778\7\u015d\2\2\u0778\u077a\5\u00dep\2\u0779"+
		"\u077b\5\u0126\u0094\2\u077a\u0779\3\2\2\2\u077a\u077b\3\2\2\2\u077b\u0125"+
		"\3\2\2\2\u077c\u077d\7l\2\2\u077d\u077e\7R\2\2\u077e\u077f\7\u015e\2\2"+
		"\u077f\u078e\7\u015f\2\2\u0780\u0781\7l\2\2\u0781\u0782\7R\2\2\u0782\u0783"+
		"\7\u015e\2\2\u0783\u0784\7\u015f\2\2\u0784\u0785\7I\2\2\u0785\u0786\7"+
		"\u0160\2\2\u0786\u078e\7\u0162\2\2\u0787\u0788\7l\2\2\u0788\u0789\7|\2"+
		"\2\u0789\u078e\7\u015f\2\2\u078a\u078b\7I\2\2\u078b\u078c\7\u0160\2\2"+
		"\u078c\u078e\7\u0162\2\2\u078d\u077c\3\2\2\2\u078d\u0780\3\2\2\2\u078d"+
		"\u0787\3\2\2\2\u078d\u078a\3\2\2\2\u078e\u0127\3\2\2\2\u078f\u0791\7L"+
		"\2\2\u0790\u0792\5\u00ecw\2\u0791\u0790\3\2\2\2\u0791\u0792\3\2\2\2\u0792"+
		"\u0794\3\2\2\2\u0793\u0795\5\u012a\u0096\2\u0794\u0793\3\2\2\2\u0795\u0796"+
		"\3\2\2\2\u0796\u0794\3\2\2\2\u0796\u0797\3\2\2\2\u0797\u0799\3\2\2\2\u0798"+
		"\u079a\5\u012c\u0097\2\u0799\u0798\3\2\2\2\u0799\u079a\3\2\2\2\u079a\u079b"+
		"\3\2\2\2\u079b\u079c\7\u011a\2\2\u079c\u0129\3\2\2\2\u079d\u079e\7M\2"+
		"\2\u079e\u079f\5\u00dep\2\u079f\u07a0\7`\2\2\u07a0\u07a1\5\u00dep\2\u07a1"+
		"\u012b\3\2\2\2\u07a2\u07a3\7_\2\2\u07a3\u07a4\5\u00dep\2\u07a4\u012d\3"+
		"\2\2\2\u07a5\u07a6\7\u0081\2\2\u07a6\u07a7\5\u00dep\2\u07a7\u07a8\5\u0130"+
		"\u0099\2\u07a8\u012f\3\2\2\2\u07a9\u07aa\t&\2\2\u07aa\u0131\3\2\2\2\u07ab"+
		"\u07ac\7p\2\2\u07ac\u07ad\7r\2\2\u07ad\u07b2\5\u0134\u009b\2\u07ae\u07af"+
		"\7$\2\2\u07af\u07b1\5\u0134\u009b\2\u07b0\u07ae\3\2\2\2\u07b1\u07b4\3"+
		"\2\2\2\u07b2\u07b0\3\2\2\2\u07b2\u07b3\3\2\2\2\u07b3\u0133\3\2\2\2\u07b4"+
		"\u07b2\3\2\2\2\u07b5\u07b9\5\u00a2R\2\u07b6\u07b9\5\u0088E\2\u07b7\u07b9"+
		"\5\u00dep\2\u07b8\u07b5\3\2\2\2\u07b8\u07b6\3\2\2\2\u07b8\u07b7\3\2\2"+
		"\2\u07b9\u07bb\3\2\2\2\u07ba\u07bc\t%\2\2\u07bb\u07ba\3\2\2\2\u07bb\u07bc"+
		"\3\2\2\2\u07bc\u0135\3\2\2\2\u07bd\u07bf\5\u0138\u009d\2\u07be\u07c0\5"+
		"\u013a\u009e\2\u07bf\u07be\3\2\2\2\u07bf\u07c0\3\2\2\2\u07c0\u07c2\3\2"+
		"\2\2\u07c1\u07c3\5\u013e\u00a0\2\u07c2\u07c1\3\2\2\2\u07c2\u07c3\3\2\2"+
		"\2\u07c3\u07c5\3\2\2\2\u07c4\u07c6\5\u0140\u00a1\2\u07c5\u07c4\3\2\2\2"+
		"\u07c5\u07c6\3\2\2\2\u07c6\u07c8\3\2\2\2\u07c7\u07c9\t\'\2\2\u07c8\u07c7"+
		"\3\2\2\2\u07c8\u07c9\3\2\2\2\u07c9\u07cb\3\2\2\2\u07ca\u07cc\7\u0158\2"+
		"\2\u07cb\u07ca\3\2\2\2\u07cb\u07cc\3\2\2\2\u07cc\u07d6\3\2\2\2\u07cd\u07ce"+
		"\5\u0138\u009d\2\u07ce\u07d0\5\u013c\u009f\2\u07cf\u07d1\5\u013e\u00a0"+
		"\2\u07d0\u07cf\3\2\2\2\u07d0\u07d1\3\2\2\2\u07d1\u07d3\3\2\2\2\u07d2\u07d4"+
		"\5\u0140\u00a1\2\u07d3\u07d2\3\2\2\2\u07d3\u07d4\3\2\2\2\u07d4\u07d6\3"+
		"\2\2\2\u07d5\u07bd\3\2\2\2\u07d5\u07cd\3\2\2\2\u07d6\u0137\3\2\2\2\u07d7"+
		"\u07d8\t(\2\2\u07d8\u0139\3\2\2\2\u07d9\u07da\7\36\2\2\u07da\u07dd\7\u02bf"+
		"\2\2\u07db\u07dc\7$\2\2\u07dc\u07de\7\u02bf\2\2\u07dd\u07db\3\2\2\2\u07dd"+
		"\u07de\3\2\2\2\u07de\u07df\3\2\2\2\u07df\u07e0\7\37\2\2\u07e0\u013b\3"+
		"\2\2\2\u07e1\u07e2\7\36\2\2\u07e2\u07e7\7\u02be\2\2\u07e3\u07e4\7$\2\2"+
		"\u07e4\u07e6\7\u02be\2\2\u07e5\u07e3\3\2\2\2\u07e6\u07e9\3\2\2\2\u07e7"+
		"\u07e5\3\2\2\2\u07e7\u07e8\3\2\2\2\u07e8\u07ea\3\2\2\2\u07e9\u07e7\3\2"+
		"\2\2\u07ea\u07eb\7\37\2\2\u07eb\u013d\3\2\2\2\u07ec\u07ed\t)\2\2\u07ed"+
		"\u07ef\78\2\2\u07ee\u07f0\7\27\2\2\u07ef\u07ee\3\2\2\2\u07ef\u07f0\3\2"+
		"\2\2\u07f0\u07f1\3\2\2\2\u07f1\u07f2\5\u0142\u00a2\2\u07f2\u013f\3\2\2"+
		"\2\u07f3\u07f5\7\u010a\2\2\u07f4\u07f6\7\27\2\2\u07f5\u07f4\3\2\2\2\u07f5"+
		"\u07f6\3\2\2\2\u07f6\u07f9\3\2\2\2\u07f7\u07fa\7\u02be\2\2\u07f8\u07fa"+
		"\5\u0142\u00a2\2\u07f9\u07f7\3\2\2\2\u07f9\u07f8\3\2\2\2\u07fa\u0141\3"+
		"\2\2\2\u07fb\u07fe\5\u0098M\2\u07fc\u07fd\7\23\2\2\u07fd\u07ff\5\u0098"+
		"M\2\u07fe\u07fc\3\2\2\2\u07fe\u07ff\3\2\2\2\u07ff\u0143\3\2\2\2\u0800"+
		"\u0805\5\u0142\u00a2\2\u0801\u0802\7$\2\2\u0802\u0804\5\u0142\u00a2\2"+
		"\u0803\u0801\3\2\2\2\u0804\u0807\3\2\2\2\u0805\u0803\3\2\2\2\u0805\u0806"+
		"\3\2\2\2\u0806\u0145\3\2\2\2\u0807\u0805\3\2\2\2\u00f7\u0149\u014d\u0152"+
		"\u0155\u0158\u015b\u015e\u0166\u016a\u0177\u017c\u017f\u0183\u0188\u0191"+
		"\u0194\u0197\u019a\u019d\u01a9\u01b2\u01b9\u01be\u01c7\u01ca\u01cd\u01d0"+
		"\u01d3\u01d8\u01db\u01de\u01e9\u01ed\u01f2\u01f6\u01fa\u0206\u020b\u0211"+
		"\u0217\u021d\u0220\u022c\u0230\u0234\u023c\u0240\u024c\u0251\u0254\u0259"+
		"\u0265\u026a\u026f\u0275\u0277\u027d\u027f\u0285\u028d\u0292\u0295\u029a"+
		"\u029d\u02a2\u02aa\u02b2\u02b8\u02c0\u02c5\u02c8\u02cc\u02d3\u02d8\u02e0"+
		"\u02e5\u02ec\u02f1\u02f4\u02f7\u02fa\u02fd\u0300\u0303\u0306\u0309\u0313"+
		"\u0319\u031f\u0324\u0327\u032a\u032d\u0331\u0340\u0349\u034f\u0354\u0357"+
		"\u035a\u035d\u0361\u0365\u036b\u0374\u037e\u0389\u038b\u0393\u0399\u039d"+
		"\u03a1\u03a5\u03af\u03b3\u03b9\u03c5\u03ca\u03d3\u03da\u03de\u03e2\u03ea"+
		"\u03fd\u0403\u040b\u0413\u041f\u0425\u0427\u042d\u042f\u0431\u0439\u0444"+
		"\u0447\u044b\u044e\u0459\u045c\u0460\u0463\u0467\u0473\u0478\u047b\u047e"+
		"\u0481\u048a\u0491\u049c\u04a4\u04a8\u04ac\u04b2\u04b5\u04bb\u04be\u04c5"+
		"\u04cc\u04d0\u04d3\u04da\u04de\u04fd\u0507\u0519\u0522\u0524\u0531\u0540"+
		"\u0542\u0549\u0550\u0559\u0560\u056e\u0574\u0578\u057e\u05ad\u05af\u05bb"+
		"\u05c3\u05c9\u05d4\u05dd\u05df\u05e1\u05e7\u05ec\u05f3\u05f7\u05fb\u0607"+
		"\u060a\u060d\u0610\u0613\u061c\u0622\u0630\u0644\u0649\u0650\u0654\u0657"+
		"\u065b\u0666\u0681\u0691\u06a3\u06a8\u06be\u06c1\u06cb\u06d1\u06d5\u06d8"+
		"\u06e1\u06e5\u0773\u077a\u078d\u0791\u0796\u0799\u07b2\u07b8\u07bb\u07bf"+
		"\u07c2\u07c5\u07c8\u07cb\u07d0\u07d3\u07d5\u07dd\u07e7\u07ef\u07f5\u07f9"+
		"\u07fe\u0805";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}