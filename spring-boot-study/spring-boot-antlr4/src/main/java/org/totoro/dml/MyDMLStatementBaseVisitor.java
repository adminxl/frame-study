package org.totoro.dml;

import org.antlr.v4.runtime.tree.ParseTree;

/**
 * @author daocr
 * @date 2020/3/24
 * @see https://www.cnblogs.com/chunzhulovefeiyue/p/7577199.html
 * @see https://cloud.tencent.com/developer/article/1556983
 */
public class MyDMLStatementBaseVisitor extends DMLStatementBaseVisitor<Object> {


    @Override
    public Object visitSelectClause(DMLStatementParser.SelectClauseContext ctx) {

        for (ParseTree child : ctx.whereClause().children) {
            System.out.println(child.getText());
        }

        return super.visitSelectClause(ctx);
    }
}
