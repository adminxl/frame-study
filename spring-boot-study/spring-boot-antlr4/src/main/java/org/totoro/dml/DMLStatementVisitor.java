package org.totoro.dml;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/DMLStatement.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link DMLStatementParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface DMLStatementVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#insert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert(DMLStatementParser.InsertContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#insertSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertSpecification_(DMLStatementParser.InsertSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#insertValuesClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertValuesClause(DMLStatementParser.InsertValuesClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#insertSelectClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsertSelectClause(DMLStatementParser.InsertSelectClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#onDuplicateKeyClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOnDuplicateKeyClause(DMLStatementParser.OnDuplicateKeyClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#replace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(DMLStatementParser.ReplaceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#replaceSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplaceSpecification_(DMLStatementParser.ReplaceSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#update}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate(DMLStatementParser.UpdateContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#updateSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdateSpecification_(DMLStatementParser.UpdateSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(DMLStatementParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#setAssignmentsClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetAssignmentsClause(DMLStatementParser.SetAssignmentsClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#assignmentValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentValues(DMLStatementParser.AssignmentValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#assignmentValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentValue(DMLStatementParser.AssignmentValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#blobValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlobValue(DMLStatementParser.BlobValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#delete}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelete(DMLStatementParser.DeleteContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#deleteSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeleteSpecification_(DMLStatementParser.DeleteSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#singleTableClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleTableClause(DMLStatementParser.SingleTableClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#multipleTablesClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleTablesClause(DMLStatementParser.MultipleTablesClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#multipleTableNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleTableNames(DMLStatementParser.MultipleTableNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#select}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect(DMLStatementParser.SelectContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(DMLStatementParser.CallContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#doStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoStatement(DMLStatementParser.DoStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#handlerStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerStatement(DMLStatementParser.HandlerStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerOpenStatement(DMLStatementParser.HandlerOpenStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerReadIndexStatement(DMLStatementParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerReadStatement(DMLStatementParser.HandlerReadStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHandlerCloseStatement(DMLStatementParser.HandlerCloseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#importStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportStatement(DMLStatementParser.ImportStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#loadDataStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoadDataStatement(DMLStatementParser.LoadDataStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoadXmlStatement(DMLStatementParser.LoadXmlStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#withClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWithClause_(DMLStatementParser.WithClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#cteClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCteClause_(DMLStatementParser.CteClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#unionClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnionClause(DMLStatementParser.UnionClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#selectClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectClause(DMLStatementParser.SelectClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#selectSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectSpecification(DMLStatementParser.SelectSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#duplicateSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDuplicateSpecification(DMLStatementParser.DuplicateSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#projections}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProjections(DMLStatementParser.ProjectionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#projection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProjection(DMLStatementParser.ProjectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlias(DMLStatementParser.AliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#unqualifiedShorthand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnqualifiedShorthand(DMLStatementParser.UnqualifiedShorthandContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#qualifiedShorthand}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualifiedShorthand(DMLStatementParser.QualifiedShorthandContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#fromClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFromClause(DMLStatementParser.FromClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#tableReferences}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableReferences(DMLStatementParser.TableReferencesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#escapedTableReference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEscapedTableReference(DMLStatementParser.EscapedTableReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#tableReference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableReference(DMLStatementParser.TableReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#tableFactor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableFactor(DMLStatementParser.TableFactorContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#partitionNames_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionNames_(DMLStatementParser.PartitionNames_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#indexHintList_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexHintList_(DMLStatementParser.IndexHintList_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#indexHint_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexHint_(DMLStatementParser.IndexHint_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#joinedTable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoinedTable(DMLStatementParser.JoinedTableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#joinSpecification}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoinSpecification(DMLStatementParser.JoinSpecificationContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#whereClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereClause(DMLStatementParser.WhereClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#groupByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupByClause(DMLStatementParser.GroupByClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#havingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHavingClause(DMLStatementParser.HavingClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#limitClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitClause(DMLStatementParser.LimitClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#limitRowCount}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitRowCount(DMLStatementParser.LimitRowCountContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#limitOffset}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimitOffset(DMLStatementParser.LimitOffsetContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#windowClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowClause_(DMLStatementParser.WindowClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#windowItem_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowItem_(DMLStatementParser.WindowItem_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubquery(DMLStatementParser.SubqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#selectLinesInto_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectLinesInto_(DMLStatementParser.SelectLinesInto_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#selectFieldsInto_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectFieldsInto_(DMLStatementParser.SelectFieldsInto_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#selectIntoExpression_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectIntoExpression_(DMLStatementParser.SelectIntoExpression_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#lockClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLockClause(DMLStatementParser.LockClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#parameterMarker}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterMarker(DMLStatementParser.ParameterMarkerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#literals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiterals(DMLStatementParser.LiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#stringLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiterals(DMLStatementParser.StringLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#numberLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberLiterals(DMLStatementParser.NumberLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateTimeLiterals(DMLStatementParser.DateTimeLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHexadecimalLiterals(DMLStatementParser.HexadecimalLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitValueLiterals(DMLStatementParser.BitValueLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#booleanLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanLiterals(DMLStatementParser.BooleanLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullValueLiterals(DMLStatementParser.NullValueLiteralsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#characterSetName_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacterSetName_(DMLStatementParser.CharacterSetName_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#collationName_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollationName_(DMLStatementParser.CollationName_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(DMLStatementParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#unreservedWord}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnreservedWord(DMLStatementParser.UnreservedWordContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(DMLStatementParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#schemaName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSchemaName(DMLStatementParser.SchemaNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#tableName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableName(DMLStatementParser.TableNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#columnName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnName(DMLStatementParser.ColumnNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#indexName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexName(DMLStatementParser.IndexNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#userName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUserName(DMLStatementParser.UserNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#eventName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEventName(DMLStatementParser.EventNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#serverName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitServerName(DMLStatementParser.ServerNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#wrapperName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWrapperName(DMLStatementParser.WrapperNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#functionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionName(DMLStatementParser.FunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#viewName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitViewName(DMLStatementParser.ViewNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#owner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOwner(DMLStatementParser.OwnerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(DMLStatementParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#tableNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTableNames(DMLStatementParser.TableNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#columnNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumnNames(DMLStatementParser.ColumnNamesContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#groupName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupName(DMLStatementParser.GroupNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#shardLibraryName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShardLibraryName(DMLStatementParser.ShardLibraryNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#componentName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComponentName(DMLStatementParser.ComponentNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#pluginName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPluginName(DMLStatementParser.PluginNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#hostName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHostName(DMLStatementParser.HostNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#port}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPort(DMLStatementParser.PortContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#cloneInstance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloneInstance(DMLStatementParser.CloneInstanceContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#cloneDir}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloneDir(DMLStatementParser.CloneDirContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#channelName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChannelName(DMLStatementParser.ChannelNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#logName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogName(DMLStatementParser.LogNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#roleName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoleName(DMLStatementParser.RoleNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#engineName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEngineName(DMLStatementParser.EngineNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#triggerName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerName(DMLStatementParser.TriggerNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#triggerTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerTime(DMLStatementParser.TriggerTimeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#userOrRole}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUserOrRole(DMLStatementParser.UserOrRoleContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#partitionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionName(DMLStatementParser.PartitionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#triggerEvent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerEvent(DMLStatementParser.TriggerEventContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#triggerOrder}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTriggerOrder(DMLStatementParser.TriggerOrderContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(DMLStatementParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#logicalOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOperator(DMLStatementParser.LogicalOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#notOperator_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotOperator_(DMLStatementParser.NotOperator_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#booleanPrimary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanPrimary(DMLStatementParser.BooleanPrimaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#comparisonOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparisonOperator(DMLStatementParser.ComparisonOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#predicate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicate(DMLStatementParser.PredicateContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#bitExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitExpr(DMLStatementParser.BitExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#simpleExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleExpr(DMLStatementParser.SimpleExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(DMLStatementParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#aggregationFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunction(DMLStatementParser.AggregationFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregationFunctionName(DMLStatementParser.AggregationFunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#distinct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDistinct(DMLStatementParser.DistinctContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#overClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverClause_(DMLStatementParser.OverClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#windowSpecification_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowSpecification_(DMLStatementParser.WindowSpecification_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#partitionClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPartitionClause_(DMLStatementParser.PartitionClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#frameClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameClause_(DMLStatementParser.FrameClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#frameStart_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameStart_(DMLStatementParser.FrameStart_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#frameEnd_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameEnd_(DMLStatementParser.FrameEnd_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#frameBetween_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrameBetween_(DMLStatementParser.FrameBetween_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#specialFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecialFunction(DMLStatementParser.SpecialFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupConcatFunction(DMLStatementParser.GroupConcatFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#windowFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowFunction(DMLStatementParser.WindowFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#castFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCastFunction(DMLStatementParser.CastFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#convertFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConvertFunction(DMLStatementParser.ConvertFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#positionFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPositionFunction(DMLStatementParser.PositionFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#substringFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstringFunction(DMLStatementParser.SubstringFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#extractFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractFunction(DMLStatementParser.ExtractFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#charFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharFunction(DMLStatementParser.CharFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#trimFunction_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrimFunction_(DMLStatementParser.TrimFunction_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#valuesFunction_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuesFunction_(DMLStatementParser.ValuesFunction_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#weightStringFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWeightStringFunction(DMLStatementParser.WeightStringFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#levelClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLevelClause_(DMLStatementParser.LevelClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLevelInWeightListElement_(DMLStatementParser.LevelInWeightListElement_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#regularFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegularFunction(DMLStatementParser.RegularFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRegularFunctionName_(DMLStatementParser.RegularFunctionName_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#matchExpression_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatchExpression_(DMLStatementParser.MatchExpression_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatchSearchModifier_(DMLStatementParser.MatchSearchModifier_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#caseExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseExpression(DMLStatementParser.CaseExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#caseWhen_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseWhen_(DMLStatementParser.CaseWhen_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#caseElse_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseElse_(DMLStatementParser.CaseElse_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#intervalExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalExpression(DMLStatementParser.IntervalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#intervalUnit_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalUnit_(DMLStatementParser.IntervalUnit_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#orderByClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrderByClause(DMLStatementParser.OrderByClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#orderByItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrderByItem(DMLStatementParser.OrderByItemContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(DMLStatementParser.DataTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#dataTypeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeName(DMLStatementParser.DataTypeNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#dataTypeLength}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataTypeLength(DMLStatementParser.DataTypeLengthContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#collectionOptions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollectionOptions(DMLStatementParser.CollectionOptionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#characterSet_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacterSet_(DMLStatementParser.CharacterSet_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#collateClause_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollateClause_(DMLStatementParser.CollateClause_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIgnoredIdentifier_(DMLStatementParser.IgnoredIdentifier_Context ctx);
	/**
	 * Visit a parse tree produced by {@link DMLStatementParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIgnoredIdentifiers_(DMLStatementParser.IgnoredIdentifiers_Context ctx);
}