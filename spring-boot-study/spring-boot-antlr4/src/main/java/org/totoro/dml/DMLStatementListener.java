package org.totoro.dml;// Generated from /Users/daocr/IdeaProjects/open_source/incubator-shardingsphere/shardingsphere-sql-parser/shardingsphere-sql-parser-dialect/shardingsphere-sql-parser-mysql/src/main/antlr4/imports/mysql/DMLStatement.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link DMLStatementParser}.
 */
public interface DMLStatementListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#insert}.
	 * @param ctx the parse tree
	 */
	void enterInsert(DMLStatementParser.InsertContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#insert}.
	 * @param ctx the parse tree
	 */
	void exitInsert(DMLStatementParser.InsertContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#insertSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterInsertSpecification_(DMLStatementParser.InsertSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#insertSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitInsertSpecification_(DMLStatementParser.InsertSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#insertValuesClause}.
	 * @param ctx the parse tree
	 */
	void enterInsertValuesClause(DMLStatementParser.InsertValuesClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#insertValuesClause}.
	 * @param ctx the parse tree
	 */
	void exitInsertValuesClause(DMLStatementParser.InsertValuesClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#insertSelectClause}.
	 * @param ctx the parse tree
	 */
	void enterInsertSelectClause(DMLStatementParser.InsertSelectClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#insertSelectClause}.
	 * @param ctx the parse tree
	 */
	void exitInsertSelectClause(DMLStatementParser.InsertSelectClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#onDuplicateKeyClause}.
	 * @param ctx the parse tree
	 */
	void enterOnDuplicateKeyClause(DMLStatementParser.OnDuplicateKeyClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#onDuplicateKeyClause}.
	 * @param ctx the parse tree
	 */
	void exitOnDuplicateKeyClause(DMLStatementParser.OnDuplicateKeyClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#replace}.
	 * @param ctx the parse tree
	 */
	void enterReplace(DMLStatementParser.ReplaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#replace}.
	 * @param ctx the parse tree
	 */
	void exitReplace(DMLStatementParser.ReplaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#replaceSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterReplaceSpecification_(DMLStatementParser.ReplaceSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#replaceSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitReplaceSpecification_(DMLStatementParser.ReplaceSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#update}.
	 * @param ctx the parse tree
	 */
	void enterUpdate(DMLStatementParser.UpdateContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#update}.
	 * @param ctx the parse tree
	 */
	void exitUpdate(DMLStatementParser.UpdateContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#updateSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterUpdateSpecification_(DMLStatementParser.UpdateSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#updateSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitUpdateSpecification_(DMLStatementParser.UpdateSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(DMLStatementParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(DMLStatementParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#setAssignmentsClause}.
	 * @param ctx the parse tree
	 */
	void enterSetAssignmentsClause(DMLStatementParser.SetAssignmentsClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#setAssignmentsClause}.
	 * @param ctx the parse tree
	 */
	void exitSetAssignmentsClause(DMLStatementParser.SetAssignmentsClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#assignmentValues}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentValues(DMLStatementParser.AssignmentValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#assignmentValues}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentValues(DMLStatementParser.AssignmentValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#assignmentValue}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentValue(DMLStatementParser.AssignmentValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#assignmentValue}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentValue(DMLStatementParser.AssignmentValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#blobValue}.
	 * @param ctx the parse tree
	 */
	void enterBlobValue(DMLStatementParser.BlobValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#blobValue}.
	 * @param ctx the parse tree
	 */
	void exitBlobValue(DMLStatementParser.BlobValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#delete}.
	 * @param ctx the parse tree
	 */
	void enterDelete(DMLStatementParser.DeleteContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#delete}.
	 * @param ctx the parse tree
	 */
	void exitDelete(DMLStatementParser.DeleteContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#deleteSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterDeleteSpecification_(DMLStatementParser.DeleteSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#deleteSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitDeleteSpecification_(DMLStatementParser.DeleteSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#singleTableClause}.
	 * @param ctx the parse tree
	 */
	void enterSingleTableClause(DMLStatementParser.SingleTableClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#singleTableClause}.
	 * @param ctx the parse tree
	 */
	void exitSingleTableClause(DMLStatementParser.SingleTableClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#multipleTablesClause}.
	 * @param ctx the parse tree
	 */
	void enterMultipleTablesClause(DMLStatementParser.MultipleTablesClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#multipleTablesClause}.
	 * @param ctx the parse tree
	 */
	void exitMultipleTablesClause(DMLStatementParser.MultipleTablesClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#multipleTableNames}.
	 * @param ctx the parse tree
	 */
	void enterMultipleTableNames(DMLStatementParser.MultipleTableNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#multipleTableNames}.
	 * @param ctx the parse tree
	 */
	void exitMultipleTableNames(DMLStatementParser.MultipleTableNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#select}.
	 * @param ctx the parse tree
	 */
	void enterSelect(DMLStatementParser.SelectContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#select}.
	 * @param ctx the parse tree
	 */
	void exitSelect(DMLStatementParser.SelectContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#call}.
	 * @param ctx the parse tree
	 */
	void enterCall(DMLStatementParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#call}.
	 * @param ctx the parse tree
	 */
	void exitCall(DMLStatementParser.CallContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#doStatement}.
	 * @param ctx the parse tree
	 */
	void enterDoStatement(DMLStatementParser.DoStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#doStatement}.
	 * @param ctx the parse tree
	 */
	void exitDoStatement(DMLStatementParser.DoStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#handlerStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerStatement(DMLStatementParser.HandlerStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#handlerStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerStatement(DMLStatementParser.HandlerStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerOpenStatement(DMLStatementParser.HandlerOpenStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#handlerOpenStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerOpenStatement(DMLStatementParser.HandlerOpenStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerReadIndexStatement(DMLStatementParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#handlerReadIndexStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerReadIndexStatement(DMLStatementParser.HandlerReadIndexStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerReadStatement(DMLStatementParser.HandlerReadStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#handlerReadStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerReadStatement(DMLStatementParser.HandlerReadStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 */
	void enterHandlerCloseStatement(DMLStatementParser.HandlerCloseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#handlerCloseStatement}.
	 * @param ctx the parse tree
	 */
	void exitHandlerCloseStatement(DMLStatementParser.HandlerCloseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void enterImportStatement(DMLStatementParser.ImportStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void exitImportStatement(DMLStatementParser.ImportStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#loadDataStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoadDataStatement(DMLStatementParser.LoadDataStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#loadDataStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoadDataStatement(DMLStatementParser.LoadDataStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 */
	void enterLoadXmlStatement(DMLStatementParser.LoadXmlStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#loadXmlStatement}.
	 * @param ctx the parse tree
	 */
	void exitLoadXmlStatement(DMLStatementParser.LoadXmlStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#withClause_}.
	 * @param ctx the parse tree
	 */
	void enterWithClause_(DMLStatementParser.WithClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#withClause_}.
	 * @param ctx the parse tree
	 */
	void exitWithClause_(DMLStatementParser.WithClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#cteClause_}.
	 * @param ctx the parse tree
	 */
	void enterCteClause_(DMLStatementParser.CteClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#cteClause_}.
	 * @param ctx the parse tree
	 */
	void exitCteClause_(DMLStatementParser.CteClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#unionClause}.
	 * @param ctx the parse tree
	 */
	void enterUnionClause(DMLStatementParser.UnionClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#unionClause}.
	 * @param ctx the parse tree
	 */
	void exitUnionClause(DMLStatementParser.UnionClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void enterSelectClause(DMLStatementParser.SelectClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void exitSelectClause(DMLStatementParser.SelectClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#selectSpecification}.
	 * @param ctx the parse tree
	 */
	void enterSelectSpecification(DMLStatementParser.SelectSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#selectSpecification}.
	 * @param ctx the parse tree
	 */
	void exitSelectSpecification(DMLStatementParser.SelectSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#duplicateSpecification}.
	 * @param ctx the parse tree
	 */
	void enterDuplicateSpecification(DMLStatementParser.DuplicateSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#duplicateSpecification}.
	 * @param ctx the parse tree
	 */
	void exitDuplicateSpecification(DMLStatementParser.DuplicateSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#projections}.
	 * @param ctx the parse tree
	 */
	void enterProjections(DMLStatementParser.ProjectionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#projections}.
	 * @param ctx the parse tree
	 */
	void exitProjections(DMLStatementParser.ProjectionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#projection}.
	 * @param ctx the parse tree
	 */
	void enterProjection(DMLStatementParser.ProjectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#projection}.
	 * @param ctx the parse tree
	 */
	void exitProjection(DMLStatementParser.ProjectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#alias}.
	 * @param ctx the parse tree
	 */
	void enterAlias(DMLStatementParser.AliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#alias}.
	 * @param ctx the parse tree
	 */
	void exitAlias(DMLStatementParser.AliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#unqualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void enterUnqualifiedShorthand(DMLStatementParser.UnqualifiedShorthandContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#unqualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void exitUnqualifiedShorthand(DMLStatementParser.UnqualifiedShorthandContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#qualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedShorthand(DMLStatementParser.QualifiedShorthandContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#qualifiedShorthand}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedShorthand(DMLStatementParser.QualifiedShorthandContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void enterFromClause(DMLStatementParser.FromClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void exitFromClause(DMLStatementParser.FromClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#tableReferences}.
	 * @param ctx the parse tree
	 */
	void enterTableReferences(DMLStatementParser.TableReferencesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#tableReferences}.
	 * @param ctx the parse tree
	 */
	void exitTableReferences(DMLStatementParser.TableReferencesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#escapedTableReference}.
	 * @param ctx the parse tree
	 */
	void enterEscapedTableReference(DMLStatementParser.EscapedTableReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#escapedTableReference}.
	 * @param ctx the parse tree
	 */
	void exitEscapedTableReference(DMLStatementParser.EscapedTableReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#tableReference}.
	 * @param ctx the parse tree
	 */
	void enterTableReference(DMLStatementParser.TableReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#tableReference}.
	 * @param ctx the parse tree
	 */
	void exitTableReference(DMLStatementParser.TableReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#tableFactor}.
	 * @param ctx the parse tree
	 */
	void enterTableFactor(DMLStatementParser.TableFactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#tableFactor}.
	 * @param ctx the parse tree
	 */
	void exitTableFactor(DMLStatementParser.TableFactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#partitionNames_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionNames_(DMLStatementParser.PartitionNames_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#partitionNames_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionNames_(DMLStatementParser.PartitionNames_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#indexHintList_}.
	 * @param ctx the parse tree
	 */
	void enterIndexHintList_(DMLStatementParser.IndexHintList_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#indexHintList_}.
	 * @param ctx the parse tree
	 */
	void exitIndexHintList_(DMLStatementParser.IndexHintList_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#indexHint_}.
	 * @param ctx the parse tree
	 */
	void enterIndexHint_(DMLStatementParser.IndexHint_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#indexHint_}.
	 * @param ctx the parse tree
	 */
	void exitIndexHint_(DMLStatementParser.IndexHint_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#joinedTable}.
	 * @param ctx the parse tree
	 */
	void enterJoinedTable(DMLStatementParser.JoinedTableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#joinedTable}.
	 * @param ctx the parse tree
	 */
	void exitJoinedTable(DMLStatementParser.JoinedTableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#joinSpecification}.
	 * @param ctx the parse tree
	 */
	void enterJoinSpecification(DMLStatementParser.JoinSpecificationContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#joinSpecification}.
	 * @param ctx the parse tree
	 */
	void exitJoinSpecification(DMLStatementParser.JoinSpecificationContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void enterWhereClause(DMLStatementParser.WhereClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void exitWhereClause(DMLStatementParser.WhereClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#groupByClause}.
	 * @param ctx the parse tree
	 */
	void enterGroupByClause(DMLStatementParser.GroupByClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#groupByClause}.
	 * @param ctx the parse tree
	 */
	void exitGroupByClause(DMLStatementParser.GroupByClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#havingClause}.
	 * @param ctx the parse tree
	 */
	void enterHavingClause(DMLStatementParser.HavingClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#havingClause}.
	 * @param ctx the parse tree
	 */
	void exitHavingClause(DMLStatementParser.HavingClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#limitClause}.
	 * @param ctx the parse tree
	 */
	void enterLimitClause(DMLStatementParser.LimitClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#limitClause}.
	 * @param ctx the parse tree
	 */
	void exitLimitClause(DMLStatementParser.LimitClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#limitRowCount}.
	 * @param ctx the parse tree
	 */
	void enterLimitRowCount(DMLStatementParser.LimitRowCountContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#limitRowCount}.
	 * @param ctx the parse tree
	 */
	void exitLimitRowCount(DMLStatementParser.LimitRowCountContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#limitOffset}.
	 * @param ctx the parse tree
	 */
	void enterLimitOffset(DMLStatementParser.LimitOffsetContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#limitOffset}.
	 * @param ctx the parse tree
	 */
	void exitLimitOffset(DMLStatementParser.LimitOffsetContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#windowClause_}.
	 * @param ctx the parse tree
	 */
	void enterWindowClause_(DMLStatementParser.WindowClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#windowClause_}.
	 * @param ctx the parse tree
	 */
	void exitWindowClause_(DMLStatementParser.WindowClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#windowItem_}.
	 * @param ctx the parse tree
	 */
	void enterWindowItem_(DMLStatementParser.WindowItem_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#windowItem_}.
	 * @param ctx the parse tree
	 */
	void exitWindowItem_(DMLStatementParser.WindowItem_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#subquery}.
	 * @param ctx the parse tree
	 */
	void enterSubquery(DMLStatementParser.SubqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#subquery}.
	 * @param ctx the parse tree
	 */
	void exitSubquery(DMLStatementParser.SubqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#selectLinesInto_}.
	 * @param ctx the parse tree
	 */
	void enterSelectLinesInto_(DMLStatementParser.SelectLinesInto_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#selectLinesInto_}.
	 * @param ctx the parse tree
	 */
	void exitSelectLinesInto_(DMLStatementParser.SelectLinesInto_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#selectFieldsInto_}.
	 * @param ctx the parse tree
	 */
	void enterSelectFieldsInto_(DMLStatementParser.SelectFieldsInto_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#selectFieldsInto_}.
	 * @param ctx the parse tree
	 */
	void exitSelectFieldsInto_(DMLStatementParser.SelectFieldsInto_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#selectIntoExpression_}.
	 * @param ctx the parse tree
	 */
	void enterSelectIntoExpression_(DMLStatementParser.SelectIntoExpression_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#selectIntoExpression_}.
	 * @param ctx the parse tree
	 */
	void exitSelectIntoExpression_(DMLStatementParser.SelectIntoExpression_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#lockClause}.
	 * @param ctx the parse tree
	 */
	void enterLockClause(DMLStatementParser.LockClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#lockClause}.
	 * @param ctx the parse tree
	 */
	void exitLockClause(DMLStatementParser.LockClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#parameterMarker}.
	 * @param ctx the parse tree
	 */
	void enterParameterMarker(DMLStatementParser.ParameterMarkerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#parameterMarker}.
	 * @param ctx the parse tree
	 */
	void exitParameterMarker(DMLStatementParser.ParameterMarkerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#literals}.
	 * @param ctx the parse tree
	 */
	void enterLiterals(DMLStatementParser.LiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#literals}.
	 * @param ctx the parse tree
	 */
	void exitLiterals(DMLStatementParser.LiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#stringLiterals}.
	 * @param ctx the parse tree
	 */
	void enterStringLiterals(DMLStatementParser.StringLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#stringLiterals}.
	 * @param ctx the parse tree
	 */
	void exitStringLiterals(DMLStatementParser.StringLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#numberLiterals}.
	 * @param ctx the parse tree
	 */
	void enterNumberLiterals(DMLStatementParser.NumberLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#numberLiterals}.
	 * @param ctx the parse tree
	 */
	void exitNumberLiterals(DMLStatementParser.NumberLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 */
	void enterDateTimeLiterals(DMLStatementParser.DateTimeLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#dateTimeLiterals}.
	 * @param ctx the parse tree
	 */
	void exitDateTimeLiterals(DMLStatementParser.DateTimeLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 */
	void enterHexadecimalLiterals(DMLStatementParser.HexadecimalLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#hexadecimalLiterals}.
	 * @param ctx the parse tree
	 */
	void exitHexadecimalLiterals(DMLStatementParser.HexadecimalLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 */
	void enterBitValueLiterals(DMLStatementParser.BitValueLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#bitValueLiterals}.
	 * @param ctx the parse tree
	 */
	void exitBitValueLiterals(DMLStatementParser.BitValueLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#booleanLiterals}.
	 * @param ctx the parse tree
	 */
	void enterBooleanLiterals(DMLStatementParser.BooleanLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#booleanLiterals}.
	 * @param ctx the parse tree
	 */
	void exitBooleanLiterals(DMLStatementParser.BooleanLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 */
	void enterNullValueLiterals(DMLStatementParser.NullValueLiteralsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#nullValueLiterals}.
	 * @param ctx the parse tree
	 */
	void exitNullValueLiterals(DMLStatementParser.NullValueLiteralsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#characterSetName_}.
	 * @param ctx the parse tree
	 */
	void enterCharacterSetName_(DMLStatementParser.CharacterSetName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#characterSetName_}.
	 * @param ctx the parse tree
	 */
	void exitCharacterSetName_(DMLStatementParser.CharacterSetName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#collationName_}.
	 * @param ctx the parse tree
	 */
	void enterCollationName_(DMLStatementParser.CollationName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#collationName_}.
	 * @param ctx the parse tree
	 */
	void exitCollationName_(DMLStatementParser.CollationName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(DMLStatementParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(DMLStatementParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#unreservedWord}.
	 * @param ctx the parse tree
	 */
	void enterUnreservedWord(DMLStatementParser.UnreservedWordContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#unreservedWord}.
	 * @param ctx the parse tree
	 */
	void exitUnreservedWord(DMLStatementParser.UnreservedWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(DMLStatementParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(DMLStatementParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#schemaName}.
	 * @param ctx the parse tree
	 */
	void enterSchemaName(DMLStatementParser.SchemaNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#schemaName}.
	 * @param ctx the parse tree
	 */
	void exitSchemaName(DMLStatementParser.SchemaNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#tableName}.
	 * @param ctx the parse tree
	 */
	void enterTableName(DMLStatementParser.TableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#tableName}.
	 * @param ctx the parse tree
	 */
	void exitTableName(DMLStatementParser.TableNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#columnName}.
	 * @param ctx the parse tree
	 */
	void enterColumnName(DMLStatementParser.ColumnNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#columnName}.
	 * @param ctx the parse tree
	 */
	void exitColumnName(DMLStatementParser.ColumnNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#indexName}.
	 * @param ctx the parse tree
	 */
	void enterIndexName(DMLStatementParser.IndexNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#indexName}.
	 * @param ctx the parse tree
	 */
	void exitIndexName(DMLStatementParser.IndexNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#userName}.
	 * @param ctx the parse tree
	 */
	void enterUserName(DMLStatementParser.UserNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#userName}.
	 * @param ctx the parse tree
	 */
	void exitUserName(DMLStatementParser.UserNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#eventName}.
	 * @param ctx the parse tree
	 */
	void enterEventName(DMLStatementParser.EventNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#eventName}.
	 * @param ctx the parse tree
	 */
	void exitEventName(DMLStatementParser.EventNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#serverName}.
	 * @param ctx the parse tree
	 */
	void enterServerName(DMLStatementParser.ServerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#serverName}.
	 * @param ctx the parse tree
	 */
	void exitServerName(DMLStatementParser.ServerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#wrapperName}.
	 * @param ctx the parse tree
	 */
	void enterWrapperName(DMLStatementParser.WrapperNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#wrapperName}.
	 * @param ctx the parse tree
	 */
	void exitWrapperName(DMLStatementParser.WrapperNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(DMLStatementParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(DMLStatementParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#viewName}.
	 * @param ctx the parse tree
	 */
	void enterViewName(DMLStatementParser.ViewNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#viewName}.
	 * @param ctx the parse tree
	 */
	void exitViewName(DMLStatementParser.ViewNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#owner}.
	 * @param ctx the parse tree
	 */
	void enterOwner(DMLStatementParser.OwnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#owner}.
	 * @param ctx the parse tree
	 */
	void exitOwner(DMLStatementParser.OwnerContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(DMLStatementParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(DMLStatementParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#tableNames}.
	 * @param ctx the parse tree
	 */
	void enterTableNames(DMLStatementParser.TableNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#tableNames}.
	 * @param ctx the parse tree
	 */
	void exitTableNames(DMLStatementParser.TableNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#columnNames}.
	 * @param ctx the parse tree
	 */
	void enterColumnNames(DMLStatementParser.ColumnNamesContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#columnNames}.
	 * @param ctx the parse tree
	 */
	void exitColumnNames(DMLStatementParser.ColumnNamesContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#groupName}.
	 * @param ctx the parse tree
	 */
	void enterGroupName(DMLStatementParser.GroupNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#groupName}.
	 * @param ctx the parse tree
	 */
	void exitGroupName(DMLStatementParser.GroupNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#shardLibraryName}.
	 * @param ctx the parse tree
	 */
	void enterShardLibraryName(DMLStatementParser.ShardLibraryNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#shardLibraryName}.
	 * @param ctx the parse tree
	 */
	void exitShardLibraryName(DMLStatementParser.ShardLibraryNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#componentName}.
	 * @param ctx the parse tree
	 */
	void enterComponentName(DMLStatementParser.ComponentNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#componentName}.
	 * @param ctx the parse tree
	 */
	void exitComponentName(DMLStatementParser.ComponentNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#pluginName}.
	 * @param ctx the parse tree
	 */
	void enterPluginName(DMLStatementParser.PluginNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#pluginName}.
	 * @param ctx the parse tree
	 */
	void exitPluginName(DMLStatementParser.PluginNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#hostName}.
	 * @param ctx the parse tree
	 */
	void enterHostName(DMLStatementParser.HostNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#hostName}.
	 * @param ctx the parse tree
	 */
	void exitHostName(DMLStatementParser.HostNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#port}.
	 * @param ctx the parse tree
	 */
	void enterPort(DMLStatementParser.PortContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#port}.
	 * @param ctx the parse tree
	 */
	void exitPort(DMLStatementParser.PortContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#cloneInstance}.
	 * @param ctx the parse tree
	 */
	void enterCloneInstance(DMLStatementParser.CloneInstanceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#cloneInstance}.
	 * @param ctx the parse tree
	 */
	void exitCloneInstance(DMLStatementParser.CloneInstanceContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#cloneDir}.
	 * @param ctx the parse tree
	 */
	void enterCloneDir(DMLStatementParser.CloneDirContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#cloneDir}.
	 * @param ctx the parse tree
	 */
	void exitCloneDir(DMLStatementParser.CloneDirContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#channelName}.
	 * @param ctx the parse tree
	 */
	void enterChannelName(DMLStatementParser.ChannelNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#channelName}.
	 * @param ctx the parse tree
	 */
	void exitChannelName(DMLStatementParser.ChannelNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#logName}.
	 * @param ctx the parse tree
	 */
	void enterLogName(DMLStatementParser.LogNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#logName}.
	 * @param ctx the parse tree
	 */
	void exitLogName(DMLStatementParser.LogNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#roleName}.
	 * @param ctx the parse tree
	 */
	void enterRoleName(DMLStatementParser.RoleNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#roleName}.
	 * @param ctx the parse tree
	 */
	void exitRoleName(DMLStatementParser.RoleNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#engineName}.
	 * @param ctx the parse tree
	 */
	void enterEngineName(DMLStatementParser.EngineNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#engineName}.
	 * @param ctx the parse tree
	 */
	void exitEngineName(DMLStatementParser.EngineNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#triggerName}.
	 * @param ctx the parse tree
	 */
	void enterTriggerName(DMLStatementParser.TriggerNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#triggerName}.
	 * @param ctx the parse tree
	 */
	void exitTriggerName(DMLStatementParser.TriggerNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#triggerTime}.
	 * @param ctx the parse tree
	 */
	void enterTriggerTime(DMLStatementParser.TriggerTimeContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#triggerTime}.
	 * @param ctx the parse tree
	 */
	void exitTriggerTime(DMLStatementParser.TriggerTimeContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#userOrRole}.
	 * @param ctx the parse tree
	 */
	void enterUserOrRole(DMLStatementParser.UserOrRoleContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#userOrRole}.
	 * @param ctx the parse tree
	 */
	void exitUserOrRole(DMLStatementParser.UserOrRoleContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#partitionName}.
	 * @param ctx the parse tree
	 */
	void enterPartitionName(DMLStatementParser.PartitionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#partitionName}.
	 * @param ctx the parse tree
	 */
	void exitPartitionName(DMLStatementParser.PartitionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#triggerEvent}.
	 * @param ctx the parse tree
	 */
	void enterTriggerEvent(DMLStatementParser.TriggerEventContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#triggerEvent}.
	 * @param ctx the parse tree
	 */
	void exitTriggerEvent(DMLStatementParser.TriggerEventContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#triggerOrder}.
	 * @param ctx the parse tree
	 */
	void enterTriggerOrder(DMLStatementParser.TriggerOrderContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#triggerOrder}.
	 * @param ctx the parse tree
	 */
	void exitTriggerOrder(DMLStatementParser.TriggerOrderContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(DMLStatementParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(DMLStatementParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperator(DMLStatementParser.LogicalOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperator(DMLStatementParser.LogicalOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#notOperator_}.
	 * @param ctx the parse tree
	 */
	void enterNotOperator_(DMLStatementParser.NotOperator_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#notOperator_}.
	 * @param ctx the parse tree
	 */
	void exitNotOperator_(DMLStatementParser.NotOperator_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#booleanPrimary}.
	 * @param ctx the parse tree
	 */
	void enterBooleanPrimary(DMLStatementParser.BooleanPrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#booleanPrimary}.
	 * @param ctx the parse tree
	 */
	void exitBooleanPrimary(DMLStatementParser.BooleanPrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void enterComparisonOperator(DMLStatementParser.ComparisonOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void exitComparisonOperator(DMLStatementParser.ComparisonOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(DMLStatementParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(DMLStatementParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#bitExpr}.
	 * @param ctx the parse tree
	 */
	void enterBitExpr(DMLStatementParser.BitExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#bitExpr}.
	 * @param ctx the parse tree
	 */
	void exitBitExpr(DMLStatementParser.BitExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpr(DMLStatementParser.SimpleExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#simpleExpr}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpr(DMLStatementParser.SimpleExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(DMLStatementParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(DMLStatementParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunction(DMLStatementParser.AggregationFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#aggregationFunction}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunction(DMLStatementParser.AggregationFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 */
	void enterAggregationFunctionName(DMLStatementParser.AggregationFunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#aggregationFunctionName}.
	 * @param ctx the parse tree
	 */
	void exitAggregationFunctionName(DMLStatementParser.AggregationFunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#distinct}.
	 * @param ctx the parse tree
	 */
	void enterDistinct(DMLStatementParser.DistinctContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#distinct}.
	 * @param ctx the parse tree
	 */
	void exitDistinct(DMLStatementParser.DistinctContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#overClause_}.
	 * @param ctx the parse tree
	 */
	void enterOverClause_(DMLStatementParser.OverClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#overClause_}.
	 * @param ctx the parse tree
	 */
	void exitOverClause_(DMLStatementParser.OverClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#windowSpecification_}.
	 * @param ctx the parse tree
	 */
	void enterWindowSpecification_(DMLStatementParser.WindowSpecification_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#windowSpecification_}.
	 * @param ctx the parse tree
	 */
	void exitWindowSpecification_(DMLStatementParser.WindowSpecification_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#partitionClause_}.
	 * @param ctx the parse tree
	 */
	void enterPartitionClause_(DMLStatementParser.PartitionClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#partitionClause_}.
	 * @param ctx the parse tree
	 */
	void exitPartitionClause_(DMLStatementParser.PartitionClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#frameClause_}.
	 * @param ctx the parse tree
	 */
	void enterFrameClause_(DMLStatementParser.FrameClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#frameClause_}.
	 * @param ctx the parse tree
	 */
	void exitFrameClause_(DMLStatementParser.FrameClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#frameStart_}.
	 * @param ctx the parse tree
	 */
	void enterFrameStart_(DMLStatementParser.FrameStart_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#frameStart_}.
	 * @param ctx the parse tree
	 */
	void exitFrameStart_(DMLStatementParser.FrameStart_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#frameEnd_}.
	 * @param ctx the parse tree
	 */
	void enterFrameEnd_(DMLStatementParser.FrameEnd_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#frameEnd_}.
	 * @param ctx the parse tree
	 */
	void exitFrameEnd_(DMLStatementParser.FrameEnd_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#frameBetween_}.
	 * @param ctx the parse tree
	 */
	void enterFrameBetween_(DMLStatementParser.FrameBetween_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#frameBetween_}.
	 * @param ctx the parse tree
	 */
	void exitFrameBetween_(DMLStatementParser.FrameBetween_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#specialFunction}.
	 * @param ctx the parse tree
	 */
	void enterSpecialFunction(DMLStatementParser.SpecialFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#specialFunction}.
	 * @param ctx the parse tree
	 */
	void exitSpecialFunction(DMLStatementParser.SpecialFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 */
	void enterGroupConcatFunction(DMLStatementParser.GroupConcatFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#groupConcatFunction}.
	 * @param ctx the parse tree
	 */
	void exitGroupConcatFunction(DMLStatementParser.GroupConcatFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#windowFunction}.
	 * @param ctx the parse tree
	 */
	void enterWindowFunction(DMLStatementParser.WindowFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#windowFunction}.
	 * @param ctx the parse tree
	 */
	void exitWindowFunction(DMLStatementParser.WindowFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#castFunction}.
	 * @param ctx the parse tree
	 */
	void enterCastFunction(DMLStatementParser.CastFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#castFunction}.
	 * @param ctx the parse tree
	 */
	void exitCastFunction(DMLStatementParser.CastFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#convertFunction}.
	 * @param ctx the parse tree
	 */
	void enterConvertFunction(DMLStatementParser.ConvertFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#convertFunction}.
	 * @param ctx the parse tree
	 */
	void exitConvertFunction(DMLStatementParser.ConvertFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#positionFunction}.
	 * @param ctx the parse tree
	 */
	void enterPositionFunction(DMLStatementParser.PositionFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#positionFunction}.
	 * @param ctx the parse tree
	 */
	void exitPositionFunction(DMLStatementParser.PositionFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#substringFunction}.
	 * @param ctx the parse tree
	 */
	void enterSubstringFunction(DMLStatementParser.SubstringFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#substringFunction}.
	 * @param ctx the parse tree
	 */
	void exitSubstringFunction(DMLStatementParser.SubstringFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#extractFunction}.
	 * @param ctx the parse tree
	 */
	void enterExtractFunction(DMLStatementParser.ExtractFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#extractFunction}.
	 * @param ctx the parse tree
	 */
	void exitExtractFunction(DMLStatementParser.ExtractFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#charFunction}.
	 * @param ctx the parse tree
	 */
	void enterCharFunction(DMLStatementParser.CharFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#charFunction}.
	 * @param ctx the parse tree
	 */
	void exitCharFunction(DMLStatementParser.CharFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#trimFunction_}.
	 * @param ctx the parse tree
	 */
	void enterTrimFunction_(DMLStatementParser.TrimFunction_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#trimFunction_}.
	 * @param ctx the parse tree
	 */
	void exitTrimFunction_(DMLStatementParser.TrimFunction_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#valuesFunction_}.
	 * @param ctx the parse tree
	 */
	void enterValuesFunction_(DMLStatementParser.ValuesFunction_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#valuesFunction_}.
	 * @param ctx the parse tree
	 */
	void exitValuesFunction_(DMLStatementParser.ValuesFunction_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#weightStringFunction}.
	 * @param ctx the parse tree
	 */
	void enterWeightStringFunction(DMLStatementParser.WeightStringFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#weightStringFunction}.
	 * @param ctx the parse tree
	 */
	void exitWeightStringFunction(DMLStatementParser.WeightStringFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#levelClause_}.
	 * @param ctx the parse tree
	 */
	void enterLevelClause_(DMLStatementParser.LevelClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#levelClause_}.
	 * @param ctx the parse tree
	 */
	void exitLevelClause_(DMLStatementParser.LevelClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 */
	void enterLevelInWeightListElement_(DMLStatementParser.LevelInWeightListElement_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#levelInWeightListElement_}.
	 * @param ctx the parse tree
	 */
	void exitLevelInWeightListElement_(DMLStatementParser.LevelInWeightListElement_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#regularFunction}.
	 * @param ctx the parse tree
	 */
	void enterRegularFunction(DMLStatementParser.RegularFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#regularFunction}.
	 * @param ctx the parse tree
	 */
	void exitRegularFunction(DMLStatementParser.RegularFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 */
	void enterRegularFunctionName_(DMLStatementParser.RegularFunctionName_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#regularFunctionName_}.
	 * @param ctx the parse tree
	 */
	void exitRegularFunctionName_(DMLStatementParser.RegularFunctionName_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#matchExpression_}.
	 * @param ctx the parse tree
	 */
	void enterMatchExpression_(DMLStatementParser.MatchExpression_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#matchExpression_}.
	 * @param ctx the parse tree
	 */
	void exitMatchExpression_(DMLStatementParser.MatchExpression_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 */
	void enterMatchSearchModifier_(DMLStatementParser.MatchSearchModifier_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#matchSearchModifier_}.
	 * @param ctx the parse tree
	 */
	void exitMatchSearchModifier_(DMLStatementParser.MatchSearchModifier_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#caseExpression}.
	 * @param ctx the parse tree
	 */
	void enterCaseExpression(DMLStatementParser.CaseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#caseExpression}.
	 * @param ctx the parse tree
	 */
	void exitCaseExpression(DMLStatementParser.CaseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#caseWhen_}.
	 * @param ctx the parse tree
	 */
	void enterCaseWhen_(DMLStatementParser.CaseWhen_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#caseWhen_}.
	 * @param ctx the parse tree
	 */
	void exitCaseWhen_(DMLStatementParser.CaseWhen_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#caseElse_}.
	 * @param ctx the parse tree
	 */
	void enterCaseElse_(DMLStatementParser.CaseElse_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#caseElse_}.
	 * @param ctx the parse tree
	 */
	void exitCaseElse_(DMLStatementParser.CaseElse_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#intervalExpression}.
	 * @param ctx the parse tree
	 */
	void enterIntervalExpression(DMLStatementParser.IntervalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#intervalExpression}.
	 * @param ctx the parse tree
	 */
	void exitIntervalExpression(DMLStatementParser.IntervalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#intervalUnit_}.
	 * @param ctx the parse tree
	 */
	void enterIntervalUnit_(DMLStatementParser.IntervalUnit_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#intervalUnit_}.
	 * @param ctx the parse tree
	 */
	void exitIntervalUnit_(DMLStatementParser.IntervalUnit_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void enterOrderByClause(DMLStatementParser.OrderByClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#orderByClause}.
	 * @param ctx the parse tree
	 */
	void exitOrderByClause(DMLStatementParser.OrderByClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void enterOrderByItem(DMLStatementParser.OrderByItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#orderByItem}.
	 * @param ctx the parse tree
	 */
	void exitOrderByItem(DMLStatementParser.OrderByItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterDataType(DMLStatementParser.DataTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitDataType(DMLStatementParser.DataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#dataTypeName}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeName(DMLStatementParser.DataTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#dataTypeName}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeName(DMLStatementParser.DataTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#dataTypeLength}.
	 * @param ctx the parse tree
	 */
	void enterDataTypeLength(DMLStatementParser.DataTypeLengthContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#dataTypeLength}.
	 * @param ctx the parse tree
	 */
	void exitDataTypeLength(DMLStatementParser.DataTypeLengthContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#collectionOptions}.
	 * @param ctx the parse tree
	 */
	void enterCollectionOptions(DMLStatementParser.CollectionOptionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#collectionOptions}.
	 * @param ctx the parse tree
	 */
	void exitCollectionOptions(DMLStatementParser.CollectionOptionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#characterSet_}.
	 * @param ctx the parse tree
	 */
	void enterCharacterSet_(DMLStatementParser.CharacterSet_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#characterSet_}.
	 * @param ctx the parse tree
	 */
	void exitCharacterSet_(DMLStatementParser.CharacterSet_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#collateClause_}.
	 * @param ctx the parse tree
	 */
	void enterCollateClause_(DMLStatementParser.CollateClause_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#collateClause_}.
	 * @param ctx the parse tree
	 */
	void exitCollateClause_(DMLStatementParser.CollateClause_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 */
	void enterIgnoredIdentifier_(DMLStatementParser.IgnoredIdentifier_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#ignoredIdentifier_}.
	 * @param ctx the parse tree
	 */
	void exitIgnoredIdentifier_(DMLStatementParser.IgnoredIdentifier_Context ctx);
	/**
	 * Enter a parse tree produced by {@link DMLStatementParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 */
	void enterIgnoredIdentifiers_(DMLStatementParser.IgnoredIdentifiers_Context ctx);
	/**
	 * Exit a parse tree produced by {@link DMLStatementParser#ignoredIdentifiers_}.
	 * @param ctx the parse tree
	 */
	void exitIgnoredIdentifiers_(DMLStatementParser.IgnoredIdentifiers_Context ctx);
}