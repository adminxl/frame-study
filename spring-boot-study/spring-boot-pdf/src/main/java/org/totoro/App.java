package org.totoro;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws DocumentException, IOException {

//        test();

// step 1
        Document document = new Document();
// step 2
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("/Users/daocr/IdeaProjects/study/frame-study/spring-boot-study/spring-boot-pdf/src/main/java/org/totoro/test.pdf"));
// step 3
        pdfWriter.setStrictImageSequence(true);
        document.open();
// step 4

        Image image = Image.getInstance(new URL("http://www.wubizi.net/tuku/biao/xsj.jpg"));

        image.scaleToFit(document.getPageSize().getWidth(), document.getPageSize().getHeight());

        document.add(image);
        document.add(image);
        document.add(image);
        document.add(image);
        document.add(image);
// step 5
        document.close();


    }


    private static void test() {
        File dir = new File("/Users/daocr/IdeaProjects/study/自考树202008");


        String regex = "^[0-9]{1,2}";

        // 创建 Pattern 对象
        Pattern pattern = Pattern.compile(regex);

        File[] files = dir.listFiles();

        ArrayList<PdfFile> pdfFiles = new ArrayList<>();

        for (File course : files) {

            if (course.isDirectory()) {
                PdfFile pdfFile = new PdfFile();
                pdfFile.setFileName(course.getName());

                List<File> chapters = Arrays.asList(course.listFiles());
                chapters.sort(Comparator.comparing(File::getName));

                ArrayList<PdfFile.Chapter> chaptersList = new ArrayList<>();

                for (File chapterDir : chapters) {
                    String name = chapterDir.getName();

                    PdfFile.Chapter chapter1 = new PdfFile.Chapter();
                    chapter1.setChapterName(name);

                    File[] chapterImgDir = chapterDir.listFiles();


                    if (chapterImgDir != null) {
                        for (File chapterImgFile : chapterImgDir) {

                            if (chapterImgFile.isDirectory()) {
                                if ("图片".equals(chapterImgFile.getName())) {


                                    File[] files1 = chapterImgFile.listFiles();

                                    List<File> collect = Arrays.asList(files1).stream().sorted(new Comparator<File>() {
                                        @Override
                                        public int compare(File o1, File o2) {
                                            Matcher matcher1 = pattern.matcher(o1.getName());
                                            Matcher matcher2 = pattern.matcher(o2.getName());
                                            matcher1.find();
                                            matcher2.find();

                                            Integer file1 = Integer.valueOf(matcher1.group(0));
                                            Integer file2 = Integer.valueOf(matcher2.group(0));

                                            return file1.compareTo(file2);
                                        }
                                    }).collect(Collectors.toList());
                                    chapter1.setImg(collect);
                                    break;
                                }
                            }
                        }
                        chaptersList.add(chapter1);
                    }
                }

                pdfFile.setChapters(chaptersList.stream().filter(e -> e.getImg() != null).collect(Collectors.toList()));
                pdfFiles.add(pdfFile);
            }
        }


        System.out.println();
    }

    public static class PdfFile {

        private String fileName;

        private List<Chapter> chapters;

        public String getFileName() {
            return fileName;
        }

        public PdfFile setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public List<Chapter> getChapters() {
            return chapters;
        }

        public PdfFile setChapters(List<Chapter> chapters) {
            this.chapters = chapters;
            return this;
        }

        public static class Chapter {
            /**
             * 章节名称
             */
            private String chapterName;
            private List<File> img;

            public String getChapterName() {
                return chapterName;
            }

            public Chapter setChapterName(String chapterName) {
                this.chapterName = chapterName;
                return this;
            }

            public List<File> getImg() {
                return img;
            }

            public Chapter setImg(List<File> img) {
                this.img = img;
                return this;
            }
        }
    }
}
