package org.totoro.control;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("limit")

public class TestControl {

    @RequestMapping("/method1")
    public String method1() throws InterruptedException {

        Thread.sleep(100);
        return "返回成功！";
    }


    @GetMapping("/method2")
    public Object method2(){

        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

        objectObjectHashMap.put("code",500);

        return objectObjectHashMap;

    }
}
