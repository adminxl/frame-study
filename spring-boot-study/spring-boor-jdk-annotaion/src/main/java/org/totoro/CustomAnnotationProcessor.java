package org.totoro;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.Set;

/**
 *
 */
@AutoService(Processor.class)
public class CustomAnnotationProcessor extends AbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {


        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "111!");

        throw new RuntimeException();

    }


    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);

        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "2222!");
        throw new RuntimeException();

    }

    protected CustomAnnotationProcessor() {
        super();
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "3333!");
        throw new RuntimeException();
    }

    @Override
    public Set<String> getSupportedOptions() {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "444!");
//        return super.getSupportedOptions();

        throw new RuntimeException();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "555!");
        return super.getSupportedAnnotationTypes();
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "666!");
//        return SourceVersion.latest();
        throw new RuntimeException();
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "777!");
//        return super.getCompletions(element, annotation, member, userText);
        throw new RuntimeException();
    }

    @Override
    protected synchronized boolean isInitialized() {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "888!");
//        return super.isInitialized();
        throw new RuntimeException();
    }
}
