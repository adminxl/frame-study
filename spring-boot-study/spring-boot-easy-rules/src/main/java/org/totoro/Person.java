package org.totoro;

public class Person {

    private String name;

    private boolean adult;

    private int age;

    private ExtInfo extInfo;


    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isAdult() {
        return adult;
    }

    public Person setAdult(boolean adult) {
        this.adult = adult;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }

    public ExtInfo getExtInfo() {
        return extInfo;
    }

    public Person setExtInfo(ExtInfo extInfo) {
        this.extInfo = extInfo;
        return this;
    }

    public static class ExtInfo {

        private String email;

    }
}