package org.totoro;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.RulesEngineParameters;

/**
 * 参考:http://tech.dianwoda.com/2019/06/05/gui-ze-yin-qing-easy-rulejie-shao/
 *
 * 类似产品:
 * https://github.com/alibaba/QLExpress
 * https://github.com/kiegroup/drools
 *
 */
public class App {
    public static void main(String[] args) {


        RulesEngineParameters parameters = new
                RulesEngineParameters();

        //只要匹配到第一条规则就跳过后面规则匹配
        parameters.skipOnFirstAppliedRule(true);

        RulesEngine rulesEngine = new DefaultRulesEngine(parameters);
        Facts facts = new Facts();

        Person person = new Person();
        person.setAge(11);
        person.setName("李四");

        facts.put("person", person);

        Rules rules = new Rules();

        rules.register(new PersonRule());

        rulesEngine.fire(rules, facts);

    }
}
