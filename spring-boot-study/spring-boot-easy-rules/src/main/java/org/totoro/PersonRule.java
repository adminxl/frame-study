package org.totoro;

import org.jeasy.rules.annotation.*;

@Rule(name = "personRule", description = " person rule ")
public class PersonRule {

    @Condition
    public boolean conditon1(@Fact("person") Person number) {

        return true;
    }


    @Action(order = 1)
    public void threeAction(@Fact("person") Person number) {
        System.out.println(number + " is three");
    }

    @Action(order = 2)
    public void threeAction2(@Fact("person") Person number) {
        System.out.println(number + " is three");
    }


    @Priority //优先级注解：return 数值越小，优先级越高
    public int getPriority() {
        return 1;
    }
}