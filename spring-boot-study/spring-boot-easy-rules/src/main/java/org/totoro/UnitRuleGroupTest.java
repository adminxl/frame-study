package org.totoro;

import org.jeasy.rules.support.UnitRuleGroup;

/**
 * 多个 条件
 *
 * @author daocr
 * @date 2019-08-07
 */
public class UnitRuleGroupTest extends UnitRuleGroup {

    public UnitRuleGroupTest(Object... rules) {

        for (Object rule : rules) {
            addRule(rule);
        }
    }

    @Override
    public int getPriority() {
        return super.getPriority();
    }
}
