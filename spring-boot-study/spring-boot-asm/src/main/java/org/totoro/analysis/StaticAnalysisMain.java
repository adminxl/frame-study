package org.totoro.analysis;

import org.totoro.AnalysisService;

import java.io.IOException;

/**
 * 静态扫描
 *
 * @author daocr
 * @date 2020/9/23
 */
public class StaticAnalysisMain {

    public static void main(String[] args) throws IOException {
        MyClassVisitor visitor = new MyClassVisitor(AnalysisService.class.getName());
        for (MethodInfo method : visitor.getMethods()) {
            System.out.println("Title: invokes of " + method.getClasz() + ":" + method.getMethod());
            for (MethodInfo invoke : method.getInvoke()) {
                if (!invoke.getClasz().startsWith("java.")) {
                    System.out.println(method.getClasz() + "->" + invoke.getClasz() + ":" + invoke.getMethod());
                }
            }
            System.out.println();
        }
    }
}
