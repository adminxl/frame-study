package org.totoro.analysis;

import org.objectweb.asm.Type;

import java.util.ArrayList;
import java.util.List;

public class MethodInfo {
    String cname;
    String mname;
    String descripter;
    List<MethodInfo> invokeInfo = new ArrayList<MethodInfo>();

    public MethodInfo(String cname, String mname, String descripter) {
        this.cname = cname;
        this.mname = mname;
        this.descripter = descripter;
    }

    public void addInvoke(MethodInfo invoke) {
        invokeInfo.add(invoke);
    }

    public List<MethodInfo> getInvoke() {
        return invokeInfo;
    }

    public String getClasz() {
        return Type.getType("L" + cname + ";").getClassName();
    }

    public String getMethod() {
        Type retType = Type.getReturnType(descripter);
        Type[] argTypes = Type.getArgumentTypes(descripter);
        StringBuilder sb = new StringBuilder();
        String split = "";
        for (Type argType : argTypes) {
            sb.append(split).append(argType.getClassName());
            split = ",";
        }
        return mname + "#(" + sb + ")" + retType.getClassName();
    }
}