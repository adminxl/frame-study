package org.totoro.analysisV2;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daocr
 * @date 2020/9/24
 */
@Data
public class MethodStack {

    private String methodName;

    private String className;

    private String descriptor;
    private List<MethodStack> visitMethod = new ArrayList<>();

    public MethodStack() {
    }

    public MethodStack(String className, String methodName, String descriptor) {
        this.className = className;
        this.methodName = methodName;
        this.descriptor = descriptor;
    }
}
