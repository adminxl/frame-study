package org.totoro.analysisV2;

import java.io.File;
import java.io.IOException;

/**
 * @author daocr
 * @date 2020/9/24
 */
public class Main {

    public static void main(String[] args) throws IOException {

        AnalysisClass load = LoadClass.load(File.class.getName(), 0, null, "getParentFile");

        System.out.println(load);

    }
}
