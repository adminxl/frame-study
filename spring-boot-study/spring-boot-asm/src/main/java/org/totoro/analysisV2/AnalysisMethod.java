package org.totoro.analysisV2;

import org.objectweb.asm.MethodVisitor;

/**
 * @author daocr
 * @date 2020/9/24
 */
public class AnalysisMethod extends MethodVisitor {


    private int api;
    private int level;

    private MethodStack methodStack;

    public AnalysisMethod(int api, MethodStack methodStack, int level) {
        super(api);
        this.methodStack = methodStack;
        this.api = api;
        this.level = level;
    }

    public AnalysisMethod(int api, MethodVisitor methodVisitor, MethodStack methodStack, int level) {
        super(api, methodVisitor);
        this.methodStack = methodStack;
        this.api = api;
        this.level = level;
    }


    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {

        LoadClass.load(owner, level, methodStack, name);

        super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
    }
}
