package org.totoro.analysisV2;

import lombok.extern.slf4j.Slf4j;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;

import java.io.IOException;

/**
 * @author daocr
 * @date 2020/9/24
 */
@Slf4j
public class LoadClass {

    public static AnalysisClass load(String className, int level, MethodStack parentMethodStack,String methodName) {
        try {
            AnalysisClass analysisClass = new AnalysisClass(Opcodes.ASM8, level, parentMethodStack,methodName);
            ClassReader cr = new ClassReader(className);
            cr.accept(analysisClass, ClassReader.SKIP_DEBUG);
            return analysisClass;
        } catch (IOException e) {
            log.info("加载失败", e);
        }

        return null;

    }
}
