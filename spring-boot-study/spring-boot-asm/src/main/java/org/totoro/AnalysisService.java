package org.totoro;

/**
 * @author daocr
 * @date 2020/9/23
 */
public class AnalysisService {

    private String name;

    public AnalysisService() {

    }

    public String method1(Integer age) throws Exception {
        return method2(age);
    }

    public String method2(Integer age) throws Exception {
        this.method3(age);
        return null;
    }

    public String method3(Integer age) throws Exception {
        return null;
    }

//    public static class InnerClass {
//
//    }
}
