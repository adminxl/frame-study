package org.totoro.producer.config;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author YHL
 * @version V1.0
 * @Description: 确认消息已经放入了队列（需要配置    publisher-confirms: true）
 * @date 2018-02-02
 */
@Component
public class ProducerAck {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @PostConstruct
    public void init() {


        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                System.out.println("setConfirmCallback   >>  ack：" + ack + "     cause: " + cause);
            }
        });

    }


}
