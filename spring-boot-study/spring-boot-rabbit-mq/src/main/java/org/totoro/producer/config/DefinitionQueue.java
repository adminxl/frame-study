package org.totoro.producer.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.totoro.QueueNams;

/**
 * @author YHL
 * @version V1.0
 * @Description: 定义队列
 * @date 2018-02-02
 */
@Configuration
public class DefinitionQueue {

    @Bean
    public Queue testQueue() {
        return new Queue(QueueNams.TEST_QUEUE);
    }

    @Bean
    public Queue testQueue2() {
        return new Queue(QueueNams.TEST_QUEUE2);
    }
}
