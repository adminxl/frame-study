package org.totoro.producer;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;
import org.totoro.producer.config.DefinitionQueue;

import java.util.Properties;

/**
 * Hello world!
 *
 * @author 阳辉龙
 */
@SpringBootApplication
@Import(DefinitionQueue.class)
public class Producer {
    public static void main(String[] args) throws InterruptedException {
        ConfigurableApplicationContext run = SpringApplication.run(Producer.class, args);

        RabbitAdmin rabbitAdmin = run.getBean(RabbitAdmin.class);

        String queueName = "test00031";

        /**
         * 动态添加队列
         */
        rabbitAdmin.declareQueue(new Queue(queueName));


        /**
         * 动态定义 交换机
         */

        String topicName = "pay.topic";

        rabbitAdmin.declareExchange(new TopicExchange(topicName, true, false));

        /**
         * 动态绑定队列
         *
         * 如果绑定的队列 不存在，则会报  notfind
         *
         */
        TopicExchange topicExchange = new TopicExchange(topicName);
        Binding with = BindingBuilder.bind(new Queue(queueName)).to(topicExchange).with("pay.callback.message");
        rabbitAdmin.declareBinding(with);

        /**
         * 获取队列 信息
         */
        Properties queueProperties = rabbitAdmin.getQueueProperties(queueName + "---");

        RabbitTemplate bean = run.getBean(RabbitTemplate.class);

        for (int i = 0; i < 10; i++) {

            bean.convertAndSend("yhl", "routingKey001", "阳辉龙");


//            bean.convertAndSend(queueNormalName, "内容：" + i);
//            bean.convertAndSend(QueueNams.TEST_QUEUE, "内容：" + i);
//            bean.convertAndSend(QueueNams.TEST_QUEUE2, "内容：" + i);
            Thread.sleep(50);
        }
    }

    public void create(RabbitAdmin rabbitAdmin) {

        //创建exchange
//        rabbitAdmin.declareExchange(new DirectExchange("log.direct.exchange", true,false));
//        rabbitAdmin.declareExchange(new TopicExchange("log.topic.exchange", true,false));
//        rabbitAdmin.declareExchange(new FanoutExchange("log.fanout.exchange", true,false));
//        rabbitAdmin.declareExchange(new HeadersExchange("log.headers.exchange", true,false));

        //删除exchange
//        rabbitAdmin.deleteExchange("log.headers.exchange");

        //创建队列
//        rabbitAdmin.declareQueue(new Queue("log.debug", true));
//        rabbitAdmin.declareQueue(new Queue("log.info", true));
//        rabbitAdmin.declareQueue(new Queue("log.error", true));

        //删除队列
//        rabbitAdmin.deleteQueue("log.error");

        //清空队列消息
//        rabbitAdmin.purgeQueue("log.info", false);

        //binding
//        Binding binding = new Binding("log.debug", Binding.DestinationType.QUEUE,"log.direct.exchange","debug", null);
//        Binding binding1 = new Binding("log.info", Binding.DestinationType.QUEUE,"log.direct.exchange","info", null);
//        rabbitAdmin.declareBinding(binding);
//        rabbitAdmin.declareBinding(binding1);

        //builder binding
//        rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("log.info")).to(new TopicExchange("log.topic.exchange")).with("info.#"));
//        rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("log.debug")).to(new TopicExchange("log.topic.exchange")).with("debug.#"));

//         rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("log.debug")).to(new FanoutExchange("log.fanout.exchange")));
//         rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("log.info")).to(new FanoutExchange("log.fanout.exchange")));


//         Map<String,Object> headers = new HashMap<String,Object>();
//         headers.put("type", 1);
//         headers.put("count", 10);
//         Map<String,Object> headers1 = new HashMap<String,Object>();
//        headers1.put("type", 2);
//        headers1.put("count", 10);
        //全部匹配
//         rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("log.debug")).to(new HeadersExchange("log.headers.exchange")).whereAll(headers).match());
        //匹配任意一个
//         rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("log.info")).to(new HeadersExchange("log.headers.exchange")).whereAny(headers1).match());

        //解绑
//        rabbitAdmin.removeBinding(BindingBuilder.bind(new Queue("log.debug")).to(new FanoutExchange("log.fanout.exchange")));

        //exchange和exchange的binging
//        rabbitAdmin.declareBinding(new Binding("log.info", Binding.DestinationType.EXCHANGE,"log.all","all",null));
//        rabbitAdmin.declareBinding(BindingBuilder.bind(new DirectExchange("log.all")).to(new DirectExchange("log.info")).with("all"));


    }
}
