package org.totoro.dead;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author YHL
 * @version V1.0
 * @Description: 死信队列 服务端
 * @date 2018-05-29
 */

@SpringBootApplication
public class DealQueueProducer {


    /**
     * 正常队列名称
     */
    public static String queueNormalName = "normal-queue-001";

    /**
     * 正常队列路由key
     */
    public static String routingKey = "normal-queue-routing-key";

    /**
     * 交换机
     */
    public static String topicName = "normal.queue.topic";

    /**
     * 死心队列名称
     */
    public static String dead_queue_name = "dead-queue";

    /**
     * 死信队列路由key
     */
    public static String dead_exchange_routing_key = "normal-queue-001-fail-routing-key";


    public static void main(String[] args) throws InterruptedException {

        ConfigurableApplicationContext run = SpringApplication.run(DealQueueProducer.class, args);

        RabbitAdmin rabbitAdmin = run.getBean(RabbitAdmin.class);

        /**
         * 动态添加队列
         */

        Map<String, Object> queueArgs = new HashMap<>();

        //设置死信交换机
        //设置死信routingKey
        queueArgs.put("x-dead-letter-exchange", topicName);
        queueArgs.put("x-dead-letter-routing-key", dead_exchange_routing_key);
        queueArgs.put("origin", "test");

        Queue queue = new Queue(queueNormalName, true, false, false, queueArgs);

        rabbitAdmin.declareQueue(queue);


        /**
         * 动态定义 交换机
         */
        rabbitAdmin.declareExchange(new TopicExchange(topicName, true, false));

        /**
         *
         * 正常队列绑定到交换机上
         *
         *
         * 如果绑定的队列 不存在，则会报  notfind
         *
         */
        TopicExchange topicExchange = new TopicExchange(topicName);
        Binding with = BindingBuilder.bind(new Queue(queueNormalName)).to(topicExchange).with(routingKey);
        rabbitAdmin.declareBinding(with);

        /**
         * 把死信队列绑定到交换机上
         */

        Queue deadQueue = new Queue(dead_queue_name, true, false, false);

        rabbitAdmin.declareQueue(deadQueue);

        Binding dealWith = BindingBuilder.bind(new Queue(dead_queue_name, true)).to(topicExchange).with(dead_exchange_routing_key);
        rabbitAdmin.declareBinding(dealWith);

        RabbitTemplate bean = run.getBean(RabbitTemplate.class);

        for (int i = 0; i < 10; i++) {

            bean.convertAndSend(topicName, routingKey, "阳辉龙");

            Thread.sleep(50);
        }
        System.out.println("启动完成");

        Thread.sleep(Long.MAX_VALUE);

    }

}
