package org.totoro.consumer.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.totoro.QueueNams;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-02-02
 */
@Configuration
public class MyMessageListenerContainer {


    @Autowired
    private ConnectionFactory connectionFactory;

    @Autowired
    private ConsumerQueueRabbitListener consumerQueueRabbitListener;


    /**
     * 手动 ack，确认消息
     *
     * @return
     */
    @Bean
    public SimpleMessageListenerContainer loginFactory() {

        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer(connectionFactory);
        // 队列名称
        simpleMessageListenerContainer.setQueueNames(QueueNams.TEST_QUEUE);
        // 是否暴露注册通道
        simpleMessageListenerContainer.setExposeListenerChannel(true);
        // 设置ack 模式  为手动确认
        simpleMessageListenerContainer.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        // 设置监听器
        simpleMessageListenerContainer.setMessageListener(consumerQueueRabbitListener);

        return simpleMessageListenerContainer;
    }


}
