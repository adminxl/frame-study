package org.totoro.consumer.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author YHL
 * @version V1.0
 * @Description: 注解实现，监听
 * @date 2018-02-05
 */
@Component
public class AnnotationQueueListener {


    @RabbitListener(queues = "rabbit-mq-test-2")
    public void listener(Message message) {
        System.out.println("rabbit-mq-test-2  >" + message);
    }

}
