package org.totoro.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.totoro.producer.config.DefinitionQueue;

/**
 * @author yhl
 * @Description: 消费者
 * @date：2017/9/30 0030
 * @tiem：15:10
 */
@SpringBootApplication
@Import(DefinitionQueue.class)
public class Consumer {

    public static void main(String[] args) {
        SpringApplication.run(Consumer.class, args);
    }
}
