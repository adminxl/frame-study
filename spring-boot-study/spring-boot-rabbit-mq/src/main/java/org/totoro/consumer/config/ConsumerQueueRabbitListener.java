package org.totoro.consumer.config;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Service;

/**
 * @author YHL
 * @version V1.0
 * @Description: 手动 ack 确认消息
 * @date 2018-02-02
 */
@Service
public class ConsumerQueueRabbitListener implements ChannelAwareMessageListener {

    public ConsumerQueueRabbitListener() {
        System.out.println("ConsumerRabbitListener init ");
    }


    /**
     * 确认消息，已经被消费
     * <p>
     * deliveryTag:该消息的index
     * multiple：是否批量.true:将一次性ack所有小于deliveryTag的消息。
     *
     * @see Channel#basicAck(long, boolean)
     * <p>
     * 消息消费失败，重新回到队列（可批量拒绝）
     * <p>
     * <p>
     * <p>
     * <p>
     * deliveryTag:该消息的index
     * multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
     * requeue：被拒绝的是否重新入队列
     * @see Channel#basicNack(long, boolean, boolean)
     * <p>
     * <p>
     * <p>
     * <p>
     * 消息消费失败，重新回到队列
     * deliveryTag:该消息的index
     * requeue：被拒绝的是否重新入队列
     * @see Channel#basicReject(long, boolean)
     * <p>
     * <p>
     * 绑定消息到新队列中
     * @see Channel#exchangeBind(String, String, String)
     */
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {

        System.out.println("onMessage +" + new String(message.getBody()));

        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);

//        channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
//        channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
    }
}
