package org.totoro;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * @author yhl
 */
public class ImageUtils {
    /**
     * 将网络图片进行Base64位编码
     *
     * @param imageUrl 图片的url路径，如http://.....xx.jpg
     * @return
     */
    public static String encodeImgageToBase64(URL imageUrl) throws IOException {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理

        BufferedImage bufferedImage = ImageIO.read(imageUrl);

        return encode(bufferedImage);
    }

    /**
     * 将本地图片进行Base64位编码
     *
     * @param imageFile 图片的url路径，如http://.....xx.jpg
     * @return
     */
    public static String encodeImgageToBase64(File imageFile) throws IOException {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理

        BufferedImage bufferedImage = ImageIO.read(imageFile);

        return encode(bufferedImage);
    }

    /**
     * 文件编码成base64
     *
     * @param bufferedImage
     * @return
     * @throws IOException
     */
    public static String encode(BufferedImage bufferedImage) throws IOException {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpg", outputStream);
        // 对字节数组Base64编码
        BASE64Encoder encoder = new BASE64Encoder();

        // 返回Base64编码过的字节数组字符串
        return encoder.encode(outputStream.toByteArray());
    }


    /**
     * 将Base64位编码的图片进行解码，并保存到指定目录
     *
     * @param base64
     * @param path
     * @param imgName
     * @throws IOException
     */
    public static void decodeBase64ToImage(String base64, String path,
                                           String imgName) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();

        FileOutputStream write = new FileOutputStream(new File(path + imgName));
        byte[] decoderBytes = decoder.decodeBuffer(base64);
        write.write(decoderBytes);
        write.close();
    }
}
 