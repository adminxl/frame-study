package org.totoro;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.List;

/**
 * Hello world!
 */
public class App {


    public static void main(String[] args) throws IOException, TranscoderException {

        String svgFile = getSvgFile().replace("${userName}","追儿");

        // 读取svg 文件
        TranscoderInput transcoderInput = new TranscoderInput(new StringReader(svgFile));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();


        PNGTranscoder t = new PNGTranscoder();
        // 合成图片写入内存
        TranscoderOutput output = new TranscoderOutput(outputStream);
        // 转化图片
        t.transcode(transcoderInput, output);
        outputStream.writeTo(new FileOutputStream(new File("00.jpg")));

        outputStream.flush();
        outputStream.close();


    }

    private static String getSvgFile() throws IOException {


        ClassPathResource classPathResource = new ClassPathResource("svg/index.svg");

        InputStream inputStream = classPathResource.getInputStream();

        List<String> list = IOUtils.readLines(inputStream);

        StringBuilder stringBuilder = new StringBuilder();

        list.forEach(e -> stringBuilder.append(e));

        return stringBuilder.toString();

    }


}
