package org.totoro.querydsl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.totoro.StartUpApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StartUpApplication.class)
public class UserDslServiceTest {


    @Autowired
    private UserDslService userDslService;


    /**
     * 基础查询
     */
    @Test
    public void basicsQuery() {

        userDslService.joinQuery();

    }

    @Test
    public void groupQuery() {
        userDslService.groupQuery();
    }

    @Test
    public void caseWhenQuery(){
        userDslService.caseWhenQuery();
    }
}