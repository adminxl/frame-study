

[TOC]



# 基本映射关系



## @ManyToOne

join 时带条件

```java
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumnsOrFormulas(value={
            @JoinColumnOrFormula(column=@JoinColumn(name ="respondent_id", referencedColumnName ="respondent_id", insertable = false, updatable = false)) ,
            @JoinColumnOrFormula(column=@JoinColumn(name = "user_phone", referencedColumnName = "mobile", insertable
                    = false, updatable = false)),
            @JoinColumnOrFormula(formula=@JoinFormula(value="'1'", referencedColumnName = "status"))
    })
```



