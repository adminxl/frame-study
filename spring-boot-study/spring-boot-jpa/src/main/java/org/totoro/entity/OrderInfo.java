package org.totoro.entity;

import javax.persistence.*;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-08-07
 */
@Entity
@Table(name = "order_info")
public class OrderInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OrderInfo{");
        sb.append("id=").append(id);
        sb.append('}');
        return sb.toString();
    }
}
