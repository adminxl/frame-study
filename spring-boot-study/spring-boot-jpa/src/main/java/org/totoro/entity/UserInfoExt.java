package org.totoro.entity;

import javax.persistence.*;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-08-07
 */

@Entity
@Table(name = "user_info_ext")
public class UserInfoExt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "addr")
    private String addr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }


}
