package org.totoro.config;

import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

/**
 * @author YHL
 * @version V1.0
 * @Description: 多租户
 * @date 2018-08-13
 * @see https://www.baeldung.com/hibernate-5-multitenancy
 * @see http://www.codeweblog.com/%E5%A4%9A%E7%A7%9F%E6%88%B7%E6%95%B0%E6%8D%AE%E6%9E%B6%E6%9E%84%E4%BB%A5%E5%8F%8Ahibernate%E6%94%AF%E6%8C%81-multi-tenantdataarchitecture/
 * @see https://github.com/nklkarthi/java-tutorials/blob/05f30e1492439d05e22dac6a68d8ece6b3eab6a2/hibernate5/src/main/java/com/baeldung/hibernate/App.java
 */
public class SchemaMultiTenantConnectionProvider extends AbstractMultiTenantConnectionProvider {
    @Override
    protected ConnectionProvider getAnyConnectionProvider() {

        return null;
    }

    @Override
    protected ConnectionProvider selectConnectionProvider(String tenantIdentifier) {




        return null;
    }
}
