package org.totoro.config;

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.Configurable;
import org.hibernate.service.spi.Stoppable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-08-13
 */
public class MyConnectionProvider implements ConnectionProvider, Configurable, Stoppable {
    @Override
    public Connection getConnection() throws SQLException {
        return null;
    }

    @Override
    public void closeConnection(Connection conn) throws SQLException {

    }

    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    @Override
    public void configure(Map configurationValues) {

    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isUnwrappableAs(Class unwrapType) {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> unwrapType) {
        return null;
    }
}
