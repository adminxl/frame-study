create table order_info
(
    id int auto_increment,
    constraint order_info_pk
        primary key (id)
)
    comment '订单表';


-- auto-generated definition
create table user_info
(
    id          int auto_increment
        primary key,
    name        varchar(20) null,
    email       varchar(20) null,
    password    varchar(20) null,
    user_ext_id int         null
);


create table user_info_ext
(
    id   int auto_increment,
    addr varchar(20) null,
    constraint user_info_ext_pk
        primary key (id)
);

