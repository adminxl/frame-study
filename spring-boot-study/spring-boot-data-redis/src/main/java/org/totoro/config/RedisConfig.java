package org.totoro.config;


import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

/**
 * @author daocr
 * @date 2019-04-29
 */

@Configuration
public class RedisConfig {


    @ConditionalOnMissingBean
    public JedisConnectionFactory getJedisConnectionFactory(RedisProperties redisProperties) {


        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();


        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();

        return jedisConnectionFactory;
    }

}
