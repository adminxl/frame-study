package org.totoro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Hello world!
 */
@SpringBootApplication

@EnableCaching
public class App {
    public static void main(String[] args) {

        ConfigurableApplicationContext applicationContext = SpringApplication.run(App.class, args);

        RedisTemplate redisTemplate = applicationContext.getBean(StringRedisTemplate.class);


        for (int i = 0; i < 100; i++) {
            long a1 = redisTemplate.opsForValue().increment("a1", 1).longValue();
            System.out.println(a1);
        }


    }
}
