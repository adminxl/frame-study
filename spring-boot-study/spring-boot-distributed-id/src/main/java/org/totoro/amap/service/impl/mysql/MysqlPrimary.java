package org.totoro.amap.service.impl.mysql;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.totoro.amap.service.impl.mysql.entity.GenerateKeyEntity;
import org.totoro.amap.service.impl.mysql.repository.GenerateKeyEntityRepository;
import org.totoro.amap.service.DistributedGenerateService;


@Service
@Order(1)
public class MysqlPrimary implements DistributedGenerateService {

    @Autowired
    private JPAQueryFactory jpaQueryFactory;

    @Autowired

    private GenerateKeyEntityRepository generateKeyEntityRepository;

    @Transactional
    @Override
    public String generateKey() {

        GenerateKeyEntity generateKeyEntity = new GenerateKeyEntity();

        generateKeyEntityRepository.save(generateKeyEntity);

        Long id = generateKeyEntity.id;

        return String.valueOf(id);
    }
}
