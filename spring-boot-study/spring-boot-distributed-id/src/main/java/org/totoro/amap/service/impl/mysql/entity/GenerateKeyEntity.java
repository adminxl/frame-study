package org.totoro.amap.service.impl.mysql.entity;


import javax.persistence.*;

@Entity
@Table(name = "t_generate_key")
public class GenerateKeyEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

}
