package org.totoro.amap.service.impl.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.totoro.amap.service.DistributedGenerateService;

@Service
@Order(2)
public class Redis implements DistributedGenerateService {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public String generateKey() {

        Long increment = redisTemplate.opsForValue().increment("distributed:generate:service", 1000);

        return String.valueOf(increment);
    }
}
