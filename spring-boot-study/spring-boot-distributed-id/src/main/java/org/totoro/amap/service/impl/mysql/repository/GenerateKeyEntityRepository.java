package org.totoro.amap.service.impl.mysql.repository;

import org.springframework.data.repository.CrudRepository;
import org.totoro.amap.service.impl.mysql.entity.GenerateKeyEntity;

public interface GenerateKeyEntityRepository extends CrudRepository<GenerateKeyEntity, Long> {
}
