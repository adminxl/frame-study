package org.totoro.amap.service;

/**
 * 抽象 分布式 id 生成 服务
 */
public interface DistributedGenerateService {

    /**
     * @return
     */
    public String generateKey();

}
