package org.totoro.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.totoro.config.KafkaConfig;

@Component
public class KafkaConsumer {


    @KafkaListener(topics = {KafkaConfig.topic_name})
    public void listen(ConsumerRecord<?, ?> record) {
        System.out.printf("offset = %d,key =%s,value=%s\n", record.offset(), record.key(), record.value());
    }

}