package org.totoro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Hello world!
 */

/*
扩展知识:Spring Session提供了3种方式存储session的方式。分别对应3各种注
@EnableRedisHttpSession-存放在缓存redis
@EnableMongoHttpSession-存放在Nosql的MongoDB
@EnableJdbcHttpSession-存放数据库

参考：
http://www.jianshu.com/p/e4191997da56
http://www.jianshu.com/p/ece9ac8e2f81


原理：

1、用过滤器（SessionRepositoryFilter）拦截所以请求
2、在拦截器调用链中，设置自己的 HttpServletRequestWrapper   HttpServletResponseWrapper 的实现类


 */
@EnableRedisHttpSession
@SpringBootApplication
@Controller
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping("testSession")
    public void testSession(HttpSession session) {

        session.setAttribute("123132", "sdfs");
        System.out.println("请求成功!");
    }
}
