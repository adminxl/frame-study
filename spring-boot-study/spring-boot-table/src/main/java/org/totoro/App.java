package org.totoro;

import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        String[] students = {"小明", "李雷", "小二"};
        double[] scores = {90.1123123213213123, 84.3, 99.7};
        Table table = Table.create("学生分数统计表")
                .addColumns(StringColumn.create("姓名", students),
                        DoubleColumn.create("分数", scores),   DoubleColumn.create("分数sdfasdf", scores),   DoubleColumn.create("分数312312", scores));
        System.out.println(table.print());
    }
}
