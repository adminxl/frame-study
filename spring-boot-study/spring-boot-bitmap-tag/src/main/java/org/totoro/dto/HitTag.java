package org.totoro.dto;

import java.util.List;

/**
 * @author daocr
 * @date 2019-04-26
 */
public class HitTag {

    private List<Long> hitList;


    public HitTag() {
    }

    public HitTag(List<Long> hitList) {
        this.hitList = hitList;
    }

    public List<Long> getHitList() {
        return hitList;
    }

    public void setHitList(List<Long> hitList) {
        this.hitList = hitList;
    }
}
