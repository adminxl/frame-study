package org.totoro.dto;

import javax.validation.constraints.NotNull;

/**
 * @author daocr
 * @date 2019-04-26
 */
public class SingleTag {

    /**
     * 标签名称
     */
    @NotNull
    private String tagName;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

}
