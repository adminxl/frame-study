package org.totoro.dto;

import org.totoro.enums.Strategy;

import javax.validation.constraints.NotNull;


/**
 * @author daocr
 * @date 2019-04-26
 */
public class MultipleTag extends SingleTag {


    /**
     * 命中策略
     */
    @NotNull
    private Strategy strategy;


    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

}
