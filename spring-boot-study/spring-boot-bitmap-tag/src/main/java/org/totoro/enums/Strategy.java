package org.totoro.enums;


import com.googlecode.javaewah.datastructure.BitSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author daocr
 * @date 2019-04-26
 */
public enum Strategy {

    /**
     * 按位与（AND）
     *
     * <pre>
     *     0101
     * AND 0011
     *   = 0001
     * </pre>
     *
     * @param l
     * @param r
     * @return
     */
    eq {
        @Override
        public BitSet apply(BitSet l, BitSet r) {
            BitSet clone = l.clone();
            clone.and(r);
            return clone;
        }

        @Override
        public Integer sort() {
            return 1;
        }


    },
    /**
     * 按位异或（XOR）
     *
     * <pre>
     *     0101
     * XOR 0011
     *   = 0110
     * </pre>
     *
     * @param l
     * @param r
     * @return
     */
    not {
        @Override
        public BitSet apply(BitSet l, BitSet r) {
            BitSet clone = l.clone();
            clone.andNot(r);
            return clone;
        }

        @Override
        public Integer sort() {
            return 2;
        }


    },
    /**
     * 按位或（OR）
     * <pre>
     *    0101
     * OR 0011
     * =  0111
     * </pre>
     *
     * @param l
     * @param r
     * @return
     */
    or {
        @Override
        public BitSet apply(BitSet l, BitSet r) {
            BitSet clone = l.clone();
            clone.or(r);
            return clone;
        }

        @Override
        public Integer sort() {
            return 3;
        }

    };

    public static List<Long> conv(BitSet bitSet) {

        if (bitSet == null) {
            return Collections.emptyList();
        }

        List<Long> ids = new ArrayList<>();
        bitSet.forEach(e -> ids.add(Long.valueOf(e + "")));

        return ids;

    }


    /**
     * 开始运算
     *
     * @param l
     * @param r
     * @return
     */
    public abstract BitSet apply(BitSet l, BitSet r);


    /**
     * 优先级排序
     *
     * @return
     */
    public abstract Integer sort();


}
