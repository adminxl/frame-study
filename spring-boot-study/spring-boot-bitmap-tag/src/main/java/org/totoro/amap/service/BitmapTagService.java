package org.totoro.amap.service;

import org.totoro.dto.HitTag;
import org.totoro.dto.MultipleTag;

import java.util.List;

/**
 * @author daocr
 * @date 2019-04-26
 */
public interface BitmapTagService {

    /**
     * 标签搜索
     *
     * @param multipleTag
     * @param ids
     * @return
     */
    HitTag match(MultipleTag multipleTag, List<Long> ids);


    /**
     * 多条件查询
     * 过滤优先级：eq > not > or
     *
     * @param multipleTag
     * @param ids
     * @return
     */
    HitTag match(List<MultipleTag> multipleTag, List<Long> ids);


}
