package org.totoro;

import java.util.BitSet;

/**
 * 参考：https://www.cnblogs.com/fonxian/p/10937882.html
 * <p>
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        BitSet bitSet = new BitSet();

        bitSet.stream().forEach(System.out::println);

        System.out.println(bitSet);

    }
}
