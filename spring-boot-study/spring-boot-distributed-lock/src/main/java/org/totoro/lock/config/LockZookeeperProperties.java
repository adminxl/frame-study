package org.totoro.lock.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author YHL
 * @version V1.0
 * @Description: zk 配置
 * @date 2018-12-03
 */
@ConfigurationProperties(prefix = Constant.Zookeeper.LOCK_ZK_PREFIX)
@Component
public class LockZookeeperProperties {

    /**
     * 连接串
     */
    private String connectString = "127.0.0.1:2181";

    /**
     * 超时时间
     */
    private Integer sessionTimeout = 5000;

    /**
     * 根节点名称
     */
    private String rootTreeName = Constant.Zookeeper.DISTRIBUTED_LOCK_TREE_ROOT;


    public String getConnectString() {
        return connectString;
    }

    public LockZookeeperProperties setConnectString(String connectString) {
        this.connectString = connectString;
        return this;
    }

    public Integer getSessionTimeout() {
        return sessionTimeout;
    }

    public LockZookeeperProperties setSessionTimeout(Integer sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
        return this;
    }

    public String getRootTreeName() {
        return rootTreeName;
    }

    public LockZookeeperProperties setRootTreeName(String rootTreeName) {
        this.rootTreeName = rootTreeName;
        return this;
    }

}
