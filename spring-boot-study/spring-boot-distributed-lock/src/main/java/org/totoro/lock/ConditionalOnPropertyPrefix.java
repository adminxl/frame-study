
package org.totoro.lock;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;


/**
 * @author yhl
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Conditional(OnPropertySourceCondition.class)
public @interface ConditionalOnPropertyPrefix {

    String prefix();

}
