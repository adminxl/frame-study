package org.totoro.lock.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;

/**
 * @author YHL
 * @version V1.0
 * @Description: redis 客户端
 * @date 2018-11-30
 */
public class RedisClient {

    @Autowired(required = false)
    @Qualifier(value = "lockRedisTemplate")
    private RedisTemplate lockRedisTemplate;


    @Autowired(required = false)
    private RedisTemplate defaultRedisTemplate;

    private RedisTemplate redisTemplate;

    @PostConstruct
    public void init() {

        if (lockRedisTemplate != null) {
            this.redisTemplate = lockRedisTemplate;
            return;
        }

        if (defaultRedisTemplate != null) {
            this.redisTemplate = defaultRedisTemplate;
        }

        throw new IllegalStateException("请配置 redis 信息");

    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }
}
