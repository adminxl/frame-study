package org.totoro.lock.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.totoro.lock.DistributedReentrantLockAbstract;
import org.totoro.lock.config.RedisClient;

/**
 * @author YHL
 * @version V1.0
 * @Description: 分布式锁redis 实现
 * @date 2018-11-27
 */
@Import(RedisClient.class)
public class RedisDistributedReentrantLock extends DistributedReentrantLockAbstract {

    @Autowired
    private RedisClient redisClient;


}
