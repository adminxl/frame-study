package org.totoro.lock;

import org.apache.zookeeper.KeeperException;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * @author YHL
 * @version V1.0
 * @Description: 分布式锁
 * @date 2018-11-27
 */
public interface DistributedReentrantLock extends Lock {


    /**
     * 锁定
     *
     * @param lockId 资源 id
     */
    void lock(String lockId) throws KeeperException, InterruptedException, UnsupportedEncodingException;


    /**
     * 响应中断锁定
     *
     * @param lockId 资源 id
     * @throws InterruptedException
     */
    void lockInterruptibly(String lockId) throws InterruptedException;


    /**
     * 尝试获取锁
     *
     * @param lockId 资源 id
     * @return
     */
    boolean tryLock(String lockId);


    /**
     * 尝试获取锁 等待时间
     *
     * @param lockId 资源id
     * @param time   时间
     * @param unit   单位
     * @return
     * @throws InterruptedException
     */
    boolean tryLock(String lockId, long time, TimeUnit unit) throws InterruptedException;


    /**
     * 解锁
     *
     * @param lockId 资源 id
     */
    void unlock(String lockId);

    /**
     * 获取lock id
     *
     * @return
     */
    String getLockId();
}
