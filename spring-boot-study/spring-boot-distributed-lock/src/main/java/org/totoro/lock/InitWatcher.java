package org.totoro.lock;

import org.apache.zookeeper.Watcher;

/**
 * @author YHL
 * @version V1.0
 * @Description: 初始化
 * @date 2018-12-03
 */
public interface InitWatcher extends Watcher {

}
