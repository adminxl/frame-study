package org.totoro.lock.config;

/**
 * @author YHL
 * @version V1.0
 * @Description: 常量
 * @date 2018-12-03
 */
public abstract class Constant {


    /**
     * redis 配置
     */
    public abstract static class Redis {

        /**
         * redis 配置前缀
         */
        public static final String LOCK_REDIS_PREFIX = "spring.lock.redis";

        /**
         * 客户端 bean 名称
         */
        public static final String LOCK_REDIS_CLIENT_BEAN_NAME = "lockRedisClient";
    }

    /**
     * zk 配置
     */
    public abstract static class Zookeeper {


        /**
         * 客户端 bean 名称
         */
        public static final String LOCK_ZOOKEEPER_CLIENT_BEAN_NAME = "lockZookeeperClient";

        /**
         * 配置前缀
         */
        public static final String LOCK_ZK_PREFIX = "spring.lock.zk";

        /**
         * 初始化 回调bean
         */
        public static final String ZK_INIT = "zkInit";
        /**
         * 根节点名称
         */
        public static final String DISTRIBUTED_LOCK_TREE_ROOT = "/distributed-lock/";
    }
}
