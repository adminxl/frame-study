package org.totoro.lock.config;

import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.totoro.lock.ConditionalOnPropertyPrefix;
import org.totoro.lock.InitWatcher;

import java.io.IOException;

/**
 * @author YHL
 * @version V1.0
 * @Description: redis 缓存配置
 * @date 2018-11-27
 */
@Configuration
public class LockConfig {

//
//    public static final Logger LOGGER = LoggerFactory.getLogger(LockConfig.class);
//
//    @Bean(Constant.Redis.LOCK_REDIS_CLIENT_BEAN_NAME)
//    @ConditionalOnMissingBean(name = Constant.Redis.LOCK_REDIS_CLIENT_BEAN_NAME)
//    @ConditionalOnPropertyPrefix(prefix = Constant.Redis.LOCK_REDIS_PREFIX)
//    public RedisTemplate lockRedisTemplate(@Autowired(required = false) RedisProperties redisPropertiesConfig) {
//
//        RedisTemplate redisClient = new RedisTemplate();
//        JedisConnectionFactory config = this.getJedisConnectionFactory(redisPropertiesConfig);
//        redisClient.setConnectionFactory(config);
//        return redisClient;
//    }
//
//
//    @Bean(Constant.Zookeeper.LOCK_ZOOKEEPER_CLIENT_BEAN_NAME)
//    @ConditionalOnPropertyPrefix(prefix = Constant.Zookeeper.LOCK_ZK_PREFIX)
//    @ConditionalOnMissingBean(name = Constant.Zookeeper.LOCK_ZOOKEEPER_CLIENT_BEAN_NAME)
//    public ZooKeeper lockZookeeper(@Autowired(required = false) LockZookeeperProperties lockZkProperties,
//                                   InitWatcher initWatcher) throws IOException {
//
//        String connectString = lockZkProperties.getConnectString();
//        Integer sessionTimeout = lockZkProperties.getSessionTimeout();
//        ZooKeeper zooKeeper = new ZooKeeper(connectString, sessionTimeout, initWatcher);
//
//        return zooKeeper;
//    }
//
//    @Bean
//    @ConditionalOnMissingBean(name = Constant.Zookeeper.ZK_INIT)
//    public InitWatcher zkInitWatcher() {
//        return (o) -> LOGGER.info(" 分布式 zk 连接成功");
//    }
//
//
//    /**
//     * 配置 redis 连接池 工厂
//     *
//     * @param redisPropertiesConfig
//     * @return
//     */
//    private JedisConnectionFactory getJedisConnectionFactory(RedisProperties redisPropertiesConfig) {
//        JedisConnectionFactory config = new JedisConnectionFactory();
//
//        config.setPort(redisPropertiesConfig.getPort());
//        config.setPassword(redisPropertiesConfig.getPassword());
//        config.setDatabase(redisPropertiesConfig.getDatabase());
//        config.setHostName(redisPropertiesConfig.getHost());
//        //   JedisPoolConfig jedisPoolConfig = this.getJedisPoolConfig(redisPropertiesConfig);
//        config.setPoolConfig(null);
//        config.afterPropertiesSet();
//
//        return config;
//    }


//    /**
//     * 配置 redis 连接池 信息
//     *
//     * @param redisPropertiesConfig
//     * @return
//     */
//    private JedisPoolConfig getJedisPoolConfig(RedisProperties redisPropertiesConfig) {
//
//        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
//        RedisProperties.Pool pool = redisPropertiesConfig.getPool();
//
//        if (pool != null) {
//            jedisPoolConfig.setMaxIdle(pool.getMaxIdle());
//            jedisPoolConfig.setMaxTotal(pool.getMaxActive());
//            jedisPoolConfig.setMaxWaitMillis(pool.getMaxWait());
//        }
//
//        return jedisPoolConfig;
//    }
}
