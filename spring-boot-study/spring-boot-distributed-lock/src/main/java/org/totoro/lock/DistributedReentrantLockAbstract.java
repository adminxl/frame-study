package org.totoro.lock;

import org.apache.zookeeper.KeeperException;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-12-04
 */
public abstract class DistributedReentrantLockAbstract implements DistributedReentrantLock {

    @Override
    public void lock(String lockId) throws KeeperException, InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void lockInterruptibly(String lockId) throws InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean tryLock(String lockId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean tryLock(String lockId, long time, TimeUnit unit) throws InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void unlock(String lockId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void lock() {

        try {
            lock(getLockId());
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        lockInterruptibly(getLockId());
    }

    @Override
    public boolean tryLock() {
        return tryLock(getLockId());
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return tryLock(getLockId(), time, unit);
    }

    @Override
    public void unlock() {
        unlock(getLockId());
    }

    @Override
    public Condition newCondition() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getLockId() {

        throw new UnsupportedOperationException();
    }
}
