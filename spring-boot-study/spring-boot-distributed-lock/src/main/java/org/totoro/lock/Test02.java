package org.totoro.lock;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-12-05
 */
public class Test02 {


    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {

        CountDownLatch countDownLatch = new CountDownLatch(1);
        ZooKeeper zooKeeper = new ZooKeeper("127.0.0.1:2181", 5000, e -> {
            countDownLatch.countDown();
        });

        countDownLatch.await();


        Stat exists = zooKeeper.exists("/ll", o -> {

            System.out.println("445646546546465");
        });


        System.out.println(exists);
//
//        List<String> children = zooKeeper.getChildren("/lock", false);
//
//        for (String child : children) {
//            System.out.println(child);
//        }






    }
}
