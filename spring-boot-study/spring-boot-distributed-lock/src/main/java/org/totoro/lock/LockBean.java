package org.totoro.lock;

/**
 * @author YHL
 * @version V1.0
 * @Description: 锁定对象
 * @date 2018-12-04
 */
public class LockBean {

    /**
     * 线程名称
     */
    private String threadName;

    /**
     * 线程编号
     */
    private String threadId;
    /**
     * 加锁资源 key
     */
    private String lockKey;

    public String getThreadName() {
        return threadName;
    }

    public LockBean setThreadName(String threadName) {
        this.threadName = threadName;
        return this;
    }

    public String getLockKey() {
        return lockKey;
    }

    public LockBean setLockKey(String lockKey) {
        this.lockKey = lockKey;
        return this;
    }

    public String getThreadId() {
        return threadId;
    }

    public LockBean setThreadId(String threadId) {
        this.threadId = threadId;
        return this;
    }
}
