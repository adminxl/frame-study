package org.totoro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.totoro.lock.redis.RedisDistributedReentrantLock;

/**
 * <p>
 * https://blog.csdn.net/xiaolyuh123/article/details/78536708?locationNum=8&fps=1
 * https://blog.csdn.net/hry2015/article/details/74937375?locationNum=6&fps=1
 * <p>
 * <p>
 * 超时处理思路
 * https://my.oschina.net/qixiaobo025/blog/2209801?tdsourcetag=s_pcqq_aiomsg
 */
@SpringBootApplication
@Import(RedisDistributedReentrantLock.class)
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
