package org.totoro.service;

import org.springframework.stereotype.Service;
import org.totoro.annotation.Cacheable;

/**
 * @author YHL
 * @version V1.0
 * @Description: 用户服务
 * @date 2018-11-15
 */
@Service
public class UserServiceImpl implements UserService {

    @Cacheable(cacheManager = "local")
    public UserInfo findUser(String userName) {

        UserInfo userInfo = new UserInfo();
        userInfo.setPassWord("123456");
        userInfo.setUserName("张三");

        return userInfo;
    }
}
