package org.totoro.service;

import org.totoro.annotation.Cacheable;

/**
 * @author YHL
 * @version V1.0
 * @Description: 用户服务
 * @date 2018-11-15
 */
public interface UserService {

    /**
     * @param userName
     * @return
     */
    @Cacheable(cacheManager = "local")
    UserInfo findUser(String userName);

}
