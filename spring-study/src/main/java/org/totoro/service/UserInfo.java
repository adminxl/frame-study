package org.totoro.service;

/**
 * @author YHL
 * @version V1.0
 * @Description:
 * @date 2018-11-15
 */
public class UserInfo {

    private String userName;

    private String passWord;

    public String getUserName() {
        return userName;
    }

    public UserInfo setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassWord() {
        return passWord;
    }

    public UserInfo setPassWord(String passWord) {
        this.passWord = passWord;
        return this;
    }
}
