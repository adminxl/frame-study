package org.totoro.annotation;

import java.lang.annotation.*;


@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cacheable {

    /**
     * 缓存的 key (不填时为全部示参数)
     *
     * @return
     */
    String keyGeneratorExpression() default "";

    /**
     * 缓存时间 单位 TimeUnit.MILLISECONDS (默认15分钟)
     *
     * @return
     */
    long expire() default 15 * 60 * 1000;

    /**
     * 是否启用降级
     *
     * @return
     */
    boolean isDowngrade() default false;


    /**
     * 缓存处理器在 spring 容器中的 key
     *
     * @return
     */
    String cacheManager();


}
