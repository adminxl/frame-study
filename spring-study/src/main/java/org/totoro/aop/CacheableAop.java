package org.totoro.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.totoro.annotation.Cacheable;

/**
 * @author YHL
 * @version V1.0
 * @Description: 缓存切面
 * @date 2018-11-16
 */

@Aspect
@Component
public class CacheableAop {


    @Around("@annotation(cacheable)")
    public Object precess(ProceedingJoinPoint pjd, Cacheable cacheable) throws Throwable {


        System.out.println("进入切面");

        return pjd.proceed();
    }

}
