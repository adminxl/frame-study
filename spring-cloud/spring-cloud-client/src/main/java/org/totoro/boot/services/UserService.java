package org.totoro.boot.services;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.totoro.boot.UserBean;

/**
 * Created by Administrator on 2017/7/18.
 */
@FeignClient(name = "CLOUD-SERVICE")
public interface UserService {

    @RequestMapping(value = "/findUser/{name}",method = RequestMethod.POST)
    public UserBean findUser(@PathVariable("name") String name);

}
