package org.totoro.boot.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/6/29.
 */


@RestController()
@RequestMapping("eurekaController")
public class EurekaController {

    @Autowired
    private DiscoveryClient discoveryClient;



    @RequestMapping("index")
    public String index(){


        ServiceInstance localServiceInstance = discoveryClient.getLocalServiceInstance();

        System.out.println();

        return "调用成功！";
    }



}
