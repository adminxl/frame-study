package org.totoro.boot;

/**
 * Created by Administrator on 2017/6/29.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 使用eureka做服务发现
 * @EnableEurekaServer
 * 该注解表明应用为eureka服务，有可以联合多个服务作为集群，对外提供服务注册以及发现功能
 * @author pangps
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {

    /**
     * http://blog.csdn.net/loveuserzzz/article/details/53414685
     * <p>
     * https://github.com/yifanzzz/spring-cloud/
     * <p>
     * http://blog.csdn.net/beyannanfei/article/details/52069725
     *
     * @param args
     */


    public static void main(String[] args) {

        SpringApplication.run(EurekaApplication.class, args);
    }

}
