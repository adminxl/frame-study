
#spring 相关配置
### spring.application.name
> 项目名称
### spring.application.profiles
> 在yml文件中会配置多个profile，该参数是设置profile的名称

### spring.profiles.active=peer1
> 激活配置制定配置

###spring.cloud.loadbalancer.retry.enabled
> 该参数用来开启重试机制，它默认是关闭的。这里需要注意，官方文档中的配置参数少了enabled。

### server.port
> web 服务器端口

#eureka 注册中心配置

### eureka.client.serviceUrl.defaultZone
> 注册中心地址

### eureka.instance.hostname
> 设置注册中心的hostname，主要是为了，在ip 修改了的时候，只需要修改服务器的hosts文件即可

### eureka.instance.perferIpAddress
> 默认值:false
> 如果不想通过 hostname 访问，可以设置该参数

### eureka.client.fetchRegistry
> 默认值:false
> 是否将eureka自身作为应用注册到eureka注册中心 （单机模式下设置为true，集群模式下设置为false  PS:如果集群模式下为true，会导致 eureka 一直处于 unavailable-replicas 状态）


###hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds

> 断路器的超时时间需要大于ribbon的超时时间，不然不会触发重试。

###ribbon.ConnectTimeout

>请求连接的超时时间

###ribbon.ReadTimeout

> 请求处理的超时时间

### ribbon.OkToRetryOnAllOperations

> 对所有操作请求都进行重试

### ribbon.MaxAutoRetriesNextServer

> 切换实例的重试次数

###ribbon.MaxAutoRetries

> 对当前实例的重试次数

















