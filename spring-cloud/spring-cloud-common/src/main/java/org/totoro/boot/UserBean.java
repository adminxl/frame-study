package org.totoro.boot;

/**
 * Created by Administrator on 2017/6/29.
 */
public class UserBean {

    public String username;
    public Integer age;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public UserBean(String username, Integer age) {
        this.username = username;
        this.age = age;
    }

    public UserBean() {

    }
}
