package org.totoro;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * Hello world!
 */
@SpringCloudApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }


}
