package org.totoro.demo;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author YHL
 * @version V1.0
 * @Description: test 控制器
 * @date 2018-10-22
 */
@RestController
@RequestMapping("/test")
public class TestContr {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String test(@PathVariable int id) {

        return "a1 ：" + id;
    }

}
