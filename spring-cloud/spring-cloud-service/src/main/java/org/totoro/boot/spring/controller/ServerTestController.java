package org.totoro.boot.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import org.totoro.boot.UserBean;

/**
 * Created by Administrator on 2017/6/29.
 */
@RestController()
public class ServerTestController {

    public ServerTestController(){
        System.out.println("init");
    }


    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/findUser/{name}")
    public UserBean findUser(@PathVariable("name") String name){

        ServiceInstance localServiceInstance = discoveryClient.getLocalServiceInstance();

        return new UserBean(name,12);
    }

}
