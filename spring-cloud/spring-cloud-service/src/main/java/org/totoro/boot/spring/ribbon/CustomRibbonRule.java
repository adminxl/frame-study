package org.totoro.boot.spring.ribbon;

import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.RoundRobinRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.WeightedResponseTimeRule;

/**
 * Created by Administrator on 2017/6/30.
 */
public class CustomRibbonRule extends RoundRobinRule {


    @Override
    public Server choose(ILoadBalancer lb, Object key) {
        return super.choose(lb, key);
    }

    @Override
    public Server choose(Object key) {
        return super.choose(key);
    }
}
