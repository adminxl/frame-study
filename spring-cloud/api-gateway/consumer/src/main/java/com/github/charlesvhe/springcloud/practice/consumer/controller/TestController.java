package com.github.charlesvhe.springcloud.practice.consumer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by charles on 2017/5/25.
 */
@RestController
@RequestMapping("/test")
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping(method = RequestMethod.GET)
    public String test(@RequestHeader("x-label") String label) {
        logger.info("label: " + label);
        String result = restTemplate.getForObject("http://provider/user", String.class);
        /**
         * 如果启用了   @LoadBalanced，就不能通过ip 地址进行访问了，不然会报 No instances available for
         */
        //     String result = restTemplate.getForObject("http://127.0.0.1:18080/user", String.class);
        return result;
    }
}
