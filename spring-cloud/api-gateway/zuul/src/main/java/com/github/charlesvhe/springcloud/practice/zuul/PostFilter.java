package com.github.charlesvhe.springcloud.practice.zuul;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.util.StreamUtils;

import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Zuul提供默认的四种过滤器类型，通过filterType方法进行标识
 * <pre>
 * pre：可以在请求被路由之前调用
 * route：在路由请求时候被调用
 * post：在route和error过滤器之后被调用
 * error：处理请求时发生错误时被调用
 * </pre>
 *
 *
 *
 * <p>
 *     调用过滤器源码
 *     <pre>
 *         主要是通过 @see com.netflix.zuul.FilterProcessor
 *     </pre>
 * </p>
 */
public class PostFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * true：执行这个过滤器
     * false：不执行这个过滤器
     *
     * @return
     */
    @Override
    public boolean shouldFilter() {

        RequestContext ctx = RequestContext.getCurrentContext();

        String requestURI = String.valueOf(ctx.get("requestURI"));

        System.out.println(requestURI);

        return true;
    }

    @Override
    public Object run() {

        RequestContext context = RequestContext.getCurrentContext();

        try {
            // 获取返回值内容，加以处理
            InputStream stream = context.getResponseDataStream();
            String body = StreamUtils.copyToString(stream, Charset.forName("UTF-8"));
            String returnStr = body + " 我是过滤器加入的内容";
            // 内容重新写入
            context.setResponseBody(returnStr);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }
}
