package com.github.charlesvhe.springcloud.practice.zuul.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulClassConfig {

    @Autowired
    private ZuulProperties zuulProperties;
    @Autowired
    private ServerProperties server;
    @Autowired
    private DiscoveryClient discovery;


    @Bean
    public DynamicRefreshableRouteLocator getDynamicRefreshableRouteLocator() {

        DynamicRefreshableRouteLocator dynamicRefreshableRouteLocator = new DynamicRefreshableRouteLocator(server
                .getServletPrefix(), discovery,
                zuulProperties);

        return dynamicRefreshableRouteLocator;
    }

}
