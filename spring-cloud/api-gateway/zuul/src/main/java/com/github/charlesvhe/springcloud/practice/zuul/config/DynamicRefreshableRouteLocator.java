package com.github.charlesvhe.springcloud.practice.zuul.config;

import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator;
import org.springframework.cloud.netflix.zuul.filters.discovery.ServiceRouteMapper;

import java.util.LinkedHashMap;

/**
 * 动态刷新路由
 *
 * <pre>
 * 源码实现
 *
 * @see org.springframework.cloud.netflix.zuul.filters.discovery.DiscoveryClientRouteLocator
 * <pre>
 *
 *
 * 配合 路由刷新事件，便可以动态刷新路由
 * @see org.springframework.cloud.netflix.zuul.RoutesRefreshedEvent
 * @see RefreshRouteService
 */
public class DynamicRefreshableRouteLocator extends DiscoveryClientRouteLocator {


    public DynamicRefreshableRouteLocator(String servletPath, DiscoveryClient discovery, ZuulProperties properties) {
        super(servletPath, discovery, properties);
    }

    public DynamicRefreshableRouteLocator(String servletPath, DiscoveryClient discovery, ZuulProperties properties, ServiceRouteMapper serviceRouteMapper) {
        super(servletPath, discovery, properties, serviceRouteMapper);
    }

    @Override
    protected LinkedHashMap<String, ZuulProperties.ZuulRoute> locateRoutes() {

        LinkedHashMap<String, ZuulProperties.ZuulRoute> stringZuulRouteLinkedHashMap = super.locateRoutes();

        ZuulProperties.ZuulRoute zuulRoute = new ZuulProperties.ZuulRoute();
        zuulRoute.setId("provider1");
        /**
         * 设置zuul path 进行转发
         */
        zuulRoute.setPath("/provider5555/**");
        zuulRoute.setServiceId("provider");
        /**
         * 值必须和 zuulRoute#path 的一样
         */
        stringZuulRouteLinkedHashMap.put("/provider5555/**", zuulRoute);


        return stringZuulRouteLinkedHashMap;
    }
}
